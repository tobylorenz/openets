#!/usr/bin/python3

import logging
import os
import xml.etree.ElementTree as ET
import glob


# replace namespace URLs to namespace prefix
def fix_ns(tag):
    tag = tag.replace('{http://knx.org/xml/project/10}', '')
    tag = tag.replace('{http://knx.org/xml/project/11}', '')
    tag = tag.replace('{http://knx.org/xml/project/12}', '')
    tag = tag.replace('{http://knx.org/xml/project/13}', '')
    tag = tag.replace('{http://knx.org/xml/project/14}', '')
    tag = tag.replace('{http://knx.org/xml/project/20}', '')
    return tag

# ID to tag
id_to_tag = dict()

def get_id_to_tag(xml_element):
    tag = fix_ns(xml_element.tag)
    if 'Id' in xml_element.attrib:
        id_to_tag[xml_element.attrib['Id']] = tag

    for child in xml_element:
        get_id_to_tag(child)

# id_mask to tag
id_mask_to_tag = dict()

def id_to_mask(id):
    id_parts = id.split('_')
    mask_parts = []
    for id_part in id_parts:
        sub_id_parts = id_part.split('-')
        mask_parts.append(sub_id_parts[0] + '-*')
    return '_'.join(mask_parts)

def get_id_mask_to_tag():
    for id in id_to_tag:
        id_mask = id_to_mask(id)
        id_mask_to_tag[id_mask] = id_to_tag[id]


# tag to attrib to tag
tag_to_attrib_to_tag = dict()

def get_tag_to_attrib_to_tag(xml_element):
    tag = fix_ns(xml_element.tag)
    if 'Id' in xml_element.attrib:
        id = xml_element.attrib['Id']
        id_mask = id_to_mask(id)
        tag = tag + " (" + id_mask + ")"
    for attrib in xml_element.attrib:
        if attrib != "Id":
            if xml_element.attrib[attrib] in id_to_tag:
                id2 = xml_element.attrib[attrib]
                id_mask2 = id_to_mask(id2)
                tag2 = id_to_tag[id2]
                if not tag in tag_to_attrib_to_tag:
                    tag_to_attrib_to_tag[tag] = dict()
                if not attrib in tag_to_attrib_to_tag[tag]:
                    tag_to_attrib_to_tag[tag][attrib] = set()
                tag_to_attrib_to_tag[tag][attrib].add(tag2 + " (" + id_mask2 + ")")

    for child in xml_element:
        get_tag_to_attrib_to_tag(child)

# main
def main():
    logging.basicConfig(
        format='%(levelname)s:%(name)s:%(message)s [%(filename)s:%(lineno)d]',
        level=logging.WARNING)  # DEBUG, INFO, WARNING, ERROR, CRITICAL

    # get all XML files
    xml_filenames = glob.glob('/home/users/tobias/bitbucket/OpenETS.references/Sample_Projects/2022-01-03_Kinzenbach/**/*.xml', recursive=True)
    print("=== Files ===")
    for xml_filename in xml_filenames:
        print(xml_filename)

    # parse ID to tag
    for xml_filename in xml_filenames:
        tree = ET.parse(xml_filename)
        root = tree.getroot()
        get_id_to_tag(root)

    # parse ID mask to tag
    get_id_mask_to_tag()

    for xml_filename in xml_filenames:
        tree = ET.parse(xml_filename)
        root = tree.getroot()
        get_tag_to_attrib_to_tag(root)

    print("=== ID masks and corresponding tags ===")
    for id_mask in sorted(id_mask_to_tag):
        print(id_mask + " " + id_mask_to_tag[id_mask])

    print("=== Tags referencing other tags ===")
    for tag1 in sorted(tag_to_attrib_to_tag):
        print("element " + tag1)
        for attrib in tag_to_attrib_to_tag[tag1]:
            print("  attribute " + attrib)
            for tag2 in tag_to_attrib_to_tag[tag1][attrib]:
                print("    element " + tag2)

if __name__ == '__main__':
    # execute only if run as a script
    main()
