/* This file is generated. */

#pragma once

#include <Project/Base.h>

namespace Project {
namespace v10 {
namespace knx {

/* forward declarations */
class BinaryDataRef_t;
class IndependentParameterBlockChoose_t;
class ParameterBlock_t;

class ApplicationProgramDynamic_ChannelIndependentBlock_t : public Base
{
    Q_OBJECT

public:
    explicit ApplicationProgramDynamic_ChannelIndependentBlock_t(Base * parent = nullptr);
    virtual ~ApplicationProgramDynamic_ChannelIndependentBlock_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* elements */
    // xs:choice ParameterBlock_t * ParameterBlock{};
    // xs:choice IndependentParameterBlockChoose_t * Choose{};
    // xs:choice BinaryDataRef_t * BinaryDataRef{};
};

ApplicationProgramDynamic_ChannelIndependentBlock_t * make_ApplicationProgramDynamic_ChannelIndependentBlock_t(Base * parent);

} // namespace knx
} // namespace v10
} // namespace Project
