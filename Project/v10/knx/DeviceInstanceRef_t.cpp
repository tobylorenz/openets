/* This file is generated. */

#include <Project/v10/knx/DeviceInstanceRef_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v10/knx/DeviceInstance_t.h>
#include <Project/v10/knx/KNX_t.h>

namespace Project {
namespace v10 {
namespace knx {

DeviceInstanceRef_t::DeviceInstanceRef_t(Base * parent) :
    Base(parent)
{
}

DeviceInstanceRef_t::~DeviceInstanceRef_t()
{
}

void DeviceInstanceRef_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("RefId")) {
            RefId = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int DeviceInstanceRef_t::tableColumnCount() const
{
    return 2;
}

QVariant DeviceInstanceRef_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "DeviceInstanceRef";
        }
        if (qualifiedName == QString("RefId")) {
            return RefId;
        }
        break;
    }
    return QVariant();
}

QVariant DeviceInstanceRef_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "RefId";
            }
        }
    }
    return QVariant();
}

QVariant DeviceInstanceRef_t::treeData(int role) const
{
    auto * ref = getDeviceInstance();
    if (ref) {
        return ref->treeData(role);
    }

    switch (role) {
    case Qt::DisplayRole:
        return "DeviceInstanceRef";
    case Qt::DecorationRole:
        return QIcon::fromTheme("DeviceInstanceRef");
    }
    return QVariant();
}

DeviceInstance_t * DeviceInstanceRef_t::getDeviceInstance() const
{
    if (RefId.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<DeviceInstance_t *>(knx->ids[RefId]);
}

DeviceInstanceRef_t * make_DeviceInstanceRef_t(Base * parent)
{
    return new DeviceInstanceRef_t(parent);
}

} // namespace knx
} // namespace v10
} // namespace Project
