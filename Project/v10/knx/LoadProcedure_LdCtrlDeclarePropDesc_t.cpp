/* This file is generated. */

#include <Project/v10/knx/LoadProcedure_LdCtrlDeclarePropDesc_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v10/knx/KNX_t.h>

namespace Project {
namespace v10 {
namespace knx {

LoadProcedure_LdCtrlDeclarePropDesc_t::LoadProcedure_LdCtrlDeclarePropDesc_t(Base * parent) :
    Base(parent)
{
}

LoadProcedure_LdCtrlDeclarePropDesc_t::~LoadProcedure_LdCtrlDeclarePropDesc_t()
{
}

void LoadProcedure_LdCtrlDeclarePropDesc_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("ObjIdx")) {
            ObjIdx = attribute.value().toString();
            continue;
        }
        if (name == QString("ObjType")) {
            ObjType = attribute.value().toString();
            continue;
        }
        if (name == QString("Occurrence")) {
            Occurrence = attribute.value().toString();
            continue;
        }
        if (name == QString("PropId")) {
            PropId = attribute.value().toString();
            continue;
        }
        if (name == QString("PropType")) {
            PropType = attribute.value().toString();
            continue;
        }
        if (name == QString("MaxElements")) {
            MaxElements = attribute.value().toString();
            continue;
        }
        if (name == QString("ReadAccess")) {
            ReadAccess = attribute.value().toString();
            continue;
        }
        if (name == QString("WriteAccess")) {
            WriteAccess = attribute.value().toString();
            continue;
        }
        if (name == QString("Writable")) {
            Writable = attribute.value().toString();
            continue;
        }
        if (name == QString("AppliesTo")) {
            AppliesTo = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int LoadProcedure_LdCtrlDeclarePropDesc_t::tableColumnCount() const
{
    return 11;
}

QVariant LoadProcedure_LdCtrlDeclarePropDesc_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "LoadProcedure_LdCtrlDeclarePropDesc";
        }
        if (qualifiedName == QString("ObjIdx")) {
            return ObjIdx;
        }
        if (qualifiedName == QString("ObjType")) {
            return ObjType;
        }
        if (qualifiedName == QString("Occurrence")) {
            return Occurrence;
        }
        if (qualifiedName == QString("PropId")) {
            return PropId;
        }
        if (qualifiedName == QString("PropType")) {
            return PropType;
        }
        if (qualifiedName == QString("MaxElements")) {
            return MaxElements;
        }
        if (qualifiedName == QString("ReadAccess")) {
            return ReadAccess;
        }
        if (qualifiedName == QString("WriteAccess")) {
            return WriteAccess;
        }
        if (qualifiedName == QString("Writable")) {
            return Writable;
        }
        if (qualifiedName == QString("AppliesTo")) {
            return AppliesTo;
        }
        break;
    }
    return QVariant();
}

QVariant LoadProcedure_LdCtrlDeclarePropDesc_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "ObjIdx";
            case 2:
                return "ObjType";
            case 3:
                return "Occurrence";
            case 4:
                return "PropId";
            case 5:
                return "PropType";
            case 6:
                return "MaxElements";
            case 7:
                return "ReadAccess";
            case 8:
                return "WriteAccess";
            case 9:
                return "Writable";
            case 10:
                return "AppliesTo";
            }
        }
    }
    return QVariant();
}

QVariant LoadProcedure_LdCtrlDeclarePropDesc_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "LdCtrlDeclarePropDesc";
    case Qt::DecorationRole:
        return QIcon::fromTheme("LoadProcedure_LdCtrlDeclarePropDesc");
    }
    return QVariant();
}

LoadProcedure_LdCtrlDeclarePropDesc_t * make_LoadProcedure_LdCtrlDeclarePropDesc_t(Base * parent)
{
    return new LoadProcedure_LdCtrlDeclarePropDesc_t(parent);
}

} // namespace knx
} // namespace v10
} // namespace Project
