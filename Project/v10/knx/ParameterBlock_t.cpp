/* This file is generated. */

#include <Project/v10/knx/ParameterBlock_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v10/knx/Assign_t.h>
#include <Project/v10/knx/BinaryDataRef_t.h>
#include <Project/v10/knx/KNX_t.h>
#include <Project/v10/knx/ParameterChoose_t.h>
#include <Project/v10/knx/ParameterRefRef_t.h>
#include <Project/v10/knx/ParameterSeparator_t.h>

namespace Project {
namespace v10 {
namespace knx {

ParameterBlock_t::ParameterBlock_t(Base * parent) :
    Base(parent)
{
}

ParameterBlock_t::~ParameterBlock_t()
{
}

void ParameterBlock_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == "Id") {
            Id = attribute.value().toString();
            KNX_t * knx = findParent<KNX_t *>();
            Q_ASSERT(knx);
            knx->ids[Id] = this;
            continue;
        }
        if (name == QString("Name")) {
            Name = attribute.value().toString();
            continue;
        }
        if (name == QString("Text")) {
            Text = attribute.value().toString();
            continue;
        }
        if (name == QString("Access")) {
            Access = attribute.value().toString();
            continue;
        }
        if (name == QString("HelpTopic")) {
            HelpTopic = attribute.value().toString();
            continue;
        }
        if (name == QString("InternalDescription")) {
            InternalDescription = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("ParameterSeparator")) {
            auto * ParameterSeparator = make_ParameterSeparator_t(this);
            ParameterSeparator->read(reader);
            continue;
        }
        if (reader.name() == QString("ParameterRefRef")) {
            auto * ParameterRefRef = make_ParameterRefRef_t(this);
            ParameterRefRef->read(reader);
            continue;
        }
        if (reader.name() == QString("choose")) {
            auto * Choose = make_ParameterChoose_t(this);
            Choose->read(reader);
            continue;
        }
        if (reader.name() == QString("BinaryDataRef")) {
            auto * BinaryDataRef = make_BinaryDataRef_t(this);
            BinaryDataRef->read(reader);
            continue;
        }
        if (reader.name() == QString("Assign")) {
            auto * Assign = make_Assign_t(this);
            Assign->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ParameterBlock_t::tableColumnCount() const
{
    return 7;
}

QVariant ParameterBlock_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ParameterBlock";
        }
        if (qualifiedName == QString("Id")) {
            return Id;
        }
        if (qualifiedName == QString("Name")) {
            return Name;
        }
        if (qualifiedName == QString("Text")) {
            return Text;
        }
        if (qualifiedName == QString("Access")) {
            return Access;
        }
        if (qualifiedName == QString("HelpTopic")) {
            return HelpTopic;
        }
        if (qualifiedName == QString("InternalDescription")) {
            return InternalDescription;
        }
        break;
    }
    return QVariant();
}

QVariant ParameterBlock_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Id";
            case 2:
                return "Name";
            case 3:
                return "Text";
            case 4:
                return "Access";
            case 5:
                return "HelpTopic";
            case 6:
                return "InternalDescription";
            }
        }
    }
    return QVariant();
}

QVariant ParameterBlock_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return !Name.isEmpty() ? Name : "ParameterBlock";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ParameterBlock");
    }
    return QVariant();
}

ParameterBlock_t * make_ParameterBlock_t(Base * parent)
{
    return new ParameterBlock_t(parent);
}

} // namespace knx
} // namespace v10
} // namespace Project
