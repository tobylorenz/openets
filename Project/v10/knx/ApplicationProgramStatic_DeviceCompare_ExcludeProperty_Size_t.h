/* This file is generated. */

#pragma once

#include <Project/xs/UnsignedInt.h>

namespace Project {
namespace v10 {
namespace knx {

using ApplicationProgramStatic_DeviceCompare_ExcludeProperty_Size_t = xs::UnsignedInt;

} // namespace knx
} // namespace v10
} // namespace Project
