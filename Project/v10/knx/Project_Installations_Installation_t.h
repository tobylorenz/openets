/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v10/knx/CompletionStatus_t.h>
#include <Project/v10/knx/Ipv4Address_t.h>
#include <Project/v10/knx/Project_Installations_Installation_InstallationId_t.h>
#include <Project/v10/knx/String50_t.h>
#include <Project/xs/String.h>
#include <Project/xs/UnsignedLong.h>

namespace Project {
namespace v10 {
namespace knx {

/* forward declarations */
class Buildings_t;
class BusAccess_t;
class GroupAddresses_t;
class Topology_t;
class Trades_t;

class Project_Installations_Installation_t : public Base
{
    Q_OBJECT

public:
    explicit Project_Installations_Installation_t(Base * parent = nullptr);
    virtual ~Project_Installations_Installation_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    String50_t Name{};
    Project_Installations_Installation_InstallationId_t InstallationId{};
    xs::UnsignedLong BCUKey{"4294967295"};
    Ipv4Address_t IPRoutingMulticastAddress{"224.0.23.12"};
    xs::String DefaultLine{};
    CompletionStatus_t CompletionStatus{"Undefined"};

    /* elements */
    Topology_t * Topology{};
    Buildings_t * Buildings{};
    GroupAddresses_t * GroupAddresses{};
    Trades_t * Trades{};
    BusAccess_t * BusAccess{};
};

Project_Installations_Installation_t * make_Project_Installations_Installation_t(Base * parent);

} // namespace knx
} // namespace v10
} // namespace Project
