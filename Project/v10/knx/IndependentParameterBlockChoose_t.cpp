/* This file is generated. */

#include <Project/v10/knx/IndependentParameterBlockChoose_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v10/knx/KNX_t.h>

namespace Project {
namespace v10 {
namespace knx {

IndependentParameterBlockChoose_t::IndependentParameterBlockChoose_t(Base * parent) :
    Base(parent)
{
}

IndependentParameterBlockChoose_t::~IndependentParameterBlockChoose_t()
{
}

void IndependentParameterBlockChoose_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("ParamRefId")) {
            ParamRefId = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("when")) {
            auto * newWhen = make_IndependentParameterBlockChoose_when_t(this);
            newWhen->read(reader);
            When.append(newWhen);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int IndependentParameterBlockChoose_t::tableColumnCount() const
{
    return 2;
}

QVariant IndependentParameterBlockChoose_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "IndependentParameterBlockChoose";
        }
        if (qualifiedName == QString("ParamRefId")) {
            return ParamRefId;
        }
        break;
    }
    return QVariant();
}

QVariant IndependentParameterBlockChoose_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "ParamRefId";
            }
        }
    }
    return QVariant();
}

QVariant IndependentParameterBlockChoose_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "IndependentParameterBlockChoose";
    case Qt::DecorationRole:
        return QIcon::fromTheme("IndependentParameterBlockChoose");
    }
    return QVariant();
}

IndependentParameterBlockChoose_t * make_IndependentParameterBlockChoose_t(Base * parent)
{
    return new IndependentParameterBlockChoose_t(parent);
}

} // namespace knx
} // namespace v10
} // namespace Project
