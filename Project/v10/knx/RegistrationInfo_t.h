/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v10/knx/RegistrationInfo_RegistrationKey_t.h>
#include <Project/v10/knx/RegistrationNumber_t.h>
#include <Project/v10/knx/RegistrationStatus_t.h>
#include <Project/xs/Base64Binary.h>
#include <Project/xs/Date.h>

namespace Project {
namespace v10 {
namespace knx {

class RegistrationInfo_t : public Base
{
    Q_OBJECT

public:
    explicit RegistrationInfo_t(Base * parent = nullptr);
    virtual ~RegistrationInfo_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    RegistrationStatus_t RegistrationStatus{};
    RegistrationNumber_t RegistrationNumber{};
    RegistrationNumber_t OriginalRegistrationNumber{};
    xs::Date RegistrationDate{};
    xs::Base64Binary RegistrationSignature{};
    RegistrationInfo_RegistrationKey_t RegistrationKey{"knxconv"};
};

RegistrationInfo_t * make_RegistrationInfo_t(Base * parent);

} // namespace knx
} // namespace v10
} // namespace Project
