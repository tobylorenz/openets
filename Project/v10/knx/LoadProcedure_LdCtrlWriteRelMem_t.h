/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v10/knx/LdCtrlProcType_t.h>
#include <Project/xs/Boolean.h>
#include <Project/xs/HexBinary.h>
#include <Project/xs/UnsignedByte.h>
#include <Project/xs/UnsignedInt.h>

namespace Project {
namespace v10 {
namespace knx {

class LoadProcedure_LdCtrlWriteRelMem_t : public Base
{
    Q_OBJECT

public:
    explicit LoadProcedure_LdCtrlWriteRelMem_t(Base * parent = nullptr);
    virtual ~LoadProcedure_LdCtrlWriteRelMem_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::UnsignedByte ObjIdx{};
    xs::UnsignedInt Offset{};
    xs::UnsignedInt Size{};
    xs::Boolean Verify{};
    xs::HexBinary InlineData{};
    LdCtrlProcType_t AppliesTo{"auto"};
};

LoadProcedure_LdCtrlWriteRelMem_t * make_LoadProcedure_LdCtrlWriteRelMem_t(Base * parent);

} // namespace knx
} // namespace v10
} // namespace Project
