/* This file is generated. */

#pragma once

#include <Project/Base.h>

namespace Project {
namespace v10 {
namespace knx {

/* forward declarations */
class LoadProcedure_LdCtrlAbsSegment_t;
class LoadProcedure_LdCtrlClearCachedObjectTypes_t;
class LoadProcedure_LdCtrlClearLCFilterTable_t;
class LoadProcedure_LdCtrlCompareMem_t;
class LoadProcedure_LdCtrlCompareProp_t;
class LoadProcedure_LdCtrlCompareRelMem_t;
class LoadProcedure_LdCtrlConnect_t;
class LoadProcedure_LdCtrlDeclarePropDesc_t;
class LoadProcedure_LdCtrlDelay_t;
class LoadProcedure_LdCtrlDisconnect_t;
class LoadProcedure_LdCtrlInvokeFunctionProp_t;
class LoadProcedure_LdCtrlLoadCompleted_t;
class LoadProcedure_LdCtrlLoadImageMem_t;
class LoadProcedure_LdCtrlLoadImageProp_t;
class LoadProcedure_LdCtrlLoadImageRelMem_t;
class LoadProcedure_LdCtrlLoad_t;
class LoadProcedure_LdCtrlMapError_t;
class LoadProcedure_LdCtrlMaxLength_t;
class LoadProcedure_LdCtrlMerge_t;
class LoadProcedure_LdCtrlProgressText_t;
class LoadProcedure_LdCtrlReadFunctionProp_t;
class LoadProcedure_LdCtrlRelSegment_t;
class LoadProcedure_LdCtrlRestart_t;
class LoadProcedure_LdCtrlSetControlVariable_t;
class LoadProcedure_LdCtrlTaskCtrl1_t;
class LoadProcedure_LdCtrlTaskCtrl2_t;
class LoadProcedure_LdCtrlTaskPtr_t;
class LoadProcedure_LdCtrlTaskSegment_t;
class LoadProcedure_LdCtrlUnload_t;
class LoadProcedure_LdCtrlWriteMem_t;
class LoadProcedure_LdCtrlWriteProp_t;
class LoadProcedure_LdCtrlWriteRelMem_t;

class LoadProcedure_t : public Base
{
    Q_OBJECT

public:
    explicit LoadProcedure_t(Base * parent = nullptr);
    virtual ~LoadProcedure_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* elements */
    // xs:choice LoadProcedure_LdCtrlUnload_t * LdCtrlUnload{};
    // xs:choice LoadProcedure_LdCtrlLoad_t * LdCtrlLoad{};
    // xs:choice LoadProcedure_LdCtrlMaxLength_t * LdCtrlMaxLength{};
    // xs:choice LoadProcedure_LdCtrlClearCachedObjectTypes_t * LdCtrlClearCachedObjectTypes{};
    // xs:choice LoadProcedure_LdCtrlLoadCompleted_t * LdCtrlLoadCompleted{};
    // xs:choice LoadProcedure_LdCtrlAbsSegment_t * LdCtrlAbsSegment{};
    // xs:choice LoadProcedure_LdCtrlRelSegment_t * LdCtrlRelSegment{};
    // xs:choice LoadProcedure_LdCtrlTaskSegment_t * LdCtrlTaskSegment{};
    // xs:choice LoadProcedure_LdCtrlTaskPtr_t * LdCtrlTaskPtr{};
    // xs:choice LoadProcedure_LdCtrlTaskCtrl1_t * LdCtrlTaskCtrl1{};
    // xs:choice LoadProcedure_LdCtrlTaskCtrl2_t * LdCtrlTaskCtrl2{};
    // xs:choice LoadProcedure_LdCtrlWriteProp_t * LdCtrlWriteProp{};
    // xs:choice LoadProcedure_LdCtrlCompareProp_t * LdCtrlCompareProp{};
    // xs:choice LoadProcedure_LdCtrlLoadImageProp_t * LdCtrlLoadImageProp{};
    // xs:choice LoadProcedure_LdCtrlInvokeFunctionProp_t * LdCtrlInvokeFunctionProp{};
    // xs:choice LoadProcedure_LdCtrlReadFunctionProp_t * LdCtrlReadFunctionProp{};
    // xs:choice LoadProcedure_LdCtrlWriteMem_t * LdCtrlWriteMem{};
    // xs:choice LoadProcedure_LdCtrlCompareMem_t * LdCtrlCompareMem{};
    // xs:choice LoadProcedure_LdCtrlLoadImageMem_t * LdCtrlLoadImageMem{};
    // xs:choice LoadProcedure_LdCtrlWriteRelMem_t * LdCtrlWriteRelMem{};
    // xs:choice LoadProcedure_LdCtrlCompareRelMem_t * LdCtrlCompareRelMem{};
    // xs:choice LoadProcedure_LdCtrlLoadImageRelMem_t * LdCtrlLoadImageRelMem{};
    // xs:choice LoadProcedure_LdCtrlConnect_t * LdCtrlConnect{};
    // xs:choice LoadProcedure_LdCtrlDisconnect_t * LdCtrlDisconnect{};
    // xs:choice LoadProcedure_LdCtrlRestart_t * LdCtrlRestart{};
    // xs:choice LoadProcedure_LdCtrlDelay_t * LdCtrlDelay{};
    // xs:choice LoadProcedure_LdCtrlSetControlVariable_t * LdCtrlSetControlVariable{};
    // xs:choice LoadProcedure_LdCtrlMapError_t * LdCtrlMapError{};
    // xs:choice LoadProcedure_LdCtrlProgressText_t * LdCtrlProgressText{};
    // xs:choice LoadProcedure_LdCtrlDeclarePropDesc_t * LdCtrlDeclarePropDesc{};
    // xs:choice LoadProcedure_LdCtrlClearLCFilterTable_t * LdCtrlClearLCFilterTable{};
    // xs:choice LoadProcedure_LdCtrlMerge_t * LdCtrlMerge{};
};

LoadProcedure_t * make_LoadProcedure_t(Base * parent);

} // namespace knx
} // namespace v10
} // namespace Project
