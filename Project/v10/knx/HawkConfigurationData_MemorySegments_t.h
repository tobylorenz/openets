/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v10/knx/HawkConfigurationData_MemorySegments_MemorySegment_t.h>

namespace Project {
namespace v10 {
namespace knx {

class HawkConfigurationData_MemorySegments_t : public Base
{
    Q_OBJECT

public:
    explicit HawkConfigurationData_MemorySegments_t(Base * parent = nullptr);
    virtual ~HawkConfigurationData_MemorySegments_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* elements */
    QVector<HawkConfigurationData_MemorySegments_MemorySegment_t *> MemorySegment;
};

HawkConfigurationData_MemorySegments_t * make_HawkConfigurationData_MemorySegments_t(Base * parent);

} // namespace knx
} // namespace v10
} // namespace Project
