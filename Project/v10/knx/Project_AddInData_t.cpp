/* This file is generated. */

#include <Project/v10/knx/Project_AddInData_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v10/knx/KNX_t.h>

namespace Project {
namespace v10 {
namespace knx {

Project_AddInData_t::Project_AddInData_t(Base * parent) :
    Base(parent)
{
}

Project_AddInData_t::~Project_AddInData_t()
{
}

void Project_AddInData_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("AddInData")) {
            auto * newAddInData = make_AddInData_t(this);
            newAddInData->read(reader);
            AddInData.append(newAddInData);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int Project_AddInData_t::tableColumnCount() const
{
    return 1;
}

QVariant Project_AddInData_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "Project_AddInData";
        }
        break;
    }
    return QVariant();
}

QVariant Project_AddInData_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant Project_AddInData_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "AddInData";
    case Qt::DecorationRole:
        return QIcon::fromTheme("Project_AddInData");
    }
    return QVariant();
}

Project_AddInData_t * make_Project_AddInData_t(Base * parent)
{
    return new Project_AddInData_t(parent);
}

} // namespace knx
} // namespace v10
} // namespace Project
