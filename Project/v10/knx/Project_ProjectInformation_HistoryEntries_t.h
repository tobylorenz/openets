/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v10/knx/Project_ProjectInformation_HistoryEntries_HistoryEntry_t.h>

namespace Project {
namespace v10 {
namespace knx {

class Project_ProjectInformation_HistoryEntries_t : public Base
{
    Q_OBJECT

public:
    explicit Project_ProjectInformation_HistoryEntries_t(Base * parent = nullptr);
    virtual ~Project_ProjectInformation_HistoryEntries_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* elements */
    QVector<Project_ProjectInformation_HistoryEntries_HistoryEntry_t *> HistoryEntry;
};

Project_ProjectInformation_HistoryEntries_t * make_Project_ProjectInformation_HistoryEntries_t(Base * parent);

} // namespace knx
} // namespace v10
} // namespace Project
