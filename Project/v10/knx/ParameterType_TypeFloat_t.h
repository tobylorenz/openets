/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v10/knx/ParameterType_TypeFloat_Encoding_t.h>
#include <Project/v10/knx/ParameterType_TypeFloat_UIHint_t.h>
#include <Project/xs/Double.h>
#include <Project/xs/String.h>

namespace Project {
namespace v10 {
namespace knx {

class ParameterType_TypeFloat_t : public Base
{
    Q_OBJECT

public:
    explicit ParameterType_TypeFloat_t(Base * parent = nullptr);
    virtual ~ParameterType_TypeFloat_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    ParameterType_TypeFloat_Encoding_t Encoding{};
    xs::Double minInclusive{};
    xs::Double maxInclusive{};
    ParameterType_TypeFloat_UIHint_t UIHint{};
    xs::String DisplayFormat{};
};

ParameterType_TypeFloat_t * make_ParameterType_TypeFloat_t(Base * parent);

} // namespace knx
} // namespace v10
} // namespace Project
