/* This file is generated. */

#pragma once

#include <Project/xs/UnsignedInt.h>

namespace Project {
namespace v10 {
namespace knx {

using ApplicationProgramStatic_Parameters_Union_Memory_Offset_t = xs::UnsignedInt;

} // namespace knx
} // namespace v10
} // namespace Project
