/* This file is generated. */

#pragma once

#include <Project/v10/knx/LdCtrlProcType_t.h>

namespace Project {
namespace v10 {
namespace knx {

using HawkConfigurationData_Procedures_Procedure_ProcedureSubType_t = LdCtrlProcType_t;

} // namespace knx
} // namespace v10
} // namespace Project
