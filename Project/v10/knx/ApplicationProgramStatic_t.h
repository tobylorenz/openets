/* This file is generated. */

#pragma once

#include <Project/Base.h>

namespace Project {
namespace v10 {
namespace knx {

/* forward declarations */
class ApplicationProgramStatic_AddressTable_t;
class ApplicationProgramStatic_AssociationTable_t;
class ApplicationProgramStatic_BinaryData_t;
class ApplicationProgramStatic_Code_t;
class ApplicationProgramStatic_ComObjectRefs_t;
class ApplicationProgramStatic_ComObjectTable_t;
class ApplicationProgramStatic_DeviceCompare_t;
class ApplicationProgramStatic_Extension_t;
class ApplicationProgramStatic_FixupList_t;
class ApplicationProgramStatic_Options_t;
class ApplicationProgramStatic_ParameterCalculations_t;
class ApplicationProgramStatic_ParameterRefs_t;
class ApplicationProgramStatic_ParameterTypes_t;
class ApplicationProgramStatic_Parameters_t;
class LoadProcedures_t;

class ApplicationProgramStatic_t : public Base
{
    Q_OBJECT

public:
    explicit ApplicationProgramStatic_t(Base * parent = nullptr);
    virtual ~ApplicationProgramStatic_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* elements */
    ApplicationProgramStatic_Code_t * Code{};
    ApplicationProgramStatic_ParameterTypes_t * ParameterTypes{};
    ApplicationProgramStatic_Parameters_t * Parameters{};
    ApplicationProgramStatic_ParameterRefs_t * ParameterRefs{};
    ApplicationProgramStatic_ParameterCalculations_t * ParameterCalculations{};
    ApplicationProgramStatic_ComObjectTable_t * ComObjectTable{};
    ApplicationProgramStatic_ComObjectRefs_t * ComObjectRefs{};
    ApplicationProgramStatic_AddressTable_t * AddressTable{};
    ApplicationProgramStatic_AssociationTable_t * AssociationTable{};
    ApplicationProgramStatic_FixupList_t * FixupList{};
    LoadProcedures_t * LoadProcedures{};
    ApplicationProgramStatic_Extension_t * Extension{};
    ApplicationProgramStatic_BinaryData_t * BinaryData{};
    ApplicationProgramStatic_DeviceCompare_t * DeviceCompare{};
    ApplicationProgramStatic_Options_t * Options{};
};

ApplicationProgramStatic_t * make_ApplicationProgramStatic_t(Base * parent);

} // namespace knx
} // namespace v10
} // namespace Project
