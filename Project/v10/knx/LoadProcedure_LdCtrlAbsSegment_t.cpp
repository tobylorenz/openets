/* This file is generated. */

#include <Project/v10/knx/LoadProcedure_LdCtrlAbsSegment_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v10/knx/KNX_t.h>

namespace Project {
namespace v10 {
namespace knx {

LoadProcedure_LdCtrlAbsSegment_t::LoadProcedure_LdCtrlAbsSegment_t(Base * parent) :
    Base(parent)
{
}

LoadProcedure_LdCtrlAbsSegment_t::~LoadProcedure_LdCtrlAbsSegment_t()
{
}

void LoadProcedure_LdCtrlAbsSegment_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("LsmIdx")) {
            LsmIdx = attribute.value().toString();
            continue;
        }
        if (name == QString("ObjType")) {
            ObjType = attribute.value().toString();
            continue;
        }
        if (name == QString("Occurrence")) {
            Occurrence = attribute.value().toString();
            continue;
        }
        if (name == QString("SegType")) {
            SegType = attribute.value().toString();
            continue;
        }
        if (name == QString("Address")) {
            Address = attribute.value().toString();
            continue;
        }
        if (name == QString("Size")) {
            Size = attribute.value().toString();
            continue;
        }
        if (name == QString("Access")) {
            Access = attribute.value().toString();
            continue;
        }
        if (name == QString("MemType")) {
            MemType = attribute.value().toString();
            continue;
        }
        if (name == QString("SegFlags")) {
            SegFlags = attribute.value().toString();
            continue;
        }
        if (name == QString("AppliesTo")) {
            AppliesTo = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int LoadProcedure_LdCtrlAbsSegment_t::tableColumnCount() const
{
    return 11;
}

QVariant LoadProcedure_LdCtrlAbsSegment_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "LoadProcedure_LdCtrlAbsSegment";
        }
        if (qualifiedName == QString("LsmIdx")) {
            return LsmIdx;
        }
        if (qualifiedName == QString("ObjType")) {
            return ObjType;
        }
        if (qualifiedName == QString("Occurrence")) {
            return Occurrence;
        }
        if (qualifiedName == QString("SegType")) {
            return SegType;
        }
        if (qualifiedName == QString("Address")) {
            return Address;
        }
        if (qualifiedName == QString("Size")) {
            return Size;
        }
        if (qualifiedName == QString("Access")) {
            return Access;
        }
        if (qualifiedName == QString("MemType")) {
            return MemType;
        }
        if (qualifiedName == QString("SegFlags")) {
            return SegFlags;
        }
        if (qualifiedName == QString("AppliesTo")) {
            return AppliesTo;
        }
        break;
    }
    return QVariant();
}

QVariant LoadProcedure_LdCtrlAbsSegment_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "LsmIdx";
            case 2:
                return "ObjType";
            case 3:
                return "Occurrence";
            case 4:
                return "SegType";
            case 5:
                return "Address";
            case 6:
                return "Size";
            case 7:
                return "Access";
            case 8:
                return "MemType";
            case 9:
                return "SegFlags";
            case 10:
                return "AppliesTo";
            }
        }
    }
    return QVariant();
}

QVariant LoadProcedure_LdCtrlAbsSegment_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "LdCtrlAbsSegment";
    case Qt::DecorationRole:
        return QIcon::fromTheme("LoadProcedure_LdCtrlAbsSegment");
    }
    return QVariant();
}

LoadProcedure_LdCtrlAbsSegment_t * make_LoadProcedure_LdCtrlAbsSegment_t(Base * parent)
{
    return new LoadProcedure_LdCtrlAbsSegment_t(parent);
}

} // namespace knx
} // namespace v10
} // namespace Project
