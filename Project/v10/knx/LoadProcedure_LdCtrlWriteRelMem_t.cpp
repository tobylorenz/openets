/* This file is generated. */

#include <Project/v10/knx/LoadProcedure_LdCtrlWriteRelMem_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v10/knx/KNX_t.h>

namespace Project {
namespace v10 {
namespace knx {

LoadProcedure_LdCtrlWriteRelMem_t::LoadProcedure_LdCtrlWriteRelMem_t(Base * parent) :
    Base(parent)
{
}

LoadProcedure_LdCtrlWriteRelMem_t::~LoadProcedure_LdCtrlWriteRelMem_t()
{
}

void LoadProcedure_LdCtrlWriteRelMem_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("ObjIdx")) {
            ObjIdx = attribute.value().toString();
            continue;
        }
        if (name == QString("Offset")) {
            Offset = attribute.value().toString();
            continue;
        }
        if (name == QString("Size")) {
            Size = attribute.value().toString();
            continue;
        }
        if (name == QString("Verify")) {
            Verify = attribute.value().toString();
            continue;
        }
        if (name == QString("InlineData")) {
            InlineData = attribute.value().toString();
            continue;
        }
        if (name == QString("AppliesTo")) {
            AppliesTo = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int LoadProcedure_LdCtrlWriteRelMem_t::tableColumnCount() const
{
    return 7;
}

QVariant LoadProcedure_LdCtrlWriteRelMem_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "LoadProcedure_LdCtrlWriteRelMem";
        }
        if (qualifiedName == QString("ObjIdx")) {
            return ObjIdx;
        }
        if (qualifiedName == QString("Offset")) {
            return Offset;
        }
        if (qualifiedName == QString("Size")) {
            return Size;
        }
        if (qualifiedName == QString("Verify")) {
            return Verify;
        }
        if (qualifiedName == QString("InlineData")) {
            return InlineData;
        }
        if (qualifiedName == QString("AppliesTo")) {
            return AppliesTo;
        }
        break;
    }
    return QVariant();
}

QVariant LoadProcedure_LdCtrlWriteRelMem_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "ObjIdx";
            case 2:
                return "Offset";
            case 3:
                return "Size";
            case 4:
                return "Verify";
            case 5:
                return "InlineData";
            case 6:
                return "AppliesTo";
            }
        }
    }
    return QVariant();
}

QVariant LoadProcedure_LdCtrlWriteRelMem_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "LdCtrlWriteRelMem";
    case Qt::DecorationRole:
        return QIcon::fromTheme("LoadProcedure_LdCtrlWriteRelMem");
    }
    return QVariant();
}

LoadProcedure_LdCtrlWriteRelMem_t * make_LoadProcedure_LdCtrlWriteRelMem_t(Base * parent)
{
    return new LoadProcedure_LdCtrlWriteRelMem_t(parent);
}

} // namespace knx
} // namespace v10
} // namespace Project
