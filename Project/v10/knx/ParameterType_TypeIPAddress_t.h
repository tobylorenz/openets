/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v10/knx/ParameterType_TypeIPAddress_AddressType_t.h>
#include <Project/v10/knx/ParameterType_TypeIPAddress_Version_t.h>

namespace Project {
namespace v10 {
namespace knx {

class ParameterType_TypeIPAddress_t : public Base
{
    Q_OBJECT

public:
    explicit ParameterType_TypeIPAddress_t(Base * parent = nullptr);
    virtual ~ParameterType_TypeIPAddress_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    ParameterType_TypeIPAddress_AddressType_t AddressType{};
    ParameterType_TypeIPAddress_Version_t Version{"IPv4"};
};

ParameterType_TypeIPAddress_t * make_ParameterType_TypeIPAddress_t(Base * parent);

} // namespace knx
} // namespace v10
} // namespace Project
