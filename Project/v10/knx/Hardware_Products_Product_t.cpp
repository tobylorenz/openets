/* This file is generated. */

#include <Project/v10/knx/Hardware_Products_Product_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v10/knx/Hardware_Products_Product_Attributes_t.h>
#include <Project/v10/knx/Hardware_Products_Product_Baggages_t.h>
#include <Project/v10/knx/KNX_t.h>
#include <Project/v10/knx/RegistrationInfo_t.h>

namespace Project {
namespace v10 {
namespace knx {

Hardware_Products_Product_t::Hardware_Products_Product_t(Base * parent) :
    Base(parent)
{
}

Hardware_Products_Product_t::~Hardware_Products_Product_t()
{
}

void Hardware_Products_Product_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == "Id") {
            Id = attribute.value().toString();
            KNX_t * knx = findParent<KNX_t *>();
            Q_ASSERT(knx);
            knx->ids[Id] = this;
            continue;
        }
        if (name == QString("Text")) {
            Text = attribute.value().toString();
            continue;
        }
        if (name == QString("OrderNumber")) {
            OrderNumber = attribute.value().toString();
            continue;
        }
        if (name == QString("IsRailMounted")) {
            IsRailMounted = attribute.value().toString();
            continue;
        }
        if (name == QString("WidthInMillimeter")) {
            WidthInMillimeter = attribute.value().toString();
            continue;
        }
        if (name == QString("VisibleDescription")) {
            VisibleDescription = attribute.value().toString();
            continue;
        }
        if (name == QString("DefaultLanguage")) {
            DefaultLanguage = attribute.value().toString();
            continue;
        }
        if (name == QString("NonRegRelevantDataVersion")) {
            NonRegRelevantDataVersion = attribute.value().toString();
            continue;
        }
        if (name == QString("Hash")) {
            Hash = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("Baggages")) {
            if (!Baggages) {
                Baggages = make_Hardware_Products_Product_Baggages_t(this);
            }
            Baggages->read(reader);
            continue;
        }
        if (reader.name() == QString("Attributes")) {
            if (!Attributes) {
                Attributes = make_Hardware_Products_Product_Attributes_t(this);
            }
            Attributes->read(reader);
            continue;
        }
        if (reader.name() == QString("RegistrationInfo")) {
            if (!RegistrationInfo) {
                RegistrationInfo = make_RegistrationInfo_t(this);
            }
            RegistrationInfo->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int Hardware_Products_Product_t::tableColumnCount() const
{
    return 10;
}

QVariant Hardware_Products_Product_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "Hardware_Products_Product";
        }
        if (qualifiedName == QString("Id")) {
            return Id;
        }
        if (qualifiedName == QString("Text")) {
            return Text;
        }
        if (qualifiedName == QString("OrderNumber")) {
            return OrderNumber;
        }
        if (qualifiedName == QString("IsRailMounted")) {
            return IsRailMounted;
        }
        if (qualifiedName == QString("WidthInMillimeter")) {
            return WidthInMillimeter;
        }
        if (qualifiedName == QString("VisibleDescription")) {
            return VisibleDescription;
        }
        if (qualifiedName == QString("DefaultLanguage")) {
            return DefaultLanguage;
        }
        if (qualifiedName == QString("NonRegRelevantDataVersion")) {
            return NonRegRelevantDataVersion;
        }
        if (qualifiedName == QString("Hash")) {
            return Hash;
        }
        break;
    }
    return QVariant();
}

QVariant Hardware_Products_Product_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Id";
            case 2:
                return "Text";
            case 3:
                return "OrderNumber";
            case 4:
                return "IsRailMounted";
            case 5:
                return "WidthInMillimeter";
            case 6:
                return "VisibleDescription";
            case 7:
                return "DefaultLanguage";
            case 8:
                return "NonRegRelevantDataVersion";
            case 9:
                return "Hash";
            }
        }
    }
    return QVariant();
}

QVariant Hardware_Products_Product_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return QString("[%1] %2").arg(OrderNumber).arg(Text);
    case Qt::DecorationRole:
        return QIcon::fromTheme("Hardware_Products_Product");
    }
    return QVariant();
}

Hardware_Products_Product_t * make_Hardware_Products_Product_t(Base * parent)
{
    return new Hardware_Products_Product_t(parent);
}

} // namespace knx
} // namespace v10
} // namespace Project
