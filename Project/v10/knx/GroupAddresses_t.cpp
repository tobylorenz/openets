/* This file is generated. */

#include <Project/v10/knx/GroupAddresses_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v10/knx/GroupAddresses_GroupRanges_t.h>
#include <Project/v10/knx/KNX_t.h>

namespace Project {
namespace v10 {
namespace knx {

GroupAddresses_t::GroupAddresses_t(Base * parent) :
    Base(parent)
{
}

GroupAddresses_t::~GroupAddresses_t()
{
}

void GroupAddresses_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("GroupRanges")) {
            if (!GroupRanges) {
                GroupRanges = make_GroupAddresses_GroupRanges_t(this);
            }
            GroupRanges->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int GroupAddresses_t::tableColumnCount() const
{
    return 1;
}

QVariant GroupAddresses_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "GroupAddresses";
        }
        break;
    }
    return QVariant();
}

QVariant GroupAddresses_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant GroupAddresses_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "GroupAddresses";
    case Qt::DecorationRole:
        return QIcon::fromTheme("GroupAddresses");
    }
    return QVariant();
}

GroupAddresses_t * make_GroupAddresses_t(Base * parent)
{
    return new GroupAddresses_t(parent);
}

} // namespace knx
} // namespace v10
} // namespace Project
