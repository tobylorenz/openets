/* This file is generated. */

#include <Project/v10/knx/LoadProcedure_LdCtrlLoadImageProp_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v10/knx/KNX_t.h>

namespace Project {
namespace v10 {
namespace knx {

LoadProcedure_LdCtrlLoadImageProp_t::LoadProcedure_LdCtrlLoadImageProp_t(Base * parent) :
    Base(parent)
{
}

LoadProcedure_LdCtrlLoadImageProp_t::~LoadProcedure_LdCtrlLoadImageProp_t()
{
}

void LoadProcedure_LdCtrlLoadImageProp_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("ObjIdx")) {
            ObjIdx = attribute.value().toString();
            continue;
        }
        if (name == QString("ObjType")) {
            ObjType = attribute.value().toString();
            continue;
        }
        if (name == QString("Occurrence")) {
            Occurrence = attribute.value().toString();
            continue;
        }
        if (name == QString("PropId")) {
            PropId = attribute.value().toString();
            continue;
        }
        if (name == QString("Count")) {
            Count = attribute.value().toString();
            continue;
        }
        if (name == QString("StartElement")) {
            StartElement = attribute.value().toString();
            continue;
        }
        if (name == QString("AppliesTo")) {
            AppliesTo = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int LoadProcedure_LdCtrlLoadImageProp_t::tableColumnCount() const
{
    return 8;
}

QVariant LoadProcedure_LdCtrlLoadImageProp_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "LoadProcedure_LdCtrlLoadImageProp";
        }
        if (qualifiedName == QString("ObjIdx")) {
            return ObjIdx;
        }
        if (qualifiedName == QString("ObjType")) {
            return ObjType;
        }
        if (qualifiedName == QString("Occurrence")) {
            return Occurrence;
        }
        if (qualifiedName == QString("PropId")) {
            return PropId;
        }
        if (qualifiedName == QString("Count")) {
            return Count;
        }
        if (qualifiedName == QString("StartElement")) {
            return StartElement;
        }
        if (qualifiedName == QString("AppliesTo")) {
            return AppliesTo;
        }
        break;
    }
    return QVariant();
}

QVariant LoadProcedure_LdCtrlLoadImageProp_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "ObjIdx";
            case 2:
                return "ObjType";
            case 3:
                return "Occurrence";
            case 4:
                return "PropId";
            case 5:
                return "Count";
            case 6:
                return "StartElement";
            case 7:
                return "AppliesTo";
            }
        }
    }
    return QVariant();
}

QVariant LoadProcedure_LdCtrlLoadImageProp_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "LdCtrlLoadImageProp";
    case Qt::DecorationRole:
        return QIcon::fromTheme("LoadProcedure_LdCtrlLoadImageProp");
    }
    return QVariant();
}

LoadProcedure_LdCtrlLoadImageProp_t * make_LoadProcedure_LdCtrlLoadImageProp_t(Base * parent)
{
    return new LoadProcedure_LdCtrlLoadImageProp_t(parent);
}

} // namespace knx
} // namespace v10
} // namespace Project
