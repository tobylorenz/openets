/* This file is generated. */

#pragma once

#include <Project/Base.h>

namespace Project {
namespace v10 {
namespace knx {

class ParameterType_TypeNone_t : public Base
{
    Q_OBJECT

public:
    explicit ParameterType_TypeNone_t(Base * parent = nullptr);
    virtual ~ParameterType_TypeNone_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;
};

ParameterType_TypeNone_t * make_ParameterType_TypeNone_t(Base * parent);

} // namespace knx
} // namespace v10
} // namespace Project
