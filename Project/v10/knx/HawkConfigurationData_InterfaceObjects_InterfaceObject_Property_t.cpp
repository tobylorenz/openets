/* This file is generated. */

#include <Project/v10/knx/HawkConfigurationData_InterfaceObjects_InterfaceObject_Property_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v10/knx/KNX_t.h>

namespace Project {
namespace v10 {
namespace knx {

HawkConfigurationData_InterfaceObjects_InterfaceObject_Property_t::HawkConfigurationData_InterfaceObjects_InterfaceObject_Property_t(Base * parent) :
    Base(parent)
{
}

HawkConfigurationData_InterfaceObjects_InterfaceObject_Property_t::~HawkConfigurationData_InterfaceObjects_InterfaceObject_Property_t()
{
}

void HawkConfigurationData_InterfaceObjects_InterfaceObject_Property_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("PropertyID")) {
            PropertyID = attribute.value().toString();
            continue;
        }
        if (name == QString("PropertyDataType")) {
            PropertyDataType = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int HawkConfigurationData_InterfaceObjects_InterfaceObject_Property_t::tableColumnCount() const
{
    return 3;
}

QVariant HawkConfigurationData_InterfaceObjects_InterfaceObject_Property_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "HawkConfigurationData_InterfaceObjects_InterfaceObject_Property";
        }
        if (qualifiedName == QString("PropertyID")) {
            return PropertyID;
        }
        if (qualifiedName == QString("PropertyDataType")) {
            return PropertyDataType;
        }
        break;
    }
    return QVariant();
}

QVariant HawkConfigurationData_InterfaceObjects_InterfaceObject_Property_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "PropertyID";
            case 2:
                return "PropertyDataType";
            }
        }
    }
    return QVariant();
}

QVariant HawkConfigurationData_InterfaceObjects_InterfaceObject_Property_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "Property";
    case Qt::DecorationRole:
        return QIcon::fromTheme("HawkConfigurationData_InterfaceObjects_InterfaceObject_Property");
    }
    return QVariant();
}

HawkConfigurationData_InterfaceObjects_InterfaceObject_Property_t * make_HawkConfigurationData_InterfaceObjects_InterfaceObject_Property_t(Base * parent)
{
    return new HawkConfigurationData_InterfaceObjects_InterfaceObject_Property_t(parent);
}

} // namespace knx
} // namespace v10
} // namespace Project
