/* This file is generated. */

#include <Project/v10/knx/MasterData_DatapointTypes_DatapointType_DatapointSubtypes_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v10/knx/KNX_t.h>

namespace Project {
namespace v10 {
namespace knx {

MasterData_DatapointTypes_DatapointType_DatapointSubtypes_t::MasterData_DatapointTypes_DatapointType_DatapointSubtypes_t(Base * parent) :
    Base(parent)
{
}

MasterData_DatapointTypes_DatapointType_DatapointSubtypes_t::~MasterData_DatapointTypes_DatapointType_DatapointSubtypes_t()
{
}

void MasterData_DatapointTypes_DatapointType_DatapointSubtypes_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("DatapointSubtype")) {
            QString newId = reader.attributes().value("Id").toString();
            Q_ASSERT(!newId.isEmpty());
            MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_t * newDatapointSubtype;
            if (DatapointSubtype.contains(newId)) {
                newDatapointSubtype = DatapointSubtype[newId];
            } else {
                newDatapointSubtype = make_MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_t(this);
                DatapointSubtype[newId] = newDatapointSubtype;
            }
            newDatapointSubtype->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int MasterData_DatapointTypes_DatapointType_DatapointSubtypes_t::tableColumnCount() const
{
    return 1;
}

QVariant MasterData_DatapointTypes_DatapointType_DatapointSubtypes_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "MasterData_DatapointTypes_DatapointType_DatapointSubtypes";
        }
        break;
    }
    return QVariant();
}

QVariant MasterData_DatapointTypes_DatapointType_DatapointSubtypes_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant MasterData_DatapointTypes_DatapointType_DatapointSubtypes_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "DatapointSubtypes";
    case Qt::DecorationRole:
        return QIcon::fromTheme("MasterData_DatapointTypes_DatapointType_DatapointSubtypes");
    }
    return QVariant();
}

MasterData_DatapointTypes_DatapointType_DatapointSubtypes_t * make_MasterData_DatapointTypes_DatapointType_DatapointSubtypes_t(Base * parent)
{
    return new MasterData_DatapointTypes_DatapointType_DatapointSubtypes_t(parent);
}

} // namespace knx
} // namespace v10
} // namespace Project
