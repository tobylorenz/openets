/* This file is generated. */

#include <Project/v10/knx/LoadProcedure_LdCtrlLoadCompleted_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v10/knx/KNX_t.h>

namespace Project {
namespace v10 {
namespace knx {

LoadProcedure_LdCtrlLoadCompleted_t::LoadProcedure_LdCtrlLoadCompleted_t(Base * parent) :
    Base(parent)
{
}

LoadProcedure_LdCtrlLoadCompleted_t::~LoadProcedure_LdCtrlLoadCompleted_t()
{
}

void LoadProcedure_LdCtrlLoadCompleted_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("LsmIdx")) {
            LsmIdx = attribute.value().toString();
            continue;
        }
        if (name == QString("ObjType")) {
            ObjType = attribute.value().toString();
            continue;
        }
        if (name == QString("Occurrence")) {
            Occurrence = attribute.value().toString();
            continue;
        }
        if (name == QString("AppliesTo")) {
            AppliesTo = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int LoadProcedure_LdCtrlLoadCompleted_t::tableColumnCount() const
{
    return 5;
}

QVariant LoadProcedure_LdCtrlLoadCompleted_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "LoadProcedure_LdCtrlLoadCompleted";
        }
        if (qualifiedName == QString("LsmIdx")) {
            return LsmIdx;
        }
        if (qualifiedName == QString("ObjType")) {
            return ObjType;
        }
        if (qualifiedName == QString("Occurrence")) {
            return Occurrence;
        }
        if (qualifiedName == QString("AppliesTo")) {
            return AppliesTo;
        }
        break;
    }
    return QVariant();
}

QVariant LoadProcedure_LdCtrlLoadCompleted_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "LsmIdx";
            case 2:
                return "ObjType";
            case 3:
                return "Occurrence";
            case 4:
                return "AppliesTo";
            }
        }
    }
    return QVariant();
}

QVariant LoadProcedure_LdCtrlLoadCompleted_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "LdCtrlLoadCompleted";
    case Qt::DecorationRole:
        return QIcon::fromTheme("LoadProcedure_LdCtrlLoadCompleted");
    }
    return QVariant();
}

LoadProcedure_LdCtrlLoadCompleted_t * make_LoadProcedure_LdCtrlLoadCompleted_t(Base * parent)
{
    return new LoadProcedure_LdCtrlLoadCompleted_t(parent);
}

} // namespace knx
} // namespace v10
} // namespace Project
