/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v10/knx/ComObjectPriority_t.h>
#include <Project/v10/knx/ComObjectSize_t.h>
#include <Project/v10/knx/Enable_t.h>
#include <Project/v10/knx/IDREF.h>
#include <Project/v10/knx/IDREFS.h>
#include <Project/v10/knx/LanguageDependentString255_t.h>
#include <Project/v10/knx/LanguageDependentString_t.h>
#include <Project/v10/knx/MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_t.h>
#include <Project/v10/knx/String50_t.h>
#include <Project/xs/ID.h>

namespace Project {
namespace v10 {
namespace knx {

/* forward declarations */
class ComObject_t;

class ComObjectRef_t : public Base
{
    Q_OBJECT

public:
    explicit ComObjectRef_t(Base * parent = nullptr);
    virtual ~ComObjectRef_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::ID Id{};
    IDREF RefId{};
    String50_t Name{};
    LanguageDependentString255_t Text{};
    String50_t Tag{};
    LanguageDependentString255_t FunctionText{};
    LanguageDependentString_t VisibleDescription{};
    ComObjectPriority_t Priority{};
    ComObjectSize_t ObjectSize{};
    Enable_t ReadFlag{};
    Enable_t WriteFlag{};
    Enable_t CommunicationFlag{};
    Enable_t TransmitFlag{};
    Enable_t UpdateFlag{};
    Enable_t ReadOnInitFlag{};
    IDREFS DatapointType{};

    /* getters */
    ComObject_t * getComObject() const; // attribute: RefId
    QList<MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_t *> getDatapointType() const; // attribute: DatapointType
};

ComObjectRef_t * make_ComObjectRef_t(Base * parent);

} // namespace knx
} // namespace v10
} // namespace Project
