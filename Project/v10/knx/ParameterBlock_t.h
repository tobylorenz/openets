/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v10/knx/Access_t.h>
#include <Project/v10/knx/LanguageDependentString255_t.h>
#include <Project/v10/knx/String50_t.h>
#include <Project/xs/ID.h>
#include <Project/xs/String.h>
#include <Project/xs/UnsignedInt.h>

namespace Project {
namespace v10 {
namespace knx {

/* forward declarations */
class Assign_t;
class BinaryDataRef_t;
class ParameterChoose_t;
class ParameterRefRef_t;
class ParameterSeparator_t;

class ParameterBlock_t : public Base
{
    Q_OBJECT

public:
    explicit ParameterBlock_t(Base * parent = nullptr);
    virtual ~ParameterBlock_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::ID Id{};
    String50_t Name{};
    LanguageDependentString255_t Text{};
    Access_t Access{"ReadWrite"};
    xs::UnsignedInt HelpTopic{};
    xs::String InternalDescription{};

    /* elements */
    // xs:choice ParameterSeparator_t * ParameterSeparator{};
    // xs:choice ParameterRefRef_t * ParameterRefRef{};
    // xs:choice ParameterChoose_t * Choose{};
    // xs:choice BinaryDataRef_t * BinaryDataRef{};
    // xs:choice Assign_t * Assign{};
};

ParameterBlock_t * make_ParameterBlock_t(Base * parent);

} // namespace knx
} // namespace v10
} // namespace Project
