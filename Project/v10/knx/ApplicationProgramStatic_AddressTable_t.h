/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v10/knx/ApplicationProgramStatic_AddressTable_Offset_t.h>
#include <Project/v10/knx/IDREF.h>
#include <Project/xs/UnsignedInt.h>

namespace Project {
namespace v10 {
namespace knx {

/* forward declarations */
class ApplicationProgramStatic_Code_AbsoluteSegment_t;

class ApplicationProgramStatic_AddressTable_t : public Base
{
    Q_OBJECT

public:
    explicit ApplicationProgramStatic_AddressTable_t(Base * parent = nullptr);
    virtual ~ApplicationProgramStatic_AddressTable_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    IDREF CodeSegment{};
    ApplicationProgramStatic_AddressTable_Offset_t Offset{};
    xs::UnsignedInt MaxEntries{};

    /* getters */
    ApplicationProgramStatic_Code_AbsoluteSegment_t * getCodeSegment() const; // attribute: CodeSegment
};

ApplicationProgramStatic_AddressTable_t * make_ApplicationProgramStatic_AddressTable_t(Base * parent);

} // namespace knx
} // namespace v10
} // namespace Project
