/* This file is generated. */

#include <Project/v10/knx/Hardware_Products_Product_Attributes_Attribute_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v10/knx/KNX_t.h>

namespace Project {
namespace v10 {
namespace knx {

Hardware_Products_Product_Attributes_Attribute_t::Hardware_Products_Product_Attributes_Attribute_t(Base * parent) :
    Base(parent)
{
}

Hardware_Products_Product_Attributes_Attribute_t::~Hardware_Products_Product_Attributes_Attribute_t()
{
}

void Hardware_Products_Product_Attributes_Attribute_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == "Id") {
            Id = attribute.value().toString();
            KNX_t * knx = findParent<KNX_t *>();
            Q_ASSERT(knx);
            knx->ids[Id] = this;
            continue;
        }
        if (name == QString("Name")) {
            Name = attribute.value().toString();
            continue;
        }
        if (name == QString("Value")) {
            Value = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int Hardware_Products_Product_Attributes_Attribute_t::tableColumnCount() const
{
    return 4;
}

QVariant Hardware_Products_Product_Attributes_Attribute_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "Hardware_Products_Product_Attributes_Attribute";
        }
        if (qualifiedName == QString("Id")) {
            return Id;
        }
        if (qualifiedName == QString("Name")) {
            return Name;
        }
        if (qualifiedName == QString("Value")) {
            return Value;
        }
        break;
    }
    return QVariant();
}

QVariant Hardware_Products_Product_Attributes_Attribute_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Id";
            case 2:
                return "Name";
            case 3:
                return "Value";
            }
        }
    }
    return QVariant();
}

QVariant Hardware_Products_Product_Attributes_Attribute_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return !Name.isEmpty() ? Name : "Attribute";
    case Qt::DecorationRole:
        return QIcon::fromTheme("Hardware_Products_Product_Attributes_Attribute");
    }
    return QVariant();
}

Hardware_Products_Product_Attributes_Attribute_t * make_Hardware_Products_Product_Attributes_Attribute_t(Base * parent)
{
    return new Hardware_Products_Product_Attributes_Attribute_t(parent);
}

} // namespace knx
} // namespace v10
} // namespace Project
