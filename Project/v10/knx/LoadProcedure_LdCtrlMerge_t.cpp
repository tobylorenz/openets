/* This file is generated. */

#include <Project/v10/knx/LoadProcedure_LdCtrlMerge_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v10/knx/KNX_t.h>

namespace Project {
namespace v10 {
namespace knx {

LoadProcedure_LdCtrlMerge_t::LoadProcedure_LdCtrlMerge_t(Base * parent) :
    Base(parent)
{
}

LoadProcedure_LdCtrlMerge_t::~LoadProcedure_LdCtrlMerge_t()
{
}

void LoadProcedure_LdCtrlMerge_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("MergeId")) {
            MergeId = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int LoadProcedure_LdCtrlMerge_t::tableColumnCount() const
{
    return 2;
}

QVariant LoadProcedure_LdCtrlMerge_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "LoadProcedure_LdCtrlMerge";
        }
        if (qualifiedName == QString("MergeId")) {
            return MergeId;
        }
        break;
    }
    return QVariant();
}

QVariant LoadProcedure_LdCtrlMerge_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "MergeId";
            }
        }
    }
    return QVariant();
}

QVariant LoadProcedure_LdCtrlMerge_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "LdCtrlMerge";
    case Qt::DecorationRole:
        return QIcon::fromTheme("LoadProcedure_LdCtrlMerge");
    }
    return QVariant();
}

LoadProcedure_LdCtrlMerge_t * make_LoadProcedure_LdCtrlMerge_t(Base * parent)
{
    return new LoadProcedure_LdCtrlMerge_t(parent);
}

} // namespace knx
} // namespace v10
} // namespace Project
