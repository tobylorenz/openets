/* This file is generated. */

#include <Project/v10/knx/DeviceInstance_ParameterInstanceRefs_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v10/knx/KNX_t.h>

namespace Project {
namespace v10 {
namespace knx {

DeviceInstance_ParameterInstanceRefs_t::DeviceInstance_ParameterInstanceRefs_t(Base * parent) :
    Base(parent)
{
}

DeviceInstance_ParameterInstanceRefs_t::~DeviceInstance_ParameterInstanceRefs_t()
{
}

void DeviceInstance_ParameterInstanceRefs_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("ParameterInstanceRef")) {
            QString newRefId = reader.attributes().value("RefId").toString();
            Q_ASSERT(!newRefId.isEmpty());
            ParameterInstanceRef_t * newParameterInstanceRef;
            if (ParameterInstanceRef.contains(newRefId)) {
                newParameterInstanceRef = ParameterInstanceRef[newRefId];
            } else {
                newParameterInstanceRef = make_ParameterInstanceRef_t(this);
                ParameterInstanceRef[newRefId] = newParameterInstanceRef;
            }
            newParameterInstanceRef->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int DeviceInstance_ParameterInstanceRefs_t::tableColumnCount() const
{
    return 1;
}

QVariant DeviceInstance_ParameterInstanceRefs_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "DeviceInstance_ParameterInstanceRefs";
        }
        break;
    }
    return QVariant();
}

QVariant DeviceInstance_ParameterInstanceRefs_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant DeviceInstance_ParameterInstanceRefs_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "ParameterInstanceRefs";
    case Qt::DecorationRole:
        return QIcon::fromTheme("DeviceInstance_ParameterInstanceRefs");
    }
    return QVariant();
}

DeviceInstance_ParameterInstanceRefs_t * make_DeviceInstance_ParameterInstanceRefs_t(Base * parent)
{
    return new DeviceInstance_ParameterInstanceRefs_t(parent);
}

} // namespace knx
} // namespace v10
} // namespace Project
