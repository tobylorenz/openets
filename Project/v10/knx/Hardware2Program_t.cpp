/* This file is generated. */

#include <Project/v10/knx/Hardware2Program_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v10/knx/KNX_t.h>
#include <Project/v10/knx/RegistrationInfo_t.h>

namespace Project {
namespace v10 {
namespace knx {

Hardware2Program_t::Hardware2Program_t(Base * parent) :
    Base(parent)
{
}

Hardware2Program_t::~Hardware2Program_t()
{
}

void Hardware2Program_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == "Id") {
            Id = attribute.value().toString();
            KNX_t * knx = findParent<KNX_t *>();
            Q_ASSERT(knx);
            knx->ids[Id] = this;
            continue;
        }
        if (name == QString("MediumTypes")) {
            MediumTypes = attribute.value().toString().split(' ');
            continue;
        }
        if (name == QString("Hash")) {
            Hash = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("ApplicationProgramRef")) {
            QString newRefId = reader.attributes().value("RefId").toString();
            Q_ASSERT(!newRefId.isEmpty());
            ApplicationProgramRef_t * newApplicationProgramRef;
            if (ApplicationProgramRef.contains(newRefId)) {
                newApplicationProgramRef = ApplicationProgramRef[newRefId];
            } else {
                newApplicationProgramRef = make_ApplicationProgramRef_t(this);
                ApplicationProgramRef[newRefId] = newApplicationProgramRef;
            }
            newApplicationProgramRef->read(reader);
            continue;
        }
        if (reader.name() == QString("RegistrationInfo")) {
            if (!RegistrationInfo) {
                RegistrationInfo = make_RegistrationInfo_t(this);
            }
            RegistrationInfo->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int Hardware2Program_t::tableColumnCount() const
{
    return 4;
}

QVariant Hardware2Program_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "Hardware2Program";
        }
        if (qualifiedName == QString("Id")) {
            return Id;
        }
        if (qualifiedName == QString("MediumTypes")) {
            return MediumTypes.join(' ');
        }
        if (qualifiedName == QString("Hash")) {
            return Hash;
        }
        break;
    }
    return QVariant();
}

QVariant Hardware2Program_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Id";
            case 2:
                return "MediumTypes";
            case 3:
                return "Hash";
            }
        }
    }
    return QVariant();
}

QVariant Hardware2Program_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
    {
        int applicationProgramCount = Id.count("-O");
        switch (applicationProgramCount) {
        case 0:
            return "-";
        case 1:
            return "ApplicationProgram"; // @todo is the name of the program(s) meant here?
        case 2:
            return "ApplicationProgram / ApplicationProgram2";
        }
    }
    case Qt::DecorationRole:
        return QIcon::fromTheme("Hardware2Program");
    }
    return QVariant();
}

QList<MasterData_MediumTypes_MediumType_t *> Hardware2Program_t::getMediumTypes() const
{
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    QList<MasterData_MediumTypes_MediumType_t *> retVal;
    for (QString id : MediumTypes) {
        retVal << qobject_cast<MasterData_MediumTypes_MediumType_t *>(knx->ids[id]);
    }
    return retVal;
}

Hardware2Program_t * make_Hardware2Program_t(Base * parent)
{
    return new Hardware2Program_t(parent);
}

} // namespace knx
} // namespace v10
} // namespace Project
