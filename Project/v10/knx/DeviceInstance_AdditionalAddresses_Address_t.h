/* This file is generated. */

#pragma once

#include <Project/xs/UnsignedByte.h>

namespace Project {
namespace v10 {
namespace knx {

using DeviceInstance_AdditionalAddresses_Address_t = xs::UnsignedByte;

} // namespace knx
} // namespace v10
} // namespace Project
