/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v10/knx/LdCtrlProcType_t.h>
#include <Project/xs/UnsignedByte.h>
#include <Project/xs/UnsignedInt.h>
#include <Project/xs/UnsignedShort.h>

namespace Project {
namespace v10 {
namespace knx {

class LoadProcedure_LdCtrlMaxLength_t : public Base
{
    Q_OBJECT

public:
    explicit LoadProcedure_LdCtrlMaxLength_t(Base * parent = nullptr);
    virtual ~LoadProcedure_LdCtrlMaxLength_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::UnsignedByte LsmIdx{};
    xs::UnsignedShort ObjType{};
    xs::UnsignedByte Occurrence{"0"};
    LdCtrlProcType_t AppliesTo{"auto"};
    xs::UnsignedInt Size{};
};

LoadProcedure_LdCtrlMaxLength_t * make_LoadProcedure_LdCtrlMaxLength_t(Base * parent);

} // namespace knx
} // namespace v10
} // namespace Project
