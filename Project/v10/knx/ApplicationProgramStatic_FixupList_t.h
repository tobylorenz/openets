/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v10/knx/Fixup_t.h>

namespace Project {
namespace v10 {
namespace knx {

class ApplicationProgramStatic_FixupList_t : public Base
{
    Q_OBJECT

public:
    explicit ApplicationProgramStatic_FixupList_t(Base * parent = nullptr);
    virtual ~ApplicationProgramStatic_FixupList_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* elements */
    QMap<IDREF, Fixup_t *> Fixup; // key: FunctionRef
};

ApplicationProgramStatic_FixupList_t * make_ApplicationProgramStatic_FixupList_t(Base * parent);

} // namespace knx
} // namespace v10
} // namespace Project
