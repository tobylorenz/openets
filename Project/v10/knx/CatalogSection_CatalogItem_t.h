/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v10/knx/IDREF.h>
#include <Project/v10/knx/IDREFS.h>
#include <Project/v10/knx/LanguageDependentString255_t.h>
#include <Project/v10/knx/LanguageDependentString_t.h>
#include <Project/xs/ID.h>
#include <Project/xs/Int.h>
#include <Project/xs/Language.h>
#include <Project/xs/UnsignedShort.h>

namespace Project {
namespace v10 {
namespace knx {

/* forward declarations */
class Hardware2Program_t;
class Hardware_Products_Product_t;

class CatalogSection_CatalogItem_t : public Base
{
    Q_OBJECT

public:
    explicit CatalogSection_CatalogItem_t(Base * parent = nullptr);
    virtual ~CatalogSection_CatalogItem_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::ID Id{};
    LanguageDependentString255_t Name{};
    xs::Int Number{};
    LanguageDependentString_t VisibleDescription{};
    IDREF ProductRefId{};
    IDREF Hardware2ProgramRefId{};
    xs::Language DefaultLanguage{};
    IDREFS MediumTypes{};
    xs::UnsignedShort NonRegRelevantDataVersion{"0"};

    /* getters */
    Hardware_Products_Product_t * getProduct() const; // attribute: ProductRefId
    Hardware2Program_t * getHardware2Program() const; // attribute: Hardware2ProgramRefId
};

CatalogSection_CatalogItem_t * make_CatalogSection_CatalogItem_t(Base * parent);

} // namespace knx
} // namespace v10
} // namespace Project
