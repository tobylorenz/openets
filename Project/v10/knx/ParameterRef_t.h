/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v10/knx/Access_t.h>
#include <Project/v10/knx/IDREF.h>
#include <Project/v10/knx/LanguageDependentString255_t.h>
#include <Project/v10/knx/String50_t.h>
#include <Project/v10/knx/Value_t.h>
#include <Project/xs/ID.h>
#include <Project/xs/Int.h>

namespace Project {
namespace v10 {
namespace knx {

class ParameterRef_t : public Base
{
    Q_OBJECT

public:
    explicit ParameterRef_t(Base * parent = nullptr);
    virtual ~ParameterRef_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::ID Id{};
    IDREF RefId{};
    String50_t Name{};
    LanguageDependentString255_t Text{};
    String50_t Tag{};
    xs::Int DisplayOrder{};
    Access_t Access{};
    Value_t Value{};

    /* getters */
    Base * getParameter() const; // attribute: RefId
};

ParameterRef_t * make_ParameterRef_t(Base * parent);

} // namespace knx
} // namespace v10
} // namespace Project
