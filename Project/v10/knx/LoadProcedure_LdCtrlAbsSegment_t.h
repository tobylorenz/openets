/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v10/knx/LdCtrlProcType_t.h>
#include <Project/xs/UnsignedByte.h>
#include <Project/xs/UnsignedShort.h>

namespace Project {
namespace v10 {
namespace knx {

class LoadProcedure_LdCtrlAbsSegment_t : public Base
{
    Q_OBJECT

public:
    explicit LoadProcedure_LdCtrlAbsSegment_t(Base * parent = nullptr);
    virtual ~LoadProcedure_LdCtrlAbsSegment_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::UnsignedByte LsmIdx{};
    xs::UnsignedShort ObjType{};
    xs::UnsignedByte Occurrence{"0"};
    xs::UnsignedByte SegType{};
    xs::UnsignedShort Address{};
    xs::UnsignedShort Size{};
    xs::UnsignedByte Access{};
    xs::UnsignedByte MemType{};
    xs::UnsignedByte SegFlags{};
    LdCtrlProcType_t AppliesTo{"auto"};
};

LoadProcedure_LdCtrlAbsSegment_t * make_LoadProcedure_LdCtrlAbsSegment_t(Base * parent);

} // namespace knx
} // namespace v10
} // namespace Project
