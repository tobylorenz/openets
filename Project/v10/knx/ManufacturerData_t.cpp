/* This file is generated. */

#include <Project/v10/knx/ManufacturerData_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v10/knx/KNX_t.h>

namespace Project {
namespace v10 {
namespace knx {

ManufacturerData_t::ManufacturerData_t(Base * parent) :
    Base(parent)
{
}

ManufacturerData_t::~ManufacturerData_t()
{
}

void ManufacturerData_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("Manufacturer")) {
            QString newRefId = reader.attributes().value("RefId").toString();
            Q_ASSERT(!newRefId.isEmpty());
            ManufacturerData_Manufacturer_t * newManufacturer;
            if (Manufacturer.contains(newRefId)) {
                newManufacturer = Manufacturer[newRefId];
            } else {
                newManufacturer = make_ManufacturerData_Manufacturer_t(this);
                Manufacturer[newRefId] = newManufacturer;
            }
            newManufacturer->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ManufacturerData_t::tableColumnCount() const
{
    return 1;
}

QVariant ManufacturerData_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ManufacturerData";
        }
        break;
    }
    return QVariant();
}

QVariant ManufacturerData_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant ManufacturerData_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "ManufacturerData";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ManufacturerData");
    }
    return QVariant();
}

ManufacturerData_t * make_ManufacturerData_t(Base * parent)
{
    return new ManufacturerData_t(parent);
}

} // namespace knx
} // namespace v10
} // namespace Project
