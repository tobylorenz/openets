/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v10/knx/IDREF.h>
#include <Project/v10/knx/ParameterCalculation_RParameters_ParameterRefRef_t.h>

namespace Project {
namespace v10 {
namespace knx {

class ParameterCalculation_RParameters_t : public Base
{
    Q_OBJECT

public:
    explicit ParameterCalculation_RParameters_t(Base * parent = nullptr);
    virtual ~ParameterCalculation_RParameters_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* elements */
    QMap<IDREF, ParameterCalculation_RParameters_ParameterRefRef_t *> ParameterRefRef; // key: RefId
};

ParameterCalculation_RParameters_t * make_ParameterCalculation_RParameters_t(Base * parent);

} // namespace knx
} // namespace v10
} // namespace Project
