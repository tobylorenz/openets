/* This file is generated. */

#pragma once

#include <Project/Base.h>

namespace Project {
namespace v10 {
namespace knx {

/* forward declarations */
class ApplicationProgramStatic_Parameters_Union_t;
class Parameter_t;

class ApplicationProgramStatic_Parameters_t : public Base
{
    Q_OBJECT

public:
    explicit ApplicationProgramStatic_Parameters_t(Base * parent = nullptr);
    virtual ~ApplicationProgramStatic_Parameters_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* elements */
    // xs:choice Parameter_t * Parameter{};
    // xs:choice ApplicationProgramStatic_Parameters_Union_t * Union{};
};

ApplicationProgramStatic_Parameters_t * make_ApplicationProgramStatic_Parameters_t(Base * parent);

} // namespace knx
} // namespace v10
} // namespace Project
