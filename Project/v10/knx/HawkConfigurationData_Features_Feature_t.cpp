/* This file is generated. */

#include <Project/v10/knx/HawkConfigurationData_Features_Feature_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v10/knx/KNX_t.h>

namespace Project {
namespace v10 {
namespace knx {

HawkConfigurationData_Features_Feature_t::HawkConfigurationData_Features_Feature_t(Base * parent) :
    Base(parent)
{
}

HawkConfigurationData_Features_Feature_t::~HawkConfigurationData_Features_Feature_t()
{
}

void HawkConfigurationData_Features_Feature_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("Name")) {
            Name = attribute.value().toString();
            continue;
        }
        if (name == QString("Value")) {
            Value = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int HawkConfigurationData_Features_Feature_t::tableColumnCount() const
{
    return 3;
}

QVariant HawkConfigurationData_Features_Feature_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "HawkConfigurationData_Features_Feature";
        }
        if (qualifiedName == QString("Name")) {
            return Name;
        }
        if (qualifiedName == QString("Value")) {
            return Value;
        }
        break;
    }
    return QVariant();
}

QVariant HawkConfigurationData_Features_Feature_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Name";
            case 2:
                return "Value";
            }
        }
    }
    return QVariant();
}

QVariant HawkConfigurationData_Features_Feature_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return !Name.isEmpty() ? Name : "Feature";
    case Qt::DecorationRole:
        return QIcon::fromTheme("HawkConfigurationData_Features_Feature");
    }
    return QVariant();
}

HawkConfigurationData_Features_Feature_t * make_HawkConfigurationData_Features_Feature_t(Base * parent)
{
    return new HawkConfigurationData_Features_Feature_t(parent);
}

} // namespace knx
} // namespace v10
} // namespace Project
