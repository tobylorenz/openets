/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v10/knx/AccessLevel_t.h>
#include <Project/v10/knx/LdCtrlProcType_t.h>
#include <Project/v10/knx/LoadProcedure_LdCtrlDeclarePropDesc_MaxElements_t.h>
#include <Project/v10/knx/PropType_t.h>
#include <Project/xs/Boolean.h>
#include <Project/xs/UnsignedByte.h>
#include <Project/xs/UnsignedShort.h>

namespace Project {
namespace v10 {
namespace knx {

class LoadProcedure_LdCtrlDeclarePropDesc_t : public Base
{
    Q_OBJECT

public:
    explicit LoadProcedure_LdCtrlDeclarePropDesc_t(Base * parent = nullptr);
    virtual ~LoadProcedure_LdCtrlDeclarePropDesc_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::UnsignedByte ObjIdx{};
    xs::UnsignedShort ObjType{};
    xs::UnsignedByte Occurrence{"0"};
    xs::UnsignedByte PropId{};
    PropType_t PropType{};
    LoadProcedure_LdCtrlDeclarePropDesc_MaxElements_t MaxElements{};
    AccessLevel_t ReadAccess{};
    AccessLevel_t WriteAccess{};
    xs::Boolean Writable{};
    LdCtrlProcType_t AppliesTo{"auto"};
};

LoadProcedure_LdCtrlDeclarePropDesc_t * make_LoadProcedure_LdCtrlDeclarePropDesc_t(Base * parent);

} // namespace knx
} // namespace v10
} // namespace Project
