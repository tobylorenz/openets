/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v10/knx/LdCtrlProcType_t.h>
#include <Project/v10/knx/LoadProcedure_LdCtrlWriteProp_Count_t.h>
#include <Project/v10/knx/LoadProcedure_LdCtrlWriteProp_StartElement_t.h>
#include <Project/xs/Boolean.h>
#include <Project/xs/HexBinary.h>
#include <Project/xs/UnsignedByte.h>
#include <Project/xs/UnsignedShort.h>

namespace Project {
namespace v10 {
namespace knx {

class LoadProcedure_LdCtrlWriteProp_t : public Base
{
    Q_OBJECT

public:
    explicit LoadProcedure_LdCtrlWriteProp_t(Base * parent = nullptr);
    virtual ~LoadProcedure_LdCtrlWriteProp_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::UnsignedByte ObjIdx{};
    xs::UnsignedShort ObjType{};
    xs::UnsignedByte Occurrence{"0"};
    xs::UnsignedByte PropId{};
    LoadProcedure_LdCtrlWriteProp_StartElement_t StartElement{"1"};
    LoadProcedure_LdCtrlWriteProp_Count_t Count{"1"};
    xs::Boolean Verify{};
    xs::HexBinary InlineData{};
    LdCtrlProcType_t AppliesTo{"auto"};
};

LoadProcedure_LdCtrlWriteProp_t * make_LoadProcedure_LdCtrlWriteProp_t(Base * parent);

} // namespace knx
} // namespace v10
} // namespace Project
