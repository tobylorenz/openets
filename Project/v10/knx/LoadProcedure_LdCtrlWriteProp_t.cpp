/* This file is generated. */

#include <Project/v10/knx/LoadProcedure_LdCtrlWriteProp_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v10/knx/KNX_t.h>

namespace Project {
namespace v10 {
namespace knx {

LoadProcedure_LdCtrlWriteProp_t::LoadProcedure_LdCtrlWriteProp_t(Base * parent) :
    Base(parent)
{
}

LoadProcedure_LdCtrlWriteProp_t::~LoadProcedure_LdCtrlWriteProp_t()
{
}

void LoadProcedure_LdCtrlWriteProp_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("ObjIdx")) {
            ObjIdx = attribute.value().toString();
            continue;
        }
        if (name == QString("ObjType")) {
            ObjType = attribute.value().toString();
            continue;
        }
        if (name == QString("Occurrence")) {
            Occurrence = attribute.value().toString();
            continue;
        }
        if (name == QString("PropId")) {
            PropId = attribute.value().toString();
            continue;
        }
        if (name == QString("StartElement")) {
            StartElement = attribute.value().toString();
            continue;
        }
        if (name == QString("Count")) {
            Count = attribute.value().toString();
            continue;
        }
        if (name == QString("Verify")) {
            Verify = attribute.value().toString();
            continue;
        }
        if (name == QString("InlineData")) {
            InlineData = attribute.value().toString();
            continue;
        }
        if (name == QString("AppliesTo")) {
            AppliesTo = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int LoadProcedure_LdCtrlWriteProp_t::tableColumnCount() const
{
    return 10;
}

QVariant LoadProcedure_LdCtrlWriteProp_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "LoadProcedure_LdCtrlWriteProp";
        }
        if (qualifiedName == QString("ObjIdx")) {
            return ObjIdx;
        }
        if (qualifiedName == QString("ObjType")) {
            return ObjType;
        }
        if (qualifiedName == QString("Occurrence")) {
            return Occurrence;
        }
        if (qualifiedName == QString("PropId")) {
            return PropId;
        }
        if (qualifiedName == QString("StartElement")) {
            return StartElement;
        }
        if (qualifiedName == QString("Count")) {
            return Count;
        }
        if (qualifiedName == QString("Verify")) {
            return Verify;
        }
        if (qualifiedName == QString("InlineData")) {
            return InlineData;
        }
        if (qualifiedName == QString("AppliesTo")) {
            return AppliesTo;
        }
        break;
    }
    return QVariant();
}

QVariant LoadProcedure_LdCtrlWriteProp_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "ObjIdx";
            case 2:
                return "ObjType";
            case 3:
                return "Occurrence";
            case 4:
                return "PropId";
            case 5:
                return "StartElement";
            case 6:
                return "Count";
            case 7:
                return "Verify";
            case 8:
                return "InlineData";
            case 9:
                return "AppliesTo";
            }
        }
    }
    return QVariant();
}

QVariant LoadProcedure_LdCtrlWriteProp_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "LdCtrlWriteProp";
    case Qt::DecorationRole:
        return QIcon::fromTheme("LoadProcedure_LdCtrlWriteProp");
    }
    return QVariant();
}

LoadProcedure_LdCtrlWriteProp_t * make_LoadProcedure_LdCtrlWriteProp_t(Base * parent)
{
    return new LoadProcedure_LdCtrlWriteProp_t(parent);
}

} // namespace knx
} // namespace v10
} // namespace Project
