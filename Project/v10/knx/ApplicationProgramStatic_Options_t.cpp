/* This file is generated. */

#include <Project/v10/knx/ApplicationProgramStatic_Options_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v10/knx/KNX_t.h>

namespace Project {
namespace v10 {
namespace knx {

ApplicationProgramStatic_Options_t::ApplicationProgramStatic_Options_t(Base * parent) :
    Base(parent)
{
}

ApplicationProgramStatic_Options_t::~ApplicationProgramStatic_Options_t()
{
}

void ApplicationProgramStatic_Options_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("PreferPartialDownloadIfApplicationLoaded")) {
            PreferPartialDownloadIfApplicationLoaded = attribute.value().toString();
            continue;
        }
        if (name == QString("EasyCtrlModeModeStyleEmptyGroupComTables")) {
            EasyCtrlModeModeStyleEmptyGroupComTables = attribute.value().toString();
            continue;
        }
        if (name == QString("SetObjectTableLengthAlwaysToOne")) {
            SetObjectTableLengthAlwaysToOne = attribute.value().toString();
            continue;
        }
        if (name == QString("TextParameterEncoding")) {
            TextParameterEncoding = attribute.value().toString();
            continue;
        }
        if (name == QString("TextParameterZeroTerminate")) {
            TextParameterZeroTerminate = attribute.value().toString();
            continue;
        }
        if (name == QString("ParameterByteOrder")) {
            ParameterByteOrder = attribute.value().toString();
            continue;
        }
        if (name == QString("PartialDownloadOnlyVisibleParameters")) {
            PartialDownloadOnlyVisibleParameters = attribute.value().toString();
            continue;
        }
        if (name == QString("LegacyNoPartialDownload")) {
            LegacyNoPartialDownload = attribute.value().toString();
            continue;
        }
        if (name == QString("LegacyNoMemoryVerifyMode")) {
            LegacyNoMemoryVerifyMode = attribute.value().toString();
            continue;
        }
        if (name == QString("LegacyNoOptimisticWrite")) {
            LegacyNoOptimisticWrite = attribute.value().toString();
            continue;
        }
        if (name == QString("LegacyDoNotReportPropertyWriteErrors")) {
            LegacyDoNotReportPropertyWriteErrors = attribute.value().toString();
            continue;
        }
        if (name == QString("LegacyNoBackgroundDownload")) {
            LegacyNoBackgroundDownload = attribute.value().toString();
            continue;
        }
        if (name == QString("LegacyDoNotCheckManufacturerId")) {
            LegacyDoNotCheckManufacturerId = attribute.value().toString();
            continue;
        }
        if (name == QString("LegacyAlwaysReloadAppIfCoVisibilityChanged")) {
            LegacyAlwaysReloadAppIfCoVisibilityChanged = attribute.value().toString();
            continue;
        }
        if (name == QString("LegacyNeverReloadAppIfCoVisibilityChanged")) {
            LegacyNeverReloadAppIfCoVisibilityChanged = attribute.value().toString();
            continue;
        }
        if (name == QString("LegacyDoNotSupportUndoDelete")) {
            LegacyDoNotSupportUndoDelete = attribute.value().toString();
            continue;
        }
        if (name == QString("LegacyAllowPartialDownloadIfAp2Mismatch")) {
            LegacyAllowPartialDownloadIfAp2Mismatch = attribute.value().toString();
            continue;
        }
        if (name == QString("LegacyKeepObjectTableGaps")) {
            LegacyKeepObjectTableGaps = attribute.value().toString();
            continue;
        }
        if (name == QString("LegacyProxyCommunicationObjects")) {
            LegacyProxyCommunicationObjects = attribute.value().toString();
            continue;
        }
        if (name == QString("DeviceInfoIgnoreRunState")) {
            DeviceInfoIgnoreRunState = attribute.value().toString();
            continue;
        }
        if (name == QString("DeviceInfoIgnoreLoadedState")) {
            DeviceInfoIgnoreLoadedState = attribute.value().toString();
            continue;
        }
        if (name == QString("DeviceCompareAllowCompatibleManufacturerId")) {
            DeviceCompareAllowCompatibleManufacturerId = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ApplicationProgramStatic_Options_t::tableColumnCount() const
{
    return 23;
}

QVariant ApplicationProgramStatic_Options_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ApplicationProgramStatic_Options";
        }
        if (qualifiedName == QString("PreferPartialDownloadIfApplicationLoaded")) {
            return PreferPartialDownloadIfApplicationLoaded;
        }
        if (qualifiedName == QString("EasyCtrlModeModeStyleEmptyGroupComTables")) {
            return EasyCtrlModeModeStyleEmptyGroupComTables;
        }
        if (qualifiedName == QString("SetObjectTableLengthAlwaysToOne")) {
            return SetObjectTableLengthAlwaysToOne;
        }
        if (qualifiedName == QString("TextParameterEncoding")) {
            return TextParameterEncoding;
        }
        if (qualifiedName == QString("TextParameterZeroTerminate")) {
            return TextParameterZeroTerminate;
        }
        if (qualifiedName == QString("ParameterByteOrder")) {
            return ParameterByteOrder;
        }
        if (qualifiedName == QString("PartialDownloadOnlyVisibleParameters")) {
            return PartialDownloadOnlyVisibleParameters;
        }
        if (qualifiedName == QString("LegacyNoPartialDownload")) {
            return LegacyNoPartialDownload;
        }
        if (qualifiedName == QString("LegacyNoMemoryVerifyMode")) {
            return LegacyNoMemoryVerifyMode;
        }
        if (qualifiedName == QString("LegacyNoOptimisticWrite")) {
            return LegacyNoOptimisticWrite;
        }
        if (qualifiedName == QString("LegacyDoNotReportPropertyWriteErrors")) {
            return LegacyDoNotReportPropertyWriteErrors;
        }
        if (qualifiedName == QString("LegacyNoBackgroundDownload")) {
            return LegacyNoBackgroundDownload;
        }
        if (qualifiedName == QString("LegacyDoNotCheckManufacturerId")) {
            return LegacyDoNotCheckManufacturerId;
        }
        if (qualifiedName == QString("LegacyAlwaysReloadAppIfCoVisibilityChanged")) {
            return LegacyAlwaysReloadAppIfCoVisibilityChanged;
        }
        if (qualifiedName == QString("LegacyNeverReloadAppIfCoVisibilityChanged")) {
            return LegacyNeverReloadAppIfCoVisibilityChanged;
        }
        if (qualifiedName == QString("LegacyDoNotSupportUndoDelete")) {
            return LegacyDoNotSupportUndoDelete;
        }
        if (qualifiedName == QString("LegacyAllowPartialDownloadIfAp2Mismatch")) {
            return LegacyAllowPartialDownloadIfAp2Mismatch;
        }
        if (qualifiedName == QString("LegacyKeepObjectTableGaps")) {
            return LegacyKeepObjectTableGaps;
        }
        if (qualifiedName == QString("LegacyProxyCommunicationObjects")) {
            return LegacyProxyCommunicationObjects;
        }
        if (qualifiedName == QString("DeviceInfoIgnoreRunState")) {
            return DeviceInfoIgnoreRunState;
        }
        if (qualifiedName == QString("DeviceInfoIgnoreLoadedState")) {
            return DeviceInfoIgnoreLoadedState;
        }
        if (qualifiedName == QString("DeviceCompareAllowCompatibleManufacturerId")) {
            return DeviceCompareAllowCompatibleManufacturerId;
        }
        break;
    }
    return QVariant();
}

QVariant ApplicationProgramStatic_Options_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "PreferPartialDownloadIfApplicationLoaded";
            case 2:
                return "EasyCtrlModeModeStyleEmptyGroupComTables";
            case 3:
                return "SetObjectTableLengthAlwaysToOne";
            case 4:
                return "TextParameterEncoding";
            case 5:
                return "TextParameterZeroTerminate";
            case 6:
                return "ParameterByteOrder";
            case 7:
                return "PartialDownloadOnlyVisibleParameters";
            case 8:
                return "LegacyNoPartialDownload";
            case 9:
                return "LegacyNoMemoryVerifyMode";
            case 10:
                return "LegacyNoOptimisticWrite";
            case 11:
                return "LegacyDoNotReportPropertyWriteErrors";
            case 12:
                return "LegacyNoBackgroundDownload";
            case 13:
                return "LegacyDoNotCheckManufacturerId";
            case 14:
                return "LegacyAlwaysReloadAppIfCoVisibilityChanged";
            case 15:
                return "LegacyNeverReloadAppIfCoVisibilityChanged";
            case 16:
                return "LegacyDoNotSupportUndoDelete";
            case 17:
                return "LegacyAllowPartialDownloadIfAp2Mismatch";
            case 18:
                return "LegacyKeepObjectTableGaps";
            case 19:
                return "LegacyProxyCommunicationObjects";
            case 20:
                return "DeviceInfoIgnoreRunState";
            case 21:
                return "DeviceInfoIgnoreLoadedState";
            case 22:
                return "DeviceCompareAllowCompatibleManufacturerId";
            }
        }
    }
    return QVariant();
}

QVariant ApplicationProgramStatic_Options_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "Options";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ApplicationProgramStatic_Options");
    }
    return QVariant();
}

ApplicationProgramStatic_Options_t * make_ApplicationProgramStatic_Options_t(Base * parent)
{
    return new ApplicationProgramStatic_Options_t(parent);
}

} // namespace knx
} // namespace v10
} // namespace Project
