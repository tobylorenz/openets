/* This file is generated. */

#include <Project/v10/knx/Parameter_Property_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v10/knx/KNX_t.h>

namespace Project {
namespace v10 {
namespace knx {

Parameter_Property_t::Parameter_Property_t(Base * parent) :
    Base(parent)
{
}

Parameter_Property_t::~Parameter_Property_t()
{
}

void Parameter_Property_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("ObjectIndex")) {
            ObjectIndex = attribute.value().toString();
            continue;
        }
        if (name == QString("ObjectType")) {
            ObjectType = attribute.value().toString();
            continue;
        }
        if (name == QString("Occurrence")) {
            Occurrence = attribute.value().toString();
            continue;
        }
        if (name == QString("PropertyId")) {
            PropertyId = attribute.value().toString();
            continue;
        }
        if (name == QString("Offset")) {
            Offset = attribute.value().toString();
            continue;
        }
        if (name == QString("BitOffset")) {
            BitOffset = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int Parameter_Property_t::tableColumnCount() const
{
    return 7;
}

QVariant Parameter_Property_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "Parameter_Property";
        }
        if (qualifiedName == QString("ObjectIndex")) {
            return ObjectIndex;
        }
        if (qualifiedName == QString("ObjectType")) {
            return ObjectType;
        }
        if (qualifiedName == QString("Occurrence")) {
            return Occurrence;
        }
        if (qualifiedName == QString("PropertyId")) {
            return PropertyId;
        }
        if (qualifiedName == QString("Offset")) {
            return Offset;
        }
        if (qualifiedName == QString("BitOffset")) {
            return BitOffset;
        }
        break;
    }
    return QVariant();
}

QVariant Parameter_Property_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "ObjectIndex";
            case 2:
                return "ObjectType";
            case 3:
                return "Occurrence";
            case 4:
                return "PropertyId";
            case 5:
                return "Offset";
            case 6:
                return "BitOffset";
            }
        }
    }
    return QVariant();
}

QVariant Parameter_Property_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "Property";
    case Qt::DecorationRole:
        return QIcon::fromTheme("Parameter_Property");
    }
    return QVariant();
}

Parameter_Property_t * make_Parameter_Property_t(Base * parent)
{
    return new Parameter_Property_t(parent);
}

} // namespace knx
} // namespace v10
} // namespace Project
