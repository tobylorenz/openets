/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v10/knx/ApplicationProgramStatic_Code_AbsoluteSegment_t.h>
#include <Project/v10/knx/ApplicationProgramStatic_Code_RelativeSegment_t.h>
#include <Project/xs/ID.h>

namespace Project {
namespace v10 {
namespace knx {

class ApplicationProgramStatic_Code_t : public Base
{
    Q_OBJECT

public:
    explicit ApplicationProgramStatic_Code_t(Base * parent = nullptr);
    virtual ~ApplicationProgramStatic_Code_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* elements */
    QMap<xs::ID, ApplicationProgramStatic_Code_AbsoluteSegment_t *> AbsoluteSegment; // key: Id
    QMap<xs::ID, ApplicationProgramStatic_Code_RelativeSegment_t *> RelativeSegment; // key: Id
};

ApplicationProgramStatic_Code_t * make_ApplicationProgramStatic_Code_t(Base * parent);

} // namespace knx
} // namespace v10
} // namespace Project
