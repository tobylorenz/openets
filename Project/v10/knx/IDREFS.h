/* This file is generated. */

#pragma once

#include <Project/v10/knx/IDREF.h>
#include <Project/xs/List.h>

namespace Project {
namespace v10 {
namespace knx {

using IDREFS = xs::List<IDREF>;

} // namespace knx
} // namespace v10
} // namespace Project
