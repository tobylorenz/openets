/* This file is generated. */

#include <Project/v10/knx/ApplicationProgramStatic_DeviceCompare_ExcludeProperty_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v10/knx/KNX_t.h>

namespace Project {
namespace v10 {
namespace knx {

ApplicationProgramStatic_DeviceCompare_ExcludeProperty_t::ApplicationProgramStatic_DeviceCompare_ExcludeProperty_t(Base * parent) :
    Base(parent)
{
}

ApplicationProgramStatic_DeviceCompare_ExcludeProperty_t::~ApplicationProgramStatic_DeviceCompare_ExcludeProperty_t()
{
}

void ApplicationProgramStatic_DeviceCompare_ExcludeProperty_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("ObjectIndex")) {
            ObjectIndex = attribute.value().toString();
            continue;
        }
        if (name == QString("ObjectType")) {
            ObjectType = attribute.value().toString();
            continue;
        }
        if (name == QString("Occurrence")) {
            Occurrence = attribute.value().toString();
            continue;
        }
        if (name == QString("PropertyId")) {
            PropertyId = attribute.value().toString();
            continue;
        }
        if (name == QString("Offset")) {
            Offset = attribute.value().toString();
            continue;
        }
        if (name == QString("Size")) {
            Size = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ApplicationProgramStatic_DeviceCompare_ExcludeProperty_t::tableColumnCount() const
{
    return 7;
}

QVariant ApplicationProgramStatic_DeviceCompare_ExcludeProperty_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ApplicationProgramStatic_DeviceCompare_ExcludeProperty";
        }
        if (qualifiedName == QString("ObjectIndex")) {
            return ObjectIndex;
        }
        if (qualifiedName == QString("ObjectType")) {
            return ObjectType;
        }
        if (qualifiedName == QString("Occurrence")) {
            return Occurrence;
        }
        if (qualifiedName == QString("PropertyId")) {
            return PropertyId;
        }
        if (qualifiedName == QString("Offset")) {
            return Offset;
        }
        if (qualifiedName == QString("Size")) {
            return Size;
        }
        break;
    }
    return QVariant();
}

QVariant ApplicationProgramStatic_DeviceCompare_ExcludeProperty_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "ObjectIndex";
            case 2:
                return "ObjectType";
            case 3:
                return "Occurrence";
            case 4:
                return "PropertyId";
            case 5:
                return "Offset";
            case 6:
                return "Size";
            }
        }
    }
    return QVariant();
}

QVariant ApplicationProgramStatic_DeviceCompare_ExcludeProperty_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "ExcludeProperty";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ApplicationProgramStatic_DeviceCompare_ExcludeProperty");
    }
    return QVariant();
}

ApplicationProgramStatic_DeviceCompare_ExcludeProperty_t * make_ApplicationProgramStatic_DeviceCompare_ExcludeProperty_t(Base * parent)
{
    return new ApplicationProgramStatic_DeviceCompare_ExcludeProperty_t(parent);
}

} // namespace knx
} // namespace v10
} // namespace Project
