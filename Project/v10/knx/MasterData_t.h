/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/xs/Base64Binary.h>
#include <Project/xs/UnsignedInt.h>

namespace Project {
namespace v10 {
namespace knx {

/* forward declarations */
class MasterData_DatapointTypes_t;
class MasterData_Languages_t;
class MasterData_Manufacturers_t;
class MasterData_MaskVersions_t;
class MasterData_MediumTypes_t;

class MasterData_t : public Base
{
    Q_OBJECT

public:
    explicit MasterData_t(Base * parent = nullptr);
    virtual ~MasterData_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::UnsignedInt Version{};
    xs::Base64Binary Signature{};

    /* elements */
    MasterData_DatapointTypes_t * DatapointTypes{};
    MasterData_MediumTypes_t * MediumTypes{};
    MasterData_MaskVersions_t * MaskVersions{};
    MasterData_Manufacturers_t * Manufacturers{};
    MasterData_Languages_t * Languages{};
};

MasterData_t * make_MasterData_t(Base * parent);

} // namespace knx
} // namespace v10
} // namespace Project
