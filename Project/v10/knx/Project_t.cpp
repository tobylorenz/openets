/* This file is generated. */

#include <Project/v10/knx/Project_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v10/knx/KNX_t.h>
#include <Project/v10/knx/Project_AddInData_t.h>
#include <Project/v10/knx/Project_Installations_t.h>
#include <Project/v10/knx/Project_ProjectInformation_t.h>
#include <Project/v10/knx/Project_UserFiles_t.h>

namespace Project {
namespace v10 {
namespace knx {

Project_t::Project_t(Base * parent) :
    Base(parent)
{
}

Project_t::~Project_t()
{
}

void Project_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == "Id") {
            Id = attribute.value().toString();
            KNX_t * knx = findParent<KNX_t *>();
            Q_ASSERT(knx);
            knx->ids[Id] = this;
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("ProjectInformation")) {
            if (!ProjectInformation) {
                ProjectInformation = make_Project_ProjectInformation_t(this);
            }
            ProjectInformation->read(reader);
            continue;
        }
        if (reader.name() == QString("Installations")) {
            if (!Installations) {
                Installations = make_Project_Installations_t(this);
            }
            Installations->read(reader);
            continue;
        }
        if (reader.name() == QString("UserFiles")) {
            if (!UserFiles) {
                UserFiles = make_Project_UserFiles_t(this);
            }
            UserFiles->read(reader);
            continue;
        }
        if (reader.name() == QString("AddInData")) {
            if (!AddInData) {
                AddInData = make_Project_AddInData_t(this);
            }
            AddInData->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int Project_t::tableColumnCount() const
{
    return 2;
}

QVariant Project_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "Project";
        }
        if (qualifiedName == QString("Id")) {
            return Id;
        }
        break;
    }
    return QVariant();
}

QVariant Project_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Id";
            }
        }
    }
    return QVariant();
}

QVariant Project_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "Project";
    case Qt::DecorationRole:
        return QIcon::fromTheme("Project");
    }
    return QVariant();
}

Project_t * make_Project_t(Base * parent)
{
    return new Project_t(parent);
}

} // namespace knx
} // namespace v10
} // namespace Project
