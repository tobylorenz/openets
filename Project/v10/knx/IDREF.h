/* This file is generated. */

#pragma once

#include <Project/xs/NCName.h>

namespace Project {
namespace v10 {
namespace knx {

using IDREF = xs::NCName;

} // namespace knx
} // namespace v10
} // namespace Project
