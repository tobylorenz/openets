/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v10/knx/Condition_t.h>
#include <Project/xs/Boolean.h>

namespace Project {
namespace v10 {
namespace knx {

/* forward declarations */
class IndependentParameterBlockChoose_t;
class ParameterBlockRename_t;
class ParameterBlock_t;

class IndependentParameterBlockChoose_when_t : public Base
{
    Q_OBJECT

public:
    explicit IndependentParameterBlockChoose_when_t(Base * parent = nullptr);
    virtual ~IndependentParameterBlockChoose_when_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    Condition_t Test{};
    xs::Boolean Default{"false"};

    /* elements */
    // xs:choice ParameterBlock_t * ParameterBlock{};
    // xs:choice IndependentParameterBlockChoose_t * Choose{};
    // xs:choice ParameterBlockRename_t * ParameterBlockRename{};
};

IndependentParameterBlockChoose_when_t * make_IndependentParameterBlockChoose_when_t(Base * parent);

} // namespace knx
} // namespace v10
} // namespace Project
