/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v10/knx/Condition_t.h>
#include <Project/xs/Boolean.h>

namespace Project {
namespace v10 {
namespace knx {

/* forward declarations */
class Assign_t;
class BinaryDataRef_t;
class ParameterBlockRename_t;
class ParameterChoose_t;
class ParameterRefRef_t;
class ParameterSeparator_t;

class ParameterChoose_when_t : public Base
{
    Q_OBJECT

public:
    explicit ParameterChoose_when_t(Base * parent = nullptr);
    virtual ~ParameterChoose_when_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    Condition_t Test{};
    xs::Boolean Default{"false"};

    /* elements */
    // xs:choice ParameterSeparator_t * ParameterSeparator{};
    // xs:choice ParameterRefRef_t * ParameterRefRef{};
    // xs:choice ParameterChoose_t * Choose{};
    // xs:choice BinaryDataRef_t * BinaryDataRef{};
    // xs:choice Assign_t * Assign{};
    // xs:choice ParameterBlockRename_t * ParameterBlockRename{};
};

ParameterChoose_when_t * make_ParameterChoose_when_t(Base * parent);

} // namespace knx
} // namespace v10
} // namespace Project
