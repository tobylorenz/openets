/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v10/knx/LdCtrlProcType_t.h>
#include <Project/xs/UnsignedInt.h>

namespace Project {
namespace v10 {
namespace knx {

class LoadProcedure_LdCtrlProgressText_t : public Base
{
    Q_OBJECT

public:
    explicit LoadProcedure_LdCtrlProgressText_t(Base * parent = nullptr);
    virtual ~LoadProcedure_LdCtrlProgressText_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::UnsignedInt TextId{};
    LdCtrlProcType_t AppliesTo{"auto"};
};

LoadProcedure_LdCtrlProgressText_t * make_LoadProcedure_LdCtrlProgressText_t(Base * parent);

} // namespace knx
} // namespace v10
} // namespace Project
