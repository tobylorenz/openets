/* This file is generated. */

#include <Project/v10/knx/ChannelChoose_when_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v10/knx/BinaryDataRef_t.h>
#include <Project/v10/knx/ChannelChoose_t.h>
#include <Project/v10/knx/ComObjectParameterBlock_t.h>
#include <Project/v10/knx/ComObjectRefRef_t.h>
#include <Project/v10/knx/KNX_t.h>
#include <Project/v10/knx/ParameterBlockRename_t.h>

namespace Project {
namespace v10 {
namespace knx {

ChannelChoose_when_t::ChannelChoose_when_t(Base * parent) :
    Base(parent)
{
}

ChannelChoose_when_t::~ChannelChoose_when_t()
{
}

void ChannelChoose_when_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("test")) {
            Test = attribute.value().toString();
            continue;
        }
        if (name == QString("default")) {
            Default = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("ParameterBlock")) {
            auto * ParameterBlock = make_ComObjectParameterBlock_t(this);
            ParameterBlock->read(reader);
            continue;
        }
        if (reader.name() == QString("ComObjectRefRef")) {
            auto * ComObjectRefRef = make_ComObjectRefRef_t(this);
            ComObjectRefRef->read(reader);
            continue;
        }
        if (reader.name() == QString("BinaryDataRef")) {
            auto * BinaryDataRef = make_BinaryDataRef_t(this);
            BinaryDataRef->read(reader);
            continue;
        }
        if (reader.name() == QString("choose")) {
            auto * Choose = make_ChannelChoose_t(this);
            Choose->read(reader);
            continue;
        }
        if (reader.name() == QString("ParameterBlockRename")) {
            auto * ParameterBlockRename = make_ParameterBlockRename_t(this);
            ParameterBlockRename->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ChannelChoose_when_t::tableColumnCount() const
{
    return 3;
}

QVariant ChannelChoose_when_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ChannelChoose_when";
        }
        if (qualifiedName == QString("test")) {
            return Test;
        }
        if (qualifiedName == QString("default")) {
            return Default;
        }
        break;
    }
    return QVariant();
}

QVariant ChannelChoose_when_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Test";
            case 2:
                return "Default";
            }
        }
    }
    return QVariant();
}

QVariant ChannelChoose_when_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "when";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ChannelChoose_when");
    }
    return QVariant();
}

ChannelChoose_when_t * make_ChannelChoose_when_t(Base * parent)
{
    return new ChannelChoose_when_t(parent);
}

} // namespace knx
} // namespace v10
} // namespace Project
