/* This file is generated. */

#include <Project/v10/knx/KNX_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v10/knx/KNX_t.h>
#include <Project/v10/knx/ManufacturerData_t.h>
#include <Project/v10/knx/MasterData_t.h>

namespace Project {
namespace v10 {
namespace knx {

KNX_t::KNX_t(Base * parent) :
    Base(parent)
{
}

KNX_t::~KNX_t()
{
}

void KNX_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("CreatedBy")) {
            CreatedBy = attribute.value().toString();
            continue;
        }
        if (name == QString("ToolVersion")) {
            ToolVersion = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("MasterData")) {
            if (!MasterData) {
                MasterData = make_MasterData_t(this);
            }
            MasterData->read(reader);
            continue;
        }
        if (reader.name() == QString("ManufacturerData")) {
            if (!ManufacturerData) {
                ManufacturerData = make_ManufacturerData_t(this);
            }
            ManufacturerData->read(reader);
            continue;
        }
        if (reader.name() == QString("Project")) {
            QString newId = reader.attributes().value("Id").toString();
            Q_ASSERT(!newId.isEmpty());
            Project_t * newProject;
            if (Project.contains(newId)) {
                newProject = Project[newId];
            } else {
                newProject = make_Project_t(this);
                Project[newId] = newProject;
            }
            newProject->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int KNX_t::tableColumnCount() const
{
    return 3;
}

QVariant KNX_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "KNX";
        }
        if (qualifiedName == QString("CreatedBy")) {
            return CreatedBy;
        }
        if (qualifiedName == QString("ToolVersion")) {
            return ToolVersion;
        }
        break;
    }
    return QVariant();
}

QVariant KNX_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "CreatedBy";
            case 2:
                return "ToolVersion";
            }
        }
    }
    return QVariant();
}

QVariant KNX_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "KNX";
    case Qt::DecorationRole:
        return QIcon::fromTheme("KNX");
    }
    return QVariant();
}

KNX_t * make_KNX_t(Base * parent)
{
    return new KNX_t(parent);
}

} // namespace knx
} // namespace v10
} // namespace Project
