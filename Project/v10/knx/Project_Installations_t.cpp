/* This file is generated. */

#include <Project/v10/knx/Project_Installations_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v10/knx/KNX_t.h>

namespace Project {
namespace v10 {
namespace knx {

Project_Installations_t::Project_Installations_t(Base * parent) :
    Base(parent)
{
}

Project_Installations_t::~Project_Installations_t()
{
}

void Project_Installations_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("Installation")) {
            auto * newInstallation = make_Project_Installations_Installation_t(this);
            newInstallation->read(reader);
            Installation.append(newInstallation);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int Project_Installations_t::tableColumnCount() const
{
    return 1;
}

QVariant Project_Installations_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "Project_Installations";
        }
        break;
    }
    return QVariant();
}

QVariant Project_Installations_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant Project_Installations_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "Installations";
    case Qt::DecorationRole:
        return QIcon::fromTheme("Project_Installations");
    }
    return QVariant();
}

Project_Installations_t * make_Project_Installations_t(Base * parent)
{
    return new Project_Installations_t(parent);
}

} // namespace knx
} // namespace v10
} // namespace Project
