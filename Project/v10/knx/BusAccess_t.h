/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v10/knx/Guid_t.h>
#include <Project/xs/String.h>

namespace Project {
namespace v10 {
namespace knx {

class BusAccess_t : public Base
{
    Q_OBJECT

public:
    explicit BusAccess_t(Base * parent = nullptr);
    virtual ~BusAccess_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::String Name{};
    Guid_t Edi{};
    xs::String Parameter{};
};

BusAccess_t * make_BusAccess_t(Base * parent);

} // namespace knx
} // namespace v10
} // namespace Project
