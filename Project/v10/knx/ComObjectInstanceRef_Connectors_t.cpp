/* This file is generated. */

#include <Project/v10/knx/ComObjectInstanceRef_Connectors_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v10/knx/ComObjectInstanceRef_Connectors_Send_t.h>
#include <Project/v10/knx/KNX_t.h>

namespace Project {
namespace v10 {
namespace knx {

ComObjectInstanceRef_Connectors_t::ComObjectInstanceRef_Connectors_t(Base * parent) :
    Base(parent)
{
}

ComObjectInstanceRef_Connectors_t::~ComObjectInstanceRef_Connectors_t()
{
}

void ComObjectInstanceRef_Connectors_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("Send")) {
            if (!Send) {
                Send = make_ComObjectInstanceRef_Connectors_Send_t(this);
            }
            Send->read(reader);
            continue;
        }
        if (reader.name() == QString("Receive")) {
            QString newGroupAddressRefId = reader.attributes().value("GroupAddressRefId").toString();
            Q_ASSERT(!newGroupAddressRefId.isEmpty());
            ComObjectInstanceRef_Connectors_Receive_t * newReceive;
            if (Receive.contains(newGroupAddressRefId)) {
                newReceive = Receive[newGroupAddressRefId];
            } else {
                newReceive = make_ComObjectInstanceRef_Connectors_Receive_t(this);
                Receive[newGroupAddressRefId] = newReceive;
            }
            newReceive->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ComObjectInstanceRef_Connectors_t::tableColumnCount() const
{
    return 1;
}

QVariant ComObjectInstanceRef_Connectors_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ComObjectInstanceRef_Connectors";
        }
        break;
    }
    return QVariant();
}

QVariant ComObjectInstanceRef_Connectors_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant ComObjectInstanceRef_Connectors_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "Connectors";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ComObjectInstanceRef_Connectors");
    }
    return QVariant();
}

ComObjectInstanceRef_Connectors_t * make_ComObjectInstanceRef_Connectors_t(Base * parent)
{
    return new ComObjectInstanceRef_Connectors_t(parent);
}

} // namespace knx
} // namespace v10
} // namespace Project
