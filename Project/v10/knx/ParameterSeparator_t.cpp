/* This file is generated. */

#include <Project/v10/knx/ParameterSeparator_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v10/knx/KNX_t.h>

namespace Project {
namespace v10 {
namespace knx {

ParameterSeparator_t::ParameterSeparator_t(Base * parent) :
    Base(parent)
{
}

ParameterSeparator_t::~ParameterSeparator_t()
{
}

void ParameterSeparator_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == "Id") {
            Id = attribute.value().toString();
            KNX_t * knx = findParent<KNX_t *>();
            Q_ASSERT(knx);
            knx->ids[Id] = this;
            continue;
        }
        if (name == QString("Text")) {
            Text = attribute.value().toString();
            continue;
        }
        if (name == QString("Access")) {
            Access = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ParameterSeparator_t::tableColumnCount() const
{
    return 4;
}

QVariant ParameterSeparator_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ParameterSeparator";
        }
        if (qualifiedName == QString("Id")) {
            return Id;
        }
        if (qualifiedName == QString("Text")) {
            return Text;
        }
        if (qualifiedName == QString("Access")) {
            return Access;
        }
        break;
    }
    return QVariant();
}

QVariant ParameterSeparator_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Id";
            case 2:
                return "Text";
            case 3:
                return "Access";
            }
        }
    }
    return QVariant();
}

QVariant ParameterSeparator_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return Text;
    case Qt::DecorationRole:
        return QIcon::fromTheme("ParameterSeparator");
    }
    return QVariant();
}

ParameterSeparator_t * make_ParameterSeparator_t(Base * parent)
{
    return new ParameterSeparator_t(parent);
}

} // namespace knx
} // namespace v10
} // namespace Project
