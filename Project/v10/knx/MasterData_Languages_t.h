/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v10/knx/LanguageData_t.h>

namespace Project {
namespace v10 {
namespace knx {

class MasterData_Languages_t : public Base
{
    Q_OBJECT

public:
    explicit MasterData_Languages_t(Base * parent = nullptr);
    virtual ~MasterData_Languages_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* elements */
    QMap<xs::Language, LanguageData_t *> Language; // key: Identifier
};

MasterData_Languages_t * make_MasterData_Languages_t(Base * parent);

} // namespace knx
} // namespace v10
} // namespace Project
