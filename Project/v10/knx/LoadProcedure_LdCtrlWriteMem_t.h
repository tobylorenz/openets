/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v10/knx/LdCtrlMemAddrSpace_t.h>
#include <Project/v10/knx/LdCtrlProcType_t.h>
#include <Project/xs/Boolean.h>
#include <Project/xs/HexBinary.h>
#include <Project/xs/UnsignedInt.h>

namespace Project {
namespace v10 {
namespace knx {

class LoadProcedure_LdCtrlWriteMem_t : public Base
{
    Q_OBJECT

public:
    explicit LoadProcedure_LdCtrlWriteMem_t(Base * parent = nullptr);
    virtual ~LoadProcedure_LdCtrlWriteMem_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    LdCtrlMemAddrSpace_t AddressSpace{"Standard"};
    xs::UnsignedInt Address{};
    xs::UnsignedInt Size{};
    xs::Boolean Verify{};
    xs::HexBinary InlineData{};
    LdCtrlProcType_t AppliesTo{"auto"};
};

LoadProcedure_LdCtrlWriteMem_t * make_LoadProcedure_LdCtrlWriteMem_t(Base * parent);

} // namespace knx
} // namespace v10
} // namespace Project
