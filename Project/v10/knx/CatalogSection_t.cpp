/* This file is generated. */

#include <Project/v10/knx/CatalogSection_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v10/knx/KNX_t.h>

namespace Project {
namespace v10 {
namespace knx {

CatalogSection_t::CatalogSection_t(Base * parent) :
    Base(parent)
{
}

CatalogSection_t::~CatalogSection_t()
{
}

void CatalogSection_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == "Id") {
            Id = attribute.value().toString();
            KNX_t * knx = findParent<KNX_t *>();
            Q_ASSERT(knx);
            knx->ids[Id] = this;
            continue;
        }
        if (name == QString("Name")) {
            Name = attribute.value().toString();
            continue;
        }
        if (name == QString("Number")) {
            Number = attribute.value().toString();
            continue;
        }
        if (name == QString("VisibleDescription")) {
            VisibleDescription = attribute.value().toString();
            continue;
        }
        if (name == QString("DefaultLanguage")) {
            DefaultLanguage = attribute.value().toString();
            continue;
        }
        if (name == QString("NonRegRelevantDataVersion")) {
            NonRegRelevantDataVersion = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("CatalogSection")) {
            QString newId = reader.attributes().value("Id").toString();
            Q_ASSERT(!newId.isEmpty());
            CatalogSection_t * newCatalogSection;
            if (CatalogSection.contains(newId)) {
                newCatalogSection = CatalogSection[newId];
            } else {
                newCatalogSection = make_CatalogSection_t(this);
                CatalogSection[newId] = newCatalogSection;
            }
            newCatalogSection->read(reader);
            continue;
        }
        if (reader.name() == QString("CatalogItem")) {
            QString newId = reader.attributes().value("Id").toString();
            Q_ASSERT(!newId.isEmpty());
            CatalogSection_CatalogItem_t * newCatalogItem;
            if (CatalogItem.contains(newId)) {
                newCatalogItem = CatalogItem[newId];
            } else {
                newCatalogItem = make_CatalogSection_CatalogItem_t(this);
                CatalogItem[newId] = newCatalogItem;
            }
            newCatalogItem->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int CatalogSection_t::tableColumnCount() const
{
    return 7;
}

QVariant CatalogSection_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "CatalogSection";
        }
        if (qualifiedName == QString("Id")) {
            return Id;
        }
        if (qualifiedName == QString("Name")) {
            return Name;
        }
        if (qualifiedName == QString("Number")) {
            return Number;
        }
        if (qualifiedName == QString("VisibleDescription")) {
            return VisibleDescription;
        }
        if (qualifiedName == QString("DefaultLanguage")) {
            return DefaultLanguage;
        }
        if (qualifiedName == QString("NonRegRelevantDataVersion")) {
            return NonRegRelevantDataVersion;
        }
        break;
    }
    return QVariant();
}

QVariant CatalogSection_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Id";
            case 2:
                return "Name";
            case 3:
                return "Number";
            case 4:
                return "VisibleDescription";
            case 5:
                return "DefaultLanguage";
            case 6:
                return "NonRegRelevantDataVersion";
            }
        }
    }
    return QVariant();
}

QVariant CatalogSection_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return QString("[%1] %2").arg(Number).arg(Name);
    case Qt::DecorationRole:
        return QIcon::fromTheme("CatalogSection");
    }
    return QVariant();
}

CatalogSection_t * make_CatalogSection_t(Base * parent)
{
    return new CatalogSection_t(parent);
}

} // namespace knx
} // namespace v10
} // namespace Project
