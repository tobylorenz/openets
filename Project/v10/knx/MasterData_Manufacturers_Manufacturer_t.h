/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/SimpleElementTextType.h>
#include <Project/v10/knx/String255_t.h>
#include <Project/xs/ID.h>
#include <Project/xs/Language.h>
#include <Project/xs/String.h>
#include <Project/xs/UnsignedShort.h>

namespace Project {
namespace v10 {
namespace knx {

class MasterData_Manufacturers_Manufacturer_t : public Base
{
    Q_OBJECT

public:
    explicit MasterData_Manufacturers_Manufacturer_t(Base * parent = nullptr);
    virtual ~MasterData_Manufacturers_Manufacturer_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::ID Id{};
    String255_t Name{};
    xs::UnsignedShort KnxManufacturerId{};
    xs::Language DefaultLanguage{};
    xs::UnsignedShort CompatibilityGroup{};

    /* elements */
    SimpleElementTextType * OrderNumberFormattingScript{};
};

MasterData_Manufacturers_Manufacturer_t * make_MasterData_Manufacturers_Manufacturer_t(Base * parent);

} // namespace knx
} // namespace v10
} // namespace Project
