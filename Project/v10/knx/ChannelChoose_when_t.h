/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v10/knx/Condition_t.h>
#include <Project/xs/Boolean.h>

namespace Project {
namespace v10 {
namespace knx {

/* forward declarations */
class BinaryDataRef_t;
class ChannelChoose_t;
class ComObjectParameterBlock_t;
class ComObjectRefRef_t;
class ParameterBlockRename_t;

class ChannelChoose_when_t : public Base
{
    Q_OBJECT

public:
    explicit ChannelChoose_when_t(Base * parent = nullptr);
    virtual ~ChannelChoose_when_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    Condition_t Test{};
    xs::Boolean Default{"false"};

    /* elements */
    // xs:choice ComObjectParameterBlock_t * ParameterBlock{};
    // xs:choice ComObjectRefRef_t * ComObjectRefRef{};
    // xs:choice BinaryDataRef_t * BinaryDataRef{};
    // xs:choice ChannelChoose_t * Choose{};
    // xs:choice ParameterBlockRename_t * ParameterBlockRename{};
};

ChannelChoose_when_t * make_ChannelChoose_when_t(Base * parent);

} // namespace knx
} // namespace v10
} // namespace Project
