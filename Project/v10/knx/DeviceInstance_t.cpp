/* This file is generated. */

#include <Project/v10/knx/DeviceInstance_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v10/knx/DeviceInstance_AdditionalAddresses_t.h>
#include <Project/v10/knx/DeviceInstance_BinaryData_t.h>
#include <Project/v10/knx/DeviceInstance_ComObjectInstanceRefs_t.h>
#include <Project/v10/knx/DeviceInstance_ParameterInstanceRefs_t.h>
#include <Project/v10/knx/Hardware2Program_t.h>
#include <Project/v10/knx/Hardware_Products_Product_t.h>
#include <Project/v10/knx/IPConfig_t.h>
#include <Project/v10/knx/KNX_t.h>
#include <Project/v10/knx/MasterData_MediumTypes_MediumType_t.h>
#include <Project/v10/knx/Topology_Area_Line_t.h>
#include <Project/v10/knx/Topology_Area_t.h>

namespace Project {
namespace v10 {
namespace knx {

DeviceInstance_t::DeviceInstance_t(Base * parent) :
    Base(parent)
{
}

DeviceInstance_t::~DeviceInstance_t()
{
}

void DeviceInstance_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("Name")) {
            Name = attribute.value().toString();
            continue;
        }
        if (name == "Id") {
            Id = attribute.value().toString();
            KNX_t * knx = findParent<KNX_t *>();
            Q_ASSERT(knx);
            knx->ids[Id] = this;
            continue;
        }
        if (name == QString("ProductRefId")) {
            ProductRefId = attribute.value().toString();
            continue;
        }
        if (name == QString("Hardware2ProgramRefId")) {
            Hardware2ProgramRefId = attribute.value().toString();
            continue;
        }
        if (name == QString("Address")) {
            Address = attribute.value().toString();
            continue;
        }
        if (name == QString("Comment")) {
            Comment = attribute.value().toString();
            continue;
        }
        if (name == QString("LastModified")) {
            LastModified = attribute.value().toString();
            continue;
        }
        if (name == QString("LastDownload")) {
            LastDownload = attribute.value().toString();
            continue;
        }
        if (name == QString("InstallationHints")) {
            InstallationHints = attribute.value().toString();
            continue;
        }
        if (name == QString("CompletionStatus")) {
            CompletionStatus = attribute.value().toString();
            continue;
        }
        if (name == QString("IndividualAddressLoaded")) {
            IndividualAddressLoaded = attribute.value().toString();
            continue;
        }
        if (name == QString("ApplicationProgramLoaded")) {
            ApplicationProgramLoaded = attribute.value().toString();
            continue;
        }
        if (name == QString("ParametersLoaded")) {
            ParametersLoaded = attribute.value().toString();
            continue;
        }
        if (name == QString("CommunicationPartLoaded")) {
            CommunicationPartLoaded = attribute.value().toString();
            continue;
        }
        if (name == QString("MediumConfigLoaded")) {
            MediumConfigLoaded = attribute.value().toString();
            continue;
        }
        if (name == QString("LoadedImage")) {
            LoadedImage = attribute.value().toString();
            continue;
        }
        if (name == QString("Description")) {
            Description = attribute.value().toString();
            continue;
        }
        if (name == QString("CheckSums")) {
            CheckSums = attribute.value().toString();
            continue;
        }
        if (name == QString("IsCommunicationObjectVisibilityCalculated")) {
            IsCommunicationObjectVisibilityCalculated = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("ParameterInstanceRefs")) {
            if (!ParameterInstanceRefs) {
                ParameterInstanceRefs = make_DeviceInstance_ParameterInstanceRefs_t(this);
            }
            ParameterInstanceRefs->read(reader);
            continue;
        }
        if (reader.name() == QString("ComObjectInstanceRefs")) {
            if (!ComObjectInstanceRefs) {
                ComObjectInstanceRefs = make_DeviceInstance_ComObjectInstanceRefs_t(this);
            }
            ComObjectInstanceRefs->read(reader);
            continue;
        }
        if (reader.name() == QString("AdditionalAddresses")) {
            if (!AdditionalAddresses) {
                AdditionalAddresses = make_DeviceInstance_AdditionalAddresses_t(this);
            }
            AdditionalAddresses->read(reader);
            continue;
        }
        if (reader.name() == QString("BinaryData")) {
            if (!BinaryData) {
                BinaryData = make_DeviceInstance_BinaryData_t(this);
            }
            BinaryData->read(reader);
            continue;
        }
        if (reader.name() == QString("IPConfig")) {
            if (!IPConfig) {
                IPConfig = make_IPConfig_t(this);
            }
            IPConfig->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int DeviceInstance_t::tableColumnCount() const
{
    return 20;
}

QVariant DeviceInstance_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "DeviceInstance";
        }
        if (qualifiedName == QString("Name")) {
            return Name;
        }
        if (qualifiedName == QString("Id")) {
            return Id;
        }
        if (qualifiedName == QString("ProductRefId")) {
            return ProductRefId;
        }
        if (qualifiedName == QString("Hardware2ProgramRefId")) {
            return Hardware2ProgramRefId;
        }
        if (qualifiedName == QString("Address")) {
            return Address;
        }
        if (qualifiedName == QString("Comment")) {
            return Comment;
        }
        if (qualifiedName == QString("LastModified")) {
            return LastModified;
        }
        if (qualifiedName == QString("LastDownload")) {
            return LastDownload;
        }
        if (qualifiedName == QString("InstallationHints")) {
            return InstallationHints;
        }
        if (qualifiedName == QString("CompletionStatus")) {
            return CompletionStatus;
        }
        if (qualifiedName == QString("IndividualAddressLoaded")) {
            return IndividualAddressLoaded;
        }
        if (qualifiedName == QString("ApplicationProgramLoaded")) {
            return ApplicationProgramLoaded;
        }
        if (qualifiedName == QString("ParametersLoaded")) {
            return ParametersLoaded;
        }
        if (qualifiedName == QString("CommunicationPartLoaded")) {
            return CommunicationPartLoaded;
        }
        if (qualifiedName == QString("MediumConfigLoaded")) {
            return MediumConfigLoaded;
        }
        if (qualifiedName == QString("LoadedImage")) {
            return LoadedImage;
        }
        if (qualifiedName == QString("Description")) {
            return Description;
        }
        if (qualifiedName == QString("CheckSums")) {
            return CheckSums;
        }
        if (qualifiedName == QString("IsCommunicationObjectVisibilityCalculated")) {
            return IsCommunicationObjectVisibilityCalculated;
        }
        break;
    }
    return QVariant();
}

QVariant DeviceInstance_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Name";
            case 2:
                return "Id";
            case 3:
                return "ProductRefId";
            case 4:
                return "Hardware2ProgramRefId";
            case 5:
                return "Address";
            case 6:
                return "Comment";
            case 7:
                return "LastModified";
            case 8:
                return "LastDownload";
            case 9:
                return "InstallationHints";
            case 10:
                return "CompletionStatus";
            case 11:
                return "IndividualAddressLoaded";
            case 12:
                return "ApplicationProgramLoaded";
            case 13:
                return "ParametersLoaded";
            case 14:
                return "CommunicationPartLoaded";
            case 15:
                return "MediumConfigLoaded";
            case 16:
                return "LoadedImage";
            case 17:
                return "Description";
            case 18:
                return "CheckSums";
            case 19:
                return "IsCommunicationObjectVisibilityCalculated";
            }
        }
    }
    return QVariant();
}

QVariant DeviceInstance_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
    {
        QString text;
        Topology_Area_Line_t * line = findParent<Topology_Area_Line_t *>();
        if (line) {
            /* assigned device instance */
            Topology_Area_t * area = findParent<Topology_Area_t *>();
            Q_ASSERT(area);
            text = QString("%1.%2.%3").arg(area->Address).arg(line->Address).arg(Address);
        } else {
            /* unassigned device instance */
            text = "DeviceInstance";
        }
        if (!Description.isEmpty()) {
            text += " " + Description.simplified();
        }
        if (!ProductRefId.isEmpty()) {
            Hardware_Products_Product_t * product = getProduct();
            if (product) {
                text += " " + product->Text;
            }
        }
        return text;
    }
    case Qt::DecorationRole:
    {
        Topology_Area_Line_t * line = findParent<Topology_Area_Line_t *>();
        if (line) {
            MasterData_MediumTypes_MediumType_t * mediumType = line->getMediumType();
            Q_ASSERT(mediumType);
            return QIcon::fromTheme("DeviceInstance_" + mediumType->Name);
        } else {
            return QIcon::fromTheme("DeviceInstance");
        }
    }
    }
    return QVariant();
}

Hardware_Products_Product_t * DeviceInstance_t::getProduct() const
{
    if (ProductRefId.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<Hardware_Products_Product_t *>(knx->ids[ProductRefId]);
}

Hardware2Program_t * DeviceInstance_t::getHardware2Program() const
{
    if (Hardware2ProgramRefId.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<Hardware2Program_t *>(knx->ids[Hardware2ProgramRefId]);
}

DeviceInstance_t * make_DeviceInstance_t(Base * parent)
{
    return new DeviceInstance_t(parent);
}

} // namespace knx
} // namespace v10
} // namespace Project
