/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v10/knx/GroupAddress_t.h>
#include <Project/v10/knx/GroupRange_t.h>
#include <Project/v10/knx/String255_t.h>
#include <Project/xs/ID.h>
#include <Project/xs/Boolean.h>
#include <Project/xs/String.h>
#include <Project/xs/UnsignedShort.h>

namespace Project {
namespace v10 {
namespace knx {

class GroupRange_t : public Base
{
    Q_OBJECT

public:
    explicit GroupRange_t(Base * parent = nullptr);
    virtual ~GroupRange_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::ID Id{};
    String255_t Name{};
    xs::UnsignedShort RangeStart{};
    xs::UnsignedShort RangeEnd{};
    xs::Boolean Unfiltered{"false"};
    xs::String Description{};
    xs::String Comment{};

    /* elements */
    QMap<xs::ID, GroupRange_t *> GroupRange; // key: Id
    QMap<xs::ID, GroupAddress_t *> GroupAddress; // key: Id
};

GroupRange_t * make_GroupRange_t(Base * parent);

} // namespace knx
} // namespace v10
} // namespace Project
