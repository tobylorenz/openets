/* This file is generated. */

#pragma once

#include <Project/v10/knx/LanguageDependentString_t.h>

namespace Project {
namespace v10 {
namespace knx {

using LanguageDependentString50_t = LanguageDependentString_t;

} // namespace knx
} // namespace v10
} // namespace Project
