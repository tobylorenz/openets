/* This file is generated. */

#include <Project/v10/knx/ComObjectRef_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v10/knx/ComObject_t.h>
#include <Project/v10/knx/KNX_t.h>

namespace Project {
namespace v10 {
namespace knx {

ComObjectRef_t::ComObjectRef_t(Base * parent) :
    Base(parent)
{
}

ComObjectRef_t::~ComObjectRef_t()
{
}

void ComObjectRef_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == "Id") {
            Id = attribute.value().toString();
            KNX_t * knx = findParent<KNX_t *>();
            Q_ASSERT(knx);
            knx->ids[Id] = this;
            continue;
        }
        if (name == QString("RefId")) {
            RefId = attribute.value().toString();
            continue;
        }
        if (name == QString("Name")) {
            Name = attribute.value().toString();
            continue;
        }
        if (name == QString("Text")) {
            Text = attribute.value().toString();
            continue;
        }
        if (name == QString("Tag")) {
            Tag = attribute.value().toString();
            continue;
        }
        if (name == QString("FunctionText")) {
            FunctionText = attribute.value().toString();
            continue;
        }
        if (name == QString("VisibleDescription")) {
            VisibleDescription = attribute.value().toString();
            continue;
        }
        if (name == QString("Priority")) {
            Priority = attribute.value().toString();
            continue;
        }
        if (name == QString("ObjectSize")) {
            ObjectSize = attribute.value().toString();
            continue;
        }
        if (name == QString("ReadFlag")) {
            ReadFlag = attribute.value().toString();
            continue;
        }
        if (name == QString("WriteFlag")) {
            WriteFlag = attribute.value().toString();
            continue;
        }
        if (name == QString("CommunicationFlag")) {
            CommunicationFlag = attribute.value().toString();
            continue;
        }
        if (name == QString("TransmitFlag")) {
            TransmitFlag = attribute.value().toString();
            continue;
        }
        if (name == QString("UpdateFlag")) {
            UpdateFlag = attribute.value().toString();
            continue;
        }
        if (name == QString("ReadOnInitFlag")) {
            ReadOnInitFlag = attribute.value().toString();
            continue;
        }
        if (name == QString("DatapointType")) {
            DatapointType = attribute.value().toString().split(' ');
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ComObjectRef_t::tableColumnCount() const
{
    return 17;
}

QVariant ComObjectRef_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ComObjectRef";
        }
        if (qualifiedName == QString("Id")) {
            return Id;
        }
        if (qualifiedName == QString("RefId")) {
            return RefId;
        }
        if (qualifiedName == QString("Name")) {
            return Name;
        }
        if (qualifiedName == QString("Text")) {
            return Text;
        }
        if (qualifiedName == QString("Tag")) {
            return Tag;
        }
        if (qualifiedName == QString("FunctionText")) {
            return FunctionText;
        }
        if (qualifiedName == QString("VisibleDescription")) {
            return VisibleDescription;
        }
        if (qualifiedName == QString("Priority")) {
            return Priority;
        }
        if (qualifiedName == QString("ObjectSize")) {
            return ObjectSize;
        }
        if (qualifiedName == QString("ReadFlag")) {
            return ReadFlag;
        }
        if (qualifiedName == QString("WriteFlag")) {
            return WriteFlag;
        }
        if (qualifiedName == QString("CommunicationFlag")) {
            return CommunicationFlag;
        }
        if (qualifiedName == QString("TransmitFlag")) {
            return TransmitFlag;
        }
        if (qualifiedName == QString("UpdateFlag")) {
            return UpdateFlag;
        }
        if (qualifiedName == QString("ReadOnInitFlag")) {
            return ReadOnInitFlag;
        }
        if (qualifiedName == QString("DatapointType")) {
            return DatapointType.join(' ');
        }
        break;
    }
    return QVariant();
}

QVariant ComObjectRef_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Id";
            case 2:
                return "RefId";
            case 3:
                return "Name";
            case 4:
                return "Text";
            case 5:
                return "Tag";
            case 6:
                return "FunctionText";
            case 7:
                return "VisibleDescription";
            case 8:
                return "Priority";
            case 9:
                return "ObjectSize";
            case 10:
                return "ReadFlag";
            case 11:
                return "WriteFlag";
            case 12:
                return "CommunicationFlag";
            case 13:
                return "TransmitFlag";
            case 14:
                return "UpdateFlag";
            case 15:
                return "ReadOnInitFlag";
            case 16:
                return "DatapointType";
            }
        }
    }
    return QVariant();
}

QVariant ComObjectRef_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
    {
        QStringList differences;
        if (!Name.isEmpty()) {
            differences.append("Name");
        }
        if (!Text.isEmpty()) {
            differences.append("Text");
        }
        if (!Tag.isEmpty()) {
            differences.append("Tag");
        }
        if (!FunctionText.isEmpty()) {
            differences.append("FunctionText");
        }
        if (!Priority.isEmpty()) {
            differences.append("Priority");
        }
        if (!ObjectSize.isEmpty()) {
            differences.append("ObjectSize");
        }
        if (!ReadFlag.isEmpty()) {
            differences.append("ReadFlag");
        }
        if (!WriteFlag.isEmpty()) {
            differences.append("WriteFlag");
        }
        if (!CommunicationFlag.isEmpty()) {
            differences.append("CommunicationFlag");
        }
        if (!TransmitFlag.isEmpty()) {
            differences.append("TransmitFlag");
        }
        if (!UpdateFlag.isEmpty()) {
            differences.append("UpdateFlag");
        }
        if (!ReadOnInitFlag.isEmpty()) {
            differences.append("ReadOnInitFlag");
        }
        if (!DatapointType.isEmpty()) {
            differences.append("DatapointType");
        }
#if PROJECT_VERSION >= 12
        if (!TextParameterRefId.isEmpty()) {
            differences.append("TextParameterRefId");
        }
        if (!InternalDescription.isEmpty()) {
            differences.append("InternalDescription");
        }
#endif
#if PROJECT_VERSION >= 13
        if (!Roles.isEmpty()) {
            differences.append("Roles");
        }
        if (!SecurityRequired.isEmpty()) {
            differences.append("SecurityRequired");
        }
#endif
        QString comObjectNumber = RefId.mid(Id.lastIndexOf("_O-") + 3);
        QString uniqueNumber = Id.mid(Id.lastIndexOf("_R-") + 3);
        return QString("[%1 %2] %3-%4 (%5)").arg(comObjectNumber).arg(uniqueNumber).arg(!Name.isEmpty() ? Name : Text).arg(FunctionText).arg(differences.join(", "));
    }
    case Qt::DecorationRole:
        return QIcon::fromTheme("ComObjectRef");
    }
    return QVariant();
}

ComObject_t * ComObjectRef_t::getComObject() const
{
    if (RefId.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<ComObject_t *>(knx->ids[RefId]);
}

QList<MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_t *> ComObjectRef_t::getDatapointType() const
{
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    QList<MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_t *> retVal;
    for (QString id : DatapointType) {
        retVal << qobject_cast<MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_t *>(knx->ids[id]);
    }
    return retVal;
}

ComObjectRef_t * make_ComObjectRef_t(Base * parent)
{
    return new ComObjectRef_t(parent);
}

} // namespace knx
} // namespace v10
} // namespace Project
