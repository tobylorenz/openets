/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v10/knx/IDREF.h>

namespace Project {
namespace v10 {
namespace knx {

/* forward declarations */
class ManufacturerData_Manufacturer_Baggages_Baggage_t;

class ApplicationProgramStatic_Extension_Baggage_t : public Base
{
    Q_OBJECT

public:
    explicit ApplicationProgramStatic_Extension_Baggage_t(Base * parent = nullptr);
    virtual ~ApplicationProgramStatic_Extension_Baggage_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    IDREF RefId{};

    /* getters */
    ManufacturerData_Manufacturer_Baggages_Baggage_t * getBaggage() const; // attribute: RefId
};

ApplicationProgramStatic_Extension_Baggage_t * make_ApplicationProgramStatic_Extension_Baggage_t(Base * parent);

} // namespace knx
} // namespace v10
} // namespace Project
