/* This file is generated. */

#pragma once

#include <Project/v10/knx/String255_t.h>

namespace Project {
namespace v10 {
namespace knx {

using ManufacturerData_Manufacturer_Baggages_Baggage_Name_t = String255_t;

} // namespace knx
} // namespace v10
} // namespace Project
