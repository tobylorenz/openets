/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v10/knx/Access_t.h>
#include <Project/v10/knx/LanguageDependentString255_t.h>
#include <Project/xs/ID.h>

namespace Project {
namespace v10 {
namespace knx {

class ParameterSeparator_t : public Base
{
    Q_OBJECT

public:
    explicit ParameterSeparator_t(Base * parent = nullptr);
    virtual ~ParameterSeparator_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::ID Id{};
    LanguageDependentString255_t Text{};
    Access_t Access{"ReadWrite"};

    /* getters */
};

ParameterSeparator_t * make_ParameterSeparator_t(Base * parent);

} // namespace knx
} // namespace v10
} // namespace Project
