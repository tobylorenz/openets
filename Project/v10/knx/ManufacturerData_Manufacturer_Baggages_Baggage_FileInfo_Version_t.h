/* This file is generated. */

#pragma once

#include <Project/xs/String.h>

namespace Project {
namespace v10 {
namespace knx {

using ManufacturerData_Manufacturer_Baggages_Baggage_FileInfo_Version_t = xs::String;

} // namespace knx
} // namespace v10
} // namespace Project
