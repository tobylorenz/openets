/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/SimpleElementTextType.h>
#include <Project/v10/knx/ManufacturerData_Manufacturer_Baggages_Baggage_Name_t.h>
#include <Project/v10/knx/ManufacturerData_Manufacturer_Baggages_Baggage_TargetPath_t.h>
#include <Project/xs/ID.h>
#include <Project/xs/Base64Binary.h>
#include <Project/xs/Boolean.h>

namespace Project {
namespace v10 {
namespace knx {

/* forward declarations */
class ManufacturerData_Manufacturer_Baggages_Baggage_FileInfo_t;

class ManufacturerData_Manufacturer_Baggages_Baggage_t : public Base
{
    Q_OBJECT

public:
    explicit ManufacturerData_Manufacturer_Baggages_Baggage_t(Base * parent = nullptr);
    virtual ~ManufacturerData_Manufacturer_Baggages_Baggage_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    ManufacturerData_Manufacturer_Baggages_Baggage_TargetPath_t TargetPath{};
    ManufacturerData_Manufacturer_Baggages_Baggage_Name_t Name{};
    xs::Boolean InstallOnImport{};
    xs::ID Id{};

    /* elements */
    ManufacturerData_Manufacturer_Baggages_Baggage_FileInfo_t * FileInfo{};
    SimpleElementTextType * Data{};
};

ManufacturerData_Manufacturer_Baggages_Baggage_t * make_ManufacturerData_Manufacturer_Baggages_Baggage_t(Base * parent);

} // namespace knx
} // namespace v10
} // namespace Project
