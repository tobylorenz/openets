/* This file is generated. */

#include <Project/v10/knx/ParameterChoose_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v10/knx/KNX_t.h>

namespace Project {
namespace v10 {
namespace knx {

ParameterChoose_t::ParameterChoose_t(Base * parent) :
    Base(parent)
{
}

ParameterChoose_t::~ParameterChoose_t()
{
}

void ParameterChoose_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("ParamRefId")) {
            ParamRefId = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("when")) {
            auto * newWhen = make_ParameterChoose_when_t(this);
            newWhen->read(reader);
            When.append(newWhen);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ParameterChoose_t::tableColumnCount() const
{
    return 2;
}

QVariant ParameterChoose_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ParameterChoose";
        }
        if (qualifiedName == QString("ParamRefId")) {
            return ParamRefId;
        }
        break;
    }
    return QVariant();
}

QVariant ParameterChoose_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "ParamRefId";
            }
        }
    }
    return QVariant();
}

QVariant ParameterChoose_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "ParameterChoose";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ParameterChoose");
    }
    return QVariant();
}

ParameterChoose_t * make_ParameterChoose_t(Base * parent)
{
    return new ParameterChoose_t(parent);
}

} // namespace knx
} // namespace v10
} // namespace Project
