/* This file is generated. */

#include <Project/v10/knx/ComObjectParameterBlock_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v10/knx/Assign_t.h>
#include <Project/v10/knx/BinaryDataRef_t.h>
#include <Project/v10/knx/ComObjectParameterChoose_t.h>
#include <Project/v10/knx/ComObjectRefRef_t.h>
#include <Project/v10/knx/KNX_t.h>
#include <Project/v10/knx/ParameterRefRef_t.h>
#include <Project/v10/knx/ParameterRef_t.h>
#include <Project/v10/knx/ParameterSeparator_t.h>

namespace Project {
namespace v10 {
namespace knx {

ComObjectParameterBlock_t::ComObjectParameterBlock_t(Base * parent) :
    Base(parent)
{
}

ComObjectParameterBlock_t::~ComObjectParameterBlock_t()
{
}

void ComObjectParameterBlock_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == "Id") {
            Id = attribute.value().toString();
            KNX_t * knx = findParent<KNX_t *>();
            Q_ASSERT(knx);
            knx->ids[Id] = this;
            continue;
        }
        if (name == QString("Name")) {
            Name = attribute.value().toString();
            continue;
        }
        if (name == QString("Text")) {
            Text = attribute.value().toString();
            continue;
        }
        if (name == QString("Access")) {
            Access = attribute.value().toString();
            continue;
        }
        if (name == QString("HelpTopic")) {
            HelpTopic = attribute.value().toString();
            continue;
        }
        if (name == QString("InternalDescription")) {
            InternalDescription = attribute.value().toString();
            continue;
        }
        if (name == QString("ParamRefId")) {
            ParamRefId = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("ParameterSeparator")) {
            auto * ParameterSeparator = make_ParameterSeparator_t(this);
            ParameterSeparator->read(reader);
            continue;
        }
        if (reader.name() == QString("ParameterRefRef")) {
            auto * ParameterRefRef = make_ParameterRefRef_t(this);
            ParameterRefRef->read(reader);
            continue;
        }
        if (reader.name() == QString("choose")) {
            auto * Choose = make_ComObjectParameterChoose_t(this);
            Choose->read(reader);
            continue;
        }
        if (reader.name() == QString("BinaryDataRef")) {
            auto * BinaryDataRef = make_BinaryDataRef_t(this);
            BinaryDataRef->read(reader);
            continue;
        }
        if (reader.name() == QString("ComObjectRefRef")) {
            auto * ComObjectRefRef = make_ComObjectRefRef_t(this);
            ComObjectRefRef->read(reader);
            continue;
        }
        if (reader.name() == QString("Assign")) {
            auto * Assign = make_Assign_t(this);
            Assign->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ComObjectParameterBlock_t::tableColumnCount() const
{
    return 8;
}

QVariant ComObjectParameterBlock_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ComObjectParameterBlock";
        }
        if (qualifiedName == QString("Id")) {
            return Id;
        }
        if (qualifiedName == QString("Name")) {
            return Name;
        }
        if (qualifiedName == QString("Text")) {
            return Text;
        }
        if (qualifiedName == QString("Access")) {
            return Access;
        }
        if (qualifiedName == QString("HelpTopic")) {
            return HelpTopic;
        }
        if (qualifiedName == QString("InternalDescription")) {
            return InternalDescription;
        }
        if (qualifiedName == QString("ParamRefId")) {
            return ParamRefId;
        }
        break;
    }
    return QVariant();
}

QVariant ComObjectParameterBlock_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Id";
            case 2:
                return "Name";
            case 3:
                return "Text";
            case 4:
                return "Access";
            case 5:
                return "HelpTopic";
            case 6:
                return "InternalDescription";
            case 7:
                return "ParamRefId";
            }
        }
    }
    return QVariant();
}

QVariant ComObjectParameterBlock_t::treeData(int role) const
{
    auto * ref = getParameterRef();
    if (ref) {
        return ref->treeData(role);
    }

    switch (role) {
    case Qt::DisplayRole:
        return !Name.isEmpty() ? Name : Text;
    case Qt::DecorationRole:
        return QIcon::fromTheme("ComObjectParameterBlock");
    }
    return QVariant();
}

ParameterRef_t * ComObjectParameterBlock_t::getParameterRef() const
{
    if (ParamRefId.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<ParameterRef_t *>(knx->ids[ParamRefId]);
}

ComObjectParameterBlock_t * make_ComObjectParameterBlock_t(Base * parent)
{
    return new ComObjectParameterBlock_t(parent);
}

} // namespace knx
} // namespace v10
} // namespace Project
