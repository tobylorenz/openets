/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v10/knx/Access_t.h>
#include <Project/v10/knx/IDREF.h>
#include <Project/v10/knx/LanguageDependentString255_t.h>
#include <Project/v10/knx/String50_t.h>
#include <Project/v10/knx/Value_t.h>
#include <Project/xs/ID.h>
#include <Project/xs/Boolean.h>

namespace Project {
namespace v10 {
namespace knx {

/* forward declarations */
class ParameterType_t;
class Parameter_Memory_t;
class Parameter_Property_t;

class Parameter_t : public Base
{
    Q_OBJECT

public:
    explicit Parameter_t(Base * parent = nullptr);
    virtual ~Parameter_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::Boolean LegacyPatchAlways{"false"};
    xs::ID Id{};
    String50_t Name{};
    IDREF ParameterType{};
    LanguageDependentString255_t Text{};
    Access_t Access{"ReadWrite"};
    Value_t Value{};

    /* elements */
    // xs:choice Parameter_Memory_t * Memory{};
    // xs:choice Parameter_Property_t * Property{};

    /* getters */
    ParameterType_t * getParameterType() const; // attribute: ParameterType
};

Parameter_t * make_Parameter_t(Base * parent);

} // namespace knx
} // namespace v10
} // namespace Project
