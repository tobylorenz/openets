/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v10/knx/DeviceInstance_BinaryData_BinaryData_t.h>

namespace Project {
namespace v10 {
namespace knx {

class DeviceInstance_BinaryData_t : public Base
{
    Q_OBJECT

public:
    explicit DeviceInstance_BinaryData_t(Base * parent = nullptr);
    virtual ~DeviceInstance_BinaryData_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* elements */
    QVector<DeviceInstance_BinaryData_BinaryData_t *> BinaryData;
};

DeviceInstance_BinaryData_t * make_DeviceInstance_BinaryData_t(Base * parent);

} // namespace knx
} // namespace v10
} // namespace Project
