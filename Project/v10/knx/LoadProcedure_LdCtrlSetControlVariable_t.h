/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v10/knx/LdCtrlControlVariable_t.h>
#include <Project/v10/knx/LdCtrlProcType_t.h>
#include <Project/xs/Boolean.h>

namespace Project {
namespace v10 {
namespace knx {

class LoadProcedure_LdCtrlSetControlVariable_t : public Base
{
    Q_OBJECT

public:
    explicit LoadProcedure_LdCtrlSetControlVariable_t(Base * parent = nullptr);
    virtual ~LoadProcedure_LdCtrlSetControlVariable_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    LdCtrlControlVariable_t Name{};
    xs::Boolean Value{};
    LdCtrlProcType_t AppliesTo{"auto"};
};

LoadProcedure_LdCtrlSetControlVariable_t * make_LoadProcedure_LdCtrlSetControlVariable_t(Base * parent);

} // namespace knx
} // namespace v10
} // namespace Project
