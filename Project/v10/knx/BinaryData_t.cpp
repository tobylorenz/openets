/* This file is generated. */

#include <Project/v10/knx/BinaryData_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v10/knx/KNX_t.h>

namespace Project {
namespace v10 {
namespace knx {

BinaryData_t::BinaryData_t(Base * parent) :
    Base(parent)
{
}

BinaryData_t::~BinaryData_t()
{
}

void BinaryData_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("Name")) {
            Name = attribute.value().toString();
            continue;
        }
        if (name == "Id") {
            Id = attribute.value().toString();
            KNX_t * knx = findParent<KNX_t *>();
            Q_ASSERT(knx);
            knx->ids[Id] = this;
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("Data")) {
            if (!Data) {
                Data = make_SimpleElementTextType("Data", this);
            }
            Data->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int BinaryData_t::tableColumnCount() const
{
    return 3;
}

QVariant BinaryData_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "BinaryData";
        }
        if (qualifiedName == QString("Name")) {
            return Name;
        }
        if (qualifiedName == QString("Id")) {
            return Id;
        }
        break;
    }
    return QVariant();
}

QVariant BinaryData_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Name";
            case 2:
                return "Id";
            }
        }
    }
    return QVariant();
}

QVariant BinaryData_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return !Name.isEmpty() ? Name : "BinaryData";
    case Qt::DecorationRole:
        return QIcon::fromTheme("BinaryData");
    }
    return QVariant();
}

BinaryData_t * make_BinaryData_t(Base * parent)
{
    return new BinaryData_t(parent);
}

} // namespace knx
} // namespace v10
} // namespace Project
