/* This file is generated. */

#include <Project/v10/knx/ApplicationProgramStatic_Parameters_Union_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v10/knx/ApplicationProgramStatic_Parameters_Union_Memory_t.h>
#include <Project/v10/knx/ApplicationProgramStatic_Parameters_Union_Property_t.h>
#include <Project/v10/knx/KNX_t.h>

namespace Project {
namespace v10 {
namespace knx {

ApplicationProgramStatic_Parameters_Union_t::ApplicationProgramStatic_Parameters_Union_t(Base * parent) :
    Base(parent)
{
}

ApplicationProgramStatic_Parameters_Union_t::~ApplicationProgramStatic_Parameters_Union_t()
{
}

void ApplicationProgramStatic_Parameters_Union_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("SizeInBit")) {
            SizeInBit = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("Memory")) {
            auto * Memory = make_ApplicationProgramStatic_Parameters_Union_Memory_t(this);
            Memory->read(reader);
            continue;
        }
        if (reader.name() == QString("Property")) {
            auto * Property = make_ApplicationProgramStatic_Parameters_Union_Property_t(this);
            Property->read(reader);
            continue;
        }
        if (reader.name() == QString("Parameter")) {
            QString newId = reader.attributes().value("Id").toString();
            Q_ASSERT(!newId.isEmpty());
            UnionParameter_t * newParameter;
            if (Parameter.contains(newId)) {
                newParameter = Parameter[newId];
            } else {
                newParameter = make_UnionParameter_t(this);
                Parameter[newId] = newParameter;
            }
            newParameter->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ApplicationProgramStatic_Parameters_Union_t::tableColumnCount() const
{
    return 2;
}

QVariant ApplicationProgramStatic_Parameters_Union_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ApplicationProgramStatic_Parameters_Union";
        }
        if (qualifiedName == QString("SizeInBit")) {
            return SizeInBit;
        }
        break;
    }
    return QVariant();
}

QVariant ApplicationProgramStatic_Parameters_Union_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "SizeInBit";
            }
        }
    }
    return QVariant();
}

QVariant ApplicationProgramStatic_Parameters_Union_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "Union";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ApplicationProgramStatic_Parameters_Union");
    }
    return QVariant();
}

ApplicationProgramStatic_Parameters_Union_t * make_ApplicationProgramStatic_Parameters_Union_t(Base * parent)
{
    return new ApplicationProgramStatic_Parameters_Union_t(parent);
}

} // namespace knx
} // namespace v10
} // namespace Project
