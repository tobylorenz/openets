/* This file is generated. */

#include <Project/v10/knx/ParameterType_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v10/knx/KNX_t.h>
#include <Project/v10/knx/ParameterType_TypeDate_t.h>
#include <Project/v10/knx/ParameterType_TypeFloat_t.h>
#include <Project/v10/knx/ParameterType_TypeIPAddress_t.h>
#include <Project/v10/knx/ParameterType_TypeNone_t.h>
#include <Project/v10/knx/ParameterType_TypeNumber_t.h>
#include <Project/v10/knx/ParameterType_TypeRestriction_t.h>
#include <Project/v10/knx/ParameterType_TypeText_t.h>
#include <Project/v10/knx/ParameterType_TypeTime_t.h>

namespace Project {
namespace v10 {
namespace knx {

ParameterType_t::ParameterType_t(Base * parent) :
    Base(parent)
{
}

ParameterType_t::~ParameterType_t()
{
}

void ParameterType_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == "Id") {
            Id = attribute.value().toString();
            KNX_t * knx = findParent<KNX_t *>();
            Q_ASSERT(knx);
            knx->ids[Id] = this;
            continue;
        }
        if (name == QString("Name")) {
            Name = attribute.value().toString();
            continue;
        }
        if (name == QString("InternalDescription")) {
            InternalDescription = attribute.value().toString();
            continue;
        }
        if (name == QString("Plugin")) {
            Plugin = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("TypeNumber")) {
            auto * TypeNumber = make_ParameterType_TypeNumber_t(this);
            TypeNumber->read(reader);
            continue;
        }
        if (reader.name() == QString("TypeFloat")) {
            auto * TypeFloat = make_ParameterType_TypeFloat_t(this);
            TypeFloat->read(reader);
            continue;
        }
        if (reader.name() == QString("TypeRestriction")) {
            auto * TypeRestriction = make_ParameterType_TypeRestriction_t(this);
            TypeRestriction->read(reader);
            continue;
        }
        if (reader.name() == QString("TypeText")) {
            auto * TypeText = make_ParameterType_TypeText_t(this);
            TypeText->read(reader);
            continue;
        }
        if (reader.name() == QString("TypeTime")) {
            auto * TypeTime = make_ParameterType_TypeTime_t(this);
            TypeTime->read(reader);
            continue;
        }
        if (reader.name() == QString("TypeDate")) {
            auto * TypeDate = make_ParameterType_TypeDate_t(this);
            TypeDate->read(reader);
            continue;
        }
        if (reader.name() == QString("TypeIPAddress")) {
            auto * TypeIPAddress = make_ParameterType_TypeIPAddress_t(this);
            TypeIPAddress->read(reader);
            continue;
        }
        if (reader.name() == QString("TypeNone")) {
            auto * TypeNone = make_ParameterType_TypeNone_t(this);
            TypeNone->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ParameterType_t::tableColumnCount() const
{
    return 5;
}

QVariant ParameterType_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ParameterType";
        }
        if (qualifiedName == QString("Id")) {
            return Id;
        }
        if (qualifiedName == QString("Name")) {
            return Name;
        }
        if (qualifiedName == QString("InternalDescription")) {
            return InternalDescription;
        }
        if (qualifiedName == QString("Plugin")) {
            return Plugin;
        }
        break;
    }
    return QVariant();
}

QVariant ParameterType_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Id";
            case 2:
                return "Name";
            case 3:
                return "InternalDescription";
            case 4:
                return "Plugin";
            }
        }
    }
    return QVariant();
}

QVariant ParameterType_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return !Name.isEmpty() ? Name : "ParameterType";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ParameterType");
    }
    return QVariant();
}

ParameterType_t * make_ParameterType_t(Base * parent)
{
    return new ParameterType_t(parent);
}

} // namespace knx
} // namespace v10
} // namespace Project
