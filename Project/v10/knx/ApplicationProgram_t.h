/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v10/knx/ApplicationProgramIPConfig_t.h>
#include <Project/v10/knx/ApplicationProgramType_t.h>
#include <Project/v10/knx/ApplicationProgram_MinEtsVersion_t.h>
#include <Project/v10/knx/ApplicationProgram_ReplacesVersions_t.h>
#include <Project/v10/knx/IDREF.h>
#include <Project/v10/knx/LanguageDependentString255_t.h>
#include <Project/v10/knx/LanguageDependentString_t.h>
#include <Project/v10/knx/LoadProcedureStyle_t.h>
#include <Project/xs/ID.h>
#include <Project/xs/Base64Binary.h>
#include <Project/xs/Boolean.h>
#include <Project/xs/Int.h>
#include <Project/xs/Language.h>
#include <Project/xs/UnsignedByte.h>
#include <Project/xs/UnsignedInt.h>
#include <Project/xs/UnsignedShort.h>

namespace Project {
namespace v10 {
namespace knx {

/* forward declarations */
class ApplicationProgramDynamic_t;
class ApplicationProgramStatic_t;
class MaskVersion_t;
class MasterData_Manufacturers_Manufacturer_t;

class ApplicationProgram_t : public Base
{
    Q_OBJECT

public:
    explicit ApplicationProgram_t(Base * parent = nullptr);
    virtual ~ApplicationProgram_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::ID Id{};
    xs::UnsignedShort ApplicationNumber{};
    xs::UnsignedByte ApplicationVersion{};
    ApplicationProgramType_t ProgramType{};
    IDREF MaskVersion{};
    LanguageDependentString_t VisibleDescription{};
    LanguageDependentString255_t Name{};
    LoadProcedureStyle_t LoadProcedureStyle{};
    xs::UnsignedByte PeiType{};
    xs::UnsignedInt HelpTopic{};
    LanguageDependentString255_t HelpFile{};
    xs::Language DefaultLanguage{};
    xs::Boolean DynamicTableManagement{};
    xs::Boolean Linkable{};
    ApplicationProgram_MinEtsVersion_t MinEtsVersion{};
    IDREF OriginalManufacturer{};
    xs::Boolean PreEts4Style{"false"};
    xs::Boolean ConvertedFromPreEts4Data{"false"};
    ApplicationProgramIPConfig_t IPConfig{"Tool"};
    xs::Int AdditionalAddressesCount{"0"};
    xs::UnsignedShort NonRegRelevantDataVersion{"0"};
    xs::Boolean Broken{"false"};
    xs::Boolean DownloadInfoIncomplete{"false"};
    ApplicationProgram_ReplacesVersions_t ReplacesVersions{};
    xs::Base64Binary Hash{};

    /* elements */
    ApplicationProgramStatic_t * Static{};
    ApplicationProgramDynamic_t * Dynamic{};

    /* getters */
    MaskVersion_t * getMaskVersion() const; // attribute: MaskVersion
    MasterData_Manufacturers_Manufacturer_t * getOriginalManufacturer() const; // attribute: OriginalManufacturer
};

ApplicationProgram_t * make_ApplicationProgram_t(Base * parent);

} // namespace knx
} // namespace v10
} // namespace Project
