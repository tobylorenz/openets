/* This file is generated. */

#include <Project/v10/knx/MasterData_Languages_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v10/knx/KNX_t.h>

namespace Project {
namespace v10 {
namespace knx {

MasterData_Languages_t::MasterData_Languages_t(Base * parent) :
    Base(parent)
{
}

MasterData_Languages_t::~MasterData_Languages_t()
{
}

void MasterData_Languages_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("Language")) {
            QString newIdentifier = reader.attributes().value("Identifier").toString();
            Q_ASSERT(!newIdentifier.isEmpty());
            LanguageData_t * newLanguage;
            if (Language.contains(newIdentifier)) {
                newLanguage = Language[newIdentifier];
            } else {
                newLanguage = make_LanguageData_t(this);
                Language[newIdentifier] = newLanguage;
            }
            newLanguage->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int MasterData_Languages_t::tableColumnCount() const
{
    return 1;
}

QVariant MasterData_Languages_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "MasterData_Languages";
        }
        break;
    }
    return QVariant();
}

QVariant MasterData_Languages_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant MasterData_Languages_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "Languages";
    case Qt::DecorationRole:
        return QIcon::fromTheme("MasterData_Languages");
    }
    return QVariant();
}

MasterData_Languages_t * make_MasterData_Languages_t(Base * parent)
{
    return new MasterData_Languages_t(parent);
}

} // namespace knx
} // namespace v10
} // namespace Project
