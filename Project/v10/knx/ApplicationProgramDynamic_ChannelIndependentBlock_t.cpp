/* This file is generated. */

#include <Project/v10/knx/ApplicationProgramDynamic_ChannelIndependentBlock_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v10/knx/BinaryDataRef_t.h>
#include <Project/v10/knx/IndependentParameterBlockChoose_t.h>
#include <Project/v10/knx/KNX_t.h>
#include <Project/v10/knx/ParameterBlock_t.h>

namespace Project {
namespace v10 {
namespace knx {

ApplicationProgramDynamic_ChannelIndependentBlock_t::ApplicationProgramDynamic_ChannelIndependentBlock_t(Base * parent) :
    Base(parent)
{
}

ApplicationProgramDynamic_ChannelIndependentBlock_t::~ApplicationProgramDynamic_ChannelIndependentBlock_t()
{
}

void ApplicationProgramDynamic_ChannelIndependentBlock_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("ParameterBlock")) {
            auto * ParameterBlock = make_ParameterBlock_t(this);
            ParameterBlock->read(reader);
            continue;
        }
        if (reader.name() == QString("choose")) {
            auto * Choose = make_IndependentParameterBlockChoose_t(this);
            Choose->read(reader);
            continue;
        }
        if (reader.name() == QString("BinaryDataRef")) {
            auto * BinaryDataRef = make_BinaryDataRef_t(this);
            BinaryDataRef->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ApplicationProgramDynamic_ChannelIndependentBlock_t::tableColumnCount() const
{
    return 1;
}

QVariant ApplicationProgramDynamic_ChannelIndependentBlock_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ApplicationProgramDynamic_ChannelIndependentBlock";
        }
        break;
    }
    return QVariant();
}

QVariant ApplicationProgramDynamic_ChannelIndependentBlock_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant ApplicationProgramDynamic_ChannelIndependentBlock_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "ChannelIndependentBlock";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ApplicationProgramDynamic_ChannelIndependentBlock");
    }
    return QVariant();
}

ApplicationProgramDynamic_ChannelIndependentBlock_t * make_ApplicationProgramDynamic_ChannelIndependentBlock_t(Base * parent)
{
    return new ApplicationProgramDynamic_ChannelIndependentBlock_t(parent);
}

} // namespace knx
} // namespace v10
} // namespace Project
