/* This file is generated. */

#pragma once

#include <Project/xs/Int.h>

namespace Project {
namespace v10 {
namespace knx {

using Topology_Area_Address_t = xs::Int;

} // namespace knx
} // namespace v10
} // namespace Project
