/* This file is generated. */

#include <Project/v10/knx/HawkConfigurationData_MemorySegments_MemorySegment_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v10/knx/HawkConfigurationData_MemorySegments_MemorySegment_AccessRights_t.h>
#include <Project/v10/knx/KNX_t.h>
#include <Project/v10/knx/ResourceLocation_t.h>

namespace Project {
namespace v10 {
namespace knx {

HawkConfigurationData_MemorySegments_MemorySegment_t::HawkConfigurationData_MemorySegments_MemorySegment_t(Base * parent) :
    Base(parent)
{
}

HawkConfigurationData_MemorySegments_MemorySegment_t::~HawkConfigurationData_MemorySegments_MemorySegment_t()
{
}

void HawkConfigurationData_MemorySegments_MemorySegment_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("Length")) {
            Length = attribute.value().toString();
            continue;
        }
        if (name == QString("Optional")) {
            Optional = attribute.value().toString();
            continue;
        }
        if (name == QString("MemoryType")) {
            MemoryType = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("Location")) {
            if (!Location) {
                Location = make_ResourceLocation_t(this);
            }
            Location->read(reader);
            continue;
        }
        if (reader.name() == QString("AccessRights")) {
            if (!AccessRights) {
                AccessRights = make_HawkConfigurationData_MemorySegments_MemorySegment_AccessRights_t(this);
            }
            AccessRights->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int HawkConfigurationData_MemorySegments_MemorySegment_t::tableColumnCount() const
{
    return 4;
}

QVariant HawkConfigurationData_MemorySegments_MemorySegment_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "HawkConfigurationData_MemorySegments_MemorySegment";
        }
        if (qualifiedName == QString("Length")) {
            return Length;
        }
        if (qualifiedName == QString("Optional")) {
            return Optional;
        }
        if (qualifiedName == QString("MemoryType")) {
            return MemoryType;
        }
        break;
    }
    return QVariant();
}

QVariant HawkConfigurationData_MemorySegments_MemorySegment_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Length";
            case 2:
                return "Optional";
            case 3:
                return "MemoryType";
            }
        }
    }
    return QVariant();
}

QVariant HawkConfigurationData_MemorySegments_MemorySegment_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return MemoryType;
    case Qt::DecorationRole:
        return QIcon::fromTheme("HawkConfigurationData_MemorySegments_MemorySegment");
    }
    return QVariant();
}

HawkConfigurationData_MemorySegments_MemorySegment_t * make_HawkConfigurationData_MemorySegments_MemorySegment_t(Base * parent)
{
    return new HawkConfigurationData_MemorySegments_MemorySegment_t(parent);
}

} // namespace knx
} // namespace v10
} // namespace Project
