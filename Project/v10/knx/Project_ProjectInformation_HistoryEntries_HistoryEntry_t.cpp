/* This file is generated. */

#include <Project/v10/knx/Project_ProjectInformation_HistoryEntries_HistoryEntry_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v10/knx/KNX_t.h>

namespace Project {
namespace v10 {
namespace knx {

Project_ProjectInformation_HistoryEntries_HistoryEntry_t::Project_ProjectInformation_HistoryEntries_HistoryEntry_t(Base * parent) :
    Base(parent)
{
}

Project_ProjectInformation_HistoryEntries_HistoryEntry_t::~Project_ProjectInformation_HistoryEntries_HistoryEntry_t()
{
}

void Project_ProjectInformation_HistoryEntries_HistoryEntry_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("Date")) {
            Date = attribute.value().toString();
            continue;
        }
        if (name == QString("User")) {
            User = attribute.value().toString();
            continue;
        }
        if (name == QString("Text")) {
            Text = attribute.value().toString();
            continue;
        }
        if (name == QString("Detail")) {
            Detail = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int Project_ProjectInformation_HistoryEntries_HistoryEntry_t::tableColumnCount() const
{
    return 5;
}

QVariant Project_ProjectInformation_HistoryEntries_HistoryEntry_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "Project_ProjectInformation_HistoryEntries_HistoryEntry";
        }
        if (qualifiedName == QString("Date")) {
            return Date;
        }
        if (qualifiedName == QString("User")) {
            return User;
        }
        if (qualifiedName == QString("Text")) {
            return Text;
        }
        if (qualifiedName == QString("Detail")) {
            return Detail;
        }
        break;
    }
    return QVariant();
}

QVariant Project_ProjectInformation_HistoryEntries_HistoryEntry_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Date";
            case 2:
                return "User";
            case 3:
                return "Text";
            case 4:
                return "Detail";
            }
        }
    }
    return QVariant();
}

QVariant Project_ProjectInformation_HistoryEntries_HistoryEntry_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "HistoryEntry";
    case Qt::DecorationRole:
        return QIcon::fromTheme("Project_ProjectInformation_HistoryEntries_HistoryEntry");
    }
    return QVariant();
}

Project_ProjectInformation_HistoryEntries_HistoryEntry_t * make_Project_ProjectInformation_HistoryEntries_HistoryEntry_t(Base * parent)
{
    return new Project_ProjectInformation_HistoryEntries_HistoryEntry_t(parent);
}

} // namespace knx
} // namespace v10
} // namespace Project
