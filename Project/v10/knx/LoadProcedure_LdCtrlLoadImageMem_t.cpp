/* This file is generated. */

#include <Project/v10/knx/LoadProcedure_LdCtrlLoadImageMem_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v10/knx/KNX_t.h>

namespace Project {
namespace v10 {
namespace knx {

LoadProcedure_LdCtrlLoadImageMem_t::LoadProcedure_LdCtrlLoadImageMem_t(Base * parent) :
    Base(parent)
{
}

LoadProcedure_LdCtrlLoadImageMem_t::~LoadProcedure_LdCtrlLoadImageMem_t()
{
}

void LoadProcedure_LdCtrlLoadImageMem_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("AddressSpace")) {
            AddressSpace = attribute.value().toString();
            continue;
        }
        if (name == QString("Address")) {
            Address = attribute.value().toString();
            continue;
        }
        if (name == QString("Size")) {
            Size = attribute.value().toString();
            continue;
        }
        if (name == QString("AppliesTo")) {
            AppliesTo = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int LoadProcedure_LdCtrlLoadImageMem_t::tableColumnCount() const
{
    return 5;
}

QVariant LoadProcedure_LdCtrlLoadImageMem_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "LoadProcedure_LdCtrlLoadImageMem";
        }
        if (qualifiedName == QString("AddressSpace")) {
            return AddressSpace;
        }
        if (qualifiedName == QString("Address")) {
            return Address;
        }
        if (qualifiedName == QString("Size")) {
            return Size;
        }
        if (qualifiedName == QString("AppliesTo")) {
            return AppliesTo;
        }
        break;
    }
    return QVariant();
}

QVariant LoadProcedure_LdCtrlLoadImageMem_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "AddressSpace";
            case 2:
                return "Address";
            case 3:
                return "Size";
            case 4:
                return "AppliesTo";
            }
        }
    }
    return QVariant();
}

QVariant LoadProcedure_LdCtrlLoadImageMem_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "LdCtrlLoadImageMem";
    case Qt::DecorationRole:
        return QIcon::fromTheme("LoadProcedure_LdCtrlLoadImageMem");
    }
    return QVariant();
}

LoadProcedure_LdCtrlLoadImageMem_t * make_LoadProcedure_LdCtrlLoadImageMem_t(Base * parent)
{
    return new LoadProcedure_LdCtrlLoadImageMem_t(parent);
}

} // namespace knx
} // namespace v10
} // namespace Project
