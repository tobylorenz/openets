/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v10/knx/LoadProcedures_LoadProcedure_t.h>

namespace Project {
namespace v10 {
namespace knx {

class LoadProcedures_t : public Base
{
    Q_OBJECT

public:
    explicit LoadProcedures_t(Base * parent = nullptr);
    virtual ~LoadProcedures_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* elements */
    QVector<LoadProcedures_LoadProcedure_t *> LoadProcedure;
};

LoadProcedures_t * make_LoadProcedures_t(Base * parent);

} // namespace knx
} // namespace v10
} // namespace Project
