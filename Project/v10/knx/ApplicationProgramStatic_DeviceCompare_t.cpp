/* This file is generated. */

#include <Project/v10/knx/ApplicationProgramStatic_DeviceCompare_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v10/knx/KNX_t.h>

namespace Project {
namespace v10 {
namespace knx {

ApplicationProgramStatic_DeviceCompare_t::ApplicationProgramStatic_DeviceCompare_t(Base * parent) :
    Base(parent)
{
}

ApplicationProgramStatic_DeviceCompare_t::~ApplicationProgramStatic_DeviceCompare_t()
{
}

void ApplicationProgramStatic_DeviceCompare_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("ExcludeMemory")) {
            QString newCodeSegment = reader.attributes().value("CodeSegment").toString();
            Q_ASSERT(!newCodeSegment.isEmpty());
            ApplicationProgramStatic_DeviceCompare_ExcludeMemory_t * newExcludeMemory;
            if (ExcludeMemory.contains(newCodeSegment)) {
                newExcludeMemory = ExcludeMemory[newCodeSegment];
            } else {
                newExcludeMemory = make_ApplicationProgramStatic_DeviceCompare_ExcludeMemory_t(this);
                ExcludeMemory[newCodeSegment] = newExcludeMemory;
            }
            newExcludeMemory->read(reader);
            continue;
        }
        if (reader.name() == QString("ExcludeProperty")) {
            auto * newExcludeProperty = make_ApplicationProgramStatic_DeviceCompare_ExcludeProperty_t(this);
            newExcludeProperty->read(reader);
            ExcludeProperty.append(newExcludeProperty);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ApplicationProgramStatic_DeviceCompare_t::tableColumnCount() const
{
    return 1;
}

QVariant ApplicationProgramStatic_DeviceCompare_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ApplicationProgramStatic_DeviceCompare";
        }
        break;
    }
    return QVariant();
}

QVariant ApplicationProgramStatic_DeviceCompare_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant ApplicationProgramStatic_DeviceCompare_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "DeviceCompare";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ApplicationProgramStatic_DeviceCompare");
    }
    return QVariant();
}

ApplicationProgramStatic_DeviceCompare_t * make_ApplicationProgramStatic_DeviceCompare_t(Base * parent)
{
    return new ApplicationProgramStatic_DeviceCompare_t(parent);
}

} // namespace knx
} // namespace v10
} // namespace Project
