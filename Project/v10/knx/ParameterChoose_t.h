/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v10/knx/IDREF.h>
#include <Project/v10/knx/ParameterChoose_when_t.h>

namespace Project {
namespace v10 {
namespace knx {

class ParameterChoose_t : public Base
{
    Q_OBJECT

public:
    explicit ParameterChoose_t(Base * parent = nullptr);
    virtual ~ParameterChoose_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    IDREF ParamRefId{};

    /* elements */
    QVector<ParameterChoose_when_t *> When;
};

ParameterChoose_t * make_ParameterChoose_t(Base * parent);

} // namespace knx
} // namespace v10
} // namespace Project
