/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v10/knx/IDREF.h>
#include <Project/v10/knx/IndependentParameterBlockChoose_when_t.h>

namespace Project {
namespace v10 {
namespace knx {

class IndependentParameterBlockChoose_t : public Base
{
    Q_OBJECT

public:
    explicit IndependentParameterBlockChoose_t(Base * parent = nullptr);
    virtual ~IndependentParameterBlockChoose_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    IDREF ParamRefId{};

    /* elements */
    QVector<IndependentParameterBlockChoose_when_t *> When;
};

IndependentParameterBlockChoose_t * make_IndependentParameterBlockChoose_t(Base * parent);

} // namespace knx
} // namespace v10
} // namespace Project
