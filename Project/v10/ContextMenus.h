#pragma once

#include <QMdiArea>
#include <QMenu>

#include <Project/v10/knx/DeviceInstance_t.h>

/* context menus */
QMenu * getCustomContextMenu(QMdiArea * mdiArea, Project::v10::knx::DeviceInstance_t * deviceInstance);
