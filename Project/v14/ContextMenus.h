#pragma once

#include <QMdiArea>
#include <QMenu>

#include <Project/v14/knx/DeviceInstance_t.h>

/* context menus */
QMenu * getCustomContextMenu(QMdiArea * mdiArea, Project::v14::knx::DeviceInstance_t * deviceInstance);
