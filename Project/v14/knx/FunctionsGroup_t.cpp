/* This file is generated. */

#include <Project/v14/knx/FunctionsGroup_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v14/knx/KNX_t.h>

namespace Project {
namespace v14 {
namespace knx {

FunctionsGroup_t::FunctionsGroup_t(Base * parent) :
    Base(parent)
{
}

FunctionsGroup_t::~FunctionsGroup_t()
{
}

void FunctionsGroup_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == "Id") {
            Id = attribute.value().toString();
            KNX_t * knx = findParent<KNX_t *>();
            Q_ASSERT(knx);
            knx->ids[Id] = this;
            continue;
        }
        if (name == QString("Number")) {
            Number = attribute.value().toString();
            continue;
        }
        if (name == QString("Text")) {
            Text = attribute.value().toString();
            continue;
        }
        if (name == QString("Description")) {
            Description = attribute.value().toString();
            continue;
        }
        if (name == QString("Status")) {
            Status = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("FunctionsGroup")) {
            QString newId = reader.attributes().value("Id").toString();
            Q_ASSERT(!newId.isEmpty());
            FunctionsGroup_t * newFunctionsGroup;
            if (FunctionsGroup.contains(newId)) {
                newFunctionsGroup = FunctionsGroup[newId];
            } else {
                newFunctionsGroup = make_FunctionsGroup_t(this);
                FunctionsGroup[newId] = newFunctionsGroup;
            }
            newFunctionsGroup->read(reader);
            continue;
        }
        if (reader.name() == QString("FunctionType")) {
            QString newId = reader.attributes().value("Id").toString();
            Q_ASSERT(!newId.isEmpty());
            FunctionType_t * newFunctionType;
            if (FunctionType.contains(newId)) {
                newFunctionType = FunctionType[newId];
            } else {
                newFunctionType = make_FunctionType_t(this);
                FunctionType[newId] = newFunctionType;
            }
            newFunctionType->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int FunctionsGroup_t::tableColumnCount() const
{
    return 6;
}

QVariant FunctionsGroup_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "FunctionsGroup";
        }
        if (qualifiedName == QString("Id")) {
            return Id;
        }
        if (qualifiedName == QString("Number")) {
            return Number;
        }
        if (qualifiedName == QString("Text")) {
            return Text;
        }
        if (qualifiedName == QString("Description")) {
            return Description;
        }
        if (qualifiedName == QString("Status")) {
            return Status;
        }
        break;
    }
    return QVariant();
}

QVariant FunctionsGroup_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Id";
            case 2:
                return "Number";
            case 3:
                return "Text";
            case 4:
                return "Description";
            case 5:
                return "Status";
            }
        }
    }
    return QVariant();
}

QVariant FunctionsGroup_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "FunctionsGroup";
    case Qt::DecorationRole:
        return QIcon::fromTheme("FunctionsGroup");
    }
    return QVariant();
}

FunctionsGroup_t * make_FunctionsGroup_t(Base * parent)
{
    return new FunctionsGroup_t(parent);
}

} // namespace knx
} // namespace v14
} // namespace Project
