/* This file is generated. */

#include <Project/v14/knx/ParameterValidation_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v14/knx/KNX_t.h>
#include <Project/v14/knx/ParameterValidation_Parameters_t.h>

namespace Project {
namespace v14 {
namespace knx {

ParameterValidation_t::ParameterValidation_t(Base * parent) :
    Base(parent)
{
}

ParameterValidation_t::~ParameterValidation_t()
{
}

void ParameterValidation_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == "Id") {
            Id = attribute.value().toString();
            KNX_t * knx = findParent<KNX_t *>();
            Q_ASSERT(knx);
            knx->ids[Id] = this;
            continue;
        }
        if (name == QString("Name")) {
            Name = attribute.value().toString();
            continue;
        }
        if (name == QString("InternalDescription")) {
            InternalDescription = attribute.value().toString();
            continue;
        }
        if (name == QString("ValidationFunc")) {
            ValidationFunc = attribute.value().toString();
            continue;
        }
        if (name == QString("ValidationParameters")) {
            ValidationParameters = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("Parameters")) {
            if (!Parameters) {
                Parameters = make_ParameterValidation_Parameters_t(this);
            }
            Parameters->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ParameterValidation_t::tableColumnCount() const
{
    return 6;
}

QVariant ParameterValidation_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ParameterValidation";
        }
        if (qualifiedName == QString("Id")) {
            return Id;
        }
        if (qualifiedName == QString("Name")) {
            return Name;
        }
        if (qualifiedName == QString("InternalDescription")) {
            return InternalDescription;
        }
        if (qualifiedName == QString("ValidationFunc")) {
            return ValidationFunc;
        }
        if (qualifiedName == QString("ValidationParameters")) {
            return ValidationParameters;
        }
        break;
    }
    return QVariant();
}

QVariant ParameterValidation_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Id";
            case 2:
                return "Name";
            case 3:
                return "InternalDescription";
            case 4:
                return "ValidationFunc";
            case 5:
                return "ValidationParameters";
            }
        }
    }
    return QVariant();
}

QVariant ParameterValidation_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return !Name.isEmpty() ? Name : "ParameterValidation";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ParameterValidation");
    }
    return QVariant();
}

ParameterValidation_t * make_ParameterValidation_t(Base * parent)
{
    return new ParameterValidation_t(parent);
}

} // namespace knx
} // namespace v14
} // namespace Project
