/* This file is generated. */

#include <Project/v14/knx/MaskVersion_DownwardCompatibleMasks_DownwardCompatibleMask_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v14/knx/KNX_t.h>
#include <Project/v14/knx/MaskVersion_t.h>

namespace Project {
namespace v14 {
namespace knx {

MaskVersion_DownwardCompatibleMasks_DownwardCompatibleMask_t::MaskVersion_DownwardCompatibleMasks_DownwardCompatibleMask_t(Base * parent) :
    Base(parent)
{
}

MaskVersion_DownwardCompatibleMasks_DownwardCompatibleMask_t::~MaskVersion_DownwardCompatibleMasks_DownwardCompatibleMask_t()
{
}

void MaskVersion_DownwardCompatibleMasks_DownwardCompatibleMask_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("RefId")) {
            RefId = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int MaskVersion_DownwardCompatibleMasks_DownwardCompatibleMask_t::tableColumnCount() const
{
    return 2;
}

QVariant MaskVersion_DownwardCompatibleMasks_DownwardCompatibleMask_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "MaskVersion_DownwardCompatibleMasks_DownwardCompatibleMask";
        }
        if (qualifiedName == QString("RefId")) {
            return RefId;
        }
        break;
    }
    return QVariant();
}

QVariant MaskVersion_DownwardCompatibleMasks_DownwardCompatibleMask_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "RefId";
            }
        }
    }
    return QVariant();
}

QVariant MaskVersion_DownwardCompatibleMasks_DownwardCompatibleMask_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "DownwardCompatibleMask";
    case Qt::DecorationRole:
        return QIcon::fromTheme("MaskVersion_DownwardCompatibleMasks_DownwardCompatibleMask");
    }
    return QVariant();
}

MaskVersion_t * MaskVersion_DownwardCompatibleMasks_DownwardCompatibleMask_t::getMaskVersion() const
{
    if (RefId.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<MaskVersion_t *>(knx->ids[RefId]);
}

MaskVersion_DownwardCompatibleMasks_DownwardCompatibleMask_t * make_MaskVersion_DownwardCompatibleMasks_DownwardCompatibleMask_t(Base * parent)
{
    return new MaskVersion_DownwardCompatibleMasks_DownwardCompatibleMask_t(parent);
}

} // namespace knx
} // namespace v14
} // namespace Project
