/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v14/knx/SplitInfo_Cookie_t.h>
#include <Project/xs/String.h>

namespace Project {
namespace v14 {
namespace knx {

class SplitInfo_t : public Base
{
    Q_OBJECT

public:
    explicit SplitInfo_t(Base * parent = nullptr);
    virtual ~SplitInfo_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::String ObjectPath{};
    SplitInfo_Cookie_t Cookie{};
};

SplitInfo_t * make_SplitInfo_t(Base * parent);

} // namespace knx
} // namespace v14
} // namespace Project
