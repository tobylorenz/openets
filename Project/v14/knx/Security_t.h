/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v14/knx/Aes128Key_t.h>
#include <Project/v14/knx/IDREF.h>
#include <Project/v14/knx/String20_t.h>
#include <Project/xs/Base64Binary.h>
#include <Project/xs/DateTime.h>
#include <Project/xs/UnsignedLong.h>

namespace Project {
namespace v14 {
namespace knx {

/* forward declarations */
class Security_Role_t;

class Security_t : public Base
{
    Q_OBJECT

public:
    explicit Security_t(Base * parent = nullptr);
    virtual ~Security_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    Aes128Key_t LoadedIPRoutingBackboneKey{};
    String20_t DeviceAuthenticationCode{};
    xs::Base64Binary DeviceAuthenticationCodeHash{};
    xs::Base64Binary LoadedDeviceAuthenticationCodeHash{};
    String20_t DeviceManagementPassword{};
    xs::Base64Binary DeviceManagementPasswordHash{};
    xs::Base64Binary LoadedDeviceManagementPasswordHash{};
    Aes128Key_t ToolKey{};
    Aes128Key_t LoadedToolKey{};
    xs::UnsignedLong SequenceNumber{};
    xs::DateTime SequenceNumberTimestamp{};

    /* elements */
    Security_Role_t * Role{};
};

Security_t * make_Security_t(Base * parent);

} // namespace knx
} // namespace v14
} // namespace Project
