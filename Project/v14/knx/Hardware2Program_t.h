/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v14/knx/ApplicationProgramRef_t.h>
#include <Project/v14/knx/IDREFS.h>
#include <Project/v14/knx/MasterData_MediumTypes_MediumType_t.h>
#include <Project/xs/ID.h>
#include <Project/xs/Base64Binary.h>

namespace Project {
namespace v14 {
namespace knx {

/* forward declarations */
class RegistrationInfo_t;

class Hardware2Program_t : public Base
{
    Q_OBJECT

public:
    explicit Hardware2Program_t(Base * parent = nullptr);
    virtual ~Hardware2Program_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::ID Id{};
    IDREFS MediumTypes{};
    xs::Base64Binary Hash{};
    xs::Base64Binary CheckSums{};
    xs::Base64Binary LoadedImage{};

    /* elements */
    QMap<IDREF, ApplicationProgramRef_t *> ApplicationProgramRef; // key: RefId
    RegistrationInfo_t * RegistrationInfo{};

    /* getters */
    QList<MasterData_MediumTypes_MediumType_t *> getMediumTypes() const; // attribute: MediumTypes
};

Hardware2Program_t * make_Hardware2Program_t(Base * parent);

} // namespace knx
} // namespace v14
} // namespace Project
