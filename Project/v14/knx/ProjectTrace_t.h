/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/xs/DateTime.h>
#include <Project/xs/String.h>

namespace Project {
namespace v14 {
namespace knx {

class ProjectTrace_t : public Base
{
    Q_OBJECT

public:
    explicit ProjectTrace_t(Base * parent = nullptr);
    virtual ~ProjectTrace_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::DateTime Date{};
    xs::String UserName{};
    xs::String Comment{};
};

ProjectTrace_t * make_ProjectTrace_t(Base * parent);

} // namespace knx
} // namespace v14
} // namespace Project
