/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v14/knx/ParameterInstanceRef_t.h>

namespace Project {
namespace v14 {
namespace knx {

class DeviceInstance_ParameterInstanceRefs_t : public Base
{
    Q_OBJECT

public:
    explicit DeviceInstance_ParameterInstanceRefs_t(Base * parent = nullptr);
    virtual ~DeviceInstance_ParameterInstanceRefs_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* elements */
    QMap<IDREF, ParameterInstanceRef_t *> ParameterInstanceRef; // key: RefId
};

DeviceInstance_ParameterInstanceRefs_t * make_DeviceInstance_ParameterInstanceRefs_t(Base * parent);

} // namespace knx
} // namespace v14
} // namespace Project
