/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v14/knx/ApplicationProgramStatic_Messages_Message_t.h>
#include <Project/xs/ID.h>

namespace Project {
namespace v14 {
namespace knx {

class ApplicationProgramStatic_Messages_t : public Base
{
    Q_OBJECT

public:
    explicit ApplicationProgramStatic_Messages_t(Base * parent = nullptr);
    virtual ~ApplicationProgramStatic_Messages_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* elements */
    QMap<xs::ID, ApplicationProgramStatic_Messages_Message_t *> Message; // key: Id
};

ApplicationProgramStatic_Messages_t * make_ApplicationProgramStatic_Messages_t(Base * parent);

} // namespace knx
} // namespace v14
} // namespace Project
