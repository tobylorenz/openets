/* This file is generated. */

#pragma once

#include <Project/xs/Int.h>

namespace Project {
namespace v14 {
namespace knx {

using DeviceInstance_Address_t = xs::Int;

} // namespace knx
} // namespace v14
} // namespace Project
