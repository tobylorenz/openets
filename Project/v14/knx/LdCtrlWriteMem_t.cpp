/* This file is generated. */

#include <Project/v14/knx/LdCtrlWriteMem_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v14/knx/KNX_t.h>

namespace Project {
namespace v14 {
namespace knx {

LdCtrlWriteMem_t::LdCtrlWriteMem_t(Base * parent) :
    Base(parent)
{
}

LdCtrlWriteMem_t::~LdCtrlWriteMem_t()
{
}

void LdCtrlWriteMem_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("AppliesTo")) {
            AppliesTo = attribute.value().toString();
            continue;
        }
        if (name == QString("InternalDescription")) {
            InternalDescription = attribute.value().toString();
            continue;
        }
        if (name == QString("AddressSpace")) {
            AddressSpace = attribute.value().toString();
            continue;
        }
        if (name == QString("Address")) {
            Address = attribute.value().toString();
            continue;
        }
        if (name == QString("Size")) {
            Size = attribute.value().toString();
            continue;
        }
        if (name == QString("Verify")) {
            Verify = attribute.value().toString();
            continue;
        }
        if (name == QString("InlineData")) {
            InlineData = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("OnError")) {
            auto * newOnError = make_LdCtrlBase_OnError_t(this);
            newOnError->read(reader);
            OnError.append(newOnError);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int LdCtrlWriteMem_t::tableColumnCount() const
{
    return 8;
}

QVariant LdCtrlWriteMem_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "LdCtrlWriteMem";
        }
        if (qualifiedName == QString("AppliesTo")) {
            return AppliesTo;
        }
        if (qualifiedName == QString("InternalDescription")) {
            return InternalDescription;
        }
        if (qualifiedName == QString("AddressSpace")) {
            return AddressSpace;
        }
        if (qualifiedName == QString("Address")) {
            return Address;
        }
        if (qualifiedName == QString("Size")) {
            return Size;
        }
        if (qualifiedName == QString("Verify")) {
            return Verify;
        }
        if (qualifiedName == QString("InlineData")) {
            return InlineData;
        }
        break;
    }
    return QVariant();
}

QVariant LdCtrlWriteMem_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "AppliesTo";
            case 2:
                return "InternalDescription";
            case 3:
                return "AddressSpace";
            case 4:
                return "Address";
            case 5:
                return "Size";
            case 6:
                return "Verify";
            case 7:
                return "InlineData";
            }
        }
    }
    return QVariant();
}

QVariant LdCtrlWriteMem_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "LdCtrlWriteMem";
    case Qt::DecorationRole:
        return QIcon::fromTheme("LdCtrlWriteMem");
    }
    return QVariant();
}

LdCtrlWriteMem_t * make_LdCtrlWriteMem_t(Base * parent)
{
    return new LdCtrlWriteMem_t(parent);
}

} // namespace knx
} // namespace v14
} // namespace Project
