/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v14/knx/ApplicationProgramStatic_BusInterfaces_BusInterface_AccessType_t.h>
#include <Project/v14/knx/LanguageDependentString255_t.h>
#include <Project/xs/ID.h>
#include <Project/xs/UnsignedByte.h>

namespace Project {
namespace v14 {
namespace knx {

class ApplicationProgramStatic_BusInterfaces_BusInterface_t : public Base
{
    Q_OBJECT

public:
    explicit ApplicationProgramStatic_BusInterfaces_BusInterface_t(Base * parent = nullptr);
    virtual ~ApplicationProgramStatic_BusInterfaces_BusInterface_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::ID Id{};
    xs::UnsignedByte AddressIndex{};
    ApplicationProgramStatic_BusInterfaces_BusInterface_AccessType_t AccessType{};
    LanguageDependentString255_t Text{};
};

ApplicationProgramStatic_BusInterfaces_BusInterface_t * make_ApplicationProgramStatic_BusInterfaces_BusInterface_t(Base * parent);

} // namespace knx
} // namespace v14
} // namespace Project
