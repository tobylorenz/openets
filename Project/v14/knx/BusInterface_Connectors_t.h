/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v14/knx/BusInterface_Connectors_Connector_t.h>
#include <Project/v14/knx/IDREF.h>

namespace Project {
namespace v14 {
namespace knx {

class BusInterface_Connectors_t : public Base
{
    Q_OBJECT

public:
    explicit BusInterface_Connectors_t(Base * parent = nullptr);
    virtual ~BusInterface_Connectors_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* elements */
    QMap<IDREF, BusInterface_Connectors_Connector_t *> Connector; // key: GroupAddressRefId
};

BusInterface_Connectors_t * make_BusInterface_Connectors_t(Base * parent);

} // namespace knx
} // namespace v14
} // namespace Project
