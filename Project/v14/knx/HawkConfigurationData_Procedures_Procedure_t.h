/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v14/knx/HawkConfigurationData_Procedures_Procedure_Access_t.h>
#include <Project/v14/knx/HawkConfigurationData_Procedures_Procedure_ProcedureSubType_t.h>
#include <Project/v14/knx/LdCtrlBase_t.h>
#include <Project/v14/knx/ProcedureType_t.h>

namespace Project {
namespace v14 {
namespace knx {

/* forward declarations */
class LdCtrlBaseChoose_t;

class HawkConfigurationData_Procedures_Procedure_t : public Base
{
    Q_OBJECT

public:
    explicit HawkConfigurationData_Procedures_Procedure_t(Base * parent = nullptr);
    virtual ~HawkConfigurationData_Procedures_Procedure_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    ProcedureType_t ProcedureType{};
    HawkConfigurationData_Procedures_Procedure_ProcedureSubType_t ProcedureSubType{};
    HawkConfigurationData_Procedures_Procedure_Access_t Access{};

    /* elements */
    LdCtrlBase_t * LdCtrlBase{};
    // xs:choice LdCtrlBaseChoose_t * Choose{};
};

HawkConfigurationData_Procedures_Procedure_t * make_HawkConfigurationData_Procedures_Procedure_t(Base * parent);

} // namespace knx
} // namespace v14
} // namespace Project
