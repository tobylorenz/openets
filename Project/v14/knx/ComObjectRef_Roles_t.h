/* This file is generated. */

#pragma once

#include <Project/xs/List.h>
#include <Project/xs/String.h>

namespace Project {
namespace v14 {
namespace knx {

using ComObjectRef_Roles_t = xs::List<xs::String>;

} // namespace knx
} // namespace v14
} // namespace Project
