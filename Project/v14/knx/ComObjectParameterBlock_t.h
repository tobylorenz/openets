/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v14/knx/Access_t.h>
#include <Project/v14/knx/CellRef_t.h>
#include <Project/v14/knx/IDREF.h>
#include <Project/v14/knx/LanguageDependentString255_t.h>
#include <Project/v14/knx/ParameterBlockLayout_t.h>
#include <Project/v14/knx/String50_t.h>
#include <Project/xs/ID.h>
#include <Project/xs/Boolean.h>
#include <Project/xs/String.h>
#include <Project/xs/UnsignedInt.h>

namespace Project {
namespace v14 {
namespace knx {

/* forward declarations */
class ApplicationProgramChannel_t;
class Assign_t;
class BinaryDataRef_t;
class Button_t;
class ComObjectParameterBlock_Columns_t;
class ComObjectParameterBlock_Rows_t;
class ComObjectParameterBlock_t;
class ComObjectParameterChoose_t;
class ComObjectRefRef_t;
class ParameterRefRef_t;
class ParameterRef_t;
class ParameterSeparator_t;

class ComObjectParameterBlock_t : public Base
{
    Q_OBJECT

public:
    explicit ComObjectParameterBlock_t(Base * parent = nullptr);
    virtual ~ComObjectParameterBlock_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::ID Id{};
    String50_t Name{};
    LanguageDependentString255_t Text{};
    Access_t Access{"ReadWrite"};
    xs::UnsignedInt HelpTopic{};
    xs::String InternalDescription{};
    IDREF ParamRefId{};
    IDREF TextParameterRefId{};
    xs::Boolean Inline{"false"};
    ParameterBlockLayout_t Layout{"List"};
    CellRef_t Cell{};
    xs::String Icon{};
    xs::String HelpContext{};
    xs::Boolean ShowInComObjectTree{"false"};

    /* elements */
    ComObjectParameterBlock_Rows_t * Rows{};
    ComObjectParameterBlock_Columns_t * Columns{};
    // xs:choice ComObjectParameterBlock_t * ParameterBlock{};
    // xs:choice ParameterSeparator_t * ParameterSeparator{};
    // xs:choice ParameterRefRef_t * ParameterRefRef{};
    // xs:choice Button_t * Button{};
    // xs:choice ComObjectParameterChoose_t * Choose{};
    // xs:choice BinaryDataRef_t * BinaryDataRef{};
    // xs:choice ComObjectRefRef_t * ComObjectRefRef{};
    // xs:choice Assign_t * Assign{};
    // xs:choice ApplicationProgramChannel_t * Channel{};

    /* getters */
    ParameterRef_t * getParameterRef() const; // attribute: ParamRefId
    ParameterRef_t * getTextParameterRef() const; // attribute: TextParameterRefId
};

ComObjectParameterBlock_t * make_ComObjectParameterBlock_t(Base * parent);

} // namespace knx
} // namespace v14
} // namespace Project
