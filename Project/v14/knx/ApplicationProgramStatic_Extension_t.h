/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v14/knx/ApplicationProgramStatic_Extension_Baggage_t.h>
#include <Project/v14/knx/Capabilities_t.h>
#include <Project/v14/knx/IDREF.h>
#include <Project/xs/Boolean.h>
#include <Project/xs/String.h>

namespace Project {
namespace v14 {
namespace knx {

class ApplicationProgramStatic_Extension_t : public Base
{
    Q_OBJECT

public:
    explicit ApplicationProgramStatic_Extension_t(Base * parent = nullptr);
    virtual ~ApplicationProgramStatic_Extension_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::String EtsDownloadPlugin{};
    xs::String EtsUiPlugin{};
    xs::String EtsDataHandler{};
    Capabilities_t EtsDataHandlerCapabilities{};
    xs::Boolean RequiresExternalSoftware{"false"};

    /* elements */
    QMap<IDREF, ApplicationProgramStatic_Extension_Baggage_t *> Baggage; // key: RefId
};

ApplicationProgramStatic_Extension_t * make_ApplicationProgramStatic_Extension_t(Base * parent);

} // namespace knx
} // namespace v14
} // namespace Project
