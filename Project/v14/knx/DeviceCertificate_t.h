/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v14/knx/Aes128Key_t.h>
#include <Project/xs/Base64Binary.h>

namespace Project {
namespace v14 {
namespace knx {

class DeviceCertificate_t : public Base
{
    Q_OBJECT

public:
    explicit DeviceCertificate_t(Base * parent = nullptr);
    virtual ~DeviceCertificate_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::Base64Binary SerialNumber{};
    Aes128Key_t FDSK{};
};

DeviceCertificate_t * make_DeviceCertificate_t(Base * parent);

} // namespace knx
} // namespace v14
} // namespace Project
