/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v14/knx/ComObjectParameterBlock_Columns_Column_Width_t.h>
#include <Project/v14/knx/IDREF.h>
#include <Project/v14/knx/LanguageDependentString255_t.h>
#include <Project/v14/knx/String50_t.h>
#include <Project/xs/ID.h>
#include <Project/xs/String.h>

namespace Project {
namespace v14 {
namespace knx {

/* forward declarations */
class ParameterRef_t;

class ComObjectParameterBlock_Columns_Column_t : public Base
{
    Q_OBJECT

public:
    explicit ComObjectParameterBlock_Columns_Column_t(Base * parent = nullptr);
    virtual ~ComObjectParameterBlock_Columns_Column_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::ID Id{};
    String50_t Name{};
    LanguageDependentString255_t Text{};
    IDREF TextParameterRefId{};
    ComObjectParameterBlock_Columns_Column_Width_t Width{};
    xs::String InternalDescription{};

    /* getters */
    ParameterRef_t * getTextParameterRef() const; // attribute: TextParameterRefId
};

ComObjectParameterBlock_Columns_Column_t * make_ComObjectParameterBlock_Columns_Column_t(Base * parent);

} // namespace knx
} // namespace v14
} // namespace Project
