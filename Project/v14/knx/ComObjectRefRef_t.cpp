/* This file is generated. */

#include <Project/v14/knx/ComObjectRefRef_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v14/knx/ComObjectRef_t.h>
#include <Project/v14/knx/KNX_t.h>

namespace Project {
namespace v14 {
namespace knx {

ComObjectRefRef_t::ComObjectRefRef_t(Base * parent) :
    Base(parent)
{
}

ComObjectRefRef_t::~ComObjectRefRef_t()
{
}

void ComObjectRefRef_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("RefId")) {
            RefId = attribute.value().toString();
            continue;
        }
        if (name == QString("InternalDescription")) {
            InternalDescription = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ComObjectRefRef_t::tableColumnCount() const
{
    return 3;
}

QVariant ComObjectRefRef_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ComObjectRefRef";
        }
        if (qualifiedName == QString("RefId")) {
            return RefId;
        }
        if (qualifiedName == QString("InternalDescription")) {
            return InternalDescription;
        }
        break;
    }
    return QVariant();
}

QVariant ComObjectRefRef_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "RefId";
            case 2:
                return "InternalDescription";
            }
        }
    }
    return QVariant();
}

QVariant ComObjectRefRef_t::treeData(int role) const
{
    auto * ref = getComObjectRef();
    if (ref) {
        return ref->treeData(role);
    }

    switch (role) {
    case Qt::DisplayRole:
        return "ComObjectRefRef";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ComObjectRefRef");
    }
    return QVariant();
}

ComObjectRef_t * ComObjectRefRef_t::getComObjectRef() const
{
    if (RefId.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<ComObjectRef_t *>(knx->ids[RefId]);
}

ComObjectRefRef_t * make_ComObjectRefRef_t(Base * parent)
{
    return new ComObjectRefRef_t(parent);
}

} // namespace knx
} // namespace v14
} // namespace Project
