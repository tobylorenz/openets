/* This file is generated. */

#include <Project/v14/knx/ParameterRef_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v14/knx/KNX_t.h>
#include <Project/v14/knx/ParameterRef_t.h>

namespace Project {
namespace v14 {
namespace knx {

ParameterRef_t::ParameterRef_t(Base * parent) :
    Base(parent)
{
}

ParameterRef_t::~ParameterRef_t()
{
}

void ParameterRef_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == "Id") {
            Id = attribute.value().toString();
            KNX_t * knx = findParent<KNX_t *>();
            Q_ASSERT(knx);
            knx->ids[Id] = this;
            continue;
        }
        if (name == QString("RefId")) {
            RefId = attribute.value().toString();
            continue;
        }
        if (name == QString("Name")) {
            Name = attribute.value().toString();
            continue;
        }
        if (name == QString("Text")) {
            Text = attribute.value().toString();
            continue;
        }
        if (name == QString("SuffixText")) {
            SuffixText = attribute.value().toString();
            continue;
        }
        if (name == QString("Tag")) {
            Tag = attribute.value().toString();
            continue;
        }
        if (name == QString("DisplayOrder")) {
            DisplayOrder = attribute.value().toString();
            continue;
        }
        if (name == QString("Access")) {
            Access = attribute.value().toString();
            continue;
        }
        if (name == QString("Value")) {
            Value = attribute.value().toString();
            continue;
        }
        if (name == QString("InitialValue")) {
            InitialValue = attribute.value().toString();
            continue;
        }
        if (name == QString("CustomerAdjustable")) {
            CustomerAdjustable = attribute.value().toString();
            continue;
        }
        if (name == QString("TextParameterRefId")) {
            TextParameterRefId = attribute.value().toString();
            continue;
        }
        if (name == QString("InternalDescription")) {
            InternalDescription = attribute.value().toString();
            continue;
        }
        if (name == QString("ForbidGrantingUseByCustomer")) {
            ForbidGrantingUseByCustomer = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ParameterRef_t::tableColumnCount() const
{
    return 15;
}

QVariant ParameterRef_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ParameterRef";
        }
        if (qualifiedName == QString("Id")) {
            return Id;
        }
        if (qualifiedName == QString("RefId")) {
            return RefId;
        }
        if (qualifiedName == QString("Name")) {
            return Name;
        }
        if (qualifiedName == QString("Text")) {
            return Text;
        }
        if (qualifiedName == QString("SuffixText")) {
            return SuffixText;
        }
        if (qualifiedName == QString("Tag")) {
            return Tag;
        }
        if (qualifiedName == QString("DisplayOrder")) {
            return DisplayOrder;
        }
        if (qualifiedName == QString("Access")) {
            return Access;
        }
        if (qualifiedName == QString("Value")) {
            return Value;
        }
        if (qualifiedName == QString("InitialValue")) {
            return InitialValue;
        }
        if (qualifiedName == QString("CustomerAdjustable")) {
            return CustomerAdjustable;
        }
        if (qualifiedName == QString("TextParameterRefId")) {
            return TextParameterRefId;
        }
        if (qualifiedName == QString("InternalDescription")) {
            return InternalDescription;
        }
        if (qualifiedName == QString("ForbidGrantingUseByCustomer")) {
            return ForbidGrantingUseByCustomer;
        }
        break;
    }
    return QVariant();
}

QVariant ParameterRef_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Id";
            case 2:
                return "RefId";
            case 3:
                return "Name";
            case 4:
                return "Text";
            case 5:
                return "SuffixText";
            case 6:
                return "Tag";
            case 7:
                return "DisplayOrder";
            case 8:
                return "Access";
            case 9:
                return "Value";
            case 10:
                return "InitialValue";
            case 11:
                return "CustomerAdjustable";
            case 12:
                return "TextParameterRefId";
            case 13:
                return "InternalDescription";
            case 14:
                return "ForbidGrantingUseByCustomer";
            }
        }
    }
    return QVariant();
}

QVariant ParameterRef_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
    {
        QString uniqueNumber = Id.mid(Id.lastIndexOf("_R-") + 3);
        QStringList differences;
        if (!Name.isEmpty()) {
            differences.append("Name");
        }
        if (!Text.isEmpty()) {
            differences.append("Text");
        }
#if PROJECT_VERSION >= 11
        if (!SuffixText.isEmpty()) {
            differences.append("SuffixText");
        }
#endif
        if (!Access.isEmpty()) {
            differences.append("Access");
        }
        if (!Value.isEmpty()) {
            differences.append("Value");
        }
#if PROJECT_VERSION >= 12
        if (!InternalDescription.isEmpty()) {
            differences.append("InternalDescription");
        }
#endif
        return QString("[%1] %2 (%3)").arg(uniqueNumber).arg(Name).arg(differences.join(", "));
    }
    case Qt::DecorationRole:
        return QIcon::fromTheme("ParameterRef");
    }
    return QVariant();
}

Base * ParameterRef_t::getParameter() const
{
    if (RefId.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<Base *>(knx->ids[RefId]);
}

ParameterRef_t * ParameterRef_t::getTextParameterRef() const
{
    if (TextParameterRefId.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<ParameterRef_t *>(knx->ids[TextParameterRefId]);
}

ParameterRef_t * make_ParameterRef_t(Base * parent)
{
    return new ParameterRef_t(parent);
}

} // namespace knx
} // namespace v14
} // namespace Project
