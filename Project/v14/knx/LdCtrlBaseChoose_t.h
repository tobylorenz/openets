/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v14/knx/IDREF.h>
#include <Project/v14/knx/LdCtrlBaseChoose_when_t.h>
#include <Project/xs/String.h>

namespace Project {
namespace v14 {
namespace knx {

/* forward declarations */
class ParameterRef_t;

class LdCtrlBaseChoose_t : public Base
{
    Q_OBJECT

public:
    explicit LdCtrlBaseChoose_t(Base * parent = nullptr);
    virtual ~LdCtrlBaseChoose_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    IDREF ParamRefId{};
    xs::String InternalDescription{};

    /* elements */
    QVector<LdCtrlBaseChoose_when_t *> When;

    /* getters */
    ParameterRef_t * getParameterRef() const; // attribute: ParamRefId
};

LdCtrlBaseChoose_t * make_LdCtrlBaseChoose_t(Base * parent);

} // namespace knx
} // namespace v14
} // namespace Project
