/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v14/knx/DeviceInstance_AdditionalAddresses_Address_Address_t.h>
#include <Project/v14/knx/String255_t.h>
#include <Project/xs/String.h>

namespace Project {
namespace v14 {
namespace knx {

class DeviceInstance_AdditionalAddresses_Address_t : public Base
{
    Q_OBJECT

public:
    explicit DeviceInstance_AdditionalAddresses_Address_t(Base * parent = nullptr);
    virtual ~DeviceInstance_AdditionalAddresses_Address_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    DeviceInstance_AdditionalAddresses_Address_Address_t Address{};
    String255_t Name{};
    xs::String Description{};
    xs::String Comment{};
};

DeviceInstance_AdditionalAddresses_Address_t * make_DeviceInstance_AdditionalAddresses_Address_t(Base * parent);

} // namespace knx
} // namespace v14
} // namespace Project
