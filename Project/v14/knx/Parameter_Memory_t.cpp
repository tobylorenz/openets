/* This file is generated. */

#include <Project/v14/knx/Parameter_Memory_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v14/knx/ApplicationProgramStatic_Code_AbsoluteSegment_t.h>
#include <Project/v14/knx/KNX_t.h>

namespace Project {
namespace v14 {
namespace knx {

Parameter_Memory_t::Parameter_Memory_t(Base * parent) :
    Base(parent)
{
}

Parameter_Memory_t::~Parameter_Memory_t()
{
}

void Parameter_Memory_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("CodeSegment")) {
            CodeSegment = attribute.value().toString();
            continue;
        }
        if (name == QString("Offset")) {
            Offset = attribute.value().toString();
            continue;
        }
        if (name == QString("BitOffset")) {
            BitOffset = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int Parameter_Memory_t::tableColumnCount() const
{
    return 4;
}

QVariant Parameter_Memory_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "Parameter_Memory";
        }
        if (qualifiedName == QString("CodeSegment")) {
            return CodeSegment;
        }
        if (qualifiedName == QString("Offset")) {
            return Offset;
        }
        if (qualifiedName == QString("BitOffset")) {
            return BitOffset;
        }
        break;
    }
    return QVariant();
}

QVariant Parameter_Memory_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "CodeSegment";
            case 2:
                return "Offset";
            case 3:
                return "BitOffset";
            }
        }
    }
    return QVariant();
}

QVariant Parameter_Memory_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
    {
        QString address;
        ApplicationProgramStatic_Code_AbsoluteSegment_t * codeSegment = getCodeSegment();
        if (codeSegment) {
            address = QString(codeSegment->UserMemory == "false" ? "[%1]" : "[U%1]").arg(codeSegment->Address);
        }
        return QString("%1+%2.%3").arg(address).arg(Offset).arg(BitOffset);
    }
    case Qt::DecorationRole:
        return QIcon::fromTheme("Parameter_Memory");
    }
    return QVariant();
}

ApplicationProgramStatic_Code_AbsoluteSegment_t * Parameter_Memory_t::getCodeSegment() const
{
    if (CodeSegment.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<ApplicationProgramStatic_Code_AbsoluteSegment_t *>(knx->ids[CodeSegment]);
}

Parameter_Memory_t * make_Parameter_Memory_t(Base * parent)
{
    return new Parameter_Memory_t(parent);
}

} // namespace knx
} // namespace v14
} // namespace Project
