/* This file is generated. */

#include <Project/v14/knx/LdCtrlRestart_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v14/knx/KNX_t.h>

namespace Project {
namespace v14 {
namespace knx {

LdCtrlRestart_t::LdCtrlRestart_t(Base * parent) :
    Base(parent)
{
}

LdCtrlRestart_t::~LdCtrlRestart_t()
{
}

void LdCtrlRestart_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("AppliesTo")) {
            AppliesTo = attribute.value().toString();
            continue;
        }
        if (name == QString("InternalDescription")) {
            InternalDescription = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("OnError")) {
            auto * newOnError = make_LdCtrlBase_OnError_t(this);
            newOnError->read(reader);
            OnError.append(newOnError);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int LdCtrlRestart_t::tableColumnCount() const
{
    return 3;
}

QVariant LdCtrlRestart_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "LdCtrlRestart";
        }
        if (qualifiedName == QString("AppliesTo")) {
            return AppliesTo;
        }
        if (qualifiedName == QString("InternalDescription")) {
            return InternalDescription;
        }
        break;
    }
    return QVariant();
}

QVariant LdCtrlRestart_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "AppliesTo";
            case 2:
                return "InternalDescription";
            }
        }
    }
    return QVariant();
}

QVariant LdCtrlRestart_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "LdCtrlRestart";
    case Qt::DecorationRole:
        return QIcon::fromTheme("LdCtrlRestart");
    }
    return QVariant();
}

LdCtrlRestart_t * make_LdCtrlRestart_t(Base * parent)
{
    return new LdCtrlRestart_t(parent);
}

} // namespace knx
} // namespace v14
} // namespace Project
