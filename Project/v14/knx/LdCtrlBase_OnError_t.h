/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v14/knx/IDREF.h>
#include <Project/v14/knx/LdCtrlErrorCause_t.h>
#include <Project/xs/Boolean.h>

namespace Project {
namespace v14 {
namespace knx {

class LdCtrlBase_OnError_t : public Base
{
    Q_OBJECT

public:
    explicit LdCtrlBase_OnError_t(Base * parent = nullptr);
    virtual ~LdCtrlBase_OnError_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    LdCtrlErrorCause_t Cause{};
    xs::Boolean Ignore{"false"};
    IDREF MessageRef{};

    /* getters */
    Base * getMessage() const; // attribute: MessageRef
};

LdCtrlBase_OnError_t * make_LdCtrlBase_OnError_t(Base * parent);

} // namespace knx
} // namespace v14
} // namespace Project
