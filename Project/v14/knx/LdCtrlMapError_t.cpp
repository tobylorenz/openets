/* This file is generated. */

#include <Project/v14/knx/LdCtrlMapError_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v14/knx/KNX_t.h>

namespace Project {
namespace v14 {
namespace knx {

LdCtrlMapError_t::LdCtrlMapError_t(Base * parent) :
    Base(parent)
{
}

LdCtrlMapError_t::~LdCtrlMapError_t()
{
}

void LdCtrlMapError_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("AppliesTo")) {
            AppliesTo = attribute.value().toString();
            continue;
        }
        if (name == QString("InternalDescription")) {
            InternalDescription = attribute.value().toString();
            continue;
        }
        if (name == QString("LdCtrlFilter")) {
            LdCtrlFilter = attribute.value().toString();
            continue;
        }
        if (name == QString("OriginalError")) {
            OriginalError = attribute.value().toString();
            continue;
        }
        if (name == QString("MappedError")) {
            MappedError = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("OnError")) {
            auto * newOnError = make_LdCtrlBase_OnError_t(this);
            newOnError->read(reader);
            OnError.append(newOnError);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int LdCtrlMapError_t::tableColumnCount() const
{
    return 6;
}

QVariant LdCtrlMapError_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "LdCtrlMapError";
        }
        if (qualifiedName == QString("AppliesTo")) {
            return AppliesTo;
        }
        if (qualifiedName == QString("InternalDescription")) {
            return InternalDescription;
        }
        if (qualifiedName == QString("LdCtrlFilter")) {
            return LdCtrlFilter;
        }
        if (qualifiedName == QString("OriginalError")) {
            return OriginalError;
        }
        if (qualifiedName == QString("MappedError")) {
            return MappedError;
        }
        break;
    }
    return QVariant();
}

QVariant LdCtrlMapError_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "AppliesTo";
            case 2:
                return "InternalDescription";
            case 3:
                return "LdCtrlFilter";
            case 4:
                return "OriginalError";
            case 5:
                return "MappedError";
            }
        }
    }
    return QVariant();
}

QVariant LdCtrlMapError_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "LdCtrlMapError";
    case Qt::DecorationRole:
        return QIcon::fromTheme("LdCtrlMapError");
    }
    return QVariant();
}

LdCtrlMapError_t * make_LdCtrlMapError_t(Base * parent)
{
    return new LdCtrlMapError_t(parent);
}

} // namespace knx
} // namespace v14
} // namespace Project
