/* This file is generated. */

#include <Project/v14/knx/Button_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v14/knx/KNX_t.h>

namespace Project {
namespace v14 {
namespace knx {

Button_t::Button_t(Base * parent) :
    Base(parent)
{
}

Button_t::~Button_t()
{
}

void Button_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == "Id") {
            Id = attribute.value().toString();
            KNX_t * knx = findParent<KNX_t *>();
            Q_ASSERT(knx);
            knx->ids[Id] = this;
            continue;
        }
        if (name == QString("Text")) {
            Text = attribute.value().toString();
            continue;
        }
        if (name == QString("Access")) {
            Access = attribute.value().toString();
            continue;
        }
        if (name == QString("TextParameterRefId")) {
            QString value = attribute.value().toString();
            if (!value.isEmpty())
                qWarning() << "knx:Button_t attribute TextParameterRefId references to" << value;
        }
        if (name == QString("TextParameterRefId")) {
            TextParameterRefId = attribute.value().toString();
            continue;
        }
        if (name == QString("InternalDescription")) {
            InternalDescription = attribute.value().toString();
            continue;
        }
        if (name == QString("Cell")) {
            Cell = attribute.value().toString();
            continue;
        }
        if (name == QString("Icon")) {
            Icon = attribute.value().toString();
            continue;
        }
        if (name == QString("EventHandler")) {
            EventHandler = attribute.value().toString();
            continue;
        }
        if (name == QString("EventHandlerParameters")) {
            EventHandlerParameters = attribute.value().toString();
            continue;
        }
        if (name == QString("EventHandlerOnline")) {
            EventHandlerOnline = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int Button_t::tableColumnCount() const
{
    return 11;
}

QVariant Button_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "Button";
        }
        if (qualifiedName == QString("Id")) {
            return Id;
        }
        if (qualifiedName == QString("Text")) {
            return Text;
        }
        if (qualifiedName == QString("Access")) {
            return Access;
        }
        if (qualifiedName == QString("TextParameterRefId")) {
            return TextParameterRefId;
        }
        if (qualifiedName == QString("InternalDescription")) {
            return InternalDescription;
        }
        if (qualifiedName == QString("Cell")) {
            return Cell;
        }
        if (qualifiedName == QString("Icon")) {
            return Icon;
        }
        if (qualifiedName == QString("EventHandler")) {
            return EventHandler;
        }
        if (qualifiedName == QString("EventHandlerParameters")) {
            return EventHandlerParameters;
        }
        if (qualifiedName == QString("EventHandlerOnline")) {
            return EventHandlerOnline;
        }
        break;
    }
    return QVariant();
}

QVariant Button_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Id";
            case 2:
                return "Text";
            case 3:
                return "Access";
            case 4:
                return "TextParameterRefId";
            case 5:
                return "InternalDescription";
            case 6:
                return "Cell";
            case 7:
                return "Icon";
            case 8:
                return "EventHandler";
            case 9:
                return "EventHandlerParameters";
            case 10:
                return "EventHandlerOnline";
            }
        }
    }
    return QVariant();
}

QVariant Button_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "Button";
    case Qt::DecorationRole:
        return QIcon::fromTheme("Button");
    }
    return QVariant();
}

Base * Button_t::getTextParameter() const
{
    if (TextParameterRefId.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<Base *>(knx->ids[TextParameterRefId]);
}

Button_t * make_Button_t(Base * parent)
{
    return new Button_t(parent);
}

} // namespace knx
} // namespace v14
} // namespace Project
