/* This file is generated. */

#include <Project/v14/knx/Space_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v14/knx/KNX_t.h>
#include <Project/v14/knx/SpaceUsage_t.h>

namespace Project {
namespace v14 {
namespace knx {

Space_t::Space_t(Base * parent) :
    Base(parent)
{
}

Space_t::~Space_t()
{
}

void Space_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == "Id") {
            Id = attribute.value().toString();
            KNX_t * knx = findParent<KNX_t *>();
            Q_ASSERT(knx);
            knx->ids[Id] = this;
            continue;
        }
        if (name == QString("Name")) {
            Name = attribute.value().toString();
            continue;
        }
        if (name == QString("Type")) {
            Type = attribute.value().toString();
            continue;
        }
        if (name == QString("Usage")) {
            Usage = attribute.value().toString();
            continue;
        }
        if (name == QString("Number")) {
            Number = attribute.value().toString();
            continue;
        }
        if (name == QString("Comment")) {
            Comment = attribute.value().toString();
            continue;
        }
        if (name == QString("Description")) {
            Description = attribute.value().toString();
            continue;
        }
        if (name == QString("CompletionStatus")) {
            CompletionStatus = attribute.value().toString();
            continue;
        }
        if (name == QString("DefaultLine")) {
            DefaultLine = attribute.value().toString();
            continue;
        }
        if (name == QString("Puid")) {
            Puid = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("Space")) {
            QString newId = reader.attributes().value("Id").toString();
            Q_ASSERT(!newId.isEmpty());
            Space_t * newSpace;
            if (Space.contains(newId)) {
                newSpace = Space[newId];
            } else {
                newSpace = make_Space_t(this);
                Space[newId] = newSpace;
            }
            newSpace->read(reader);
            continue;
        }
        if (reader.name() == QString("DeviceInstanceRef")) {
            QString newRefId = reader.attributes().value("RefId").toString();
            Q_ASSERT(!newRefId.isEmpty());
            DeviceInstanceRef_t * newDeviceInstanceRef;
            if (DeviceInstanceRef.contains(newRefId)) {
                newDeviceInstanceRef = DeviceInstanceRef[newRefId];
            } else {
                newDeviceInstanceRef = make_DeviceInstanceRef_t(this);
                DeviceInstanceRef[newRefId] = newDeviceInstanceRef;
            }
            newDeviceInstanceRef->read(reader);
            continue;
        }
        if (reader.name() == QString("Function")) {
            QString newId = reader.attributes().value("Id").toString();
            Q_ASSERT(!newId.isEmpty());
            Function_t * newFunction;
            if (Function.contains(newId)) {
                newFunction = Function[newId];
            } else {
                newFunction = make_Function_t(this);
                Function[newId] = newFunction;
            }
            newFunction->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int Space_t::tableColumnCount() const
{
    return 11;
}

QVariant Space_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "Space";
        }
        if (qualifiedName == QString("Id")) {
            return Id;
        }
        if (qualifiedName == QString("Name")) {
            return Name;
        }
        if (qualifiedName == QString("Type")) {
            return Type;
        }
        if (qualifiedName == QString("Usage")) {
            return Usage;
        }
        if (qualifiedName == QString("Number")) {
            return Number;
        }
        if (qualifiedName == QString("Comment")) {
            return Comment;
        }
        if (qualifiedName == QString("Description")) {
            return Description;
        }
        if (qualifiedName == QString("CompletionStatus")) {
            return CompletionStatus;
        }
        if (qualifiedName == QString("DefaultLine")) {
            return DefaultLine;
        }
        if (qualifiedName == QString("Puid")) {
            return Puid;
        }
        break;
    }
    return QVariant();
}

QVariant Space_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Id";
            case 2:
                return "Name";
            case 3:
                return "Type";
            case 4:
                return "Usage";
            case 5:
                return "Number";
            case 6:
                return "Comment";
            case 7:
                return "Description";
            case 8:
                return "CompletionStatus";
            case 9:
                return "DefaultLine";
            case 10:
                return "Puid";
            }
        }
    }
    return QVariant();
}

QVariant Space_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return !Name.isEmpty() ? Name : "Space";
    case Qt::DecorationRole:
        if (!Usage.isEmpty()) {
            return getUsage()->treeData(Qt::DecorationRole);
        } else {
            return QIcon::fromTheme(QString("Space_%1").arg(Type));
        }
    }
    return QVariant();
}

SpaceUsage_t * Space_t::getUsage() const
{
    if (Usage.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<SpaceUsage_t *>(knx->ids[Usage]);
}

Space_t * make_Space_t(Base * parent)
{
    return new Space_t(parent);
}

} // namespace knx
} // namespace v14
} // namespace Project
