/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v14/knx/LdCtrlBase_OnError_t.h>
#include <Project/v14/knx/LdCtrlProcType_t.h>
#include <Project/xs/String.h>
#include <Project/xs/UnsignedByte.h>

namespace Project {
namespace v14 {
namespace knx {

class LdCtrlMerge_t : public Base
{
    Q_OBJECT

public:
    explicit LdCtrlMerge_t(Base * parent = nullptr);
    virtual ~LdCtrlMerge_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    LdCtrlProcType_t AppliesTo{"auto"};
    xs::String InternalDescription{};
    xs::UnsignedByte MergeId{};

    /* elements */
    QVector<LdCtrlBase_OnError_t *> OnError;
};

LdCtrlMerge_t * make_LdCtrlMerge_t(Base * parent);

} // namespace knx
} // namespace v14
} // namespace Project
