/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/xs/ID.h>
#include <Project/xs/Boolean.h>
#include <Project/xs/UnsignedInt.h>

namespace Project {
namespace v14 {
namespace knx {

/* forward declarations */
class MasterData_Manufacturers_Manufacturer_PublicKeys_PublicKey_RSAKeyValue_t;

class MasterData_Manufacturers_Manufacturer_PublicKeys_PublicKey_t : public Base
{
    Q_OBJECT

public:
    explicit MasterData_Manufacturers_Manufacturer_PublicKeys_PublicKey_t(Base * parent = nullptr);
    virtual ~MasterData_Manufacturers_Manufacturer_PublicKeys_PublicKey_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::ID Id{};
    xs::UnsignedInt Number{};
    xs::Boolean Revoked{"false"};

    /* elements */
    MasterData_Manufacturers_Manufacturer_PublicKeys_PublicKey_RSAKeyValue_t * RSAKeyValue{};
};

MasterData_Manufacturers_Manufacturer_PublicKeys_PublicKey_t * make_MasterData_Manufacturers_Manufacturer_PublicKeys_PublicKey_t(Base * parent);

} // namespace knx
} // namespace v14
} // namespace Project
