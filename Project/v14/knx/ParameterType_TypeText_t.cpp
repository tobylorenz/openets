/* This file is generated. */

#include <Project/v14/knx/ParameterType_TypeText_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v14/knx/KNX_t.h>

namespace Project {
namespace v14 {
namespace knx {

ParameterType_TypeText_t::ParameterType_TypeText_t(Base * parent) :
    Base(parent)
{
}

ParameterType_TypeText_t::~ParameterType_TypeText_t()
{
}

void ParameterType_TypeText_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("SizeInBit")) {
            SizeInBit = attribute.value().toString();
            continue;
        }
        if (name == QString("Pattern")) {
            Pattern = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ParameterType_TypeText_t::tableColumnCount() const
{
    return 3;
}

QVariant ParameterType_TypeText_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ParameterType_TypeText";
        }
        if (qualifiedName == QString("SizeInBit")) {
            return SizeInBit;
        }
        if (qualifiedName == QString("Pattern")) {
            return Pattern;
        }
        break;
    }
    return QVariant();
}

QVariant ParameterType_TypeText_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "SizeInBit";
            case 2:
                return "Pattern";
            }
        }
    }
    return QVariant();
}

QVariant ParameterType_TypeText_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "TypeText";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ParameterType_TypeText");
    }
    return QVariant();
}

ParameterType_TypeText_t * make_ParameterType_TypeText_t(Base * parent)
{
    return new ParameterType_TypeText_t(parent);
}

} // namespace knx
} // namespace v14
} // namespace Project
