/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v14/knx/DeprecationStatus_t.h>
#include <Project/v14/knx/String255_t.h>
#include <Project/xs/ID.h>
#include <Project/xs/String.h>
#include <Project/xs/UnsignedInt.h>

namespace Project {
namespace v14 {
namespace knx {

class DatapointRole_t : public Base
{
    Q_OBJECT

public:
    explicit DatapointRole_t(Base * parent = nullptr);
    virtual ~DatapointRole_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::ID Id{};
    xs::UnsignedInt Number{};
    String255_t Name{};
    xs::String Description{};
    DeprecationStatus_t Status{"active"};
};

DatapointRole_t * make_DatapointRole_t(Base * parent);

} // namespace knx
} // namespace v14
} // namespace Project
