/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v14/knx/Hardware_Products_Product_Attributes_Attribute_t.h>

namespace Project {
namespace v14 {
namespace knx {

class Hardware_Products_Product_Attributes_t : public Base
{
    Q_OBJECT

public:
    explicit Hardware_Products_Product_Attributes_t(Base * parent = nullptr);
    virtual ~Hardware_Products_Product_Attributes_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* elements */
    QVector<Hardware_Products_Product_Attributes_Attribute_t *> Attribute;
};

Hardware_Products_Product_Attributes_t * make_Hardware_Products_Product_Attributes_t(Base * parent);

} // namespace knx
} // namespace v14
} // namespace Project
