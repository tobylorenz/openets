/* This file is generated. */

#pragma once

#include <Project/xs/String.h>

namespace Project {
namespace v14 {
namespace knx {

using Ipv4Address_t = xs::String;

} // namespace knx
} // namespace v14
} // namespace Project
