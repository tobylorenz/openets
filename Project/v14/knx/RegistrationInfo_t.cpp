/* This file is generated. */

#include <Project/v14/knx/RegistrationInfo_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v14/knx/KNX_t.h>

namespace Project {
namespace v14 {
namespace knx {

RegistrationInfo_t::RegistrationInfo_t(Base * parent) :
    Base(parent)
{
}

RegistrationInfo_t::~RegistrationInfo_t()
{
}

void RegistrationInfo_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("RegistrationStatus")) {
            RegistrationStatus = attribute.value().toString();
            continue;
        }
        if (name == QString("RegistrationNumber")) {
            RegistrationNumber = attribute.value().toString();
            continue;
        }
        if (name == QString("OriginalRegistrationNumber")) {
            OriginalRegistrationNumber = attribute.value().toString();
            continue;
        }
        if (name == QString("RegistrationDate")) {
            RegistrationDate = attribute.value().toString();
            continue;
        }
        if (name == QString("RegistrationSignature")) {
            RegistrationSignature = attribute.value().toString();
            continue;
        }
        if (name == QString("RegistrationKey")) {
            RegistrationKey = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int RegistrationInfo_t::tableColumnCount() const
{
    return 7;
}

QVariant RegistrationInfo_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "RegistrationInfo";
        }
        if (qualifiedName == QString("RegistrationStatus")) {
            return RegistrationStatus;
        }
        if (qualifiedName == QString("RegistrationNumber")) {
            return RegistrationNumber;
        }
        if (qualifiedName == QString("OriginalRegistrationNumber")) {
            return OriginalRegistrationNumber;
        }
        if (qualifiedName == QString("RegistrationDate")) {
            return RegistrationDate;
        }
        if (qualifiedName == QString("RegistrationSignature")) {
            return RegistrationSignature;
        }
        if (qualifiedName == QString("RegistrationKey")) {
            return RegistrationKey;
        }
        break;
    }
    return QVariant();
}

QVariant RegistrationInfo_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "RegistrationStatus";
            case 2:
                return "RegistrationNumber";
            case 3:
                return "OriginalRegistrationNumber";
            case 4:
                return "RegistrationDate";
            case 5:
                return "RegistrationSignature";
            case 6:
                return "RegistrationKey";
            }
        }
    }
    return QVariant();
}

QVariant RegistrationInfo_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "RegistrationInfo";
    case Qt::DecorationRole:
        return QIcon::fromTheme("RegistrationInfo");
    }
    return QVariant();
}

RegistrationInfo_t * make_RegistrationInfo_t(Base * parent)
{
    return new RegistrationInfo_t(parent);
}

} // namespace knx
} // namespace v14
} // namespace Project
