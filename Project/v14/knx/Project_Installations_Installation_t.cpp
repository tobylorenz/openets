/* This file is generated. */

#include <Project/v14/knx/Project_Installations_Installation_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v14/knx/BusAccess_t.h>
#include <Project/v14/knx/GroupAddresses_t.h>
#include <Project/v14/knx/KNX_t.h>
#include <Project/v14/knx/Locations_t.h>
#include <Project/v14/knx/SplitInfos_t.h>
#include <Project/v14/knx/Topology_t.h>
#include <Project/v14/knx/Trades_t.h>

namespace Project {
namespace v14 {
namespace knx {

Project_Installations_Installation_t::Project_Installations_Installation_t(Base * parent) :
    Base(parent)
{
}

Project_Installations_Installation_t::~Project_Installations_Installation_t()
{
}

void Project_Installations_Installation_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("Name")) {
            Name = attribute.value().toString();
            continue;
        }
        if (name == QString("InstallationId")) {
            InstallationId = attribute.value().toString();
            continue;
        }
        if (name == QString("BCUKey")) {
            BCUKey = attribute.value().toString();
            continue;
        }
        if (name == QString("IPRoutingMulticastAddress")) {
            IPRoutingMulticastAddress = attribute.value().toString();
            continue;
        }
        if (name == QString("MulticastTTL")) {
            MulticastTTL = attribute.value().toString();
            continue;
        }
        if (name == QString("IPRoutingBackboneKey")) {
            IPRoutingBackboneKey = attribute.value().toString();
            continue;
        }
        if (name == QString("IPRoutingLatencyTolerance")) {
            IPRoutingLatencyTolerance = attribute.value().toString();
            continue;
        }
        if (name == QString("IPSyncLatencyFraction")) {
            IPSyncLatencyFraction = attribute.value().toString();
            continue;
        }
        if (name == QString("DefaultLine")) {
            DefaultLine = attribute.value().toString();
            continue;
        }
        if (name == QString("CompletionStatus")) {
            CompletionStatus = attribute.value().toString();
            continue;
        }
        if (name == QString("IPRoutingBackboneSecurity")) {
            IPRoutingBackboneSecurity = attribute.value().toString();
            continue;
        }
        if (name == QString("SplitType")) {
            SplitType = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("Topology")) {
            if (!Topology) {
                Topology = make_Topology_t(this);
            }
            Topology->read(reader);
            continue;
        }
        if (reader.name() == QString("Locations")) {
            if (!Locations) {
                Locations = make_Locations_t(this);
            }
            Locations->read(reader);
            continue;
        }
        if (reader.name() == QString("GroupAddresses")) {
            if (!GroupAddresses) {
                GroupAddresses = make_GroupAddresses_t(this);
            }
            GroupAddresses->read(reader);
            continue;
        }
        if (reader.name() == QString("Trades")) {
            if (!Trades) {
                Trades = make_Trades_t(this);
            }
            Trades->read(reader);
            continue;
        }
        if (reader.name() == QString("BusAccess")) {
            if (!BusAccess) {
                BusAccess = make_BusAccess_t(this);
            }
            BusAccess->read(reader);
            continue;
        }
        if (reader.name() == QString("SplitInfos")) {
            if (!SplitInfos) {
                SplitInfos = make_SplitInfos_t(this);
            }
            SplitInfos->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int Project_Installations_Installation_t::tableColumnCount() const
{
    return 13;
}

QVariant Project_Installations_Installation_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "Project_Installations_Installation";
        }
        if (qualifiedName == QString("Name")) {
            return Name;
        }
        if (qualifiedName == QString("InstallationId")) {
            return InstallationId;
        }
        if (qualifiedName == QString("BCUKey")) {
            return BCUKey;
        }
        if (qualifiedName == QString("IPRoutingMulticastAddress")) {
            return IPRoutingMulticastAddress;
        }
        if (qualifiedName == QString("MulticastTTL")) {
            return MulticastTTL;
        }
        if (qualifiedName == QString("IPRoutingBackboneKey")) {
            return IPRoutingBackboneKey;
        }
        if (qualifiedName == QString("IPRoutingLatencyTolerance")) {
            return IPRoutingLatencyTolerance;
        }
        if (qualifiedName == QString("IPSyncLatencyFraction")) {
            return IPSyncLatencyFraction;
        }
        if (qualifiedName == QString("DefaultLine")) {
            return DefaultLine;
        }
        if (qualifiedName == QString("CompletionStatus")) {
            return CompletionStatus;
        }
        if (qualifiedName == QString("IPRoutingBackboneSecurity")) {
            return IPRoutingBackboneSecurity;
        }
        if (qualifiedName == QString("SplitType")) {
            return SplitType;
        }
        break;
    }
    return QVariant();
}

QVariant Project_Installations_Installation_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Name";
            case 2:
                return "InstallationId";
            case 3:
                return "BCUKey";
            case 4:
                return "IPRoutingMulticastAddress";
            case 5:
                return "MulticastTTL";
            case 6:
                return "IPRoutingBackboneKey";
            case 7:
                return "IPRoutingLatencyTolerance";
            case 8:
                return "IPSyncLatencyFraction";
            case 9:
                return "DefaultLine";
            case 10:
                return "CompletionStatus";
            case 11:
                return "IPRoutingBackboneSecurity";
            case 12:
                return "SplitType";
            }
        }
    }
    return QVariant();
}

QVariant Project_Installations_Installation_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return !Name.isEmpty() ? Name : "Installation";
    case Qt::DecorationRole:
        return QIcon::fromTheme("Project_Installations_Installation");
    }
    return QVariant();
}

Project_Installations_Installation_t * make_Project_Installations_Installation_t(Base * parent)
{
    return new Project_Installations_Installation_t(parent);
}

} // namespace knx
} // namespace v14
} // namespace Project
