/* This file is generated. */

#include <Project/v14/knx/LdCtrlAbsSegment_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v14/knx/KNX_t.h>

namespace Project {
namespace v14 {
namespace knx {

LdCtrlAbsSegment_t::LdCtrlAbsSegment_t(Base * parent) :
    Base(parent)
{
}

LdCtrlAbsSegment_t::~LdCtrlAbsSegment_t()
{
}

void LdCtrlAbsSegment_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("AppliesTo")) {
            AppliesTo = attribute.value().toString();
            continue;
        }
        if (name == QString("InternalDescription")) {
            InternalDescription = attribute.value().toString();
            continue;
        }
        if (name == QString("LsmIdx")) {
            LsmIdx = attribute.value().toString();
            continue;
        }
        if (name == QString("ObjType")) {
            ObjType = attribute.value().toString();
            continue;
        }
        if (name == QString("Occurrence")) {
            Occurrence = attribute.value().toString();
            continue;
        }
        if (name == QString("SegType")) {
            SegType = attribute.value().toString();
            continue;
        }
        if (name == QString("Address")) {
            Address = attribute.value().toString();
            continue;
        }
        if (name == QString("Size")) {
            Size = attribute.value().toString();
            continue;
        }
        if (name == QString("Access")) {
            Access = attribute.value().toString();
            continue;
        }
        if (name == QString("MemType")) {
            MemType = attribute.value().toString();
            continue;
        }
        if (name == QString("SegFlags")) {
            SegFlags = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("OnError")) {
            auto * newOnError = make_LdCtrlBase_OnError_t(this);
            newOnError->read(reader);
            OnError.append(newOnError);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int LdCtrlAbsSegment_t::tableColumnCount() const
{
    return 12;
}

QVariant LdCtrlAbsSegment_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "LdCtrlAbsSegment";
        }
        if (qualifiedName == QString("AppliesTo")) {
            return AppliesTo;
        }
        if (qualifiedName == QString("InternalDescription")) {
            return InternalDescription;
        }
        if (qualifiedName == QString("LsmIdx")) {
            return LsmIdx;
        }
        if (qualifiedName == QString("ObjType")) {
            return ObjType;
        }
        if (qualifiedName == QString("Occurrence")) {
            return Occurrence;
        }
        if (qualifiedName == QString("SegType")) {
            return SegType;
        }
        if (qualifiedName == QString("Address")) {
            return Address;
        }
        if (qualifiedName == QString("Size")) {
            return Size;
        }
        if (qualifiedName == QString("Access")) {
            return Access;
        }
        if (qualifiedName == QString("MemType")) {
            return MemType;
        }
        if (qualifiedName == QString("SegFlags")) {
            return SegFlags;
        }
        break;
    }
    return QVariant();
}

QVariant LdCtrlAbsSegment_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "AppliesTo";
            case 2:
                return "InternalDescription";
            case 3:
                return "LsmIdx";
            case 4:
                return "ObjType";
            case 5:
                return "Occurrence";
            case 6:
                return "SegType";
            case 7:
                return "Address";
            case 8:
                return "Size";
            case 9:
                return "Access";
            case 10:
                return "MemType";
            case 11:
                return "SegFlags";
            }
        }
    }
    return QVariant();
}

QVariant LdCtrlAbsSegment_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "LdCtrlAbsSegment";
    case Qt::DecorationRole:
        return QIcon::fromTheme("LdCtrlAbsSegment");
    }
    return QVariant();
}

LdCtrlAbsSegment_t * make_LdCtrlAbsSegment_t(Base * parent)
{
    return new LdCtrlAbsSegment_t(parent);
}

} // namespace knx
} // namespace v14
} // namespace Project
