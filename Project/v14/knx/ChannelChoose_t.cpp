/* This file is generated. */

#include <Project/v14/knx/ChannelChoose_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v14/knx/KNX_t.h>
#include <Project/v14/knx/ParameterRef_t.h>

namespace Project {
namespace v14 {
namespace knx {

ChannelChoose_t::ChannelChoose_t(Base * parent) :
    Base(parent)
{
}

ChannelChoose_t::~ChannelChoose_t()
{
}

void ChannelChoose_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("ParamRefId")) {
            ParamRefId = attribute.value().toString();
            continue;
        }
        if (name == QString("InternalDescription")) {
            InternalDescription = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("when")) {
            auto * newWhen = make_ChannelChoose_when_t(this);
            newWhen->read(reader);
            When.append(newWhen);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ChannelChoose_t::tableColumnCount() const
{
    return 3;
}

QVariant ChannelChoose_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ChannelChoose";
        }
        if (qualifiedName == QString("ParamRefId")) {
            return ParamRefId;
        }
        if (qualifiedName == QString("InternalDescription")) {
            return InternalDescription;
        }
        break;
    }
    return QVariant();
}

QVariant ChannelChoose_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "ParamRefId";
            case 2:
                return "InternalDescription";
            }
        }
    }
    return QVariant();
}

QVariant ChannelChoose_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "ChannelChoose";
    case Qt::DecorationRole:
        return QIcon::fromTheme((When.count() == 1) && (When[0]->Default == "true") ? "ChannelChoose_Conditionless" : "ChannelChoose");
    }
    return QVariant();
}

ParameterRef_t * ChannelChoose_t::getParameterRef() const
{
    if (ParamRefId.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<ParameterRef_t *>(knx->ids[ParamRefId]);
}

ChannelChoose_t * make_ChannelChoose_t(Base * parent)
{
    return new ChannelChoose_t(parent);
}

} // namespace knx
} // namespace v14
} // namespace Project
