/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/xs/ID.h>

namespace Project {
namespace v14 {
namespace knx {

/* forward declarations */
class Project_AddinData_t;
class Project_Installations_t;
class Project_ProjectInformation_t;
class Project_UserFiles_t;

class Project_t : public Base
{
    Q_OBJECT

public:
    explicit Project_t(Base * parent = nullptr);
    virtual ~Project_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::ID Id{};

    /* elements */
    Project_ProjectInformation_t * ProjectInformation{};
    Project_Installations_t * Installations{};
    Project_UserFiles_t * UserFiles{};
    Project_AddinData_t * AddinData{};
};

Project_t * make_Project_t(Base * parent);

} // namespace knx
} // namespace v14
} // namespace Project
