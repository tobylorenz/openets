/* This file is generated. */

#include <Project/v14/knx/MasterData_MediumTypes_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v14/knx/KNX_t.h>

namespace Project {
namespace v14 {
namespace knx {

MasterData_MediumTypes_t::MasterData_MediumTypes_t(Base * parent) :
    Base(parent)
{
}

MasterData_MediumTypes_t::~MasterData_MediumTypes_t()
{
}

void MasterData_MediumTypes_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("MediumType")) {
            QString newId = reader.attributes().value("Id").toString();
            Q_ASSERT(!newId.isEmpty());
            MasterData_MediumTypes_MediumType_t * newMediumType;
            if (MediumType.contains(newId)) {
                newMediumType = MediumType[newId];
            } else {
                newMediumType = make_MasterData_MediumTypes_MediumType_t(this);
                MediumType[newId] = newMediumType;
            }
            newMediumType->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int MasterData_MediumTypes_t::tableColumnCount() const
{
    return 1;
}

QVariant MasterData_MediumTypes_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "MasterData_MediumTypes";
        }
        break;
    }
    return QVariant();
}

QVariant MasterData_MediumTypes_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant MasterData_MediumTypes_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "MediumTypes";
    case Qt::DecorationRole:
        return QIcon::fromTheme("MasterData_MediumTypes");
    }
    return QVariant();
}

MasterData_MediumTypes_t * make_MasterData_MediumTypes_t(Base * parent)
{
    return new MasterData_MediumTypes_t(parent);
}

} // namespace knx
} // namespace v14
} // namespace Project
