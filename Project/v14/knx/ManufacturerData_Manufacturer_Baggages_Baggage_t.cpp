/* This file is generated. */

#include <Project/v14/knx/ManufacturerData_Manufacturer_Baggages_Baggage_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v14/knx/KNX_t.h>
#include <Project/v14/knx/ManufacturerData_Manufacturer_Baggages_Baggage_FileInfo_t.h>

namespace Project {
namespace v14 {
namespace knx {

ManufacturerData_Manufacturer_Baggages_Baggage_t::ManufacturerData_Manufacturer_Baggages_Baggage_t(Base * parent) :
    Base(parent)
{
}

ManufacturerData_Manufacturer_Baggages_Baggage_t::~ManufacturerData_Manufacturer_Baggages_Baggage_t()
{
}

void ManufacturerData_Manufacturer_Baggages_Baggage_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("TargetPath")) {
            TargetPath = attribute.value().toString();
            continue;
        }
        if (name == QString("Name")) {
            Name = attribute.value().toString();
            continue;
        }
        if (name == QString("FileIntegrity")) {
            FileIntegrity = attribute.value().toString();
            continue;
        }
        if (name == "Id") {
            Id = attribute.value().toString();
            KNX_t * knx = findParent<KNX_t *>();
            Q_ASSERT(knx);
            knx->ids[Id] = this;
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("FileInfo")) {
            if (!FileInfo) {
                FileInfo = make_ManufacturerData_Manufacturer_Baggages_Baggage_FileInfo_t(this);
            }
            FileInfo->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ManufacturerData_Manufacturer_Baggages_Baggage_t::tableColumnCount() const
{
    return 5;
}

QVariant ManufacturerData_Manufacturer_Baggages_Baggage_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ManufacturerData_Manufacturer_Baggages_Baggage";
        }
        if (qualifiedName == QString("TargetPath")) {
            return TargetPath;
        }
        if (qualifiedName == QString("Name")) {
            return Name;
        }
        if (qualifiedName == QString("FileIntegrity")) {
            return FileIntegrity;
        }
        if (qualifiedName == QString("Id")) {
            return Id;
        }
        break;
    }
    return QVariant();
}

QVariant ManufacturerData_Manufacturer_Baggages_Baggage_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "TargetPath";
            case 2:
                return "Name";
            case 3:
                return "FileIntegrity";
            case 4:
                return "Id";
            }
        }
    }
    return QVariant();
}

QVariant ManufacturerData_Manufacturer_Baggages_Baggage_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return QString("%1\%2").arg(TargetPath).arg(Name);
    case Qt::DecorationRole:
        return QIcon::fromTheme("ManufacturerData_Manufacturer_Baggages_Baggage");
    }
    return QVariant();
}

ManufacturerData_Manufacturer_Baggages_Baggage_t * make_ManufacturerData_Manufacturer_Baggages_Baggage_t(Base * parent)
{
    return new ManufacturerData_Manufacturer_Baggages_Baggage_t(parent);
}

} // namespace knx
} // namespace v14
} // namespace Project
