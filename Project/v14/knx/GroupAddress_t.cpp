/* This file is generated. */

#include <Project/v14/knx/GroupAddress_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v14/knx/DatapointType_DatapointSubtypes_DatapointSubtype_t.h>
#include <Project/v14/knx/KNX_t.h>
#include <Project/v14/knx/Project_ProjectInformation_t.h>
#include <Project/v14/knx/Project_t.h>

namespace Project {
namespace v14 {
namespace knx {

GroupAddress_t::GroupAddress_t(Base * parent) :
    Base(parent)
{
}

GroupAddress_t::~GroupAddress_t()
{
}

void GroupAddress_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == "Id") {
            Id = attribute.value().toString();
            KNX_t * knx = findParent<KNX_t *>();
            Q_ASSERT(knx);
            knx->ids[Id] = this;
            continue;
        }
        if (name == QString("Address")) {
            Address = attribute.value().toString();
            continue;
        }
        if (name == QString("Name")) {
            Name = attribute.value().toString();
            continue;
        }
        if (name == QString("Unfiltered")) {
            Unfiltered = attribute.value().toString();
            continue;
        }
        if (name == QString("Central")) {
            Central = attribute.value().toString();
            continue;
        }
        if (name == QString("Global")) {
            Global = attribute.value().toString();
            continue;
        }
        if (name == QString("DatapointType")) {
            DatapointType = attribute.value().toString();
            continue;
        }
        if (name == QString("Description")) {
            Description = attribute.value().toString();
            continue;
        }
        if (name == QString("Comment")) {
            Comment = attribute.value().toString();
            continue;
        }
        if (name == QString("Puid")) {
            Puid = attribute.value().toString();
            continue;
        }
        if (name == QString("Key")) {
            Key = attribute.value().toString();
            continue;
        }
        if (name == QString("Security")) {
            Security = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int GroupAddress_t::tableColumnCount() const
{
    return 13;
}

QVariant GroupAddress_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "GroupAddress";
        }
        if (qualifiedName == QString("Id")) {
            return Id;
        }
        if (qualifiedName == QString("Address")) {
            return Address;
        }
        if (qualifiedName == QString("Name")) {
            return Name;
        }
        if (qualifiedName == QString("Unfiltered")) {
            return Unfiltered;
        }
        if (qualifiedName == QString("Central")) {
            return Central;
        }
        if (qualifiedName == QString("Global")) {
            return Global;
        }
        if (qualifiedName == QString("DatapointType")) {
            return DatapointType;
        }
        if (qualifiedName == QString("Description")) {
            return Description;
        }
        if (qualifiedName == QString("Comment")) {
            return Comment;
        }
        if (qualifiedName == QString("Puid")) {
            return Puid;
        }
        if (qualifiedName == QString("Key")) {
            return Key;
        }
        if (qualifiedName == QString("Security")) {
            return Security;
        }
        break;
    }
    return QVariant();
}

QVariant GroupAddress_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Id";
            case 2:
                return "Address";
            case 3:
                return "Name";
            case 4:
                return "Unfiltered";
            case 5:
                return "Central";
            case 6:
                return "Global";
            case 7:
                return "DatapointType";
            case 8:
                return "Description";
            case 9:
                return "Comment";
            case 10:
                return "Puid";
            case 11:
                return "Key";
            case 12:
                return "Security";
            }
        }
    }
    return QVariant();
}

QVariant GroupAddress_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return QString("%1 %2").arg(getStyledAddress()).arg(Name);
    case Qt::DecorationRole:
        return QIcon::fromTheme("GroupAddress");
    }
    return QVariant();
}

DatapointType_DatapointSubtypes_DatapointSubtype_t * GroupAddress_t::getDatapointType() const
{
    if (DatapointType.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<DatapointType_DatapointSubtypes_DatapointSubtype_t *>(knx->ids[DatapointType]);
}

QString GroupAddress_t::getStyledAddress() const {
    Project_t * project = findParent<Project_t *>();
    Q_ASSERT(project);
    Project_ProjectInformation_t * projectInformation = project->ProjectInformation;
    Q_ASSERT(projectInformation);
    QString groupAddressStyle = projectInformation->GroupAddressStyle;
    if (groupAddressStyle == "ThreeLevel") {
        // ThreeLevel: Main Group (M) 5 Bit / Middle Group (Mi) 3 Bit / Subgroup (G) 8 Bit
        int address = Address.toInt();
        const uint16_t main = (address >> 11) & 0x1f;
        const uint16_t middle = (address >> 8) & 0x07;
        const uint16_t sub = address & 0xff;
        return QString("%1/%2/%3").arg(main).arg(middle).arg(sub); // 5/3/8
    }
    if (groupAddressStyle == "TwoLevel") {
        // TwoLevel: Main Group (M) 5 Bit / Subgroup (G) 11 Bit
        int address = Address.toInt();
        const uint16_t main = (address >> 11) & 0x1f;
        const uint16_t sub = address & 0x07ff;
        return QString("%1/%2").arg(main).arg(sub);
    }
    // Free: 16 Bit
    return Address;
}

GroupAddress_t * make_GroupAddress_t(Base * parent)
{
    return new GroupAddress_t(parent);
}

} // namespace knx
} // namespace v14
} // namespace Project
