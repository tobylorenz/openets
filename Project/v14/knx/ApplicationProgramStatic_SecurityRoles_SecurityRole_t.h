/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v14/knx/LanguageDependentString255_t.h>
#include <Project/xs/ID.h>
#include <Project/xs/UnsignedShort.h>

namespace Project {
namespace v14 {
namespace knx {

class ApplicationProgramStatic_SecurityRoles_SecurityRole_t : public Base
{
    Q_OBJECT

public:
    explicit ApplicationProgramStatic_SecurityRoles_SecurityRole_t(Base * parent = nullptr);
    virtual ~ApplicationProgramStatic_SecurityRoles_SecurityRole_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::ID Id{};
    LanguageDependentString255_t Text{};
    xs::UnsignedShort Mask{};
};

ApplicationProgramStatic_SecurityRoles_SecurityRole_t * make_ApplicationProgramStatic_SecurityRoles_SecurityRole_t(Base * parent);

} // namespace knx
} // namespace v14
} // namespace Project
