/* This file is generated. */

#include <Project/v14/knx/LdCtrlBaseChoose_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v14/knx/KNX_t.h>
#include <Project/v14/knx/ParameterRef_t.h>

namespace Project {
namespace v14 {
namespace knx {

LdCtrlBaseChoose_t::LdCtrlBaseChoose_t(Base * parent) :
    Base(parent)
{
}

LdCtrlBaseChoose_t::~LdCtrlBaseChoose_t()
{
}

void LdCtrlBaseChoose_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("ParamRefId")) {
            ParamRefId = attribute.value().toString();
            continue;
        }
        if (name == QString("InternalDescription")) {
            InternalDescription = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("when")) {
            auto * newWhen = make_LdCtrlBaseChoose_when_t(this);
            newWhen->read(reader);
            When.append(newWhen);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int LdCtrlBaseChoose_t::tableColumnCount() const
{
    return 3;
}

QVariant LdCtrlBaseChoose_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "LdCtrlBaseChoose";
        }
        if (qualifiedName == QString("ParamRefId")) {
            return ParamRefId;
        }
        if (qualifiedName == QString("InternalDescription")) {
            return InternalDescription;
        }
        break;
    }
    return QVariant();
}

QVariant LdCtrlBaseChoose_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "ParamRefId";
            case 2:
                return "InternalDescription";
            }
        }
    }
    return QVariant();
}

QVariant LdCtrlBaseChoose_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "LdCtrlBaseChoose";
    case Qt::DecorationRole:
        return QIcon::fromTheme("LdCtrlBaseChoose");
    }
    return QVariant();
}

ParameterRef_t * LdCtrlBaseChoose_t::getParameterRef() const
{
    if (ParamRefId.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<ParameterRef_t *>(knx->ids[ParamRefId]);
}

LdCtrlBaseChoose_t * make_LdCtrlBaseChoose_t(Base * parent)
{
    return new LdCtrlBaseChoose_t(parent);
}

} // namespace knx
} // namespace v14
} // namespace Project
