/* This file is generated. */

#include <Project/v14/knx/MasterData_FunctionalBlocks_FunctionalBlock_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v14/knx/KNX_t.h>

namespace Project {
namespace v14 {
namespace knx {

MasterData_FunctionalBlocks_FunctionalBlock_t::MasterData_FunctionalBlocks_FunctionalBlock_t(Base * parent) :
    Base(parent)
{
}

MasterData_FunctionalBlocks_FunctionalBlock_t::~MasterData_FunctionalBlocks_FunctionalBlock_t()
{
}

void MasterData_FunctionalBlocks_FunctionalBlock_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("Name")) {
            Name = attribute.value().toString();
            continue;
        }
        if (name == "Id") {
            Id = attribute.value().toString();
            KNX_t * knx = findParent<KNX_t *>();
            Q_ASSERT(knx);
            knx->ids[Id] = this;
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("Parameters")) {
            QString newObjectType = reader.attributes().value("ObjectType").toString();
            Q_ASSERT(!newObjectType.isEmpty());
            MasterData_FunctionalBlocks_FunctionalBlock_Parameters_t * newParameters;
            if (Parameters.contains(newObjectType)) {
                newParameters = Parameters[newObjectType];
            } else {
                newParameters = make_MasterData_FunctionalBlocks_FunctionalBlock_Parameters_t(this);
                Parameters[newObjectType] = newParameters;
            }
            newParameters->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int MasterData_FunctionalBlocks_FunctionalBlock_t::tableColumnCount() const
{
    return 3;
}

QVariant MasterData_FunctionalBlocks_FunctionalBlock_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "MasterData_FunctionalBlocks_FunctionalBlock";
        }
        if (qualifiedName == QString("Name")) {
            return Name;
        }
        if (qualifiedName == QString("Id")) {
            return Id;
        }
        break;
    }
    return QVariant();
}

QVariant MasterData_FunctionalBlocks_FunctionalBlock_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Name";
            case 2:
                return "Id";
            }
        }
    }
    return QVariant();
}

QVariant MasterData_FunctionalBlocks_FunctionalBlock_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return !Name.isEmpty() ? Name : "FunctionalBlock";
    case Qt::DecorationRole:
        return QIcon::fromTheme("MasterData_FunctionalBlocks_FunctionalBlock");
    }
    return QVariant();
}

MasterData_FunctionalBlocks_FunctionalBlock_t * make_MasterData_FunctionalBlocks_FunctionalBlock_t(Base * parent)
{
    return new MasterData_FunctionalBlocks_FunctionalBlock_t(parent);
}

} // namespace knx
} // namespace v14
} // namespace Project
