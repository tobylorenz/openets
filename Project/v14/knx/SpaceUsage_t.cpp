/* This file is generated. */

#include <Project/v14/knx/SpaceUsage_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v14/knx/KNX_t.h>

namespace Project {
namespace v14 {
namespace knx {

SpaceUsage_t::SpaceUsage_t(Base * parent) :
    Base(parent)
{
}

SpaceUsage_t::~SpaceUsage_t()
{
}

void SpaceUsage_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == "Id") {
            Id = attribute.value().toString();
            KNX_t * knx = findParent<KNX_t *>();
            Q_ASSERT(knx);
            knx->ids[Id] = this;
            continue;
        }
        if (name == QString("Number")) {
            Number = attribute.value().toString();
            continue;
        }
        if (name == QString("Text")) {
            Text = attribute.value().toString();
            continue;
        }
        if (name == QString("Description")) {
            Description = attribute.value().toString();
            continue;
        }
        if (name == QString("Relations")) {
            QString value = attribute.value().toString();
            if (!value.isEmpty())
                qWarning() << "knx:SpaceUsage_t attribute Relations references to" << value;
        }
        if (name == QString("Relations")) {
            Relations = attribute.value().toString().split(' ');
            continue;
        }
        if (name == QString("Status")) {
            Status = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("SpaceUsage")) {
            QString newId = reader.attributes().value("Id").toString();
            Q_ASSERT(!newId.isEmpty());
            SpaceUsage_t * newSpaceUsage;
            if (SpaceUsage.contains(newId)) {
                newSpaceUsage = SpaceUsage[newId];
            } else {
                newSpaceUsage = make_SpaceUsage_t(this);
                SpaceUsage[newId] = newSpaceUsage;
            }
            newSpaceUsage->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int SpaceUsage_t::tableColumnCount() const
{
    return 7;
}

QVariant SpaceUsage_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "SpaceUsage";
        }
        if (qualifiedName == QString("Id")) {
            return Id;
        }
        if (qualifiedName == QString("Number")) {
            return Number;
        }
        if (qualifiedName == QString("Text")) {
            return Text;
        }
        if (qualifiedName == QString("Description")) {
            return Description;
        }
        if (qualifiedName == QString("Relations")) {
            return Relations.join(' ');
        }
        if (qualifiedName == QString("Status")) {
            return Status;
        }
        break;
    }
    return QVariant();
}

QVariant SpaceUsage_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Id";
            case 2:
                return "Number";
            case 3:
                return "Text";
            case 4:
                return "Description";
            case 5:
                return "Relations";
            case 6:
                return "Status";
            }
        }
    }
    return QVariant();
}

QVariant SpaceUsage_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return QString("%1: %2").arg(Id).arg(Text);
    case Qt::DecorationRole:
        return QIcon::fromTheme(QString("SpaceUsage_%1").arg(Number));
    }
    return QVariant();
}

SpaceUsage_t * make_SpaceUsage_t(Base * parent)
{
    return new SpaceUsage_t(parent);
}

} // namespace knx
} // namespace v14
} // namespace Project
