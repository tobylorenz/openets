/* This file is generated. */

#include <Project/v14/knx/Security_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v14/knx/KNX_t.h>
#include <Project/v14/knx/Security_Role_t.h>

namespace Project {
namespace v14 {
namespace knx {

Security_t::Security_t(Base * parent) :
    Base(parent)
{
}

Security_t::~Security_t()
{
}

void Security_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("LoadedIPRoutingBackboneKey")) {
            LoadedIPRoutingBackboneKey = attribute.value().toString();
            continue;
        }
        if (name == QString("DeviceAuthenticationCode")) {
            DeviceAuthenticationCode = attribute.value().toString();
            continue;
        }
        if (name == QString("DeviceAuthenticationCodeHash")) {
            DeviceAuthenticationCodeHash = attribute.value().toString();
            continue;
        }
        if (name == QString("LoadedDeviceAuthenticationCodeHash")) {
            LoadedDeviceAuthenticationCodeHash = attribute.value().toString();
            continue;
        }
        if (name == QString("DeviceManagementPassword")) {
            DeviceManagementPassword = attribute.value().toString();
            continue;
        }
        if (name == QString("DeviceManagementPasswordHash")) {
            DeviceManagementPasswordHash = attribute.value().toString();
            continue;
        }
        if (name == QString("LoadedDeviceManagementPasswordHash")) {
            LoadedDeviceManagementPasswordHash = attribute.value().toString();
            continue;
        }
        if (name == QString("ToolKey")) {
            ToolKey = attribute.value().toString();
            continue;
        }
        if (name == QString("LoadedToolKey")) {
            LoadedToolKey = attribute.value().toString();
            continue;
        }
        if (name == QString("SequenceNumber")) {
            SequenceNumber = attribute.value().toString();
            continue;
        }
        if (name == QString("SequenceNumberTimestamp")) {
            SequenceNumberTimestamp = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("Role")) {
            if (!Role) {
                Role = make_Security_Role_t(this);
            }
            Role->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int Security_t::tableColumnCount() const
{
    return 12;
}

QVariant Security_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "Security";
        }
        if (qualifiedName == QString("LoadedIPRoutingBackboneKey")) {
            return LoadedIPRoutingBackboneKey;
        }
        if (qualifiedName == QString("DeviceAuthenticationCode")) {
            return DeviceAuthenticationCode;
        }
        if (qualifiedName == QString("DeviceAuthenticationCodeHash")) {
            return DeviceAuthenticationCodeHash;
        }
        if (qualifiedName == QString("LoadedDeviceAuthenticationCodeHash")) {
            return LoadedDeviceAuthenticationCodeHash;
        }
        if (qualifiedName == QString("DeviceManagementPassword")) {
            return DeviceManagementPassword;
        }
        if (qualifiedName == QString("DeviceManagementPasswordHash")) {
            return DeviceManagementPasswordHash;
        }
        if (qualifiedName == QString("LoadedDeviceManagementPasswordHash")) {
            return LoadedDeviceManagementPasswordHash;
        }
        if (qualifiedName == QString("ToolKey")) {
            return ToolKey;
        }
        if (qualifiedName == QString("LoadedToolKey")) {
            return LoadedToolKey;
        }
        if (qualifiedName == QString("SequenceNumber")) {
            return SequenceNumber;
        }
        if (qualifiedName == QString("SequenceNumberTimestamp")) {
            return SequenceNumberTimestamp;
        }
        break;
    }
    return QVariant();
}

QVariant Security_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "LoadedIPRoutingBackboneKey";
            case 2:
                return "DeviceAuthenticationCode";
            case 3:
                return "DeviceAuthenticationCodeHash";
            case 4:
                return "LoadedDeviceAuthenticationCodeHash";
            case 5:
                return "DeviceManagementPassword";
            case 6:
                return "DeviceManagementPasswordHash";
            case 7:
                return "LoadedDeviceManagementPasswordHash";
            case 8:
                return "ToolKey";
            case 9:
                return "LoadedToolKey";
            case 10:
                return "SequenceNumber";
            case 11:
                return "SequenceNumberTimestamp";
            }
        }
    }
    return QVariant();
}

QVariant Security_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "Security";
    case Qt::DecorationRole:
        return QIcon::fromTheme("Security");
    }
    return QVariant();
}

Security_t * make_Security_t(Base * parent)
{
    return new Security_t(parent);
}

} // namespace knx
} // namespace v14
} // namespace Project
