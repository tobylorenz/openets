/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v14/knx/HorizontalAlignment_t.h>
#include <Project/v14/knx/LanguageDependentString255_t.h>
#include <Project/xs/ID.h>
#include <Project/xs/Base64Binary.h>
#include <Project/xs/Int.h>
#include <Project/xs/String.h>
#include <Project/xs/UnsignedInt.h>

namespace Project {
namespace v14 {
namespace knx {

class ParameterType_TypeRestriction_Enumeration_t : public Base
{
    Q_OBJECT

public:
    explicit ParameterType_TypeRestriction_Enumeration_t(Base * parent = nullptr);
    virtual ~ParameterType_TypeRestriction_Enumeration_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    LanguageDependentString255_t Text{};
    xs::String Icon{};
    HorizontalAlignment_t PictureAlignment{"Left"};
    xs::UnsignedInt Value{};
    xs::ID Id{};
    xs::Int DisplayOrder{};
    xs::Base64Binary BinaryValue{};
};

ParameterType_TypeRestriction_Enumeration_t * make_ParameterType_TypeRestriction_Enumeration_t(Base * parent);

} // namespace knx
} // namespace v14
} // namespace Project
