/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v14/knx/LdCtrlBase_OnError_t.h>
#include <Project/v14/knx/LdCtrlLoadImageProp_Count_t.h>
#include <Project/v14/knx/LdCtrlLoadImageProp_StartElement_t.h>
#include <Project/v14/knx/LdCtrlProcType_t.h>
#include <Project/xs/String.h>
#include <Project/xs/UnsignedByte.h>
#include <Project/xs/UnsignedShort.h>

namespace Project {
namespace v14 {
namespace knx {

class LdCtrlLoadImageProp_t : public Base
{
    Q_OBJECT

public:
    explicit LdCtrlLoadImageProp_t(Base * parent = nullptr);
    virtual ~LdCtrlLoadImageProp_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    LdCtrlProcType_t AppliesTo{"auto"};
    xs::String InternalDescription{};
    xs::UnsignedByte ObjIdx{};
    xs::UnsignedShort ObjType{};
    xs::UnsignedByte Occurrence{"0"};
    xs::UnsignedByte PropId{};
    LdCtrlLoadImageProp_Count_t Count{"1"};
    LdCtrlLoadImageProp_StartElement_t StartElement{"1"};

    /* elements */
    QVector<LdCtrlBase_OnError_t *> OnError;
};

LdCtrlLoadImageProp_t * make_LdCtrlLoadImageProp_t(Base * parent);

} // namespace knx
} // namespace v14
} // namespace Project
