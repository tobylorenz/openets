/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v14/knx/HawkConfigurationData_InterfaceObjects_InterfaceObject_t.h>

namespace Project {
namespace v14 {
namespace knx {

class HawkConfigurationData_InterfaceObjects_t : public Base
{
    Q_OBJECT

public:
    explicit HawkConfigurationData_InterfaceObjects_t(Base * parent = nullptr);
    virtual ~HawkConfigurationData_InterfaceObjects_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* elements */
    QVector<HawkConfigurationData_InterfaceObjects_InterfaceObject_t *> InterfaceObject;
};

HawkConfigurationData_InterfaceObjects_t * make_HawkConfigurationData_InterfaceObjects_t(Base * parent);

} // namespace knx
} // namespace v14
} // namespace Project
