/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v14/knx/CompletionStatus_t.h>
#include <Project/v14/knx/DeviceInstance_t.h>
#include <Project/v14/knx/IDREF.h>
#include <Project/v14/knx/String255_t.h>
#include <Project/v14/knx/Topology_Area_Line_Address_t.h>
#include <Project/xs/ID.h>
#include <Project/xs/Int.h>
#include <Project/xs/String.h>
#include <Project/xs/UnsignedLong.h>

namespace Project {
namespace v14 {
namespace knx {

/* forward declarations */
class BusAccess_t;
class MasterData_MediumTypes_MediumType_t;
class Topology_Area_Line_AdditionalGroupAddresses_t;

class Topology_Area_Line_t : public Base
{
    Q_OBJECT

public:
    explicit Topology_Area_Line_t(Base * parent = nullptr);
    virtual ~Topology_Area_Line_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::ID Id{};
    String255_t Name{};
    Topology_Area_Line_Address_t Address{};
    IDREF MediumTypeRefId{};
    xs::String Comment{};
    xs::UnsignedLong DomainAddress{};
    CompletionStatus_t CompletionStatus{};
    xs::String Description{};
    xs::Int Puid{};

    /* elements */
    QMap<xs::ID, DeviceInstance_t *> DeviceInstance; // key: Id
    BusAccess_t * BusAccess{};
    Topology_Area_Line_AdditionalGroupAddresses_t * AdditionalGroupAddresses{};

    /* getters */
    MasterData_MediumTypes_MediumType_t * getMediumType() const; // attribute: MediumTypeRefId
};

Topology_Area_Line_t * make_Topology_Area_Line_t(Base * parent);

} // namespace knx
} // namespace v14
} // namespace Project
