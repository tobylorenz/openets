/* This file is generated. */

#include <Project/v14/knx/GroupRange_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v14/knx/KNX_t.h>

namespace Project {
namespace v14 {
namespace knx {

GroupRange_t::GroupRange_t(Base * parent) :
    Base(parent)
{
}

GroupRange_t::~GroupRange_t()
{
}

void GroupRange_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == "Id") {
            Id = attribute.value().toString();
            KNX_t * knx = findParent<KNX_t *>();
            Q_ASSERT(knx);
            knx->ids[Id] = this;
            continue;
        }
        if (name == QString("Name")) {
            Name = attribute.value().toString();
            continue;
        }
        if (name == QString("RangeStart")) {
            RangeStart = attribute.value().toString();
            continue;
        }
        if (name == QString("RangeEnd")) {
            RangeEnd = attribute.value().toString();
            continue;
        }
        if (name == QString("Unfiltered")) {
            Unfiltered = attribute.value().toString();
            continue;
        }
        if (name == QString("Description")) {
            Description = attribute.value().toString();
            continue;
        }
        if (name == QString("Comment")) {
            Comment = attribute.value().toString();
            continue;
        }
        if (name == QString("Puid")) {
            Puid = attribute.value().toString();
            continue;
        }
        if (name == QString("Security")) {
            Security = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("GroupRange")) {
            QString newId = reader.attributes().value("Id").toString();
            Q_ASSERT(!newId.isEmpty());
            GroupRange_t * newGroupRange;
            if (GroupRange.contains(newId)) {
                newGroupRange = GroupRange[newId];
            } else {
                newGroupRange = make_GroupRange_t(this);
                GroupRange[newId] = newGroupRange;
            }
            newGroupRange->read(reader);
            continue;
        }
        if (reader.name() == QString("GroupAddress")) {
            QString newId = reader.attributes().value("Id").toString();
            Q_ASSERT(!newId.isEmpty());
            GroupAddress_t * newGroupAddress;
            if (GroupAddress.contains(newId)) {
                newGroupAddress = GroupAddress[newId];
            } else {
                newGroupAddress = make_GroupAddress_t(this);
                GroupAddress[newId] = newGroupAddress;
            }
            newGroupAddress->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int GroupRange_t::tableColumnCount() const
{
    return 10;
}

QVariant GroupRange_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "GroupRange";
        }
        if (qualifiedName == QString("Id")) {
            return Id;
        }
        if (qualifiedName == QString("Name")) {
            return Name;
        }
        if (qualifiedName == QString("RangeStart")) {
            return RangeStart;
        }
        if (qualifiedName == QString("RangeEnd")) {
            return RangeEnd;
        }
        if (qualifiedName == QString("Unfiltered")) {
            return Unfiltered;
        }
        if (qualifiedName == QString("Description")) {
            return Description;
        }
        if (qualifiedName == QString("Comment")) {
            return Comment;
        }
        if (qualifiedName == QString("Puid")) {
            return Puid;
        }
        if (qualifiedName == QString("Security")) {
            return Security;
        }
        break;
    }
    return QVariant();
}

QVariant GroupRange_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Id";
            case 2:
                return "Name";
            case 3:
                return "RangeStart";
            case 4:
                return "RangeEnd";
            case 5:
                return "Unfiltered";
            case 6:
                return "Description";
            case 7:
                return "Comment";
            case 8:
                return "Puid";
            case 9:
                return "Security";
            }
        }
    }
    return QVariant();
}

QVariant GroupRange_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
    {
        // @todo TwoLevel: Main Group (M) 5 Bit / Subgroup (G) 11 Bit
        // @todo ThreeLevel: Main Group (M) 5 Bit / Middle Group (Mi) 3 Bit / Subgroup (G) 8 Bit
        int rangeStart = RangeStart.toInt();
        int rangeStart1 = (rangeStart >> 11) & 0x1f;
        int rangeStart2 = (rangeStart >>  8) & 0x07;
        //int rangeStart3 = (rangeStart >>  0) & 0xff;
        if (!GroupAddress.isEmpty() || GroupRange.isEmpty()) {
            return QString("%1/%2 %3").arg(rangeStart1).arg(rangeStart2).arg(Name);
        } else {
            return QString("%1 %2").arg(rangeStart1).arg(Name);
        }
    }
    case Qt::DecorationRole:
        return QIcon::fromTheme(QString((!GroupAddress.isEmpty() || GroupRange.isEmpty()) ? "GroupRange_Middle" : "GroupRange_Main"));
    }
    return QVariant();
}

GroupRange_t * make_GroupRange_t(Base * parent)
{
    return new GroupRange_t(parent);
}

} // namespace knx
} // namespace v14
} // namespace Project
