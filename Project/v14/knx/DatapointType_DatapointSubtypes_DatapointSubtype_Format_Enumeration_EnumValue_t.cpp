/* This file is generated. */

#include <Project/v14/knx/DatapointType_DatapointSubtypes_DatapointSubtype_Format_Enumeration_EnumValue_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v14/knx/KNX_t.h>

namespace Project {
namespace v14 {
namespace knx {

DatapointType_DatapointSubtypes_DatapointSubtype_Format_Enumeration_EnumValue_t::DatapointType_DatapointSubtypes_DatapointSubtype_Format_Enumeration_EnumValue_t(Base * parent) :
    Base(parent)
{
}

DatapointType_DatapointSubtypes_DatapointSubtype_Format_Enumeration_EnumValue_t::~DatapointType_DatapointSubtypes_DatapointSubtype_Format_Enumeration_EnumValue_t()
{
}

void DatapointType_DatapointSubtypes_DatapointSubtype_Format_Enumeration_EnumValue_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == "Id") {
            Id = attribute.value().toString();
            KNX_t * knx = findParent<KNX_t *>();
            Q_ASSERT(knx);
            knx->ids[Id] = this;
            continue;
        }
        if (name == QString("Value")) {
            Value = attribute.value().toString();
            continue;
        }
        if (name == QString("Text")) {
            Text = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int DatapointType_DatapointSubtypes_DatapointSubtype_Format_Enumeration_EnumValue_t::tableColumnCount() const
{
    return 4;
}

QVariant DatapointType_DatapointSubtypes_DatapointSubtype_Format_Enumeration_EnumValue_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "DatapointType_DatapointSubtypes_DatapointSubtype_Format_Enumeration_EnumValue";
        }
        if (qualifiedName == QString("Id")) {
            return Id;
        }
        if (qualifiedName == QString("Value")) {
            return Value;
        }
        if (qualifiedName == QString("Text")) {
            return Text;
        }
        break;
    }
    return QVariant();
}

QVariant DatapointType_DatapointSubtypes_DatapointSubtype_Format_Enumeration_EnumValue_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Id";
            case 2:
                return "Value";
            case 3:
                return "Text";
            }
        }
    }
    return QVariant();
}

QVariant DatapointType_DatapointSubtypes_DatapointSubtype_Format_Enumeration_EnumValue_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return QString("[%1] %2").arg(Value).arg(Text);
    case Qt::DecorationRole:
        return QIcon::fromTheme("DatapointType_DatapointSubtypes_DatapointSubtype_Format_Enumeration_EnumValue");
    }
    return QVariant();
}

DatapointType_DatapointSubtypes_DatapointSubtype_Format_Enumeration_EnumValue_t * make_DatapointType_DatapointSubtypes_DatapointSubtype_Format_Enumeration_EnumValue_t(Base * parent)
{
    return new DatapointType_DatapointSubtypes_DatapointSubtype_Format_Enumeration_EnumValue_t(parent);
}

} // namespace knx
} // namespace v14
} // namespace Project
