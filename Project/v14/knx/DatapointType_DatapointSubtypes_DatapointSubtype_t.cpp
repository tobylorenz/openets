/* This file is generated. */

#include <Project/v14/knx/DatapointType_DatapointSubtypes_DatapointSubtype_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v14/knx/DatapointType_DatapointSubtypes_DatapointSubtype_Format_t.h>
#include <Project/v14/knx/KNX_t.h>
#include <Project/v14/knx/MasterData_PropertyDataTypes_PropertyDataType_t.h>

namespace Project {
namespace v14 {
namespace knx {

DatapointType_DatapointSubtypes_DatapointSubtype_t::DatapointType_DatapointSubtypes_DatapointSubtype_t(Base * parent) :
    Base(parent)
{
}

DatapointType_DatapointSubtypes_DatapointSubtype_t::~DatapointType_DatapointSubtypes_DatapointSubtype_t()
{
}

void DatapointType_DatapointSubtypes_DatapointSubtype_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == "Id") {
            Id = attribute.value().toString();
            KNX_t * knx = findParent<KNX_t *>();
            Q_ASSERT(knx);
            knx->ids[Id] = this;
            continue;
        }
        if (name == QString("Number")) {
            Number = attribute.value().toString();
            continue;
        }
        if (name == QString("Name")) {
            Name = attribute.value().toString();
            continue;
        }
        if (name == QString("Text")) {
            Text = attribute.value().toString();
            continue;
        }
        if (name == QString("Default")) {
            Default = attribute.value().toString();
            continue;
        }
        if (name == QString("PDT")) {
            PDT = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("Format")) {
            if (!Format) {
                Format = make_DatapointType_DatapointSubtypes_DatapointSubtype_Format_t(this);
            }
            Format->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int DatapointType_DatapointSubtypes_DatapointSubtype_t::tableColumnCount() const
{
    return 7;
}

QVariant DatapointType_DatapointSubtypes_DatapointSubtype_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "DatapointType_DatapointSubtypes_DatapointSubtype";
        }
        if (qualifiedName == QString("Id")) {
            return Id;
        }
        if (qualifiedName == QString("Number")) {
            return Number;
        }
        if (qualifiedName == QString("Name")) {
            return Name;
        }
        if (qualifiedName == QString("Text")) {
            return Text;
        }
        if (qualifiedName == QString("Default")) {
            return Default;
        }
        if (qualifiedName == QString("PDT")) {
            return PDT;
        }
        break;
    }
    return QVariant();
}

QVariant DatapointType_DatapointSubtypes_DatapointSubtype_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Id";
            case 2:
                return "Number";
            case 3:
                return "Name";
            case 4:
                return "Text";
            case 5:
                return "Default";
            case 6:
                return "PDT";
            }
        }
    }
    return QVariant();
}

QVariant DatapointType_DatapointSubtypes_DatapointSubtype_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return QString("%1: %2").arg(Id).arg(Text);
    case Qt::DecorationRole:
        return QIcon::fromTheme("DatapointType_DatapointSubtypes_DatapointSubtype");
    }
    return QVariant();
}

MasterData_PropertyDataTypes_PropertyDataType_t * DatapointType_DatapointSubtypes_DatapointSubtype_t::getPropertyDataType() const
{
    if (PDT.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<MasterData_PropertyDataTypes_PropertyDataType_t *>(knx->ids[PDT]);
}

DatapointType_DatapointSubtypes_DatapointSubtype_t * make_DatapointType_DatapointSubtypes_DatapointSubtype_t(Base * parent)
{
    return new DatapointType_DatapointSubtypes_DatapointSubtype_t(parent);
}

} // namespace knx
} // namespace v14
} // namespace Project
