/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v14/knx/LdCtrlBase_OnError_t.h>
#include <Project/v14/knx/LdCtrlProcType_t.h>
#include <Project/xs/String.h>
#include <Project/xs/UnsignedByte.h>
#include <Project/xs/UnsignedInt.h>

namespace Project {
namespace v14 {
namespace knx {

class LdCtrlMapError_t : public Base
{
    Q_OBJECT

public:
    explicit LdCtrlMapError_t(Base * parent = nullptr);
    virtual ~LdCtrlMapError_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    LdCtrlProcType_t AppliesTo{"auto"};
    xs::String InternalDescription{};
    xs::UnsignedByte LdCtrlFilter{"0"};
    xs::UnsignedInt OriginalError{};
    xs::UnsignedInt MappedError{};

    /* elements */
    QVector<LdCtrlBase_OnError_t *> OnError;
};

LdCtrlMapError_t * make_LdCtrlMapError_t(Base * parent);

} // namespace knx
} // namespace v14
} // namespace Project
