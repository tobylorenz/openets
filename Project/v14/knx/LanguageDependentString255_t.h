/* This file is generated. */

#pragma once

#include <Project/v14/knx/LanguageDependentString_t.h>

namespace Project {
namespace v14 {
namespace knx {

using LanguageDependentString255_t = LanguageDependentString_t;

} // namespace knx
} // namespace v14
} // namespace Project
