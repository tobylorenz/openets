/* This file is generated. */

#include <Project/v14/knx/ComObjectInstanceRef_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v14/knx/ApplicationProgramChannel_t.h>
#include <Project/v14/knx/ComObjectInstanceRef_Connectors_t.h>
#include <Project/v14/knx/ComObjectRef_t.h>
#include <Project/v14/knx/KNX_t.h>

namespace Project {
namespace v14 {
namespace knx {

ComObjectInstanceRef_t::ComObjectInstanceRef_t(Base * parent) :
    Base(parent)
{
}

ComObjectInstanceRef_t::~ComObjectInstanceRef_t()
{
}

void ComObjectInstanceRef_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == "Id") {
            Id = attribute.value().toString();
            KNX_t * knx = findParent<KNX_t *>();
            Q_ASSERT(knx);
            knx->ids[Id] = this;
            continue;
        }
        if (name == QString("RefId")) {
            RefId = attribute.value().toString();
            continue;
        }
        if (name == QString("Text")) {
            Text = attribute.value().toString();
            continue;
        }
        if (name == QString("FunctionText")) {
            FunctionText = attribute.value().toString();
            continue;
        }
        if (name == QString("Priority")) {
            Priority = attribute.value().toString();
            continue;
        }
        if (name == QString("ReadFlag")) {
            ReadFlag = attribute.value().toString();
            continue;
        }
        if (name == QString("WriteFlag")) {
            WriteFlag = attribute.value().toString();
            continue;
        }
        if (name == QString("CommunicationFlag")) {
            CommunicationFlag = attribute.value().toString();
            continue;
        }
        if (name == QString("TransmitFlag")) {
            TransmitFlag = attribute.value().toString();
            continue;
        }
        if (name == QString("UpdateFlag")) {
            UpdateFlag = attribute.value().toString();
            continue;
        }
        if (name == QString("ReadOnInitFlag")) {
            ReadOnInitFlag = attribute.value().toString();
            continue;
        }
        if (name == QString("DatapointType")) {
            DatapointType = attribute.value().toString().split(' ');
            continue;
        }
        if (name == QString("Description")) {
            Description = attribute.value().toString();
            continue;
        }
        if (name == QString("IsActive")) {
            IsActive = attribute.value().toString();
            continue;
        }
        if (name == QString("ChannelId")) {
            ChannelId = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("Connectors")) {
            if (!Connectors) {
                Connectors = make_ComObjectInstanceRef_Connectors_t(this);
            }
            Connectors->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ComObjectInstanceRef_t::tableColumnCount() const
{
    return 16;
}

QVariant ComObjectInstanceRef_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ComObjectInstanceRef";
        }
        if (qualifiedName == QString("Id")) {
            return Id;
        }
        if (qualifiedName == QString("RefId")) {
            return RefId;
        }
        if (qualifiedName == QString("Text")) {
            return Text;
        }
        if (qualifiedName == QString("FunctionText")) {
            return FunctionText;
        }
        if (qualifiedName == QString("Priority")) {
            return Priority;
        }
        if (qualifiedName == QString("ReadFlag")) {
            return ReadFlag;
        }
        if (qualifiedName == QString("WriteFlag")) {
            return WriteFlag;
        }
        if (qualifiedName == QString("CommunicationFlag")) {
            return CommunicationFlag;
        }
        if (qualifiedName == QString("TransmitFlag")) {
            return TransmitFlag;
        }
        if (qualifiedName == QString("UpdateFlag")) {
            return UpdateFlag;
        }
        if (qualifiedName == QString("ReadOnInitFlag")) {
            return ReadOnInitFlag;
        }
        if (qualifiedName == QString("DatapointType")) {
            return DatapointType.join(' ');
        }
        if (qualifiedName == QString("Description")) {
            return Description;
        }
        if (qualifiedName == QString("IsActive")) {
            return IsActive;
        }
        if (qualifiedName == QString("ChannelId")) {
            return ChannelId;
        }
        break;
    }
    return QVariant();
}

QVariant ComObjectInstanceRef_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Id";
            case 2:
                return "RefId";
            case 3:
                return "Text";
            case 4:
                return "FunctionText";
            case 5:
                return "Priority";
            case 6:
                return "ReadFlag";
            case 7:
                return "WriteFlag";
            case 8:
                return "CommunicationFlag";
            case 9:
                return "TransmitFlag";
            case 10:
                return "UpdateFlag";
            case 11:
                return "ReadOnInitFlag";
            case 12:
                return "DatapointType";
            case 13:
                return "Description";
            case 14:
                return "IsActive";
            case 15:
                return "ChannelId";
            }
        }
    }
    return QVariant();
}

QVariant ComObjectInstanceRef_t::treeData(int role) const
{
    auto * ref = getComObjectRef();
    if (ref) {
        return ref->treeData(role);
    }

    switch (role) {
    case Qt::DisplayRole:
        return "ComObjectInstanceRef";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ComObjectInstanceRef");
    }
    return QVariant();
}

ComObjectRef_t * ComObjectInstanceRef_t::getComObjectRef() const
{
    if (RefId.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<ComObjectRef_t *>(knx->ids[RefId]);
}

QList<DatapointType_DatapointSubtypes_DatapointSubtype_t *> ComObjectInstanceRef_t::getDatapointType() const
{
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    QList<DatapointType_DatapointSubtypes_DatapointSubtype_t *> retVal;
    for (QString id : DatapointType) {
        retVal << qobject_cast<DatapointType_DatapointSubtypes_DatapointSubtype_t *>(knx->ids[id]);
    }
    return retVal;
}

ApplicationProgramChannel_t * ComObjectInstanceRef_t::getChannel() const
{
    if (ChannelId.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<ApplicationProgramChannel_t *>(knx->ids[ChannelId]);
}

ComObjectInstanceRef_t * make_ComObjectInstanceRef_t(Base * parent)
{
    return new ComObjectInstanceRef_t(parent);
}

} // namespace knx
} // namespace v14
} // namespace Project
