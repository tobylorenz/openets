/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v14/knx/ApplicationProgramStatic_ComObjectTable_Offset_t.h>
#include <Project/v14/knx/ComObject_t.h>
#include <Project/v14/knx/IDREF.h>

namespace Project {
namespace v14 {
namespace knx {

/* forward declarations */
class ApplicationProgramStatic_Code_AbsoluteSegment_t;

class ApplicationProgramStatic_ComObjectTable_t : public Base
{
    Q_OBJECT

public:
    explicit ApplicationProgramStatic_ComObjectTable_t(Base * parent = nullptr);
    virtual ~ApplicationProgramStatic_ComObjectTable_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    IDREF CodeSegment{};
    ApplicationProgramStatic_ComObjectTable_Offset_t Offset{};

    /* elements */
    QMap<xs::ID, ComObject_t *> ComObject; // key: Id

    /* getters */
    ApplicationProgramStatic_Code_AbsoluteSegment_t * getCodeSegment() const; // attribute: CodeSegment
};

ApplicationProgramStatic_ComObjectTable_t * make_ApplicationProgramStatic_ComObjectTable_t(Base * parent);

} // namespace knx
} // namespace v14
} // namespace Project
