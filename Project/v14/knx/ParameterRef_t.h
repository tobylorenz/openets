/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v14/knx/Access_t.h>
#include <Project/v14/knx/IDREF.h>
#include <Project/v14/knx/LanguageDependentString20_t.h>
#include <Project/v14/knx/LanguageDependentString255_t.h>
#include <Project/v14/knx/LanguageDependentString_t.h>
#include <Project/v14/knx/String50_t.h>
#include <Project/v14/knx/Value_t.h>
#include <Project/xs/ID.h>
#include <Project/xs/Boolean.h>
#include <Project/xs/Int.h>
#include <Project/xs/String.h>

namespace Project {
namespace v14 {
namespace knx {

/* forward declarations */
class ParameterRef_t;

class ParameterRef_t : public Base
{
    Q_OBJECT

public:
    explicit ParameterRef_t(Base * parent = nullptr);
    virtual ~ParameterRef_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::ID Id{};
    IDREF RefId{};
    String50_t Name{};
    LanguageDependentString255_t Text{};
    LanguageDependentString20_t SuffixText{};
    String50_t Tag{};
    xs::Int DisplayOrder{};
    Access_t Access{};
    Value_t Value{};
    LanguageDependentString_t InitialValue{};
    xs::Boolean CustomerAdjustable{};
    IDREF TextParameterRefId{};
    xs::String InternalDescription{};
    xs::Boolean ForbidGrantingUseByCustomer{"false"};

    /* getters */
    Base * getParameter() const; // attribute: RefId
    ParameterRef_t * getTextParameterRef() const; // attribute: TextParameterRefId
};

ParameterRef_t * make_ParameterRef_t(Base * parent);

} // namespace knx
} // namespace v14
} // namespace Project
