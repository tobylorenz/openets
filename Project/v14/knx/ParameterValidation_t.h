/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v14/knx/String50_t.h>
#include <Project/xs/ID.h>
#include <Project/xs/String.h>

namespace Project {
namespace v14 {
namespace knx {

/* forward declarations */
class ParameterValidation_Parameters_t;

class ParameterValidation_t : public Base
{
    Q_OBJECT

public:
    explicit ParameterValidation_t(Base * parent = nullptr);
    virtual ~ParameterValidation_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::ID Id{};
    String50_t Name{};
    xs::String InternalDescription{};
    xs::String ValidationFunc{};
    xs::String ValidationParameters{};

    /* elements */
    ParameterValidation_Parameters_t * Parameters{};
};

ParameterValidation_t * make_ParameterValidation_t(Base * parent);

} // namespace knx
} // namespace v14
} // namespace Project
