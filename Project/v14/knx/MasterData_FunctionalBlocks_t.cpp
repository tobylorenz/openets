/* This file is generated. */

#include <Project/v14/knx/MasterData_FunctionalBlocks_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v14/knx/KNX_t.h>

namespace Project {
namespace v14 {
namespace knx {

MasterData_FunctionalBlocks_t::MasterData_FunctionalBlocks_t(Base * parent) :
    Base(parent)
{
}

MasterData_FunctionalBlocks_t::~MasterData_FunctionalBlocks_t()
{
}

void MasterData_FunctionalBlocks_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("FunctionalBlock")) {
            QString newId = reader.attributes().value("Id").toString();
            Q_ASSERT(!newId.isEmpty());
            MasterData_FunctionalBlocks_FunctionalBlock_t * newFunctionalBlock;
            if (FunctionalBlock.contains(newId)) {
                newFunctionalBlock = FunctionalBlock[newId];
            } else {
                newFunctionalBlock = make_MasterData_FunctionalBlocks_FunctionalBlock_t(this);
                FunctionalBlock[newId] = newFunctionalBlock;
            }
            newFunctionalBlock->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int MasterData_FunctionalBlocks_t::tableColumnCount() const
{
    return 1;
}

QVariant MasterData_FunctionalBlocks_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "MasterData_FunctionalBlocks";
        }
        break;
    }
    return QVariant();
}

QVariant MasterData_FunctionalBlocks_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant MasterData_FunctionalBlocks_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "FunctionalBlocks";
    case Qt::DecorationRole:
        return QIcon::fromTheme("MasterData_FunctionalBlocks");
    }
    return QVariant();
}

MasterData_FunctionalBlocks_t * make_MasterData_FunctionalBlocks_t(Base * parent)
{
    return new MasterData_FunctionalBlocks_t(parent);
}

} // namespace knx
} // namespace v14
} // namespace Project
