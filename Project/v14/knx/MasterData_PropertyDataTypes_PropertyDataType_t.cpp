/* This file is generated. */

#include <Project/v14/knx/MasterData_PropertyDataTypes_PropertyDataType_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v14/knx/KNX_t.h>

namespace Project {
namespace v14 {
namespace knx {

MasterData_PropertyDataTypes_PropertyDataType_t::MasterData_PropertyDataTypes_PropertyDataType_t(Base * parent) :
    Base(parent)
{
}

MasterData_PropertyDataTypes_PropertyDataType_t::~MasterData_PropertyDataTypes_PropertyDataType_t()
{
}

void MasterData_PropertyDataTypes_PropertyDataType_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == "Id") {
            Id = attribute.value().toString();
            KNX_t * knx = findParent<KNX_t *>();
            Q_ASSERT(knx);
            knx->ids[Id] = this;
            continue;
        }
        if (name == QString("Number")) {
            Number = attribute.value().toString();
            continue;
        }
        if (name == QString("Name")) {
            Name = attribute.value().toString();
            continue;
        }
        if (name == QString("Size")) {
            Size = attribute.value().toString();
            continue;
        }
        if (name == QString("ReadSize")) {
            ReadSize = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int MasterData_PropertyDataTypes_PropertyDataType_t::tableColumnCount() const
{
    return 6;
}

QVariant MasterData_PropertyDataTypes_PropertyDataType_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "MasterData_PropertyDataTypes_PropertyDataType";
        }
        if (qualifiedName == QString("Id")) {
            return Id;
        }
        if (qualifiedName == QString("Number")) {
            return Number;
        }
        if (qualifiedName == QString("Name")) {
            return Name;
        }
        if (qualifiedName == QString("Size")) {
            return Size;
        }
        if (qualifiedName == QString("ReadSize")) {
            return ReadSize;
        }
        break;
    }
    return QVariant();
}

QVariant MasterData_PropertyDataTypes_PropertyDataType_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Id";
            case 2:
                return "Number";
            case 3:
                return "Name";
            case 4:
                return "Size";
            case 5:
                return "ReadSize";
            }
        }
    }
    return QVariant();
}

QVariant MasterData_PropertyDataTypes_PropertyDataType_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return QString("%1: %2").arg(Id).arg(Name);
    case Qt::DecorationRole:
        return QIcon::fromTheme("MasterData_PropertyDataTypes_PropertyDataType");
    }
    return QVariant();
}

MasterData_PropertyDataTypes_PropertyDataType_t * make_MasterData_PropertyDataTypes_PropertyDataType_t(Base * parent)
{
    return new MasterData_PropertyDataTypes_PropertyDataType_t(parent);
}

} // namespace knx
} // namespace v14
} // namespace Project
