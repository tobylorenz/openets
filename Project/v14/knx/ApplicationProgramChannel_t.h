/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v14/knx/IDREF.h>
#include <Project/v14/knx/LanguageDependentString255_t.h>
#include <Project/v14/knx/String50_t.h>
#include <Project/xs/ID.h>
#include <Project/xs/String.h>

namespace Project {
namespace v14 {
namespace knx {

/* forward declarations */
class BinaryDataRef_t;
class ChannelChoose_t;
class ComObjectParameterBlock_t;
class ComObjectRefRef_t;
class ParameterRef_t;

class ApplicationProgramChannel_t : public Base
{
    Q_OBJECT

public:
    explicit ApplicationProgramChannel_t(Base * parent = nullptr);
    virtual ~ApplicationProgramChannel_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    String50_t Name{};
    LanguageDependentString255_t Text{};
    String50_t Number{};
    xs::ID Id{};
    IDREF TextParameterRefId{};
    xs::String InternalDescription{};
    xs::String Icon{};
    xs::String HelpContext{};

    /* elements */
    // xs:choice ComObjectParameterBlock_t * ParameterBlock{};
    // xs:choice ComObjectRefRef_t * ComObjectRefRef{};
    // xs:choice BinaryDataRef_t * BinaryDataRef{};
    // xs:choice ChannelChoose_t * Choose{};

    /* getters */
    ParameterRef_t * getTextParameterRef() const; // attribute: TextParameterRefId
};

ApplicationProgramChannel_t * make_ApplicationProgramChannel_t(Base * parent);

} // namespace knx
} // namespace v14
} // namespace Project
