/* This file is generated. */

#include <Project/v14/knx/ComObjectParameterBlock_Columns_Column_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v14/knx/KNX_t.h>
#include <Project/v14/knx/ParameterRef_t.h>

namespace Project {
namespace v14 {
namespace knx {

ComObjectParameterBlock_Columns_Column_t::ComObjectParameterBlock_Columns_Column_t(Base * parent) :
    Base(parent)
{
}

ComObjectParameterBlock_Columns_Column_t::~ComObjectParameterBlock_Columns_Column_t()
{
}

void ComObjectParameterBlock_Columns_Column_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == "Id") {
            Id = attribute.value().toString();
            KNX_t * knx = findParent<KNX_t *>();
            Q_ASSERT(knx);
            knx->ids[Id] = this;
            continue;
        }
        if (name == QString("Name")) {
            Name = attribute.value().toString();
            continue;
        }
        if (name == QString("Text")) {
            Text = attribute.value().toString();
            continue;
        }
        if (name == QString("TextParameterRefId")) {
            TextParameterRefId = attribute.value().toString();
            continue;
        }
        if (name == QString("Width")) {
            Width = attribute.value().toString();
            continue;
        }
        if (name == QString("InternalDescription")) {
            InternalDescription = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ComObjectParameterBlock_Columns_Column_t::tableColumnCount() const
{
    return 7;
}

QVariant ComObjectParameterBlock_Columns_Column_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ComObjectParameterBlock_Columns_Column";
        }
        if (qualifiedName == QString("Id")) {
            return Id;
        }
        if (qualifiedName == QString("Name")) {
            return Name;
        }
        if (qualifiedName == QString("Text")) {
            return Text;
        }
        if (qualifiedName == QString("TextParameterRefId")) {
            return TextParameterRefId;
        }
        if (qualifiedName == QString("Width")) {
            return Width;
        }
        if (qualifiedName == QString("InternalDescription")) {
            return InternalDescription;
        }
        break;
    }
    return QVariant();
}

QVariant ComObjectParameterBlock_Columns_Column_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Id";
            case 2:
                return "Name";
            case 3:
                return "Text";
            case 4:
                return "TextParameterRefId";
            case 5:
                return "Width";
            case 6:
                return "InternalDescription";
            }
        }
    }
    return QVariant();
}

QVariant ComObjectParameterBlock_Columns_Column_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return !Name.isEmpty() ? Name : "Column";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ComObjectParameterBlock_Columns_Column");
    }
    return QVariant();
}

ParameterRef_t * ComObjectParameterBlock_Columns_Column_t::getTextParameterRef() const
{
    if (TextParameterRefId.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<ParameterRef_t *>(knx->ids[TextParameterRefId]);
}

ComObjectParameterBlock_Columns_Column_t * make_ComObjectParameterBlock_Columns_Column_t(Base * parent)
{
    return new ComObjectParameterBlock_Columns_Column_t(parent);
}

} // namespace knx
} // namespace v14
} // namespace Project
