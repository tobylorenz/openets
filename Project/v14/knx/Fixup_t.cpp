/* This file is generated. */

#include <Project/v14/knx/Fixup_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v14/knx/ApplicationProgramStatic_Code_AbsoluteSegment_t.h>
#include <Project/v14/knx/Function_t.h>
#include <Project/v14/knx/KNX_t.h>

namespace Project {
namespace v14 {
namespace knx {

Fixup_t::Fixup_t(Base * parent) :
    Base(parent)
{
}

Fixup_t::~Fixup_t()
{
}

void Fixup_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("FunctionRef")) {
            FunctionRef = attribute.value().toString();
            continue;
        }
        if (name == QString("CodeSegment")) {
            CodeSegment = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("Offset")) {
            auto * newOffset = make_SimpleElementTextType("Offset", this);
            newOffset->read(reader);
            Offset.append(newOffset);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int Fixup_t::tableColumnCount() const
{
    return 3;
}

QVariant Fixup_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "Fixup";
        }
        if (qualifiedName == QString("FunctionRef")) {
            return FunctionRef;
        }
        if (qualifiedName == QString("CodeSegment")) {
            return CodeSegment;
        }
        break;
    }
    return QVariant();
}

QVariant Fixup_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "FunctionRef";
            case 2:
                return "CodeSegment";
            }
        }
    }
    return QVariant();
}

QVariant Fixup_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
    {
        QString functionName;
#if PROJECT_VERSION >= 12
        Function_t * function = getFunction();
        if (function) {
            functionName = function->treeData(role).toString();
        }
#endif
        QString codeSegmentName;
        ApplicationProgramStatic_Code_AbsoluteSegment_t * codeSegment = getCodeSegment();
        if (codeSegment) {
            codeSegmentName = codeSegment->treeData(role).toString();
        }
        return QString("%1 %2").arg(functionName).arg(codeSegmentName);
    }
    case Qt::DecorationRole:
        return QIcon::fromTheme("Fixup");
    }
    return QVariant();
}

Function_t * Fixup_t::getFunction() const
{
    if (FunctionRef.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<Function_t *>(knx->ids[FunctionRef]);
}

ApplicationProgramStatic_Code_AbsoluteSegment_t * Fixup_t::getCodeSegment() const
{
    if (CodeSegment.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<ApplicationProgramStatic_Code_AbsoluteSegment_t *>(knx->ids[CodeSegment]);
}

Fixup_t * make_Fixup_t(Base * parent)
{
    return new Fixup_t(parent);
}

} // namespace knx
} // namespace v14
} // namespace Project
