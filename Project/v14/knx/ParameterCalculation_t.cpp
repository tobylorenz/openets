/* This file is generated. */

#include <Project/v14/knx/ParameterCalculation_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v14/knx/KNX_t.h>
#include <Project/v14/knx/ParameterCalculation_LParameters_t.h>
#include <Project/v14/knx/ParameterCalculation_RParameters_t.h>

namespace Project {
namespace v14 {
namespace knx {

ParameterCalculation_t::ParameterCalculation_t(Base * parent) :
    Base(parent)
{
}

ParameterCalculation_t::~ParameterCalculation_t()
{
}

void ParameterCalculation_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == "Id") {
            Id = attribute.value().toString();
            KNX_t * knx = findParent<KNX_t *>();
            Q_ASSERT(knx);
            knx->ids[Id] = this;
            continue;
        }
        if (name == QString("Language")) {
            Language = attribute.value().toString();
            continue;
        }
        if (name == QString("Name")) {
            Name = attribute.value().toString();
            continue;
        }
        if (name == QString("InternalDescription")) {
            InternalDescription = attribute.value().toString();
            continue;
        }
        if (name == QString("RLTransformationFunc")) {
            RLTransformationFunc = attribute.value().toString();
            continue;
        }
        if (name == QString("RLTransformationParameters")) {
            RLTransformationParameters = attribute.value().toString();
            continue;
        }
        if (name == QString("LRTransformationFunc")) {
            LRTransformationFunc = attribute.value().toString();
            continue;
        }
        if (name == QString("LRTransformationParameters")) {
            LRTransformationParameters = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("RLTransformation")) {
            if (!RLTransformation) {
                RLTransformation = make_SimpleElementTextType("RLTransformation", this);
            }
            RLTransformation->read(reader);
            continue;
        }
        if (reader.name() == QString("LRTransformation")) {
            if (!LRTransformation) {
                LRTransformation = make_SimpleElementTextType("LRTransformation", this);
            }
            LRTransformation->read(reader);
            continue;
        }
        if (reader.name() == QString("LParameters")) {
            if (!LParameters) {
                LParameters = make_ParameterCalculation_LParameters_t(this);
            }
            LParameters->read(reader);
            continue;
        }
        if (reader.name() == QString("RParameters")) {
            if (!RParameters) {
                RParameters = make_ParameterCalculation_RParameters_t(this);
            }
            RParameters->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ParameterCalculation_t::tableColumnCount() const
{
    return 9;
}

QVariant ParameterCalculation_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ParameterCalculation";
        }
        if (qualifiedName == QString("Id")) {
            return Id;
        }
        if (qualifiedName == QString("Language")) {
            return Language;
        }
        if (qualifiedName == QString("Name")) {
            return Name;
        }
        if (qualifiedName == QString("InternalDescription")) {
            return InternalDescription;
        }
        if (qualifiedName == QString("RLTransformationFunc")) {
            return RLTransformationFunc;
        }
        if (qualifiedName == QString("RLTransformationParameters")) {
            return RLTransformationParameters;
        }
        if (qualifiedName == QString("LRTransformationFunc")) {
            return LRTransformationFunc;
        }
        if (qualifiedName == QString("LRTransformationParameters")) {
            return LRTransformationParameters;
        }
        break;
    }
    return QVariant();
}

QVariant ParameterCalculation_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Id";
            case 2:
                return "Language";
            case 3:
                return "Name";
            case 4:
                return "InternalDescription";
            case 5:
                return "RLTransformationFunc";
            case 6:
                return "RLTransformationParameters";
            case 7:
                return "LRTransformationFunc";
            case 8:
                return "LRTransformationParameters";
            }
        }
    }
    return QVariant();
}

QVariant ParameterCalculation_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return !Name.isEmpty() ? Name : "ParameterCalculation";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ParameterCalculation");
    }
    return QVariant();
}

ParameterCalculation_t * make_ParameterCalculation_t(Base * parent)
{
    return new ParameterCalculation_t(parent);
}

} // namespace knx
} // namespace v14
} // namespace Project
