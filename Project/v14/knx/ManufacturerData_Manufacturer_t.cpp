/* This file is generated. */

#include <Project/v14/knx/ManufacturerData_Manufacturer_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v14/knx/KNX_t.h>
#include <Project/v14/knx/ManufacturerData_Manufacturer_ApplicationPrograms_t.h>
#include <Project/v14/knx/ManufacturerData_Manufacturer_Baggages_t.h>
#include <Project/v14/knx/ManufacturerData_Manufacturer_Catalog_t.h>
#include <Project/v14/knx/ManufacturerData_Manufacturer_Hardware_t.h>
#include <Project/v14/knx/ManufacturerData_Manufacturer_Languages_t.h>
#include <Project/v14/knx/MasterData_Manufacturers_Manufacturer_t.h>

namespace Project {
namespace v14 {
namespace knx {

ManufacturerData_Manufacturer_t::ManufacturerData_Manufacturer_t(Base * parent) :
    Base(parent)
{
}

ManufacturerData_Manufacturer_t::~ManufacturerData_Manufacturer_t()
{
}

void ManufacturerData_Manufacturer_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("RefId")) {
            RefId = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("Catalog")) {
            if (!Catalog) {
                Catalog = make_ManufacturerData_Manufacturer_Catalog_t(this);
            }
            Catalog->read(reader);
            continue;
        }
        if (reader.name() == QString("ApplicationPrograms")) {
            if (!ApplicationPrograms) {
                ApplicationPrograms = make_ManufacturerData_Manufacturer_ApplicationPrograms_t(this);
            }
            ApplicationPrograms->read(reader);
            continue;
        }
        if (reader.name() == QString("Baggages")) {
            if (!Baggages) {
                Baggages = make_ManufacturerData_Manufacturer_Baggages_t(this);
            }
            Baggages->read(reader);
            continue;
        }
        if (reader.name() == QString("Hardware")) {
            if (!Hardware) {
                Hardware = make_ManufacturerData_Manufacturer_Hardware_t(this);
            }
            Hardware->read(reader);
            continue;
        }
        if (reader.name() == QString("Languages")) {
            if (!Languages) {
                Languages = make_ManufacturerData_Manufacturer_Languages_t(this);
            }
            Languages->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ManufacturerData_Manufacturer_t::tableColumnCount() const
{
    return 2;
}

QVariant ManufacturerData_Manufacturer_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ManufacturerData_Manufacturer";
        }
        if (qualifiedName == QString("RefId")) {
            return RefId;
        }
        break;
    }
    return QVariant();
}

QVariant ManufacturerData_Manufacturer_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "RefId";
            }
        }
    }
    return QVariant();
}

QVariant ManufacturerData_Manufacturer_t::treeData(int role) const
{
    auto * ref = getManufacturer();
    if (ref) {
        return ref->treeData(role);
    }

    switch (role) {
    case Qt::DisplayRole:
        return "Manufacturer";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ManufacturerData_Manufacturer");
    }
    return QVariant();
}

MasterData_Manufacturers_Manufacturer_t * ManufacturerData_Manufacturer_t::getManufacturer() const
{
    if (RefId.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<MasterData_Manufacturers_Manufacturer_t *>(knx->ids[RefId]);
}

ManufacturerData_Manufacturer_t * make_ManufacturerData_Manufacturer_t(Base * parent)
{
    return new ManufacturerData_Manufacturer_t(parent);
}

} // namespace knx
} // namespace v14
} // namespace Project
