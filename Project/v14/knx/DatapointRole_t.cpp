/* This file is generated. */

#include <Project/v14/knx/DatapointRole_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v14/knx/KNX_t.h>

namespace Project {
namespace v14 {
namespace knx {

DatapointRole_t::DatapointRole_t(Base * parent) :
    Base(parent)
{
}

DatapointRole_t::~DatapointRole_t()
{
}

void DatapointRole_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == "Id") {
            Id = attribute.value().toString();
            KNX_t * knx = findParent<KNX_t *>();
            Q_ASSERT(knx);
            knx->ids[Id] = this;
            continue;
        }
        if (name == QString("Number")) {
            Number = attribute.value().toString();
            continue;
        }
        if (name == QString("Name")) {
            Name = attribute.value().toString();
            continue;
        }
        if (name == QString("Description")) {
            Description = attribute.value().toString();
            continue;
        }
        if (name == QString("Status")) {
            Status = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int DatapointRole_t::tableColumnCount() const
{
    return 6;
}

QVariant DatapointRole_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "DatapointRole";
        }
        if (qualifiedName == QString("Id")) {
            return Id;
        }
        if (qualifiedName == QString("Number")) {
            return Number;
        }
        if (qualifiedName == QString("Name")) {
            return Name;
        }
        if (qualifiedName == QString("Description")) {
            return Description;
        }
        if (qualifiedName == QString("Status")) {
            return Status;
        }
        break;
    }
    return QVariant();
}

QVariant DatapointRole_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Id";
            case 2:
                return "Number";
            case 3:
                return "Name";
            case 4:
                return "Description";
            case 5:
                return "Status";
            }
        }
    }
    return QVariant();
}

QVariant DatapointRole_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return QString("%1: %2").arg(Id).arg(Name);
    case Qt::DecorationRole:
        return QIcon::fromTheme("DatapointRole");
    }
    return QVariant();
}

DatapointRole_t * make_DatapointRole_t(Base * parent)
{
    return new DatapointRole_t(parent);
}

} // namespace knx
} // namespace v14
} // namespace Project
