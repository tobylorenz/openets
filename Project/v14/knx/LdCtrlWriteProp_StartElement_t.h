/* This file is generated. */

#pragma once

#include <Project/xs/UnsignedShort.h>

namespace Project {
namespace v14 {
namespace knx {

using LdCtrlWriteProp_StartElement_t = xs::UnsignedShort;

} // namespace knx
} // namespace v14
} // namespace Project
