/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/xs/Language.h>

namespace Project {
namespace v14 {
namespace knx {

class MasterData_ProductLanguages_Language_t : public Base
{
    Q_OBJECT

public:
    explicit MasterData_ProductLanguages_Language_t(Base * parent = nullptr);
    virtual ~MasterData_ProductLanguages_Language_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::Language Identifier{};
};

MasterData_ProductLanguages_Language_t * make_MasterData_ProductLanguages_Language_t(Base * parent);

} // namespace knx
} // namespace v14
} // namespace Project
