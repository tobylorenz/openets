/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v14/knx/Condition_t.h>
#include <Project/v14/knx/LdCtrlBase_t.h>
#include <Project/xs/Boolean.h>
#include <Project/xs/String.h>

namespace Project {
namespace v14 {
namespace knx {

/* forward declarations */
class LdCtrlBaseChoose_t;

class LdCtrlBaseChoose_when_t : public Base
{
    Q_OBJECT

public:
    explicit LdCtrlBaseChoose_when_t(Base * parent = nullptr);
    virtual ~LdCtrlBaseChoose_when_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    Condition_t Test{};
    xs::Boolean Default{"false"};
    xs::String InternalDescription{};

    /* elements */
    LdCtrlBase_t * LdCtrlBase{};
    // xs:choice LdCtrlBaseChoose_t * Choose{};
};

LdCtrlBaseChoose_when_t * make_LdCtrlBaseChoose_when_t(Base * parent);

} // namespace knx
} // namespace v14
} // namespace Project
