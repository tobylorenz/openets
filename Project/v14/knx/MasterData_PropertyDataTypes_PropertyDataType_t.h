/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v14/knx/String255_t.h>
#include <Project/xs/ID.h>
#include <Project/xs/UnsignedByte.h>
#include <Project/xs/UnsignedInt.h>

namespace Project {
namespace v14 {
namespace knx {

class MasterData_PropertyDataTypes_PropertyDataType_t : public Base
{
    Q_OBJECT

public:
    explicit MasterData_PropertyDataTypes_PropertyDataType_t(Base * parent = nullptr);
    virtual ~MasterData_PropertyDataTypes_PropertyDataType_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::ID Id{};
    xs::UnsignedInt Number{};
    String255_t Name{};
    xs::UnsignedInt Size{};
    xs::UnsignedByte ReadSize{};
};

MasterData_PropertyDataTypes_PropertyDataType_t * make_MasterData_PropertyDataTypes_PropertyDataType_t(Base * parent);

} // namespace knx
} // namespace v14
} // namespace Project
