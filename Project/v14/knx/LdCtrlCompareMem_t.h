/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v14/knx/LdCtrlBase_OnError_t.h>
#include <Project/v14/knx/LdCtrlCompareBase_Range_t.h>
#include <Project/v14/knx/LdCtrlMemAddrSpace_t.h>
#include <Project/v14/knx/LdCtrlProcType_t.h>
#include <Project/xs/Boolean.h>
#include <Project/xs/HexBinary.h>
#include <Project/xs/String.h>
#include <Project/xs/UnsignedInt.h>
#include <Project/xs/UnsignedShort.h>

namespace Project {
namespace v14 {
namespace knx {

class LdCtrlCompareMem_t : public Base
{
    Q_OBJECT

public:
    explicit LdCtrlCompareMem_t(Base * parent = nullptr);
    virtual ~LdCtrlCompareMem_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    LdCtrlProcType_t AppliesTo{"auto"};
    xs::String InternalDescription{};
    xs::Boolean AllowCachedValue{"false"};
    xs::HexBinary InlineData{};
    xs::HexBinary Mask{};
    LdCtrlCompareBase_Range_t Range{};
    xs::Boolean Invert{"false"};
    xs::UnsignedShort RetryInterval{"0"};
    xs::UnsignedShort TimeOut{"0"};
    LdCtrlMemAddrSpace_t AddressSpace{"Standard"};
    xs::UnsignedInt Address{};
    xs::UnsignedInt Size{};

    /* elements */
    QVector<LdCtrlBase_OnError_t *> OnError;
};

LdCtrlCompareMem_t * make_LdCtrlCompareMem_t(Base * parent);

} // namespace knx
} // namespace v14
} // namespace Project
