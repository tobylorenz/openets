/* This file is generated. */

#include <Project/v14/knx/ApplicationProgramStatic_Messages_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v14/knx/KNX_t.h>

namespace Project {
namespace v14 {
namespace knx {

ApplicationProgramStatic_Messages_t::ApplicationProgramStatic_Messages_t(Base * parent) :
    Base(parent)
{
}

ApplicationProgramStatic_Messages_t::~ApplicationProgramStatic_Messages_t()
{
}

void ApplicationProgramStatic_Messages_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("Message")) {
            QString newId = reader.attributes().value("Id").toString();
            Q_ASSERT(!newId.isEmpty());
            ApplicationProgramStatic_Messages_Message_t * newMessage;
            if (Message.contains(newId)) {
                newMessage = Message[newId];
            } else {
                newMessage = make_ApplicationProgramStatic_Messages_Message_t(this);
                Message[newId] = newMessage;
            }
            newMessage->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ApplicationProgramStatic_Messages_t::tableColumnCount() const
{
    return 1;
}

QVariant ApplicationProgramStatic_Messages_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ApplicationProgramStatic_Messages";
        }
        break;
    }
    return QVariant();
}

QVariant ApplicationProgramStatic_Messages_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant ApplicationProgramStatic_Messages_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "Messages";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ApplicationProgramStatic_Messages");
    }
    return QVariant();
}

ApplicationProgramStatic_Messages_t * make_ApplicationProgramStatic_Messages_t(Base * parent)
{
    return new ApplicationProgramStatic_Messages_t(parent);
}

} // namespace knx
} // namespace v14
} // namespace Project
