/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v14/knx/FunctionType_FunctionPoint_Characteristics_t.h>
#include <Project/v14/knx/IDREF.h>
#include <Project/v14/knx/LanguageDependentString255_t.h>

namespace Project {
namespace v14 {
namespace knx {

/* forward declarations */
class DatapointRole_t;
class DatapointType_DatapointSubtypes_DatapointSubtype_t;

class FunctionType_FunctionPoint_t : public Base
{
    Q_OBJECT

public:
    explicit FunctionType_FunctionPoint_t(Base * parent = nullptr);
    virtual ~FunctionType_FunctionPoint_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    LanguageDependentString255_t Text{};
    IDREF Role{};
    IDREF DatapointType{};
    FunctionType_FunctionPoint_Characteristics_t Characteristics{};

    /* getters */
    DatapointRole_t * getRole() const; // attribute: Role
    DatapointType_DatapointSubtypes_DatapointSubtype_t * getDatapointType() const; // attribute: DatapointType
};

FunctionType_FunctionPoint_t * make_FunctionType_FunctionPoint_t(Base * parent);

} // namespace knx
} // namespace v14
} // namespace Project
