/* This file is generated. */

#include <Project/v14/knx/LdCtrlTaskCtrl2_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v14/knx/KNX_t.h>

namespace Project {
namespace v14 {
namespace knx {

LdCtrlTaskCtrl2_t::LdCtrlTaskCtrl2_t(Base * parent) :
    Base(parent)
{
}

LdCtrlTaskCtrl2_t::~LdCtrlTaskCtrl2_t()
{
}

void LdCtrlTaskCtrl2_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("AppliesTo")) {
            AppliesTo = attribute.value().toString();
            continue;
        }
        if (name == QString("InternalDescription")) {
            InternalDescription = attribute.value().toString();
            continue;
        }
        if (name == QString("LsmIdx")) {
            LsmIdx = attribute.value().toString();
            continue;
        }
        if (name == QString("ObjType")) {
            ObjType = attribute.value().toString();
            continue;
        }
        if (name == QString("Occurrence")) {
            Occurrence = attribute.value().toString();
            continue;
        }
        if (name == QString("Callback")) {
            Callback = attribute.value().toString();
            continue;
        }
        if (name == QString("Address")) {
            Address = attribute.value().toString();
            continue;
        }
        if (name == QString("Seg0")) {
            Seg0 = attribute.value().toString();
            continue;
        }
        if (name == QString("Seg1")) {
            Seg1 = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("OnError")) {
            auto * newOnError = make_LdCtrlBase_OnError_t(this);
            newOnError->read(reader);
            OnError.append(newOnError);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int LdCtrlTaskCtrl2_t::tableColumnCount() const
{
    return 10;
}

QVariant LdCtrlTaskCtrl2_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "LdCtrlTaskCtrl2";
        }
        if (qualifiedName == QString("AppliesTo")) {
            return AppliesTo;
        }
        if (qualifiedName == QString("InternalDescription")) {
            return InternalDescription;
        }
        if (qualifiedName == QString("LsmIdx")) {
            return LsmIdx;
        }
        if (qualifiedName == QString("ObjType")) {
            return ObjType;
        }
        if (qualifiedName == QString("Occurrence")) {
            return Occurrence;
        }
        if (qualifiedName == QString("Callback")) {
            return Callback;
        }
        if (qualifiedName == QString("Address")) {
            return Address;
        }
        if (qualifiedName == QString("Seg0")) {
            return Seg0;
        }
        if (qualifiedName == QString("Seg1")) {
            return Seg1;
        }
        break;
    }
    return QVariant();
}

QVariant LdCtrlTaskCtrl2_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "AppliesTo";
            case 2:
                return "InternalDescription";
            case 3:
                return "LsmIdx";
            case 4:
                return "ObjType";
            case 5:
                return "Occurrence";
            case 6:
                return "Callback";
            case 7:
                return "Address";
            case 8:
                return "Seg0";
            case 9:
                return "Seg1";
            }
        }
    }
    return QVariant();
}

QVariant LdCtrlTaskCtrl2_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "LdCtrlTaskCtrl2";
    case Qt::DecorationRole:
        return QIcon::fromTheme("LdCtrlTaskCtrl2");
    }
    return QVariant();
}

LdCtrlTaskCtrl2_t * make_LdCtrlTaskCtrl2_t(Base * parent)
{
    return new LdCtrlTaskCtrl2_t(parent);
}

} // namespace knx
} // namespace v14
} // namespace Project
