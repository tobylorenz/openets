/* This file is generated. */

#include <Project/v14/knx/Locations_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v14/knx/KNX_t.h>

namespace Project {
namespace v14 {
namespace knx {

Locations_t::Locations_t(Base * parent) :
    Base(parent)
{
}

Locations_t::~Locations_t()
{
}

void Locations_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("Space")) {
            QString newId = reader.attributes().value("Id").toString();
            Q_ASSERT(!newId.isEmpty());
            Space_t * newSpace;
            if (Space.contains(newId)) {
                newSpace = Space[newId];
            } else {
                newSpace = make_Space_t(this);
                Space[newId] = newSpace;
            }
            newSpace->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int Locations_t::tableColumnCount() const
{
    return 1;
}

QVariant Locations_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "Locations";
        }
        break;
    }
    return QVariant();
}

QVariant Locations_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant Locations_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "Locations";
    case Qt::DecorationRole:
        return QIcon::fromTheme("Locations");
    }
    return QVariant();
}

Locations_t * make_Locations_t(Base * parent)
{
    return new Locations_t(parent);
}

} // namespace knx
} // namespace v14
} // namespace Project
