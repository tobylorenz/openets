/* This file is generated. */

#pragma once

#include <Project/xs/UnsignedInt.h>

namespace Project {
namespace v14 {
namespace knx {

using ApplicationProgramStatic_Code_AbsoluteSegment_Address_t = xs::UnsignedInt;

} // namespace knx
} // namespace v14
} // namespace Project
