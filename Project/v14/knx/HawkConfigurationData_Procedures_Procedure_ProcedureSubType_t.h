/* This file is generated. */

#pragma once

#include <Project/v14/knx/LdCtrlProcType_t.h>

namespace Project {
namespace v14 {
namespace knx {

using HawkConfigurationData_Procedures_Procedure_ProcedureSubType_t = LdCtrlProcType_t;

} // namespace knx
} // namespace v14
} // namespace Project
