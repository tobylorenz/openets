/* This file is generated. */

#include <Project/v14/knx/BusInterface_Connectors_Connector_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v14/knx/GroupAddress_t.h>
#include <Project/v14/knx/KNX_t.h>

namespace Project {
namespace v14 {
namespace knx {

BusInterface_Connectors_Connector_t::BusInterface_Connectors_Connector_t(Base * parent) :
    Base(parent)
{
}

BusInterface_Connectors_Connector_t::~BusInterface_Connectors_Connector_t()
{
}

void BusInterface_Connectors_Connector_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("GroupAddressRefId")) {
            GroupAddressRefId = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int BusInterface_Connectors_Connector_t::tableColumnCount() const
{
    return 2;
}

QVariant BusInterface_Connectors_Connector_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "BusInterface_Connectors_Connector";
        }
        if (qualifiedName == QString("GroupAddressRefId")) {
            return GroupAddressRefId;
        }
        break;
    }
    return QVariant();
}

QVariant BusInterface_Connectors_Connector_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "GroupAddressRefId";
            }
        }
    }
    return QVariant();
}

QVariant BusInterface_Connectors_Connector_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "Connector";
    case Qt::DecorationRole:
        return QIcon::fromTheme("BusInterface_Connectors_Connector");
    }
    return QVariant();
}

GroupAddress_t * BusInterface_Connectors_Connector_t::getGroupAddress() const
{
    if (GroupAddressRefId.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<GroupAddress_t *>(knx->ids[GroupAddressRefId]);
}

BusInterface_Connectors_Connector_t * make_BusInterface_Connectors_Connector_t(Base * parent)
{
    return new BusInterface_Connectors_Connector_t(parent);
}

} // namespace knx
} // namespace v14
} // namespace Project
