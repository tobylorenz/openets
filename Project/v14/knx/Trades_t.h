/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v14/knx/Trade_t.h>

namespace Project {
namespace v14 {
namespace knx {

class Trades_t : public Base
{
    Q_OBJECT

public:
    explicit Trades_t(Base * parent = nullptr);
    virtual ~Trades_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* elements */
    QVector<Trade_t *> Trade;
};

Trades_t * make_Trades_t(Base * parent);

} // namespace knx
} // namespace v14
} // namespace Project
