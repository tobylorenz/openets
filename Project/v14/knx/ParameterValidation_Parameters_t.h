/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v14/knx/IDREF.h>
#include <Project/v14/knx/ParameterValidation_Parameters_ParameterRefRef_t.h>

namespace Project {
namespace v14 {
namespace knx {

class ParameterValidation_Parameters_t : public Base
{
    Q_OBJECT

public:
    explicit ParameterValidation_Parameters_t(Base * parent = nullptr);
    virtual ~ParameterValidation_Parameters_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* elements */
    QMap<IDREF, ParameterValidation_Parameters_ParameterRefRef_t *> ParameterRefRef; // key: RefId
};

ParameterValidation_Parameters_t * make_ParameterValidation_Parameters_t(Base * parent);

} // namespace knx
} // namespace v14
} // namespace Project
