/* This file is generated. */

#include <Project/v14/knx/ApplicationProgramStatic_BusInterfaces_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v14/knx/KNX_t.h>

namespace Project {
namespace v14 {
namespace knx {

ApplicationProgramStatic_BusInterfaces_t::ApplicationProgramStatic_BusInterfaces_t(Base * parent) :
    Base(parent)
{
}

ApplicationProgramStatic_BusInterfaces_t::~ApplicationProgramStatic_BusInterfaces_t()
{
}

void ApplicationProgramStatic_BusInterfaces_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("BusInterface")) {
            QString newId = reader.attributes().value("Id").toString();
            Q_ASSERT(!newId.isEmpty());
            ApplicationProgramStatic_BusInterfaces_BusInterface_t * newBusInterface;
            if (BusInterface.contains(newId)) {
                newBusInterface = BusInterface[newId];
            } else {
                newBusInterface = make_ApplicationProgramStatic_BusInterfaces_BusInterface_t(this);
                BusInterface[newId] = newBusInterface;
            }
            newBusInterface->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ApplicationProgramStatic_BusInterfaces_t::tableColumnCount() const
{
    return 1;
}

QVariant ApplicationProgramStatic_BusInterfaces_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ApplicationProgramStatic_BusInterfaces";
        }
        break;
    }
    return QVariant();
}

QVariant ApplicationProgramStatic_BusInterfaces_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant ApplicationProgramStatic_BusInterfaces_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "BusInterfaces";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ApplicationProgramStatic_BusInterfaces");
    }
    return QVariant();
}

ApplicationProgramStatic_BusInterfaces_t * make_ApplicationProgramStatic_BusInterfaces_t(Base * parent)
{
    return new ApplicationProgramStatic_BusInterfaces_t(parent);
}

} // namespace knx
} // namespace v14
} // namespace Project
