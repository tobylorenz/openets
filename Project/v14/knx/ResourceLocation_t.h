/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v14/knx/ResourceAddrSpace_t.h>
#include <Project/v14/knx/ResourceName_t.h>
#include <Project/xs/UnsignedByte.h>
#include <Project/xs/UnsignedInt.h>
#include <Project/xs/UnsignedShort.h>

namespace Project {
namespace v14 {
namespace knx {

class ResourceLocation_t : public Base
{
    Q_OBJECT

public:
    explicit ResourceLocation_t(Base * parent = nullptr);
    virtual ~ResourceLocation_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    ResourceAddrSpace_t AddressSpace{};
    xs::UnsignedShort InterfaceObjectRef{};
    xs::UnsignedShort PropertyID{};
    xs::UnsignedInt StartAddress{};
    xs::UnsignedByte Occurrence{"0"};
    ResourceName_t PtrResource{};
};

ResourceLocation_t * make_ResourceLocation_t(Base * parent);

} // namespace knx
} // namespace v14
} // namespace Project
