/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v14/knx/LdCtrlBase_t.h>

namespace Project {
namespace v14 {
namespace knx {

/* forward declarations */
class LdCtrlBaseChoose_t;

class LoadProcedure_t : public Base
{
    Q_OBJECT

public:
    explicit LoadProcedure_t(Base * parent = nullptr);
    virtual ~LoadProcedure_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* elements */
    LdCtrlBase_t * LdCtrlBase{};
    // xs:choice LdCtrlBaseChoose_t * Choose{};
};

LoadProcedure_t * make_LoadProcedure_t(Base * parent);

} // namespace knx
} // namespace v14
} // namespace Project
