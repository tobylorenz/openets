/* This file is generated. */

#include <Project/v14/knx/LdCtrlInvokeFunctionProp_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v14/knx/KNX_t.h>

namespace Project {
namespace v14 {
namespace knx {

LdCtrlInvokeFunctionProp_t::LdCtrlInvokeFunctionProp_t(Base * parent) :
    Base(parent)
{
}

LdCtrlInvokeFunctionProp_t::~LdCtrlInvokeFunctionProp_t()
{
}

void LdCtrlInvokeFunctionProp_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("AppliesTo")) {
            AppliesTo = attribute.value().toString();
            continue;
        }
        if (name == QString("InternalDescription")) {
            InternalDescription = attribute.value().toString();
            continue;
        }
        if (name == QString("ObjIdx")) {
            ObjIdx = attribute.value().toString();
            continue;
        }
        if (name == QString("ObjType")) {
            ObjType = attribute.value().toString();
            continue;
        }
        if (name == QString("Occurrence")) {
            Occurrence = attribute.value().toString();
            continue;
        }
        if (name == QString("PropId")) {
            PropId = attribute.value().toString();
            continue;
        }
        if (name == QString("InlineData")) {
            InlineData = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("OnError")) {
            auto * newOnError = make_LdCtrlBase_OnError_t(this);
            newOnError->read(reader);
            OnError.append(newOnError);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int LdCtrlInvokeFunctionProp_t::tableColumnCount() const
{
    return 8;
}

QVariant LdCtrlInvokeFunctionProp_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "LdCtrlInvokeFunctionProp";
        }
        if (qualifiedName == QString("AppliesTo")) {
            return AppliesTo;
        }
        if (qualifiedName == QString("InternalDescription")) {
            return InternalDescription;
        }
        if (qualifiedName == QString("ObjIdx")) {
            return ObjIdx;
        }
        if (qualifiedName == QString("ObjType")) {
            return ObjType;
        }
        if (qualifiedName == QString("Occurrence")) {
            return Occurrence;
        }
        if (qualifiedName == QString("PropId")) {
            return PropId;
        }
        if (qualifiedName == QString("InlineData")) {
            return InlineData;
        }
        break;
    }
    return QVariant();
}

QVariant LdCtrlInvokeFunctionProp_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "AppliesTo";
            case 2:
                return "InternalDescription";
            case 3:
                return "ObjIdx";
            case 4:
                return "ObjType";
            case 5:
                return "Occurrence";
            case 6:
                return "PropId";
            case 7:
                return "InlineData";
            }
        }
    }
    return QVariant();
}

QVariant LdCtrlInvokeFunctionProp_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "LdCtrlInvokeFunctionProp";
    case Qt::DecorationRole:
        return QIcon::fromTheme("LdCtrlInvokeFunctionProp");
    }
    return QVariant();
}

LdCtrlInvokeFunctionProp_t * make_LdCtrlInvokeFunctionProp_t(Base * parent)
{
    return new LdCtrlInvokeFunctionProp_t(parent);
}

} // namespace knx
} // namespace v14
} // namespace Project
