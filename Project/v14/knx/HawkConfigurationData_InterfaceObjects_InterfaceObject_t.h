/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v14/knx/HawkConfigurationData_InterfaceObjects_InterfaceObject_Property_t.h>
#include <Project/xs/UnsignedByte.h>
#include <Project/xs/UnsignedShort.h>

namespace Project {
namespace v14 {
namespace knx {

class HawkConfigurationData_InterfaceObjects_InterfaceObject_t : public Base
{
    Q_OBJECT

public:
    explicit HawkConfigurationData_InterfaceObjects_InterfaceObject_t(Base * parent = nullptr);
    virtual ~HawkConfigurationData_InterfaceObjects_InterfaceObject_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::UnsignedByte Index{};
    xs::UnsignedShort ObjectType{};

    /* elements */
    QVector<HawkConfigurationData_InterfaceObjects_InterfaceObject_Property_t *> Property;
};

HawkConfigurationData_InterfaceObjects_InterfaceObject_t * make_HawkConfigurationData_InterfaceObjects_InterfaceObject_t(Base * parent);

} // namespace knx
} // namespace v14
} // namespace Project
