/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v14/knx/ParameterValidation_t.h>

namespace Project {
namespace v14 {
namespace knx {

class ApplicationProgramStatic_ParameterValidations_t : public Base
{
    Q_OBJECT

public:
    explicit ApplicationProgramStatic_ParameterValidations_t(Base * parent = nullptr);
    virtual ~ApplicationProgramStatic_ParameterValidations_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* elements */
    QMap<xs::ID, ParameterValidation_t *> ParameterValidation; // key: Id
};

ApplicationProgramStatic_ParameterValidations_t * make_ApplicationProgramStatic_ParameterValidations_t(Base * parent);

} // namespace knx
} // namespace v14
} // namespace Project
