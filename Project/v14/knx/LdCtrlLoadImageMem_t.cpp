/* This file is generated. */

#include <Project/v14/knx/LdCtrlLoadImageMem_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v14/knx/KNX_t.h>

namespace Project {
namespace v14 {
namespace knx {

LdCtrlLoadImageMem_t::LdCtrlLoadImageMem_t(Base * parent) :
    Base(parent)
{
}

LdCtrlLoadImageMem_t::~LdCtrlLoadImageMem_t()
{
}

void LdCtrlLoadImageMem_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("AppliesTo")) {
            AppliesTo = attribute.value().toString();
            continue;
        }
        if (name == QString("InternalDescription")) {
            InternalDescription = attribute.value().toString();
            continue;
        }
        if (name == QString("AddressSpace")) {
            AddressSpace = attribute.value().toString();
            continue;
        }
        if (name == QString("Address")) {
            Address = attribute.value().toString();
            continue;
        }
        if (name == QString("Size")) {
            Size = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("OnError")) {
            auto * newOnError = make_LdCtrlBase_OnError_t(this);
            newOnError->read(reader);
            OnError.append(newOnError);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int LdCtrlLoadImageMem_t::tableColumnCount() const
{
    return 6;
}

QVariant LdCtrlLoadImageMem_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "LdCtrlLoadImageMem";
        }
        if (qualifiedName == QString("AppliesTo")) {
            return AppliesTo;
        }
        if (qualifiedName == QString("InternalDescription")) {
            return InternalDescription;
        }
        if (qualifiedName == QString("AddressSpace")) {
            return AddressSpace;
        }
        if (qualifiedName == QString("Address")) {
            return Address;
        }
        if (qualifiedName == QString("Size")) {
            return Size;
        }
        break;
    }
    return QVariant();
}

QVariant LdCtrlLoadImageMem_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "AppliesTo";
            case 2:
                return "InternalDescription";
            case 3:
                return "AddressSpace";
            case 4:
                return "Address";
            case 5:
                return "Size";
            }
        }
    }
    return QVariant();
}

QVariant LdCtrlLoadImageMem_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "LdCtrlLoadImageMem";
    case Qt::DecorationRole:
        return QIcon::fromTheme("LdCtrlLoadImageMem");
    }
    return QVariant();
}

LdCtrlLoadImageMem_t * make_LdCtrlLoadImageMem_t(Base * parent)
{
    return new LdCtrlLoadImageMem_t(parent);
}

} // namespace knx
} // namespace v14
} // namespace Project
