/* This file is generated. */

#include <Project/v14/knx/Topology_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v14/knx/KNX_t.h>
#include <Project/v14/knx/Topology_UnassignedDevices_t.h>

namespace Project {
namespace v14 {
namespace knx {

Topology_t::Topology_t(Base * parent) :
    Base(parent)
{
}

Topology_t::~Topology_t()
{
}

void Topology_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("Area")) {
            auto * newArea = make_Topology_Area_t(this);
            newArea->read(reader);
            Area.append(newArea);
            continue;
        }
        if (reader.name() == QString("UnassignedDevices")) {
            if (!UnassignedDevices) {
                UnassignedDevices = make_Topology_UnassignedDevices_t(this);
            }
            UnassignedDevices->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int Topology_t::tableColumnCount() const
{
    return 1;
}

QVariant Topology_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "Topology";
        }
        break;
    }
    return QVariant();
}

QVariant Topology_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant Topology_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "Topology";
    case Qt::DecorationRole:
        return QIcon::fromTheme("Topology");
    }
    return QVariant();
}

Topology_t * make_Topology_t(Base * parent)
{
    return new Topology_t(parent);
}

} // namespace knx
} // namespace v14
} // namespace Project
