/* This file is generated. */

#include <Project/v14/knx/Hardware_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v14/knx/Hardware_Hardware2Programs_t.h>
#include <Project/v14/knx/Hardware_Products_t.h>
#include <Project/v14/knx/KNX_t.h>
#include <Project/v14/knx/MasterData_Manufacturers_Manufacturer_t.h>

namespace Project {
namespace v14 {
namespace knx {

Hardware_t::Hardware_t(Base * parent) :
    Base(parent)
{
}

Hardware_t::~Hardware_t()
{
}

void Hardware_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == "Id") {
            Id = attribute.value().toString();
            KNX_t * knx = findParent<KNX_t *>();
            Q_ASSERT(knx);
            knx->ids[Id] = this;
            continue;
        }
        if (name == QString("Name")) {
            Name = attribute.value().toString();
            continue;
        }
        if (name == QString("SerialNumber")) {
            SerialNumber = attribute.value().toString();
            continue;
        }
        if (name == QString("VersionNumber")) {
            VersionNumber = attribute.value().toString();
            continue;
        }
        if (name == QString("BusCurrent")) {
            BusCurrent = attribute.value().toString();
            continue;
        }
        if (name == QString("IsAccessory")) {
            IsAccessory = attribute.value().toString();
            continue;
        }
        if (name == QString("HasIndividualAddress")) {
            HasIndividualAddress = attribute.value().toString();
            continue;
        }
        if (name == QString("HasApplicationProgram")) {
            HasApplicationProgram = attribute.value().toString();
            continue;
        }
        if (name == QString("HasApplicationProgram2")) {
            HasApplicationProgram2 = attribute.value().toString();
            continue;
        }
        if (name == QString("IsPowerSupply")) {
            IsPowerSupply = attribute.value().toString();
            continue;
        }
        if (name == QString("IsChoke")) {
            IsChoke = attribute.value().toString();
            continue;
        }
        if (name == QString("IsCoupler")) {
            IsCoupler = attribute.value().toString();
            continue;
        }
        if (name == QString("IsPowerLineRepeater")) {
            IsPowerLineRepeater = attribute.value().toString();
            continue;
        }
        if (name == QString("IsPowerLineSignalFilter")) {
            IsPowerLineSignalFilter = attribute.value().toString();
            continue;
        }
        if (name == QString("IsCable")) {
            IsCable = attribute.value().toString();
            continue;
        }
        if (name == QString("IsIPEnabled")) {
            IsIPEnabled = attribute.value().toString();
            continue;
        }
        if (name == QString("IsRFRetransmitter")) {
            IsRFRetransmitter = attribute.value().toString();
            continue;
        }
        if (name == QString("RuntimeUnidirectional")) {
            RuntimeUnidirectional = attribute.value().toString();
            continue;
        }
        if (name == QString("OriginalManufacturer")) {
            OriginalManufacturer = attribute.value().toString();
            continue;
        }
        if (name == QString("RFDeviceMode")) {
            RFDeviceMode = attribute.value().toString();
            continue;
        }
        if (name == QString("NoDownloadWithoutPlugin")) {
            NoDownloadWithoutPlugin = attribute.value().toString();
            continue;
        }
        if (name == QString("NonRegRelevantDataVersion")) {
            NonRegRelevantDataVersion = attribute.value().toString();
            continue;
        }
        if (name == QString("InternalDescription")) {
            InternalDescription = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("Products")) {
            if (!Products) {
                Products = make_Hardware_Products_t(this);
            }
            Products->read(reader);
            continue;
        }
        if (reader.name() == QString("Hardware2Programs")) {
            if (!Hardware2Programs) {
                Hardware2Programs = make_Hardware_Hardware2Programs_t(this);
            }
            Hardware2Programs->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int Hardware_t::tableColumnCount() const
{
    return 24;
}

QVariant Hardware_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "Hardware";
        }
        if (qualifiedName == QString("Id")) {
            return Id;
        }
        if (qualifiedName == QString("Name")) {
            return Name;
        }
        if (qualifiedName == QString("SerialNumber")) {
            return SerialNumber;
        }
        if (qualifiedName == QString("VersionNumber")) {
            return VersionNumber;
        }
        if (qualifiedName == QString("BusCurrent")) {
            return BusCurrent;
        }
        if (qualifiedName == QString("IsAccessory")) {
            return IsAccessory;
        }
        if (qualifiedName == QString("HasIndividualAddress")) {
            return HasIndividualAddress;
        }
        if (qualifiedName == QString("HasApplicationProgram")) {
            return HasApplicationProgram;
        }
        if (qualifiedName == QString("HasApplicationProgram2")) {
            return HasApplicationProgram2;
        }
        if (qualifiedName == QString("IsPowerSupply")) {
            return IsPowerSupply;
        }
        if (qualifiedName == QString("IsChoke")) {
            return IsChoke;
        }
        if (qualifiedName == QString("IsCoupler")) {
            return IsCoupler;
        }
        if (qualifiedName == QString("IsPowerLineRepeater")) {
            return IsPowerLineRepeater;
        }
        if (qualifiedName == QString("IsPowerLineSignalFilter")) {
            return IsPowerLineSignalFilter;
        }
        if (qualifiedName == QString("IsCable")) {
            return IsCable;
        }
        if (qualifiedName == QString("IsIPEnabled")) {
            return IsIPEnabled;
        }
        if (qualifiedName == QString("IsRFRetransmitter")) {
            return IsRFRetransmitter;
        }
        if (qualifiedName == QString("RuntimeUnidirectional")) {
            return RuntimeUnidirectional;
        }
        if (qualifiedName == QString("OriginalManufacturer")) {
            return OriginalManufacturer;
        }
        if (qualifiedName == QString("RFDeviceMode")) {
            return RFDeviceMode;
        }
        if (qualifiedName == QString("NoDownloadWithoutPlugin")) {
            return NoDownloadWithoutPlugin;
        }
        if (qualifiedName == QString("NonRegRelevantDataVersion")) {
            return NonRegRelevantDataVersion;
        }
        if (qualifiedName == QString("InternalDescription")) {
            return InternalDescription;
        }
        break;
    }
    return QVariant();
}

QVariant Hardware_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Id";
            case 2:
                return "Name";
            case 3:
                return "SerialNumber";
            case 4:
                return "VersionNumber";
            case 5:
                return "BusCurrent";
            case 6:
                return "IsAccessory";
            case 7:
                return "HasIndividualAddress";
            case 8:
                return "HasApplicationProgram";
            case 9:
                return "HasApplicationProgram2";
            case 10:
                return "IsPowerSupply";
            case 11:
                return "IsChoke";
            case 12:
                return "IsCoupler";
            case 13:
                return "IsPowerLineRepeater";
            case 14:
                return "IsPowerLineSignalFilter";
            case 15:
                return "IsCable";
            case 16:
                return "IsIPEnabled";
            case 17:
                return "IsRFRetransmitter";
            case 18:
                return "RuntimeUnidirectional";
            case 19:
                return "OriginalManufacturer";
            case 20:
                return "RFDeviceMode";
            case 21:
                return "NoDownloadWithoutPlugin";
            case 22:
                return "NonRegRelevantDataVersion";
            case 23:
                return "InternalDescription";
            }
        }
    }
    return QVariant();
}

QVariant Hardware_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return QString("[%1 %2] %3").arg(SerialNumber).arg(VersionNumber).arg(Name);
    case Qt::DecorationRole:
        return QIcon::fromTheme("Hardware");
    }
    return QVariant();
}

MasterData_Manufacturers_Manufacturer_t * Hardware_t::getOriginalManufacturer() const
{
    if (OriginalManufacturer.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<MasterData_Manufacturers_Manufacturer_t *>(knx->ids[OriginalManufacturer]);
}

Hardware_t * make_Hardware_t(Base * parent)
{
    return new Hardware_t(parent);
}

} // namespace knx
} // namespace v14
} // namespace Project
