/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v14/knx/ParameterType_TypeTime_SizeInBit_t.h>
#include <Project/v14/knx/ParameterType_TypeTime_UIHint_t.h>
#include <Project/v14/knx/ParameterType_TypeTime_Unit_t.h>
#include <Project/xs/Long.h>

namespace Project {
namespace v14 {
namespace knx {

class ParameterType_TypeTime_t : public Base
{
    Q_OBJECT

public:
    explicit ParameterType_TypeTime_t(Base * parent = nullptr);
    virtual ~ParameterType_TypeTime_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    ParameterType_TypeTime_SizeInBit_t SizeInBit{};
    ParameterType_TypeTime_Unit_t Unit{};
    xs::Long minInclusive{};
    xs::Long maxInclusive{};
    ParameterType_TypeTime_UIHint_t UIHint{};
};

ParameterType_TypeTime_t * make_ParameterType_TypeTime_t(Base * parent);

} // namespace knx
} // namespace v14
} // namespace Project
