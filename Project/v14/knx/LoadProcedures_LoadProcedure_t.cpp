/* This file is generated. */

#include <Project/v14/knx/LoadProcedures_LoadProcedure_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v14/knx/KNX_t.h>
#include <Project/v14/knx/LdCtrlBaseChoose_t.h>

namespace Project {
namespace v14 {
namespace knx {

LoadProcedures_LoadProcedure_t::LoadProcedures_LoadProcedure_t(Base * parent) :
    Base(parent)
{
}

LoadProcedures_LoadProcedure_t::~LoadProcedures_LoadProcedure_t()
{
}

void LoadProcedures_LoadProcedure_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("MergeId")) {
            MergeId = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("LdCtrlBase")) {
            if (!LdCtrlBase) {
                LdCtrlBase = make_LdCtrlBase_t(this);
            }
            LdCtrlBase->read(reader);
            continue;
        }
        if (reader.name() == QString("choose")) {
            auto * Choose = make_LdCtrlBaseChoose_t(this);
            Choose->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int LoadProcedures_LoadProcedure_t::tableColumnCount() const
{
    return 2;
}

QVariant LoadProcedures_LoadProcedure_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "LoadProcedures_LoadProcedure";
        }
        if (qualifiedName == QString("MergeId")) {
            return MergeId;
        }
        break;
    }
    return QVariant();
}

QVariant LoadProcedures_LoadProcedure_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "MergeId";
            }
        }
    }
    return QVariant();
}

QVariant LoadProcedures_LoadProcedure_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "LoadProcedure";
    case Qt::DecorationRole:
        return QIcon::fromTheme("LoadProcedures_LoadProcedure");
    }
    return QVariant();
}

LoadProcedures_LoadProcedure_t * make_LoadProcedures_LoadProcedure_t(Base * parent)
{
    return new LoadProcedures_LoadProcedure_t(parent);
}

} // namespace knx
} // namespace v14
} // namespace Project
