/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v14/knx/ParameterType_TypeDate_Encoding_t.h>
#include <Project/xs/Boolean.h>

namespace Project {
namespace v14 {
namespace knx {

class ParameterType_TypeDate_t : public Base
{
    Q_OBJECT

public:
    explicit ParameterType_TypeDate_t(Base * parent = nullptr);
    virtual ~ParameterType_TypeDate_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    ParameterType_TypeDate_Encoding_t Encoding{};
    xs::Boolean DisplayTheYear{"true"};
};

ParameterType_TypeDate_t * make_ParameterType_TypeDate_t(Base * parent);

} // namespace knx
} // namespace v14
} // namespace Project
