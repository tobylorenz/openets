/* This file is generated. */

#include <Project/v14/knx/LdCtrlBaseChoose_when_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v14/knx/KNX_t.h>
#include <Project/v14/knx/LdCtrlBaseChoose_t.h>

namespace Project {
namespace v14 {
namespace knx {

LdCtrlBaseChoose_when_t::LdCtrlBaseChoose_when_t(Base * parent) :
    Base(parent)
{
}

LdCtrlBaseChoose_when_t::~LdCtrlBaseChoose_when_t()
{
}

void LdCtrlBaseChoose_when_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("test")) {
            Test = attribute.value().toString();
            continue;
        }
        if (name == QString("default")) {
            Default = attribute.value().toString();
            continue;
        }
        if (name == QString("InternalDescription")) {
            InternalDescription = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("LdCtrlBase")) {
            if (!LdCtrlBase) {
                LdCtrlBase = make_LdCtrlBase_t(this);
            }
            LdCtrlBase->read(reader);
            continue;
        }
        if (reader.name() == QString("choose")) {
            auto * Choose = make_LdCtrlBaseChoose_t(this);
            Choose->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int LdCtrlBaseChoose_when_t::tableColumnCount() const
{
    return 4;
}

QVariant LdCtrlBaseChoose_when_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "LdCtrlBaseChoose_when";
        }
        if (qualifiedName == QString("test")) {
            return Test;
        }
        if (qualifiedName == QString("default")) {
            return Default;
        }
        if (qualifiedName == QString("InternalDescription")) {
            return InternalDescription;
        }
        break;
    }
    return QVariant();
}

QVariant LdCtrlBaseChoose_when_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Test";
            case 2:
                return "Default";
            case 3:
                return "InternalDescription";
            }
        }
    }
    return QVariant();
}

QVariant LdCtrlBaseChoose_when_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "when";
    case Qt::DecorationRole:
        return QIcon::fromTheme("LdCtrlBaseChoose_when");
    }
    return QVariant();
}

LdCtrlBaseChoose_when_t * make_LdCtrlBaseChoose_when_t(Base * parent)
{
    return new LdCtrlBaseChoose_when_t(parent);
}

} // namespace knx
} // namespace v14
} // namespace Project
