/* This file is generated. */

#pragma once

#include <Project/xs/String.h>

namespace Project {
namespace v14 {
namespace knx {

using Hardware_Products_Product_Attributes_Attribute_Name_t = xs::String;

} // namespace knx
} // namespace v14
} // namespace Project
