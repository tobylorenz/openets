/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v14/knx/AccessLevel_t.h>
#include <Project/v14/knx/LdCtrlBase_OnError_t.h>
#include <Project/v14/knx/LdCtrlDeclarePropDesc_MaxElements_t.h>
#include <Project/v14/knx/LdCtrlProcType_t.h>
#include <Project/v14/knx/PropType_t.h>
#include <Project/xs/Boolean.h>
#include <Project/xs/String.h>
#include <Project/xs/UnsignedByte.h>
#include <Project/xs/UnsignedShort.h>

namespace Project {
namespace v14 {
namespace knx {

class LdCtrlDeclarePropDesc_t : public Base
{
    Q_OBJECT

public:
    explicit LdCtrlDeclarePropDesc_t(Base * parent = nullptr);
    virtual ~LdCtrlDeclarePropDesc_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    LdCtrlProcType_t AppliesTo{"auto"};
    xs::String InternalDescription{};
    xs::UnsignedByte ObjIdx{};
    xs::UnsignedShort ObjType{};
    xs::UnsignedByte Occurrence{"0"};
    xs::UnsignedByte PropId{};
    PropType_t PropType{};
    LdCtrlDeclarePropDesc_MaxElements_t MaxElements{};
    AccessLevel_t ReadAccess{};
    AccessLevel_t WriteAccess{};
    xs::Boolean Writable{};

    /* elements */
    QVector<LdCtrlBase_OnError_t *> OnError;
};

LdCtrlDeclarePropDesc_t * make_LdCtrlDeclarePropDesc_t(Base * parent);

} // namespace knx
} // namespace v14
} // namespace Project
