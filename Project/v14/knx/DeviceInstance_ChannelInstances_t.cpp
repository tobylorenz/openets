/* This file is generated. */

#include <Project/v14/knx/DeviceInstance_ChannelInstances_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v14/knx/KNX_t.h>

namespace Project {
namespace v14 {
namespace knx {

DeviceInstance_ChannelInstances_t::DeviceInstance_ChannelInstances_t(Base * parent) :
    Base(parent)
{
}

DeviceInstance_ChannelInstances_t::~DeviceInstance_ChannelInstances_t()
{
}

void DeviceInstance_ChannelInstances_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("ChannelInstance")) {
            QString newId = reader.attributes().value("Id").toString();
            Q_ASSERT(!newId.isEmpty());
            ChannelInstance_t * newChannelInstance;
            if (ChannelInstance.contains(newId)) {
                newChannelInstance = ChannelInstance[newId];
            } else {
                newChannelInstance = make_ChannelInstance_t(this);
                ChannelInstance[newId] = newChannelInstance;
            }
            newChannelInstance->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int DeviceInstance_ChannelInstances_t::tableColumnCount() const
{
    return 1;
}

QVariant DeviceInstance_ChannelInstances_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "DeviceInstance_ChannelInstances";
        }
        break;
    }
    return QVariant();
}

QVariant DeviceInstance_ChannelInstances_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant DeviceInstance_ChannelInstances_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "ChannelInstances";
    case Qt::DecorationRole:
        return QIcon::fromTheme("DeviceInstance_ChannelInstances");
    }
    return QVariant();
}

DeviceInstance_ChannelInstances_t * make_DeviceInstance_ChannelInstances_t(Base * parent)
{
    return new DeviceInstance_ChannelInstances_t(parent);
}

} // namespace knx
} // namespace v14
} // namespace Project
