/* This file is generated. */

#include <Project/v14/knx/ParameterType_TypeRawData_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v14/knx/KNX_t.h>

namespace Project {
namespace v14 {
namespace knx {

ParameterType_TypeRawData_t::ParameterType_TypeRawData_t(Base * parent) :
    Base(parent)
{
}

ParameterType_TypeRawData_t::~ParameterType_TypeRawData_t()
{
}

void ParameterType_TypeRawData_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("MaxSize")) {
            MaxSize = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ParameterType_TypeRawData_t::tableColumnCount() const
{
    return 2;
}

QVariant ParameterType_TypeRawData_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ParameterType_TypeRawData";
        }
        if (qualifiedName == QString("MaxSize")) {
            return MaxSize;
        }
        break;
    }
    return QVariant();
}

QVariant ParameterType_TypeRawData_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "MaxSize";
            }
        }
    }
    return QVariant();
}

QVariant ParameterType_TypeRawData_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "TypeRawData";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ParameterType_TypeRawData");
    }
    return QVariant();
}

ParameterType_TypeRawData_t * make_ParameterType_TypeRawData_t(Base * parent)
{
    return new ParameterType_TypeRawData_t(parent);
}

} // namespace knx
} // namespace v14
} // namespace Project
