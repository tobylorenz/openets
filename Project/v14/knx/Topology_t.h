/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v14/knx/Topology_Area_t.h>

namespace Project {
namespace v14 {
namespace knx {

/* forward declarations */
class Topology_UnassignedDevices_t;

class Topology_t : public Base
{
    Q_OBJECT

public:
    explicit Topology_t(Base * parent = nullptr);
    virtual ~Topology_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* elements */
    QVector<Topology_Area_t *> Area;
    Topology_UnassignedDevices_t * UnassignedDevices{};
};

Topology_t * make_Topology_t(Base * parent);

} // namespace knx
} // namespace v14
} // namespace Project
