/* This file is generated. */

#include <Project/v14/knx/MaskVersion_DownwardCompatibleMasks_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v14/knx/KNX_t.h>

namespace Project {
namespace v14 {
namespace knx {

MaskVersion_DownwardCompatibleMasks_t::MaskVersion_DownwardCompatibleMasks_t(Base * parent) :
    Base(parent)
{
}

MaskVersion_DownwardCompatibleMasks_t::~MaskVersion_DownwardCompatibleMasks_t()
{
}

void MaskVersion_DownwardCompatibleMasks_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("DownwardCompatibleMask")) {
            QString newRefId = reader.attributes().value("RefId").toString();
            Q_ASSERT(!newRefId.isEmpty());
            MaskVersion_DownwardCompatibleMasks_DownwardCompatibleMask_t * newDownwardCompatibleMask;
            if (DownwardCompatibleMask.contains(newRefId)) {
                newDownwardCompatibleMask = DownwardCompatibleMask[newRefId];
            } else {
                newDownwardCompatibleMask = make_MaskVersion_DownwardCompatibleMasks_DownwardCompatibleMask_t(this);
                DownwardCompatibleMask[newRefId] = newDownwardCompatibleMask;
            }
            newDownwardCompatibleMask->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int MaskVersion_DownwardCompatibleMasks_t::tableColumnCount() const
{
    return 1;
}

QVariant MaskVersion_DownwardCompatibleMasks_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "MaskVersion_DownwardCompatibleMasks";
        }
        break;
    }
    return QVariant();
}

QVariant MaskVersion_DownwardCompatibleMasks_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant MaskVersion_DownwardCompatibleMasks_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "DownwardCompatibleMasks";
    case Qt::DecorationRole:
        return QIcon::fromTheme("MaskVersion_DownwardCompatibleMasks");
    }
    return QVariant();
}

MaskVersion_DownwardCompatibleMasks_t * make_MaskVersion_DownwardCompatibleMasks_t(Base * parent)
{
    return new MaskVersion_DownwardCompatibleMasks_t(parent);
}

} // namespace knx
} // namespace v14
} // namespace Project
