/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v14/knx/MemoryType_t.h>
#include <Project/xs/Boolean.h>
#include <Project/xs/UnsignedInt.h>

namespace Project {
namespace v14 {
namespace knx {

/* forward declarations */
class HawkConfigurationData_MemorySegments_MemorySegment_AccessRights_t;
class ResourceLocation_t;

class HawkConfigurationData_MemorySegments_MemorySegment_t : public Base
{
    Q_OBJECT

public:
    explicit HawkConfigurationData_MemorySegments_MemorySegment_t(Base * parent = nullptr);
    virtual ~HawkConfigurationData_MemorySegments_MemorySegment_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::UnsignedInt Length{};
    xs::Boolean Optional{"false"};
    MemoryType_t MemoryType{};

    /* elements */
    ResourceLocation_t * Location{};
    HawkConfigurationData_MemorySegments_MemorySegment_AccessRights_t * AccessRights{};
};

HawkConfigurationData_MemorySegments_MemorySegment_t * make_HawkConfigurationData_MemorySegments_MemorySegment_t(Base * parent);

} // namespace knx
} // namespace v14
} // namespace Project
