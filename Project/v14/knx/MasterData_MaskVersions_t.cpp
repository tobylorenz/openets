/* This file is generated. */

#include <Project/v14/knx/MasterData_MaskVersions_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v14/knx/KNX_t.h>

namespace Project {
namespace v14 {
namespace knx {

MasterData_MaskVersions_t::MasterData_MaskVersions_t(Base * parent) :
    Base(parent)
{
}

MasterData_MaskVersions_t::~MasterData_MaskVersions_t()
{
}

void MasterData_MaskVersions_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("MaskVersion")) {
            QString newId = reader.attributes().value("Id").toString();
            Q_ASSERT(!newId.isEmpty());
            MaskVersion_t * newMaskVersion;
            if (MaskVersion.contains(newId)) {
                newMaskVersion = MaskVersion[newId];
            } else {
                newMaskVersion = make_MaskVersion_t(this);
                MaskVersion[newId] = newMaskVersion;
            }
            newMaskVersion->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int MasterData_MaskVersions_t::tableColumnCount() const
{
    return 1;
}

QVariant MasterData_MaskVersions_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "MasterData_MaskVersions";
        }
        break;
    }
    return QVariant();
}

QVariant MasterData_MaskVersions_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant MasterData_MaskVersions_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "MaskVersions";
    case Qt::DecorationRole:
        return QIcon::fromTheme("MasterData_MaskVersions");
    }
    return QVariant();
}

MasterData_MaskVersions_t * make_MasterData_MaskVersions_t(Base * parent)
{
    return new MasterData_MaskVersions_t(parent);
}

} // namespace knx
} // namespace v14
} // namespace Project
