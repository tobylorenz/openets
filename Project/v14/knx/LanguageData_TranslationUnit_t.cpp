/* This file is generated. */

#include <Project/v14/knx/LanguageData_TranslationUnit_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v14/knx/CatalogSection_t.h>
#include <Project/v14/knx/KNX_t.h>

namespace Project {
namespace v14 {
namespace knx {

LanguageData_TranslationUnit_t::LanguageData_TranslationUnit_t(Base * parent) :
    Base(parent)
{
}

LanguageData_TranslationUnit_t::~LanguageData_TranslationUnit_t()
{
}

void LanguageData_TranslationUnit_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("RefId")) {
            RefId = attribute.value().toString();
            continue;
        }
        if (name == QString("Version")) {
            Version = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("TranslationElement")) {
            QString newRefId = reader.attributes().value("RefId").toString();
            Q_ASSERT(!newRefId.isEmpty());
            LanguageData_TranslationUnit_TranslationElement_t * newTranslationElement;
            if (TranslationElement.contains(newRefId)) {
                newTranslationElement = TranslationElement[newRefId];
            } else {
                newTranslationElement = make_LanguageData_TranslationUnit_TranslationElement_t(this);
                TranslationElement[newRefId] = newTranslationElement;
            }
            newTranslationElement->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int LanguageData_TranslationUnit_t::tableColumnCount() const
{
    return 3;
}

QVariant LanguageData_TranslationUnit_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "LanguageData_TranslationUnit";
        }
        if (qualifiedName == QString("RefId")) {
            return RefId;
        }
        if (qualifiedName == QString("Version")) {
            return Version;
        }
        break;
    }
    return QVariant();
}

QVariant LanguageData_TranslationUnit_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "RefId";
            case 2:
                return "Version";
            }
        }
    }
    return QVariant();
}

QVariant LanguageData_TranslationUnit_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "TranslationUnit";
    case Qt::DecorationRole:
        return QIcon::fromTheme("LanguageData_TranslationUnit");
    }
    return QVariant();
}

CatalogSection_t * LanguageData_TranslationUnit_t::getCatalogSection() const
{
    if (RefId.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<CatalogSection_t *>(knx->ids[RefId]);
}

LanguageData_TranslationUnit_t * make_LanguageData_TranslationUnit_t(Base * parent)
{
    return new LanguageData_TranslationUnit_t(parent);
}

} // namespace knx
} // namespace v14
} // namespace Project
