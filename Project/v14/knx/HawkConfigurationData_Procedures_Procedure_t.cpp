/* This file is generated. */

#include <Project/v14/knx/HawkConfigurationData_Procedures_Procedure_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v14/knx/KNX_t.h>
#include <Project/v14/knx/LdCtrlBaseChoose_t.h>

namespace Project {
namespace v14 {
namespace knx {

HawkConfigurationData_Procedures_Procedure_t::HawkConfigurationData_Procedures_Procedure_t(Base * parent) :
    Base(parent)
{
}

HawkConfigurationData_Procedures_Procedure_t::~HawkConfigurationData_Procedures_Procedure_t()
{
}

void HawkConfigurationData_Procedures_Procedure_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("ProcedureType")) {
            ProcedureType = attribute.value().toString();
            continue;
        }
        if (name == QString("ProcedureSubType")) {
            ProcedureSubType = attribute.value().toString();
            continue;
        }
        if (name == QString("Access")) {
            Access = attribute.value().toString().split(' ');
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("LdCtrlBase")) {
            if (!LdCtrlBase) {
                LdCtrlBase = make_LdCtrlBase_t(this);
            }
            LdCtrlBase->read(reader);
            continue;
        }
        if (reader.name() == QString("choose")) {
            auto * Choose = make_LdCtrlBaseChoose_t(this);
            Choose->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int HawkConfigurationData_Procedures_Procedure_t::tableColumnCount() const
{
    return 4;
}

QVariant HawkConfigurationData_Procedures_Procedure_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "HawkConfigurationData_Procedures_Procedure";
        }
        if (qualifiedName == QString("ProcedureType")) {
            return ProcedureType;
        }
        if (qualifiedName == QString("ProcedureSubType")) {
            return ProcedureSubType;
        }
        if (qualifiedName == QString("Access")) {
            return Access.join(' ');
        }
        break;
    }
    return QVariant();
}

QVariant HawkConfigurationData_Procedures_Procedure_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "ProcedureType";
            case 2:
                return "ProcedureSubType";
            case 3:
                return "Access";
            }
        }
    }
    return QVariant();
}

QVariant HawkConfigurationData_Procedures_Procedure_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "Procedure";
    case Qt::DecorationRole:
        return QIcon::fromTheme("HawkConfigurationData_Procedures_Procedure");
    }
    return QVariant();
}

HawkConfigurationData_Procedures_Procedure_t * make_HawkConfigurationData_Procedures_Procedure_t(Base * parent)
{
    return new HawkConfigurationData_Procedures_Procedure_t(parent);
}

} // namespace knx
} // namespace v14
} // namespace Project
