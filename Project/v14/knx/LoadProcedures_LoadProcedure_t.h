/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v14/knx/LdCtrlBase_t.h>
#include <Project/xs/UnsignedByte.h>

namespace Project {
namespace v14 {
namespace knx {

/* forward declarations */
class LdCtrlBaseChoose_t;

class LoadProcedures_LoadProcedure_t : public Base
{
    Q_OBJECT

public:
    explicit LoadProcedures_LoadProcedure_t(Base * parent = nullptr);
    virtual ~LoadProcedures_LoadProcedure_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::UnsignedByte MergeId{};

    /* elements */
    LdCtrlBase_t * LdCtrlBase{};
    // xs:choice LdCtrlBaseChoose_t * Choose{};
};

LoadProcedures_LoadProcedure_t * make_LoadProcedures_LoadProcedure_t(Base * parent);

} // namespace knx
} // namespace v14
} // namespace Project
