/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v14/knx/MasterData_FunctionalBlocks_FunctionalBlock_t.h>
#include <Project/xs/ID.h>

namespace Project {
namespace v14 {
namespace knx {

class MasterData_FunctionalBlocks_t : public Base
{
    Q_OBJECT

public:
    explicit MasterData_FunctionalBlocks_t(Base * parent = nullptr);
    virtual ~MasterData_FunctionalBlocks_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* elements */
    QMap<xs::ID, MasterData_FunctionalBlocks_FunctionalBlock_t *> FunctionalBlock; // key: Id
};

MasterData_FunctionalBlocks_t * make_MasterData_FunctionalBlocks_t(Base * parent);

} // namespace knx
} // namespace v14
} // namespace Project
