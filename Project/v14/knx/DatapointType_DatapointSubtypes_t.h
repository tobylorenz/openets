/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v14/knx/DatapointType_DatapointSubtypes_DatapointSubtype_t.h>
#include <Project/xs/ID.h>

namespace Project {
namespace v14 {
namespace knx {

class DatapointType_DatapointSubtypes_t : public Base
{
    Q_OBJECT

public:
    explicit DatapointType_DatapointSubtypes_t(Base * parent = nullptr);
    virtual ~DatapointType_DatapointSubtypes_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* elements */
    QMap<xs::ID, DatapointType_DatapointSubtypes_DatapointSubtype_t *> DatapointSubtype; // key: Id
};

DatapointType_DatapointSubtypes_t * make_DatapointType_DatapointSubtypes_t(Base * parent);

} // namespace knx
} // namespace v14
} // namespace Project
