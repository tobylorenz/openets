/* This file is generated. */

#include <Project/v14/knx/Assign_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v14/knx/KNX_t.h>
#include <Project/v14/knx/ParameterRefRef_t.h>

namespace Project {
namespace v14 {
namespace knx {

Assign_t::Assign_t(Base * parent) :
    Base(parent)
{
}

Assign_t::~Assign_t()
{
}

void Assign_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("TargetParamRefRef")) {
            TargetParamRefRef = attribute.value().toString();
            continue;
        }
        if (name == QString("SourceParamRefRef")) {
            SourceParamRefRef = attribute.value().toString();
            continue;
        }
        if (name == QString("Value")) {
            Value = attribute.value().toString();
            continue;
        }
        if (name == QString("InternalDescription")) {
            InternalDescription = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int Assign_t::tableColumnCount() const
{
    return 5;
}

QVariant Assign_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "Assign";
        }
        if (qualifiedName == QString("TargetParamRefRef")) {
            return TargetParamRefRef;
        }
        if (qualifiedName == QString("SourceParamRefRef")) {
            return SourceParamRefRef;
        }
        if (qualifiedName == QString("Value")) {
            return Value;
        }
        if (qualifiedName == QString("InternalDescription")) {
            return InternalDescription;
        }
        break;
    }
    return QVariant();
}

QVariant Assign_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "TargetParamRefRef";
            case 2:
                return "SourceParamRefRef";
            case 3:
                return "Value";
            case 4:
                return "InternalDescription";
            }
        }
    }
    return QVariant();
}

QVariant Assign_t::treeData(int role) const
{
    auto * ref = getTargetParameterRefRef();
    if (ref) {
        return ref->treeData(role);
    }

    switch (role) {
    case Qt::DisplayRole:
        return "Assign";
    case Qt::DecorationRole:
        return QIcon::fromTheme("Assign");
    }
    return QVariant();
}

ParameterRefRef_t * Assign_t::getTargetParameterRefRef() const
{
    if (TargetParamRefRef.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<ParameterRefRef_t *>(knx->ids[TargetParamRefRef]);
}

ParameterRefRef_t * Assign_t::getSourceParameterRefRef() const
{
    if (SourceParamRefRef.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<ParameterRefRef_t *>(knx->ids[SourceParamRefRef]);
}

Assign_t * make_Assign_t(Base * parent)
{
    return new Assign_t(parent);
}

} // namespace knx
} // namespace v14
} // namespace Project
