/* This file is generated. */

#include <Project/v14/knx/DatapointType_DatapointSubtypes_DatapointSubtype_Format_Reserved_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v14/knx/KNX_t.h>

namespace Project {
namespace v14 {
namespace knx {

DatapointType_DatapointSubtypes_DatapointSubtype_Format_Reserved_t::DatapointType_DatapointSubtypes_DatapointSubtype_Format_Reserved_t(Base * parent) :
    Base(parent)
{
}

DatapointType_DatapointSubtypes_DatapointSubtype_Format_Reserved_t::~DatapointType_DatapointSubtypes_DatapointSubtype_Format_Reserved_t()
{
}

void DatapointType_DatapointSubtypes_DatapointSubtype_Format_Reserved_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("Width")) {
            Width = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int DatapointType_DatapointSubtypes_DatapointSubtype_Format_Reserved_t::tableColumnCount() const
{
    return 2;
}

QVariant DatapointType_DatapointSubtypes_DatapointSubtype_Format_Reserved_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "DatapointType_DatapointSubtypes_DatapointSubtype_Format_Reserved";
        }
        if (qualifiedName == QString("Width")) {
            return Width;
        }
        break;
    }
    return QVariant();
}

QVariant DatapointType_DatapointSubtypes_DatapointSubtype_Format_Reserved_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Width";
            }
        }
    }
    return QVariant();
}

QVariant DatapointType_DatapointSubtypes_DatapointSubtype_Format_Reserved_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "Reserved";
    case Qt::DecorationRole:
        return QIcon::fromTheme("DatapointType_DatapointSubtypes_DatapointSubtype_Format_Reserved");
    }
    return QVariant();
}

DatapointType_DatapointSubtypes_DatapointSubtype_Format_Reserved_t * make_DatapointType_DatapointSubtypes_DatapointSubtype_Format_Reserved_t(Base * parent)
{
    return new DatapointType_DatapointSubtypes_DatapointSubtype_Format_Reserved_t(parent);
}

} // namespace knx
} // namespace v14
} // namespace Project
