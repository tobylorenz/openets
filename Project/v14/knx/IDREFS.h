/* This file is generated. */

#pragma once

#include <Project/v14/knx/IDREF.h>
#include <Project/xs/List.h>

namespace Project {
namespace v14 {
namespace knx {

using IDREFS = xs::List<IDREF>;

} // namespace knx
} // namespace v14
} // namespace Project
