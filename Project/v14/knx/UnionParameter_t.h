/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v14/knx/Access_t.h>
#include <Project/v14/knx/BitOffset_t.h>
#include <Project/v14/knx/IDREF.h>
#include <Project/v14/knx/LanguageDependentString20_t.h>
#include <Project/v14/knx/LanguageDependentString255_t.h>
#include <Project/v14/knx/LanguageDependentString_t.h>
#include <Project/v14/knx/String50_t.h>
#include <Project/v14/knx/UnionParameter_Offset_t.h>
#include <Project/v14/knx/Value_t.h>
#include <Project/xs/ID.h>
#include <Project/xs/Boolean.h>
#include <Project/xs/String.h>

namespace Project {
namespace v14 {
namespace knx {

/* forward declarations */
class ParameterType_t;

class UnionParameter_t : public Base
{
    Q_OBJECT

public:
    explicit UnionParameter_t(Base * parent = nullptr);
    virtual ~UnionParameter_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::ID Id{};
    String50_t Name{};
    IDREF ParameterType{};
    UnionParameter_Offset_t Offset{};
    BitOffset_t BitOffset{};
    LanguageDependentString255_t Text{};
    LanguageDependentString20_t SuffixText{};
    Access_t Access{"ReadWrite"};
    Value_t Value{};
    LanguageDependentString_t InitialValue{};
    xs::Boolean CustomerAdjustable{"false"};
    xs::Boolean DefaultUnionParameter{"false"};
    xs::String InternalDescription{};

    /* getters */
    ParameterType_t * getParameterType() const; // attribute: ParameterType
};

UnionParameter_t * make_UnionParameter_t(Base * parent);

} // namespace knx
} // namespace v14
} // namespace Project
