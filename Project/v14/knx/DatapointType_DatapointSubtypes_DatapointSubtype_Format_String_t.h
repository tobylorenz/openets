/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v14/knx/LanguageDependentString_t.h>
#include <Project/v14/knx/TextEncoding_t.h>
#include <Project/xs/ID.h>
#include <Project/xs/UnsignedInt.h>

namespace Project {
namespace v14 {
namespace knx {

class DatapointType_DatapointSubtypes_DatapointSubtype_Format_String_t : public Base
{
    Q_OBJECT

public:
    explicit DatapointType_DatapointSubtypes_DatapointSubtype_Format_String_t(Base * parent = nullptr);
    virtual ~DatapointType_DatapointSubtypes_DatapointSubtype_Format_String_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::ID Id{};
    xs::UnsignedInt Width{};
    LanguageDependentString_t Name{};
    LanguageDependentString_t Unit{};
    TextEncoding_t Encoding{};
};

DatapointType_DatapointSubtypes_DatapointSubtype_Format_String_t * make_DatapointType_DatapointSubtypes_DatapointSubtype_Format_String_t(Base * parent);

} // namespace knx
} // namespace v14
} // namespace Project
