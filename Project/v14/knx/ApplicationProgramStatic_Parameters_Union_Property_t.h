/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v14/knx/BitOffset_t.h>
#include <Project/xs/UnsignedByte.h>
#include <Project/xs/UnsignedInt.h>
#include <Project/xs/UnsignedShort.h>

namespace Project {
namespace v14 {
namespace knx {

class ApplicationProgramStatic_Parameters_Union_Property_t : public Base
{
    Q_OBJECT

public:
    explicit ApplicationProgramStatic_Parameters_Union_Property_t(Base * parent = nullptr);
    virtual ~ApplicationProgramStatic_Parameters_Union_Property_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::UnsignedByte ObjectIndex{};
    xs::UnsignedShort ObjectType{};
    xs::UnsignedByte Occurrence{"0"};
    xs::UnsignedByte PropertyId{};
    xs::UnsignedInt Offset{};
    BitOffset_t BitOffset{};
};

ApplicationProgramStatic_Parameters_Union_Property_t * make_ApplicationProgramStatic_Parameters_Union_Property_t(Base * parent);

} // namespace knx
} // namespace v14
} // namespace Project
