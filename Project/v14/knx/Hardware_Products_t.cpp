/* This file is generated. */

#include <Project/v14/knx/Hardware_Products_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v14/knx/KNX_t.h>

namespace Project {
namespace v14 {
namespace knx {

Hardware_Products_t::Hardware_Products_t(Base * parent) :
    Base(parent)
{
}

Hardware_Products_t::~Hardware_Products_t()
{
}

void Hardware_Products_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("Product")) {
            QString newId = reader.attributes().value("Id").toString();
            Q_ASSERT(!newId.isEmpty());
            Hardware_Products_Product_t * newProduct;
            if (Product.contains(newId)) {
                newProduct = Product[newId];
            } else {
                newProduct = make_Hardware_Products_Product_t(this);
                Product[newId] = newProduct;
            }
            newProduct->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int Hardware_Products_t::tableColumnCount() const
{
    return 1;
}

QVariant Hardware_Products_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "Hardware_Products";
        }
        break;
    }
    return QVariant();
}

QVariant Hardware_Products_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant Hardware_Products_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "Products";
    case Qt::DecorationRole:
        return QIcon::fromTheme("Hardware_Products");
    }
    return QVariant();
}

Hardware_Products_t * make_Hardware_Products_t(Base * parent)
{
    return new Hardware_Products_t(parent);
}

} // namespace knx
} // namespace v14
} // namespace Project
