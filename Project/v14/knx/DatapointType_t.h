/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v14/knx/IDREF.h>
#include <Project/v14/knx/LanguageDependentString255_t.h>
#include <Project/v14/knx/String255_t.h>
#include <Project/xs/ID.h>
#include <Project/xs/Boolean.h>
#include <Project/xs/UnsignedInt.h>

namespace Project {
namespace v14 {
namespace knx {

/* forward declarations */
class DatapointType_DatapointSubtypes_t;
class MasterData_PropertyDataTypes_PropertyDataType_t;

class DatapointType_t : public Base
{
    Q_OBJECT

public:
    explicit DatapointType_t(Base * parent = nullptr);
    virtual ~DatapointType_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::ID Id{};
    xs::UnsignedInt Number{};
    String255_t Name{};
    LanguageDependentString255_t Text{};
    xs::UnsignedInt SizeInBit{};
    xs::Boolean Default{};
    IDREF PDT{};

    /* elements */
    DatapointType_DatapointSubtypes_t * DatapointSubtypes{};

    /* getters */
    MasterData_PropertyDataTypes_PropertyDataType_t * getPropertyDataType() const; // attribute: PDT
};

DatapointType_t * make_DatapointType_t(Base * parent);

} // namespace knx
} // namespace v14
} // namespace Project
