/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v14/knx/CellRef_t.h>
#include <Project/v14/knx/IDREF.h>
#include <Project/xs/Byte.h>
#include <Project/xs/String.h>

namespace Project {
namespace v14 {
namespace knx {

/* forward declarations */
class ParameterRef_t;

class ParameterRefRef_t : public Base
{
    Q_OBJECT

public:
    explicit ParameterRefRef_t(Base * parent = nullptr);
    virtual ~ParameterRefRef_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    IDREF RefId{};
    xs::String HelpContext{};
    xs::Byte IndentLevel{"0"};
    xs::String InternalDescription{};
    CellRef_t Cell{};
    xs::String Icon{};

    /* getters */
    ParameterRef_t * getParameterRef() const; // attribute: RefId
};

ParameterRefRef_t * make_ParameterRefRef_t(Base * parent);

} // namespace knx
} // namespace v14
} // namespace Project
