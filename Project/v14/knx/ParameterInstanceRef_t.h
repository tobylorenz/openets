/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v14/knx/IDREF.h>
#include <Project/v14/knx/Value_t.h>
#include <Project/xs/ID.h>
#include <Project/xs/Boolean.h>
#include <Project/xs/String.h>

namespace Project {
namespace v14 {
namespace knx {

/* forward declarations */
class ParameterRef_t;

class ParameterInstanceRef_t : public Base
{
    Q_OBJECT

public:
    explicit ParameterInstanceRef_t(Base * parent = nullptr);
    virtual ~ParameterInstanceRef_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::ID Id{};
    IDREF RefId{};
    Value_t Value{};
    xs::Boolean GrantUseByCustomer{"false"};
    xs::String CustomizedText{};

    /* getters */
    ParameterRef_t * getParameterRef() const; // attribute: RefId
};

ParameterInstanceRef_t * make_ParameterInstanceRef_t(Base * parent);

} // namespace knx
} // namespace v14
} // namespace Project
