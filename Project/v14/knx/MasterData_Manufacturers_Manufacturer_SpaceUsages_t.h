/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v14/knx/SpaceUsage_t.h>

namespace Project {
namespace v14 {
namespace knx {

class MasterData_Manufacturers_Manufacturer_SpaceUsages_t : public Base
{
    Q_OBJECT

public:
    explicit MasterData_Manufacturers_Manufacturer_SpaceUsages_t(Base * parent = nullptr);
    virtual ~MasterData_Manufacturers_Manufacturer_SpaceUsages_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* elements */
    QMap<xs::ID, SpaceUsage_t *> SpaceUsage; // key: Id
};

MasterData_Manufacturers_Manufacturer_SpaceUsages_t * make_MasterData_Manufacturers_Manufacturer_SpaceUsages_t(Base * parent);

} // namespace knx
} // namespace v14
} // namespace Project
