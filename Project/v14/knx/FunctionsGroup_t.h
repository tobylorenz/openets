/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v14/knx/DeprecationStatus_t.h>
#include <Project/v14/knx/FunctionType_t.h>
#include <Project/v14/knx/FunctionsGroup_t.h>
#include <Project/v14/knx/LanguageDependentString255_t.h>
#include <Project/xs/ID.h>
#include <Project/xs/UnsignedInt.h>

namespace Project {
namespace v14 {
namespace knx {

class FunctionsGroup_t : public Base
{
    Q_OBJECT

public:
    explicit FunctionsGroup_t(Base * parent = nullptr);
    virtual ~FunctionsGroup_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::ID Id{};
    xs::UnsignedInt Number{};
    LanguageDependentString255_t Text{};
    LanguageDependentString255_t Description{};
    DeprecationStatus_t Status{"active"};

    /* elements */
    QMap<xs::ID, FunctionsGroup_t *> FunctionsGroup; // key: Id
    QMap<xs::ID, FunctionType_t *> FunctionType; // key: Id
};

FunctionsGroup_t * make_FunctionsGroup_t(Base * parent);

} // namespace knx
} // namespace v14
} // namespace Project
