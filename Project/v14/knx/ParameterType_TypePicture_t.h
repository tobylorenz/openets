/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v14/knx/HorizontalAlignment_t.h>
#include <Project/v14/knx/IDREF.h>

namespace Project {
namespace v14 {
namespace knx {

/* forward declarations */
class ManufacturerData_Manufacturer_Baggages_Baggage_t;

class ParameterType_TypePicture_t : public Base
{
    Q_OBJECT

public:
    explicit ParameterType_TypePicture_t(Base * parent = nullptr);
    virtual ~ParameterType_TypePicture_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    IDREF RefId{};
    HorizontalAlignment_t HorizontalAlignment{"Left"};

    /* getters */
    ManufacturerData_Manufacturer_Baggages_Baggage_t * getBaggage() const; // attribute: RefId
};

ParameterType_TypePicture_t * make_ParameterType_TypePicture_t(Base * parent);

} // namespace knx
} // namespace v14
} // namespace Project
