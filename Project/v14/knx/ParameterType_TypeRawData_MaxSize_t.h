/* This file is generated. */

#pragma once

#include <Project/xs/UnsignedInt.h>

namespace Project {
namespace v14 {
namespace knx {

using ParameterType_TypeRawData_MaxSize_t = xs::UnsignedInt;

} // namespace knx
} // namespace v14
} // namespace Project
