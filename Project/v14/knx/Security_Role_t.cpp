/* This file is generated. */

#include <Project/v14/knx/Security_Role_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v14/knx/KNX_t.h>

namespace Project {
namespace v14 {
namespace knx {

Security_Role_t::Security_Role_t(Base * parent) :
    Base(parent)
{
}

Security_Role_t::~Security_Role_t()
{
}

void Security_Role_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("RefId")) {
            RefId = attribute.value().toString();
            continue;
        }
        if (name == QString("Address")) {
            Address = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int Security_Role_t::tableColumnCount() const
{
    return 3;
}

QVariant Security_Role_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "Security_Role";
        }
        if (qualifiedName == QString("RefId")) {
            return RefId;
        }
        if (qualifiedName == QString("Address")) {
            return Address;
        }
        break;
    }
    return QVariant();
}

QVariant Security_Role_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "RefId";
            case 2:
                return "Address";
            }
        }
    }
    return QVariant();
}

QVariant Security_Role_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "Role";
    case Qt::DecorationRole:
        return QIcon::fromTheme("Security_Role");
    }
    return QVariant();
}

Security_Role_t * make_Security_Role_t(Base * parent)
{
    return new Security_Role_t(parent);
}

} // namespace knx
} // namespace v14
} // namespace Project
