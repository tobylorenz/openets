/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v14/knx/IPConfigAssign_t.h>
#include <Project/v14/knx/Ipv4Address_t.h>
#include <Project/v14/knx/String50_t.h>

namespace Project {
namespace v14 {
namespace knx {

class IPConfig_t : public Base
{
    Q_OBJECT

public:
    explicit IPConfig_t(Base * parent = nullptr);
    virtual ~IPConfig_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    IPConfigAssign_t Assign{"Auto"};
    Ipv4Address_t IPAddress{};
    Ipv4Address_t SubnetMask{};
    Ipv4Address_t DefaultGateway{};
    String50_t MACAddress{};
};

IPConfig_t * make_IPConfig_t(Base * parent);

} // namespace knx
} // namespace v14
} // namespace Project
