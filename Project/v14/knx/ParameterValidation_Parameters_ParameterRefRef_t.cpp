/* This file is generated. */

#include <Project/v14/knx/ParameterValidation_Parameters_ParameterRefRef_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v14/knx/KNX_t.h>

namespace Project {
namespace v14 {
namespace knx {

ParameterValidation_Parameters_ParameterRefRef_t::ParameterValidation_Parameters_ParameterRefRef_t(Base * parent) :
    Base(parent)
{
}

ParameterValidation_Parameters_ParameterRefRef_t::~ParameterValidation_Parameters_ParameterRefRef_t()
{
}

void ParameterValidation_Parameters_ParameterRefRef_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("RefId")) {
            RefId = attribute.value().toString();
            continue;
        }
        if (name == QString("HelpContext")) {
            HelpContext = attribute.value().toString();
            continue;
        }
        if (name == QString("IndentLevel")) {
            IndentLevel = attribute.value().toString();
            continue;
        }
        if (name == QString("InternalDescription")) {
            InternalDescription = attribute.value().toString();
            continue;
        }
        if (name == QString("Cell")) {
            Cell = attribute.value().toString();
            continue;
        }
        if (name == QString("Icon")) {
            Icon = attribute.value().toString();
            continue;
        }
        if (name == QString("AliasName")) {
            AliasName = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ParameterValidation_Parameters_ParameterRefRef_t::tableColumnCount() const
{
    return 8;
}

QVariant ParameterValidation_Parameters_ParameterRefRef_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ParameterValidation_Parameters_ParameterRefRef";
        }
        if (qualifiedName == QString("RefId")) {
            return RefId;
        }
        if (qualifiedName == QString("HelpContext")) {
            return HelpContext;
        }
        if (qualifiedName == QString("IndentLevel")) {
            return IndentLevel;
        }
        if (qualifiedName == QString("InternalDescription")) {
            return InternalDescription;
        }
        if (qualifiedName == QString("Cell")) {
            return Cell;
        }
        if (qualifiedName == QString("Icon")) {
            return Icon;
        }
        if (qualifiedName == QString("AliasName")) {
            return AliasName;
        }
        break;
    }
    return QVariant();
}

QVariant ParameterValidation_Parameters_ParameterRefRef_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "RefId";
            case 2:
                return "HelpContext";
            case 3:
                return "IndentLevel";
            case 4:
                return "InternalDescription";
            case 5:
                return "Cell";
            case 6:
                return "Icon";
            case 7:
                return "AliasName";
            }
        }
    }
    return QVariant();
}

QVariant ParameterValidation_Parameters_ParameterRefRef_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "ParameterRefRef";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ParameterValidation_Parameters_ParameterRefRef");
    }
    return QVariant();
}

ParameterValidation_Parameters_ParameterRefRef_t * make_ParameterValidation_Parameters_ParameterRefRef_t(Base * parent)
{
    return new ParameterValidation_Parameters_ParameterRefRef_t(parent);
}

} // namespace knx
} // namespace v14
} // namespace Project
