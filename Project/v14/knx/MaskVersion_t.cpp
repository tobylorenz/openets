/* This file is generated. */

#include <Project/v14/knx/MaskVersion_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v14/knx/KNX_t.h>
#include <Project/v14/knx/MaskVersion_DownwardCompatibleMasks_t.h>
#include <Project/v14/knx/MaskVersion_MaskEntries_t.h>
#include <Project/v14/knx/MasterData_MediumTypes_MediumType_t.h>

namespace Project {
namespace v14 {
namespace knx {

MaskVersion_t::MaskVersion_t(Base * parent) :
    Base(parent)
{
}

MaskVersion_t::~MaskVersion_t()
{
}

void MaskVersion_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == "Id") {
            Id = attribute.value().toString();
            KNX_t * knx = findParent<KNX_t *>();
            Q_ASSERT(knx);
            knx->ids[Id] = this;
            continue;
        }
        if (name == QString("Name")) {
            Name = attribute.value().toString();
            continue;
        }
        if (name == QString("MaskVersion")) {
            MaskVersion = attribute.value().toString();
            continue;
        }
        if (name == QString("MgmtDescriptor01")) {
            MgmtDescriptor01 = attribute.value().toString();
            continue;
        }
        if (name == QString("ManagementModel")) {
            ManagementModel = attribute.value().toString();
            continue;
        }
        if (name == QString("MediumTypeRefId")) {
            MediumTypeRefId = attribute.value().toString();
            continue;
        }
        if (name == QString("OtherMediumTypeRefId")) {
            OtherMediumTypeRefId = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("DownwardCompatibleMasks")) {
            if (!DownwardCompatibleMasks) {
                DownwardCompatibleMasks = make_MaskVersion_DownwardCompatibleMasks_t(this);
            }
            DownwardCompatibleMasks->read(reader);
            continue;
        }
        if (reader.name() == QString("MaskEntries")) {
            if (!MaskEntries) {
                MaskEntries = make_MaskVersion_MaskEntries_t(this);
            }
            MaskEntries->read(reader);
            continue;
        }
        if (reader.name() == QString("HawkConfigurationData")) {
            auto * newHawkConfigurationData = make_HawkConfigurationData_t(this);
            newHawkConfigurationData->read(reader);
            HawkConfigurationData.append(newHawkConfigurationData);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int MaskVersion_t::tableColumnCount() const
{
    return 8;
}

QVariant MaskVersion_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "MaskVersion";
        }
        if (qualifiedName == QString("Id")) {
            return Id;
        }
        if (qualifiedName == QString("Name")) {
            return Name;
        }
        if (qualifiedName == QString("MaskVersion")) {
            return MaskVersion;
        }
        if (qualifiedName == QString("MgmtDescriptor01")) {
            return MgmtDescriptor01;
        }
        if (qualifiedName == QString("ManagementModel")) {
            return ManagementModel;
        }
        if (qualifiedName == QString("MediumTypeRefId")) {
            return MediumTypeRefId;
        }
        if (qualifiedName == QString("OtherMediumTypeRefId")) {
            return OtherMediumTypeRefId;
        }
        break;
    }
    return QVariant();
}

QVariant MaskVersion_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Id";
            case 2:
                return "Name";
            case 3:
                return "MaskVersion";
            case 4:
                return "MgmtDescriptor01";
            case 5:
                return "ManagementModel";
            case 6:
                return "MediumTypeRefId";
            case 7:
                return "OtherMediumTypeRefId";
            }
        }
    }
    return QVariant();
}

QVariant MaskVersion_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return QString("%1: %2").arg(Id).arg(Name);
    case Qt::DecorationRole:
        return QIcon::fromTheme("MaskVersion");
    }
    return QVariant();
}

MasterData_MediumTypes_MediumType_t * MaskVersion_t::getMediumType() const
{
    if (MediumTypeRefId.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<MasterData_MediumTypes_MediumType_t *>(knx->ids[MediumTypeRefId]);
}

MasterData_MediumTypes_MediumType_t * MaskVersion_t::getOtherMediumType() const
{
    if (OtherMediumTypeRefId.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<MasterData_MediumTypes_MediumType_t *>(knx->ids[OtherMediumTypeRefId]);
}

MaskVersion_t * make_MaskVersion_t(Base * parent)
{
    return new MaskVersion_t(parent);
}

} // namespace knx
} // namespace v14
} // namespace Project
