/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v14/knx/IDREF.h>
#include <Project/xs/UnsignedByte.h>

namespace Project {
namespace v14 {
namespace knx {

class Security_Role_t : public Base
{
    Q_OBJECT

public:
    explicit Security_Role_t(Base * parent = nullptr);
    virtual ~Security_Role_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    IDREF RefId{};
    xs::UnsignedByte Address{};
};

Security_Role_t * make_Security_Role_t(Base * parent);

} // namespace knx
} // namespace v14
} // namespace Project
