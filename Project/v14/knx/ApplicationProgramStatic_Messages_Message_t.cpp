/* This file is generated. */

#include <Project/v14/knx/ApplicationProgramStatic_Messages_Message_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v14/knx/KNX_t.h>

namespace Project {
namespace v14 {
namespace knx {

ApplicationProgramStatic_Messages_Message_t::ApplicationProgramStatic_Messages_Message_t(Base * parent) :
    Base(parent)
{
}

ApplicationProgramStatic_Messages_Message_t::~ApplicationProgramStatic_Messages_Message_t()
{
}

void ApplicationProgramStatic_Messages_Message_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == "Id") {
            Id = attribute.value().toString();
            KNX_t * knx = findParent<KNX_t *>();
            Q_ASSERT(knx);
            knx->ids[Id] = this;
            continue;
        }
        if (name == QString("Name")) {
            Name = attribute.value().toString();
            continue;
        }
        if (name == QString("InternalDescription")) {
            InternalDescription = attribute.value().toString();
            continue;
        }
        if (name == QString("Text")) {
            Text = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ApplicationProgramStatic_Messages_Message_t::tableColumnCount() const
{
    return 5;
}

QVariant ApplicationProgramStatic_Messages_Message_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ApplicationProgramStatic_Messages_Message";
        }
        if (qualifiedName == QString("Id")) {
            return Id;
        }
        if (qualifiedName == QString("Name")) {
            return Name;
        }
        if (qualifiedName == QString("InternalDescription")) {
            return InternalDescription;
        }
        if (qualifiedName == QString("Text")) {
            return Text;
        }
        break;
    }
    return QVariant();
}

QVariant ApplicationProgramStatic_Messages_Message_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Id";
            case 2:
                return "Name";
            case 3:
                return "InternalDescription";
            case 4:
                return "Text";
            }
        }
    }
    return QVariant();
}

QVariant ApplicationProgramStatic_Messages_Message_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return !Name.isEmpty() ? Name : "Message";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ApplicationProgramStatic_Messages_Message");
    }
    return QVariant();
}

ApplicationProgramStatic_Messages_Message_t * make_ApplicationProgramStatic_Messages_Message_t(Base * parent)
{
    return new ApplicationProgramStatic_Messages_Message_t(parent);
}

} // namespace knx
} // namespace v14
} // namespace Project
