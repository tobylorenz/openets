/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v14/knx/LdCtrlBase_OnError_t.h>
#include <Project/v14/knx/LdCtrlCompareBase_Range_t.h>
#include <Project/v14/knx/LdCtrlCompareProp_Count_t.h>
#include <Project/v14/knx/LdCtrlCompareProp_StartElement_t.h>
#include <Project/v14/knx/LdCtrlProcType_t.h>
#include <Project/xs/Boolean.h>
#include <Project/xs/HexBinary.h>
#include <Project/xs/String.h>
#include <Project/xs/UnsignedByte.h>
#include <Project/xs/UnsignedShort.h>

namespace Project {
namespace v14 {
namespace knx {

class LdCtrlCompareProp_t : public Base
{
    Q_OBJECT

public:
    explicit LdCtrlCompareProp_t(Base * parent = nullptr);
    virtual ~LdCtrlCompareProp_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    LdCtrlProcType_t AppliesTo{"auto"};
    xs::String InternalDescription{};
    xs::Boolean AllowCachedValue{"false"};
    xs::HexBinary InlineData{};
    xs::HexBinary Mask{};
    LdCtrlCompareBase_Range_t Range{};
    xs::Boolean Invert{"false"};
    xs::UnsignedShort RetryInterval{"0"};
    xs::UnsignedShort TimeOut{"0"};
    xs::UnsignedByte ObjIdx{};
    xs::UnsignedShort ObjType{};
    xs::UnsignedByte Occurrence{"0"};
    xs::UnsignedByte PropId{};
    LdCtrlCompareProp_StartElement_t StartElement{"1"};
    LdCtrlCompareProp_Count_t Count{"1"};

    /* elements */
    QVector<LdCtrlBase_OnError_t *> OnError;
};

LdCtrlCompareProp_t * make_LdCtrlCompareProp_t(Base * parent);

} // namespace knx
} // namespace v14
} // namespace Project
