/* This file is generated. */

#include <Project/v14/knx/FunctionType_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v14/knx/KNX_t.h>

namespace Project {
namespace v14 {
namespace knx {

FunctionType_t::FunctionType_t(Base * parent) :
    Base(parent)
{
}

FunctionType_t::~FunctionType_t()
{
}

void FunctionType_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == "Id") {
            Id = attribute.value().toString();
            KNX_t * knx = findParent<KNX_t *>();
            Q_ASSERT(knx);
            knx->ids[Id] = this;
            continue;
        }
        if (name == QString("Number")) {
            Number = attribute.value().toString();
            continue;
        }
        if (name == QString("Text")) {
            Text = attribute.value().toString();
            continue;
        }
        if (name == QString("Description")) {
            Description = attribute.value().toString();
            continue;
        }
        if (name == QString("Status")) {
            Status = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("FunctionPoint")) {
            QString newRole = reader.attributes().value("Role").toString();
            Q_ASSERT(!newRole.isEmpty());
            FunctionType_FunctionPoint_t * newFunctionPoint;
            if (FunctionPoint.contains(newRole)) {
                newFunctionPoint = FunctionPoint[newRole];
            } else {
                newFunctionPoint = make_FunctionType_FunctionPoint_t(this);
                FunctionPoint[newRole] = newFunctionPoint;
            }
            newFunctionPoint->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int FunctionType_t::tableColumnCount() const
{
    return 6;
}

QVariant FunctionType_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "FunctionType";
        }
        if (qualifiedName == QString("Id")) {
            return Id;
        }
        if (qualifiedName == QString("Number")) {
            return Number;
        }
        if (qualifiedName == QString("Text")) {
            return Text;
        }
        if (qualifiedName == QString("Description")) {
            return Description;
        }
        if (qualifiedName == QString("Status")) {
            return Status;
        }
        break;
    }
    return QVariant();
}

QVariant FunctionType_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Id";
            case 2:
                return "Number";
            case 3:
                return "Text";
            case 4:
                return "Description";
            case 5:
                return "Status";
            }
        }
    }
    return QVariant();
}

QVariant FunctionType_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return QString("%1: %2").arg(Id).arg(Text);
    case Qt::DecorationRole:
        return QIcon::fromTheme("FunctionType");
    }
    return QVariant();
}

FunctionType_t * make_FunctionType_t(Base * parent)
{
    return new FunctionType_t(parent);
}

} // namespace knx
} // namespace v14
} // namespace Project
