/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v14/knx/String50_t.h>
#include <Project/xs/ID.h>
#include <Project/xs/UnsignedShort.h>

namespace Project {
namespace v14 {
namespace knx {

class MaskVersion_MaskEntries_MaskEntry_t : public Base
{
    Q_OBJECT

public:
    explicit MaskVersion_MaskEntries_MaskEntry_t(Base * parent = nullptr);
    virtual ~MaskVersion_MaskEntries_MaskEntry_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::ID Id{};
    xs::UnsignedShort Address{};
    String50_t Name{};
};

MaskVersion_MaskEntries_MaskEntry_t * make_MaskVersion_MaskEntries_MaskEntry_t(Base * parent);

} // namespace knx
} // namespace v14
} // namespace Project
