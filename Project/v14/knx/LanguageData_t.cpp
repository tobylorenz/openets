/* This file is generated. */

#include <Project/v14/knx/LanguageData_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v14/knx/KNX_t.h>

namespace Project {
namespace v14 {
namespace knx {

LanguageData_t::LanguageData_t(Base * parent) :
    Base(parent)
{
}

LanguageData_t::~LanguageData_t()
{
}

void LanguageData_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("Identifier")) {
            Identifier = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("TranslationUnit")) {
            QString newRefId = reader.attributes().value("RefId").toString();
            Q_ASSERT(!newRefId.isEmpty());
            LanguageData_TranslationUnit_t * newTranslationUnit;
            if (TranslationUnit.contains(newRefId)) {
                newTranslationUnit = TranslationUnit[newRefId];
            } else {
                newTranslationUnit = make_LanguageData_TranslationUnit_t(this);
                TranslationUnit[newRefId] = newTranslationUnit;
            }
            newTranslationUnit->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int LanguageData_t::tableColumnCount() const
{
    return 2;
}

QVariant LanguageData_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "LanguageData";
        }
        if (qualifiedName == QString("Identifier")) {
            return Identifier;
        }
        break;
    }
    return QVariant();
}

QVariant LanguageData_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Identifier";
            }
        }
    }
    return QVariant();
}

QVariant LanguageData_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return Identifier;
    case Qt::DecorationRole:
        return QIcon::fromTheme("LanguageData");
    }
    return QVariant();
}

LanguageData_t * make_LanguageData_t(Base * parent)
{
    return new LanguageData_t(parent);
}

} // namespace knx
} // namespace v14
} // namespace Project
