/* This file is generated. */

#include <Project/v14/knx/Hardware_Products_Product_Baggages_Baggage_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v14/knx/KNX_t.h>
#include <Project/v14/knx/ManufacturerData_Manufacturer_Baggages_Baggage_t.h>

namespace Project {
namespace v14 {
namespace knx {

Hardware_Products_Product_Baggages_Baggage_t::Hardware_Products_Product_Baggages_Baggage_t(Base * parent) :
    Base(parent)
{
}

Hardware_Products_Product_Baggages_Baggage_t::~Hardware_Products_Product_Baggages_Baggage_t()
{
}

void Hardware_Products_Product_Baggages_Baggage_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("RefId")) {
            RefId = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int Hardware_Products_Product_Baggages_Baggage_t::tableColumnCount() const
{
    return 2;
}

QVariant Hardware_Products_Product_Baggages_Baggage_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "Hardware_Products_Product_Baggages_Baggage";
        }
        if (qualifiedName == QString("RefId")) {
            return RefId;
        }
        break;
    }
    return QVariant();
}

QVariant Hardware_Products_Product_Baggages_Baggage_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "RefId";
            }
        }
    }
    return QVariant();
}

QVariant Hardware_Products_Product_Baggages_Baggage_t::treeData(int role) const
{
    auto * ref = getBaggage();
    if (ref) {
        return ref->treeData(role);
    }

    switch (role) {
    case Qt::DisplayRole:
        return "Baggage";
    case Qt::DecorationRole:
        return QIcon::fromTheme("Hardware_Products_Product_Baggages_Baggage");
    }
    return QVariant();
}

ManufacturerData_Manufacturer_Baggages_Baggage_t * Hardware_Products_Product_Baggages_Baggage_t::getBaggage() const
{
    if (RefId.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<ManufacturerData_Manufacturer_Baggages_Baggage_t *>(knx->ids[RefId]);
}

Hardware_Products_Product_Baggages_Baggage_t * make_Hardware_Products_Product_Baggages_Baggage_t(Base * parent)
{
    return new Hardware_Products_Product_Baggages_Baggage_t(parent);
}

} // namespace knx
} // namespace v14
} // namespace Project
