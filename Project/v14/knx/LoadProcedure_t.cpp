/* This file is generated. */

#include <Project/v14/knx/LoadProcedure_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v14/knx/KNX_t.h>
#include <Project/v14/knx/LdCtrlBaseChoose_t.h>

namespace Project {
namespace v14 {
namespace knx {

LoadProcedure_t::LoadProcedure_t(Base * parent) :
    Base(parent)
{
}

LoadProcedure_t::~LoadProcedure_t()
{
}

void LoadProcedure_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("LdCtrlBase")) {
            if (!LdCtrlBase) {
                LdCtrlBase = make_LdCtrlBase_t(this);
            }
            LdCtrlBase->read(reader);
            continue;
        }
        if (reader.name() == QString("choose")) {
            auto * Choose = make_LdCtrlBaseChoose_t(this);
            Choose->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int LoadProcedure_t::tableColumnCount() const
{
    return 1;
}

QVariant LoadProcedure_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "LoadProcedure";
        }
        break;
    }
    return QVariant();
}

QVariant LoadProcedure_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant LoadProcedure_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "LoadProcedure";
    case Qt::DecorationRole:
        return QIcon::fromTheme("LoadProcedure");
    }
    return QVariant();
}

LoadProcedure_t * make_LoadProcedure_t(Base * parent)
{
    return new LoadProcedure_t(parent);
}

} // namespace knx
} // namespace v14
} // namespace Project
