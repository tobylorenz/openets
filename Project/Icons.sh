#!/bin/bash

QRC_FILE=/home/users/tobias/bitbucket/OpenETS/Project/Icons.qrc
ICON_DIR=/home/users/tobias/bitbucket/OpenETS/Project/icons

# header
cat << EOF > $QRC_FILE
<RCC>
    <qresource prefix="/">
EOF

# body
pushd $ICON_DIR
find -L -name *.png | sort | grep -v unused | sed -e 's/^\.\//        <file>icons\//' -e 's/$/<\/file>/' >> $QRC_FILE
popd

# footer
cat << EOF >> $QRC_FILE
        <file>icons/OpenETS/index.theme</file>
    </qresource>
</RCC>
EOF
