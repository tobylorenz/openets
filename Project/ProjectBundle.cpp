#include "ProjectBundle.h"

#include <QDataStream>
#include <QDebug>
#include <QDirIterator>
#include <QFileInfo>
#include <QXmlStreamReader>

#include <quazip/quazipfile.h>

ProjectBundle::ProjectBundle(QObject * parent) :
    QObject(parent)
{
}

ProjectBundle::~ProjectBundle()
{
}

void ProjectBundle::setPath(const QString & path)
{
    currentPath = path;
    QFileInfo fileInfo(path);
    if (fileInfo.isFile()) {
        zip = new QuaZip();
        zip->setZipName(path);
    }
}

void ProjectBundle::close()
{
    delete zip;
    zip = nullptr;

    switch(etsProjectVersion) {
    case 10:
        delete etsProject10;
        etsProject10 = nullptr;
        break;
    case 11:
        delete etsProject11;
        etsProject11 = nullptr;
        break;
    case 12:
        delete etsProject12;
        etsProject12 = nullptr;
        break;
    case 13:
        delete etsProject13;
        etsProject13 = nullptr;
        break;
    case 14:
        delete etsProject14;
        etsProject14 = nullptr;
        break;
    case 20:
        delete etsProject20;
        etsProject20 = nullptr;
        break;
    }
    etsProjectVersion = 0;

    signatures.clear();
}

void ProjectBundle::load()
{
    // determine project version
    if (zip) {
        zip->open(QuaZip::Mode::mdUnzip);
        zip->setCurrentFile("knx_master.xml");

        QuaZipFile * file = new QuaZipFile(zip, this);
        Q_ASSERT(file);
        etsProjectVersion = getProjectVersion(file);
        delete file;
        file = nullptr;
    } else {
        QFile * file = new QFile(currentPath + "/knx_master.xml", this);
        Q_ASSERT(file);
        etsProjectVersion = getProjectVersion(file);
        delete file;
        file = nullptr;
    }
    if (etsProjectVersion == 0) {
        qDebug() << "ETS KNX project version cannot be determined.";
        close();
        return;
    }

    // create root element
    qDebug() << "ETS KNX project version" << etsProjectVersion;
    switch(etsProjectVersion) {
    case 10:
        etsProject10 = Project::v10::knx::make_KNX_t(nullptr);
        etsProject10->setParent(this);
        break;
    case 11:
        etsProject11 = Project::v11::knx::make_KNX_t(nullptr);
        etsProject11->setParent(this);
        break;
    case 12:
        etsProject12 = Project::v12::knx::make_KNX_t(nullptr);
        etsProject12->setParent(this);
        break;
    case 13:
        etsProject13 = Project::v13::knx::make_KNX_t(nullptr);
        etsProject13->setParent(this);
        break;
    case 14:
        etsProject14 = Project::v14::knx::make_KNX_t(nullptr);
        etsProject14->setParent(this);
        break;
    case 20:
        etsProject20 = Project::v20::knx::make_KNX_t(nullptr);
        etsProject20->setParent(this);
        break;
    default:
        qWarning() << "Version" << etsProjectVersion << "not supported.";
        close();
        break;
    }

    // load project files
    if (zip) {
        QStringList fileNameList = zip->getFileNameList();
        int count{0};
        const int total = fileNameList.count();
        for (auto & fileName: fileNameList) {
            Q_EMIT loadProgress(count++, total, fileName);
            zip->setCurrentFile(fileName);
            QuaZipFile * file = new QuaZipFile(zip, this);
            Q_ASSERT(file);
            loadFile(fileName, file);
            delete file;
            file = nullptr;
        }
        delete zip;
        zip = nullptr;
        Q_EMIT loadProgress(total, total, "");
    } else {
        QStringList fileNameList;
        QDirIterator it(currentPath, QDir::Files, QDirIterator::Subdirectories);
        while (it.hasNext()) {
            fileNameList << it.next();
        }
        int count{0};
        const int total = fileNameList.count();
        for (auto & fileName: fileNameList) {
            Q_EMIT loadProgress(count++, total, fileName);
            QFile * file = new QFile(fileName, this);
            Q_ASSERT(file);
            loadFile(fileName, file);
            delete file;
            file = nullptr;
        }
        Q_EMIT loadProgress(total, total, "");
    }

    qInfo() << "Processing finished.";
}

void ProjectBundle::import(QString & fileName)
{
    QFile * file = new QFile(fileName, this);
    Q_ASSERT(file);
    loadFile(fileName, file);
    delete file;
}

void ProjectBundle::save()
{
    // @todo Save Project
}

int ProjectBundle::getProjectVersion(QIODevice * file) const
{
    Q_ASSERT(file);
    if (file->open(QIODevice::ReadOnly)) {
        QXmlStreamReader reader(file);
        if (reader.readNextStartElement()) {
            if (reader.name() == QString("KNX")) {
                QStringView namespaceUri = reader.namespaceUri(); // http://knx.org/xml/project/20
                return namespaceUri.split('/').constLast().toInt();
            }
        }
        file->close();
    }
    return 0;
}

void ProjectBundle::loadFile(QString & fileName, QIODevice * file)
{
    Q_ASSERT(file);
    if (fileName.endsWith(".xml")) {
        qInfo() << "Processing" << fileName;
        if (file->open(QIODevice::ReadOnly)) {
            QXmlStreamReader reader(file);
            if (reader.readNextStartElement()) {
                if (reader.name() == QString("KNX")) {
                    QStringView namespaceUri = reader.namespaceUri(); // http://knx.org/xml/project/20
                    const int namespaceVersion = namespaceUri.split('/').constLast().toInt();
                    if (namespaceVersion != etsProjectVersion) {
                        qWarning() << "ETS KNX project version differs" << etsProjectVersion;
                    }
                    switch (etsProjectVersion) {
                    case 10:
                        etsProject10->read(reader);
                        break;
                    case 11:
                        etsProject11->read(reader);
                        break;
                    case 12:
                        etsProject12->read(reader);
                        break;
                    case 13:
                        etsProject13->read(reader);
                        break;
                    case 14:
                        etsProject14->read(reader);
                        break;
                    case 20:
                        etsProject20->read(reader);
                        break;
                    }
                }
            }
            file->close();
        }
    } else if (fileName.endsWith(".signature")) {
        qInfo() << "Processing" << fileName;
        if (file->open(QIODevice::ReadOnly)) {
            signatures[fileName] = file->readAll();
            file->close();
        }
        // @todo check signature. There is one for every manufacturer folder M-xxxx and one for the project folder P-xxxx.
    } else if (fileName.contains("/AddinData/")) {
        qInfo() << "Processing" << fileName;
        if (file->open(QIODevice::ReadOnly)) {
            addinDatas[fileName] = file->readAll();
            file->close();
        }
    } else if (fileName.contains("/Baggages/")) {
        qInfo() << "Processing" << fileName;
        if (file->open(QIODevice::ReadOnly)) {
            baggages[fileName] = file->readAll();
            file->close();
        }
    } else {
        qWarning() << "Unknown file" << fileName;
    }
}
