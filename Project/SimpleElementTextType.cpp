#include <Project/SimpleElementTextType.h>

#include <QDebug>
#include <QIcon>

namespace Project {

SimpleElementTextType::SimpleElementTextType(QString name, Base * parent) :
    Base(parent),
    name(name)
{
}

void SimpleElementTextType::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse text element */
    text = reader.readElementText();
}

int SimpleElementTextType::tableColumnCount() const
{
    return 2;
}

QVariant SimpleElementTextType::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return name;
        }
        if (qualifiedName == "text") {
            return text;
        }
        break;
    }
    return QVariant();
}

QVariant SimpleElementTextType::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "text";
            }
        }
    }
    return QVariant();
}

QVariant SimpleElementTextType::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return name;
    case Qt::DecorationRole:
        return QIcon::fromTheme(name);
    }
    return QVariant();
}

SimpleElementTextType * make_SimpleElementTextType(const QString & name, Base * parent)
{
    return new SimpleElementTextType(name, parent);
}

} // namespace Project
