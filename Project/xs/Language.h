#pragma once

#include <QString>

namespace Project {
namespace xs {

using Language = QString;

} // namespace xs
} // namespace Project
