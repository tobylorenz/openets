#pragma once

#include <QString>

namespace Project {
namespace xs {

using Base64Binary = QString;

} // namespace xs
} // namespace Project
