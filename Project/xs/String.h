#pragma once

#include <QString>

namespace Project {
namespace xs {

using String = QString;

} // namespace xs
} // namespace Project
