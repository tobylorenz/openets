#pragma once

#include <QString>

namespace Project {
namespace xs {

using UnsignedShort = QString;

} // namespace xs
} // namespace Project
