#pragma once

#include <QList>

namespace Project {
namespace xs {

template <typename T>
using List = QList<T>;

} // namespace xs
} // namespace Project
