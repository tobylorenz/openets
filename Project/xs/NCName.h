#pragma once

#include <QString>

namespace Project {
namespace xs {

using NCName = QString;

} // namespace xs
} // namespace Project
