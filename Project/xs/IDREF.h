#pragma once

#include <QString>

namespace Project {
namespace xs {

using IDREF = QString;

} // namespace xs
} // namespace Project
