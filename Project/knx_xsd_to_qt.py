#!/usr/bin/env python3
# pylint: disable=too-many-branches,too-many-statements,line-too-long,too-many-lines,too-many-locals,too-many-nested-blocks,too-many-instance-attributes

"""Generate cpp/h/xsd files as access classes for KNX project"""

import copy
import filecmp
import logging
import os
import xml.etree.ElementTree as ET


# h filenames for CMakeLists.txt
h_filenames = {
    'ContextMenus.h',
    'Helpers.h',
    'ParameterWidgets.h'}


# cpp filenames for CMakeLists.txt
cpp_filenames = {
    'ContextMenus.cpp',
    'Helpers.cpp',
    'ParameterWidgets.cpp'}


# name to Simple_Type
simple_types = {
    'xs:base64Binary': None,
    'xs:boolean': None,
    'xs:byte': None,
    'xs:date': None,
    'xs:dateTime': None,
    'xs:double': None,
    'xs:float': None,
    'xs:hexBinary': None,
    'xs:ID': None,
    'xs:IDREF': None,
    'xs:int': None,
    'xs:language': None,
    'xs:list': None,
    'xs:long': None,
    'xs:NCName': None,
    'xs:short': None,
    'xs:string': None,
    'xs:unsignedByte': None,
    'xs:unsignedInt': None,
    'xs:unsignedLong': None,
    'xs:unsignedShort': None}


# name to Element (needed for <xs:element ref="knx:..."/>)
elements = {}


# name to Complex_Type
complex_types = {}


# Qt::DisplayRole taken from referenced type
ref_display_roles = {}


def get_ref_display_roles(version):
    return {
        'knx:Assign_t': 'getTargetParameterRefRef',
        'knx:BinaryDataRef_t': 'getBinaryData',
        'knx:ComObjectInstanceRef_t': 'getComObjectRef',
        'knx:ComObjectParameterBlock_t': 'getParameterRef',
        'knx:ComObjectRefRef_t': 'getComObjectRef',
        'knx:DeviceInstanceRef_t': 'getDeviceInstance',
        'knx:Hardware_Products_Product_Baggages_Baggage_t': 'getBaggage',
        'knx:ManufacturerData_Manufacturer_t': 'getManufacturer',
        'knx:ParameterInstanceRef_t': 'getParameterRef',
        'knx:ParameterRefRef_t': 'getParameterRef',
        'knx:Rename_t': 'getComObjectParameterBlock',
    }


# Qt::DisplayRole
display_roles = {}


def get_display_roles(version):
    return {
        'knx:ApplicationProgram_t':
            '        return QString("[%1 %2] %3").arg(ApplicationNumber).arg(ApplicationVersion).arg(Name);\n',
        'knx:ApplicationProgramChannel_t':
            '        return QString("[%1] %2").arg(Number).arg(Name);\n',
        'knx:ApplicationProgramStatic_Code_AbsoluteSegment_t':
            '        return QString(UserMemory == "false" ? "[%1] %2" : "[%1] %2 (User memory)").arg(Address).arg(Name);\n',
        'knx:ApplicationProgramStatic_Code_RelativeSegment_t':
            '        return Offset == "0" ? QString("[Rel#%1+%2] %3 (Relative)").arg(LoadStateMachine).arg(Offset).arg(Name) : QString("[Rel#%1] %2 (Relative)").arg(LoadStateMachine).arg(Name);\n',
        'knx:ApplicationProgramStatic_Parameters_Union_Memory_t':
            '    {\n'
            '        QString address;\n'
            '        ApplicationProgramStatic_Code_AbsoluteSegment_t * codeSegment = getCodeSegment();\n'
            '        if (codeSegment) {\n'
            '            address = QString(codeSegment->UserMemory == "false" ? "[%1]" : "[U%1]").arg(codeSegment->Address);\n'
            '        }\n'
            '        return QString("%1+%2.%3").arg(address).arg(Offset).arg(BitOffset);\n'
            '    }\n',
        'knx:CatalogSection_t':
            '        return QString("[%1] %2").arg(Number).arg(Name);\n',
        'knx:ComObject_t':
            '        return QString("[%1] %2-%3").arg(Number).arg(!Name.isEmpty() ? Name : Text).arg(FunctionText);\n',
        'knx:ComObjectParameterBlock_t':
            '        return !Name.isEmpty() ? Name : Text;\n',
        'knx:ComObjectRef_t':
            '    {\n'
            '        QStringList differences;\n'
            '        if (!Name.isEmpty()) {\n'
            '            differences.append("Name");\n'
            '        }\n'
            '        if (!Text.isEmpty()) {\n'
            '            differences.append("Text");\n'
            '        }\n'
            '        if (!Tag.isEmpty()) {\n'
            '            differences.append("Tag");\n'
            '        }\n'
            '        if (!FunctionText.isEmpty()) {\n'
            '            differences.append("FunctionText");\n'
            '        }\n'
            '        if (!Priority.isEmpty()) {\n'
            '            differences.append("Priority");\n'
            '        }\n'
            '        if (!ObjectSize.isEmpty()) {\n'
            '            differences.append("ObjectSize");\n'
            '        }\n'
            '        if (!ReadFlag.isEmpty()) {\n'
            '            differences.append("ReadFlag");\n'
            '        }\n'
            '        if (!WriteFlag.isEmpty()) {\n'
            '            differences.append("WriteFlag");\n'
            '        }\n'
            '        if (!CommunicationFlag.isEmpty()) {\n'
            '            differences.append("CommunicationFlag");\n'
            '        }\n'
            '        if (!TransmitFlag.isEmpty()) {\n'
            '            differences.append("TransmitFlag");\n'
            '        }\n'
            '        if (!UpdateFlag.isEmpty()) {\n'
            '            differences.append("UpdateFlag");\n'
            '        }\n'
            '        if (!ReadOnInitFlag.isEmpty()) {\n'
            '            differences.append("ReadOnInitFlag");\n'
            '        }\n'
            '        if (!DatapointType.isEmpty()) {\n'
            '            differences.append("DatapointType");\n'
            '        }\n'
            f'#if PROJECT_VERSION >= 12\n'
            '        if (!TextParameterRefId.isEmpty()) {\n'
            '            differences.append("TextParameterRefId");\n'
            '        }\n'
            '        if (!InternalDescription.isEmpty()) {\n'
            '            differences.append("InternalDescription");\n'
            '        }\n'
            '#endif\n'
            f'#if PROJECT_VERSION >= 13\n'
            '        if (!Roles.isEmpty()) {\n'
            '            differences.append("Roles");\n'
            '        }\n'
            '        if (!SecurityRequired.isEmpty()) {\n'
            '            differences.append("SecurityRequired");\n'
            '        }\n'
            f'#endif\n'
            '        QString comObjectNumber = RefId.mid(Id.lastIndexOf("_O-") + 3);\n'
            '        QString uniqueNumber = Id.mid(Id.lastIndexOf("_R-") + 3);\n'
            '        return QString("[%1 %2] %3-%4 (%5)").arg(comObjectNumber).arg(uniqueNumber).arg(!Name.isEmpty() ? Name : Text).arg(FunctionText).arg(differences.join(", "));\n'
            '    }\n',
        'knx:DatapointRole_t':
            '        return QString("%1: %2").arg(Id).arg(Name);\n',
        'knx:DatapointType_t':
            '        return QString("%1: %2").arg(Id).arg(Text);\n',
        'knx:DatapointType_DatapointSubtypes_DatapointSubtype_t':
            '        return QString("%1: %2").arg(Id).arg(Text);\n',
        'knx:DatapointType_DatapointSubtypes_DatapointSubtype_Format_Enumeration_EnumValue_t':
            '        return QString("[%1] %2").arg(Value).arg(Text);\n',
        'knx:DeviceInstance_t':
            '    {\n'
            '        QString text;\n'
            '        Topology_Area_Line_t * line = findParent<Topology_Area_Line_t *>();\n'
            '        if (line) {\n'
            '            /* assigned device instance */\n'
            '            Topology_Area_t * area = findParent<Topology_Area_t *>();\n'
            '            Q_ASSERT(area);\n'
            '            text = QString("%1.%2.%3").arg(area->Address).arg(line->Address).arg(Address);\n'
            '        } else {\n'
            '            /* unassigned device instance */\n'
            '            text = "DeviceInstance";\n'
            '        }\n'
            '        if (!Description.isEmpty()) {\n'
            '            text += " " + Description.simplified();\n'
            '        }\n'
            '        if (!ProductRefId.isEmpty()) {\n'
            '            Hardware_Products_Product_t * product = getProduct();\n'
            '            if (product) {\n'
            '                text += " " + product->Text;\n'
            '            }\n'
            '        }\n'
            '        return text;\n'
            '    }\n',
        'knx:Fixup_t':
            '    {\n'
            '        QString functionName;\n'
            f'#if PROJECT_VERSION >= 12\n'
            '        Function_t * function = getFunction();\n'
            '        if (function) {\n'
            '            functionName = function->treeData(role).toString();\n'
            '        }\n'
            '#endif\n'
            '        QString codeSegmentName;\n'
            '        ApplicationProgramStatic_Code_AbsoluteSegment_t * codeSegment = getCodeSegment();\n'
            '        if (codeSegment) {\n'
            '            codeSegmentName = codeSegment->treeData(role).toString();\n'
            '        }\n'
            '        return QString("%1 %2").arg(functionName).arg(codeSegmentName);\n'
            '    }\n',
        'knx:FunctionType_t':
            '        return QString("%1: %2").arg(Id).arg(Text);\n',
        'knx:GroupAddress_t':
            '        return QString("%1 %2").arg(getStyledAddress()).arg(Name);\n',
        'knx:GroupRange_t':
            '    {\n'
            '        // @todo TwoLevel: Main Group (M) 5 Bit / Subgroup (G) 11 Bit\n'
            '        // @todo ThreeLevel: Main Group (M) 5 Bit / Middle Group (Mi) 3 Bit / Subgroup (G) 8 Bit\n'
            '        int rangeStart = RangeStart.toInt();\n'
            '        int rangeStart1 = (rangeStart >> 11) & 0x1f;\n'
            '        int rangeStart2 = (rangeStart >>  8) & 0x07;\n'
            '        //int rangeStart3 = (rangeStart >>  0) & 0xff;\n'
            '        if (!GroupAddress.isEmpty() || GroupRange.isEmpty()) {\n'
            '            return QString("%1/%2 %3").arg(rangeStart1).arg(rangeStart2).arg(Name);\n'
            '        } else {\n'
            '            return QString("%1 %2").arg(rangeStart1).arg(Name);\n'
            '        }\n'
            '    }\n',
        'knx:Hardware_t':
            '        return QString("[%1 %2] %3").arg(SerialNumber).arg(VersionNumber).arg(Name);\n',
        'knx:Hardware2Program_t':
            '    {\n'
            '        int applicationProgramCount = Id.count("-O");\n'
            '        switch (applicationProgramCount) {\n'
            '        case 0:\n'
            '            return "-";\n'
            '        case 1:\n'
            '            return "ApplicationProgram"; // @todo is the name of the program(s) meant here?\n'
            '        case 2:\n'
            '            return "ApplicationProgram / ApplicationProgram2";\n'
            '        }\n'
            '    }\n',
        'knx:Hardware_Products_Product_t':
            '        return QString("[%1] %2").arg(OrderNumber).arg(Text);\n',
        'knx:HawkConfigurationData_MemorySegments_MemorySegment_t':
            '        return MemoryType;\n',
        'knx:LanguageData_t':
            '        return Identifier;\n',
        'knx:ManufacturerData_Manufacturer_Baggages_Baggage_t':
            '        return QString("%1\\%2").arg(TargetPath).arg(Name);\n',
        'knx:MaskVersion_t':
            '        return QString("%1: %2").arg(Id).arg(Name);\n',
        'knx:MasterData_DatapointTypes_DatapointType_t':
            '        return QString("%1: %2").arg(Id).arg(Text);\n',
        'knx:MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_t':
            '        return QString("%1: %2").arg(Id).arg(Text);\n',
        'knx:MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_Enumeration_EnumValue_t':
            '        return QString("[%1] %2").arg(Value).arg(Text);\n',
        'knx:MasterData_InterfaceObjectProperties_InterfaceObjectProperty_t':
            '        return QString("%1: %2").arg(Id).arg(Name);\n',
        'knx:MasterData_InterfaceObjectTypes_InterfaceObjectType_t':
            '        return QString("%1: %2").arg(Id).arg(Name);\n',
        'knx:MasterData_Manufacturers_Manufacturer_t':
            '        return QString("%1: %2").arg(Id).arg(Name);\n',
        'knx:MasterData_MediumTypes_MediumType_t':
            '        return QString("%1: %2").arg(Id).arg(Text);\n',
        'knx:MasterData_ProductLanguages_Language_t':
            '        return Identifier;\n',
        'knx:MasterData_PropertyDataTypes_PropertyDataType_t':
            '        return QString("%1: %2").arg(Id).arg(Name);\n',
        'knx:Parameter_t':
            '    {\n'
            '        QString uniqueNumber = Id.mid(Id.lastIndexOf("_P-") + 3);\n'
            '        return QString("[%1] %2").arg(uniqueNumber).arg(Name);\n'
            '    }\n',
        'knx:Parameter_Memory_t':
            '    {\n'
            '        QString address;\n'
            '        ApplicationProgramStatic_Code_AbsoluteSegment_t * codeSegment = getCodeSegment();\n'
            '        if (codeSegment) {\n'
            '            address = QString(codeSegment->UserMemory == "false" ? "[%1]" : "[U%1]").arg(codeSegment->Address);\n'
            '        }\n'
            '        return QString("%1+%2.%3").arg(address).arg(Offset).arg(BitOffset);\n'
            '    }\n',
        'knx:ParameterRef_t':
            '    {\n'
            '        QString uniqueNumber = Id.mid(Id.lastIndexOf("_R-") + 3);\n'
            '        QStringList differences;\n'
            '        if (!Name.isEmpty()) {\n'
            '            differences.append("Name");\n'
            '        }\n'
            '        if (!Text.isEmpty()) {\n'
            '            differences.append("Text");\n'
            '        }\n'
            '#if PROJECT_VERSION >= 11\n'
            '        if (!SuffixText.isEmpty()) {\n'
            '            differences.append("SuffixText");\n'
            '        }\n'
            '#endif\n'
            '        if (!Access.isEmpty()) {\n'
            '            differences.append("Access");\n'
            '        }\n'
            '        if (!Value.isEmpty()) {\n'
            '            differences.append("Value");\n'
            '        }\n'
            '#if PROJECT_VERSION >= 12\n'
            '        if (!InternalDescription.isEmpty()) {\n'
            '            differences.append("InternalDescription");\n'
            '        }\n'
            '#endif\n'
            '        return QString("[%1] %2 (%3)").arg(uniqueNumber).arg(Name).arg(differences.join(", "));\n'
            '    }\n',
        'knx:ParameterSeparator_t':
            '        return Text;\n',
        'knx:ParameterType_TypeRestriction_Enumeration_t':
            '        return QString("[%1] %2").arg(Value).arg(Text);\n',
        'knx:SpaceUsage_t':
            '        return QString("%1: %2").arg(Id).arg(Text);\n',
        'knx:Topology_Area_t':
            '        return QString("%1 %2").arg(Address).arg(Name);\n',
        'knx:UnionParameter_t':
            '        return QString("[%1] %2").arg(Id.mid(Id.lastIndexOf("_UP-") + 4)).arg(Name);\n',
        'knx:Topology_Area_Line_t':
            '    {\n'
            '        Topology_Area_t * area = findParent<Topology_Area_t *>();\n'
            '        Q_ASSERT(area);\n'
            '        return QString("%1.%2 %3").arg(area->Address).arg(Address).arg(Name);\n'
            '    }\n',
        'knx:UserFile_t':
            '        return Filename;\n',
        'knx:When_t':
            '        return Default == "true" ? "default" : Test;\n'
    }


# Qt::DecorationRole
decoration_roles = {}


def get_decoration_roles(version):
    return {
        'knx:ApplicationProgram_t':
            '        return QIcon::fromTheme(PeiType == "1" ? "ApplicationProgram_Pei" : "ApplicationProgram");\n',  # TODO "ApplicationProgram_Registered" for registered application programs
        'knx:BuildingPart_t':
            '        return QIcon::fromTheme(QString("BuildingPart_%1").arg(Type));\n',
        'knx:ChannelChoose_t':
            '        return QIcon::fromTheme((When.count() == 1) && (When[0]->Default == "true") ? "ChannelChoose_Conditionless" : "ChannelChoose");\n',
        'knx:ComObjectParameterChoose_t':
            '        return QIcon::fromTheme((When.count() == 1) && (When[0]->Default == "true") ? "ComObjectParameterChoose_Conditionless" : "ComObjectParameterChoose");\n',
    # ComObjectParameterBlock_t -> "ComObjectParameterBlock" : "ComObjectParameterBlock_Inline"
        'knx:DependentChannelChoose_t':
            '        return QIcon::fromTheme((When.count() == 1) && (When[0]->Default == "true") ? "DependentChannelChoose_Conditionless" : "DependentChannelChoose");\n',
        'knx:DeviceInstance_t':
            '    {\n'
            '        Topology_Area_Line_t * line = findParent<Topology_Area_Line_t *>();\n'
            '        if (line) {\n'
            '            MasterData_MediumTypes_MediumType_t * mediumType = line->getMediumType();\n'
            '            Q_ASSERT(mediumType);\n'
            '            return QIcon::fromTheme("DeviceInstance_" + mediumType->Name);\n'
            '        } else {\n'
            '            return QIcon::fromTheme("DeviceInstance");\n'
            '        }\n'
            '    }\n',
        'knx:GroupRange_t':
            '        return QIcon::fromTheme(QString((!GroupAddress.isEmpty() || GroupRange.isEmpty()) ? "GroupRange_Middle" : "GroupRange_Main"));\n',
        'knx:Space_t':
            '        if (!Usage.isEmpty()) {\n'
            '            return getUsage()->treeData(Qt::DecorationRole);\n'
            '        } else {\n'
            '            return QIcon::fromTheme(QString("Space_%1").arg(Type));\n'
            '        }\n',
        'knx:SpaceUsage_t':
            '        return QIcon::fromTheme(QString("SpaceUsage_%1").arg(Number));\n',
        'knx:Topology_Area_Line_t':
            '    {\n'
            '        MasterData_MediumTypes_MediumType_t * mediumType = getMediumType();\n'
            '        Q_ASSERT(mediumType);\n'
            '        return QIcon::fromTheme("Topology_Area_Line_" + mediumType->Name);\n'
            '    }\n',
    }


# id calculation methods
id_methods = {}


def get_id_methods(version):
    return {
        'knx:AddinData_t': None,
    #                # "M????-A????"
    #                # AddinId = QString("%1-A%2").arg(ManufacturerId).arg(AddinId)
        'knx:Allocator_t': None,
    #                # TODO
        'knx:ApplicationProgramChannel_t': None,
    #                # M-????_A-????-??-????_CH-*
    #                # Id = QString("%1_CH-%2").arg(ApplicationProgramId).arg(Number)
        'knx:ApplicationProgramStatic_BusInterfaces_BusInterface_t': None,
    #                # TODO
        'knx:ApplicationProgramStatic_Code_AbsoluteSegment_t': None,
    #                # M-????_A-????-??-????_AS-????
    #                # Id = QString("%1_AS-%2").arg(ApplicationProgramId).arg(Address)
        'knx:ApplicationProgramStatic_Code_RelativeSegment_t': None,
    #                # M-????_A-????-??-????_RS-??-?????
    #                # QString("%1_RS-%2-%3").arg(ApplicationProgramId).arg(LoadStateMachine?).arg(Offset?)
        'knx:ApplicationProgramStatic_Messages_Message_t': None,
    #                # TODO
        'knx:ApplicationProgramStatic_Parameters_Parameter_t': None,
    #                # TODO
        'knx:ApplicationProgramStatic_SecurityRoles_SecurityRole_t': None,
    #                # TODO
        'knx:ApplicationProgram_t': None,
    #                # M-????_A-????-??-????
    #                # M-????_A-????-??-????-O????
    #                # QString("%1_A-%2-%3-%4").arg(ManufacturerId).arg(ApplicationNumber, 4, 16, QChar('0')).arg(ApplicationVersion, 2, 16, QChar('0')).arg(?)
    #                # if (!originalManufacturer.isEmpty()) {
    #                #     retVal.append("-O");
    #                #     retVal.append(originalManufacturer);
    #                # }
        'knx:BinaryData_t': None,
    #                # M-????_A-????-??-????_BD-*
    #                # QString("%1_BD-%2").arg(ApplicationProgramId).arg(BinaryDataName);
        'knx:BuildingPart_t': None,
    #                # P-????-?_BP-*
    #                # QString("%1-%2_BP-%3").arg(ProjectId).arg(InstallationInstallationId).arg(UniqueNumber);
        'knx:Button_t': None,
    #                # TODO
        'knx:CatalogSection_CatalogItem_t': None,
    #                # M-????_H-??-?_HP-????-??-????_CI-*
    #                # QString("%1_CI-%2").arg(Hardware2ProgramId).arg(Name)
        'knx:CatalogSection_t': None,
    #                # M-????_CS-*
    #                # QString("%1_CS-%2").arg(ManufacturerId).arg(Number)
        'knx:ChannelInstance_t': None,
    #                # P-????-?_DI-*_CI-*
    #                # QString("%1_CI-%2").arg(DeviceInstanceId).arg(UniqueNumber)
        'knx:ComObjectInstanceRef_t': None,
    #                # No Id, only RefId
        'knx:ComObjectParameterBlock_t': None,
    #                # M-????_A-????-??-????_PB-*
    #                # QString("%1_PB-%2").arg(ApplicationProgramId).arg(UniqueNumber)
        'knx:ComObjectParameterBlock_Columns_Column_t': None,
    #                # TODO
        'knx:ComObjectParameterBlock_Rows_Row_t': None,
    #                # TODO
        'knx:ComObjectRef_t': None,
    #                # M-????_A-????-??-????_O-*_R-*
    #                # QString("%1_R-%2").arg(ComObjectId).arg(Tag);
        'knx:ComObject_t': None,
    #                # M-????_A-????-??-????_O-*
    #                # QString("%1_O-%2").arg(ApplicationProgramId).arg(Number);
        'knx:DatapointRole_t': None,
    #                # DR-*
    #                # QString("DR-%1").arg(Number);
        'knx:DatapointType_t': None,
    #                # DPT-*
    #                # QString("DPT-%1").arg(Number);
        'knx:DatapointType_DatapointSubtypes_DatapointSubtype_t': None,
    #                # DPST-*-*
    #                # QString("DPST-%1-%2").arg(DatapointTypeNumber).arg(DatapointSubtypeNumber);
        'knx:DatapointType_DatapointSubtypes_DatapointSubtype_Format_Bit_t': None,
    #                # DPST-*-*_F-*
    #                # QString("%1_F-%2").arg(DatapointSubtypeId).arg(UniqueNumber)
        'knx:DatapointType_DatapointSubtypes_DatapointSubtype_Format_Enumeration_EnumValue_t': None,
    #                # DPST-*-*_F-*-*
    #                # QString("%1-%2").arg(FormatId).arg(Value)
        'knx:DatapointType_DatapointSubtypes_DatapointSubtype_Format_Enumeration_t': None,
    #                # DPST-*-*_F-*
    #                # QString("%1_F-%2").arg(DatapointSubtypeId).arg(UniqueNumber)
        'knx:DatapointType_DatapointSubtypes_DatapointSubtype_Format_Float_t': None,
    #                # DPST-*-*_F-*
    #                # QString("%1_F-%2").arg(DatapointSubtypeId).arg(UniqueNumber)
        'knx:DatapointType_DatapointSubtypes_DatapointSubtype_Format_SignedInteger_t': None,
    #                # DPST-*-*_F-*
    #                # QString("%1_F-%2").arg(DatapointSubtypeId).arg(UniqueNumber)
        'knx:DatapointType_DatapointSubtypes_DatapointSubtype_Format_String_t': None,
    #                # DPST-*-*_F-*Name.isEmpty() ? Name : Text
    #                # QString("%1_F-%2").arg(DatapointSubtypeId).arg(UniqueNumber)
        'knx:DatapointType_DatapointSubtypes_DatapointSubtype_Format_UnsignedInteger_t': None,
    #                # DPST-*-*_F-*
    #                # QString("%1_F-%2").arg(DatapointSubtypeId).arg(UniqueNumber)
        'knx:DeviceInstance_t': None,
    #                # P-????-?_DI-*
    #                # QString("%1-%2_DI-%3").arg(ProjectId).arg(InstallationId).arg(UniqueNumber);
        'knx:Function_t': None,
    #                # P-????-?_F-*
    #                # QString("%1-%2_F-%3").arg(ProjectId).arg(InstallationId).arg(UniqueNumber);
        'knx:FunctionsGroup_t': None,
    #                # TODO
        'knx:FunctionType_t': None,
    #                # FT-*
    #                # QString("FT-%1").arg(Number);
        'knx:FunctionType_FunctionPoint_t': None,
    #                # FP-*_DR-*
    #                # QString("FP-%1_DR-%2").arg(UniqueNumber).arg(DatapointRoleId);
        'knx:GroupAddressRef_t': None,
    #                # P-????-?_GF-*
    #                # QString("%1-%2_GF-%3").arg(ProjectId).arg(InstallationId).arg(UniqueNumber);
        'knx:GroupAddress_t': None,
    #                # P-????-?_GA-*
    #                # QString("%1-%2_GA-%3").arg(ProjectId).arg(InstallationId).arg(UniqueNumber);
        'knx:GroupRange_t': None,
    #                # P-????-?_GR-*
    #                # QString("%1-%2_GR-%3").arg(ProjectId).arg(InstallationId).arg(UniqueNumber);
        'knx:Hardware2Program_t': None,
    #                # M-????_H-*_HP-????-??-????
    #                # QString("%1_HP-%2").arg(HardwareId).arg(UniqueNumber)
    # QString Hardware2Program::calculateId(const QString hardwareId,
    #                                       const uint16_t applicationNumber1, const uint8_t applicationVersion1, const uint16_t hashPart1, const QString originalManufacturer1,
    #                                       const uint16_t applicationNumber2, const uint8_t applicationVersion2, const uint16_t hashPart2, const QString originalManufacturer2) {
    #     QString retVal = QString("%1_HP").arg(hardwareId);
    #     if (applicationNumber1) {
    #         retVal.append(QString("-%1-%2-%3").arg(applicationNumber1, 4, 16, QChar('0')).arg(applicationVersion1, 2, 16, QChar('0')).arg(hashPart1, 4, 16, QChar('0')));
    #         if (!originalManufacturer1.isEmpty()) {
    #             retVal.append("-O");
    #             retVal.append(originalManufacturer1);
    #         }
    #         if (applicationNumber2) {
    #             retVal.append(QString("-%1-%2-%3").arg(applicationNumber2, 4, 16, QChar('0')).arg(applicationVersion2, 2, 16, QChar('0')).arg(hashPart2, 4, 16, QChar('0')));
    #             if (!originalManufacturer2.isEmpty()) {
    #                 retVal.append("-O");
    #                 retVal.append(originalManufacturer2);
    #             }
    #         }
    #     }
    #     return retVal;
    # }
        'knx:Hardware_Products_Product_Attributes_Attribute_t': None,
    #                # M-????_H-*_P-*_AT-*
    #                # QString("%1_AT-%2").arg(ProductId).arg(Name)
        'knx:Hardware_Products_Product_t': None,
    #                # M-????_H-*_P-*
    #                # QString("%1_P-%2").arg(HardwareId).arg(OrderNumber);
        'knx:Hardware_t': None,
    #                # M-????_H-??-?
    #                # QString("%1_H-%2-%3").arg(ManufacturerId).arg(SerialNumber).arg(VersionNumber);
    #                # if (!originalManufacturer.isEmpty()) {
    #                #     retVal.append("-O");
    #                #     retVal.append(OriginalManufacturer);
    #                # }
        'knx:ManufacturerData_Manufacturer_Baggages_Baggage_t': None,
    #                # M-????_BG-*
    #                # QString("%1_BG-%2").arg(ManufacturerId).arg(Name)
        'knx:MaskVersion_MaskEntries_MaskEntry_t': None,
    #                # MV-????_ME-*
    #                # QString("%1_ME-%2").arg(MaskVersionId).arg(Name)
        'knx:MaskVersion_t': None,
    #                # MV-????
    #                # QString("MV-%1").arg(MaskVersion)
    #                # if MgmtDescriptor01:
    #                #    append("-")
    #                #    append(MgmtDescriptor01)
        'knx:MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_Bit_t': None,
    #                # DPST-*-*_F-*
    #                # QString("%1_F-%2").arg(DatapointSubtypeId).arg(UniqueNumber)
        'knx:MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_Enumeration_EnumValue_t': None,
    #                # DPST-*-*_F-*-*
    #                # QString("%1-%2").arg(FormatId).arg(Value)
        'knx:MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_Enumeration_t': None,
    #                # DPST-*-*_F-*
    #                # QString("%1_F-%2").arg(DatapointSubtypeId).arg(UniqueNumber)
        'knx:MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_Float_t': None,
    #                # DPST-*-*_F-*
    #                # QString("%1_F-%2").arg(DatapointSubtypeId).arg(UniqueNumber)
        'knx:MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_SignedInteger_t': None,
    #                # DPST-*-*_F-*
    #                # QString("%1_F-%2").arg(DatapointSubtypeId).arg(UniqueNumber)
        'knx:MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_String_t': None,
    #                # DPST-*-*_F-*Name.isEmpty() ? Name : Text
    #                # QString("%1_F-%2").arg(DatapointSubtypeId).arg(UniqueNumber)
        'knx:MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_UnsignedInteger_t': None,
    #                # DPST-*-*_F-*
    #                # QString("%1_F-%2").arg(DatapointSubtypeId).arg(UniqueNumber)
        'knx:MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_t': None,
    #                # DPST-*-*
    #                # QString("DPST-%1-%2").arg(DatapointTypeNumber).arg(DatapointSubtypeNumber);
        'knx:MasterData_DatapointTypes_DatapointType_t': None,
    #                # DPT-*
    #                # QString("DPT-%1").arg(DatapointTypeNumber);
        'knx:MasterData_FunctionalBlocks_FunctionalBlock_t': None,
    #                # FB-*
    #                # QString("FB-%1").arg(Name)
        'knx:MasterData_InterfaceObjectProperties_InterfaceObjectProperty_t': None,
    #                # PID-*-*
    #                # QString("PID-%1-%2").arg(ObjectTypeId).arg(Number)
        'knx:MasterData_InterfaceObjectTypes_InterfaceObjectType_t': None,
    #                # OT-*
    #                # QString("OT-%1").arg(Number)
        'knx:MasterData_Manufacturers_Manufacturer_PublicKeys_PublicKey_t': None,
    #                # M-????_K-?
    #                # QString("%1_K-%2").arg(ManufacturerId).arg(Number)
        'knx:MasterData_Manufacturers_Manufacturer_t': None,
    #                # M-????
    #                # QString("M-%1").arg(ManufacturerId, 4, 16, QChar('0'));
        'knx:MasterData_MediumTypes_MediumType_t': None,
    #                # MT-*
    #                # QString("MT-%1").arg(MediumTypeNumber);
        'knx:MasterData_PropertyDataTypes_PropertyDataType_t': None,
    #                # PDT-*
    #                # QString("PDT-%1").arg(Number)
        'knx:MasterData_t': None,
    #                # MD-*
    #                # QString("MD-%1").arg(UniqueNumber)
        'knx:Module_t': None,
    #                # M-????_A-????-??-????_MD-*_M-*
    #                # QString("M-%1").arg(UniqueNumber)
        'knx:Module_TextArg_t': None,
    #                # M-????_A-????-??-????_MD-*_M-*_A-*
    #                # QString("A-%1").arg(UniqueNumber)
        'knx:ModuleDef_t': None,
    #                # TODO
        'knx:ModuleDef_Arguments_Argument_t': None,
    #                # TODO
        'knx:ModuleDefStatic_ComObjects_ComObject_t': None,
    #                # TODO
        'knx:ModuleDefStatic_Parameters_Parameter_t': None,
    #                # TODO
        'knx:P2PLinks_P2PLink_t': None,
    #                # TODO
        'knx:ParameterBase_t': None,
    #                # TODO
        'knx:ParameterCalculation_t': None,
    #                # M-????_A-????-??-????_PC-*
    #                # QString("%1_PC-%2").arg(ApplicationProgramId).arg(UniqueNumber);
        'knx:ParameterInstanceRef_t': None,
        'knx:ParameterRef_t': None,
    #                # M-????_A-????-??-????_UP-*_R-*"
    #                # M-????_A-????-??-????_P-*_R-*"
    #                # QString("%1_R-%2").arg(RefId).arg(Tag)
        'knx:ParameterSeparator_t': None,
    #                # M-????_A-????-??-????_PS-*
    #                # QString("%1_PS-%2").arg(ApplicationProgramId).arg(UniqueNumber)
        'knx:Parameter_t': None,
    #                # M-????_A-????-??-????_P-*
    #                # QString("%1_P-%2").arg(ApplicationProgramId).arg(UniqueNumber);
        'knx:ParameterType_t': None,
    #                # M-????_A-????-??-????_PT-*
    #                # QString("%1_P-%2").arg(ApplicationProgramId).arg(Name);
        'knx:ParameterType_TypeRestriction_Enumeration_t': None,
    #                # M-????_A-????-??-????_PT-*_EN-*
    #                # QString("%1_EN-%22).arg(ParameterTypeId).arg(Value)
        'knx:ParameterValidation_t': None,
    #                # TODO
        'knx:Project_t': None,
    #                # P-????
    #                # QString("P-%1").arg(UniqueNumber, 4, 16, QChar('0'));
        'knx:Rename_t': None,
    #                # M-????_A-????-??-????_PR-*
    #                # QString("%1_PR-%2").arg(ApplicationProgramId).arg(UniqueNumber)
        'knx:Repeat_t': None,
    #                # TODO
        'knx:SegmentBase_t': None,
    #                # TODO
        'knx:Space_t': None,
    #                # TODO
        'knx:SpaceUsage_t': None,
    #                # SU-*
    #                # QString("SU-%1").arg(Number);
        'knx:Topology_Area_Line_t': None,
    #                # P-????-?_L-*
    #                # QString("%1-%2_L-%3").arg(ProjectId).arg(InstallationId).arg(UniqueNumber);
        'knx:Topology_Area_t': None,
    #                # P-????-?_A-*
    #                # QString("%1-%2_A-%3").arg(ProjectId).arg(InstallationId).arg(UniqueNumber);
        'knx:Trade_t': None,
    #                # P-????-?_T-*
    #                # QString("%1-%2_T-%3").arg(ProjectId).arg(InstallationId).arg(UniqueNumber);
        'knx:UnionParameter_t': None,
    #                # M-????_A-????-??-????_UP-*
    #                # QString("%1_UP-%2").arg(ApplicationProgramId).arg(UniqueNumber);
    #            else:
    #                logging.warning('No xs:ID method for complex type {0:s}'.format(complex_type.name))
    }


# getter methods for xs:IDREF, knx:IDREF, knx:IDREFS, knx:RELIDREF, knx:RELIDREFS
getter_methods = {}


def get_getter_methods(version):
    retVal = {
        'knx:Allocator_t': {
            'ErrorMessageRef': ['void', 'getErrorMessage']},
        'knx:ApplicationProgram_t': {
            'IconFile': ['void', 'getIconFile'],
            'MaskVersion': ['knx:MaskVersion_t', 'getMaskVersion'],
            'OriginalManufacturer': ['knx:MasterData_Manufacturers_Manufacturer_t', 'getOriginalManufacturer']},
        'knx:ApplicationProgramChannel_t': {
            'TextParameterRefId': ['knx:ParameterRef_t', 'getTextParameterRef']},
        'knx:ApplicationProgramRef_t': {
            'RefId': ['knx:ApplicationProgram_t', 'getApplicationProgram']},
        'knx:ApplicationProgramStatic_AddressTable_t': {
            'CodeSegment': ['knx:ApplicationProgramStatic_Code_AbsoluteSegment_t', 'getCodeSegment']},
        'knx:ApplicationProgramStatic_AssociationTable_t': {
            'CodeSegment': ['knx:ApplicationProgramStatic_Code_AbsoluteSegment_t', 'getCodeSegment']},
        'knx:ApplicationProgramStatic_ComObjectTable_t': {
            'CodeSegment': ['knx:ApplicationProgramStatic_Code_AbsoluteSegment_t', 'getCodeSegment']},
        'knx:ApplicationProgramStatic_DeviceCompare_ExcludeMemory_t': {
            'CodeSegment': ['knx:ApplicationProgramStatic_Code_AbsoluteSegment_t', 'getCodeSegment']},
        'knx:ApplicationProgramStatic_Extension_Baggage_t': {
            'RefId': ['knx:ManufacturerData_Manufacturer_Baggages_Baggage_t', 'getBaggage']},
        'knx:ApplicationProgramStatic_Options_t': {
            'NotLoadableMessageRef': ['void', 'getNotLoadableMessage']},
        'knx:ApplicationProgramStatic_Parameters_Parameter_t': {
            'ParameterType': ['knx:ParameterType_t', 'getParameterType'],
            'ParameterTypeParams': ['void', '']},
        'knx:ApplicationProgramStatic_Parameters_Union_Memory_t': {
            'CodeSegment': ['knx:ApplicationProgramStatic_Code_AbsoluteSegment_t', 'getCodeSegment']},
        'knx:Assign_t': {
            'SourceParamRefRef': ['knx:ParameterRefRef_t', 'getSourceParameterRefRef'],
            'TargetParamRefRef': ['knx:ParameterRefRef_t', 'getTargetParameterRefRef']},
        'knx:BinaryDataRef_t': {
            'RefId': ['knx:BinaryData_t', 'getBinaryData']},
        'knx:BusInterface_t': {
            'RefId': ['void', '']},
        'knx:BusInterface_Connectors_Connector_t': {
            'GroupAddressRefId': ['knx:GroupAddress_t', 'getGroupAddress']},
        'knx:Button_t': {
            'TextParameterRefId': ['void', 'getTextParameter']},
        'knx:CalculationParameterRef_t': {
            'RefId': ['void', 'getCalculationParameter']},
        'knx:CatalogSection_CatalogItem_t': {
            'ProductRefId': ['knx:Hardware_Products_Product_t', 'getProduct'],
            'Hardware2ProgramRefId': ['knx:Hardware2Program_t', 'getHardware2Program']},
        'knx:ChannelChoose_t': {
            'ParamRefId': ['knx:ParameterRef_t', 'getParameterRef']},
        'knx:ChannelInstance_t': {
            'RefId': ['knx:ApplicationProgramChannel_t', 'getChannel']},
        'knx:ComObjectInstanceRef_Connectors_Receive_t': {
            'GroupAddressRefId': ['knx:GroupAddress_t', 'getGroupAddress']},
        'knx:ComObjectInstanceRef_Connectors_Send_t': {
            'GroupAddressRefId': ['knx:GroupAddress_t', 'getGroupAddress']},
        'knx:ComObjectParameterBlock_t': {
            'ParamRefId': ['knx:ParameterRef_t', 'getParameterRef'],
            'TextParameterRefId': ['knx:ParameterRef_t', 'getTextParameterRef']},
        'knx:ComObjectParameterBlock_Columns_Column_t': {
            'TextParameterRefId': ['knx:ParameterRef_t', 'getTextParameterRef']},
        'knx:ComObjectParameterBlock_Rows_Row_t': {
            'TextParameterRefId': ['knx:ParameterRef_t', 'getTextParameterRef']},
        'knx:ComObjectParameterChoose_t': {
            'ParamRefId': ['knx:ParameterRef_t', 'getParameterRef']},
        'knx:ComObjectRefRef_t': {
            'RefId': ['knx:ComObjectRef_t', 'getComObjectRef']},
        'knx:DatapointType_t': {
            'PDT': ['knx:MasterData_PropertyDataTypes_PropertyDataType_t', 'getPropertyDataType']},
        'knx:DatapointType_DatapointSubtypes_DatapointSubtype_t': {
            'PDT': ['knx:MasterData_PropertyDataTypes_PropertyDataType_t', 'getPropertyDataType']},
        'knx:DatapointType_DatapointSubtypes_DatapointSubtype_Format_RefType_t': {
            'RefId': ['void', 'getType']},  # TODO usually points to a Format Bit
        'knx:DependentChannelChoose_t': {
            'ParamRefId': ['knx:ParameterRef_t', 'getParameterRef']},
        'knx:DeviceInstance_t': {
            'Hardware2ProgramRefId': ['knx:Hardware2Program_t', 'getHardware2Program'],
            'ProductRefId': ['knx:Hardware_Products_Product_t', 'getProduct']},
        'knx:DeviceInstance_BinaryData_BinaryData_t': {
            'RefId': ['void', 'getBinaryData']},
        'knx:DeviceInstance_GroupObjectTree_t': {
            'GroupObjectInstances': ['knx:ComObjectRef_t', 'getGroupObjectInstances']},
        'knx:DeviceInstance_RfFastAckSlots_Slot_t': {
            'GroupAddressRefId': ['void', 'getGroupAddress']},
        'knx:DeviceInstanceRef_t': {
            'RefId': ['knx:DeviceInstance_t', 'getDeviceInstance']},
        'knx:Function_t': {
            'DefaultGroupRange': ['knx:GroupRange_t', 'getDefaultGroupRange'],
            'Implements': ['void', '']},
        'knx:GroupAddressRef_t': {
            'RefId': ['knx:GroupAddress_t', 'getGroupAddress']},
        'knx:Hardware_t': {
            'OriginalManufacturer': ['knx:MasterData_Manufacturers_Manufacturer_t', 'getOriginalManufacturer']},
        'knx:Hardware_Products_Product_Baggages_Baggage_t': {
            'RefId': ['knx:ManufacturerData_Manufacturer_Baggages_Baggage_t', 'getBaggage']},
        'knx:Hardware2Program_t': {
            'MediumTypes': ['knx:MasterData_MediumTypes_MediumType_t', 'getMediumTypes']},
        'knx:LanguageData_TranslationUnit_t': {
            'RefId': ['knx:CatalogSection_t', 'getCatalogSection']},
        'knx:LanguageData_TranslationUnit_TranslationElement_t': {
            'RefId': ['void', 'getTranslationElement']},
        'knx:LdCtrlBase_OnError_t': {
            'MessageRef': ['void', 'getMessage']},
        'knx:LdCtrlBaseChoose_t': {
            'ParamRefId': ['knx:ParameterRef_t', 'getParameterRef']},
        'knx:LdCtrlProgressText_t': {
            'MessageRef': ['void', 'getMessage']},
        'knx:ManufacturerData_Manufacturer_t': {
            'RefId': ['knx:MasterData_Manufacturers_Manufacturer_t', 'getManufacturer']},
        'knx:MaskVersion_DownwardCompatibleMasks_DownwardCompatibleMask_t': {
            'RefId': ['knx:MaskVersion_t', 'getMaskVersion']},
        'knx:MaskVersion_t': {
            'MediumTypeRefId': ['knx:MasterData_MediumTypes_MediumType_t', 'getMediumType'],
            'OtherMediumTypeRefId': ['knx:MasterData_MediumTypes_MediumType_t', 'getOtherMediumType']},
        'knx:MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_RefType_t': {
            'RefId': ['void', 'getType']},  # TODO usually points to a Format Bit
        'knx:MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_t': {
            'PDT': ['knx:MasterData_PropertyDataTypes_PropertyDataType_t', 'getPropertyDataType']},
        'knx:MasterData_DatapointTypes_DatapointType_t': {
            'PDT': ['knx:MasterData_PropertyDataTypes_PropertyDataType_t', 'getPropertyDataType']},
        'knx:MasterData_FunctionalBlocks_FunctionalBlock_Parameters_Parameter_t': {
            'Property': ['knx:MasterData_InterfaceObjectProperties_InterfaceObjectProperty_t', 'getInterfaceObjectProperty']},
        'knx:MasterData_FunctionalBlocks_FunctionalBlock_Parameters_t': {
            'ObjectType': ['knx:MasterData_InterfaceObjectTypes_InterfaceObjectType_t', 'getInterfaceObjectType']},
        'knx:MemoryParameter_t': {
            'CodeSegment': ['knx:ApplicationProgramStatic_Code_AbsoluteSegment_t', 'getCodeSegment']},
        'knx:MemoryUnion_t': {
            'CodeSegment': ['knx:ApplicationProgramStatic_Code_AbsoluteSegment_t', 'getCodeSegment']},
        'knx:Module_t': {
            'RefId': ['void', '']},
        'knx:Module_NumericArg_t': {
            'AllocatorRefId': ['void', 'getAllocator'],
            'BaseValue': ['void', 'getBaseValue'],
            'RefId': ['void', '']},
        'knx:Module_TextArg_t': {
            'RefId': ['void', '']},
        'knx:ModuleArg_t': {
            'RefId': ['void', 'getRefId']},
        'knx:ModuleDefLdCtrlBaseChoose_t': {
            'ParamRefId': ['void', 'getParameter']},
        'knx:ModuleDefLdCtrlCompareProp_t': {
            'BaseObjIdx': ['void', 'getBaseObjIdx'],
            'BaseOccurrence': ['void', 'getBaseOccurrence'],
            'BaseStartElement': ['void', 'getBaseStartElement']},
        'knx:ModuleDefLdCtrlInvokeFunctionProp_t': {
            'BaseObjIdx': ['void', 'getBaseObjIdx'],
            'BaseOccurrence': ['void', 'getBaseOccurrence']},
        'knx:ModuleDefLdCtrlReadFunctionProp_t': {
            'BaseObjIdx': ['void', 'getBaseObjIdx'],
            'BaseOccurrence': ['void', 'getBaseOccurrence']},
        'knx:ModuleDefLdCtrlWriteProp_t': {
            'BaseObjIdx': ['void', 'getBaseObjIdx'],
            'BaseOccurrence': ['void', 'getBaseOccurrence'],
            'BaseStartElement': ['void', 'getBaseStartElement']},
        'knx:ModuleDefStatic_ComObjects_ComObject_t': {
            'BaseNumber': ['knx:ModuleDef_Arguments_Argument_t', 'getBaseNumber'],
            'DatapointType': ['knx:DatapointType_DatapointSubtypes_DatapointSubtype_t', 'getDatapointType']},
        'knx:ModuleDefStatic_Parameters_Parameter_t': {
            'BaseValue': ['void', 'getBaseValue'],
            'ParameterType': ['knx:ParameterType_t', 'getParameterType'],
            'ParameterTypeParams': ['void', '']},
        'knx:ModuleDefStatic_Parameters_Parameter_Memory_t': {
            'BaseOffset': ['knx:ModuleDef_Arguments_Argument_t', 'getBaseOffset'],
            'CodeSegment': ['knx:ApplicationProgramStatic_Code_AbsoluteSegment_t', 'getCodeSegment']},
        'knx:ModuleDefStatic_Parameters_Parameter_Property_t': {
            'BaseIndex': ['void', 'getBaseIndex'],
            'BaseOccurrence': ['void', 'getBaseOccurrence'],
            'BaseOffset': ['knx:ModuleDef_Arguments_Argument_t', 'getBaseOffset']},
        'knx:ModuleDefStatic_Parameters_Union_Memory_t': {
            'BaseOffset': ['knx:ModuleDef_Arguments_Argument_t', 'getBaseOffset'],
            'CodeSegment': ['knx:ApplicationProgramStatic_Code_AbsoluteSegment_t', 'getCodeSegment']},
        'knx:ModuleDefStatic_Parameters_Union_Property_t': {
            'BaseIndex': ['void', 'getBaseIndex'],
            'BaseOccurrence': ['void', 'getBaseOccurrence'],
            'BaseOffset': ['knx:ModuleDef_Arguments_Argument_t', 'getBaseOffset']},
        'knx:ModuleInstance_t': {
            'RefId': ['void', '']},
        'knx:ModuleInstance_Arguments_Argument_t': {
            'RefId': ['void', '']},
        'knx:Node_t': {
            'GroupObjectInstances': ['knx:ComObjectRef_t', 'getGroupObjectInstances'],
            'RefId': ['void', '']},
        'knx:P2PLinkBusInterfaceEndpoint_t': {
            'BusInterfaceRefId': ['void', 'getBusInterface'],
            'DeviceRefId': ['void', 'getDevice']},
        'knx:P2PLinkDeviceEndpoint_t': {
            'DeviceRefId': ['void', 'getDevice'],
            'SecurityRoles': ['void', '']},
        'knx:P2PLinkEndpoint_t': {
            'DeviceRefId': ['void', 'getDevice']},
        'knx:Parameter_t': {
            'ParameterType': ['knx:ParameterType_t', 'getParameterType']},
        'knx:Parameter_Memory_t': {
            'CodeSegment': ['knx:ApplicationProgramStatic_Code_AbsoluteSegment_t', 'getCodeSegment']},
        'knx:ParameterBase_t': {
            'ParameterType': ['knx:ParameterType_t', 'getParameterType'],
            'ParameterTypeParams': ['void', '']},
        'knx:ParameterCalculation_LParameters_ParameterRefRef_t': {
            'RefId': ['knx:ParameterRef_t', 'getParameterRef']},
        'knx:ParameterCalculation_RParameters_ParameterRefRef_t': {
            'RefId': ['knx:ParameterRef_t', 'getParameterRef']},
        'knx:ParameterInstanceRef_t': {
            'RefId': ['knx:ParameterRef_t', 'getParameterRef']},
        'knx:ParameterRef_t': {
            'RefId': ['void', 'getParameter'],  # knx:ApplicationProgramStatic_Parameters_Parameter_t or knx:UnionParameter_t
            'TextParameterRefId': ['knx:ParameterRef_t', 'getTextParameterRef']},
        'knx:ParameterRefRef_t': {
            'RefId': ['knx:ParameterRef_t', 'getParameterRef']},
        'knx:ParameterSeparator_t': {
            'TextParameterRefId': ['knx:ParameterRef_t', 'getTextParameterRef']},
        'knx:ParameterType_t': {
            'ValidationErrorRef': ['void', 'getValidationError']},
        'knx:ParameterType_TypePicture_t': {
            'RefId': ['knx:ManufacturerData_Manufacturer_Baggages_Baggage_t', 'getBaggage']},
        'knx:Space_t': {
            'Usage': ['knx:SpaceUsage_t', 'getUsage']},
        'knx:Rename_t': {
            'RefId': ['knx:ComObjectParameterBlock_t', 'getComObjectParameterBlock']},
        'knx:Repeat_t': {
            'ParameterRefId': ['void', 'getParameter']},
        'knx:SpaceUsage_t': {
            'Relations': ['void', '']},
        'knx:Topology_Area_Line_t': {
            'MediumTypeRefId': ['knx:MasterData_MediumTypes_MediumType_t', 'getMediumType']},
        'knx:UnionParameter_t': {
            'ParameterType': ['knx:ParameterType_t', 'getParameterType'],
            'ParameterTypeParams': ['void', '']},
    }
    if version < 12:
        retVal['knx:Fixup_t'] = {
            'CodeSegment': ['knx:ApplicationProgramStatic_Code_AbsoluteSegment_t', 'getCodeSegment']}
    else:
        retVal['knx:Fixup_t'] = {
            'CodeSegment': ['knx:ApplicationProgramStatic_Code_AbsoluteSegment_t', 'getCodeSegment'],
            'FunctionRef': ['knx:Function_t', 'getFunction']}
    if version < 14:
        retVal['knx:ComObject_t'] = {
            'DatapointType': ['knx:MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_t', 'getDatapointType']}
        retVal['knx:ComObjectInstanceRef_t'] = {
            'Acknowledges': ['void', ''],
            'ChannelId': ['knx:ApplicationProgramChannel_t', 'getChannel'],
            'DatapointType': ['knx:MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_t', 'getDatapointType'],
            # 'Links': ['knx:GroupAddress_t', 'getLinks'], # TODO It contains GA-17, but not P-03FD-0_GA-17, which would be an Id.
            'RefId': ['knx:ComObjectRef_t', 'getComObjectRef']}
        retVal['knx:ComObjectRef_t'] = {
            'DatapointType': ['knx:MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_t', 'getDatapointType'],
            'RefId': ['knx:ComObject_t', 'getComObject'],
            'TextParameterRefId': ['knx:ParameterRef_t', 'getTextParameterRef']}
        retVal['knx:FunctionType_FunctionPoint_t'] = {
            'DatapointType': ['knx:MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_t', 'getDatapointType'],
            'Role': ['knx:DatapointRole_t', 'getRole']}
        retVal['knx:GroupAddress_t'] = {
            'DatapointType': ['knx:MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_t', 'getDatapointType']}
        retVal['knx:MasterData_InterfaceObjectProperties_InterfaceObjectProperty_t'] = {
            'DPT': ['knx:MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_t', 'getDatapointType'],
            'ObjectType': ['knx:MasterData_InterfaceObjectTypes_InterfaceObjectType_t', 'getInterfaceObjectType'],
            'PDT': ['knx:MasterData_PropertyDataTypes_PropertyDataType_t', 'getPropertyDataType']}
    else:
        retVal['knx:ComObject_t'] = {
            'DatapointType': ['knx:DatapointType_DatapointSubtypes_DatapointSubtype_t', 'getDatapointType']}
        retVal['knx:ComObjectInstanceRef_t'] = {
            'Acknowledges': ['void', ''],
            'ChannelId': ['knx:ApplicationProgramChannel_t', 'getChannel'],
            'DatapointType': ['knx:DatapointType_DatapointSubtypes_DatapointSubtype_t', 'getDatapointType'],
            # 'Links': ['knx:GroupAddress_t', 'getLinks'], # TODO It contains GA-17, but not P-03FD-0_GA-17, which would be an Id.
            'RefId': ['knx:ComObjectRef_t', 'getComObjectRef']}
        retVal['knx:ComObjectRef_t'] = {
            'DatapointType': ['knx:DatapointType_DatapointSubtypes_DatapointSubtype_t', 'getDatapointType'],
            'RefId': ['knx:ComObject_t', 'getComObject'],
            'TextParameterRefId': ['knx:ParameterRef_t', 'getTextParameterRef']}
        retVal['knx:FunctionType_FunctionPoint_t'] = {
            'DatapointType': ['knx:DatapointType_DatapointSubtypes_DatapointSubtype_t', 'getDatapointType'],
            'Role': ['knx:DatapointRole_t', 'getRole']}
        retVal['knx:GroupAddress_t'] = {
            'DatapointType': ['knx:DatapointType_DatapointSubtypes_DatapointSubtype_t', 'getDatapointType']}
        retVal['knx:MasterData_InterfaceObjectProperties_InterfaceObjectProperty_t'] = {
            'DPT': ['knx:DatapointType_DatapointSubtypes_DatapointSubtype_t', 'getDatapointType'],
            'ObjectType': ['knx:MasterData_InterfaceObjectTypes_InterfaceObjectType_t', 'getInterfaceObjectType'],
            'PDT': ['knx:MasterData_PropertyDataTypes_PropertyDataType_t', 'getPropertyDataType']}
    return retVal


# version will be determined during conversion
version = 0


def fix_ns(tag):
    """replace namespace URLs to namespace prefix"""
    global version
    tag = tag.replace(f'{{http://knx.org/xml/project/{version:d}}}', 'knx:')
    tag = tag.replace('{http://www.w3.org/2001/XMLSchema}', 'xs:')
    return tag


def fix_cpp_name(name):
    """fix names: change reserved names in cpp"""
    if name == 'Base':
        return 'Base_'
    if name == 'choose':
        return 'Choose'
    if name == 'default':
        return 'Default'
    if name == 'test':
        return 'Test'
    if name == 'when':
        return 'When'
    return name


def to_cpp(type_str):
    """convert xsd type to cpp"""
    type_str = type_str.replace('knx:', '')
    type_str = type_str.replace('xs:base64Binary', 'xs:Base64Binary')
    type_str = type_str.replace('xs:boolean', 'xs:Boolean')
    type_str = type_str.replace('xs:byte', 'xs:Byte')
    type_str = type_str.replace('xs:date', 'xs:Date')
    type_str = type_str.replace('xs:dateTime', 'xs:DateTime')
    type_str = type_str.replace('xs:double', 'xs:Double')
    type_str = type_str.replace('xs:float', 'xs:Float')
    type_str = type_str.replace('xs:hexBinary', 'xs:HexBinary')
    type_str = type_str.replace('xs:ID', 'xs:ID')
    type_str = type_str.replace('xs:int', 'xs:Int')
    type_str = type_str.replace('xs:language', 'xs:Language')
    type_str = type_str.replace('xs:list', 'xs:List')
    type_str = type_str.replace('xs:long', 'xs:Long')
    type_str = type_str.replace('xs:NCName', 'xs:NCName')
    type_str = type_str.replace('xs:short', 'xs:Short')
    type_str = type_str.replace('xs:string', 'xs:String')
    type_str = type_str.replace('xs:unsignedByte', 'xs:UnsignedByte')
    type_str = type_str.replace('xs:unsignedInt', 'xs:UnsignedInt')
    type_str = type_str.replace('xs:unsignedLong', 'xs:UnsignedLong')
    type_str = type_str.replace('xs:unsignedShort', 'xs:UnsignedShort')
    type_str = type_str.replace(':', '::')
    return type_str


def to_filename(type_str):
    """convert xsd type to filename"""
    type_str = type_str.replace('xs:base64Binary', 'xs:Base64Binary')
    type_str = type_str.replace('xs:boolean', 'xs:Boolean')
    type_str = type_str.replace('xs:byte', 'xs:Byte')
    type_str = type_str.replace('xs:date', 'xs:Date')
    type_str = type_str.replace('xs:dateTime', 'xs:DateTime')
    type_str = type_str.replace('xs:double', 'xs:Double')
    type_str = type_str.replace('xs:float', 'xs:Float')
    type_str = type_str.replace('xs:hexBinary', 'xs:HexBinary')
    type_str = type_str.replace('xs:ID', 'xs:ID')
    type_str = type_str.replace('xs:int', 'xs:Int')
    type_str = type_str.replace('xs:language', 'xs:Language')
    type_str = type_str.replace('xs:list', 'xs:List')
    type_str = type_str.replace('xs:long', 'xs:Long')
    type_str = type_str.replace('xs:NCName', 'xs:NCName')
    type_str = type_str.replace('xs:short', 'xs:Short')
    type_str = type_str.replace('xs:string', 'xs:String')
    type_str = type_str.replace('xs:unsignedByte', 'xs:UnsignedByte')
    type_str = type_str.replace('xs:unsignedInt', 'xs:UnsignedInt')
    type_str = type_str.replace('xs:unsignedLong', 'xs:UnsignedLong')
    type_str = type_str.replace('xs:unsignedShort', 'xs:UnsignedShort')
    type_str = type_str.replace(':', '/')
    return type_str


def lchop(long_str, prefix):
    """chop prefix from string"""
    if prefix and long_str.startswith(prefix):
        return long_str[len(prefix):]
    return long_str


def rchop(long_str, suffix):
    """chop suffix from string"""
    if suffix and long_str.endswith(suffix):
        return long_str[:-len(suffix)]
    return long_str


def beautify_file(filename):
    """beautify file"""
#    os.system(f'/usr/bin/astyle --suffix=none {filename:s}')
    pass


# <xs:simpleType>
# <xs:simpleType name="">
class SimpleType:
    """xs:simpleType class"""
    def __init__(self, xml_element, name=None):
        assert fix_ns(xml_element.tag) == 'xs:simpleType'
        self.xml_element = xml_element
        self.name = xml_element.attrib.get('name', name)
        assert self.name
        if ':' not in self.name:
            self.name = f'knx:{self.name:s}'
        self.includes = set()
        self.xsd_filename = f'{to_filename(self.name):s}.xsd'
        self.h_filename = f'{to_filename(self.name):s}.h'
        self.type = None
        self.is_list = False

    def parse(self):
        """parse"""
        global simple_types
        simple_types[self.name] = self
        for child1 in self.xml_element:
            tag1 = fix_ns(child1.tag)
            if tag1 == 'xs:list':
                self.type = child1.attrib['itemType']
                self.includes.add(self.type)
                self.type = f'xs:list<{self.type:s}>'
                self.includes.add('xs:list')
                self.is_list = True
            elif tag1 == 'xs:restriction':
                self.type = child1.attrib['base']
                self.includes.add(self.type)
                # TODO xs:minInclusive
                # TODO xs:maxInclusive
                # TODO xs:enumeration
            elif tag1 == 'xs:union':
                self.type = child1.attrib['memberTypes']
                self.includes.add(self.type)
    #                for child2 in child1:
    #                    tag2 = fix_ns(child2.tag)
    #                    if tag2 == 'xs:simpleType':
    #                        simple_type = SimpleType(child2)
    #                        simple_type.parse()
    #                        self.type = memberTypes
    #                        self.simple_includes.add(simple_type.type)
    #                    else:
    #                        logging.warning('tag %s not handled', tag2)
            else:
                logging.warning('tag %s not handled', tag1)

    def write_xsd(self):
        """write xsd"""
        try:
            logging.info('Writing SimpleType %s', self.xsd_filename)
            tree = ET.ElementTree(self.xml_element)
            tmp_filename = f'{self.xsd_filename:s}.tmp'
            tree.write(tmp_filename)
            if not os.path.exists(self.xsd_filename):
                os.rename(tmp_filename, self.xsd_filename)
            elif filecmp.cmp(tmp_filename, self.xsd_filename):
                os.remove(tmp_filename)
            else:
                os.replace(tmp_filename, self.xsd_filename)
        except:
            logging.error('write_xsd(%s)', self.xml_element)

    def write_h(self):
        """write h"""
        try:
            logging.info('Writing SimpleType %s', self.h_filename)
            global h_filenames
            h_filenames.add(self.h_filename)
            tmp_filename = f'{self.h_filename:s}.tmp'
            with open(tmp_filename, mode='w', encoding='utf-8') as h_file:
                global version
                h_file.write('/* This file is generated. */\n')
                h_file.write('\n')
                h_file.write('#pragma once\n')
                h_file.write('\n')
                for include in sorted(self.includes):
                    if include.startswith('xs:'):
                        h_file.write(f'#include <Project/{to_filename(include):s}.h>\n')
                    elif include in ('Base', 'SimpleElementTextType'):
                        h_file.write(f'#include <Project/{to_filename(include):s}.h>\n')
                    elif include.startswith('knx:'):
                        h_file.write(f'#include <Project/v{version:d}/{to_filename(include):s}.h>\n')
                    else:
                        logging.warning('#include <Project/v%d/%s.h>\n', version, to_filename(include))
                h_file.write('\n')
                h_file.write('namespace Project {\n')
                h_file.write(f'namespace v{version:d} {{\n')
                h_file.write('namespace knx {\n')
                h_file.write('\n')
                h_file.write(f'using {to_cpp(self.name):s} = {to_cpp(self.type):s};\n')
                h_file.write('\n')
                h_file.write('} // namespace knx\n')
                h_file.write(f'}} // namespace v{version:d}\n')
                h_file.write('} // namespace Project\n')
            if not os.path.exists(self.h_filename):
                os.rename(tmp_filename, self.h_filename)
            elif filecmp.cmp(tmp_filename, self.h_filename):
                os.remove(tmp_filename)
            else:
                os.replace(tmp_filename, self.h_filename)
        except:
            logging.error('write_h(%s)', self.xml_element)


# <xs:attribute name="" type="" use="" default="">
class Attribute:
    """xs:attribute class"""
    def __init__(self, xml_element, parent_type=None):
        assert fix_ns(xml_element.tag) == 'xs:attribute'
        self.xml_element = xml_element
        self.parent_type = parent_type
        self.name = xml_element.attrib.get('name', None)
        self.type = xml_element.attrib.get('type', f'{rchop(parent_type, '_t'):s}_{self.name:s}_t')
        if ':' not in self.type:
            self.type = f'knx:{self.type:s}'
        self.use = xml_element.attrib.get('use', None)  # required/optional
        self.default = xml_element.attrib.get('default', None)
        self.includes = set()

    def parse(self):
        """parse"""
        self.includes.add(self.type)
        for child1 in self.xml_element:
            tag1 = fix_ns(child1.tag)
            if tag1 == 'xs:annotation':
                pass
            elif tag1 == 'xs:simpleType':
                simple_type = SimpleType(child1, self.type)
                simple_type.parse()
            else:
                logging.warning('tag %s not handled', tag1)

    def get_h(self):
        """get h"""
        assert self.type
        assert self.name
        lines = []
        if self.default:
            lines.append(f'    {to_cpp(self.type):s} {fix_cpp_name(self.name):s}{{"{self.default:s}"}};\n')
        else:
            lines.append(f'    {to_cpp(self.type):s} {fix_cpp_name(self.name):s}{{}};\n')
        return ''.join(lines)

    def get_cpp(self):
        """get cpp"""
        lines = []
        lines.append(f'        if (name == QString("{self.name:s}")) {{\n')
        global simple_types
        if (self.type in simple_types) and simple_types[self.type] and simple_types[self.type].is_list:
            lines.append(f'            {fix_cpp_name(self.name):s} = attribute.value().toString().split(\' \');\n')
        else:
            lines.append(f'            {fix_cpp_name(self.name):s} = attribute.value().toString();\n')
        lines.append('            continue;\n')
        lines.append('        }\n')
        return ''.join(lines)


# <xs:element name="" type="" minOccurs="" maxOccurs="">
# <xs:element ref="">
class Element:
    """xs:element class"""
    def __init__(self, xml_element, parent_type=None, in_xs_choice=False):
        assert fix_ns(xml_element.tag) == 'xs:element'
        self.xml_element = xml_element
        self.parent_type = parent_type
        self.in_xs_choice = in_xs_choice  # is inside an xs:choice
        self.ref = xml_element.attrib.get('ref', None)
        if not self.ref:
            self.name = xml_element.attrib.get('name', None)
            self.type = xml_element.attrib.get('type', None)
            if not self.type:
                if parent_type:
                    self.type = f'{rchop(parent_type, '_t'):s}_{self.name:s}_t'
                else:
                    # Note: This is only for KNX root element
                    self.type = f'{self.name:s}_t'
            if ':' not in self.type:
                self.type = f'knx:{self.type:s}'
            self.min_occurs = xml_element.attrib.get('minOccurs', '1')
            self.max_occurs = xml_element.attrib.get('maxOccurs', '1')
        self.includes = set()  # to be used by parent complexType
        self.forward_declarations = set()  # to be used by parent complexType

    def parse(self):
        """parse"""
        if self.ref:
            self.includes.add(f'{self.ref:s}_t')
            return
        for child1 in self.xml_element:
            tag1 = fix_ns(child1.tag)
            if tag1 == 'xs:annotation':
                pass
            elif tag1 == 'xs:complexType':
                complex_type = ComplexType(child1, name=self.type)
                complex_type.parse()
                if complex_type.key_type:
                    self.includes.add(complex_type.key_type)
            elif tag1 == 'xs:simpleType':
                simple_type = SimpleType(child1, name=self.type)
                simple_type.parse()
            else:
                logging.warning('tag %s not handled', tag1)
        global elements
        elements[self.name] = self
        global simple_types
        if self.type in simple_types:
            self.includes.add('SimpleElementTextType')
            self.includes.add(self.type)
        else:
            if (self.max_occurs > '1') or (self.max_occurs == 'unbound'):
                self.includes.add(self.type)
            else:
                self.forward_declarations.add(self.type)

    def get_h(self):
        """get h"""
        assert self.type
        assert self.name
        lines = []
        type_str = None
        global simple_types
        if self.type in simple_types:
            type_str = 'SimpleElementTextType *'
        else:
            type_str = f'{to_cpp(self.type):s} *'
        if self.in_xs_choice:
            lines.append(f'    // xs:choice {type_str:s} {fix_cpp_name(self.name):s}{{}};\n')
        elif (self.max_occurs > '1') or (self.max_occurs == 'unbound'):
            global complex_types
            if (self.type in complex_types) and complex_types[self.type].key_name:
                lines.append(f'    QMap<{to_cpp(complex_types[self.type].key_type):s}, {type_str:s}> {fix_cpp_name(self.name):s}; // key: {complex_types[self.type].key_name:s}\n')
            else:
                lines.append(f'    QVector<{type_str:s}> {fix_cpp_name(self.name):s};\n')
        else:
            lines.append(f'    {type_str:s} {fix_cpp_name(self.name):s}{{}};\n')
        return ''.join(lines)

    def get_cpp(self):
        """get cpp"""
        assert self.type
        assert self.name
        lines = []
        lines.append(f'        if (reader.name() == QString("{self.name:s}")) {{\n')
        make = None
        global simple_types
        if self.type in simple_types:
            make = f'make_SimpleElementTextType("{fix_cpp_name(self.name):s}", this)'
        else:
            make = f'make_{to_cpp(self.type):s}(this)'
        if self.in_xs_choice:
            lines.append(f'            auto * {fix_cpp_name(self.name):s} = {make:s};\n')
            lines.append(f'            {fix_cpp_name(self.name):s}->read(reader);\n')
        elif (self.max_occurs > '1') or (self.max_occurs == 'unbound'):
            global complex_types
            if (self.type in complex_types) and complex_types[self.type].key_name:
                # QMap
                lines.append(f'            QString new{complex_types[self.type].key_name:s} = reader.attributes().value("{complex_types[self.type].key_name:s}").toString();\n')
                lines.append(f'            Q_ASSERT(!new{complex_types[self.type].key_name:s}.isEmpty());\n')
                lines.append(f'            {to_cpp(self.type):s} * new{fix_cpp_name(self.name):s};\n')
                lines.append(f'            if ({fix_cpp_name(self.name):s}.contains(new{complex_types[self.type].key_name:s})) {{\n')
                lines.append(f'                new{fix_cpp_name(self.name):s} = {fix_cpp_name(self.name):s}[new{complex_types[self.type].key_name:s}];\n')
                lines.append('            } else {\n')
                lines.append(f'                new{fix_cpp_name(self.name):s} = {make:s};\n')
                lines.append(f'                {fix_cpp_name(self.name):s}[new{complex_types[self.type].key_name:s}] = new{fix_cpp_name(self.name):s};\n')
                lines.append('            }\n')
                lines.append(f'            new{fix_cpp_name(self.name):s}->read(reader);\n')
            else:
                # QVector
                lines.append(f'            auto * new{fix_cpp_name(self.name):s} = {make:s};\n')
                lines.append(f'            new{fix_cpp_name(self.name):s}->read(reader);\n')
                lines.append(f'            {fix_cpp_name(self.name):s}.append(new{fix_cpp_name(self.name):s});\n')
        else:
            lines.append(f'            if (!{fix_cpp_name(self.name):s}) {{\n')
            lines.append(f'                {fix_cpp_name(self.name):s} = {make:s};\n')
            lines.append('            }\n')
            lines.append(f'            {fix_cpp_name(self.name):s}->read(reader);\n')
        lines.append('            continue;\n')
        lines.append('        }\n')
        return ''.join(lines)


# <xs:complexType name="">
class ComplexType:
    """xs:complexType class"""
    def __init__(self, xml_element, name=None):
        assert fix_ns(xml_element.tag) == 'xs:complexType'
        self.xml_element = xml_element
        self.base = 'Base'
        self.name = xml_element.attrib.get('name', name)
        if ':' not in self.name:
            self.name = f'knx:{self.name:s}'
        self.attributes = []  # of Attribute
        self.elements = []  # of Element
        self.simple_content_type = None
        self.includes = set()
        self.forward_declarations = set()
        self.xsd_filename = f'{to_filename(self.name):s}.xsd'
        self.h_filename = f'{to_filename(self.name):s}.h'
        self.cpp_filename = f'{to_filename(self.name):s}.cpp'
        self.key_name = None
        self.key_type = None

    def parse(self):
        """parse"""
        global complex_types
        complex_types[self.name] = self
        for child1 in self.xml_element:
            tag1 = fix_ns(child1.tag)
            if tag1 == 'xs:annotation':
                pass
            elif tag1 == 'xs:attribute':
                attribute = Attribute(child1, parent_type=self.name)
                attribute.parse()
                self.includes = self.includes.union(attribute.includes)
                self.attributes.append(attribute)
            elif tag1 == 'xs:choice':
                for child2 in child1:
                    tag2 = fix_ns(child2.tag)
                    if tag2 == 'xs:annotation':
                        pass
                    elif tag2 == 'xs:element':
                        element = Element(child2, parent_type=self.name, in_xs_choice=True)
                        element.parse()
                        self.includes = self.includes.union(element.includes)
                        self.forward_declarations = self.forward_declarations.union(element.forward_declarations)
                        self.elements.append(element)
                    else:
                        logging.warning('tag %s not handled', tag2)
            elif tag1 == 'xs:complexContent':
                for child2 in child1:
                    tag2 = fix_ns(child2.tag)
                    if tag2 == 'xs:attribute':
                        attribute = Attribute(child2, parent_type=self.name)
                        attribute.parse()
                        self.includes = self.includes.union(attribute.includes)
                        self.attributes.append(attribute)
                    elif tag2 == 'xs:extension':
                        base = child2.attrib['base']
                        self.attributes = copy.deepcopy(complex_types[base].attributes)
                        self.elements = copy.deepcopy(complex_types[base].elements)
                        self.includes = copy.deepcopy(complex_types[base].includes)
                        self.forward_declarations = copy.deepcopy(complex_types[base].forward_declarations)
                        for child3 in child2:
                            tag3 = fix_ns(child3.tag)
                            if tag3 == 'xs:attribute':
                                attribute = Attribute(child3, parent_type=self.name)
                                attribute.parse()
                                self.includes = self.includes.union(attribute.includes)
                                self.attributes.append(attribute)
                            elif tag3 == 'xs:choice':
                                for child4 in child3:
                                    tag4 = fix_ns(child4.tag)
                                    if tag4 == 'xs:annotation':
                                        pass
                                    elif tag4 == 'xs:element':
                                        element = Element(child4, parent_type=self.name, in_xs_choice=True)
                                        element.parse()
                                        self.includes = self.includes.union(element.includes)
                                        self.forward_declarations = self.forward_declarations.union(element.forward_declarations)
                                        self.elements.append(element)
                                    else:
                                        logging.warning('tag %s not handled', tag4)
                            elif tag3 == 'xs:sequence':
                                for child4 in child3:
                                    tag4 = fix_ns(child4.tag)
                                    if tag4 == 'xs:annotation':
                                        pass
                                    elif tag4 == 'xs:choice':
                                        for child5 in child4:
                                            tag5 = fix_ns(child5.tag)
                                            if tag5 == 'xs:element':
                                                element = Element(child5, parent_type=self.name, in_xs_choice=True)
                                                element.parse()
                                                self.includes = self.includes.union(element.includes)
                                                self.forward_declarations = self.forward_declarations.union(element.forward_declarations)
                                                self.elements.append(element)
                                            else:
                                                logging.warning('tag %s not handled', tag5)
                                    else:
                                        logging.warning('tag %s not handled', tag4)
                            else:
                                logging.warning('tag %s not handled', tag3)
                    else:
                        logging.warning('tag %s not handled', tag2)
            elif tag1 == 'xs:sequence':
                for child2 in child1:
                    tag2 = fix_ns(child2.tag)
                    if tag2 == 'xs:annotation':
                        pass
                    elif tag2 == 'xs:choice':
                        for child3 in child2:
                            tag3 = fix_ns(child3.tag)
                            if tag3 == 'xs:annotation':
                                pass
                            elif tag3 == 'xs:element':
                                element = Element(child3, parent_type=self.name, in_xs_choice=True)
                                element.parse()
                                self.includes = self.includes.union(element.includes)
                                self.forward_declarations = self.forward_declarations.union(element.forward_declarations)
                                self.elements.append(element)
                            else:
                                logging.warning('tag %s not handled', tag3)
                    elif tag2 == 'xs:element':
                        element = Element(child2, parent_type=self.name)
                        element.parse()
                        self.includes = self.includes.union(element.includes)
                        self.forward_declarations = self.forward_declarations.union(element.forward_declarations)
                        self.elements.append(element)
                    else:
                        logging.warning('tag %s not handled', tag2)
            elif tag1 == 'xs:simpleContent':
                for child2 in child1:
                    tag2 = fix_ns(child2.tag)
                    if tag2 == 'xs:extension':
                        self.simple_content_type = child2.attrib['base']
                        self.includes.add(self.simple_content_type)
                    else:
                        logging.warning('tag %s not handled', tag2)
            else:
                logging.warning('tag %s not handled', tag1)
        self.includes.add(self.base)

        # determine key name and key type
        for key_type in ['xs:ID', 'xs:IDREF', 'knx:IDREF', 'knx:RELIDREF', 'knx:RELIDREFS']:
            for attribute in self.attributes:
                if (attribute.type == key_type) and (attribute.use == 'required'):
                    self.key_name = attribute.name
                    self.key_type = key_type
                    break
            if self.key_name:
                break
        if not self.key_name:
            logging.info('complex type %s has no suitable ID', self.name)

        # determine getters
        for attribute in self.attributes:
            if (attribute.type in ['xs:IDREF', 'knx:IDREF', 'knx:IDREFS', 'knx:RELIDREF', 'knx:RELIDREFS']):
                global getter_methods
                if self.name in getter_methods:
                    if attribute.name in getter_methods[self.name]:
                        (target_type, method_name) = getter_methods[self.name][attribute.name]
                        if target_type == 'void':
                            logging.warning('Getter method for complex type %s and attribute %s has target_type=void', self.name, attribute.name)
                    else:
                        logging.warning('No getter method for complex type %s and attribute name %s and attribute type %s', self.name, attribute.name, attribute.type)
                else:
                    logging.warning('No getter methods for complex type %s', self.name)

    def write_xsd(self):
        """write xsd"""
        try:
            logging.info('Writing ComplexType %s', self.xsd_filename)
            tree = ET.ElementTree(self.xml_element)
            tmp_filename = f'{self.xsd_filename:s}.tmp'
            tree.write(tmp_filename)
            if not os.path.exists(self.xsd_filename):
                os.rename(tmp_filename, self.xsd_filename)
            elif filecmp.cmp(tmp_filename, self.xsd_filename):
                os.remove(tmp_filename)
            else:
                os.replace(tmp_filename, self.xsd_filename)
        except:
            logging.error('write_xsd(%s)', self.xml_element)

    def write_h(self):
        """write h"""
        try:
            logging.info('Writing ComplexType %s', self.h_filename)
            global h_filenames
            h_filenames.add(self.h_filename)
            global getter_methods
            if self.name in getter_methods:
                for attribute in self.attributes:
                    if attribute.name in getter_methods[self.name]:
                        (target_type, method_name) = getter_methods[self.name][attribute.name]
                        if target_type == 'void':
                            pass
                        elif attribute.type in ['xs:IDREF', 'knx:IDREF', 'knx:RELIDREF']:
                            self.forward_declarations.add(target_type)
                        elif attribute.type in ['knx:IDREFS', 'knx:RELIDREFS']:
                            self.includes.add(target_type)
                        else:
                            logging.error('not yet implemented')
            tmp_filename = f'{self.h_filename:s}.tmp'
            with open(tmp_filename, mode='w', encoding='utf-8') as h_file:
                global version
                h_file.write('/* This file is generated. */\n')
                h_file.write('\n')
                h_file.write('#pragma once\n')
                h_file.write('\n')
                for include in sorted(self.includes):
                    if include.startswith('xs:'):
                        h_file.write(f'#include <Project/{to_filename(include):s}.h>\n')
                    elif include in ('Base', 'SimpleElementTextType'):
                        h_file.write(f'#include <Project/{to_filename(include):s}.h>\n')
                    elif include.startswith('knx:'):
                        h_file.write(f'#include <Project/v{version:d}/{to_filename(include):s}.h>\n')
                    else:
                        logging.warning('#include <Project/v%d/%s.h>\n', version, to_filename(include))
                h_file.write('\n')
                h_file.write('namespace Project {\n')
                h_file.write(f'namespace v{version:d} {{\n')
                h_file.write('namespace knx {\n')
                if self.forward_declarations:
                    h_file.write('\n')
                    h_file.write('/* forward declarations */\n')
                    for forward_declaration in sorted(self.forward_declarations):
                        h_file.write(f'class {to_cpp(forward_declaration):s};\n')
                h_file.write('\n')
                h_file.write(f'class {to_cpp(self.name):s} : public {to_cpp(self.base):s}\n')
                h_file.write('{\n')
                h_file.write('    Q_OBJECT\n')
                h_file.write('\n')
                h_file.write('public:\n')
                h_file.write(f'    explicit {to_cpp(self.name):s}(Base * parent = nullptr);\n')
                h_file.write(f'    virtual ~{to_cpp(self.name):s}();\n')
                h_file.write('\n')
                h_file.write('    void read(QXmlStreamReader & reader) override;\n')
                h_file.write('    int tableColumnCount() const override;\n')
                h_file.write('    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;\n')
                h_file.write('    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;\n')
                h_file.write('    QVariant treeData(int role = Qt::DisplayRole) const override;\n')
                if self.attributes:
                    h_file.write('\n')
                    h_file.write('    /* attributes */\n')
                    for attribute in self.attributes:
                        assert attribute
                        for line in attribute.get_h():
                            h_file.write(line)
                if self.elements:
                    h_file.write('\n')
                    h_file.write('    /* elements */\n')
                    for element in self.elements:
                        assert element
                        if element.ref:
                            global elements
                            for line in elements[to_cpp(element.ref)].get_h():
                                h_file.write(line)
                        else:
                            for line in element.get_h():
                                h_file.write(line)
                if self.simple_content_type:
                    h_file.write('\n')
                    h_file.write('    /* element text */\n')
                    h_file.write(f'    {to_cpp(self.simple_content_type):s} text{{}};\n')
                if self.name in getter_methods:
                    h_file.write('\n')
                    h_file.write('    /* getters */\n')
                    for attribute in self.attributes:
                        if attribute.name in getter_methods[self.name]:
                            (target_type, method_name) = getter_methods[self.name][attribute.name]
                            if target_type == 'void':
                                if method_name:
                                    h_file.write(f'    Base * {method_name:s}() const; // attribute: {attribute.name:s}\n')
                                else:
                                    h_file.write(f'    // Base * getTODO() const; // attribute: {attribute.name:s}\n')
                            elif attribute.type in ['xs:IDREF', 'knx:IDREF', 'knx:RELIDREF']:
                                h_file.write(f'    {lchop(target_type, 'knx:'):s} * {method_name:s}() const; // attribute: {attribute.name:s}\n')
                            elif attribute.type in ['knx:IDREFS', 'knx:RELIDREFS']:
                                h_file.write(f'    QList<{lchop(target_type, 'knx:'):s} *> {method_name:s}() const; // attribute: {attribute.name:s}\n')
                            else:
                                logging.error('not yet implemented')
                # NOTE special handling for root element
                if self.name == 'knx:KNX_t':
                    h_file.write('\n')
                    h_file.write('    /** map of all known Ids */\n')
                    h_file.write('    QMap<xs::ID, Base *> ids{};\n')
                elif self.name == 'knx:GroupAddress_t':
                    h_file.write('\n')
                    h_file.write('    QString getStyledAddress() const;\n')
                h_file.write('};\n')
                h_file.write('\n')
                h_file.write(f'{to_cpp(self.name):s} * make_{to_cpp(self.name):s}(Base * parent);\n')
                h_file.write('\n')
                h_file.write('} // namespace knx\n')
                h_file.write(f'}} // namespace v{version:d}\n')
                h_file.write('} // namespace Project\n')
            if not os.path.exists(self.h_filename):
                os.rename(tmp_filename, self.h_filename)
            elif filecmp.cmp(tmp_filename, self.h_filename):
                os.remove(tmp_filename)
            else:
                os.replace(tmp_filename, self.h_filename)
        except:
            logging.error('write_h(%s)', self.xml_element)

    def write_cpp(self):
        """write cpp"""
        try:
            logging.info('Writing ComplexType %s', self.cpp_filename)
            global cpp_filenames
            cpp_filenames.add(self.cpp_filename)
            tmp_filename = f'{self.cpp_filename:s}.tmp'
            with open(tmp_filename, mode='w', encoding='utf-8') as cpp_file:
                global version
                cpp_file.write('/* This file is generated. */\n')
                cpp_file.write('\n')
                cpp_file.write(f'#include <Project/v{version:d}/{self.h_filename:s}>\n')
                cpp_file.write('\n')
                cpp_file.write('#include <QDebug>\n')
                cpp_file.write('#include <QIcon>\n')
                cpp_file.write('\n')
                forward_declarations = self.forward_declarations.union(['knx:KNX_t'])
                if self.name == 'knx:DeviceInstance_t':
                    forward_declarations = forward_declarations.union(['knx:MasterData_MediumTypes_MediumType_t', 'knx:Topology_Area_Line_t', 'knx:Topology_Area_t'])
                elif self.name == 'knx:GroupAddress_t':
                    forward_declarations = forward_declarations.union(['knx:Project_t', 'knx:Project_ProjectInformation_t'])
                elif self.name == 'knx:Topology_Area_Line_t':
                    forward_declarations = forward_declarations.union(['knx:Topology_Area_t'])
                for forward_declaration in sorted(forward_declarations):
                    if forward_declaration.startswith('xs:'):
                        cpp_file.write(f'#include <Project/{to_filename(forward_declaration):s}.h>\n')
                    elif forward_declaration in ('Base', 'SimpleElementTextType'):
                        cpp_file.write(f'#include <Project/{to_filename(forward_declaration):s}.h>\n')
                    elif forward_declaration.startswith('knx:'):
                        cpp_file.write(f'#include <Project/v{version:d}/{to_filename(forward_declaration):s}.h>\n')
                    else:
                        logging.warning('#include <Project/v%d/%s.h>\n', version, to_filename(forward_declaration))
                cpp_file.write('\n')
                cpp_file.write('namespace Project {\n')
                cpp_file.write(f'namespace v{version:d} {{\n')
                cpp_file.write('namespace knx {\n')
                cpp_file.write('\n')
                cpp_file.write(f'{to_cpp(self.name):s}::{to_cpp(self.name):s}(Base * parent) :\n')
                cpp_file.write(f'    {to_cpp(self.base):s}(parent)\n')
                cpp_file.write('{\n')
                cpp_file.write('}\n')
                cpp_file.write('\n')
                cpp_file.write(f'{to_cpp(self.name):s}::~{to_cpp(self.name):s}()\n')
                cpp_file.write('{\n')
                cpp_file.write('}\n')

                # read
                cpp_file.write('\n')
                cpp_file.write(f'void {to_cpp(self.name):s}::read(QXmlStreamReader & reader)\n')
                cpp_file.write('{\n')
                cpp_file.write('    Q_ASSERT(reader.isStartElement());\n')
        #            if self.name:
        #                cpp_file.write(f'    Q_ASSERT(reader.name() == QString("{self.name:s}"));\n')
                cpp_file.write('\n')
                cpp_file.write('    Base::read(reader);\n')
                has_name_attribute = False
        #        has_Text_attribute = False
                cpp_file.write('\n')
                cpp_file.write('    /* parse attributes */\n')
                cpp_file.write('    for (const QXmlStreamAttribute & attribute : reader.attributes()) {\n')
                cpp_file.write('        QString name = attribute.name().toString();\n')
                for attribute in self.attributes:
                    assert attribute
                    if attribute.name == 'Name':
                        has_name_attribute = True
        #            elif attribute.name == 'Text':
        #                has_Text_attribute = True
                    global getter_methods
                    if self.name in getter_methods:
                        if attribute.name in getter_methods[self.name]:
                            if (self.name != 'knx:LanguageData_TranslationUnit_TranslationElement_t') and (attribute.name != 'RefId'):
                                (target_type, method_name) = getter_methods[self.name][attribute.name]
                                if target_type == 'void':
                                    cpp_file.write(f'        if (name == QString("{attribute.name:s}")) {{\n')
                                    cpp_file.write('            QString value = attribute.value().toString();\n')
                                    cpp_file.write('            if (!value.isEmpty())\n')
                                    cpp_file.write(f'                qWarning() << "{self.name:s} attribute {attribute.name:s} references to" << value;\n')
                                    cpp_file.write('        }\n')
                    if attribute.name == 'Id':
                        cpp_file.write('        if (name == "Id") {\n')
                        cpp_file.write('            Id = attribute.value().toString();\n')
                        cpp_file.write('            KNX_t * knx = findParent<KNX_t *>();\n')
                        cpp_file.write('            Q_ASSERT(knx);\n')
                        cpp_file.write('            knx->ids[Id] = this;\n')
                        cpp_file.write('            continue;\n')
                        cpp_file.write('        }\n')
                    else:
                        for line in attribute.get_cpp():
                            cpp_file.write(line)
                cpp_file.write('        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;\n')
                cpp_file.write('    }\n')
                cpp_file.write('\n')
                cpp_file.write('    /* parse elements */\n')
                cpp_file.write('    while (reader.readNextStartElement()) {\n')
                for element in self.elements:
                    assert element
                    if element.ref:
                        global elements
                        for line in elements[to_cpp(element.ref)].get_cpp():
                            cpp_file.write(line)
                    else:
                        for line in element.get_cpp():
                            cpp_file.write(line)
                cpp_file.write('        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();\n')
                cpp_file.write('    }\n')
                if self.simple_content_type:
                    cpp_file.write('\n')
                    cpp_file.write('    /* parse text element */\n')
                    cpp_file.write('    text = reader.readElementText();\n')
                cpp_file.write('}\n')

                # tableColumnCount
                cpp_file.write('\n')
                cpp_file.write(f'int {to_cpp(self.name):s}::tableColumnCount() const\n')
                cpp_file.write('{\n')
                cpp_file.write(f'    return {len(self.attributes) + 1:d};\n')
                cpp_file.write('}\n')

                # tableData
                cpp_file.write('\n')
                cpp_file.write(f'QVariant {to_cpp(self.name):s}::tableData(const QString qualifiedName, int role) const\n')
                cpp_file.write('{\n')
                cpp_file.write('    switch (role) {\n')
                cpp_file.write('    case Qt::DisplayRole:\n')
                cpp_file.write('        if (qualifiedName == "ElementType") {\n')
                cpp_file.write(f'            return "{rchop(lchop(self.name, 'knx:'), '_t'):s}";\n')
                cpp_file.write('        }\n')
                for attribute in self.attributes:
                    cpp_file.write(f'        if (qualifiedName == QString("{attribute.name:s}")) {{\n')
                    global simple_types
                    if (attribute.type in simple_types) and simple_types[attribute.type] and simple_types[attribute.type].is_list:
                        cpp_file.write(f'            return {fix_cpp_name(attribute.name):s}.join(\' \');\n')
                    else:
                        cpp_file.write(f'            return {fix_cpp_name(attribute.name):s};\n')
                    cpp_file.write('        }\n')
                cpp_file.write('        break;\n')
                cpp_file.write('    }\n')
                cpp_file.write('    return QVariant();\n')
                cpp_file.write('}\n')

                # tableHeaderData
                cpp_file.write('\n')
                cpp_file.write(f'QVariant {to_cpp(self.name):s}::tableHeaderData(int section, Qt::Orientation orientation, int role) const\n')
                cpp_file.write('{\n')
                cpp_file.write('    if (orientation == Qt::Horizontal) {\n')
                cpp_file.write('        if (role == Qt::DisplayRole) {\n')
                cpp_file.write('            switch (section) {\n')
                cpp_file.write('            case 0:\n')
                cpp_file.write('                return "ElementType";\n')
                index = 1
                for attribute in self.attributes:
                    cpp_file.write(f'            case {index:d}:\n')
                    cpp_file.write(f'                return "{fix_cpp_name(attribute.name):s}";\n')
                    index += 1
                cpp_file.write('            }\n')
                cpp_file.write('        }\n')
                cpp_file.write('    }\n')
                cpp_file.write('    return QVariant();\n')
                cpp_file.write('}\n')

                # treeData
                cpp_file.write('\n')
                cpp_file.write(f'QVariant {to_cpp(self.name):s}::treeData(int role) const\n')
                cpp_file.write('{\n')
                global ref_display_roles
                if self.name in ref_display_roles:
                    cpp_file.write(f'    auto * ref = {ref_display_roles[self.name]:s}();\n')
                    cpp_file.write('    if (ref) {\n')
                    cpp_file.write('        return ref->treeData(role);\n')
                    cpp_file.write('    }\n')
                    cpp_file.write('\n')
                # cpp_file.write('    QVariant retval;\n')
                cpp_file.write('    switch (role) {\n')
                # Qt::DisplayRole
                cpp_file.write('    case Qt::DisplayRole:\n')
                global display_roles
                if self.name in display_roles:
                    cpp_file.write(display_roles[self.name])
                elif has_name_attribute:
                    cpp_file.write(f'        return !Name.isEmpty() ? Name : "{lchop(self.name, 'knx:').split('_')[-2]:s}";\n')
                else:
                    cpp_file.write(f'        return "{lchop(self.name, 'knx:').split('_')[-2]:s}";\n')
                # Qt::DecorationRole
                cpp_file.write('    case Qt::DecorationRole:\n')
                global decoration_roles
                if self.name in decoration_roles:
                    cpp_file.write(decoration_roles[self.name])
                else:
                    cpp_file.write(f'        return QIcon::fromTheme("{rchop(lchop(self.name, 'knx:'), '_t'):s}");\n')
                cpp_file.write('    }\n')
                cpp_file.write('    return QVariant();\n')
                cpp_file.write('}\n')

                # get getter methods
                if self.name in getter_methods:
                    for attribute in self.attributes:
                        if attribute.name in getter_methods[self.name]:
                            (target_type, method_name) = getter_methods[self.name][attribute.name]
                            if target_type == 'void':
                                if method_name:
                                    cpp_file.write('\n')
                                    cpp_file.write(f'Base * {to_cpp(self.name):s}::{method_name:s}() const\n')
                                    cpp_file.write('{\n')
                                    cpp_file.write(f'    if ({attribute.name:s}.isEmpty()) {{\n')
                                    cpp_file.write('        return nullptr;\n')
                                    cpp_file.write('    }\n')
                                    cpp_file.write('    KNX_t * knx = findParent<KNX_t *>();\n')
                                    cpp_file.write('    Q_ASSERT(knx);\n')
                                    cpp_file.write(f'    return qobject_cast<Base *>(knx->ids[{attribute.name:s}]);\n')
                                    cpp_file.write('}\n')
                                else:
                                    pass
                            elif attribute.type in ['xs:IDREF', 'knx:IDREF', 'knx:RELIDREF']:
                                cpp_file.write('\n')
                                cpp_file.write(f'{lchop(target_type, 'knx:'):s} * {to_cpp(self.name):s}::{method_name:s}() const\n')
                                cpp_file.write('{\n')
                                cpp_file.write(f'    if ({attribute.name:s}.isEmpty()) {{\n')
                                cpp_file.write('        return nullptr;\n')
                                cpp_file.write('    }\n')
                                cpp_file.write('    KNX_t * knx = findParent<KNX_t *>();\n')
                                cpp_file.write('    Q_ASSERT(knx);\n')
                                cpp_file.write(f'    return qobject_cast<{lchop(target_type, 'knx:'):s} *>(knx->ids[{attribute.name:s}]);\n')
                                cpp_file.write('}\n')
                            elif attribute.type in ['knx:IDREFS', 'knx:RELIDREFS']:
                                cpp_file.write('\n')
                                cpp_file.write(f'QList<{lchop(target_type, 'knx:'):s} *> {to_cpp(self.name):s}::{method_name:s}() const\n')
                                cpp_file.write('{\n')
                                cpp_file.write('    KNX_t * knx = findParent<KNX_t *>();\n')
                                cpp_file.write('    Q_ASSERT(knx);\n')
                                cpp_file.write(f'    QList<{lchop(target_type, 'knx:'):s} *> retVal;\n')
                                cpp_file.write(f'    for (QString id : {attribute.name:s}) {{\n')
                                cpp_file.write(f'        retVal << qobject_cast<{lchop(target_type, 'knx:'):s} *>(knx->ids[id]);\n')
                                cpp_file.write('    }\n')
                                cpp_file.write('    return retVal;\n')
                                cpp_file.write('}\n')
                            else:
                                logging.error('not yet implemented')

                # NOTE additional methods
                if self.name == 'knx:GroupAddress_t':
                    cpp_file.write('\n')
                    cpp_file.write('QString GroupAddress_t::getStyledAddress() const {\n')
                    cpp_file.write('    Project_t * project = findParent<Project_t *>();\n')
                    cpp_file.write('    Q_ASSERT(project);\n')
                    cpp_file.write('    Project_ProjectInformation_t * projectInformation = project->ProjectInformation;\n')
                    cpp_file.write('    Q_ASSERT(projectInformation);\n')
                    cpp_file.write('    QString groupAddressStyle = projectInformation->GroupAddressStyle;\n')
                    cpp_file.write('    if (groupAddressStyle == "ThreeLevel") {\n')
                    cpp_file.write('        // ThreeLevel: Main Group (M) 5 Bit / Middle Group (Mi) 3 Bit / Subgroup (G) 8 Bit\n')
                    cpp_file.write('        int address = Address.toInt();\n')
                    cpp_file.write('        const uint16_t main = (address >> 11) & 0x1f;\n')
                    cpp_file.write('        const uint16_t middle = (address >> 8) & 0x07;\n')
                    cpp_file.write('        const uint16_t sub = address & 0xff;\n')
                    cpp_file.write('        return QString("%1/%2/%3").arg(main).arg(middle).arg(sub); // 5/3/8\n')
                    cpp_file.write('    }\n')
                    cpp_file.write('    if (groupAddressStyle == "TwoLevel") {\n')
                    cpp_file.write('        // TwoLevel: Main Group (M) 5 Bit / Subgroup (G) 11 Bit\n')
                    cpp_file.write('        int address = Address.toInt();\n')
                    cpp_file.write('        const uint16_t main = (address >> 11) & 0x1f;\n')
                    cpp_file.write('        const uint16_t sub = address & 0x07ff;\n')
                    cpp_file.write('        return QString("%1/%2").arg(main).arg(sub);\n')
                    cpp_file.write('    }\n')
                    cpp_file.write('    // Free: 16 Bit\n')
                    cpp_file.write('    return Address;\n')
                    cpp_file.write('}\n')

                # make
                cpp_file.write('\n')
                cpp_file.write(f'{to_cpp(self.name):s} * make_{to_cpp(self.name):s}(Base * parent)\n')
                cpp_file.write('{\n')
                cpp_file.write(f'    return new {to_cpp(self.name):s}(parent);\n')
                cpp_file.write('}\n')
                cpp_file.write('\n')
                cpp_file.write('} // namespace knx\n')
                cpp_file.write(f'}} // namespace v{version:d}\n')
                cpp_file.write('} // namespace Project\n')

            if not os.path.exists(self.cpp_filename):
                os.rename(tmp_filename, self.cpp_filename)
            elif filecmp.cmp(tmp_filename, self.cpp_filename):
                os.remove(tmp_filename)
            else:
                os.replace(tmp_filename, self.cpp_filename)
        except:
            logging.error('write_cpp(%s)', self.xml_element)


def convert(xsd_filename):
    """convert xsd file"""
    tree = ET.parse(xsd_filename)

    # version
    global version
    target_namespace = tree.getroot().attrib['targetNamespace']
    logging.info('targetNamespace: %s', target_namespace)
    version = int(target_namespace.split('/')[-1])
    logging.info('version: %d', version)

    # init global variables
    global ref_display_roles
    global display_roles
    global decoration_roles
    global id_methods
    global getter_methods
    ref_display_roles = get_ref_display_roles(version)
    display_roles = get_display_roles(version)
    decoration_roles = get_decoration_roles(version)
    id_methods = get_id_methods(version)
    getter_methods = get_getter_methods(version)

    # fixes
    xml_code = (
        f'<?xml version="1.0" encoding="utf-8"?>'
        f'<xs:schema xmlns:knx="{target_namespace:s}" xmlns:xs="http://www.w3.org/2001/XMLSchema" targetNamespace="{target_namespace:s}" elementFormDefault="qualified" attributeFormDefault="unqualified" version="1.0">'
        f'<xs:complexType/>'
        f'</xs:schema>')
    complex_type = ComplexType(ET.fromstring(xml_code)[0], name='knx:ParameterType_TypeNone_t')
    complex_type.includes.add('Base')
    global complex_types
    complex_types['knx:ParameterType_TypeNone_t'] = complex_type

    # early parse xsd file
    early_simple_types = []
    early_complex_types = [
        'ComObject_t',
        'LoadProcedure_t',
        'MemoryParameter_t',
        'MemoryUnion_t',
        'ParameterBase_t',
        'ParameterRefRef_t',
        'PropertyParameter_t',
        'PropertyUnion_t',
        'When_t']
    for xml_element in tree.getroot():
        tag = fix_ns(xml_element.tag)
        if tag == 'xs:complexType':
            if xml_element.attrib['name'] in early_complex_types:
                complex_type = ComplexType(xml_element)
                complex_type.parse()
        elif tag == 'xs:element':
            pass
        elif tag == 'xs:simpleType':
            if xml_element.attrib['name'] in early_simple_types:
                simple_type = SimpleType(xml_element)
                simple_type.parse()
        else:
            logging.warning('tag %s not handled', tag)

    # parse xsd file
    for xml_element in tree.getroot():
        tag = fix_ns(xml_element.tag)
        if tag == 'xs:complexType':
            if not xml_element.attrib['name'] in early_complex_types:
                complex_type = ComplexType(xml_element)
                complex_type.parse()
        elif tag == 'xs:element':
            element = Element(xml_element)
            element.parse()
        elif tag == 'xs:simpleType':
            if not xml_element.attrib['name'] in early_simple_types:
                simple_type = SimpleType(xml_element)
                simple_type.parse()
        else:
            logging.warning('tag %s not handled', tag)

    # add keys for complex_types not having an xs:ID attribute
    complex_types['knx:LanguageData_t'].key_name = 'Identifier'
    complex_types['knx:LanguageData_t'].key_type = 'xs:Language'

    # create xsd, h and cpp files
    global simple_types
    for simple_type in simple_types.keys():
        if simple_types[simple_type]:
            simple_types[simple_type].write_xsd()
            simple_types[simple_type].write_h()
            beautify_file(simple_types[simple_type].h_filename)
    for complex_type in complex_types.keys():
        complex_types[complex_type].write_xsd()
        complex_types[complex_type].write_h()
        complex_types[complex_type].write_cpp()
        beautify_file(complex_types[complex_type].h_filename)
        beautify_file(complex_types[complex_type].cpp_filename)

    # create CMakeLists.txt
    with open('CMakeLists.txt', mode='w', encoding='utf-8') as cmakefile:
        cmakefile.write(
            '# This file is generated.\n'
            '\n'
            f'add_library(OpenETS_Project_v{version:d} "")\n'
            '\n'
            f'target_include_directories(OpenETS_Project_v{version:d}\n'
            '    PUBLIC\n'
            '        ${CMAKE_BINARY_DIR}\n'
            '        ${CMAKE_SOURCE_DIR}\n'
            '        ${ZLIB_INCLUDE_DIR})\n'
            '\n'
            f'set_target_properties(OpenETS_Project_v{version:d} PROPERTIES\n'
            '    CXX_EXTENSIONS OFF\n'
            '    CXX_STANDARD 20\n'
            '    CXX_STANDARD_REQUIRED ON)\n'
            '\n'
            f'target_link_libraries(OpenETS_Project_v{version:d}\n'
            '    PRIVATE\n'
            '        Qt6::Core\n'
            '        Qt6::Gui\n'
            '        Qt6::Widgets\n'
            '        QuaZip::QuaZip)\n'
            '\n'
            f'target_sources(OpenETS_Project_v{version:d}\n'
            '    INTERFACE\n')
        global h_filenames
        for h_filename in sorted(h_filenames):
            cmakefile.write(f'        ${{CMAKE_CURRENT_SOURCE_DIR}}/{h_filename:s}\n')
        cmakefile.write('    PRIVATE')
        global cpp_filenames
        for cpp_filename in sorted(cpp_filenames):
            cmakefile.write(f'\n        ${{CMAKE_CURRENT_SOURCE_DIR}}/{cpp_filename:s}')
        cmakefile.write(')\n')


def main():
    """main"""
    logging.basicConfig(
        format='%(levelname)s:%(name)s:%(message)s [%(filename)s:%(lineno)d]',
        level=logging.WARNING)  # DEBUG, INFO, WARNING, ERROR, CRITICAL

    convert('KNX-Project-Schema.xsd')


if __name__ == '__main__':
    # execute only if run as a script
    main()
