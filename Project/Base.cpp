#include <Project/Base.h>

#include <QDebug>

namespace Project {

Base::Base(Base * parent) :
    QObject(parent)
{
}

Base::~Base()
{
}

void Base::read(QXmlStreamReader & reader)
{
    setObjectName(reader.name().toString());
}

bool Base::canDropMimeData(const QMimeData *data, Qt::DropAction action) const
{
    // @todo canDropMimeData
    Q_UNUSED(data);
    Q_UNUSED(action);
    return false;
}

Base * Base::child(int row) const
{
    if ((row >= 0) && (row < childCount())) {
        return dynamic_cast<Base *>(children().at(row));
    }

    return nullptr;
}

int Base::childCount() const
{
    return children().count();
}

bool Base::dropMimeData(const QMimeData *data, Qt::DropAction action)
{
    // @todo dropMimeData
    Q_UNUSED(data);
    Q_UNUSED(action);
    return false;
}

Qt::ItemFlags Base::flags(Qt::ItemFlags defaultFlags) const
{
    // @todo flags
    return defaultFlags;
}

QMimeData *Base::mimeData() const
{
    // @todo mimeData
    return nullptr;
}

int Base::row() const
{
    if (parent()) {
        return parentItem()->children().indexOf(const_cast<Base *>(this));
    }

    return 0;
}

Base * Base::parentItem() const
{
    if (parent()) {
        return dynamic_cast<Base *>(parent());
    }

    return nullptr;
}

void Base::update()
{
    Q_EMIT updated();
}

} // namespace Project
