#pragma once

#include <QByteArray>
#include <QMap>
#include <QSet>
#include <QString>

#include <quazip/quazip.h>

#include <Project/v10/knx/KNX_t.h>
#include <Project/v11/knx/KNX_t.h>
#include <Project/v12/knx/KNX_t.h>
#include <Project/v13/knx/KNX_t.h>
#include <Project/v14/knx/KNX_t.h>
#include <Project/v20/knx/KNX_t.h>

/**
 * @brief Project Bundle
 *
 * This is the base class to import knxproj files, which includes
 * several XML files (project, manufacturer, master), signatures and
 * bundled files (icons, graphics).
 *
 * @todo Modify Attributes: Modify, Set to default
 * @todo Modify Elements: Add, Shift, Delete
 */
class ProjectBundle :
    public QObject
{
    Q_OBJECT

public:
    explicit ProjectBundle(QObject * parent = nullptr);
    virtual ~ProjectBundle();

    /** ETS Project Path (knxproj file or unzipped dir) */
    QString currentPath{};

    void setPath(const QString & path);

    /** close file. Reset all project data */
    void close();

    /** load all contained XML and signature files */
    void load();

    /** import file into loaded project */
    void import(QString & fileName);

    /** save all contained XML and signature files */
    void save();

    /** ETS project version */
    int etsProjectVersion{0};  // 0 if the file is closed
    union {
        Project::v10::knx::KNX_t * etsProject10{nullptr};
        Project::v11::knx::KNX_t * etsProject11;
        Project::v12::knx::KNX_t * etsProject12;
        Project::v13::knx::KNX_t * etsProject13;
        Project::v14::knx::KNX_t * etsProject14;
        Project::v20::knx::KNX_t * etsProject20;
    };
    QMap<QString, QByteArray> signatures{};
    QMap<QString, QByteArray> addinDatas{};
    QMap<QString, QByteArray> baggages{};

Q_SIGNALS:
    void loadProgress(int count, int total, QString filename);

private:
    // zip for file, nullptr for dir
    QuaZip * zip{nullptr};
    int getProjectVersion(QIODevice * file) const;
    void loadFile(QString & fileName, QIODevice * file);
};
