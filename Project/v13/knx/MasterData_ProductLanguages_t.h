/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v13/knx/MasterData_ProductLanguages_Language_t.h>

namespace Project {
namespace v13 {
namespace knx {

class MasterData_ProductLanguages_t : public Base
{
    Q_OBJECT

public:
    explicit MasterData_ProductLanguages_t(Base * parent = nullptr);
    virtual ~MasterData_ProductLanguages_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* elements */
    QVector<MasterData_ProductLanguages_Language_t *> Language;
};

MasterData_ProductLanguages_t * make_MasterData_ProductLanguages_t(Base * parent);

} // namespace knx
} // namespace v13
} // namespace Project
