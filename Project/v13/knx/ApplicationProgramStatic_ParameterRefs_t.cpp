/* This file is generated. */

#include <Project/v13/knx/ApplicationProgramStatic_ParameterRefs_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v13/knx/KNX_t.h>

namespace Project {
namespace v13 {
namespace knx {

ApplicationProgramStatic_ParameterRefs_t::ApplicationProgramStatic_ParameterRefs_t(Base * parent) :
    Base(parent)
{
}

ApplicationProgramStatic_ParameterRefs_t::~ApplicationProgramStatic_ParameterRefs_t()
{
}

void ApplicationProgramStatic_ParameterRefs_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("ParameterRef")) {
            QString newId = reader.attributes().value("Id").toString();
            Q_ASSERT(!newId.isEmpty());
            ParameterRef_t * newParameterRef;
            if (ParameterRef.contains(newId)) {
                newParameterRef = ParameterRef[newId];
            } else {
                newParameterRef = make_ParameterRef_t(this);
                ParameterRef[newId] = newParameterRef;
            }
            newParameterRef->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ApplicationProgramStatic_ParameterRefs_t::tableColumnCount() const
{
    return 1;
}

QVariant ApplicationProgramStatic_ParameterRefs_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ApplicationProgramStatic_ParameterRefs";
        }
        break;
    }
    return QVariant();
}

QVariant ApplicationProgramStatic_ParameterRefs_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant ApplicationProgramStatic_ParameterRefs_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "ParameterRefs";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ApplicationProgramStatic_ParameterRefs");
    }
    return QVariant();
}

ApplicationProgramStatic_ParameterRefs_t * make_ApplicationProgramStatic_ParameterRefs_t(Base * parent)
{
    return new ApplicationProgramStatic_ParameterRefs_t(parent);
}

} // namespace knx
} // namespace v13
} // namespace Project
