/* This file is generated. */

#include <Project/v13/knx/ApplicationProgramChannel_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v13/knx/BinaryDataRef_t.h>
#include <Project/v13/knx/ChannelChoose_t.h>
#include <Project/v13/knx/ComObjectParameterBlock_t.h>
#include <Project/v13/knx/ComObjectRefRef_t.h>
#include <Project/v13/knx/KNX_t.h>
#include <Project/v13/knx/ParameterRef_t.h>

namespace Project {
namespace v13 {
namespace knx {

ApplicationProgramChannel_t::ApplicationProgramChannel_t(Base * parent) :
    Base(parent)
{
}

ApplicationProgramChannel_t::~ApplicationProgramChannel_t()
{
}

void ApplicationProgramChannel_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("Name")) {
            Name = attribute.value().toString();
            continue;
        }
        if (name == QString("Text")) {
            Text = attribute.value().toString();
            continue;
        }
        if (name == QString("Number")) {
            Number = attribute.value().toString();
            continue;
        }
        if (name == "Id") {
            Id = attribute.value().toString();
            KNX_t * knx = findParent<KNX_t *>();
            Q_ASSERT(knx);
            knx->ids[Id] = this;
            continue;
        }
        if (name == QString("TextParameterRefId")) {
            TextParameterRefId = attribute.value().toString();
            continue;
        }
        if (name == QString("InternalDescription")) {
            InternalDescription = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("ParameterBlock")) {
            auto * ParameterBlock = make_ComObjectParameterBlock_t(this);
            ParameterBlock->read(reader);
            continue;
        }
        if (reader.name() == QString("ComObjectRefRef")) {
            auto * ComObjectRefRef = make_ComObjectRefRef_t(this);
            ComObjectRefRef->read(reader);
            continue;
        }
        if (reader.name() == QString("BinaryDataRef")) {
            auto * BinaryDataRef = make_BinaryDataRef_t(this);
            BinaryDataRef->read(reader);
            continue;
        }
        if (reader.name() == QString("choose")) {
            auto * Choose = make_ChannelChoose_t(this);
            Choose->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ApplicationProgramChannel_t::tableColumnCount() const
{
    return 7;
}

QVariant ApplicationProgramChannel_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ApplicationProgramChannel";
        }
        if (qualifiedName == QString("Name")) {
            return Name;
        }
        if (qualifiedName == QString("Text")) {
            return Text;
        }
        if (qualifiedName == QString("Number")) {
            return Number;
        }
        if (qualifiedName == QString("Id")) {
            return Id;
        }
        if (qualifiedName == QString("TextParameterRefId")) {
            return TextParameterRefId;
        }
        if (qualifiedName == QString("InternalDescription")) {
            return InternalDescription;
        }
        break;
    }
    return QVariant();
}

QVariant ApplicationProgramChannel_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Name";
            case 2:
                return "Text";
            case 3:
                return "Number";
            case 4:
                return "Id";
            case 5:
                return "TextParameterRefId";
            case 6:
                return "InternalDescription";
            }
        }
    }
    return QVariant();
}

QVariant ApplicationProgramChannel_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return QString("[%1] %2").arg(Number).arg(Name);
    case Qt::DecorationRole:
        return QIcon::fromTheme("ApplicationProgramChannel");
    }
    return QVariant();
}

ParameterRef_t * ApplicationProgramChannel_t::getTextParameterRef() const
{
    if (TextParameterRefId.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<ParameterRef_t *>(knx->ids[TextParameterRefId]);
}

ApplicationProgramChannel_t * make_ApplicationProgramChannel_t(Base * parent)
{
    return new ApplicationProgramChannel_t(parent);
}

} // namespace knx
} // namespace v13
} // namespace Project
