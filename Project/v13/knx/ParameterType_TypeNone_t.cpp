/* This file is generated. */

#include <Project/v13/knx/ParameterType_TypeNone_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v13/knx/KNX_t.h>

namespace Project {
namespace v13 {
namespace knx {

ParameterType_TypeNone_t::ParameterType_TypeNone_t(Base * parent) :
    Base(parent)
{
}

ParameterType_TypeNone_t::~ParameterType_TypeNone_t()
{
}

void ParameterType_TypeNone_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ParameterType_TypeNone_t::tableColumnCount() const
{
    return 1;
}

QVariant ParameterType_TypeNone_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ParameterType_TypeNone";
        }
        break;
    }
    return QVariant();
}

QVariant ParameterType_TypeNone_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant ParameterType_TypeNone_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "TypeNone";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ParameterType_TypeNone");
    }
    return QVariant();
}

ParameterType_TypeNone_t * make_ParameterType_TypeNone_t(Base * parent)
{
    return new ParameterType_TypeNone_t(parent);
}

} // namespace knx
} // namespace v13
} // namespace Project
