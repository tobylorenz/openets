/* This file is generated. */

#pragma once

#include <Project/xs/NCName.h>

namespace Project {
namespace v13 {
namespace knx {

using IDREF = xs::NCName;

} // namespace knx
} // namespace v13
} // namespace Project
