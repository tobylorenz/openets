/* This file is generated. */

#pragma once

#include <Project/xs/String.h>

namespace Project {
namespace v13 {
namespace knx {

using Access_t = xs::String;

} // namespace knx
} // namespace v13
} // namespace Project
