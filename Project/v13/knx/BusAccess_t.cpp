/* This file is generated. */

#include <Project/v13/knx/BusAccess_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v13/knx/KNX_t.h>

namespace Project {
namespace v13 {
namespace knx {

BusAccess_t::BusAccess_t(Base * parent) :
    Base(parent)
{
}

BusAccess_t::~BusAccess_t()
{
}

void BusAccess_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("Name")) {
            Name = attribute.value().toString();
            continue;
        }
        if (name == QString("Edi")) {
            Edi = attribute.value().toString();
            continue;
        }
        if (name == QString("Parameter")) {
            Parameter = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int BusAccess_t::tableColumnCount() const
{
    return 4;
}

QVariant BusAccess_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "BusAccess";
        }
        if (qualifiedName == QString("Name")) {
            return Name;
        }
        if (qualifiedName == QString("Edi")) {
            return Edi;
        }
        if (qualifiedName == QString("Parameter")) {
            return Parameter;
        }
        break;
    }
    return QVariant();
}

QVariant BusAccess_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Name";
            case 2:
                return "Edi";
            case 3:
                return "Parameter";
            }
        }
    }
    return QVariant();
}

QVariant BusAccess_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return !Name.isEmpty() ? Name : "BusAccess";
    case Qt::DecorationRole:
        return QIcon::fromTheme("BusAccess");
    }
    return QVariant();
}

BusAccess_t * make_BusAccess_t(Base * parent)
{
    return new BusAccess_t(parent);
}

} // namespace knx
} // namespace v13
} // namespace Project
