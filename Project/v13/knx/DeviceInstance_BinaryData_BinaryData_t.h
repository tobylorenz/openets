/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/SimpleElementTextType.h>
#include <Project/v13/knx/IDREF.h>
#include <Project/v13/knx/String50_t.h>
#include <Project/xs/Base64Binary.h>
#include <Project/xs/String.h>

namespace Project {
namespace v13 {
namespace knx {

class DeviceInstance_BinaryData_BinaryData_t : public Base
{
    Q_OBJECT

public:
    explicit DeviceInstance_BinaryData_BinaryData_t(Base * parent = nullptr);
    virtual ~DeviceInstance_BinaryData_BinaryData_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::String Id{};
    IDREF RefId{};
    String50_t Name{};

    /* elements */
    SimpleElementTextType * Data{};

    /* getters */
    Base * getBinaryData() const; // attribute: RefId
};

DeviceInstance_BinaryData_BinaryData_t * make_DeviceInstance_BinaryData_BinaryData_t(Base * parent);

} // namespace knx
} // namespace v13
} // namespace Project
