/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v13/knx/Condition_t.h>
#include <Project/xs/Boolean.h>
#include <Project/xs/String.h>

namespace Project {
namespace v13 {
namespace knx {

/* forward declarations */
class BinaryDataRef_t;
class ChannelChoose_t;
class ComObjectParameterBlock_t;
class ComObjectRefRef_t;
class Rename_t;

class ChannelChoose_when_t : public Base
{
    Q_OBJECT

public:
    explicit ChannelChoose_when_t(Base * parent = nullptr);
    virtual ~ChannelChoose_when_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    Condition_t Test{};
    xs::Boolean Default{"false"};
    xs::String InternalDescription{};

    /* elements */
    // xs:choice ComObjectParameterBlock_t * ParameterBlock{};
    // xs:choice ComObjectRefRef_t * ComObjectRefRef{};
    // xs:choice BinaryDataRef_t * BinaryDataRef{};
    // xs:choice ChannelChoose_t * Choose{};
    // xs:choice Rename_t * Rename{};
};

ChannelChoose_when_t * make_ChannelChoose_when_t(Base * parent);

} // namespace knx
} // namespace v13
} // namespace Project
