/* This file is generated. */

#include <Project/v13/knx/ParameterInstanceRef_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v13/knx/KNX_t.h>
#include <Project/v13/knx/ParameterRef_t.h>

namespace Project {
namespace v13 {
namespace knx {

ParameterInstanceRef_t::ParameterInstanceRef_t(Base * parent) :
    Base(parent)
{
}

ParameterInstanceRef_t::~ParameterInstanceRef_t()
{
}

void ParameterInstanceRef_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == "Id") {
            Id = attribute.value().toString();
            KNX_t * knx = findParent<KNX_t *>();
            Q_ASSERT(knx);
            knx->ids[Id] = this;
            continue;
        }
        if (name == QString("RefId")) {
            RefId = attribute.value().toString();
            continue;
        }
        if (name == QString("Value")) {
            Value = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ParameterInstanceRef_t::tableColumnCount() const
{
    return 4;
}

QVariant ParameterInstanceRef_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ParameterInstanceRef";
        }
        if (qualifiedName == QString("Id")) {
            return Id;
        }
        if (qualifiedName == QString("RefId")) {
            return RefId;
        }
        if (qualifiedName == QString("Value")) {
            return Value;
        }
        break;
    }
    return QVariant();
}

QVariant ParameterInstanceRef_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Id";
            case 2:
                return "RefId";
            case 3:
                return "Value";
            }
        }
    }
    return QVariant();
}

QVariant ParameterInstanceRef_t::treeData(int role) const
{
    auto * ref = getParameterRef();
    if (ref) {
        return ref->treeData(role);
    }

    switch (role) {
    case Qt::DisplayRole:
        return "ParameterInstanceRef";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ParameterInstanceRef");
    }
    return QVariant();
}

ParameterRef_t * ParameterInstanceRef_t::getParameterRef() const
{
    if (RefId.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<ParameterRef_t *>(knx->ids[RefId]);
}

ParameterInstanceRef_t * make_ParameterInstanceRef_t(Base * parent)
{
    return new ParameterInstanceRef_t(parent);
}

} // namespace knx
} // namespace v13
} // namespace Project
