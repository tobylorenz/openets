/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v13/knx/String50_t.h>
#include <Project/xs/DateTime.h>
#include <Project/xs/String.h>

namespace Project {
namespace v13 {
namespace knx {

class Project_ProjectInformation_HistoryEntries_HistoryEntry_t : public Base
{
    Q_OBJECT

public:
    explicit Project_ProjectInformation_HistoryEntries_HistoryEntry_t(Base * parent = nullptr);
    virtual ~Project_ProjectInformation_HistoryEntries_HistoryEntry_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::DateTime Date{};
    String50_t User{};
    xs::String Text{};
    xs::String Detail{};
};

Project_ProjectInformation_HistoryEntries_HistoryEntry_t * make_Project_ProjectInformation_HistoryEntries_HistoryEntry_t(Base * parent);

} // namespace knx
} // namespace v13
} // namespace Project
