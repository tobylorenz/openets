/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v13/knx/MaskVersion_MaskEntries_MaskEntry_t.h>
#include <Project/xs/ID.h>

namespace Project {
namespace v13 {
namespace knx {

class MaskVersion_MaskEntries_t : public Base
{
    Q_OBJECT

public:
    explicit MaskVersion_MaskEntries_t(Base * parent = nullptr);
    virtual ~MaskVersion_MaskEntries_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* elements */
    QMap<xs::ID, MaskVersion_MaskEntries_MaskEntry_t *> MaskEntry; // key: Id
};

MaskVersion_MaskEntries_t * make_MaskVersion_MaskEntries_t(Base * parent);

} // namespace knx
} // namespace v13
} // namespace Project
