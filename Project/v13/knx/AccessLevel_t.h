/* This file is generated. */

#pragma once

#include <Project/xs/UnsignedByte.h>

namespace Project {
namespace v13 {
namespace knx {

using AccessLevel_t = xs::UnsignedByte;

} // namespace knx
} // namespace v13
} // namespace Project
