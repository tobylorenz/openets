/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v13/knx/Aes128Key_t.h>
#include <Project/v13/knx/String20_t.h>
#include <Project/xs/DateTime.h>
#include <Project/xs/UnsignedLong.h>

namespace Project {
namespace v13 {
namespace knx {

class Security_t : public Base
{
    Q_OBJECT

public:
    explicit Security_t(Base * parent = nullptr);
    virtual ~Security_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    Aes128Key_t LoadedIPRoutingBackboneKey{};
    String20_t DeviceAuthenticationCode{};
    String20_t LoadedDeviceAuthenticationCode{};
    String20_t DeviceManagementPassword{};
    String20_t LoadedDeviceManagementPassword{};
    Aes128Key_t ToolKey{};
    Aes128Key_t LoadedToolKey{};
    xs::UnsignedLong SequenceNumber{};
    xs::DateTime SequenceNumberTimestamp{};
};

Security_t * make_Security_t(Base * parent);

} // namespace knx
} // namespace v13
} // namespace Project
