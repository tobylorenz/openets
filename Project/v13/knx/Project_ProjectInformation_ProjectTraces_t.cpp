/* This file is generated. */

#include <Project/v13/knx/Project_ProjectInformation_ProjectTraces_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v13/knx/KNX_t.h>

namespace Project {
namespace v13 {
namespace knx {

Project_ProjectInformation_ProjectTraces_t::Project_ProjectInformation_ProjectTraces_t(Base * parent) :
    Base(parent)
{
}

Project_ProjectInformation_ProjectTraces_t::~Project_ProjectInformation_ProjectTraces_t()
{
}

void Project_ProjectInformation_ProjectTraces_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("ProjectTrace")) {
            auto * newProjectTrace = make_ProjectTrace_t(this);
            newProjectTrace->read(reader);
            ProjectTrace.append(newProjectTrace);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int Project_ProjectInformation_ProjectTraces_t::tableColumnCount() const
{
    return 1;
}

QVariant Project_ProjectInformation_ProjectTraces_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "Project_ProjectInformation_ProjectTraces";
        }
        break;
    }
    return QVariant();
}

QVariant Project_ProjectInformation_ProjectTraces_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant Project_ProjectInformation_ProjectTraces_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "ProjectTraces";
    case Qt::DecorationRole:
        return QIcon::fromTheme("Project_ProjectInformation_ProjectTraces");
    }
    return QVariant();
}

Project_ProjectInformation_ProjectTraces_t * make_Project_ProjectInformation_ProjectTraces_t(Base * parent)
{
    return new Project_ProjectInformation_ProjectTraces_t(parent);
}

} // namespace knx
} // namespace v13
} // namespace Project
