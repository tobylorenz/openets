/* This file is generated. */

#pragma once

#include <Project/xs/UnsignedInt.h>

namespace Project {
namespace v13 {
namespace knx {

using ApplicationProgramStatic_Parameters_Union_Memory_Offset_t = xs::UnsignedInt;

} // namespace knx
} // namespace v13
} // namespace Project
