/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v13/knx/IDREF.h>
#include <Project/v13/knx/MasterData_FunctionalBlocks_FunctionalBlock_Parameters_Parameter_t.h>

namespace Project {
namespace v13 {
namespace knx {

/* forward declarations */
class MasterData_InterfaceObjectTypes_InterfaceObjectType_t;

class MasterData_FunctionalBlocks_FunctionalBlock_Parameters_t : public Base
{
    Q_OBJECT

public:
    explicit MasterData_FunctionalBlocks_FunctionalBlock_Parameters_t(Base * parent = nullptr);
    virtual ~MasterData_FunctionalBlocks_FunctionalBlock_Parameters_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    IDREF ObjectType{};

    /* elements */
    QMap<IDREF, MasterData_FunctionalBlocks_FunctionalBlock_Parameters_Parameter_t *> Parameter; // key: Property

    /* getters */
    MasterData_InterfaceObjectTypes_InterfaceObjectType_t * getInterfaceObjectType() const; // attribute: ObjectType
};

MasterData_FunctionalBlocks_FunctionalBlock_Parameters_t * make_MasterData_FunctionalBlocks_FunctionalBlock_Parameters_t(Base * parent);

} // namespace knx
} // namespace v13
} // namespace Project
