/* This file is generated. */

#include <Project/v13/knx/ParameterType_TypePicture_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v13/knx/KNX_t.h>
#include <Project/v13/knx/ManufacturerData_Manufacturer_Baggages_Baggage_t.h>

namespace Project {
namespace v13 {
namespace knx {

ParameterType_TypePicture_t::ParameterType_TypePicture_t(Base * parent) :
    Base(parent)
{
}

ParameterType_TypePicture_t::~ParameterType_TypePicture_t()
{
}

void ParameterType_TypePicture_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("RefId")) {
            RefId = attribute.value().toString();
            continue;
        }
        if (name == QString("HorizontalAlignment")) {
            HorizontalAlignment = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ParameterType_TypePicture_t::tableColumnCount() const
{
    return 3;
}

QVariant ParameterType_TypePicture_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ParameterType_TypePicture";
        }
        if (qualifiedName == QString("RefId")) {
            return RefId;
        }
        if (qualifiedName == QString("HorizontalAlignment")) {
            return HorizontalAlignment;
        }
        break;
    }
    return QVariant();
}

QVariant ParameterType_TypePicture_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "RefId";
            case 2:
                return "HorizontalAlignment";
            }
        }
    }
    return QVariant();
}

QVariant ParameterType_TypePicture_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "TypePicture";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ParameterType_TypePicture");
    }
    return QVariant();
}

ManufacturerData_Manufacturer_Baggages_Baggage_t * ParameterType_TypePicture_t::getBaggage() const
{
    if (RefId.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<ManufacturerData_Manufacturer_Baggages_Baggage_t *>(knx->ids[RefId]);
}

ParameterType_TypePicture_t * make_ParameterType_TypePicture_t(Base * parent)
{
    return new ParameterType_TypePicture_t(parent);
}

} // namespace knx
} // namespace v13
} // namespace Project
