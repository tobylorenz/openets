/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v13/knx/MasterData_MediumTypes_MediumType_t.h>
#include <Project/xs/ID.h>

namespace Project {
namespace v13 {
namespace knx {

class MasterData_MediumTypes_t : public Base
{
    Q_OBJECT

public:
    explicit MasterData_MediumTypes_t(Base * parent = nullptr);
    virtual ~MasterData_MediumTypes_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* elements */
    QMap<xs::ID, MasterData_MediumTypes_MediumType_t *> MediumType; // key: Id
};

MasterData_MediumTypes_t * make_MasterData_MediumTypes_t(Base * parent);

} // namespace knx
} // namespace v13
} // namespace Project
