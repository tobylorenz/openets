/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v13/knx/HawkConfigurationData_t.h>
#include <Project/v13/knx/IDREF.h>
#include <Project/v13/knx/LanguageDependentString50_t.h>
#include <Project/v13/knx/MaskVersion_ManagementModel_t.h>
#include <Project/xs/ID.h>
#include <Project/xs/HexBinary.h>
#include <Project/xs/UnsignedShort.h>

namespace Project {
namespace v13 {
namespace knx {

/* forward declarations */
class MaskVersion_DownwardCompatibleMasks_t;
class MaskVersion_MaskEntries_t;
class MasterData_MediumTypes_MediumType_t;

class MaskVersion_t : public Base
{
    Q_OBJECT

public:
    explicit MaskVersion_t(Base * parent = nullptr);
    virtual ~MaskVersion_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::ID Id{};
    LanguageDependentString50_t Name{};
    xs::UnsignedShort MaskVersion{};
    xs::HexBinary MgmtDescriptor01{};
    MaskVersion_ManagementModel_t ManagementModel{};
    IDREF MediumTypeRefId{};
    IDREF OtherMediumTypeRefId{};

    /* elements */
    MaskVersion_DownwardCompatibleMasks_t * DownwardCompatibleMasks{};
    MaskVersion_MaskEntries_t * MaskEntries{};
    QVector<HawkConfigurationData_t *> HawkConfigurationData;

    /* getters */
    MasterData_MediumTypes_MediumType_t * getMediumType() const; // attribute: MediumTypeRefId
    MasterData_MediumTypes_MediumType_t * getOtherMediumType() const; // attribute: OtherMediumTypeRefId
};

MaskVersion_t * make_MaskVersion_t(Base * parent);

} // namespace knx
} // namespace v13
} // namespace Project
