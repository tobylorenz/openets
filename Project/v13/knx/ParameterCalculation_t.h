/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/SimpleElementTextType.h>
#include <Project/v13/knx/ParameterCalculation_Language_t.h>
#include <Project/v13/knx/String50_t.h>
#include <Project/xs/ID.h>
#include <Project/xs/String.h>

namespace Project {
namespace v13 {
namespace knx {

/* forward declarations */
class ParameterCalculation_LParameters_t;
class ParameterCalculation_RParameters_t;

class ParameterCalculation_t : public Base
{
    Q_OBJECT

public:
    explicit ParameterCalculation_t(Base * parent = nullptr);
    virtual ~ParameterCalculation_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::ID Id{};
    ParameterCalculation_Language_t Language{};
    String50_t Name{};
    xs::String InternalDescription{};

    /* elements */
    SimpleElementTextType * RLTransformation{};
    SimpleElementTextType * LRTransformation{};
    ParameterCalculation_LParameters_t * LParameters{};
    ParameterCalculation_RParameters_t * RParameters{};
};

ParameterCalculation_t * make_ParameterCalculation_t(Base * parent);

} // namespace knx
} // namespace v13
} // namespace Project
