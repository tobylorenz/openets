/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v13/knx/LanguageDependentString50_t.h>
#include <Project/v13/knx/String20_t.h>
#include <Project/xs/ID.h>
#include <Project/xs/Short.h>
#include <Project/xs/UnsignedInt.h>

namespace Project {
namespace v13 {
namespace knx {

class MasterData_MediumTypes_MediumType_t : public Base
{
    Q_OBJECT

public:
    explicit MasterData_MediumTypes_MediumType_t(Base * parent = nullptr);
    virtual ~MasterData_MediumTypes_MediumType_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::ID Id{};
    xs::UnsignedInt Number{};
    String20_t Name{};
    LanguageDependentString50_t Text{};
    xs::Short DomainAddressLength{};
};

MasterData_MediumTypes_MediumType_t * make_MasterData_MediumTypes_MediumType_t(Base * parent);

} // namespace knx
} // namespace v13
} // namespace Project
