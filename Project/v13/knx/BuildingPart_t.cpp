/* This file is generated. */

#include <Project/v13/knx/BuildingPart_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v13/knx/KNX_t.h>

namespace Project {
namespace v13 {
namespace knx {

BuildingPart_t::BuildingPart_t(Base * parent) :
    Base(parent)
{
}

BuildingPart_t::~BuildingPart_t()
{
}

void BuildingPart_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == "Id") {
            Id = attribute.value().toString();
            KNX_t * knx = findParent<KNX_t *>();
            Q_ASSERT(knx);
            knx->ids[Id] = this;
            continue;
        }
        if (name == QString("Name")) {
            Name = attribute.value().toString();
            continue;
        }
        if (name == QString("Type")) {
            Type = attribute.value().toString();
            continue;
        }
        if (name == QString("Number")) {
            Number = attribute.value().toString();
            continue;
        }
        if (name == QString("Comment")) {
            Comment = attribute.value().toString();
            continue;
        }
        if (name == QString("Description")) {
            Description = attribute.value().toString();
            continue;
        }
        if (name == QString("CompletionStatus")) {
            CompletionStatus = attribute.value().toString();
            continue;
        }
        if (name == QString("DefaultLine")) {
            DefaultLine = attribute.value().toString();
            continue;
        }
        if (name == QString("Puid")) {
            Puid = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("BuildingPart")) {
            QString newId = reader.attributes().value("Id").toString();
            Q_ASSERT(!newId.isEmpty());
            BuildingPart_t * newBuildingPart;
            if (BuildingPart.contains(newId)) {
                newBuildingPart = BuildingPart[newId];
            } else {
                newBuildingPart = make_BuildingPart_t(this);
                BuildingPart[newId] = newBuildingPart;
            }
            newBuildingPart->read(reader);
            continue;
        }
        if (reader.name() == QString("DeviceInstanceRef")) {
            QString newRefId = reader.attributes().value("RefId").toString();
            Q_ASSERT(!newRefId.isEmpty());
            DeviceInstanceRef_t * newDeviceInstanceRef;
            if (DeviceInstanceRef.contains(newRefId)) {
                newDeviceInstanceRef = DeviceInstanceRef[newRefId];
            } else {
                newDeviceInstanceRef = make_DeviceInstanceRef_t(this);
                DeviceInstanceRef[newRefId] = newDeviceInstanceRef;
            }
            newDeviceInstanceRef->read(reader);
            continue;
        }
        if (reader.name() == QString("Function")) {
            QString newId = reader.attributes().value("Id").toString();
            Q_ASSERT(!newId.isEmpty());
            Function_t * newFunction;
            if (Function.contains(newId)) {
                newFunction = Function[newId];
            } else {
                newFunction = make_Function_t(this);
                Function[newId] = newFunction;
            }
            newFunction->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int BuildingPart_t::tableColumnCount() const
{
    return 10;
}

QVariant BuildingPart_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "BuildingPart";
        }
        if (qualifiedName == QString("Id")) {
            return Id;
        }
        if (qualifiedName == QString("Name")) {
            return Name;
        }
        if (qualifiedName == QString("Type")) {
            return Type;
        }
        if (qualifiedName == QString("Number")) {
            return Number;
        }
        if (qualifiedName == QString("Comment")) {
            return Comment;
        }
        if (qualifiedName == QString("Description")) {
            return Description;
        }
        if (qualifiedName == QString("CompletionStatus")) {
            return CompletionStatus;
        }
        if (qualifiedName == QString("DefaultLine")) {
            return DefaultLine;
        }
        if (qualifiedName == QString("Puid")) {
            return Puid;
        }
        break;
    }
    return QVariant();
}

QVariant BuildingPart_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Id";
            case 2:
                return "Name";
            case 3:
                return "Type";
            case 4:
                return "Number";
            case 5:
                return "Comment";
            case 6:
                return "Description";
            case 7:
                return "CompletionStatus";
            case 8:
                return "DefaultLine";
            case 9:
                return "Puid";
            }
        }
    }
    return QVariant();
}

QVariant BuildingPart_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return !Name.isEmpty() ? Name : "BuildingPart";
    case Qt::DecorationRole:
        return QIcon::fromTheme(QString("BuildingPart_%1").arg(Type));
    }
    return QVariant();
}

BuildingPart_t * make_BuildingPart_t(Base * parent)
{
    return new BuildingPart_t(parent);
}

} // namespace knx
} // namespace v13
} // namespace Project
