/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v13/knx/LdCtrlProcType_t.h>
#include <Project/xs/String.h>
#include <Project/xs/UnsignedByte.h>
#include <Project/xs/UnsignedShort.h>

namespace Project {
namespace v13 {
namespace knx {

class LoadProcedure_LdCtrlUnload_t : public Base
{
    Q_OBJECT

public:
    explicit LoadProcedure_LdCtrlUnload_t(Base * parent = nullptr);
    virtual ~LoadProcedure_LdCtrlUnload_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::UnsignedByte LsmIdx{};
    xs::UnsignedShort ObjType{};
    xs::UnsignedByte Occurrence{"0"};
    LdCtrlProcType_t AppliesTo{"auto"};
    xs::String InternalDescription{};
};

LoadProcedure_LdCtrlUnload_t * make_LoadProcedure_LdCtrlUnload_t(Base * parent);

} // namespace knx
} // namespace v13
} // namespace Project
