/* This file is generated. */

#include <Project/v13/knx/BusInterface_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v13/knx/BusInterface_Connectors_t.h>
#include <Project/v13/knx/KNX_t.h>

namespace Project {
namespace v13 {
namespace knx {

BusInterface_t::BusInterface_t(Base * parent) :
    Base(parent)
{
}

BusInterface_t::~BusInterface_t()
{
}

void BusInterface_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("Name")) {
            Name = attribute.value().toString();
            continue;
        }
        if (name == QString("Description")) {
            Description = attribute.value().toString();
            continue;
        }
        if (name == QString("Comment")) {
            Comment = attribute.value().toString();
            continue;
        }
        if (name == QString("Password")) {
            Password = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("Connectors")) {
            if (!Connectors) {
                Connectors = make_BusInterface_Connectors_t(this);
            }
            Connectors->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int BusInterface_t::tableColumnCount() const
{
    return 5;
}

QVariant BusInterface_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "BusInterface";
        }
        if (qualifiedName == QString("Name")) {
            return Name;
        }
        if (qualifiedName == QString("Description")) {
            return Description;
        }
        if (qualifiedName == QString("Comment")) {
            return Comment;
        }
        if (qualifiedName == QString("Password")) {
            return Password;
        }
        break;
    }
    return QVariant();
}

QVariant BusInterface_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Name";
            case 2:
                return "Description";
            case 3:
                return "Comment";
            case 4:
                return "Password";
            }
        }
    }
    return QVariant();
}

QVariant BusInterface_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return !Name.isEmpty() ? Name : "BusInterface";
    case Qt::DecorationRole:
        return QIcon::fromTheme("BusInterface");
    }
    return QVariant();
}

BusInterface_t * make_BusInterface_t(Base * parent)
{
    return new BusInterface_t(parent);
}

} // namespace knx
} // namespace v13
} // namespace Project
