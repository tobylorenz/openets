/* This file is generated. */

#pragma once

#include <Project/v13/knx/Capability_t.h>
#include <Project/xs/List.h>

namespace Project {
namespace v13 {
namespace knx {

using Capabilities_t = xs::List<Capability_t>;

} // namespace knx
} // namespace v13
} // namespace Project
