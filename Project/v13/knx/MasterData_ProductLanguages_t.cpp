/* This file is generated. */

#include <Project/v13/knx/MasterData_ProductLanguages_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v13/knx/KNX_t.h>

namespace Project {
namespace v13 {
namespace knx {

MasterData_ProductLanguages_t::MasterData_ProductLanguages_t(Base * parent) :
    Base(parent)
{
}

MasterData_ProductLanguages_t::~MasterData_ProductLanguages_t()
{
}

void MasterData_ProductLanguages_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("Language")) {
            auto * newLanguage = make_MasterData_ProductLanguages_Language_t(this);
            newLanguage->read(reader);
            Language.append(newLanguage);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int MasterData_ProductLanguages_t::tableColumnCount() const
{
    return 1;
}

QVariant MasterData_ProductLanguages_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "MasterData_ProductLanguages";
        }
        break;
    }
    return QVariant();
}

QVariant MasterData_ProductLanguages_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant MasterData_ProductLanguages_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "ProductLanguages";
    case Qt::DecorationRole:
        return QIcon::fromTheme("MasterData_ProductLanguages");
    }
    return QVariant();
}

MasterData_ProductLanguages_t * make_MasterData_ProductLanguages_t(Base * parent)
{
    return new MasterData_ProductLanguages_t(parent);
}

} // namespace knx
} // namespace v13
} // namespace Project
