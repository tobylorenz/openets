/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/xs/UnsignedShort.h>

namespace Project {
namespace v13 {
namespace knx {

class Topology_Area_Line_AdditionalGroupAddresses_GroupAddress_t : public Base
{
    Q_OBJECT

public:
    explicit Topology_Area_Line_AdditionalGroupAddresses_GroupAddress_t(Base * parent = nullptr);
    virtual ~Topology_Area_Line_AdditionalGroupAddresses_GroupAddress_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::UnsignedShort Address{};
};

Topology_Area_Line_AdditionalGroupAddresses_GroupAddress_t * make_Topology_Area_Line_AdditionalGroupAddresses_GroupAddress_t(Base * parent);

} // namespace knx
} // namespace v13
} // namespace Project
