/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v13/knx/CompletionStatus_t.h>
#include <Project/v13/knx/GroupAddressStyle_t.h>
#include <Project/v13/knx/ProjectTracingLevel_t.h>
#include <Project/v13/knx/Project_ProjectInformation_ProjectId_t.h>
#include <Project/v13/knx/String20_t.h>
#include <Project/v13/knx/String50_t.h>
#include <Project/v13/knx/TextEncoding_t.h>
#include <Project/xs/Boolean.h>
#include <Project/xs/DateTime.h>
#include <Project/xs/Int.h>
#include <Project/xs/String.h>
#include <Project/xs/UnsignedShort.h>

namespace Project {
namespace v13 {
namespace knx {

/* forward declarations */
class Project_ProjectInformation_AddinData_t;
class Project_ProjectInformation_DeviceCertificates_t;
class Project_ProjectInformation_HistoryEntries_t;
class Project_ProjectInformation_ProjectTraces_t;
class Project_ProjectInformation_ToDoItems_t;

class Project_ProjectInformation_t : public Base
{
    Q_OBJECT

public:
    explicit Project_ProjectInformation_t(Base * parent = nullptr);
    virtual ~Project_ProjectInformation_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    String50_t Name{};
    GroupAddressStyle_t GroupAddressStyle{};
    String50_t ProjectNumber{};
    String50_t ContractNumber{};
    xs::DateTime LastModified{};
    xs::DateTime ProjectStart{};
    xs::DateTime ProjectEnd{};
    Project_ProjectInformation_ProjectId_t ProjectId{};
    String20_t ProjectPassword{};
    xs::String Comment{};
    CompletionStatus_t CompletionStatus{"Undefined"};
    ProjectTracingLevel_t ProjectTracingLevel{"None"};
    String20_t ProjectTracingPassword{};
    xs::Boolean Hide16BitGroupsFromLegacyPlugins{"false"};
    TextEncoding_t CodePage{};
    xs::Boolean BusAccessLegacyMode{"false"};
    xs::String Guid{};
    xs::Int LastUsedPuid{};
    xs::UnsignedShort DeviceCount{};

    /* elements */
    Project_ProjectInformation_HistoryEntries_t * HistoryEntries{};
    Project_ProjectInformation_ToDoItems_t * ToDoItems{};
    Project_ProjectInformation_ProjectTraces_t * ProjectTraces{};
    Project_ProjectInformation_DeviceCertificates_t * DeviceCertificates{};
    Project_ProjectInformation_AddinData_t * AddinData{};
};

Project_ProjectInformation_t * make_Project_ProjectInformation_t(Base * parent);

} // namespace knx
} // namespace v13
} // namespace Project
