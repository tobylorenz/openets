/* This file is generated. */

#pragma once

#include <Project/v13/knx/LdCtrlProcType_t.h>

namespace Project {
namespace v13 {
namespace knx {

using HawkConfigurationData_Procedures_Procedure_ProcedureSubType_t = LdCtrlProcType_t;

} // namespace knx
} // namespace v13
} // namespace Project
