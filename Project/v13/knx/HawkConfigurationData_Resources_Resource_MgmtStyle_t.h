/* This file is generated. */

#pragma once

#include <Project/v13/knx/ResourceMgmtStyle_t.h>
#include <Project/xs/List.h>

namespace Project {
namespace v13 {
namespace knx {

using HawkConfigurationData_Resources_Resource_MgmtStyle_t = xs::List<ResourceMgmtStyle_t>;

} // namespace knx
} // namespace v13
} // namespace Project
