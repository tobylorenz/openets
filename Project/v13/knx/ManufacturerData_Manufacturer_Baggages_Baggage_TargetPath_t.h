/* This file is generated. */

#pragma once

#include <Project/v13/knx/String255_t.h>

namespace Project {
namespace v13 {
namespace knx {

using ManufacturerData_Manufacturer_Baggages_Baggage_TargetPath_t = String255_t;

} // namespace knx
} // namespace v13
} // namespace Project
