/* This file is generated. */

#include <Project/v13/knx/LoadProcedure_LdCtrlDelay_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v13/knx/KNX_t.h>

namespace Project {
namespace v13 {
namespace knx {

LoadProcedure_LdCtrlDelay_t::LoadProcedure_LdCtrlDelay_t(Base * parent) :
    Base(parent)
{
}

LoadProcedure_LdCtrlDelay_t::~LoadProcedure_LdCtrlDelay_t()
{
}

void LoadProcedure_LdCtrlDelay_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("MilliSeconds")) {
            MilliSeconds = attribute.value().toString();
            continue;
        }
        if (name == QString("AppliesTo")) {
            AppliesTo = attribute.value().toString();
            continue;
        }
        if (name == QString("InternalDescription")) {
            InternalDescription = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int LoadProcedure_LdCtrlDelay_t::tableColumnCount() const
{
    return 4;
}

QVariant LoadProcedure_LdCtrlDelay_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "LoadProcedure_LdCtrlDelay";
        }
        if (qualifiedName == QString("MilliSeconds")) {
            return MilliSeconds;
        }
        if (qualifiedName == QString("AppliesTo")) {
            return AppliesTo;
        }
        if (qualifiedName == QString("InternalDescription")) {
            return InternalDescription;
        }
        break;
    }
    return QVariant();
}

QVariant LoadProcedure_LdCtrlDelay_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "MilliSeconds";
            case 2:
                return "AppliesTo";
            case 3:
                return "InternalDescription";
            }
        }
    }
    return QVariant();
}

QVariant LoadProcedure_LdCtrlDelay_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "LdCtrlDelay";
    case Qt::DecorationRole:
        return QIcon::fromTheme("LoadProcedure_LdCtrlDelay");
    }
    return QVariant();
}

LoadProcedure_LdCtrlDelay_t * make_LoadProcedure_LdCtrlDelay_t(Base * parent)
{
    return new LoadProcedure_LdCtrlDelay_t(parent);
}

} // namespace knx
} // namespace v13
} // namespace Project
