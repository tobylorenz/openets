/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v13/knx/HawkConfigurationData_Resources_Resource_Access_t.h>
#include <Project/v13/knx/HawkConfigurationData_Resources_Resource_MgmtStyle_t.h>
#include <Project/v13/knx/ResourceName_t.h>

namespace Project {
namespace v13 {
namespace knx {

/* forward declarations */
class HawkConfigurationData_Resources_Resource_AccessRights_t;
class HawkConfigurationData_Resources_Resource_ResourceType_t;
class ResourceLocation_t;

class HawkConfigurationData_Resources_Resource_t : public Base
{
    Q_OBJECT

public:
    explicit HawkConfigurationData_Resources_Resource_t(Base * parent = nullptr);
    virtual ~HawkConfigurationData_Resources_Resource_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    ResourceName_t Name{};
    HawkConfigurationData_Resources_Resource_Access_t Access{};
    HawkConfigurationData_Resources_Resource_MgmtStyle_t MgmtStyle{};

    /* elements */
    ResourceLocation_t * Location{};
    ResourceLocation_t * ImgLocation{};
    HawkConfigurationData_Resources_Resource_ResourceType_t * ResourceType{};
    HawkConfigurationData_Resources_Resource_AccessRights_t * AccessRights{};
};

HawkConfigurationData_Resources_Resource_t * make_HawkConfigurationData_Resources_Resource_t(Base * parent);

} // namespace knx
} // namespace v13
} // namespace Project
