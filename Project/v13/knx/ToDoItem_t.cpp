/* This file is generated. */

#include <Project/v13/knx/ToDoItem_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v13/knx/KNX_t.h>

namespace Project {
namespace v13 {
namespace knx {

ToDoItem_t::ToDoItem_t(Base * parent) :
    Base(parent)
{
}

ToDoItem_t::~ToDoItem_t()
{
}

void ToDoItem_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("Description")) {
            Description = attribute.value().toString();
            continue;
        }
        if (name == QString("ObjectPath")) {
            ObjectPath = attribute.value().toString();
            continue;
        }
        if (name == QString("Status")) {
            Status = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ToDoItem_t::tableColumnCount() const
{
    return 4;
}

QVariant ToDoItem_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ToDoItem";
        }
        if (qualifiedName == QString("Description")) {
            return Description;
        }
        if (qualifiedName == QString("ObjectPath")) {
            return ObjectPath;
        }
        if (qualifiedName == QString("Status")) {
            return Status;
        }
        break;
    }
    return QVariant();
}

QVariant ToDoItem_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Description";
            case 2:
                return "ObjectPath";
            case 3:
                return "Status";
            }
        }
    }
    return QVariant();
}

QVariant ToDoItem_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "ToDoItem";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ToDoItem");
    }
    return QVariant();
}

ToDoItem_t * make_ToDoItem_t(Base * parent)
{
    return new ToDoItem_t(parent);
}

} // namespace knx
} // namespace v13
} // namespace Project
