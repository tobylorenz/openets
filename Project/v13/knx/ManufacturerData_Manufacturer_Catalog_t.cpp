/* This file is generated. */

#include <Project/v13/knx/ManufacturerData_Manufacturer_Catalog_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v13/knx/KNX_t.h>

namespace Project {
namespace v13 {
namespace knx {

ManufacturerData_Manufacturer_Catalog_t::ManufacturerData_Manufacturer_Catalog_t(Base * parent) :
    Base(parent)
{
}

ManufacturerData_Manufacturer_Catalog_t::~ManufacturerData_Manufacturer_Catalog_t()
{
}

void ManufacturerData_Manufacturer_Catalog_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("CatalogSection")) {
            QString newId = reader.attributes().value("Id").toString();
            Q_ASSERT(!newId.isEmpty());
            CatalogSection_t * newCatalogSection;
            if (CatalogSection.contains(newId)) {
                newCatalogSection = CatalogSection[newId];
            } else {
                newCatalogSection = make_CatalogSection_t(this);
                CatalogSection[newId] = newCatalogSection;
            }
            newCatalogSection->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ManufacturerData_Manufacturer_Catalog_t::tableColumnCount() const
{
    return 1;
}

QVariant ManufacturerData_Manufacturer_Catalog_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ManufacturerData_Manufacturer_Catalog";
        }
        break;
    }
    return QVariant();
}

QVariant ManufacturerData_Manufacturer_Catalog_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant ManufacturerData_Manufacturer_Catalog_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "Catalog";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ManufacturerData_Manufacturer_Catalog");
    }
    return QVariant();
}

ManufacturerData_Manufacturer_Catalog_t * make_ManufacturerData_Manufacturer_Catalog_t(Base * parent)
{
    return new ManufacturerData_Manufacturer_Catalog_t(parent);
}

} // namespace knx
} // namespace v13
} // namespace Project
