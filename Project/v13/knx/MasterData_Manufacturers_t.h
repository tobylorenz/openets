/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v13/knx/MasterData_Manufacturers_Manufacturer_t.h>
#include <Project/xs/ID.h>

namespace Project {
namespace v13 {
namespace knx {

class MasterData_Manufacturers_t : public Base
{
    Q_OBJECT

public:
    explicit MasterData_Manufacturers_t(Base * parent = nullptr);
    virtual ~MasterData_Manufacturers_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* elements */
    QMap<xs::ID, MasterData_Manufacturers_Manufacturer_t *> Manufacturer; // key: Id
};

MasterData_Manufacturers_t * make_MasterData_Manufacturers_t(Base * parent);

} // namespace knx
} // namespace v13
} // namespace Project
