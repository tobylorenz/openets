/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/xs/ID.h>
#include <Project/xs/Base64Binary.h>
#include <Project/xs/UnsignedInt.h>

namespace Project {
namespace v13 {
namespace knx {

/* forward declarations */
class MasterData_DatapointTypes_t;
class MasterData_FunctionalBlocks_t;
class MasterData_InterfaceObjectProperties_t;
class MasterData_InterfaceObjectTypes_t;
class MasterData_Languages_t;
class MasterData_Manufacturers_t;
class MasterData_MaskVersions_t;
class MasterData_MediumTypes_t;
class MasterData_ProductLanguages_t;
class MasterData_PropertyDataTypes_t;

class MasterData_t : public Base
{
    Q_OBJECT

public:
    explicit MasterData_t(Base * parent = nullptr);
    virtual ~MasterData_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::UnsignedInt Version{};
    xs::Base64Binary Signature{};
    xs::ID Id{};

    /* elements */
    MasterData_DatapointTypes_t * DatapointTypes{};
    MasterData_InterfaceObjectTypes_t * InterfaceObjectTypes{};
    MasterData_InterfaceObjectProperties_t * InterfaceObjectProperties{};
    MasterData_PropertyDataTypes_t * PropertyDataTypes{};
    MasterData_MediumTypes_t * MediumTypes{};
    MasterData_MaskVersions_t * MaskVersions{};
    MasterData_FunctionalBlocks_t * FunctionalBlocks{};
    MasterData_ProductLanguages_t * ProductLanguages{};
    MasterData_Manufacturers_t * Manufacturers{};
    MasterData_Languages_t * Languages{};
};

MasterData_t * make_MasterData_t(Base * parent);

} // namespace knx
} // namespace v13
} // namespace Project
