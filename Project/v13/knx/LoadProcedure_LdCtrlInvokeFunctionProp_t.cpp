/* This file is generated. */

#include <Project/v13/knx/LoadProcedure_LdCtrlInvokeFunctionProp_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v13/knx/KNX_t.h>

namespace Project {
namespace v13 {
namespace knx {

LoadProcedure_LdCtrlInvokeFunctionProp_t::LoadProcedure_LdCtrlInvokeFunctionProp_t(Base * parent) :
    Base(parent)
{
}

LoadProcedure_LdCtrlInvokeFunctionProp_t::~LoadProcedure_LdCtrlInvokeFunctionProp_t()
{
}

void LoadProcedure_LdCtrlInvokeFunctionProp_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("ObjIdx")) {
            ObjIdx = attribute.value().toString();
            continue;
        }
        if (name == QString("ObjType")) {
            ObjType = attribute.value().toString();
            continue;
        }
        if (name == QString("Occurrence")) {
            Occurrence = attribute.value().toString();
            continue;
        }
        if (name == QString("PropId")) {
            PropId = attribute.value().toString();
            continue;
        }
        if (name == QString("InlineData")) {
            InlineData = attribute.value().toString();
            continue;
        }
        if (name == QString("AppliesTo")) {
            AppliesTo = attribute.value().toString();
            continue;
        }
        if (name == QString("InternalDescription")) {
            InternalDescription = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int LoadProcedure_LdCtrlInvokeFunctionProp_t::tableColumnCount() const
{
    return 8;
}

QVariant LoadProcedure_LdCtrlInvokeFunctionProp_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "LoadProcedure_LdCtrlInvokeFunctionProp";
        }
        if (qualifiedName == QString("ObjIdx")) {
            return ObjIdx;
        }
        if (qualifiedName == QString("ObjType")) {
            return ObjType;
        }
        if (qualifiedName == QString("Occurrence")) {
            return Occurrence;
        }
        if (qualifiedName == QString("PropId")) {
            return PropId;
        }
        if (qualifiedName == QString("InlineData")) {
            return InlineData;
        }
        if (qualifiedName == QString("AppliesTo")) {
            return AppliesTo;
        }
        if (qualifiedName == QString("InternalDescription")) {
            return InternalDescription;
        }
        break;
    }
    return QVariant();
}

QVariant LoadProcedure_LdCtrlInvokeFunctionProp_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "ObjIdx";
            case 2:
                return "ObjType";
            case 3:
                return "Occurrence";
            case 4:
                return "PropId";
            case 5:
                return "InlineData";
            case 6:
                return "AppliesTo";
            case 7:
                return "InternalDescription";
            }
        }
    }
    return QVariant();
}

QVariant LoadProcedure_LdCtrlInvokeFunctionProp_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "LdCtrlInvokeFunctionProp";
    case Qt::DecorationRole:
        return QIcon::fromTheme("LoadProcedure_LdCtrlInvokeFunctionProp");
    }
    return QVariant();
}

LoadProcedure_LdCtrlInvokeFunctionProp_t * make_LoadProcedure_LdCtrlInvokeFunctionProp_t(Base * parent)
{
    return new LoadProcedure_LdCtrlInvokeFunctionProp_t(parent);
}

} // namespace knx
} // namespace v13
} // namespace Project
