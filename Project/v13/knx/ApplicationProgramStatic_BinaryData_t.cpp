/* This file is generated. */

#include <Project/v13/knx/ApplicationProgramStatic_BinaryData_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v13/knx/KNX_t.h>

namespace Project {
namespace v13 {
namespace knx {

ApplicationProgramStatic_BinaryData_t::ApplicationProgramStatic_BinaryData_t(Base * parent) :
    Base(parent)
{
}

ApplicationProgramStatic_BinaryData_t::~ApplicationProgramStatic_BinaryData_t()
{
}

void ApplicationProgramStatic_BinaryData_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("BinaryData")) {
            QString newId = reader.attributes().value("Id").toString();
            Q_ASSERT(!newId.isEmpty());
            BinaryData_t * newBinaryData;
            if (BinaryData.contains(newId)) {
                newBinaryData = BinaryData[newId];
            } else {
                newBinaryData = make_BinaryData_t(this);
                BinaryData[newId] = newBinaryData;
            }
            newBinaryData->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ApplicationProgramStatic_BinaryData_t::tableColumnCount() const
{
    return 1;
}

QVariant ApplicationProgramStatic_BinaryData_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ApplicationProgramStatic_BinaryData";
        }
        break;
    }
    return QVariant();
}

QVariant ApplicationProgramStatic_BinaryData_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant ApplicationProgramStatic_BinaryData_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "BinaryData";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ApplicationProgramStatic_BinaryData");
    }
    return QVariant();
}

ApplicationProgramStatic_BinaryData_t * make_ApplicationProgramStatic_BinaryData_t(Base * parent)
{
    return new ApplicationProgramStatic_BinaryData_t(parent);
}

} // namespace knx
} // namespace v13
} // namespace Project
