/* This file is generated. */

#include <Project/v13/knx/ParameterType_TypeTime_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v13/knx/KNX_t.h>

namespace Project {
namespace v13 {
namespace knx {

ParameterType_TypeTime_t::ParameterType_TypeTime_t(Base * parent) :
    Base(parent)
{
}

ParameterType_TypeTime_t::~ParameterType_TypeTime_t()
{
}

void ParameterType_TypeTime_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("SizeInBit")) {
            SizeInBit = attribute.value().toString();
            continue;
        }
        if (name == QString("Unit")) {
            Unit = attribute.value().toString();
            continue;
        }
        if (name == QString("minInclusive")) {
            minInclusive = attribute.value().toString();
            continue;
        }
        if (name == QString("maxInclusive")) {
            maxInclusive = attribute.value().toString();
            continue;
        }
        if (name == QString("UIHint")) {
            UIHint = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ParameterType_TypeTime_t::tableColumnCount() const
{
    return 6;
}

QVariant ParameterType_TypeTime_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ParameterType_TypeTime";
        }
        if (qualifiedName == QString("SizeInBit")) {
            return SizeInBit;
        }
        if (qualifiedName == QString("Unit")) {
            return Unit;
        }
        if (qualifiedName == QString("minInclusive")) {
            return minInclusive;
        }
        if (qualifiedName == QString("maxInclusive")) {
            return maxInclusive;
        }
        if (qualifiedName == QString("UIHint")) {
            return UIHint;
        }
        break;
    }
    return QVariant();
}

QVariant ParameterType_TypeTime_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "SizeInBit";
            case 2:
                return "Unit";
            case 3:
                return "minInclusive";
            case 4:
                return "maxInclusive";
            case 5:
                return "UIHint";
            }
        }
    }
    return QVariant();
}

QVariant ParameterType_TypeTime_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "TypeTime";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ParameterType_TypeTime");
    }
    return QVariant();
}

ParameterType_TypeTime_t * make_ParameterType_TypeTime_t(Base * parent)
{
    return new ParameterType_TypeTime_t(parent);
}

} // namespace knx
} // namespace v13
} // namespace Project
