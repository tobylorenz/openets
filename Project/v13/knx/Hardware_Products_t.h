/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v13/knx/Hardware_Products_Product_t.h>
#include <Project/xs/ID.h>

namespace Project {
namespace v13 {
namespace knx {

class Hardware_Products_t : public Base
{
    Q_OBJECT

public:
    explicit Hardware_Products_t(Base * parent = nullptr);
    virtual ~Hardware_Products_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* elements */
    QMap<xs::ID, Hardware_Products_Product_t *> Product; // key: Id
};

Hardware_Products_t * make_Hardware_Products_t(Base * parent);

} // namespace knx
} // namespace v13
} // namespace Project
