/* This file is generated. */

#include <Project/v13/knx/HawkConfigurationData_Procedures_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v13/knx/KNX_t.h>

namespace Project {
namespace v13 {
namespace knx {

HawkConfigurationData_Procedures_t::HawkConfigurationData_Procedures_t(Base * parent) :
    Base(parent)
{
}

HawkConfigurationData_Procedures_t::~HawkConfigurationData_Procedures_t()
{
}

void HawkConfigurationData_Procedures_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("Procedure")) {
            auto * newProcedure = make_HawkConfigurationData_Procedures_Procedure_t(this);
            newProcedure->read(reader);
            Procedure.append(newProcedure);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int HawkConfigurationData_Procedures_t::tableColumnCount() const
{
    return 1;
}

QVariant HawkConfigurationData_Procedures_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "HawkConfigurationData_Procedures";
        }
        break;
    }
    return QVariant();
}

QVariant HawkConfigurationData_Procedures_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant HawkConfigurationData_Procedures_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "Procedures";
    case Qt::DecorationRole:
        return QIcon::fromTheme("HawkConfigurationData_Procedures");
    }
    return QVariant();
}

HawkConfigurationData_Procedures_t * make_HawkConfigurationData_Procedures_t(Base * parent)
{
    return new HawkConfigurationData_Procedures_t(parent);
}

} // namespace knx
} // namespace v13
} // namespace Project
