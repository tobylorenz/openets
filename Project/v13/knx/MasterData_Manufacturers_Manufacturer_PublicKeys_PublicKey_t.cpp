/* This file is generated. */

#include <Project/v13/knx/MasterData_Manufacturers_Manufacturer_PublicKeys_PublicKey_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v13/knx/KNX_t.h>
#include <Project/v13/knx/MasterData_Manufacturers_Manufacturer_PublicKeys_PublicKey_RSAKeyValue_t.h>

namespace Project {
namespace v13 {
namespace knx {

MasterData_Manufacturers_Manufacturer_PublicKeys_PublicKey_t::MasterData_Manufacturers_Manufacturer_PublicKeys_PublicKey_t(Base * parent) :
    Base(parent)
{
}

MasterData_Manufacturers_Manufacturer_PublicKeys_PublicKey_t::~MasterData_Manufacturers_Manufacturer_PublicKeys_PublicKey_t()
{
}

void MasterData_Manufacturers_Manufacturer_PublicKeys_PublicKey_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == "Id") {
            Id = attribute.value().toString();
            KNX_t * knx = findParent<KNX_t *>();
            Q_ASSERT(knx);
            knx->ids[Id] = this;
            continue;
        }
        if (name == QString("Number")) {
            Number = attribute.value().toString();
            continue;
        }
        if (name == QString("Revoked")) {
            Revoked = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("RSAKeyValue")) {
            if (!RSAKeyValue) {
                RSAKeyValue = make_MasterData_Manufacturers_Manufacturer_PublicKeys_PublicKey_RSAKeyValue_t(this);
            }
            RSAKeyValue->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int MasterData_Manufacturers_Manufacturer_PublicKeys_PublicKey_t::tableColumnCount() const
{
    return 4;
}

QVariant MasterData_Manufacturers_Manufacturer_PublicKeys_PublicKey_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "MasterData_Manufacturers_Manufacturer_PublicKeys_PublicKey";
        }
        if (qualifiedName == QString("Id")) {
            return Id;
        }
        if (qualifiedName == QString("Number")) {
            return Number;
        }
        if (qualifiedName == QString("Revoked")) {
            return Revoked;
        }
        break;
    }
    return QVariant();
}

QVariant MasterData_Manufacturers_Manufacturer_PublicKeys_PublicKey_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Id";
            case 2:
                return "Number";
            case 3:
                return "Revoked";
            }
        }
    }
    return QVariant();
}

QVariant MasterData_Manufacturers_Manufacturer_PublicKeys_PublicKey_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "PublicKey";
    case Qt::DecorationRole:
        return QIcon::fromTheme("MasterData_Manufacturers_Manufacturer_PublicKeys_PublicKey");
    }
    return QVariant();
}

MasterData_Manufacturers_Manufacturer_PublicKeys_PublicKey_t * make_MasterData_Manufacturers_Manufacturer_PublicKeys_PublicKey_t(Base * parent)
{
    return new MasterData_Manufacturers_Manufacturer_PublicKeys_PublicKey_t(parent);
}

} // namespace knx
} // namespace v13
} // namespace Project
