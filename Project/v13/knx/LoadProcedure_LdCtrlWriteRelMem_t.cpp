/* This file is generated. */

#include <Project/v13/knx/LoadProcedure_LdCtrlWriteRelMem_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v13/knx/KNX_t.h>

namespace Project {
namespace v13 {
namespace knx {

LoadProcedure_LdCtrlWriteRelMem_t::LoadProcedure_LdCtrlWriteRelMem_t(Base * parent) :
    Base(parent)
{
}

LoadProcedure_LdCtrlWriteRelMem_t::~LoadProcedure_LdCtrlWriteRelMem_t()
{
}

void LoadProcedure_LdCtrlWriteRelMem_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("ObjIdx")) {
            ObjIdx = attribute.value().toString();
            continue;
        }
        if (name == QString("ObjType")) {
            ObjType = attribute.value().toString();
            continue;
        }
        if (name == QString("Occurrence")) {
            Occurrence = attribute.value().toString();
            continue;
        }
        if (name == QString("Offset")) {
            Offset = attribute.value().toString();
            continue;
        }
        if (name == QString("Size")) {
            Size = attribute.value().toString();
            continue;
        }
        if (name == QString("Verify")) {
            Verify = attribute.value().toString();
            continue;
        }
        if (name == QString("InlineData")) {
            InlineData = attribute.value().toString();
            continue;
        }
        if (name == QString("AppliesTo")) {
            AppliesTo = attribute.value().toString();
            continue;
        }
        if (name == QString("InternalDescription")) {
            InternalDescription = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int LoadProcedure_LdCtrlWriteRelMem_t::tableColumnCount() const
{
    return 10;
}

QVariant LoadProcedure_LdCtrlWriteRelMem_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "LoadProcedure_LdCtrlWriteRelMem";
        }
        if (qualifiedName == QString("ObjIdx")) {
            return ObjIdx;
        }
        if (qualifiedName == QString("ObjType")) {
            return ObjType;
        }
        if (qualifiedName == QString("Occurrence")) {
            return Occurrence;
        }
        if (qualifiedName == QString("Offset")) {
            return Offset;
        }
        if (qualifiedName == QString("Size")) {
            return Size;
        }
        if (qualifiedName == QString("Verify")) {
            return Verify;
        }
        if (qualifiedName == QString("InlineData")) {
            return InlineData;
        }
        if (qualifiedName == QString("AppliesTo")) {
            return AppliesTo;
        }
        if (qualifiedName == QString("InternalDescription")) {
            return InternalDescription;
        }
        break;
    }
    return QVariant();
}

QVariant LoadProcedure_LdCtrlWriteRelMem_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "ObjIdx";
            case 2:
                return "ObjType";
            case 3:
                return "Occurrence";
            case 4:
                return "Offset";
            case 5:
                return "Size";
            case 6:
                return "Verify";
            case 7:
                return "InlineData";
            case 8:
                return "AppliesTo";
            case 9:
                return "InternalDescription";
            }
        }
    }
    return QVariant();
}

QVariant LoadProcedure_LdCtrlWriteRelMem_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "LdCtrlWriteRelMem";
    case Qt::DecorationRole:
        return QIcon::fromTheme("LoadProcedure_LdCtrlWriteRelMem");
    }
    return QVariant();
}

LoadProcedure_LdCtrlWriteRelMem_t * make_LoadProcedure_LdCtrlWriteRelMem_t(Base * parent)
{
    return new LoadProcedure_LdCtrlWriteRelMem_t(parent);
}

} // namespace knx
} // namespace v13
} // namespace Project
