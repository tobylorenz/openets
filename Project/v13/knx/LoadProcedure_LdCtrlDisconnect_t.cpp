/* This file is generated. */

#include <Project/v13/knx/LoadProcedure_LdCtrlDisconnect_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v13/knx/KNX_t.h>

namespace Project {
namespace v13 {
namespace knx {

LoadProcedure_LdCtrlDisconnect_t::LoadProcedure_LdCtrlDisconnect_t(Base * parent) :
    Base(parent)
{
}

LoadProcedure_LdCtrlDisconnect_t::~LoadProcedure_LdCtrlDisconnect_t()
{
}

void LoadProcedure_LdCtrlDisconnect_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("AppliesTo")) {
            AppliesTo = attribute.value().toString();
            continue;
        }
        if (name == QString("InternalDescription")) {
            InternalDescription = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int LoadProcedure_LdCtrlDisconnect_t::tableColumnCount() const
{
    return 3;
}

QVariant LoadProcedure_LdCtrlDisconnect_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "LoadProcedure_LdCtrlDisconnect";
        }
        if (qualifiedName == QString("AppliesTo")) {
            return AppliesTo;
        }
        if (qualifiedName == QString("InternalDescription")) {
            return InternalDescription;
        }
        break;
    }
    return QVariant();
}

QVariant LoadProcedure_LdCtrlDisconnect_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "AppliesTo";
            case 2:
                return "InternalDescription";
            }
        }
    }
    return QVariant();
}

QVariant LoadProcedure_LdCtrlDisconnect_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "LdCtrlDisconnect";
    case Qt::DecorationRole:
        return QIcon::fromTheme("LoadProcedure_LdCtrlDisconnect");
    }
    return QVariant();
}

LoadProcedure_LdCtrlDisconnect_t * make_LoadProcedure_LdCtrlDisconnect_t(Base * parent)
{
    return new LoadProcedure_LdCtrlDisconnect_t(parent);
}

} // namespace knx
} // namespace v13
} // namespace Project
