/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v13/knx/ManufacturerData_Manufacturer_Baggages_Baggage_FileInfo_Version_t.h>
#include <Project/xs/Boolean.h>
#include <Project/xs/DateTime.h>

namespace Project {
namespace v13 {
namespace knx {

class ManufacturerData_Manufacturer_Baggages_Baggage_FileInfo_t : public Base
{
    Q_OBJECT

public:
    explicit ManufacturerData_Manufacturer_Baggages_Baggage_FileInfo_t(Base * parent = nullptr);
    virtual ~ManufacturerData_Manufacturer_Baggages_Baggage_FileInfo_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    ManufacturerData_Manufacturer_Baggages_Baggage_FileInfo_Version_t Version{};
    xs::DateTime TimeInfo{};
    xs::Boolean Hidden{"false"};
    xs::Boolean ReadOnly{"false"};
};

ManufacturerData_Manufacturer_Baggages_Baggage_FileInfo_t * make_ManufacturerData_Manufacturer_Baggages_Baggage_FileInfo_t(Base * parent);

} // namespace knx
} // namespace v13
} // namespace Project
