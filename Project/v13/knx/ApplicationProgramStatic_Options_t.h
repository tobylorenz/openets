/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v13/knx/ApplicationProgramStatic_Options_ParameterByteOrder_t.h>
#include <Project/v13/knx/ApplicationProgramStatic_Options_TextParameterEncodingSelector_t.h>
#include <Project/v13/knx/DownloadBehavior_t.h>
#include <Project/v13/knx/TextEncoding_t.h>
#include <Project/xs/Boolean.h>

namespace Project {
namespace v13 {
namespace knx {

class ApplicationProgramStatic_Options_t : public Base
{
    Q_OBJECT

public:
    explicit ApplicationProgramStatic_Options_t(Base * parent = nullptr);
    virtual ~ApplicationProgramStatic_Options_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::Boolean PreferPartialDownloadIfApplicationLoaded{"false"};
    xs::Boolean EasyCtrlModeModeStyleEmptyGroupComTables{"false"};
    xs::Boolean SetObjectTableLengthAlwaysToOne{"false"};
    TextEncoding_t TextParameterEncoding{};
    ApplicationProgramStatic_Options_TextParameterEncodingSelector_t TextParameterEncodingSelector{"UseTextParameterEncodingCodePage"};
    xs::Boolean TextParameterZeroTerminate{"false"};
    ApplicationProgramStatic_Options_ParameterByteOrder_t ParameterByteOrder{"BigEndian"};
    xs::Boolean PartialDownloadOnlyVisibleParameters{"false"};
    xs::Boolean LegacyNoPartialDownload{"false"};
    xs::Boolean LegacyNoMemoryVerifyMode{"false"};
    xs::Boolean LegacyNoOptimisticWrite{"false"};
    xs::Boolean LegacyDoNotReportPropertyWriteErrors{"false"};
    xs::Boolean LegacyNoBackgroundDownload{"false"};
    xs::Boolean LegacyDoNotCheckManufacturerId{"false"};
    xs::Boolean LegacyAlwaysReloadAppIfCoVisibilityChanged{"false"};
    xs::Boolean LegacyNeverReloadAppIfCoVisibilityChanged{"false"};
    xs::Boolean LegacyDoNotSupportUndoDelete{"false"};
    xs::Boolean LegacyAllowPartialDownloadIfAp2Mismatch{"false"};
    xs::Boolean LegacyKeepObjectTableGaps{"false"};
    xs::Boolean LegacyProxyCommunicationObjects{"false"};
    xs::Boolean DeviceInfoIgnoreRunState{"false"};
    xs::Boolean DeviceInfoIgnoreLoadedState{"false"};
    xs::Boolean DeviceCompareAllowCompatibleManufacturerId{"false"};
    xs::Boolean LineCoupler0912NewProgrammingStyle{"false"};
    xs::Boolean Comparable{};
    xs::Boolean Reconstructable{};
    DownloadBehavior_t DownloadInvisibleParameters{"DefaultValue"};
    xs::Boolean SupportsExtendedMemoryServices{"false"};
    xs::Boolean SupportsExtendedPropertyServices{"false"};
    xs::Boolean SupportsIpSystemBroadcast{"false"};

    /* getters */
};

ApplicationProgramStatic_Options_t * make_ApplicationProgramStatic_Options_t(Base * parent);

} // namespace knx
} // namespace v13
} // namespace Project
