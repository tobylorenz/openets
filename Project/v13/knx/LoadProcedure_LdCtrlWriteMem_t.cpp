/* This file is generated. */

#include <Project/v13/knx/LoadProcedure_LdCtrlWriteMem_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v13/knx/KNX_t.h>

namespace Project {
namespace v13 {
namespace knx {

LoadProcedure_LdCtrlWriteMem_t::LoadProcedure_LdCtrlWriteMem_t(Base * parent) :
    Base(parent)
{
}

LoadProcedure_LdCtrlWriteMem_t::~LoadProcedure_LdCtrlWriteMem_t()
{
}

void LoadProcedure_LdCtrlWriteMem_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("AddressSpace")) {
            AddressSpace = attribute.value().toString();
            continue;
        }
        if (name == QString("Address")) {
            Address = attribute.value().toString();
            continue;
        }
        if (name == QString("Size")) {
            Size = attribute.value().toString();
            continue;
        }
        if (name == QString("Verify")) {
            Verify = attribute.value().toString();
            continue;
        }
        if (name == QString("InlineData")) {
            InlineData = attribute.value().toString();
            continue;
        }
        if (name == QString("AppliesTo")) {
            AppliesTo = attribute.value().toString();
            continue;
        }
        if (name == QString("InternalDescription")) {
            InternalDescription = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int LoadProcedure_LdCtrlWriteMem_t::tableColumnCount() const
{
    return 8;
}

QVariant LoadProcedure_LdCtrlWriteMem_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "LoadProcedure_LdCtrlWriteMem";
        }
        if (qualifiedName == QString("AddressSpace")) {
            return AddressSpace;
        }
        if (qualifiedName == QString("Address")) {
            return Address;
        }
        if (qualifiedName == QString("Size")) {
            return Size;
        }
        if (qualifiedName == QString("Verify")) {
            return Verify;
        }
        if (qualifiedName == QString("InlineData")) {
            return InlineData;
        }
        if (qualifiedName == QString("AppliesTo")) {
            return AppliesTo;
        }
        if (qualifiedName == QString("InternalDescription")) {
            return InternalDescription;
        }
        break;
    }
    return QVariant();
}

QVariant LoadProcedure_LdCtrlWriteMem_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "AddressSpace";
            case 2:
                return "Address";
            case 3:
                return "Size";
            case 4:
                return "Verify";
            case 5:
                return "InlineData";
            case 6:
                return "AppliesTo";
            case 7:
                return "InternalDescription";
            }
        }
    }
    return QVariant();
}

QVariant LoadProcedure_LdCtrlWriteMem_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "LdCtrlWriteMem";
    case Qt::DecorationRole:
        return QIcon::fromTheme("LoadProcedure_LdCtrlWriteMem");
    }
    return QVariant();
}

LoadProcedure_LdCtrlWriteMem_t * make_LoadProcedure_LdCtrlWriteMem_t(Base * parent)
{
    return new LoadProcedure_LdCtrlWriteMem_t(parent);
}

} // namespace knx
} // namespace v13
} // namespace Project
