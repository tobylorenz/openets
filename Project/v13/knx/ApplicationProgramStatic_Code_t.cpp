/* This file is generated. */

#include <Project/v13/knx/ApplicationProgramStatic_Code_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v13/knx/KNX_t.h>

namespace Project {
namespace v13 {
namespace knx {

ApplicationProgramStatic_Code_t::ApplicationProgramStatic_Code_t(Base * parent) :
    Base(parent)
{
}

ApplicationProgramStatic_Code_t::~ApplicationProgramStatic_Code_t()
{
}

void ApplicationProgramStatic_Code_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("AbsoluteSegment")) {
            QString newId = reader.attributes().value("Id").toString();
            Q_ASSERT(!newId.isEmpty());
            ApplicationProgramStatic_Code_AbsoluteSegment_t * newAbsoluteSegment;
            if (AbsoluteSegment.contains(newId)) {
                newAbsoluteSegment = AbsoluteSegment[newId];
            } else {
                newAbsoluteSegment = make_ApplicationProgramStatic_Code_AbsoluteSegment_t(this);
                AbsoluteSegment[newId] = newAbsoluteSegment;
            }
            newAbsoluteSegment->read(reader);
            continue;
        }
        if (reader.name() == QString("RelativeSegment")) {
            QString newId = reader.attributes().value("Id").toString();
            Q_ASSERT(!newId.isEmpty());
            ApplicationProgramStatic_Code_RelativeSegment_t * newRelativeSegment;
            if (RelativeSegment.contains(newId)) {
                newRelativeSegment = RelativeSegment[newId];
            } else {
                newRelativeSegment = make_ApplicationProgramStatic_Code_RelativeSegment_t(this);
                RelativeSegment[newId] = newRelativeSegment;
            }
            newRelativeSegment->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ApplicationProgramStatic_Code_t::tableColumnCount() const
{
    return 1;
}

QVariant ApplicationProgramStatic_Code_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ApplicationProgramStatic_Code";
        }
        break;
    }
    return QVariant();
}

QVariant ApplicationProgramStatic_Code_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant ApplicationProgramStatic_Code_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "Code";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ApplicationProgramStatic_Code");
    }
    return QVariant();
}

ApplicationProgramStatic_Code_t * make_ApplicationProgramStatic_Code_t(Base * parent)
{
    return new ApplicationProgramStatic_Code_t(parent);
}

} // namespace knx
} // namespace v13
} // namespace Project
