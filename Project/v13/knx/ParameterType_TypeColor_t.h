/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v13/knx/ParameterType_TypeColor_Space_t.h>

namespace Project {
namespace v13 {
namespace knx {

class ParameterType_TypeColor_t : public Base
{
    Q_OBJECT

public:
    explicit ParameterType_TypeColor_t(Base * parent = nullptr);
    virtual ~ParameterType_TypeColor_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    ParameterType_TypeColor_Space_t Space{};
};

ParameterType_TypeColor_t * make_ParameterType_TypeColor_t(Base * parent);

} // namespace knx
} // namespace v13
} // namespace Project
