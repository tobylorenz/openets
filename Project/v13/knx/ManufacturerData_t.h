/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v13/knx/IDREF.h>
#include <Project/v13/knx/ManufacturerData_Manufacturer_t.h>

namespace Project {
namespace v13 {
namespace knx {

class ManufacturerData_t : public Base
{
    Q_OBJECT

public:
    explicit ManufacturerData_t(Base * parent = nullptr);
    virtual ~ManufacturerData_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* elements */
    QMap<IDREF, ManufacturerData_Manufacturer_t *> Manufacturer; // key: RefId
};

ManufacturerData_t * make_ManufacturerData_t(Base * parent);

} // namespace knx
} // namespace v13
} // namespace Project
