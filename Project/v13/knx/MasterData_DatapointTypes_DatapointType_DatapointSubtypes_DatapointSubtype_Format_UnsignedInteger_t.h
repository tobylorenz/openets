/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v13/knx/LanguageDependentString_t.h>
#include <Project/xs/ID.h>
#include <Project/xs/Float.h>
#include <Project/xs/UnsignedInt.h>
#include <Project/xs/UnsignedLong.h>

namespace Project {
namespace v13 {
namespace knx {

class MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_UnsignedInteger_t : public Base
{
    Q_OBJECT

public:
    explicit MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_UnsignedInteger_t(Base * parent = nullptr);
    virtual ~MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_UnsignedInteger_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::ID Id{};
    xs::UnsignedInt Width{};
    LanguageDependentString_t Name{};
    LanguageDependentString_t Unit{};
    xs::UnsignedLong MinInclusive{};
    xs::UnsignedLong MaxInclusive{};
    xs::Float Coefficient{};
};

MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_UnsignedInteger_t * make_MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_UnsignedInteger_t(Base * parent);

} // namespace knx
} // namespace v13
} // namespace Project
