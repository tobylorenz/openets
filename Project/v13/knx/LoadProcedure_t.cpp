/* This file is generated. */

#include <Project/v13/knx/LoadProcedure_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v13/knx/KNX_t.h>
#include <Project/v13/knx/LoadProcedure_LdCtrlAbsSegment_t.h>
#include <Project/v13/knx/LoadProcedure_LdCtrlClearCachedObjectTypes_t.h>
#include <Project/v13/knx/LoadProcedure_LdCtrlClearLCFilterTable_t.h>
#include <Project/v13/knx/LoadProcedure_LdCtrlCompareMem_t.h>
#include <Project/v13/knx/LoadProcedure_LdCtrlCompareProp_t.h>
#include <Project/v13/knx/LoadProcedure_LdCtrlCompareRelMem_t.h>
#include <Project/v13/knx/LoadProcedure_LdCtrlConnect_t.h>
#include <Project/v13/knx/LoadProcedure_LdCtrlDeclarePropDesc_t.h>
#include <Project/v13/knx/LoadProcedure_LdCtrlDelay_t.h>
#include <Project/v13/knx/LoadProcedure_LdCtrlDisconnect_t.h>
#include <Project/v13/knx/LoadProcedure_LdCtrlInvokeFunctionProp_t.h>
#include <Project/v13/knx/LoadProcedure_LdCtrlLoadCompleted_t.h>
#include <Project/v13/knx/LoadProcedure_LdCtrlLoadImageMem_t.h>
#include <Project/v13/knx/LoadProcedure_LdCtrlLoadImageProp_t.h>
#include <Project/v13/knx/LoadProcedure_LdCtrlLoadImageRelMem_t.h>
#include <Project/v13/knx/LoadProcedure_LdCtrlLoad_t.h>
#include <Project/v13/knx/LoadProcedure_LdCtrlMapError_t.h>
#include <Project/v13/knx/LoadProcedure_LdCtrlMasterReset_t.h>
#include <Project/v13/knx/LoadProcedure_LdCtrlMaxLength_t.h>
#include <Project/v13/knx/LoadProcedure_LdCtrlMerge_t.h>
#include <Project/v13/knx/LoadProcedure_LdCtrlProgressText_t.h>
#include <Project/v13/knx/LoadProcedure_LdCtrlReadFunctionProp_t.h>
#include <Project/v13/knx/LoadProcedure_LdCtrlRelSegment_t.h>
#include <Project/v13/knx/LoadProcedure_LdCtrlRestart_t.h>
#include <Project/v13/knx/LoadProcedure_LdCtrlSetControlVariable_t.h>
#include <Project/v13/knx/LoadProcedure_LdCtrlTaskCtrl1_t.h>
#include <Project/v13/knx/LoadProcedure_LdCtrlTaskCtrl2_t.h>
#include <Project/v13/knx/LoadProcedure_LdCtrlTaskPtr_t.h>
#include <Project/v13/knx/LoadProcedure_LdCtrlTaskSegment_t.h>
#include <Project/v13/knx/LoadProcedure_LdCtrlUnload_t.h>
#include <Project/v13/knx/LoadProcedure_LdCtrlWriteMem_t.h>
#include <Project/v13/knx/LoadProcedure_LdCtrlWriteProp_t.h>
#include <Project/v13/knx/LoadProcedure_LdCtrlWriteRelMem_t.h>

namespace Project {
namespace v13 {
namespace knx {

LoadProcedure_t::LoadProcedure_t(Base * parent) :
    Base(parent)
{
}

LoadProcedure_t::~LoadProcedure_t()
{
}

void LoadProcedure_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("LdCtrlUnload")) {
            auto * LdCtrlUnload = make_LoadProcedure_LdCtrlUnload_t(this);
            LdCtrlUnload->read(reader);
            continue;
        }
        if (reader.name() == QString("LdCtrlLoad")) {
            auto * LdCtrlLoad = make_LoadProcedure_LdCtrlLoad_t(this);
            LdCtrlLoad->read(reader);
            continue;
        }
        if (reader.name() == QString("LdCtrlMaxLength")) {
            auto * LdCtrlMaxLength = make_LoadProcedure_LdCtrlMaxLength_t(this);
            LdCtrlMaxLength->read(reader);
            continue;
        }
        if (reader.name() == QString("LdCtrlClearCachedObjectTypes")) {
            auto * LdCtrlClearCachedObjectTypes = make_LoadProcedure_LdCtrlClearCachedObjectTypes_t(this);
            LdCtrlClearCachedObjectTypes->read(reader);
            continue;
        }
        if (reader.name() == QString("LdCtrlLoadCompleted")) {
            auto * LdCtrlLoadCompleted = make_LoadProcedure_LdCtrlLoadCompleted_t(this);
            LdCtrlLoadCompleted->read(reader);
            continue;
        }
        if (reader.name() == QString("LdCtrlAbsSegment")) {
            auto * LdCtrlAbsSegment = make_LoadProcedure_LdCtrlAbsSegment_t(this);
            LdCtrlAbsSegment->read(reader);
            continue;
        }
        if (reader.name() == QString("LdCtrlRelSegment")) {
            auto * LdCtrlRelSegment = make_LoadProcedure_LdCtrlRelSegment_t(this);
            LdCtrlRelSegment->read(reader);
            continue;
        }
        if (reader.name() == QString("LdCtrlTaskSegment")) {
            auto * LdCtrlTaskSegment = make_LoadProcedure_LdCtrlTaskSegment_t(this);
            LdCtrlTaskSegment->read(reader);
            continue;
        }
        if (reader.name() == QString("LdCtrlTaskPtr")) {
            auto * LdCtrlTaskPtr = make_LoadProcedure_LdCtrlTaskPtr_t(this);
            LdCtrlTaskPtr->read(reader);
            continue;
        }
        if (reader.name() == QString("LdCtrlTaskCtrl1")) {
            auto * LdCtrlTaskCtrl1 = make_LoadProcedure_LdCtrlTaskCtrl1_t(this);
            LdCtrlTaskCtrl1->read(reader);
            continue;
        }
        if (reader.name() == QString("LdCtrlTaskCtrl2")) {
            auto * LdCtrlTaskCtrl2 = make_LoadProcedure_LdCtrlTaskCtrl2_t(this);
            LdCtrlTaskCtrl2->read(reader);
            continue;
        }
        if (reader.name() == QString("LdCtrlWriteProp")) {
            auto * LdCtrlWriteProp = make_LoadProcedure_LdCtrlWriteProp_t(this);
            LdCtrlWriteProp->read(reader);
            continue;
        }
        if (reader.name() == QString("LdCtrlCompareProp")) {
            auto * LdCtrlCompareProp = make_LoadProcedure_LdCtrlCompareProp_t(this);
            LdCtrlCompareProp->read(reader);
            continue;
        }
        if (reader.name() == QString("LdCtrlLoadImageProp")) {
            auto * LdCtrlLoadImageProp = make_LoadProcedure_LdCtrlLoadImageProp_t(this);
            LdCtrlLoadImageProp->read(reader);
            continue;
        }
        if (reader.name() == QString("LdCtrlInvokeFunctionProp")) {
            auto * LdCtrlInvokeFunctionProp = make_LoadProcedure_LdCtrlInvokeFunctionProp_t(this);
            LdCtrlInvokeFunctionProp->read(reader);
            continue;
        }
        if (reader.name() == QString("LdCtrlReadFunctionProp")) {
            auto * LdCtrlReadFunctionProp = make_LoadProcedure_LdCtrlReadFunctionProp_t(this);
            LdCtrlReadFunctionProp->read(reader);
            continue;
        }
        if (reader.name() == QString("LdCtrlWriteMem")) {
            auto * LdCtrlWriteMem = make_LoadProcedure_LdCtrlWriteMem_t(this);
            LdCtrlWriteMem->read(reader);
            continue;
        }
        if (reader.name() == QString("LdCtrlCompareMem")) {
            auto * LdCtrlCompareMem = make_LoadProcedure_LdCtrlCompareMem_t(this);
            LdCtrlCompareMem->read(reader);
            continue;
        }
        if (reader.name() == QString("LdCtrlLoadImageMem")) {
            auto * LdCtrlLoadImageMem = make_LoadProcedure_LdCtrlLoadImageMem_t(this);
            LdCtrlLoadImageMem->read(reader);
            continue;
        }
        if (reader.name() == QString("LdCtrlWriteRelMem")) {
            auto * LdCtrlWriteRelMem = make_LoadProcedure_LdCtrlWriteRelMem_t(this);
            LdCtrlWriteRelMem->read(reader);
            continue;
        }
        if (reader.name() == QString("LdCtrlCompareRelMem")) {
            auto * LdCtrlCompareRelMem = make_LoadProcedure_LdCtrlCompareRelMem_t(this);
            LdCtrlCompareRelMem->read(reader);
            continue;
        }
        if (reader.name() == QString("LdCtrlLoadImageRelMem")) {
            auto * LdCtrlLoadImageRelMem = make_LoadProcedure_LdCtrlLoadImageRelMem_t(this);
            LdCtrlLoadImageRelMem->read(reader);
            continue;
        }
        if (reader.name() == QString("LdCtrlConnect")) {
            auto * LdCtrlConnect = make_LoadProcedure_LdCtrlConnect_t(this);
            LdCtrlConnect->read(reader);
            continue;
        }
        if (reader.name() == QString("LdCtrlDisconnect")) {
            auto * LdCtrlDisconnect = make_LoadProcedure_LdCtrlDisconnect_t(this);
            LdCtrlDisconnect->read(reader);
            continue;
        }
        if (reader.name() == QString("LdCtrlRestart")) {
            auto * LdCtrlRestart = make_LoadProcedure_LdCtrlRestart_t(this);
            LdCtrlRestart->read(reader);
            continue;
        }
        if (reader.name() == QString("LdCtrlMasterReset")) {
            auto * LdCtrlMasterReset = make_LoadProcedure_LdCtrlMasterReset_t(this);
            LdCtrlMasterReset->read(reader);
            continue;
        }
        if (reader.name() == QString("LdCtrlDelay")) {
            auto * LdCtrlDelay = make_LoadProcedure_LdCtrlDelay_t(this);
            LdCtrlDelay->read(reader);
            continue;
        }
        if (reader.name() == QString("LdCtrlSetControlVariable")) {
            auto * LdCtrlSetControlVariable = make_LoadProcedure_LdCtrlSetControlVariable_t(this);
            LdCtrlSetControlVariable->read(reader);
            continue;
        }
        if (reader.name() == QString("LdCtrlMapError")) {
            auto * LdCtrlMapError = make_LoadProcedure_LdCtrlMapError_t(this);
            LdCtrlMapError->read(reader);
            continue;
        }
        if (reader.name() == QString("LdCtrlProgressText")) {
            auto * LdCtrlProgressText = make_LoadProcedure_LdCtrlProgressText_t(this);
            LdCtrlProgressText->read(reader);
            continue;
        }
        if (reader.name() == QString("LdCtrlDeclarePropDesc")) {
            auto * LdCtrlDeclarePropDesc = make_LoadProcedure_LdCtrlDeclarePropDesc_t(this);
            LdCtrlDeclarePropDesc->read(reader);
            continue;
        }
        if (reader.name() == QString("LdCtrlClearLCFilterTable")) {
            auto * LdCtrlClearLCFilterTable = make_LoadProcedure_LdCtrlClearLCFilterTable_t(this);
            LdCtrlClearLCFilterTable->read(reader);
            continue;
        }
        if (reader.name() == QString("LdCtrlMerge")) {
            auto * LdCtrlMerge = make_LoadProcedure_LdCtrlMerge_t(this);
            LdCtrlMerge->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int LoadProcedure_t::tableColumnCount() const
{
    return 1;
}

QVariant LoadProcedure_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "LoadProcedure";
        }
        break;
    }
    return QVariant();
}

QVariant LoadProcedure_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant LoadProcedure_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "LoadProcedure";
    case Qt::DecorationRole:
        return QIcon::fromTheme("LoadProcedure");
    }
    return QVariant();
}

LoadProcedure_t * make_LoadProcedure_t(Base * parent)
{
    return new LoadProcedure_t(parent);
}

} // namespace knx
} // namespace v13
} // namespace Project
