/* This file is generated. */

#include <Project/v13/knx/Security_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v13/knx/KNX_t.h>

namespace Project {
namespace v13 {
namespace knx {

Security_t::Security_t(Base * parent) :
    Base(parent)
{
}

Security_t::~Security_t()
{
}

void Security_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("LoadedIPRoutingBackboneKey")) {
            LoadedIPRoutingBackboneKey = attribute.value().toString();
            continue;
        }
        if (name == QString("DeviceAuthenticationCode")) {
            DeviceAuthenticationCode = attribute.value().toString();
            continue;
        }
        if (name == QString("LoadedDeviceAuthenticationCode")) {
            LoadedDeviceAuthenticationCode = attribute.value().toString();
            continue;
        }
        if (name == QString("DeviceManagementPassword")) {
            DeviceManagementPassword = attribute.value().toString();
            continue;
        }
        if (name == QString("LoadedDeviceManagementPassword")) {
            LoadedDeviceManagementPassword = attribute.value().toString();
            continue;
        }
        if (name == QString("ToolKey")) {
            ToolKey = attribute.value().toString();
            continue;
        }
        if (name == QString("LoadedToolKey")) {
            LoadedToolKey = attribute.value().toString();
            continue;
        }
        if (name == QString("SequenceNumber")) {
            SequenceNumber = attribute.value().toString();
            continue;
        }
        if (name == QString("SequenceNumberTimestamp")) {
            SequenceNumberTimestamp = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int Security_t::tableColumnCount() const
{
    return 10;
}

QVariant Security_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "Security";
        }
        if (qualifiedName == QString("LoadedIPRoutingBackboneKey")) {
            return LoadedIPRoutingBackboneKey;
        }
        if (qualifiedName == QString("DeviceAuthenticationCode")) {
            return DeviceAuthenticationCode;
        }
        if (qualifiedName == QString("LoadedDeviceAuthenticationCode")) {
            return LoadedDeviceAuthenticationCode;
        }
        if (qualifiedName == QString("DeviceManagementPassword")) {
            return DeviceManagementPassword;
        }
        if (qualifiedName == QString("LoadedDeviceManagementPassword")) {
            return LoadedDeviceManagementPassword;
        }
        if (qualifiedName == QString("ToolKey")) {
            return ToolKey;
        }
        if (qualifiedName == QString("LoadedToolKey")) {
            return LoadedToolKey;
        }
        if (qualifiedName == QString("SequenceNumber")) {
            return SequenceNumber;
        }
        if (qualifiedName == QString("SequenceNumberTimestamp")) {
            return SequenceNumberTimestamp;
        }
        break;
    }
    return QVariant();
}

QVariant Security_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "LoadedIPRoutingBackboneKey";
            case 2:
                return "DeviceAuthenticationCode";
            case 3:
                return "LoadedDeviceAuthenticationCode";
            case 4:
                return "DeviceManagementPassword";
            case 5:
                return "LoadedDeviceManagementPassword";
            case 6:
                return "ToolKey";
            case 7:
                return "LoadedToolKey";
            case 8:
                return "SequenceNumber";
            case 9:
                return "SequenceNumberTimestamp";
            }
        }
    }
    return QVariant();
}

QVariant Security_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "Security";
    case Qt::DecorationRole:
        return QIcon::fromTheme("Security");
    }
    return QVariant();
}

Security_t * make_Security_t(Base * parent)
{
    return new Security_t(parent);
}

} // namespace knx
} // namespace v13
} // namespace Project
