/* This file is generated. */

#pragma once

#include <Project/v13/knx/ResourceAccess_t.h>
#include <Project/xs/List.h>

namespace Project {
namespace v13 {
namespace knx {

using HawkConfigurationData_Resources_Resource_Access_t = xs::List<ResourceAccess_t>;

} // namespace knx
} // namespace v13
} // namespace Project
