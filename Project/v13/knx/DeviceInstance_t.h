/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v13/knx/CompletionStatus_t.h>
#include <Project/v13/knx/DeviceInstance_Address_t.h>
#include <Project/v13/knx/Guid_t.h>
#include <Project/v13/knx/IDREF.h>
#include <Project/v13/knx/String255_t.h>
#include <Project/xs/ID.h>
#include <Project/xs/Base64Binary.h>
#include <Project/xs/Boolean.h>
#include <Project/xs/DateTime.h>
#include <Project/xs/Int.h>
#include <Project/xs/String.h>
#include <Project/xs/UnsignedShort.h>

namespace Project {
namespace v13 {
namespace knx {

/* forward declarations */
class DeviceInstance_AdditionalAddresses_t;
class DeviceInstance_BinaryData_t;
class DeviceInstance_BusInterfaces_t;
class DeviceInstance_ChannelInstances_t;
class DeviceInstance_ComObjectInstanceRefs_t;
class DeviceInstance_ParameterInstanceRefs_t;
class Hardware2Program_t;
class Hardware_Products_Product_t;
class IPConfig_t;
class Security_t;

class DeviceInstance_t : public Base
{
    Q_OBJECT

public:
    explicit DeviceInstance_t(Base * parent = nullptr);
    virtual ~DeviceInstance_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    String255_t Name{};
    xs::ID Id{};
    IDREF ProductRefId{};
    IDREF Hardware2ProgramRefId{};
    DeviceInstance_Address_t Address{};
    xs::String Comment{};
    xs::DateTime LastModified{};
    xs::DateTime LastDownload{};
    xs::UnsignedShort LastUsedAPDULength{};
    xs::UnsignedShort ReadMaxAPDULength{};
    xs::UnsignedShort ReadMaxRoutingAPDULength{};
    xs::String InstallationHints{};
    CompletionStatus_t CompletionStatus{"Undefined"};
    xs::Boolean IndividualAddressLoaded{"false"};
    xs::Boolean ApplicationProgramLoaded{"false"};
    xs::Boolean ParametersLoaded{"false"};
    xs::Boolean CommunicationPartLoaded{"false"};
    xs::Boolean MediumConfigLoaded{"false"};
    xs::Base64Binary LoadedImage{};
    xs::String Description{};
    xs::Base64Binary CheckSums{};
    xs::Boolean IsCommunicationObjectVisibilityCalculated{};
    xs::Boolean Broken{"false"};
    xs::Base64Binary SerialNumber{};
    Guid_t UniqueId{};
    xs::Boolean IsRFRetransmitter{"false"};
    xs::Int Puid{};

    /* elements */
    DeviceInstance_ParameterInstanceRefs_t * ParameterInstanceRefs{};
    DeviceInstance_ComObjectInstanceRefs_t * ComObjectInstanceRefs{};
    DeviceInstance_ChannelInstances_t * ChannelInstances{};
    DeviceInstance_AdditionalAddresses_t * AdditionalAddresses{};
    DeviceInstance_BinaryData_t * BinaryData{};
    IPConfig_t * IPConfig{};
    Security_t * Security{};
    DeviceInstance_BusInterfaces_t * BusInterfaces{};

    /* getters */
    Hardware_Products_Product_t * getProduct() const; // attribute: ProductRefId
    Hardware2Program_t * getHardware2Program() const; // attribute: Hardware2ProgramRefId
};

DeviceInstance_t * make_DeviceInstance_t(Base * parent);

} // namespace knx
} // namespace v13
} // namespace Project
