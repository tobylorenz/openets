/* This file is generated. */

#include <Project/v13/knx/ApplicationProgramStatic_FixupList_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v13/knx/KNX_t.h>

namespace Project {
namespace v13 {
namespace knx {

ApplicationProgramStatic_FixupList_t::ApplicationProgramStatic_FixupList_t(Base * parent) :
    Base(parent)
{
}

ApplicationProgramStatic_FixupList_t::~ApplicationProgramStatic_FixupList_t()
{
}

void ApplicationProgramStatic_FixupList_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("Fixup")) {
            QString newFunctionRef = reader.attributes().value("FunctionRef").toString();
            Q_ASSERT(!newFunctionRef.isEmpty());
            Fixup_t * newFixup;
            if (Fixup.contains(newFunctionRef)) {
                newFixup = Fixup[newFunctionRef];
            } else {
                newFixup = make_Fixup_t(this);
                Fixup[newFunctionRef] = newFixup;
            }
            newFixup->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ApplicationProgramStatic_FixupList_t::tableColumnCount() const
{
    return 1;
}

QVariant ApplicationProgramStatic_FixupList_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ApplicationProgramStatic_FixupList";
        }
        break;
    }
    return QVariant();
}

QVariant ApplicationProgramStatic_FixupList_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant ApplicationProgramStatic_FixupList_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "FixupList";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ApplicationProgramStatic_FixupList");
    }
    return QVariant();
}

ApplicationProgramStatic_FixupList_t * make_ApplicationProgramStatic_FixupList_t(Base * parent)
{
    return new ApplicationProgramStatic_FixupList_t(parent);
}

} // namespace knx
} // namespace v13
} // namespace Project
