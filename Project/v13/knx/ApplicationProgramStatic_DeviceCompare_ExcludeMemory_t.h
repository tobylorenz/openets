/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v13/knx/ApplicationProgramStatic_DeviceCompare_ExcludeMemory_Offset_t.h>
#include <Project/v13/knx/ApplicationProgramStatic_DeviceCompare_ExcludeMemory_Size_t.h>
#include <Project/v13/knx/IDREF.h>
#include <Project/xs/String.h>

namespace Project {
namespace v13 {
namespace knx {

/* forward declarations */
class ApplicationProgramStatic_Code_AbsoluteSegment_t;

class ApplicationProgramStatic_DeviceCompare_ExcludeMemory_t : public Base
{
    Q_OBJECT

public:
    explicit ApplicationProgramStatic_DeviceCompare_ExcludeMemory_t(Base * parent = nullptr);
    virtual ~ApplicationProgramStatic_DeviceCompare_ExcludeMemory_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    IDREF CodeSegment{};
    ApplicationProgramStatic_DeviceCompare_ExcludeMemory_Offset_t Offset{};
    ApplicationProgramStatic_DeviceCompare_ExcludeMemory_Size_t Size{};
    xs::String InternalDescription{};

    /* getters */
    ApplicationProgramStatic_Code_AbsoluteSegment_t * getCodeSegment() const; // attribute: CodeSegment
};

ApplicationProgramStatic_DeviceCompare_ExcludeMemory_t * make_ApplicationProgramStatic_DeviceCompare_ExcludeMemory_t(Base * parent);

} // namespace knx
} // namespace v13
} // namespace Project
