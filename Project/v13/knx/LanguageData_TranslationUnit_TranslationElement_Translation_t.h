/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/xs/String.h>

namespace Project {
namespace v13 {
namespace knx {

class LanguageData_TranslationUnit_TranslationElement_Translation_t : public Base
{
    Q_OBJECT

public:
    explicit LanguageData_TranslationUnit_TranslationElement_Translation_t(Base * parent = nullptr);
    virtual ~LanguageData_TranslationUnit_TranslationElement_Translation_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::String AttributeName{};
    xs::String Text{};
};

LanguageData_TranslationUnit_TranslationElement_Translation_t * make_LanguageData_TranslationUnit_TranslationElement_Translation_t(Base * parent);

} // namespace knx
} // namespace v13
} // namespace Project
