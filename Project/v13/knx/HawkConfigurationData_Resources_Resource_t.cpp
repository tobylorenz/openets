/* This file is generated. */

#include <Project/v13/knx/HawkConfigurationData_Resources_Resource_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v13/knx/HawkConfigurationData_Resources_Resource_AccessRights_t.h>
#include <Project/v13/knx/HawkConfigurationData_Resources_Resource_ResourceType_t.h>
#include <Project/v13/knx/KNX_t.h>
#include <Project/v13/knx/ResourceLocation_t.h>

namespace Project {
namespace v13 {
namespace knx {

HawkConfigurationData_Resources_Resource_t::HawkConfigurationData_Resources_Resource_t(Base * parent) :
    Base(parent)
{
}

HawkConfigurationData_Resources_Resource_t::~HawkConfigurationData_Resources_Resource_t()
{
}

void HawkConfigurationData_Resources_Resource_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("Name")) {
            Name = attribute.value().toString();
            continue;
        }
        if (name == QString("Access")) {
            Access = attribute.value().toString().split(' ');
            continue;
        }
        if (name == QString("MgmtStyle")) {
            MgmtStyle = attribute.value().toString().split(' ');
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("Location")) {
            if (!Location) {
                Location = make_ResourceLocation_t(this);
            }
            Location->read(reader);
            continue;
        }
        if (reader.name() == QString("ImgLocation")) {
            if (!ImgLocation) {
                ImgLocation = make_ResourceLocation_t(this);
            }
            ImgLocation->read(reader);
            continue;
        }
        if (reader.name() == QString("ResourceType")) {
            if (!ResourceType) {
                ResourceType = make_HawkConfigurationData_Resources_Resource_ResourceType_t(this);
            }
            ResourceType->read(reader);
            continue;
        }
        if (reader.name() == QString("AccessRights")) {
            if (!AccessRights) {
                AccessRights = make_HawkConfigurationData_Resources_Resource_AccessRights_t(this);
            }
            AccessRights->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int HawkConfigurationData_Resources_Resource_t::tableColumnCount() const
{
    return 4;
}

QVariant HawkConfigurationData_Resources_Resource_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "HawkConfigurationData_Resources_Resource";
        }
        if (qualifiedName == QString("Name")) {
            return Name;
        }
        if (qualifiedName == QString("Access")) {
            return Access.join(' ');
        }
        if (qualifiedName == QString("MgmtStyle")) {
            return MgmtStyle.join(' ');
        }
        break;
    }
    return QVariant();
}

QVariant HawkConfigurationData_Resources_Resource_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Name";
            case 2:
                return "Access";
            case 3:
                return "MgmtStyle";
            }
        }
    }
    return QVariant();
}

QVariant HawkConfigurationData_Resources_Resource_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return !Name.isEmpty() ? Name : "Resource";
    case Qt::DecorationRole:
        return QIcon::fromTheme("HawkConfigurationData_Resources_Resource");
    }
    return QVariant();
}

HawkConfigurationData_Resources_Resource_t * make_HawkConfigurationData_Resources_Resource_t(Base * parent)
{
    return new HawkConfigurationData_Resources_Resource_t(parent);
}

} // namespace knx
} // namespace v13
} // namespace Project
