/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v13/knx/String20_t.h>
#include <Project/v13/knx/String255_t.h>
#include <Project/xs/String.h>

namespace Project {
namespace v13 {
namespace knx {

/* forward declarations */
class BusInterface_Connectors_t;

class BusInterface_t : public Base
{
    Q_OBJECT

public:
    explicit BusInterface_t(Base * parent = nullptr);
    virtual ~BusInterface_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    String255_t Name{};
    xs::String Description{};
    xs::String Comment{};
    String20_t Password{};

    /* elements */
    BusInterface_Connectors_t * Connectors{};

    /* getters */
};

BusInterface_t * make_BusInterface_t(Base * parent);

} // namespace knx
} // namespace v13
} // namespace Project
