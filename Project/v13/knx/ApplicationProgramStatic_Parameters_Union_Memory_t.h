/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v13/knx/ApplicationProgramStatic_Parameters_Union_Memory_Offset_t.h>
#include <Project/v13/knx/BitOffset_t.h>
#include <Project/v13/knx/IDREF.h>

namespace Project {
namespace v13 {
namespace knx {

/* forward declarations */
class ApplicationProgramStatic_Code_AbsoluteSegment_t;

class ApplicationProgramStatic_Parameters_Union_Memory_t : public Base
{
    Q_OBJECT

public:
    explicit ApplicationProgramStatic_Parameters_Union_Memory_t(Base * parent = nullptr);
    virtual ~ApplicationProgramStatic_Parameters_Union_Memory_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    IDREF CodeSegment{};
    ApplicationProgramStatic_Parameters_Union_Memory_Offset_t Offset{};
    BitOffset_t BitOffset{};

    /* getters */
    ApplicationProgramStatic_Code_AbsoluteSegment_t * getCodeSegment() const; // attribute: CodeSegment
};

ApplicationProgramStatic_Parameters_Union_Memory_t * make_ApplicationProgramStatic_Parameters_Union_Memory_t(Base * parent);

} // namespace knx
} // namespace v13
} // namespace Project
