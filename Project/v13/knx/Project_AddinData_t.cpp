/* This file is generated. */

#include <Project/v13/knx/Project_AddinData_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v13/knx/KNX_t.h>

namespace Project {
namespace v13 {
namespace knx {

Project_AddinData_t::Project_AddinData_t(Base * parent) :
    Base(parent)
{
}

Project_AddinData_t::~Project_AddinData_t()
{
}

void Project_AddinData_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("AddinData")) {
            QString newAddinId = reader.attributes().value("AddinId").toString();
            Q_ASSERT(!newAddinId.isEmpty());
            AddinData_t * newAddinData;
            if (AddinData.contains(newAddinId)) {
                newAddinData = AddinData[newAddinId];
            } else {
                newAddinData = make_AddinData_t(this);
                AddinData[newAddinId] = newAddinData;
            }
            newAddinData->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int Project_AddinData_t::tableColumnCount() const
{
    return 1;
}

QVariant Project_AddinData_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "Project_AddinData";
        }
        break;
    }
    return QVariant();
}

QVariant Project_AddinData_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant Project_AddinData_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "AddinData";
    case Qt::DecorationRole:
        return QIcon::fromTheme("Project_AddinData");
    }
    return QVariant();
}

Project_AddinData_t * make_Project_AddinData_t(Base * parent)
{
    return new Project_AddinData_t(parent);
}

} // namespace knx
} // namespace v13
} // namespace Project
