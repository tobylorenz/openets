/* This file is generated. */

#pragma once

#include <Project/v13/knx/IDREF.h>
#include <Project/xs/List.h>

namespace Project {
namespace v13 {
namespace knx {

using IDREFS = xs::List<IDREF>;

} // namespace knx
} // namespace v13
} // namespace Project
