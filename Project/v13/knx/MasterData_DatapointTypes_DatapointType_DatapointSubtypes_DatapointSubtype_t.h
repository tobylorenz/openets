/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v13/knx/IDREF.h>
#include <Project/v13/knx/LanguageDependentString255_t.h>
#include <Project/v13/knx/String255_t.h>
#include <Project/xs/ID.h>
#include <Project/xs/Boolean.h>
#include <Project/xs/UnsignedInt.h>

namespace Project {
namespace v13 {
namespace knx {

/* forward declarations */
class MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_t;
class MasterData_PropertyDataTypes_PropertyDataType_t;

class MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_t : public Base
{
    Q_OBJECT

public:
    explicit MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_t(Base * parent = nullptr);
    virtual ~MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::ID Id{};
    xs::UnsignedInt Number{};
    String255_t Name{};
    LanguageDependentString255_t Text{};
    xs::Boolean Default{"false"};
    IDREF PDT{};

    /* elements */
    MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_t * Format{};

    /* getters */
    MasterData_PropertyDataTypes_PropertyDataType_t * getPropertyDataType() const; // attribute: PDT
};

MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_t * make_MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_t(Base * parent);

} // namespace knx
} // namespace v13
} // namespace Project
