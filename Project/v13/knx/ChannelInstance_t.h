/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v13/knx/IDREF.h>
#include <Project/v13/knx/String255_t.h>
#include <Project/xs/ID.h>
#include <Project/xs/Boolean.h>

namespace Project {
namespace v13 {
namespace knx {

/* forward declarations */
class ApplicationProgramChannel_t;

class ChannelInstance_t : public Base
{
    Q_OBJECT

public:
    explicit ChannelInstance_t(Base * parent = nullptr);
    virtual ~ChannelInstance_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::ID Id{};
    IDREF RefId{};
    String255_t Name{};
    String255_t Description{};
    xs::Boolean IsActive{};

    /* getters */
    ApplicationProgramChannel_t * getChannel() const; // attribute: RefId
};

ChannelInstance_t * make_ChannelInstance_t(Base * parent);

} // namespace knx
} // namespace v13
} // namespace Project
