/* This file is generated. */

#pragma once

#include <Project/xs/UnsignedShort.h>

namespace Project {
namespace v13 {
namespace knx {

using Project_Installations_Installation_InstallationId_t = xs::UnsignedShort;

} // namespace knx
} // namespace v13
} // namespace Project
