/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v13/knx/CompletionStatus_t.h>
#include <Project/v13/knx/GroupAddressRef_t.h>
#include <Project/v13/knx/IDREF.h>
#include <Project/v13/knx/String255_t.h>
#include <Project/xs/ID.h>
#include <Project/xs/Int.h>
#include <Project/xs/String.h>

namespace Project {
namespace v13 {
namespace knx {

/* forward declarations */
class GroupRange_t;

class Function_t : public Base
{
    Q_OBJECT

public:
    explicit Function_t(Base * parent = nullptr);
    virtual ~Function_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::ID Id{};
    String255_t Name{};
    String255_t Type{};
    String255_t Number{};
    xs::String Comment{};
    xs::String Description{};
    CompletionStatus_t CompletionStatus{"Undefined"};
    IDREF DefaultGroupRange{};
    xs::Int Puid{};

    /* elements */
    QMap<xs::ID, GroupAddressRef_t *> GroupAddressRef; // key: Id

    /* getters */
    GroupRange_t * getDefaultGroupRange() const; // attribute: DefaultGroupRange
};

Function_t * make_Function_t(Base * parent);

} // namespace knx
} // namespace v13
} // namespace Project
