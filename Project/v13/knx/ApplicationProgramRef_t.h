/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v13/knx/IDREF.h>

namespace Project {
namespace v13 {
namespace knx {

/* forward declarations */
class ApplicationProgram_t;

class ApplicationProgramRef_t : public Base
{
    Q_OBJECT

public:
    explicit ApplicationProgramRef_t(Base * parent = nullptr);
    virtual ~ApplicationProgramRef_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    IDREF RefId{};

    /* getters */
    ApplicationProgram_t * getApplicationProgram() const; // attribute: RefId
};

ApplicationProgramRef_t * make_ApplicationProgramRef_t(Base * parent);

} // namespace knx
} // namespace v13
} // namespace Project
