/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v13/knx/LdCtrlProcType_t.h>
#include <Project/xs/String.h>
#include <Project/xs/UnsignedByte.h>

namespace Project {
namespace v13 {
namespace knx {

class LoadProcedure_LdCtrlMasterReset_t : public Base
{
    Q_OBJECT

public:
    explicit LoadProcedure_LdCtrlMasterReset_t(Base * parent = nullptr);
    virtual ~LoadProcedure_LdCtrlMasterReset_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::UnsignedByte EraseCode{};
    xs::UnsignedByte ChannelNumber{};
    LdCtrlProcType_t AppliesTo{"auto"};
    xs::String InternalDescription{};
};

LoadProcedure_LdCtrlMasterReset_t * make_LoadProcedure_LdCtrlMasterReset_t(Base * parent);

} // namespace knx
} // namespace v13
} // namespace Project
