/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v13/knx/IDREF.h>
#include <Project/xs/ID.h>

namespace Project {
namespace v13 {
namespace knx {

/* forward declarations */
class MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_Bit_t;
class MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_Enumeration_t;
class MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_Float_t;
class MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_RefType_t;
class MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_Reserved_t;
class MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_SignedInteger_t;
class MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_String_t;
class MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_UnsignedInteger_t;

class MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_t : public Base
{
    Q_OBJECT

public:
    explicit MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_t(Base * parent = nullptr);
    virtual ~MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* elements */
    // xs:choice MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_Bit_t * Bit{};
    // xs:choice MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_UnsignedInteger_t * UnsignedInteger{};
    // xs:choice MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_SignedInteger_t * SignedInteger{};
    // xs:choice MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_String_t * String{};
    // xs:choice MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_Float_t * Float{};
    // xs:choice MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_Enumeration_t * Enumeration{};
    // xs:choice MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_Reserved_t * Reserved{};
    // xs:choice MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_RefType_t * RefType{};
};

MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_t * make_MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_t(Base * parent);

} // namespace knx
} // namespace v13
} // namespace Project
