/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v13/knx/ResourceAccessRights_t.h>

namespace Project {
namespace v13 {
namespace knx {

class HawkConfigurationData_MemorySegments_MemorySegment_AccessRights_t : public Base
{
    Q_OBJECT

public:
    explicit HawkConfigurationData_MemorySegments_MemorySegment_AccessRights_t(Base * parent = nullptr);
    virtual ~HawkConfigurationData_MemorySegments_MemorySegment_AccessRights_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    ResourceAccessRights_t Read{};
    ResourceAccessRights_t Write{};
};

HawkConfigurationData_MemorySegments_MemorySegment_AccessRights_t * make_HawkConfigurationData_MemorySegments_MemorySegment_AccessRights_t(Base * parent);

} // namespace knx
} // namespace v13
} // namespace Project
