/* This file is generated. */

#pragma once

#include <Project/v13/knx/LanguageDependentString_t.h>

namespace Project {
namespace v13 {
namespace knx {

using LanguageDependentString255_t = LanguageDependentString_t;

} // namespace knx
} // namespace v13
} // namespace Project
