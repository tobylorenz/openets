/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v13/knx/ComObjectPriority_t.h>
#include <Project/v13/knx/Enable_t.h>
#include <Project/v13/knx/IDREF.h>
#include <Project/v13/knx/IDREFS.h>
#include <Project/v13/knx/MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_t.h>
#include <Project/v13/knx/String255_t.h>
#include <Project/xs/ID.h>
#include <Project/xs/Boolean.h>
#include <Project/xs/String.h>

namespace Project {
namespace v13 {
namespace knx {

/* forward declarations */
class ApplicationProgramChannel_t;
class ComObjectInstanceRef_Connectors_t;
class ComObjectRef_t;

class ComObjectInstanceRef_t : public Base
{
    Q_OBJECT

public:
    explicit ComObjectInstanceRef_t(Base * parent = nullptr);
    virtual ~ComObjectInstanceRef_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::ID Id{};
    IDREF RefId{};
    String255_t Text{};
    String255_t FunctionText{};
    ComObjectPriority_t Priority{};
    Enable_t ReadFlag{};
    Enable_t WriteFlag{};
    Enable_t CommunicationFlag{};
    Enable_t TransmitFlag{};
    Enable_t UpdateFlag{};
    Enable_t ReadOnInitFlag{};
    IDREFS DatapointType{};
    xs::String Description{};
    xs::Boolean IsActive{};
    IDREF ChannelId{};

    /* elements */
    ComObjectInstanceRef_Connectors_t * Connectors{};

    /* getters */
    ComObjectRef_t * getComObjectRef() const; // attribute: RefId
    QList<MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_t *> getDatapointType() const; // attribute: DatapointType
    ApplicationProgramChannel_t * getChannel() const; // attribute: ChannelId
};

ComObjectInstanceRef_t * make_ComObjectInstanceRef_t(Base * parent);

} // namespace knx
} // namespace v13
} // namespace Project
