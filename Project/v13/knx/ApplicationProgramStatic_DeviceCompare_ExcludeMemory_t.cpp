/* This file is generated. */

#include <Project/v13/knx/ApplicationProgramStatic_DeviceCompare_ExcludeMemory_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v13/knx/ApplicationProgramStatic_Code_AbsoluteSegment_t.h>
#include <Project/v13/knx/KNX_t.h>

namespace Project {
namespace v13 {
namespace knx {

ApplicationProgramStatic_DeviceCompare_ExcludeMemory_t::ApplicationProgramStatic_DeviceCompare_ExcludeMemory_t(Base * parent) :
    Base(parent)
{
}

ApplicationProgramStatic_DeviceCompare_ExcludeMemory_t::~ApplicationProgramStatic_DeviceCompare_ExcludeMemory_t()
{
}

void ApplicationProgramStatic_DeviceCompare_ExcludeMemory_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("CodeSegment")) {
            CodeSegment = attribute.value().toString();
            continue;
        }
        if (name == QString("Offset")) {
            Offset = attribute.value().toString();
            continue;
        }
        if (name == QString("Size")) {
            Size = attribute.value().toString();
            continue;
        }
        if (name == QString("InternalDescription")) {
            InternalDescription = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ApplicationProgramStatic_DeviceCompare_ExcludeMemory_t::tableColumnCount() const
{
    return 5;
}

QVariant ApplicationProgramStatic_DeviceCompare_ExcludeMemory_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ApplicationProgramStatic_DeviceCompare_ExcludeMemory";
        }
        if (qualifiedName == QString("CodeSegment")) {
            return CodeSegment;
        }
        if (qualifiedName == QString("Offset")) {
            return Offset;
        }
        if (qualifiedName == QString("Size")) {
            return Size;
        }
        if (qualifiedName == QString("InternalDescription")) {
            return InternalDescription;
        }
        break;
    }
    return QVariant();
}

QVariant ApplicationProgramStatic_DeviceCompare_ExcludeMemory_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "CodeSegment";
            case 2:
                return "Offset";
            case 3:
                return "Size";
            case 4:
                return "InternalDescription";
            }
        }
    }
    return QVariant();
}

QVariant ApplicationProgramStatic_DeviceCompare_ExcludeMemory_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "ExcludeMemory";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ApplicationProgramStatic_DeviceCompare_ExcludeMemory");
    }
    return QVariant();
}

ApplicationProgramStatic_Code_AbsoluteSegment_t * ApplicationProgramStatic_DeviceCompare_ExcludeMemory_t::getCodeSegment() const
{
    if (CodeSegment.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<ApplicationProgramStatic_Code_AbsoluteSegment_t *>(knx->ids[CodeSegment]);
}

ApplicationProgramStatic_DeviceCompare_ExcludeMemory_t * make_ApplicationProgramStatic_DeviceCompare_ExcludeMemory_t(Base * parent)
{
    return new ApplicationProgramStatic_DeviceCompare_ExcludeMemory_t(parent);
}

} // namespace knx
} // namespace v13
} // namespace Project
