/* This file is generated. */

#include <Project/v13/knx/UnionParameter_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v13/knx/KNX_t.h>
#include <Project/v13/knx/ParameterType_t.h>

namespace Project {
namespace v13 {
namespace knx {

UnionParameter_t::UnionParameter_t(Base * parent) :
    Base(parent)
{
}

UnionParameter_t::~UnionParameter_t()
{
}

void UnionParameter_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == "Id") {
            Id = attribute.value().toString();
            KNX_t * knx = findParent<KNX_t *>();
            Q_ASSERT(knx);
            knx->ids[Id] = this;
            continue;
        }
        if (name == QString("Name")) {
            Name = attribute.value().toString();
            continue;
        }
        if (name == QString("ParameterType")) {
            ParameterType = attribute.value().toString();
            continue;
        }
        if (name == QString("Offset")) {
            Offset = attribute.value().toString();
            continue;
        }
        if (name == QString("BitOffset")) {
            BitOffset = attribute.value().toString();
            continue;
        }
        if (name == QString("Text")) {
            Text = attribute.value().toString();
            continue;
        }
        if (name == QString("SuffixText")) {
            SuffixText = attribute.value().toString();
            continue;
        }
        if (name == QString("Access")) {
            Access = attribute.value().toString();
            continue;
        }
        if (name == QString("Value")) {
            Value = attribute.value().toString();
            continue;
        }
        if (name == QString("DefaultUnionParameter")) {
            DefaultUnionParameter = attribute.value().toString();
            continue;
        }
        if (name == QString("InternalDescription")) {
            InternalDescription = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int UnionParameter_t::tableColumnCount() const
{
    return 12;
}

QVariant UnionParameter_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "UnionParameter";
        }
        if (qualifiedName == QString("Id")) {
            return Id;
        }
        if (qualifiedName == QString("Name")) {
            return Name;
        }
        if (qualifiedName == QString("ParameterType")) {
            return ParameterType;
        }
        if (qualifiedName == QString("Offset")) {
            return Offset;
        }
        if (qualifiedName == QString("BitOffset")) {
            return BitOffset;
        }
        if (qualifiedName == QString("Text")) {
            return Text;
        }
        if (qualifiedName == QString("SuffixText")) {
            return SuffixText;
        }
        if (qualifiedName == QString("Access")) {
            return Access;
        }
        if (qualifiedName == QString("Value")) {
            return Value;
        }
        if (qualifiedName == QString("DefaultUnionParameter")) {
            return DefaultUnionParameter;
        }
        if (qualifiedName == QString("InternalDescription")) {
            return InternalDescription;
        }
        break;
    }
    return QVariant();
}

QVariant UnionParameter_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Id";
            case 2:
                return "Name";
            case 3:
                return "ParameterType";
            case 4:
                return "Offset";
            case 5:
                return "BitOffset";
            case 6:
                return "Text";
            case 7:
                return "SuffixText";
            case 8:
                return "Access";
            case 9:
                return "Value";
            case 10:
                return "DefaultUnionParameter";
            case 11:
                return "InternalDescription";
            }
        }
    }
    return QVariant();
}

QVariant UnionParameter_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return QString("[%1] %2").arg(Id.mid(Id.lastIndexOf("_UP-") + 4)).arg(Name);
    case Qt::DecorationRole:
        return QIcon::fromTheme("UnionParameter");
    }
    return QVariant();
}

ParameterType_t * UnionParameter_t::getParameterType() const
{
    if (ParameterType.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<ParameterType_t *>(knx->ids[ParameterType]);
}

UnionParameter_t * make_UnionParameter_t(Base * parent)
{
    return new UnionParameter_t(parent);
}

} // namespace knx
} // namespace v13
} // namespace Project
