/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v13/knx/ParameterType_TypeRestriction_Base_t.h>
#include <Project/v13/knx/ParameterType_TypeRestriction_Enumeration_t.h>
#include <Project/v13/knx/ParameterType_TypeRestriction_SizeInBit_t.h>
#include <Project/xs/ID.h>

namespace Project {
namespace v13 {
namespace knx {

class ParameterType_TypeRestriction_t : public Base
{
    Q_OBJECT

public:
    explicit ParameterType_TypeRestriction_t(Base * parent = nullptr);
    virtual ~ParameterType_TypeRestriction_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    ParameterType_TypeRestriction_Base_t Base_{};
    ParameterType_TypeRestriction_SizeInBit_t SizeInBit{};

    /* elements */
    QMap<xs::ID, ParameterType_TypeRestriction_Enumeration_t *> Enumeration; // key: Id
};

ParameterType_TypeRestriction_t * make_ParameterType_TypeRestriction_t(Base * parent);

} // namespace knx
} // namespace v13
} // namespace Project
