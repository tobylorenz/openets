/* This file is generated. */

#include <Project/v13/knx/ApplicationProgramStatic_ParameterTypes_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v13/knx/KNX_t.h>

namespace Project {
namespace v13 {
namespace knx {

ApplicationProgramStatic_ParameterTypes_t::ApplicationProgramStatic_ParameterTypes_t(Base * parent) :
    Base(parent)
{
}

ApplicationProgramStatic_ParameterTypes_t::~ApplicationProgramStatic_ParameterTypes_t()
{
}

void ApplicationProgramStatic_ParameterTypes_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("ParameterType")) {
            QString newId = reader.attributes().value("Id").toString();
            Q_ASSERT(!newId.isEmpty());
            ParameterType_t * newParameterType;
            if (ParameterType.contains(newId)) {
                newParameterType = ParameterType[newId];
            } else {
                newParameterType = make_ParameterType_t(this);
                ParameterType[newId] = newParameterType;
            }
            newParameterType->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ApplicationProgramStatic_ParameterTypes_t::tableColumnCount() const
{
    return 1;
}

QVariant ApplicationProgramStatic_ParameterTypes_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ApplicationProgramStatic_ParameterTypes";
        }
        break;
    }
    return QVariant();
}

QVariant ApplicationProgramStatic_ParameterTypes_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant ApplicationProgramStatic_ParameterTypes_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "ParameterTypes";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ApplicationProgramStatic_ParameterTypes");
    }
    return QVariant();
}

ApplicationProgramStatic_ParameterTypes_t * make_ApplicationProgramStatic_ParameterTypes_t(Base * parent)
{
    return new ApplicationProgramStatic_ParameterTypes_t(parent);
}

} // namespace knx
} // namespace v13
} // namespace Project
