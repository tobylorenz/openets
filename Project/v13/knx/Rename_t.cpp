/* This file is generated. */

#include <Project/v13/knx/Rename_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v13/knx/ComObjectParameterBlock_t.h>
#include <Project/v13/knx/KNX_t.h>

namespace Project {
namespace v13 {
namespace knx {

Rename_t::Rename_t(Base * parent) :
    Base(parent)
{
}

Rename_t::~Rename_t()
{
}

void Rename_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == "Id") {
            Id = attribute.value().toString();
            KNX_t * knx = findParent<KNX_t *>();
            Q_ASSERT(knx);
            knx->ids[Id] = this;
            continue;
        }
        if (name == QString("RefId")) {
            RefId = attribute.value().toString();
            continue;
        }
        if (name == QString("Name")) {
            Name = attribute.value().toString();
            continue;
        }
        if (name == QString("Text")) {
            Text = attribute.value().toString();
            continue;
        }
        if (name == QString("InternalDescription")) {
            InternalDescription = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int Rename_t::tableColumnCount() const
{
    return 6;
}

QVariant Rename_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "Rename";
        }
        if (qualifiedName == QString("Id")) {
            return Id;
        }
        if (qualifiedName == QString("RefId")) {
            return RefId;
        }
        if (qualifiedName == QString("Name")) {
            return Name;
        }
        if (qualifiedName == QString("Text")) {
            return Text;
        }
        if (qualifiedName == QString("InternalDescription")) {
            return InternalDescription;
        }
        break;
    }
    return QVariant();
}

QVariant Rename_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Id";
            case 2:
                return "RefId";
            case 3:
                return "Name";
            case 4:
                return "Text";
            case 5:
                return "InternalDescription";
            }
        }
    }
    return QVariant();
}

QVariant Rename_t::treeData(int role) const
{
    auto * ref = getComObjectParameterBlock();
    if (ref) {
        return ref->treeData(role);
    }

    switch (role) {
    case Qt::DisplayRole:
        return !Name.isEmpty() ? Name : "Rename";
    case Qt::DecorationRole:
        return QIcon::fromTheme("Rename");
    }
    return QVariant();
}

ComObjectParameterBlock_t * Rename_t::getComObjectParameterBlock() const
{
    if (RefId.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<ComObjectParameterBlock_t *>(knx->ids[RefId]);
}

Rename_t * make_Rename_t(Base * parent)
{
    return new Rename_t(parent);
}

} // namespace knx
} // namespace v13
} // namespace Project
