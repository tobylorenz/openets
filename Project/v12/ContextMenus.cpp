#include "ContextMenus.h"

#include <QApplication>
#include <QDebug>
#include <QMdiArea>
#include <QMdiSubWindow>
#include <QScrollArea>

QMenu * getCustomContextMenu(QMdiArea * mdiArea, Project::v12::knx::DeviceInstance_t * deviceInstance)
{
    Q_ASSERT(mdiArea);
    Q_ASSERT(deviceInstance);

    /* actions */
    QAction * parameterRefsAction = new QAction(QObject::tr("Parameter References"));
    Q_ASSERT(parameterRefsAction);
    QObject::connect(parameterRefsAction, &QAction::triggered, deviceInstance, [mdiArea, deviceInstance] {
        // @todo showParameterRefsWidget(mdiArea, deviceInstance);
    });

    QAction * parameterBlockAction = new QAction(QObject::tr("Parameter Blocks"));
    Q_ASSERT(parameterBlockAction);
    QObject::connect(parameterBlockAction, &QAction::triggered, deviceInstance, [mdiArea, deviceInstance] {
        // @todo showParameterBlocksWidget(mdiArea, deviceInstance);
    });

    /* context menu */
    QMenu * contextMenu = new QMenu(QObject::tr("Context Menu"));
    Q_ASSERT(contextMenu);
    contextMenu->addAction(parameterRefsAction);
    contextMenu->addAction(parameterBlockAction);
    return contextMenu;
}
