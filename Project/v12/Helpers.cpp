#include "Helpers.h"

#include <Project/v12/knx/DeviceInstance_ParameterInstanceRefs_t.h>
#include <Project/v12/knx/Parameter_t.h>
#include <Project/v12/knx/UnionParameter_t.h>

namespace Project {
namespace v12 {

knx::GroupAddress_t * findGroupAddress(const knx::GroupAddresses_t * groupAddresses, const uint16_t groupAddress_)
{
    if (groupAddresses) {
        knx::GroupAddresses_GroupRanges_t * groupRanges = groupAddresses->GroupRanges;
        if (groupRanges) {
            return findGroupAddress(groupRanges, groupAddress_);
        }
    }

    return nullptr;
}

knx::GroupAddress_t * findGroupAddress(const knx::GroupAddresses_GroupRanges_t * groupRanges, const uint16_t groupAddress_)
{
    if (groupRanges) {
        for (knx::GroupRange_t * groupRange : groupRanges->GroupRange) {
            if ((groupRange->RangeStart.toUInt() <= groupAddress_) &&
                    (groupRange->RangeEnd.toUInt() >= groupAddress_)) {
                return findGroupAddress(groupRange, groupAddress_);
            }
        }
    }

    return nullptr;
}

knx::GroupAddress_t * findGroupAddress(const knx::GroupRange_t * groupRange_, const uint16_t groupAddress_)
{
    if (groupRange_) {
        for (knx::GroupRange_t * groupRange : groupRange_->GroupRange) {
            if ((groupRange->RangeStart.toUInt() <= groupAddress_) &&
                    (groupRange->RangeEnd.toUInt() >= groupAddress_)) {
                return findGroupAddress(groupRange, groupAddress_);
            }
        }

        for (knx::GroupAddress_t * groupAddress : groupRange_->GroupAddress) {
            if (groupAddress->Address.toUInt() == groupAddress_) {
                return groupAddress;
            }
        }
    }

    return nullptr;
}

ParameterValueInfo getParameterValueInfo(const knx::ParameterRef_t * parameterRef, const knx::DeviceInstance_t * deviceInstance)
{
    ParameterValueInfo info;

    info.parameterRefValue = parameterRef->Value;

    Project::Base * parameter = parameterRef->getParameter();
    Q_ASSERT(parameter);
    knx::Parameter_t * normalParameter = qobject_cast<knx::Parameter_t *>(parameter);
    if (normalParameter) {
        info.parameterValue = normalParameter->Value;
    }
    knx::UnionParameter_t * unionParameter = qobject_cast<knx::UnionParameter_t *>(parameter);
    if (unionParameter) {
        info.parameterValue = unionParameter->Value;
    }
    Q_ASSERT(normalParameter || unionParameter);

    knx::DeviceInstance_ParameterInstanceRefs_t * parameterInstanceRefs = deviceInstance->ParameterInstanceRefs;
    if (parameterInstanceRefs) {
        knx::ParameterInstanceRef_t * parameterInstanceRef = parameterInstanceRefs->ParameterInstanceRef.value(parameterRef->Id, nullptr);
        if (parameterInstanceRef) {
            info.parameterInstanceRefValue = parameterInstanceRef->Value;
            info.hasParameterInstanceRef = true;
        }
    }

    return info;
}

bool ParameterValueInfo::isDefault() const
{
    return !hasParameterInstanceRef;
}

knx::Value_t ParameterValueInfo::getValue() const
{
    if (hasParameterInstanceRef) {
        return parameterInstanceRefValue;
    }

    if (!parameterRefValue.isEmpty()) {
        return parameterRefValue;
    }

    return parameterValue;
}

} // namespace v12
} // namespace Project
