/* This file is generated. */

#include <Project/v12/knx/Topology_Area_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v12/knx/KNX_t.h>

namespace Project {
namespace v12 {
namespace knx {

Topology_Area_t::Topology_Area_t(Base * parent) :
    Base(parent)
{
}

Topology_Area_t::~Topology_Area_t()
{
}

void Topology_Area_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == "Id") {
            Id = attribute.value().toString();
            KNX_t * knx = findParent<KNX_t *>();
            Q_ASSERT(knx);
            knx->ids[Id] = this;
            continue;
        }
        if (name == QString("Name")) {
            Name = attribute.value().toString();
            continue;
        }
        if (name == QString("Address")) {
            Address = attribute.value().toString();
            continue;
        }
        if (name == QString("Comment")) {
            Comment = attribute.value().toString();
            continue;
        }
        if (name == QString("CompletionStatus")) {
            CompletionStatus = attribute.value().toString();
            continue;
        }
        if (name == QString("Description")) {
            Description = attribute.value().toString();
            continue;
        }
        if (name == QString("Puid")) {
            Puid = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("Line")) {
            QString newId = reader.attributes().value("Id").toString();
            Q_ASSERT(!newId.isEmpty());
            Topology_Area_Line_t * newLine;
            if (Line.contains(newId)) {
                newLine = Line[newId];
            } else {
                newLine = make_Topology_Area_Line_t(this);
                Line[newId] = newLine;
            }
            newLine->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int Topology_Area_t::tableColumnCount() const
{
    return 8;
}

QVariant Topology_Area_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "Topology_Area";
        }
        if (qualifiedName == QString("Id")) {
            return Id;
        }
        if (qualifiedName == QString("Name")) {
            return Name;
        }
        if (qualifiedName == QString("Address")) {
            return Address;
        }
        if (qualifiedName == QString("Comment")) {
            return Comment;
        }
        if (qualifiedName == QString("CompletionStatus")) {
            return CompletionStatus;
        }
        if (qualifiedName == QString("Description")) {
            return Description;
        }
        if (qualifiedName == QString("Puid")) {
            return Puid;
        }
        break;
    }
    return QVariant();
}

QVariant Topology_Area_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Id";
            case 2:
                return "Name";
            case 3:
                return "Address";
            case 4:
                return "Comment";
            case 5:
                return "CompletionStatus";
            case 6:
                return "Description";
            case 7:
                return "Puid";
            }
        }
    }
    return QVariant();
}

QVariant Topology_Area_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return QString("%1 %2").arg(Address).arg(Name);
    case Qt::DecorationRole:
        return QIcon::fromTheme("Topology_Area");
    }
    return QVariant();
}

Topology_Area_t * make_Topology_Area_t(Base * parent)
{
    return new Topology_Area_t(parent);
}

} // namespace knx
} // namespace v12
} // namespace Project
