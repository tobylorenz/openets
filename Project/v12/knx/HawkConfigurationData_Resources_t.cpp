/* This file is generated. */

#include <Project/v12/knx/HawkConfigurationData_Resources_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v12/knx/KNX_t.h>

namespace Project {
namespace v12 {
namespace knx {

HawkConfigurationData_Resources_t::HawkConfigurationData_Resources_t(Base * parent) :
    Base(parent)
{
}

HawkConfigurationData_Resources_t::~HawkConfigurationData_Resources_t()
{
}

void HawkConfigurationData_Resources_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("Resource")) {
            auto * newResource = make_HawkConfigurationData_Resources_Resource_t(this);
            newResource->read(reader);
            Resource.append(newResource);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int HawkConfigurationData_Resources_t::tableColumnCount() const
{
    return 1;
}

QVariant HawkConfigurationData_Resources_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "HawkConfigurationData_Resources";
        }
        break;
    }
    return QVariant();
}

QVariant HawkConfigurationData_Resources_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant HawkConfigurationData_Resources_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "Resources";
    case Qt::DecorationRole:
        return QIcon::fromTheme("HawkConfigurationData_Resources");
    }
    return QVariant();
}

HawkConfigurationData_Resources_t * make_HawkConfigurationData_Resources_t(Base * parent)
{
    return new HawkConfigurationData_Resources_t(parent);
}

} // namespace knx
} // namespace v12
} // namespace Project
