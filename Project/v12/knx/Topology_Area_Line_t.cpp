/* This file is generated. */

#include <Project/v12/knx/Topology_Area_Line_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v12/knx/BusAccess_t.h>
#include <Project/v12/knx/KNX_t.h>
#include <Project/v12/knx/MasterData_MediumTypes_MediumType_t.h>
#include <Project/v12/knx/Topology_Area_Line_AdditionalGroupAddresses_t.h>
#include <Project/v12/knx/Topology_Area_t.h>

namespace Project {
namespace v12 {
namespace knx {

Topology_Area_Line_t::Topology_Area_Line_t(Base * parent) :
    Base(parent)
{
}

Topology_Area_Line_t::~Topology_Area_Line_t()
{
}

void Topology_Area_Line_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == "Id") {
            Id = attribute.value().toString();
            KNX_t * knx = findParent<KNX_t *>();
            Q_ASSERT(knx);
            knx->ids[Id] = this;
            continue;
        }
        if (name == QString("Name")) {
            Name = attribute.value().toString();
            continue;
        }
        if (name == QString("Address")) {
            Address = attribute.value().toString();
            continue;
        }
        if (name == QString("MediumTypeRefId")) {
            MediumTypeRefId = attribute.value().toString();
            continue;
        }
        if (name == QString("Comment")) {
            Comment = attribute.value().toString();
            continue;
        }
        if (name == QString("DomainAddress")) {
            DomainAddress = attribute.value().toString();
            continue;
        }
        if (name == QString("CompletionStatus")) {
            CompletionStatus = attribute.value().toString();
            continue;
        }
        if (name == QString("Description")) {
            Description = attribute.value().toString();
            continue;
        }
        if (name == QString("Puid")) {
            Puid = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("DeviceInstance")) {
            QString newId = reader.attributes().value("Id").toString();
            Q_ASSERT(!newId.isEmpty());
            DeviceInstance_t * newDeviceInstance;
            if (DeviceInstance.contains(newId)) {
                newDeviceInstance = DeviceInstance[newId];
            } else {
                newDeviceInstance = make_DeviceInstance_t(this);
                DeviceInstance[newId] = newDeviceInstance;
            }
            newDeviceInstance->read(reader);
            continue;
        }
        if (reader.name() == QString("BusAccess")) {
            if (!BusAccess) {
                BusAccess = make_BusAccess_t(this);
            }
            BusAccess->read(reader);
            continue;
        }
        if (reader.name() == QString("AdditionalGroupAddresses")) {
            if (!AdditionalGroupAddresses) {
                AdditionalGroupAddresses = make_Topology_Area_Line_AdditionalGroupAddresses_t(this);
            }
            AdditionalGroupAddresses->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int Topology_Area_Line_t::tableColumnCount() const
{
    return 10;
}

QVariant Topology_Area_Line_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "Topology_Area_Line";
        }
        if (qualifiedName == QString("Id")) {
            return Id;
        }
        if (qualifiedName == QString("Name")) {
            return Name;
        }
        if (qualifiedName == QString("Address")) {
            return Address;
        }
        if (qualifiedName == QString("MediumTypeRefId")) {
            return MediumTypeRefId;
        }
        if (qualifiedName == QString("Comment")) {
            return Comment;
        }
        if (qualifiedName == QString("DomainAddress")) {
            return DomainAddress;
        }
        if (qualifiedName == QString("CompletionStatus")) {
            return CompletionStatus;
        }
        if (qualifiedName == QString("Description")) {
            return Description;
        }
        if (qualifiedName == QString("Puid")) {
            return Puid;
        }
        break;
    }
    return QVariant();
}

QVariant Topology_Area_Line_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Id";
            case 2:
                return "Name";
            case 3:
                return "Address";
            case 4:
                return "MediumTypeRefId";
            case 5:
                return "Comment";
            case 6:
                return "DomainAddress";
            case 7:
                return "CompletionStatus";
            case 8:
                return "Description";
            case 9:
                return "Puid";
            }
        }
    }
    return QVariant();
}

QVariant Topology_Area_Line_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
    {
        Topology_Area_t * area = findParent<Topology_Area_t *>();
        Q_ASSERT(area);
        return QString("%1.%2 %3").arg(area->Address).arg(Address).arg(Name);
    }
    case Qt::DecorationRole:
    {
        MasterData_MediumTypes_MediumType_t * mediumType = getMediumType();
        Q_ASSERT(mediumType);
        return QIcon::fromTheme("Topology_Area_Line_" + mediumType->Name);
    }
    }
    return QVariant();
}

MasterData_MediumTypes_MediumType_t * Topology_Area_Line_t::getMediumType() const
{
    if (MediumTypeRefId.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<MasterData_MediumTypes_MediumType_t *>(knx->ids[MediumTypeRefId]);
}

Topology_Area_Line_t * make_Topology_Area_Line_t(Base * parent)
{
    return new Topology_Area_Line_t(parent);
}

} // namespace knx
} // namespace v12
} // namespace Project
