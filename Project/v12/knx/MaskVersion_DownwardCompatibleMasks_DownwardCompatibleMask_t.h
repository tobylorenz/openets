/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v12/knx/IDREF.h>

namespace Project {
namespace v12 {
namespace knx {

/* forward declarations */
class MaskVersion_t;

class MaskVersion_DownwardCompatibleMasks_DownwardCompatibleMask_t : public Base
{
    Q_OBJECT

public:
    explicit MaskVersion_DownwardCompatibleMasks_DownwardCompatibleMask_t(Base * parent = nullptr);
    virtual ~MaskVersion_DownwardCompatibleMasks_DownwardCompatibleMask_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    IDREF RefId{};

    /* getters */
    MaskVersion_t * getMaskVersion() const; // attribute: RefId
};

MaskVersion_DownwardCompatibleMasks_DownwardCompatibleMask_t * make_MaskVersion_DownwardCompatibleMasks_DownwardCompatibleMask_t(Base * parent);

} // namespace knx
} // namespace v12
} // namespace Project
