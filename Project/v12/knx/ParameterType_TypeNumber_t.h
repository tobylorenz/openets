/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v12/knx/ParameterType_TypeNumber_SizeInBit_t.h>
#include <Project/v12/knx/ParameterType_TypeNumber_Type_t.h>
#include <Project/v12/knx/ParameterType_TypeNumber_UIHint_t.h>
#include <Project/xs/Long.h>

namespace Project {
namespace v12 {
namespace knx {

class ParameterType_TypeNumber_t : public Base
{
    Q_OBJECT

public:
    explicit ParameterType_TypeNumber_t(Base * parent = nullptr);
    virtual ~ParameterType_TypeNumber_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    ParameterType_TypeNumber_SizeInBit_t SizeInBit{};
    ParameterType_TypeNumber_Type_t Type{};
    xs::Long minInclusive{};
    xs::Long maxInclusive{};
    ParameterType_TypeNumber_UIHint_t UIHint{};
};

ParameterType_TypeNumber_t * make_ParameterType_TypeNumber_t(Base * parent);

} // namespace knx
} // namespace v12
} // namespace Project
