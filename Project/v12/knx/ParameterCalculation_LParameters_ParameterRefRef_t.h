/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v12/knx/IDREF.h>
#include <Project/v12/knx/String50_t.h>
#include <Project/xs/Byte.h>
#include <Project/xs/String.h>

namespace Project {
namespace v12 {
namespace knx {

/* forward declarations */
class ParameterRef_t;

class ParameterCalculation_LParameters_ParameterRefRef_t : public Base
{
    Q_OBJECT

public:
    explicit ParameterCalculation_LParameters_ParameterRefRef_t(Base * parent = nullptr);
    virtual ~ParameterCalculation_LParameters_ParameterRefRef_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    IDREF RefId{};
    xs::Byte IndentLevel{"0"};
    xs::String InternalDescription{};
    String50_t AliasName{};

    /* getters */
    ParameterRef_t * getParameterRef() const; // attribute: RefId
};

ParameterCalculation_LParameters_ParameterRefRef_t * make_ParameterCalculation_LParameters_ParameterRefRef_t(Base * parent);

} // namespace knx
} // namespace v12
} // namespace Project
