/* This file is generated. */

#include <Project/v12/knx/SplitInfo_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v12/knx/KNX_t.h>

namespace Project {
namespace v12 {
namespace knx {

SplitInfo_t::SplitInfo_t(Base * parent) :
    Base(parent)
{
}

SplitInfo_t::~SplitInfo_t()
{
}

void SplitInfo_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("ObjectPath")) {
            ObjectPath = attribute.value().toString();
            continue;
        }
        if (name == QString("Cookie")) {
            Cookie = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int SplitInfo_t::tableColumnCount() const
{
    return 3;
}

QVariant SplitInfo_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "SplitInfo";
        }
        if (qualifiedName == QString("ObjectPath")) {
            return ObjectPath;
        }
        if (qualifiedName == QString("Cookie")) {
            return Cookie;
        }
        break;
    }
    return QVariant();
}

QVariant SplitInfo_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "ObjectPath";
            case 2:
                return "Cookie";
            }
        }
    }
    return QVariant();
}

QVariant SplitInfo_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "SplitInfo";
    case Qt::DecorationRole:
        return QIcon::fromTheme("SplitInfo");
    }
    return QVariant();
}

SplitInfo_t * make_SplitInfo_t(Base * parent)
{
    return new SplitInfo_t(parent);
}

} // namespace knx
} // namespace v12
} // namespace Project
