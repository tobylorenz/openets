/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v12/knx/IDREF.h>
#include <Project/v12/knx/String50_t.h>
#include <Project/xs/ID.h>
#include <Project/xs/String.h>

namespace Project {
namespace v12 {
namespace knx {

/* forward declarations */
class ParameterType_TypeColor_t;
class ParameterType_TypeDate_t;
class ParameterType_TypeFloat_t;
class ParameterType_TypeIPAddress_t;
class ParameterType_TypeNone_t;
class ParameterType_TypeNumber_t;
class ParameterType_TypePicture_t;
class ParameterType_TypeRestriction_t;
class ParameterType_TypeText_t;
class ParameterType_TypeTime_t;

class ParameterType_t : public Base
{
    Q_OBJECT

public:
    explicit ParameterType_t(Base * parent = nullptr);
    virtual ~ParameterType_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::ID Id{};
    String50_t Name{};
    xs::String InternalDescription{};
    xs::String Plugin{};

    /* elements */
    // xs:choice ParameterType_TypeNumber_t * TypeNumber{};
    // xs:choice ParameterType_TypeFloat_t * TypeFloat{};
    // xs:choice ParameterType_TypeRestriction_t * TypeRestriction{};
    // xs:choice ParameterType_TypeText_t * TypeText{};
    // xs:choice ParameterType_TypeTime_t * TypeTime{};
    // xs:choice ParameterType_TypeDate_t * TypeDate{};
    // xs:choice ParameterType_TypeIPAddress_t * TypeIPAddress{};
    // xs:choice ParameterType_TypePicture_t * TypePicture{};
    // xs:choice ParameterType_TypeColor_t * TypeColor{};
    // xs:choice ParameterType_TypeNone_t * TypeNone{};

    /* getters */
};

ParameterType_t * make_ParameterType_t(Base * parent);

} // namespace knx
} // namespace v12
} // namespace Project
