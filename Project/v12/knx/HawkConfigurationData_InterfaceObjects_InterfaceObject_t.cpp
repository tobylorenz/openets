/* This file is generated. */

#include <Project/v12/knx/HawkConfigurationData_InterfaceObjects_InterfaceObject_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v12/knx/KNX_t.h>

namespace Project {
namespace v12 {
namespace knx {

HawkConfigurationData_InterfaceObjects_InterfaceObject_t::HawkConfigurationData_InterfaceObjects_InterfaceObject_t(Base * parent) :
    Base(parent)
{
}

HawkConfigurationData_InterfaceObjects_InterfaceObject_t::~HawkConfigurationData_InterfaceObjects_InterfaceObject_t()
{
}

void HawkConfigurationData_InterfaceObjects_InterfaceObject_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("Index")) {
            Index = attribute.value().toString();
            continue;
        }
        if (name == QString("ObjectType")) {
            ObjectType = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("Property")) {
            auto * newProperty = make_HawkConfigurationData_InterfaceObjects_InterfaceObject_Property_t(this);
            newProperty->read(reader);
            Property.append(newProperty);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int HawkConfigurationData_InterfaceObjects_InterfaceObject_t::tableColumnCount() const
{
    return 3;
}

QVariant HawkConfigurationData_InterfaceObjects_InterfaceObject_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "HawkConfigurationData_InterfaceObjects_InterfaceObject";
        }
        if (qualifiedName == QString("Index")) {
            return Index;
        }
        if (qualifiedName == QString("ObjectType")) {
            return ObjectType;
        }
        break;
    }
    return QVariant();
}

QVariant HawkConfigurationData_InterfaceObjects_InterfaceObject_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Index";
            case 2:
                return "ObjectType";
            }
        }
    }
    return QVariant();
}

QVariant HawkConfigurationData_InterfaceObjects_InterfaceObject_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "InterfaceObject";
    case Qt::DecorationRole:
        return QIcon::fromTheme("HawkConfigurationData_InterfaceObjects_InterfaceObject");
    }
    return QVariant();
}

HawkConfigurationData_InterfaceObjects_InterfaceObject_t * make_HawkConfigurationData_InterfaceObjects_InterfaceObject_t(Base * parent)
{
    return new HawkConfigurationData_InterfaceObjects_InterfaceObject_t(parent);
}

} // namespace knx
} // namespace v12
} // namespace Project
