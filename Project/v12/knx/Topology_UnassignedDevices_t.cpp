/* This file is generated. */

#include <Project/v12/knx/Topology_UnassignedDevices_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v12/knx/KNX_t.h>

namespace Project {
namespace v12 {
namespace knx {

Topology_UnassignedDevices_t::Topology_UnassignedDevices_t(Base * parent) :
    Base(parent)
{
}

Topology_UnassignedDevices_t::~Topology_UnassignedDevices_t()
{
}

void Topology_UnassignedDevices_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("DeviceInstance")) {
            QString newId = reader.attributes().value("Id").toString();
            Q_ASSERT(!newId.isEmpty());
            DeviceInstance_t * newDeviceInstance;
            if (DeviceInstance.contains(newId)) {
                newDeviceInstance = DeviceInstance[newId];
            } else {
                newDeviceInstance = make_DeviceInstance_t(this);
                DeviceInstance[newId] = newDeviceInstance;
            }
            newDeviceInstance->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int Topology_UnassignedDevices_t::tableColumnCount() const
{
    return 1;
}

QVariant Topology_UnassignedDevices_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "Topology_UnassignedDevices";
        }
        break;
    }
    return QVariant();
}

QVariant Topology_UnassignedDevices_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant Topology_UnassignedDevices_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "UnassignedDevices";
    case Qt::DecorationRole:
        return QIcon::fromTheme("Topology_UnassignedDevices");
    }
    return QVariant();
}

Topology_UnassignedDevices_t * make_Topology_UnassignedDevices_t(Base * parent)
{
    return new Topology_UnassignedDevices_t(parent);
}

} // namespace knx
} // namespace v12
} // namespace Project
