/* This file is generated. */

#pragma once

#include <Project/xs/UnsignedShort.h>

namespace Project {
namespace v12 {
namespace knx {

using LoadProcedure_LdCtrlWriteProp_Count_t = xs::UnsignedShort;

} // namespace knx
} // namespace v12
} // namespace Project
