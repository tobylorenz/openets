/* This file is generated. */

#include <Project/v12/knx/MasterData_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v12/knx/KNX_t.h>
#include <Project/v12/knx/MasterData_DatapointTypes_t.h>
#include <Project/v12/knx/MasterData_FunctionalBlocks_t.h>
#include <Project/v12/knx/MasterData_InterfaceObjectProperties_t.h>
#include <Project/v12/knx/MasterData_InterfaceObjectTypes_t.h>
#include <Project/v12/knx/MasterData_Languages_t.h>
#include <Project/v12/knx/MasterData_Manufacturers_t.h>
#include <Project/v12/knx/MasterData_MaskVersions_t.h>
#include <Project/v12/knx/MasterData_MediumTypes_t.h>
#include <Project/v12/knx/MasterData_ProductLanguages_t.h>
#include <Project/v12/knx/MasterData_PropertyDataTypes_t.h>

namespace Project {
namespace v12 {
namespace knx {

MasterData_t::MasterData_t(Base * parent) :
    Base(parent)
{
}

MasterData_t::~MasterData_t()
{
}

void MasterData_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("Version")) {
            Version = attribute.value().toString();
            continue;
        }
        if (name == QString("Signature")) {
            Signature = attribute.value().toString();
            continue;
        }
        if (name == "Id") {
            Id = attribute.value().toString();
            KNX_t * knx = findParent<KNX_t *>();
            Q_ASSERT(knx);
            knx->ids[Id] = this;
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("DatapointTypes")) {
            if (!DatapointTypes) {
                DatapointTypes = make_MasterData_DatapointTypes_t(this);
            }
            DatapointTypes->read(reader);
            continue;
        }
        if (reader.name() == QString("InterfaceObjectTypes")) {
            if (!InterfaceObjectTypes) {
                InterfaceObjectTypes = make_MasterData_InterfaceObjectTypes_t(this);
            }
            InterfaceObjectTypes->read(reader);
            continue;
        }
        if (reader.name() == QString("InterfaceObjectProperties")) {
            if (!InterfaceObjectProperties) {
                InterfaceObjectProperties = make_MasterData_InterfaceObjectProperties_t(this);
            }
            InterfaceObjectProperties->read(reader);
            continue;
        }
        if (reader.name() == QString("PropertyDataTypes")) {
            if (!PropertyDataTypes) {
                PropertyDataTypes = make_MasterData_PropertyDataTypes_t(this);
            }
            PropertyDataTypes->read(reader);
            continue;
        }
        if (reader.name() == QString("MediumTypes")) {
            if (!MediumTypes) {
                MediumTypes = make_MasterData_MediumTypes_t(this);
            }
            MediumTypes->read(reader);
            continue;
        }
        if (reader.name() == QString("MaskVersions")) {
            if (!MaskVersions) {
                MaskVersions = make_MasterData_MaskVersions_t(this);
            }
            MaskVersions->read(reader);
            continue;
        }
        if (reader.name() == QString("FunctionalBlocks")) {
            if (!FunctionalBlocks) {
                FunctionalBlocks = make_MasterData_FunctionalBlocks_t(this);
            }
            FunctionalBlocks->read(reader);
            continue;
        }
        if (reader.name() == QString("ProductLanguages")) {
            if (!ProductLanguages) {
                ProductLanguages = make_MasterData_ProductLanguages_t(this);
            }
            ProductLanguages->read(reader);
            continue;
        }
        if (reader.name() == QString("Manufacturers")) {
            if (!Manufacturers) {
                Manufacturers = make_MasterData_Manufacturers_t(this);
            }
            Manufacturers->read(reader);
            continue;
        }
        if (reader.name() == QString("Languages")) {
            if (!Languages) {
                Languages = make_MasterData_Languages_t(this);
            }
            Languages->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int MasterData_t::tableColumnCount() const
{
    return 4;
}

QVariant MasterData_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "MasterData";
        }
        if (qualifiedName == QString("Version")) {
            return Version;
        }
        if (qualifiedName == QString("Signature")) {
            return Signature;
        }
        if (qualifiedName == QString("Id")) {
            return Id;
        }
        break;
    }
    return QVariant();
}

QVariant MasterData_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Version";
            case 2:
                return "Signature";
            case 3:
                return "Id";
            }
        }
    }
    return QVariant();
}

QVariant MasterData_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "MasterData";
    case Qt::DecorationRole:
        return QIcon::fromTheme("MasterData");
    }
    return QVariant();
}

MasterData_t * make_MasterData_t(Base * parent)
{
    return new MasterData_t(parent);
}

} // namespace knx
} // namespace v12
} // namespace Project
