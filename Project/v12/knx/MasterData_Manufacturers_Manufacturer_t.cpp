/* This file is generated. */

#include <Project/v12/knx/MasterData_Manufacturers_Manufacturer_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v12/knx/KNX_t.h>
#include <Project/v12/knx/MasterData_Manufacturers_Manufacturer_PublicKeys_t.h>

namespace Project {
namespace v12 {
namespace knx {

MasterData_Manufacturers_Manufacturer_t::MasterData_Manufacturers_Manufacturer_t(Base * parent) :
    Base(parent)
{
}

MasterData_Manufacturers_Manufacturer_t::~MasterData_Manufacturers_Manufacturer_t()
{
}

void MasterData_Manufacturers_Manufacturer_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == "Id") {
            Id = attribute.value().toString();
            KNX_t * knx = findParent<KNX_t *>();
            Q_ASSERT(knx);
            knx->ids[Id] = this;
            continue;
        }
        if (name == QString("Name")) {
            Name = attribute.value().toString();
            continue;
        }
        if (name == QString("KnxManufacturerId")) {
            KnxManufacturerId = attribute.value().toString();
            continue;
        }
        if (name == QString("DefaultLanguage")) {
            DefaultLanguage = attribute.value().toString();
            continue;
        }
        if (name == QString("CompatibilityGroup")) {
            CompatibilityGroup = attribute.value().toString();
            continue;
        }
        if (name == QString("ImportRestriction")) {
            ImportRestriction = attribute.value().toString();
            continue;
        }
        if (name == QString("ImportGroup")) {
            ImportGroup = attribute.value().toString().split(' ');
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("OrderNumberFormattingScript")) {
            if (!OrderNumberFormattingScript) {
                OrderNumberFormattingScript = make_SimpleElementTextType("OrderNumberFormattingScript", this);
            }
            OrderNumberFormattingScript->read(reader);
            continue;
        }
        if (reader.name() == QString("PublicKeys")) {
            if (!PublicKeys) {
                PublicKeys = make_MasterData_Manufacturers_Manufacturer_PublicKeys_t(this);
            }
            PublicKeys->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int MasterData_Manufacturers_Manufacturer_t::tableColumnCount() const
{
    return 8;
}

QVariant MasterData_Manufacturers_Manufacturer_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "MasterData_Manufacturers_Manufacturer";
        }
        if (qualifiedName == QString("Id")) {
            return Id;
        }
        if (qualifiedName == QString("Name")) {
            return Name;
        }
        if (qualifiedName == QString("KnxManufacturerId")) {
            return KnxManufacturerId;
        }
        if (qualifiedName == QString("DefaultLanguage")) {
            return DefaultLanguage;
        }
        if (qualifiedName == QString("CompatibilityGroup")) {
            return CompatibilityGroup;
        }
        if (qualifiedName == QString("ImportRestriction")) {
            return ImportRestriction;
        }
        if (qualifiedName == QString("ImportGroup")) {
            return ImportGroup.join(' ');
        }
        break;
    }
    return QVariant();
}

QVariant MasterData_Manufacturers_Manufacturer_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Id";
            case 2:
                return "Name";
            case 3:
                return "KnxManufacturerId";
            case 4:
                return "DefaultLanguage";
            case 5:
                return "CompatibilityGroup";
            case 6:
                return "ImportRestriction";
            case 7:
                return "ImportGroup";
            }
        }
    }
    return QVariant();
}

QVariant MasterData_Manufacturers_Manufacturer_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return QString("%1: %2").arg(Id).arg(Name);
    case Qt::DecorationRole:
        return QIcon::fromTheme("MasterData_Manufacturers_Manufacturer");
    }
    return QVariant();
}

MasterData_Manufacturers_Manufacturer_t * make_MasterData_Manufacturers_Manufacturer_t(Base * parent)
{
    return new MasterData_Manufacturers_Manufacturer_t(parent);
}

} // namespace knx
} // namespace v12
} // namespace Project
