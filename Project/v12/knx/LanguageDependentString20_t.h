/* This file is generated. */

#pragma once

#include <Project/v12/knx/LanguageDependentString_t.h>

namespace Project {
namespace v12 {
namespace knx {

using LanguageDependentString20_t = LanguageDependentString_t;

} // namespace knx
} // namespace v12
} // namespace Project
