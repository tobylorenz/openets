/* This file is generated. */

#include <Project/v12/knx/HawkConfigurationData_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v12/knx/HawkConfigurationData_Features_t.h>
#include <Project/v12/knx/HawkConfigurationData_InterfaceObjects_t.h>
#include <Project/v12/knx/HawkConfigurationData_MemorySegments_t.h>
#include <Project/v12/knx/HawkConfigurationData_Procedures_t.h>
#include <Project/v12/knx/HawkConfigurationData_Resources_t.h>
#include <Project/v12/knx/KNX_t.h>

namespace Project {
namespace v12 {
namespace knx {

HawkConfigurationData_t::HawkConfigurationData_t(Base * parent) :
    Base(parent)
{
}

HawkConfigurationData_t::~HawkConfigurationData_t()
{
}

void HawkConfigurationData_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("Ets3SystemPlugin")) {
            Ets3SystemPlugin = attribute.value().toString();
            continue;
        }
        if (name == QString("LegacyVersion")) {
            LegacyVersion = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("Features")) {
            if (!Features) {
                Features = make_HawkConfigurationData_Features_t(this);
            }
            Features->read(reader);
            continue;
        }
        if (reader.name() == QString("Resources")) {
            if (!Resources) {
                Resources = make_HawkConfigurationData_Resources_t(this);
            }
            Resources->read(reader);
            continue;
        }
        if (reader.name() == QString("Procedures")) {
            if (!Procedures) {
                Procedures = make_HawkConfigurationData_Procedures_t(this);
            }
            Procedures->read(reader);
            continue;
        }
        if (reader.name() == QString("MemorySegments")) {
            if (!MemorySegments) {
                MemorySegments = make_HawkConfigurationData_MemorySegments_t(this);
            }
            MemorySegments->read(reader);
            continue;
        }
        if (reader.name() == QString("InterfaceObjects")) {
            if (!InterfaceObjects) {
                InterfaceObjects = make_HawkConfigurationData_InterfaceObjects_t(this);
            }
            InterfaceObjects->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int HawkConfigurationData_t::tableColumnCount() const
{
    return 3;
}

QVariant HawkConfigurationData_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "HawkConfigurationData";
        }
        if (qualifiedName == QString("Ets3SystemPlugin")) {
            return Ets3SystemPlugin;
        }
        if (qualifiedName == QString("LegacyVersion")) {
            return LegacyVersion;
        }
        break;
    }
    return QVariant();
}

QVariant HawkConfigurationData_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Ets3SystemPlugin";
            case 2:
                return "LegacyVersion";
            }
        }
    }
    return QVariant();
}

QVariant HawkConfigurationData_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "HawkConfigurationData";
    case Qt::DecorationRole:
        return QIcon::fromTheme("HawkConfigurationData");
    }
    return QVariant();
}

HawkConfigurationData_t * make_HawkConfigurationData_t(Base * parent)
{
    return new HawkConfigurationData_t(parent);
}

} // namespace knx
} // namespace v12
} // namespace Project
