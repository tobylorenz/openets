/* This file is generated. */

#pragma once

#include <Project/xs/String.h>

namespace Project {
namespace v12 {
namespace knx {

using ParameterType_TypeColor_Space_t = xs::String;

} // namespace knx
} // namespace v12
} // namespace Project
