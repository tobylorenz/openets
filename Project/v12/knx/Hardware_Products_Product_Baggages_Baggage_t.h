/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v12/knx/IDREF.h>

namespace Project {
namespace v12 {
namespace knx {

/* forward declarations */
class ManufacturerData_Manufacturer_Baggages_Baggage_t;

class Hardware_Products_Product_Baggages_Baggage_t : public Base
{
    Q_OBJECT

public:
    explicit Hardware_Products_Product_Baggages_Baggage_t(Base * parent = nullptr);
    virtual ~Hardware_Products_Product_Baggages_Baggage_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    IDREF RefId{};

    /* getters */
    ManufacturerData_Manufacturer_Baggages_Baggage_t * getBaggage() const; // attribute: RefId
};

Hardware_Products_Product_Baggages_Baggage_t * make_Hardware_Products_Product_Baggages_Baggage_t(Base * parent);

} // namespace knx
} // namespace v12
} // namespace Project
