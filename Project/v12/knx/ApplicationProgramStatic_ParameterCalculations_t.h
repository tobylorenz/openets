/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v12/knx/ParameterCalculation_t.h>

namespace Project {
namespace v12 {
namespace knx {

class ApplicationProgramStatic_ParameterCalculations_t : public Base
{
    Q_OBJECT

public:
    explicit ApplicationProgramStatic_ParameterCalculations_t(Base * parent = nullptr);
    virtual ~ApplicationProgramStatic_ParameterCalculations_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* elements */
    QMap<xs::ID, ParameterCalculation_t *> ParameterCalculation; // key: Id
};

ApplicationProgramStatic_ParameterCalculations_t * make_ApplicationProgramStatic_ParameterCalculations_t(Base * parent);

} // namespace knx
} // namespace v12
} // namespace Project
