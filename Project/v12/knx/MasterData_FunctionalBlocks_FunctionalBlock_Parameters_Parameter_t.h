/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v12/knx/IDREF.h>
#include <Project/v12/knx/LanguageDependentString_t.h>

namespace Project {
namespace v12 {
namespace knx {

/* forward declarations */
class MasterData_InterfaceObjectProperties_InterfaceObjectProperty_t;

class MasterData_FunctionalBlocks_FunctionalBlock_Parameters_Parameter_t : public Base
{
    Q_OBJECT

public:
    explicit MasterData_FunctionalBlocks_FunctionalBlock_Parameters_Parameter_t(Base * parent = nullptr);
    virtual ~MasterData_FunctionalBlocks_FunctionalBlock_Parameters_Parameter_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    IDREF Property{};
    LanguageDependentString_t Description{};

    /* getters */
    MasterData_InterfaceObjectProperties_InterfaceObjectProperty_t * getInterfaceObjectProperty() const; // attribute: Property
};

MasterData_FunctionalBlocks_FunctionalBlock_Parameters_Parameter_t * make_MasterData_FunctionalBlocks_FunctionalBlock_Parameters_Parameter_t(Base * parent);

} // namespace knx
} // namespace v12
} // namespace Project
