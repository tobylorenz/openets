/* This file is generated. */

#include <Project/v12/knx/ParameterCalculation_LParameters_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v12/knx/KNX_t.h>

namespace Project {
namespace v12 {
namespace knx {

ParameterCalculation_LParameters_t::ParameterCalculation_LParameters_t(Base * parent) :
    Base(parent)
{
}

ParameterCalculation_LParameters_t::~ParameterCalculation_LParameters_t()
{
}

void ParameterCalculation_LParameters_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("ParameterRefRef")) {
            QString newRefId = reader.attributes().value("RefId").toString();
            Q_ASSERT(!newRefId.isEmpty());
            ParameterCalculation_LParameters_ParameterRefRef_t * newParameterRefRef;
            if (ParameterRefRef.contains(newRefId)) {
                newParameterRefRef = ParameterRefRef[newRefId];
            } else {
                newParameterRefRef = make_ParameterCalculation_LParameters_ParameterRefRef_t(this);
                ParameterRefRef[newRefId] = newParameterRefRef;
            }
            newParameterRefRef->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ParameterCalculation_LParameters_t::tableColumnCount() const
{
    return 1;
}

QVariant ParameterCalculation_LParameters_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ParameterCalculation_LParameters";
        }
        break;
    }
    return QVariant();
}

QVariant ParameterCalculation_LParameters_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant ParameterCalculation_LParameters_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "LParameters";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ParameterCalculation_LParameters");
    }
    return QVariant();
}

ParameterCalculation_LParameters_t * make_ParameterCalculation_LParameters_t(Base * parent)
{
    return new ParameterCalculation_LParameters_t(parent);
}

} // namespace knx
} // namespace v12
} // namespace Project
