/* This file is generated. */

#pragma once

#include <Project/xs/String.h>

namespace Project {
namespace v12 {
namespace knx {

using Project_Installations_Installation_SplitType_t = xs::String;

} // namespace knx
} // namespace v12
} // namespace Project
