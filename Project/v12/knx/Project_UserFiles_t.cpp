/* This file is generated. */

#include <Project/v12/knx/Project_UserFiles_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v12/knx/KNX_t.h>

namespace Project {
namespace v12 {
namespace knx {

Project_UserFiles_t::Project_UserFiles_t(Base * parent) :
    Base(parent)
{
}

Project_UserFiles_t::~Project_UserFiles_t()
{
}

void Project_UserFiles_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("UserFile")) {
            auto * newUserFile = make_UserFile_t(this);
            newUserFile->read(reader);
            UserFile.append(newUserFile);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int Project_UserFiles_t::tableColumnCount() const
{
    return 1;
}

QVariant Project_UserFiles_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "Project_UserFiles";
        }
        break;
    }
    return QVariant();
}

QVariant Project_UserFiles_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant Project_UserFiles_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "UserFiles";
    case Qt::DecorationRole:
        return QIcon::fromTheme("Project_UserFiles");
    }
    return QVariant();
}

Project_UserFiles_t * make_Project_UserFiles_t(Base * parent)
{
    return new Project_UserFiles_t(parent);
}

} // namespace knx
} // namespace v12
} // namespace Project
