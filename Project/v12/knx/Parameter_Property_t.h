/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v12/knx/BitOffset_t.h>
#include <Project/xs/UnsignedByte.h>
#include <Project/xs/UnsignedInt.h>
#include <Project/xs/UnsignedShort.h>

namespace Project {
namespace v12 {
namespace knx {

class Parameter_Property_t : public Base
{
    Q_OBJECT

public:
    explicit Parameter_Property_t(Base * parent = nullptr);
    virtual ~Parameter_Property_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::UnsignedByte ObjectIndex{};
    xs::UnsignedShort ObjectType{};
    xs::UnsignedByte Occurrence{"0"};
    xs::UnsignedByte PropertyId{};
    xs::UnsignedInt Offset{};
    BitOffset_t BitOffset{};
};

Parameter_Property_t * make_Parameter_Property_t(Base * parent);

} // namespace knx
} // namespace v12
} // namespace Project
