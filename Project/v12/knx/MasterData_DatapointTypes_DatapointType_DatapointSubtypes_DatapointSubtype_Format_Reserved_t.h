/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/xs/Int.h>

namespace Project {
namespace v12 {
namespace knx {

class MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_Reserved_t : public Base
{
    Q_OBJECT

public:
    explicit MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_Reserved_t(Base * parent = nullptr);
    virtual ~MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_Reserved_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::Int Width{};
};

MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_Reserved_t * make_MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_Reserved_t(Base * parent);

} // namespace knx
} // namespace v12
} // namespace Project
