/* This file is generated. */

#pragma once

#include <Project/xs/String.h>

namespace Project {
namespace v12 {
namespace knx {

using ParameterType_TypeNumber_UIHint_t = xs::String;

} // namespace knx
} // namespace v12
} // namespace Project
