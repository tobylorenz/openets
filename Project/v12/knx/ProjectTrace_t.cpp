/* This file is generated. */

#include <Project/v12/knx/ProjectTrace_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v12/knx/KNX_t.h>

namespace Project {
namespace v12 {
namespace knx {

ProjectTrace_t::ProjectTrace_t(Base * parent) :
    Base(parent)
{
}

ProjectTrace_t::~ProjectTrace_t()
{
}

void ProjectTrace_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("Date")) {
            Date = attribute.value().toString();
            continue;
        }
        if (name == QString("UserName")) {
            UserName = attribute.value().toString();
            continue;
        }
        if (name == QString("Comment")) {
            Comment = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ProjectTrace_t::tableColumnCount() const
{
    return 4;
}

QVariant ProjectTrace_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ProjectTrace";
        }
        if (qualifiedName == QString("Date")) {
            return Date;
        }
        if (qualifiedName == QString("UserName")) {
            return UserName;
        }
        if (qualifiedName == QString("Comment")) {
            return Comment;
        }
        break;
    }
    return QVariant();
}

QVariant ProjectTrace_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Date";
            case 2:
                return "UserName";
            case 3:
                return "Comment";
            }
        }
    }
    return QVariant();
}

QVariant ProjectTrace_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "ProjectTrace";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ProjectTrace");
    }
    return QVariant();
}

ProjectTrace_t * make_ProjectTrace_t(Base * parent)
{
    return new ProjectTrace_t(parent);
}

} // namespace knx
} // namespace v12
} // namespace Project
