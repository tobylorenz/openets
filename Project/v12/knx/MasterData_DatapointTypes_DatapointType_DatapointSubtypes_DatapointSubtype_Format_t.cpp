/* This file is generated. */

#include <Project/v12/knx/MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v12/knx/KNX_t.h>
#include <Project/v12/knx/MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_Bit_t.h>
#include <Project/v12/knx/MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_Enumeration_t.h>
#include <Project/v12/knx/MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_Float_t.h>
#include <Project/v12/knx/MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_RefType_t.h>
#include <Project/v12/knx/MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_Reserved_t.h>
#include <Project/v12/knx/MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_SignedInteger_t.h>
#include <Project/v12/knx/MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_String_t.h>
#include <Project/v12/knx/MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_UnsignedInteger_t.h>

namespace Project {
namespace v12 {
namespace knx {

MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_t::MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_t(Base * parent) :
    Base(parent)
{
}

MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_t::~MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_t()
{
}

void MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("Bit")) {
            auto * Bit = make_MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_Bit_t(this);
            Bit->read(reader);
            continue;
        }
        if (reader.name() == QString("UnsignedInteger")) {
            auto * UnsignedInteger = make_MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_UnsignedInteger_t(this);
            UnsignedInteger->read(reader);
            continue;
        }
        if (reader.name() == QString("SignedInteger")) {
            auto * SignedInteger = make_MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_SignedInteger_t(this);
            SignedInteger->read(reader);
            continue;
        }
        if (reader.name() == QString("String")) {
            auto * String = make_MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_String_t(this);
            String->read(reader);
            continue;
        }
        if (reader.name() == QString("Float")) {
            auto * Float = make_MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_Float_t(this);
            Float->read(reader);
            continue;
        }
        if (reader.name() == QString("Enumeration")) {
            auto * Enumeration = make_MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_Enumeration_t(this);
            Enumeration->read(reader);
            continue;
        }
        if (reader.name() == QString("Reserved")) {
            auto * Reserved = make_MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_Reserved_t(this);
            Reserved->read(reader);
            continue;
        }
        if (reader.name() == QString("RefType")) {
            auto * RefType = make_MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_RefType_t(this);
            RefType->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_t::tableColumnCount() const
{
    return 1;
}

QVariant MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format";
        }
        break;
    }
    return QVariant();
}

QVariant MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "Format";
    case Qt::DecorationRole:
        return QIcon::fromTheme("MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format");
    }
    return QVariant();
}

MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_t * make_MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_t(Base * parent)
{
    return new MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_t(parent);
}

} // namespace knx
} // namespace v12
} // namespace Project
