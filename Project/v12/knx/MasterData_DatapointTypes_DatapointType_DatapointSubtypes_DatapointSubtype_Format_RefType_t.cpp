/* This file is generated. */

#include <Project/v12/knx/MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_RefType_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v12/knx/KNX_t.h>

namespace Project {
namespace v12 {
namespace knx {

MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_RefType_t::MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_RefType_t(Base * parent) :
    Base(parent)
{
}

MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_RefType_t::~MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_RefType_t()
{
}

void MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_RefType_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("RefId")) {
            RefId = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_RefType_t::tableColumnCount() const
{
    return 2;
}

QVariant MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_RefType_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_RefType";
        }
        if (qualifiedName == QString("RefId")) {
            return RefId;
        }
        break;
    }
    return QVariant();
}

QVariant MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_RefType_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "RefId";
            }
        }
    }
    return QVariant();
}

QVariant MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_RefType_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "RefType";
    case Qt::DecorationRole:
        return QIcon::fromTheme("MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_RefType");
    }
    return QVariant();
}

Base * MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_RefType_t::getType() const
{
    if (RefId.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<Base *>(knx->ids[RefId]);
}

MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_RefType_t * make_MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_RefType_t(Base * parent)
{
    return new MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_RefType_t(parent);
}

} // namespace knx
} // namespace v12
} // namespace Project
