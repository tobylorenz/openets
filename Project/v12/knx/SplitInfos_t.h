/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v12/knx/SplitInfo_t.h>

namespace Project {
namespace v12 {
namespace knx {

class SplitInfos_t : public Base
{
    Q_OBJECT

public:
    explicit SplitInfos_t(Base * parent = nullptr);
    virtual ~SplitInfos_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* elements */
    QVector<SplitInfo_t *> SplitInfo;
};

SplitInfos_t * make_SplitInfos_t(Base * parent);

} // namespace knx
} // namespace v12
} // namespace Project
