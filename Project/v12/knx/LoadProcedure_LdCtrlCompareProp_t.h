/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v12/knx/LdCtrlProcType_t.h>
#include <Project/v12/knx/LoadProcedure_LdCtrlCompareProp_Count_t.h>
#include <Project/v12/knx/LoadProcedure_LdCtrlCompareProp_StartElement_t.h>
#include <Project/xs/HexBinary.h>
#include <Project/xs/String.h>
#include <Project/xs/UnsignedByte.h>
#include <Project/xs/UnsignedShort.h>

namespace Project {
namespace v12 {
namespace knx {

class LoadProcedure_LdCtrlCompareProp_t : public Base
{
    Q_OBJECT

public:
    explicit LoadProcedure_LdCtrlCompareProp_t(Base * parent = nullptr);
    virtual ~LoadProcedure_LdCtrlCompareProp_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::UnsignedByte ObjIdx{};
    xs::UnsignedShort ObjType{};
    xs::UnsignedByte Occurrence{"0"};
    xs::UnsignedByte PropId{};
    LoadProcedure_LdCtrlCompareProp_StartElement_t StartElement{"1"};
    LoadProcedure_LdCtrlCompareProp_Count_t Count{"1"};
    xs::HexBinary InlineData{};
    LdCtrlProcType_t AppliesTo{"auto"};
    xs::String InternalDescription{};
};

LoadProcedure_LdCtrlCompareProp_t * make_LoadProcedure_LdCtrlCompareProp_t(Base * parent);

} // namespace knx
} // namespace v12
} // namespace Project
