/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v12/knx/CompletionStatus_t.h>
#include <Project/v12/knx/String255_t.h>
#include <Project/v12/knx/Topology_Area_Address_t.h>
#include <Project/v12/knx/Topology_Area_Line_t.h>
#include <Project/xs/ID.h>
#include <Project/xs/Int.h>
#include <Project/xs/String.h>

namespace Project {
namespace v12 {
namespace knx {

class Topology_Area_t : public Base
{
    Q_OBJECT

public:
    explicit Topology_Area_t(Base * parent = nullptr);
    virtual ~Topology_Area_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::ID Id{};
    String255_t Name{};
    Topology_Area_Address_t Address{};
    xs::String Comment{};
    CompletionStatus_t CompletionStatus{};
    xs::String Description{};
    xs::Int Puid{};

    /* elements */
    QMap<xs::ID, Topology_Area_Line_t *> Line; // key: Id
};

Topology_Area_t * make_Topology_Area_t(Base * parent);

} // namespace knx
} // namespace v12
} // namespace Project
