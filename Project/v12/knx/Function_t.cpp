/* This file is generated. */

#include <Project/v12/knx/Function_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v12/knx/GroupRange_t.h>
#include <Project/v12/knx/KNX_t.h>

namespace Project {
namespace v12 {
namespace knx {

Function_t::Function_t(Base * parent) :
    Base(parent)
{
}

Function_t::~Function_t()
{
}

void Function_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == "Id") {
            Id = attribute.value().toString();
            KNX_t * knx = findParent<KNX_t *>();
            Q_ASSERT(knx);
            knx->ids[Id] = this;
            continue;
        }
        if (name == QString("Name")) {
            Name = attribute.value().toString();
            continue;
        }
        if (name == QString("Type")) {
            Type = attribute.value().toString();
            continue;
        }
        if (name == QString("Number")) {
            Number = attribute.value().toString();
            continue;
        }
        if (name == QString("Comment")) {
            Comment = attribute.value().toString();
            continue;
        }
        if (name == QString("Description")) {
            Description = attribute.value().toString();
            continue;
        }
        if (name == QString("CompletionStatus")) {
            CompletionStatus = attribute.value().toString();
            continue;
        }
        if (name == QString("DefaultGroupRange")) {
            DefaultGroupRange = attribute.value().toString();
            continue;
        }
        if (name == QString("Puid")) {
            Puid = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("GroupAddressRef")) {
            QString newId = reader.attributes().value("Id").toString();
            Q_ASSERT(!newId.isEmpty());
            GroupAddressRef_t * newGroupAddressRef;
            if (GroupAddressRef.contains(newId)) {
                newGroupAddressRef = GroupAddressRef[newId];
            } else {
                newGroupAddressRef = make_GroupAddressRef_t(this);
                GroupAddressRef[newId] = newGroupAddressRef;
            }
            newGroupAddressRef->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int Function_t::tableColumnCount() const
{
    return 10;
}

QVariant Function_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "Function";
        }
        if (qualifiedName == QString("Id")) {
            return Id;
        }
        if (qualifiedName == QString("Name")) {
            return Name;
        }
        if (qualifiedName == QString("Type")) {
            return Type;
        }
        if (qualifiedName == QString("Number")) {
            return Number;
        }
        if (qualifiedName == QString("Comment")) {
            return Comment;
        }
        if (qualifiedName == QString("Description")) {
            return Description;
        }
        if (qualifiedName == QString("CompletionStatus")) {
            return CompletionStatus;
        }
        if (qualifiedName == QString("DefaultGroupRange")) {
            return DefaultGroupRange;
        }
        if (qualifiedName == QString("Puid")) {
            return Puid;
        }
        break;
    }
    return QVariant();
}

QVariant Function_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Id";
            case 2:
                return "Name";
            case 3:
                return "Type";
            case 4:
                return "Number";
            case 5:
                return "Comment";
            case 6:
                return "Description";
            case 7:
                return "CompletionStatus";
            case 8:
                return "DefaultGroupRange";
            case 9:
                return "Puid";
            }
        }
    }
    return QVariant();
}

QVariant Function_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return !Name.isEmpty() ? Name : "Function";
    case Qt::DecorationRole:
        return QIcon::fromTheme("Function");
    }
    return QVariant();
}

GroupRange_t * Function_t::getDefaultGroupRange() const
{
    if (DefaultGroupRange.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<GroupRange_t *>(knx->ids[DefaultGroupRange]);
}

Function_t * make_Function_t(Base * parent)
{
    return new Function_t(parent);
}

} // namespace knx
} // namespace v12
} // namespace Project
