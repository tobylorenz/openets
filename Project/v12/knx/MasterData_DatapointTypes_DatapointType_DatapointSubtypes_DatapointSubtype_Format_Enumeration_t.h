/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v12/knx/LanguageDependentString_t.h>
#include <Project/v12/knx/MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_Enumeration_EnumValue_t.h>
#include <Project/xs/ID.h>
#include <Project/xs/UnsignedInt.h>

namespace Project {
namespace v12 {
namespace knx {

class MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_Enumeration_t : public Base
{
    Q_OBJECT

public:
    explicit MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_Enumeration_t(Base * parent = nullptr);
    virtual ~MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_Enumeration_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::ID Id{};
    xs::UnsignedInt Width{};
    LanguageDependentString_t Name{};

    /* elements */
    QMap<xs::ID, MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_Enumeration_EnumValue_t *> EnumValue; // key: Id
};

MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_Enumeration_t * make_MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_Enumeration_t(Base * parent);

} // namespace knx
} // namespace v12
} // namespace Project
