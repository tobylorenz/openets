/* This file is generated. */

#include <Project/v12/knx/Trade_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v12/knx/KNX_t.h>

namespace Project {
namespace v12 {
namespace knx {

Trade_t::Trade_t(Base * parent) :
    Base(parent)
{
}

Trade_t::~Trade_t()
{
}

void Trade_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == "Id") {
            Id = attribute.value().toString();
            KNX_t * knx = findParent<KNX_t *>();
            Q_ASSERT(knx);
            knx->ids[Id] = this;
            continue;
        }
        if (name == QString("Name")) {
            Name = attribute.value().toString();
            continue;
        }
        if (name == QString("Number")) {
            Number = attribute.value().toString();
            continue;
        }
        if (name == QString("Comment")) {
            Comment = attribute.value().toString();
            continue;
        }
        if (name == QString("CompletionStatus")) {
            CompletionStatus = attribute.value().toString();
            continue;
        }
        if (name == QString("Description")) {
            Description = attribute.value().toString();
            continue;
        }
        if (name == QString("Puid")) {
            Puid = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("Trade")) {
            auto * newTrade = make_Trade_t(this);
            newTrade->read(reader);
            Trade.append(newTrade);
            continue;
        }
        if (reader.name() == QString("DeviceInstanceRef")) {
            QString newRefId = reader.attributes().value("RefId").toString();
            Q_ASSERT(!newRefId.isEmpty());
            DeviceInstanceRef_t * newDeviceInstanceRef;
            if (DeviceInstanceRef.contains(newRefId)) {
                newDeviceInstanceRef = DeviceInstanceRef[newRefId];
            } else {
                newDeviceInstanceRef = make_DeviceInstanceRef_t(this);
                DeviceInstanceRef[newRefId] = newDeviceInstanceRef;
            }
            newDeviceInstanceRef->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int Trade_t::tableColumnCount() const
{
    return 8;
}

QVariant Trade_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "Trade";
        }
        if (qualifiedName == QString("Id")) {
            return Id;
        }
        if (qualifiedName == QString("Name")) {
            return Name;
        }
        if (qualifiedName == QString("Number")) {
            return Number;
        }
        if (qualifiedName == QString("Comment")) {
            return Comment;
        }
        if (qualifiedName == QString("CompletionStatus")) {
            return CompletionStatus;
        }
        if (qualifiedName == QString("Description")) {
            return Description;
        }
        if (qualifiedName == QString("Puid")) {
            return Puid;
        }
        break;
    }
    return QVariant();
}

QVariant Trade_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Id";
            case 2:
                return "Name";
            case 3:
                return "Number";
            case 4:
                return "Comment";
            case 5:
                return "CompletionStatus";
            case 6:
                return "Description";
            case 7:
                return "Puid";
            }
        }
    }
    return QVariant();
}

QVariant Trade_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return !Name.isEmpty() ? Name : "Trade";
    case Qt::DecorationRole:
        return QIcon::fromTheme("Trade");
    }
    return QVariant();
}

Trade_t * make_Trade_t(Base * parent)
{
    return new Trade_t(parent);
}

} // namespace knx
} // namespace v12
} // namespace Project
