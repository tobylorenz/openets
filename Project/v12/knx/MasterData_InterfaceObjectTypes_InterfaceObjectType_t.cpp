/* This file is generated. */

#include <Project/v12/knx/MasterData_InterfaceObjectTypes_InterfaceObjectType_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v12/knx/KNX_t.h>

namespace Project {
namespace v12 {
namespace knx {

MasterData_InterfaceObjectTypes_InterfaceObjectType_t::MasterData_InterfaceObjectTypes_InterfaceObjectType_t(Base * parent) :
    Base(parent)
{
}

MasterData_InterfaceObjectTypes_InterfaceObjectType_t::~MasterData_InterfaceObjectTypes_InterfaceObjectType_t()
{
}

void MasterData_InterfaceObjectTypes_InterfaceObjectType_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == "Id") {
            Id = attribute.value().toString();
            KNX_t * knx = findParent<KNX_t *>();
            Q_ASSERT(knx);
            knx->ids[Id] = this;
            continue;
        }
        if (name == QString("Number")) {
            Number = attribute.value().toString();
            continue;
        }
        if (name == QString("Name")) {
            Name = attribute.value().toString();
            continue;
        }
        if (name == QString("Text")) {
            Text = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int MasterData_InterfaceObjectTypes_InterfaceObjectType_t::tableColumnCount() const
{
    return 5;
}

QVariant MasterData_InterfaceObjectTypes_InterfaceObjectType_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "MasterData_InterfaceObjectTypes_InterfaceObjectType";
        }
        if (qualifiedName == QString("Id")) {
            return Id;
        }
        if (qualifiedName == QString("Number")) {
            return Number;
        }
        if (qualifiedName == QString("Name")) {
            return Name;
        }
        if (qualifiedName == QString("Text")) {
            return Text;
        }
        break;
    }
    return QVariant();
}

QVariant MasterData_InterfaceObjectTypes_InterfaceObjectType_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Id";
            case 2:
                return "Number";
            case 3:
                return "Name";
            case 4:
                return "Text";
            }
        }
    }
    return QVariant();
}

QVariant MasterData_InterfaceObjectTypes_InterfaceObjectType_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return QString("%1: %2").arg(Id).arg(Name);
    case Qt::DecorationRole:
        return QIcon::fromTheme("MasterData_InterfaceObjectTypes_InterfaceObjectType");
    }
    return QVariant();
}

MasterData_InterfaceObjectTypes_InterfaceObjectType_t * make_MasterData_InterfaceObjectTypes_InterfaceObjectType_t(Base * parent)
{
    return new MasterData_InterfaceObjectTypes_InterfaceObjectType_t(parent);
}

} // namespace knx
} // namespace v12
} // namespace Project
