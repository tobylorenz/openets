/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v12/knx/UserFile_t.h>

namespace Project {
namespace v12 {
namespace knx {

class Project_UserFiles_t : public Base
{
    Q_OBJECT

public:
    explicit Project_UserFiles_t(Base * parent = nullptr);
    virtual ~Project_UserFiles_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* elements */
    QVector<UserFile_t *> UserFile;
};

Project_UserFiles_t * make_Project_UserFiles_t(Base * parent);

} // namespace knx
} // namespace v12
} // namespace Project
