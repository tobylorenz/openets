/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v12/knx/ComObjectInstanceRef_Connectors_Receive_t.h>
#include <Project/v12/knx/IDREF.h>

namespace Project {
namespace v12 {
namespace knx {

/* forward declarations */
class ComObjectInstanceRef_Connectors_Send_t;

class ComObjectInstanceRef_Connectors_t : public Base
{
    Q_OBJECT

public:
    explicit ComObjectInstanceRef_Connectors_t(Base * parent = nullptr);
    virtual ~ComObjectInstanceRef_Connectors_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* elements */
    ComObjectInstanceRef_Connectors_Send_t * Send{};
    QMap<IDREF, ComObjectInstanceRef_Connectors_Receive_t *> Receive; // key: GroupAddressRefId
};

ComObjectInstanceRef_Connectors_t * make_ComObjectInstanceRef_Connectors_t(Base * parent);

} // namespace knx
} // namespace v12
} // namespace Project
