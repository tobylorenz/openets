/* This file is generated. */

#include <Project/v12/knx/GroupAddressRef_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v12/knx/GroupAddress_t.h>
#include <Project/v12/knx/KNX_t.h>

namespace Project {
namespace v12 {
namespace knx {

GroupAddressRef_t::GroupAddressRef_t(Base * parent) :
    Base(parent)
{
}

GroupAddressRef_t::~GroupAddressRef_t()
{
}

void GroupAddressRef_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == "Id") {
            Id = attribute.value().toString();
            KNX_t * knx = findParent<KNX_t *>();
            Q_ASSERT(knx);
            knx->ids[Id] = this;
            continue;
        }
        if (name == QString("Name")) {
            Name = attribute.value().toString();
            continue;
        }
        if (name == QString("RefId")) {
            RefId = attribute.value().toString();
            continue;
        }
        if (name == QString("Role")) {
            Role = attribute.value().toString();
            continue;
        }
        if (name == QString("Puid")) {
            Puid = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int GroupAddressRef_t::tableColumnCount() const
{
    return 6;
}

QVariant GroupAddressRef_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "GroupAddressRef";
        }
        if (qualifiedName == QString("Id")) {
            return Id;
        }
        if (qualifiedName == QString("Name")) {
            return Name;
        }
        if (qualifiedName == QString("RefId")) {
            return RefId;
        }
        if (qualifiedName == QString("Role")) {
            return Role;
        }
        if (qualifiedName == QString("Puid")) {
            return Puid;
        }
        break;
    }
    return QVariant();
}

QVariant GroupAddressRef_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Id";
            case 2:
                return "Name";
            case 3:
                return "RefId";
            case 4:
                return "Role";
            case 5:
                return "Puid";
            }
        }
    }
    return QVariant();
}

QVariant GroupAddressRef_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return !Name.isEmpty() ? Name : "GroupAddressRef";
    case Qt::DecorationRole:
        return QIcon::fromTheme("GroupAddressRef");
    }
    return QVariant();
}

GroupAddress_t * GroupAddressRef_t::getGroupAddress() const
{
    if (RefId.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<GroupAddress_t *>(knx->ids[RefId]);
}

GroupAddressRef_t * make_GroupAddressRef_t(Base * parent)
{
    return new GroupAddressRef_t(parent);
}

} // namespace knx
} // namespace v12
} // namespace Project
