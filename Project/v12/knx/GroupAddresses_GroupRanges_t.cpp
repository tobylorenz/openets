/* This file is generated. */

#include <Project/v12/knx/GroupAddresses_GroupRanges_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v12/knx/KNX_t.h>

namespace Project {
namespace v12 {
namespace knx {

GroupAddresses_GroupRanges_t::GroupAddresses_GroupRanges_t(Base * parent) :
    Base(parent)
{
}

GroupAddresses_GroupRanges_t::~GroupAddresses_GroupRanges_t()
{
}

void GroupAddresses_GroupRanges_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("GroupRange")) {
            QString newId = reader.attributes().value("Id").toString();
            Q_ASSERT(!newId.isEmpty());
            GroupRange_t * newGroupRange;
            if (GroupRange.contains(newId)) {
                newGroupRange = GroupRange[newId];
            } else {
                newGroupRange = make_GroupRange_t(this);
                GroupRange[newId] = newGroupRange;
            }
            newGroupRange->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int GroupAddresses_GroupRanges_t::tableColumnCount() const
{
    return 1;
}

QVariant GroupAddresses_GroupRanges_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "GroupAddresses_GroupRanges";
        }
        break;
    }
    return QVariant();
}

QVariant GroupAddresses_GroupRanges_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant GroupAddresses_GroupRanges_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "GroupRanges";
    case Qt::DecorationRole:
        return QIcon::fromTheme("GroupAddresses_GroupRanges");
    }
    return QVariant();
}

GroupAddresses_GroupRanges_t * make_GroupAddresses_GroupRanges_t(Base * parent)
{
    return new GroupAddresses_GroupRanges_t(parent);
}

} // namespace knx
} // namespace v12
} // namespace Project
