/* This file is generated. */

#include <Project/v12/knx/Buildings_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v12/knx/KNX_t.h>

namespace Project {
namespace v12 {
namespace knx {

Buildings_t::Buildings_t(Base * parent) :
    Base(parent)
{
}

Buildings_t::~Buildings_t()
{
}

void Buildings_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("BuildingPart")) {
            QString newId = reader.attributes().value("Id").toString();
            Q_ASSERT(!newId.isEmpty());
            BuildingPart_t * newBuildingPart;
            if (BuildingPart.contains(newId)) {
                newBuildingPart = BuildingPart[newId];
            } else {
                newBuildingPart = make_BuildingPart_t(this);
                BuildingPart[newId] = newBuildingPart;
            }
            newBuildingPart->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int Buildings_t::tableColumnCount() const
{
    return 1;
}

QVariant Buildings_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "Buildings";
        }
        break;
    }
    return QVariant();
}

QVariant Buildings_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant Buildings_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "Buildings";
    case Qt::DecorationRole:
        return QIcon::fromTheme("Buildings");
    }
    return QVariant();
}

Buildings_t * make_Buildings_t(Base * parent)
{
    return new Buildings_t(parent);
}

} // namespace knx
} // namespace v12
} // namespace Project
