/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v12/knx/ComObjectInstanceRef_t.h>

namespace Project {
namespace v12 {
namespace knx {

class DeviceInstance_ComObjectInstanceRefs_t : public Base
{
    Q_OBJECT

public:
    explicit DeviceInstance_ComObjectInstanceRefs_t(Base * parent = nullptr);
    virtual ~DeviceInstance_ComObjectInstanceRefs_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* elements */
    QMap<IDREF, ComObjectInstanceRef_t *> ComObjectInstanceRef; // key: RefId
};

DeviceInstance_ComObjectInstanceRefs_t * make_DeviceInstance_ComObjectInstanceRefs_t(Base * parent);

} // namespace knx
} // namespace v12
} // namespace Project
