/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v12/knx/CatalogSection_t.h>

namespace Project {
namespace v12 {
namespace knx {

class ManufacturerData_Manufacturer_Catalog_t : public Base
{
    Q_OBJECT

public:
    explicit ManufacturerData_Manufacturer_Catalog_t(Base * parent = nullptr);
    virtual ~ManufacturerData_Manufacturer_Catalog_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* elements */
    QMap<xs::ID, CatalogSection_t *> CatalogSection; // key: Id
};

ManufacturerData_Manufacturer_Catalog_t * make_ManufacturerData_Manufacturer_Catalog_t(Base * parent);

} // namespace knx
} // namespace v12
} // namespace Project
