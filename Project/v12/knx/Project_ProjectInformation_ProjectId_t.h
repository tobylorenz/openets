/* This file is generated. */

#pragma once

#include <Project/xs/UnsignedShort.h>

namespace Project {
namespace v12 {
namespace knx {

using Project_ProjectInformation_ProjectId_t = xs::UnsignedShort;

} // namespace knx
} // namespace v12
} // namespace Project
