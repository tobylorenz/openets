/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v12/knx/IDREF.h>
#include <Project/v12/knx/LanguageDependentString255_t.h>
#include <Project/v12/knx/String50_t.h>
#include <Project/xs/ID.h>
#include <Project/xs/String.h>

namespace Project {
namespace v12 {
namespace knx {

/* forward declarations */
class ComObjectParameterBlock_t;

class Rename_t : public Base
{
    Q_OBJECT

public:
    explicit Rename_t(Base * parent = nullptr);
    virtual ~Rename_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::ID Id{};
    IDREF RefId{};
    String50_t Name{};
    LanguageDependentString255_t Text{};
    xs::String InternalDescription{};

    /* getters */
    ComObjectParameterBlock_t * getComObjectParameterBlock() const; // attribute: RefId
};

Rename_t * make_Rename_t(Base * parent);

} // namespace knx
} // namespace v12
} // namespace Project
