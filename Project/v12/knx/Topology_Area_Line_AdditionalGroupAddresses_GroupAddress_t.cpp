/* This file is generated. */

#include <Project/v12/knx/Topology_Area_Line_AdditionalGroupAddresses_GroupAddress_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v12/knx/KNX_t.h>

namespace Project {
namespace v12 {
namespace knx {

Topology_Area_Line_AdditionalGroupAddresses_GroupAddress_t::Topology_Area_Line_AdditionalGroupAddresses_GroupAddress_t(Base * parent) :
    Base(parent)
{
}

Topology_Area_Line_AdditionalGroupAddresses_GroupAddress_t::~Topology_Area_Line_AdditionalGroupAddresses_GroupAddress_t()
{
}

void Topology_Area_Line_AdditionalGroupAddresses_GroupAddress_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("Address")) {
            Address = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int Topology_Area_Line_AdditionalGroupAddresses_GroupAddress_t::tableColumnCount() const
{
    return 2;
}

QVariant Topology_Area_Line_AdditionalGroupAddresses_GroupAddress_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "Topology_Area_Line_AdditionalGroupAddresses_GroupAddress";
        }
        if (qualifiedName == QString("Address")) {
            return Address;
        }
        break;
    }
    return QVariant();
}

QVariant Topology_Area_Line_AdditionalGroupAddresses_GroupAddress_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Address";
            }
        }
    }
    return QVariant();
}

QVariant Topology_Area_Line_AdditionalGroupAddresses_GroupAddress_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "GroupAddress";
    case Qt::DecorationRole:
        return QIcon::fromTheme("Topology_Area_Line_AdditionalGroupAddresses_GroupAddress");
    }
    return QVariant();
}

Topology_Area_Line_AdditionalGroupAddresses_GroupAddress_t * make_Topology_Area_Line_AdditionalGroupAddresses_GroupAddress_t(Base * parent)
{
    return new Topology_Area_Line_AdditionalGroupAddresses_GroupAddress_t(parent);
}

} // namespace knx
} // namespace v12
} // namespace Project
