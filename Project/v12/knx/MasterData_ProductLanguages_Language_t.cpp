/* This file is generated. */

#include <Project/v12/knx/MasterData_ProductLanguages_Language_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v12/knx/KNX_t.h>

namespace Project {
namespace v12 {
namespace knx {

MasterData_ProductLanguages_Language_t::MasterData_ProductLanguages_Language_t(Base * parent) :
    Base(parent)
{
}

MasterData_ProductLanguages_Language_t::~MasterData_ProductLanguages_Language_t()
{
}

void MasterData_ProductLanguages_Language_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("Identifier")) {
            Identifier = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int MasterData_ProductLanguages_Language_t::tableColumnCount() const
{
    return 2;
}

QVariant MasterData_ProductLanguages_Language_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "MasterData_ProductLanguages_Language";
        }
        if (qualifiedName == QString("Identifier")) {
            return Identifier;
        }
        break;
    }
    return QVariant();
}

QVariant MasterData_ProductLanguages_Language_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Identifier";
            }
        }
    }
    return QVariant();
}

QVariant MasterData_ProductLanguages_Language_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return Identifier;
    case Qt::DecorationRole:
        return QIcon::fromTheme("MasterData_ProductLanguages_Language");
    }
    return QVariant();
}

MasterData_ProductLanguages_Language_t * make_MasterData_ProductLanguages_Language_t(Base * parent)
{
    return new MasterData_ProductLanguages_Language_t(parent);
}

} // namespace knx
} // namespace v12
} // namespace Project
