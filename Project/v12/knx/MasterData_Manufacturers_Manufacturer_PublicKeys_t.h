/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v12/knx/MasterData_Manufacturers_Manufacturer_PublicKeys_PublicKey_t.h>
#include <Project/xs/ID.h>

namespace Project {
namespace v12 {
namespace knx {

class MasterData_Manufacturers_Manufacturer_PublicKeys_t : public Base
{
    Q_OBJECT

public:
    explicit MasterData_Manufacturers_Manufacturer_PublicKeys_t(Base * parent = nullptr);
    virtual ~MasterData_Manufacturers_Manufacturer_PublicKeys_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* elements */
    QMap<xs::ID, MasterData_Manufacturers_Manufacturer_PublicKeys_PublicKey_t *> PublicKey; // key: Id
};

MasterData_Manufacturers_Manufacturer_PublicKeys_t * make_MasterData_Manufacturers_Manufacturer_PublicKeys_t(Base * parent);

} // namespace knx
} // namespace v12
} // namespace Project
