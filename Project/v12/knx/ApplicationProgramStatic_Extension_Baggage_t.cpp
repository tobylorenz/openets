/* This file is generated. */

#include <Project/v12/knx/ApplicationProgramStatic_Extension_Baggage_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v12/knx/KNX_t.h>
#include <Project/v12/knx/ManufacturerData_Manufacturer_Baggages_Baggage_t.h>

namespace Project {
namespace v12 {
namespace knx {

ApplicationProgramStatic_Extension_Baggage_t::ApplicationProgramStatic_Extension_Baggage_t(Base * parent) :
    Base(parent)
{
}

ApplicationProgramStatic_Extension_Baggage_t::~ApplicationProgramStatic_Extension_Baggage_t()
{
}

void ApplicationProgramStatic_Extension_Baggage_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("RefId")) {
            RefId = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ApplicationProgramStatic_Extension_Baggage_t::tableColumnCount() const
{
    return 2;
}

QVariant ApplicationProgramStatic_Extension_Baggage_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ApplicationProgramStatic_Extension_Baggage";
        }
        if (qualifiedName == QString("RefId")) {
            return RefId;
        }
        break;
    }
    return QVariant();
}

QVariant ApplicationProgramStatic_Extension_Baggage_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "RefId";
            }
        }
    }
    return QVariant();
}

QVariant ApplicationProgramStatic_Extension_Baggage_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "Baggage";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ApplicationProgramStatic_Extension_Baggage");
    }
    return QVariant();
}

ManufacturerData_Manufacturer_Baggages_Baggage_t * ApplicationProgramStatic_Extension_Baggage_t::getBaggage() const
{
    if (RefId.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<ManufacturerData_Manufacturer_Baggages_Baggage_t *>(knx->ids[RefId]);
}

ApplicationProgramStatic_Extension_Baggage_t * make_ApplicationProgramStatic_Extension_Baggage_t(Base * parent)
{
    return new ApplicationProgramStatic_Extension_Baggage_t(parent);
}

} // namespace knx
} // namespace v12
} // namespace Project
