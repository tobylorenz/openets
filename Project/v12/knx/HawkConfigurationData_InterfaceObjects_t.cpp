/* This file is generated. */

#include <Project/v12/knx/HawkConfigurationData_InterfaceObjects_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v12/knx/KNX_t.h>

namespace Project {
namespace v12 {
namespace knx {

HawkConfigurationData_InterfaceObjects_t::HawkConfigurationData_InterfaceObjects_t(Base * parent) :
    Base(parent)
{
}

HawkConfigurationData_InterfaceObjects_t::~HawkConfigurationData_InterfaceObjects_t()
{
}

void HawkConfigurationData_InterfaceObjects_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("InterfaceObject")) {
            auto * newInterfaceObject = make_HawkConfigurationData_InterfaceObjects_InterfaceObject_t(this);
            newInterfaceObject->read(reader);
            InterfaceObject.append(newInterfaceObject);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int HawkConfigurationData_InterfaceObjects_t::tableColumnCount() const
{
    return 1;
}

QVariant HawkConfigurationData_InterfaceObjects_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "HawkConfigurationData_InterfaceObjects";
        }
        break;
    }
    return QVariant();
}

QVariant HawkConfigurationData_InterfaceObjects_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant HawkConfigurationData_InterfaceObjects_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "InterfaceObjects";
    case Qt::DecorationRole:
        return QIcon::fromTheme("HawkConfigurationData_InterfaceObjects");
    }
    return QVariant();
}

HawkConfigurationData_InterfaceObjects_t * make_HawkConfigurationData_InterfaceObjects_t(Base * parent)
{
    return new HawkConfigurationData_InterfaceObjects_t(parent);
}

} // namespace knx
} // namespace v12
} // namespace Project
