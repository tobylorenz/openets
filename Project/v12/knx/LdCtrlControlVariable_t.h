/* This file is generated. */

#pragma once

#include <Project/xs/String.h>

namespace Project {
namespace v12 {
namespace knx {

using LdCtrlControlVariable_t = xs::String;

} // namespace knx
} // namespace v12
} // namespace Project
