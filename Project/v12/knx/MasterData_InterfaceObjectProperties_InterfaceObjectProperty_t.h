/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v12/knx/IDREF.h>
#include <Project/v12/knx/IDREFS.h>
#include <Project/v12/knx/LanguageDependentString255_t.h>
#include <Project/v12/knx/MasterData_PropertyDataTypes_PropertyDataType_t.h>
#include <Project/v12/knx/String255_t.h>
#include <Project/xs/ID.h>
#include <Project/xs/Boolean.h>
#include <Project/xs/UnsignedInt.h>

namespace Project {
namespace v12 {
namespace knx {

/* forward declarations */
class MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_t;
class MasterData_InterfaceObjectTypes_InterfaceObjectType_t;

class MasterData_InterfaceObjectProperties_InterfaceObjectProperty_t : public Base
{
    Q_OBJECT

public:
    explicit MasterData_InterfaceObjectProperties_InterfaceObjectProperty_t(Base * parent = nullptr);
    virtual ~MasterData_InterfaceObjectProperties_InterfaceObjectProperty_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::ID Id{};
    xs::UnsignedInt Number{};
    IDREF ObjectType{};
    String255_t Name{};
    LanguageDependentString255_t Text{};
    IDREFS PDT{};
    IDREF DPT{};
    xs::Boolean Array{"false"};

    /* getters */
    MasterData_InterfaceObjectTypes_InterfaceObjectType_t * getInterfaceObjectType() const; // attribute: ObjectType
    QList<MasterData_PropertyDataTypes_PropertyDataType_t *> getPropertyDataType() const; // attribute: PDT
    MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_t * getDatapointType() const; // attribute: DPT
};

MasterData_InterfaceObjectProperties_InterfaceObjectProperty_t * make_MasterData_InterfaceObjectProperties_InterfaceObjectProperty_t(Base * parent);

} // namespace knx
} // namespace v12
} // namespace Project
