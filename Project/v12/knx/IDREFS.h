/* This file is generated. */

#pragma once

#include <Project/v12/knx/IDREF.h>
#include <Project/xs/List.h>

namespace Project {
namespace v12 {
namespace knx {

using IDREFS = xs::List<IDREF>;

} // namespace knx
} // namespace v12
} // namespace Project
