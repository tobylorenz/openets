/* This file is generated. */

#include <Project/v12/knx/ComObjectParameterChoose_when_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v12/knx/Assign_t.h>
#include <Project/v12/knx/BinaryDataRef_t.h>
#include <Project/v12/knx/ComObjectParameterBlock_t.h>
#include <Project/v12/knx/ComObjectParameterChoose_t.h>
#include <Project/v12/knx/ComObjectRefRef_t.h>
#include <Project/v12/knx/KNX_t.h>
#include <Project/v12/knx/ParameterRefRef_t.h>
#include <Project/v12/knx/ParameterSeparator_t.h>
#include <Project/v12/knx/Rename_t.h>

namespace Project {
namespace v12 {
namespace knx {

ComObjectParameterChoose_when_t::ComObjectParameterChoose_when_t(Base * parent) :
    Base(parent)
{
}

ComObjectParameterChoose_when_t::~ComObjectParameterChoose_when_t()
{
}

void ComObjectParameterChoose_when_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("test")) {
            Test = attribute.value().toString();
            continue;
        }
        if (name == QString("default")) {
            Default = attribute.value().toString();
            continue;
        }
        if (name == QString("InternalDescription")) {
            InternalDescription = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("ParameterBlock")) {
            auto * ParameterBlock = make_ComObjectParameterBlock_t(this);
            ParameterBlock->read(reader);
            continue;
        }
        if (reader.name() == QString("ParameterSeparator")) {
            auto * ParameterSeparator = make_ParameterSeparator_t(this);
            ParameterSeparator->read(reader);
            continue;
        }
        if (reader.name() == QString("ParameterRefRef")) {
            auto * ParameterRefRef = make_ParameterRefRef_t(this);
            ParameterRefRef->read(reader);
            continue;
        }
        if (reader.name() == QString("choose")) {
            auto * Choose = make_ComObjectParameterChoose_t(this);
            Choose->read(reader);
            continue;
        }
        if (reader.name() == QString("BinaryDataRef")) {
            auto * BinaryDataRef = make_BinaryDataRef_t(this);
            BinaryDataRef->read(reader);
            continue;
        }
        if (reader.name() == QString("ComObjectRefRef")) {
            auto * ComObjectRefRef = make_ComObjectRefRef_t(this);
            ComObjectRefRef->read(reader);
            continue;
        }
        if (reader.name() == QString("Assign")) {
            auto * Assign = make_Assign_t(this);
            Assign->read(reader);
            continue;
        }
        if (reader.name() == QString("Rename")) {
            auto * Rename = make_Rename_t(this);
            Rename->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ComObjectParameterChoose_when_t::tableColumnCount() const
{
    return 4;
}

QVariant ComObjectParameterChoose_when_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ComObjectParameterChoose_when";
        }
        if (qualifiedName == QString("test")) {
            return Test;
        }
        if (qualifiedName == QString("default")) {
            return Default;
        }
        if (qualifiedName == QString("InternalDescription")) {
            return InternalDescription;
        }
        break;
    }
    return QVariant();
}

QVariant ComObjectParameterChoose_when_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Test";
            case 2:
                return "Default";
            case 3:
                return "InternalDescription";
            }
        }
    }
    return QVariant();
}

QVariant ComObjectParameterChoose_when_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "when";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ComObjectParameterChoose_when");
    }
    return QVariant();
}

ComObjectParameterChoose_when_t * make_ComObjectParameterChoose_when_t(Base * parent)
{
    return new ComObjectParameterChoose_when_t(parent);
}

} // namespace knx
} // namespace v12
} // namespace Project
