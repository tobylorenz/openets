/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v12/knx/AddinData_t.h>

namespace Project {
namespace v12 {
namespace knx {

class Project_AddinData_t : public Base
{
    Q_OBJECT

public:
    explicit Project_AddinData_t(Base * parent = nullptr);
    virtual ~Project_AddinData_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* elements */
    QMap<xs::ID, AddinData_t *> AddinData; // key: AddinId
};

Project_AddinData_t * make_Project_AddinData_t(Base * parent);

} // namespace knx
} // namespace v12
} // namespace Project
