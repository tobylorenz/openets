/* This file is generated. */

#include <Project/v12/knx/ApplicationProgramStatic_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v12/knx/ApplicationProgramStatic_AddressTable_t.h>
#include <Project/v12/knx/ApplicationProgramStatic_AssociationTable_t.h>
#include <Project/v12/knx/ApplicationProgramStatic_BinaryData_t.h>
#include <Project/v12/knx/ApplicationProgramStatic_Code_t.h>
#include <Project/v12/knx/ApplicationProgramStatic_ComObjectRefs_t.h>
#include <Project/v12/knx/ApplicationProgramStatic_ComObjectTable_t.h>
#include <Project/v12/knx/ApplicationProgramStatic_DeviceCompare_t.h>
#include <Project/v12/knx/ApplicationProgramStatic_Extension_t.h>
#include <Project/v12/knx/ApplicationProgramStatic_FixupList_t.h>
#include <Project/v12/knx/ApplicationProgramStatic_Options_t.h>
#include <Project/v12/knx/ApplicationProgramStatic_ParameterCalculations_t.h>
#include <Project/v12/knx/ApplicationProgramStatic_ParameterRefs_t.h>
#include <Project/v12/knx/ApplicationProgramStatic_ParameterTypes_t.h>
#include <Project/v12/knx/ApplicationProgramStatic_Parameters_t.h>
#include <Project/v12/knx/KNX_t.h>
#include <Project/v12/knx/LoadProcedures_t.h>

namespace Project {
namespace v12 {
namespace knx {

ApplicationProgramStatic_t::ApplicationProgramStatic_t(Base * parent) :
    Base(parent)
{
}

ApplicationProgramStatic_t::~ApplicationProgramStatic_t()
{
}

void ApplicationProgramStatic_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("Code")) {
            if (!Code) {
                Code = make_ApplicationProgramStatic_Code_t(this);
            }
            Code->read(reader);
            continue;
        }
        if (reader.name() == QString("ParameterTypes")) {
            if (!ParameterTypes) {
                ParameterTypes = make_ApplicationProgramStatic_ParameterTypes_t(this);
            }
            ParameterTypes->read(reader);
            continue;
        }
        if (reader.name() == QString("Parameters")) {
            if (!Parameters) {
                Parameters = make_ApplicationProgramStatic_Parameters_t(this);
            }
            Parameters->read(reader);
            continue;
        }
        if (reader.name() == QString("ParameterRefs")) {
            if (!ParameterRefs) {
                ParameterRefs = make_ApplicationProgramStatic_ParameterRefs_t(this);
            }
            ParameterRefs->read(reader);
            continue;
        }
        if (reader.name() == QString("ParameterCalculations")) {
            if (!ParameterCalculations) {
                ParameterCalculations = make_ApplicationProgramStatic_ParameterCalculations_t(this);
            }
            ParameterCalculations->read(reader);
            continue;
        }
        if (reader.name() == QString("ComObjectTable")) {
            if (!ComObjectTable) {
                ComObjectTable = make_ApplicationProgramStatic_ComObjectTable_t(this);
            }
            ComObjectTable->read(reader);
            continue;
        }
        if (reader.name() == QString("ComObjectRefs")) {
            if (!ComObjectRefs) {
                ComObjectRefs = make_ApplicationProgramStatic_ComObjectRefs_t(this);
            }
            ComObjectRefs->read(reader);
            continue;
        }
        if (reader.name() == QString("AddressTable")) {
            if (!AddressTable) {
                AddressTable = make_ApplicationProgramStatic_AddressTable_t(this);
            }
            AddressTable->read(reader);
            continue;
        }
        if (reader.name() == QString("AssociationTable")) {
            if (!AssociationTable) {
                AssociationTable = make_ApplicationProgramStatic_AssociationTable_t(this);
            }
            AssociationTable->read(reader);
            continue;
        }
        if (reader.name() == QString("FixupList")) {
            if (!FixupList) {
                FixupList = make_ApplicationProgramStatic_FixupList_t(this);
            }
            FixupList->read(reader);
            continue;
        }
        if (reader.name() == QString("LoadProcedures")) {
            if (!LoadProcedures) {
                LoadProcedures = make_LoadProcedures_t(this);
            }
            LoadProcedures->read(reader);
            continue;
        }
        if (reader.name() == QString("Extension")) {
            if (!Extension) {
                Extension = make_ApplicationProgramStatic_Extension_t(this);
            }
            Extension->read(reader);
            continue;
        }
        if (reader.name() == QString("BinaryData")) {
            if (!BinaryData) {
                BinaryData = make_ApplicationProgramStatic_BinaryData_t(this);
            }
            BinaryData->read(reader);
            continue;
        }
        if (reader.name() == QString("DeviceCompare")) {
            if (!DeviceCompare) {
                DeviceCompare = make_ApplicationProgramStatic_DeviceCompare_t(this);
            }
            DeviceCompare->read(reader);
            continue;
        }
        if (reader.name() == QString("Options")) {
            if (!Options) {
                Options = make_ApplicationProgramStatic_Options_t(this);
            }
            Options->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ApplicationProgramStatic_t::tableColumnCount() const
{
    return 1;
}

QVariant ApplicationProgramStatic_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ApplicationProgramStatic";
        }
        break;
    }
    return QVariant();
}

QVariant ApplicationProgramStatic_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant ApplicationProgramStatic_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "ApplicationProgramStatic";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ApplicationProgramStatic");
    }
    return QVariant();
}

ApplicationProgramStatic_t * make_ApplicationProgramStatic_t(Base * parent)
{
    return new ApplicationProgramStatic_t(parent);
}

} // namespace knx
} // namespace v12
} // namespace Project
