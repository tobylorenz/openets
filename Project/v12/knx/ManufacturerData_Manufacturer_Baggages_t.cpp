/* This file is generated. */

#include <Project/v12/knx/ManufacturerData_Manufacturer_Baggages_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v12/knx/KNX_t.h>

namespace Project {
namespace v12 {
namespace knx {

ManufacturerData_Manufacturer_Baggages_t::ManufacturerData_Manufacturer_Baggages_t(Base * parent) :
    Base(parent)
{
}

ManufacturerData_Manufacturer_Baggages_t::~ManufacturerData_Manufacturer_Baggages_t()
{
}

void ManufacturerData_Manufacturer_Baggages_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("Baggage")) {
            QString newId = reader.attributes().value("Id").toString();
            Q_ASSERT(!newId.isEmpty());
            ManufacturerData_Manufacturer_Baggages_Baggage_t * newBaggage;
            if (Baggage.contains(newId)) {
                newBaggage = Baggage[newId];
            } else {
                newBaggage = make_ManufacturerData_Manufacturer_Baggages_Baggage_t(this);
                Baggage[newId] = newBaggage;
            }
            newBaggage->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ManufacturerData_Manufacturer_Baggages_t::tableColumnCount() const
{
    return 1;
}

QVariant ManufacturerData_Manufacturer_Baggages_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ManufacturerData_Manufacturer_Baggages";
        }
        break;
    }
    return QVariant();
}

QVariant ManufacturerData_Manufacturer_Baggages_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant ManufacturerData_Manufacturer_Baggages_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "Baggages";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ManufacturerData_Manufacturer_Baggages");
    }
    return QVariant();
}

ManufacturerData_Manufacturer_Baggages_t * make_ManufacturerData_Manufacturer_Baggages_t(Base * parent)
{
    return new ManufacturerData_Manufacturer_Baggages_t(parent);
}

} // namespace knx
} // namespace v12
} // namespace Project
