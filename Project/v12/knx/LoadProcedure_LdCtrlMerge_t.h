/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/xs/String.h>
#include <Project/xs/UnsignedByte.h>

namespace Project {
namespace v12 {
namespace knx {

class LoadProcedure_LdCtrlMerge_t : public Base
{
    Q_OBJECT

public:
    explicit LoadProcedure_LdCtrlMerge_t(Base * parent = nullptr);
    virtual ~LoadProcedure_LdCtrlMerge_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::UnsignedByte MergeId{};
    xs::String InternalDescription{};
};

LoadProcedure_LdCtrlMerge_t * make_LoadProcedure_LdCtrlMerge_t(Base * parent);

} // namespace knx
} // namespace v12
} // namespace Project
