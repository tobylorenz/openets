/* This file is generated. */

#include <Project/v12/knx/MasterData_FunctionalBlocks_FunctionalBlock_Parameters_Parameter_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v12/knx/KNX_t.h>
#include <Project/v12/knx/MasterData_InterfaceObjectProperties_InterfaceObjectProperty_t.h>

namespace Project {
namespace v12 {
namespace knx {

MasterData_FunctionalBlocks_FunctionalBlock_Parameters_Parameter_t::MasterData_FunctionalBlocks_FunctionalBlock_Parameters_Parameter_t(Base * parent) :
    Base(parent)
{
}

MasterData_FunctionalBlocks_FunctionalBlock_Parameters_Parameter_t::~MasterData_FunctionalBlocks_FunctionalBlock_Parameters_Parameter_t()
{
}

void MasterData_FunctionalBlocks_FunctionalBlock_Parameters_Parameter_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("Property")) {
            Property = attribute.value().toString();
            continue;
        }
        if (name == QString("Description")) {
            Description = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int MasterData_FunctionalBlocks_FunctionalBlock_Parameters_Parameter_t::tableColumnCount() const
{
    return 3;
}

QVariant MasterData_FunctionalBlocks_FunctionalBlock_Parameters_Parameter_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "MasterData_FunctionalBlocks_FunctionalBlock_Parameters_Parameter";
        }
        if (qualifiedName == QString("Property")) {
            return Property;
        }
        if (qualifiedName == QString("Description")) {
            return Description;
        }
        break;
    }
    return QVariant();
}

QVariant MasterData_FunctionalBlocks_FunctionalBlock_Parameters_Parameter_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Property";
            case 2:
                return "Description";
            }
        }
    }
    return QVariant();
}

QVariant MasterData_FunctionalBlocks_FunctionalBlock_Parameters_Parameter_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "Parameter";
    case Qt::DecorationRole:
        return QIcon::fromTheme("MasterData_FunctionalBlocks_FunctionalBlock_Parameters_Parameter");
    }
    return QVariant();
}

MasterData_InterfaceObjectProperties_InterfaceObjectProperty_t * MasterData_FunctionalBlocks_FunctionalBlock_Parameters_Parameter_t::getInterfaceObjectProperty() const
{
    if (Property.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<MasterData_InterfaceObjectProperties_InterfaceObjectProperty_t *>(knx->ids[Property]);
}

MasterData_FunctionalBlocks_FunctionalBlock_Parameters_Parameter_t * make_MasterData_FunctionalBlocks_FunctionalBlock_Parameters_Parameter_t(Base * parent)
{
    return new MasterData_FunctionalBlocks_FunctionalBlock_Parameters_Parameter_t(parent);
}

} // namespace knx
} // namespace v12
} // namespace Project
