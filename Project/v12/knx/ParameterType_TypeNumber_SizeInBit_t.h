/* This file is generated. */

#pragma once

#include <Project/xs/UnsignedInt.h>

namespace Project {
namespace v12 {
namespace knx {

using ParameterType_TypeNumber_SizeInBit_t = xs::UnsignedInt;

} // namespace knx
} // namespace v12
} // namespace Project
