/* This file is generated. */

#include <Project/v12/knx/ApplicationProgramDynamic_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v12/knx/ApplicationProgramChannel_t.h>
#include <Project/v12/knx/ApplicationProgramDynamic_ChannelIndependentBlock_t.h>
#include <Project/v12/knx/DependentChannelChoose_t.h>
#include <Project/v12/knx/KNX_t.h>

namespace Project {
namespace v12 {
namespace knx {

ApplicationProgramDynamic_t::ApplicationProgramDynamic_t(Base * parent) :
    Base(parent)
{
}

ApplicationProgramDynamic_t::~ApplicationProgramDynamic_t()
{
}

void ApplicationProgramDynamic_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("ChannelIndependentBlock")) {
            auto * ChannelIndependentBlock = make_ApplicationProgramDynamic_ChannelIndependentBlock_t(this);
            ChannelIndependentBlock->read(reader);
            continue;
        }
        if (reader.name() == QString("Channel")) {
            auto * Channel = make_ApplicationProgramChannel_t(this);
            Channel->read(reader);
            continue;
        }
        if (reader.name() == QString("choose")) {
            auto * Choose = make_DependentChannelChoose_t(this);
            Choose->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ApplicationProgramDynamic_t::tableColumnCount() const
{
    return 1;
}

QVariant ApplicationProgramDynamic_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ApplicationProgramDynamic";
        }
        break;
    }
    return QVariant();
}

QVariant ApplicationProgramDynamic_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant ApplicationProgramDynamic_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "ApplicationProgramDynamic";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ApplicationProgramDynamic");
    }
    return QVariant();
}

ApplicationProgramDynamic_t * make_ApplicationProgramDynamic_t(Base * parent)
{
    return new ApplicationProgramDynamic_t(parent);
}

} // namespace knx
} // namespace v12
} // namespace Project
