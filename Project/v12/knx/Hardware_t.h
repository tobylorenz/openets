/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v12/knx/HardwareVersionNumber.h>
#include <Project/v12/knx/IDREF.h>
#include <Project/v12/knx/RFDeviceMode_t.h>
#include <Project/v12/knx/String255_t.h>
#include <Project/v12/knx/String50_t.h>
#include <Project/xs/ID.h>
#include <Project/xs/Boolean.h>
#include <Project/xs/Float.h>
#include <Project/xs/String.h>
#include <Project/xs/UnsignedShort.h>

namespace Project {
namespace v12 {
namespace knx {

/* forward declarations */
class Hardware_Hardware2Programs_t;
class Hardware_Products_t;
class MasterData_Manufacturers_Manufacturer_t;

class Hardware_t : public Base
{
    Q_OBJECT

public:
    explicit Hardware_t(Base * parent = nullptr);
    virtual ~Hardware_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::ID Id{};
    String255_t Name{};
    String50_t SerialNumber{};
    HardwareVersionNumber VersionNumber{};
    xs::Float BusCurrent{};
    xs::Boolean IsAccessory{"false"};
    xs::Boolean HasIndividualAddress{};
    xs::Boolean HasApplicationProgram{};
    xs::Boolean HasApplicationProgram2{"false"};
    xs::Boolean IsPowerSupply{"false"};
    xs::Boolean IsChoke{"false"};
    xs::Boolean IsCoupler{"false"};
    xs::Boolean IsPowerLineRepeater{"false"};
    xs::Boolean IsPowerLineSignalFilter{"false"};
    xs::Boolean IsCable{"false"};
    xs::Boolean IsIPEnabled{"false"};
    xs::Boolean IsRFRetransmitter{"false"};
    xs::Boolean RuntimeUnidirectional{"false"};
    IDREF OriginalManufacturer{};
    RFDeviceMode_t RFDeviceMode{"Ready"};
    xs::Boolean NoDownloadWithoutPlugin{"false"};
    xs::UnsignedShort NonRegRelevantDataVersion{"0"};
    xs::String InternalDescription{};

    /* elements */
    Hardware_Products_t * Products{};
    Hardware_Hardware2Programs_t * Hardware2Programs{};

    /* getters */
    MasterData_Manufacturers_Manufacturer_t * getOriginalManufacturer() const; // attribute: OriginalManufacturer
};

Hardware_t * make_Hardware_t(Base * parent);

} // namespace knx
} // namespace v12
} // namespace Project
