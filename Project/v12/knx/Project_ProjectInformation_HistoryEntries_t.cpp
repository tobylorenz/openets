/* This file is generated. */

#include <Project/v12/knx/Project_ProjectInformation_HistoryEntries_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v12/knx/KNX_t.h>

namespace Project {
namespace v12 {
namespace knx {

Project_ProjectInformation_HistoryEntries_t::Project_ProjectInformation_HistoryEntries_t(Base * parent) :
    Base(parent)
{
}

Project_ProjectInformation_HistoryEntries_t::~Project_ProjectInformation_HistoryEntries_t()
{
}

void Project_ProjectInformation_HistoryEntries_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("HistoryEntry")) {
            auto * newHistoryEntry = make_Project_ProjectInformation_HistoryEntries_HistoryEntry_t(this);
            newHistoryEntry->read(reader);
            HistoryEntry.append(newHistoryEntry);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int Project_ProjectInformation_HistoryEntries_t::tableColumnCount() const
{
    return 1;
}

QVariant Project_ProjectInformation_HistoryEntries_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "Project_ProjectInformation_HistoryEntries";
        }
        break;
    }
    return QVariant();
}

QVariant Project_ProjectInformation_HistoryEntries_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant Project_ProjectInformation_HistoryEntries_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "HistoryEntries";
    case Qt::DecorationRole:
        return QIcon::fromTheme("Project_ProjectInformation_HistoryEntries");
    }
    return QVariant();
}

Project_ProjectInformation_HistoryEntries_t * make_Project_ProjectInformation_HistoryEntries_t(Base * parent)
{
    return new Project_ProjectInformation_HistoryEntries_t(parent);
}

} // namespace knx
} // namespace v12
} // namespace Project
