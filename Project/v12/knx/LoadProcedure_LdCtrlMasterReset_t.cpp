/* This file is generated. */

#include <Project/v12/knx/LoadProcedure_LdCtrlMasterReset_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v12/knx/KNX_t.h>

namespace Project {
namespace v12 {
namespace knx {

LoadProcedure_LdCtrlMasterReset_t::LoadProcedure_LdCtrlMasterReset_t(Base * parent) :
    Base(parent)
{
}

LoadProcedure_LdCtrlMasterReset_t::~LoadProcedure_LdCtrlMasterReset_t()
{
}

void LoadProcedure_LdCtrlMasterReset_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("EraseCode")) {
            EraseCode = attribute.value().toString();
            continue;
        }
        if (name == QString("ChannelNumber")) {
            ChannelNumber = attribute.value().toString();
            continue;
        }
        if (name == QString("AppliesTo")) {
            AppliesTo = attribute.value().toString();
            continue;
        }
        if (name == QString("InternalDescription")) {
            InternalDescription = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int LoadProcedure_LdCtrlMasterReset_t::tableColumnCount() const
{
    return 5;
}

QVariant LoadProcedure_LdCtrlMasterReset_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "LoadProcedure_LdCtrlMasterReset";
        }
        if (qualifiedName == QString("EraseCode")) {
            return EraseCode;
        }
        if (qualifiedName == QString("ChannelNumber")) {
            return ChannelNumber;
        }
        if (qualifiedName == QString("AppliesTo")) {
            return AppliesTo;
        }
        if (qualifiedName == QString("InternalDescription")) {
            return InternalDescription;
        }
        break;
    }
    return QVariant();
}

QVariant LoadProcedure_LdCtrlMasterReset_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "EraseCode";
            case 2:
                return "ChannelNumber";
            case 3:
                return "AppliesTo";
            case 4:
                return "InternalDescription";
            }
        }
    }
    return QVariant();
}

QVariant LoadProcedure_LdCtrlMasterReset_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "LdCtrlMasterReset";
    case Qt::DecorationRole:
        return QIcon::fromTheme("LoadProcedure_LdCtrlMasterReset");
    }
    return QVariant();
}

LoadProcedure_LdCtrlMasterReset_t * make_LoadProcedure_LdCtrlMasterReset_t(Base * parent)
{
    return new LoadProcedure_LdCtrlMasterReset_t(parent);
}

} // namespace knx
} // namespace v12
} // namespace Project
