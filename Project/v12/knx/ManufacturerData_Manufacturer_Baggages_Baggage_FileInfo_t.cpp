/* This file is generated. */

#include <Project/v12/knx/ManufacturerData_Manufacturer_Baggages_Baggage_FileInfo_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v12/knx/KNX_t.h>

namespace Project {
namespace v12 {
namespace knx {

ManufacturerData_Manufacturer_Baggages_Baggage_FileInfo_t::ManufacturerData_Manufacturer_Baggages_Baggage_FileInfo_t(Base * parent) :
    Base(parent)
{
}

ManufacturerData_Manufacturer_Baggages_Baggage_FileInfo_t::~ManufacturerData_Manufacturer_Baggages_Baggage_FileInfo_t()
{
}

void ManufacturerData_Manufacturer_Baggages_Baggage_FileInfo_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("Version")) {
            Version = attribute.value().toString();
            continue;
        }
        if (name == QString("TimeInfo")) {
            TimeInfo = attribute.value().toString();
            continue;
        }
        if (name == QString("Hidden")) {
            Hidden = attribute.value().toString();
            continue;
        }
        if (name == QString("ReadOnly")) {
            ReadOnly = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ManufacturerData_Manufacturer_Baggages_Baggage_FileInfo_t::tableColumnCount() const
{
    return 5;
}

QVariant ManufacturerData_Manufacturer_Baggages_Baggage_FileInfo_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ManufacturerData_Manufacturer_Baggages_Baggage_FileInfo";
        }
        if (qualifiedName == QString("Version")) {
            return Version;
        }
        if (qualifiedName == QString("TimeInfo")) {
            return TimeInfo;
        }
        if (qualifiedName == QString("Hidden")) {
            return Hidden;
        }
        if (qualifiedName == QString("ReadOnly")) {
            return ReadOnly;
        }
        break;
    }
    return QVariant();
}

QVariant ManufacturerData_Manufacturer_Baggages_Baggage_FileInfo_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Version";
            case 2:
                return "TimeInfo";
            case 3:
                return "Hidden";
            case 4:
                return "ReadOnly";
            }
        }
    }
    return QVariant();
}

QVariant ManufacturerData_Manufacturer_Baggages_Baggage_FileInfo_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "FileInfo";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ManufacturerData_Manufacturer_Baggages_Baggage_FileInfo");
    }
    return QVariant();
}

ManufacturerData_Manufacturer_Baggages_Baggage_FileInfo_t * make_ManufacturerData_Manufacturer_Baggages_Baggage_FileInfo_t(Base * parent)
{
    return new ManufacturerData_Manufacturer_Baggages_Baggage_FileInfo_t(parent);
}

} // namespace knx
} // namespace v12
} // namespace Project
