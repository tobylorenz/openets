/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v12/knx/DependentChannelChoose_when_t.h>
#include <Project/v12/knx/IDREF.h>
#include <Project/xs/String.h>

namespace Project {
namespace v12 {
namespace knx {

/* forward declarations */
class ParameterRef_t;

class DependentChannelChoose_t : public Base
{
    Q_OBJECT

public:
    explicit DependentChannelChoose_t(Base * parent = nullptr);
    virtual ~DependentChannelChoose_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    IDREF ParamRefId{};
    xs::String InternalDescription{};

    /* elements */
    QVector<DependentChannelChoose_when_t *> When;

    /* getters */
    ParameterRef_t * getParameterRef() const; // attribute: ParamRefId
};

DependentChannelChoose_t * make_DependentChannelChoose_t(Base * parent);

} // namespace knx
} // namespace v12
} // namespace Project
