/* This file is generated. */

#include <Project/v12/knx/ApplicationProgramStatic_ComObjectRefs_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v12/knx/KNX_t.h>

namespace Project {
namespace v12 {
namespace knx {

ApplicationProgramStatic_ComObjectRefs_t::ApplicationProgramStatic_ComObjectRefs_t(Base * parent) :
    Base(parent)
{
}

ApplicationProgramStatic_ComObjectRefs_t::~ApplicationProgramStatic_ComObjectRefs_t()
{
}

void ApplicationProgramStatic_ComObjectRefs_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("ComObjectRef")) {
            QString newId = reader.attributes().value("Id").toString();
            Q_ASSERT(!newId.isEmpty());
            ComObjectRef_t * newComObjectRef;
            if (ComObjectRef.contains(newId)) {
                newComObjectRef = ComObjectRef[newId];
            } else {
                newComObjectRef = make_ComObjectRef_t(this);
                ComObjectRef[newId] = newComObjectRef;
            }
            newComObjectRef->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ApplicationProgramStatic_ComObjectRefs_t::tableColumnCount() const
{
    return 1;
}

QVariant ApplicationProgramStatic_ComObjectRefs_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ApplicationProgramStatic_ComObjectRefs";
        }
        break;
    }
    return QVariant();
}

QVariant ApplicationProgramStatic_ComObjectRefs_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant ApplicationProgramStatic_ComObjectRefs_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "ComObjectRefs";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ApplicationProgramStatic_ComObjectRefs");
    }
    return QVariant();
}

ApplicationProgramStatic_ComObjectRefs_t * make_ApplicationProgramStatic_ComObjectRefs_t(Base * parent)
{
    return new ApplicationProgramStatic_ComObjectRefs_t(parent);
}

} // namespace knx
} // namespace v12
} // namespace Project
