/* This file is generated. */

#include <Project/v12/knx/LanguageData_TranslationUnit_TranslationElement_Translation_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v12/knx/KNX_t.h>

namespace Project {
namespace v12 {
namespace knx {

LanguageData_TranslationUnit_TranslationElement_Translation_t::LanguageData_TranslationUnit_TranslationElement_Translation_t(Base * parent) :
    Base(parent)
{
}

LanguageData_TranslationUnit_TranslationElement_Translation_t::~LanguageData_TranslationUnit_TranslationElement_Translation_t()
{
}

void LanguageData_TranslationUnit_TranslationElement_Translation_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("AttributeName")) {
            AttributeName = attribute.value().toString();
            continue;
        }
        if (name == QString("Text")) {
            Text = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int LanguageData_TranslationUnit_TranslationElement_Translation_t::tableColumnCount() const
{
    return 3;
}

QVariant LanguageData_TranslationUnit_TranslationElement_Translation_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "LanguageData_TranslationUnit_TranslationElement_Translation";
        }
        if (qualifiedName == QString("AttributeName")) {
            return AttributeName;
        }
        if (qualifiedName == QString("Text")) {
            return Text;
        }
        break;
    }
    return QVariant();
}

QVariant LanguageData_TranslationUnit_TranslationElement_Translation_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "AttributeName";
            case 2:
                return "Text";
            }
        }
    }
    return QVariant();
}

QVariant LanguageData_TranslationUnit_TranslationElement_Translation_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "Translation";
    case Qt::DecorationRole:
        return QIcon::fromTheme("LanguageData_TranslationUnit_TranslationElement_Translation");
    }
    return QVariant();
}

LanguageData_TranslationUnit_TranslationElement_Translation_t * make_LanguageData_TranslationUnit_TranslationElement_Translation_t(Base * parent)
{
    return new LanguageData_TranslationUnit_TranslationElement_Translation_t(parent);
}

} // namespace knx
} // namespace v12
} // namespace Project
