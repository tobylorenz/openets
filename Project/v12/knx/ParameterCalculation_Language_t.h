/* This file is generated. */

#pragma once

#include <Project/xs/String.h>

namespace Project {
namespace v12 {
namespace knx {

using ParameterCalculation_Language_t = xs::String;

} // namespace knx
} // namespace v12
} // namespace Project
