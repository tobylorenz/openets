/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v12/knx/BinaryData_t.h>

namespace Project {
namespace v12 {
namespace knx {

class ApplicationProgramStatic_BinaryData_t : public Base
{
    Q_OBJECT

public:
    explicit ApplicationProgramStatic_BinaryData_t(Base * parent = nullptr);
    virtual ~ApplicationProgramStatic_BinaryData_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* elements */
    QMap<xs::ID, BinaryData_t *> BinaryData; // key: Id
};

ApplicationProgramStatic_BinaryData_t * make_ApplicationProgramStatic_BinaryData_t(Base * parent);

} // namespace knx
} // namespace v12
} // namespace Project
