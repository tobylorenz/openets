/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v12/knx/ApplicationProgramStatic_DeviceCompare_ExcludeMemory_t.h>
#include <Project/v12/knx/ApplicationProgramStatic_DeviceCompare_ExcludeProperty_t.h>
#include <Project/v12/knx/ComTableExpectation_t.h>
#include <Project/v12/knx/IDREF.h>

namespace Project {
namespace v12 {
namespace knx {

class ApplicationProgramStatic_DeviceCompare_t : public Base
{
    Q_OBJECT

public:
    explicit ApplicationProgramStatic_DeviceCompare_t(Base * parent = nullptr);
    virtual ~ApplicationProgramStatic_DeviceCompare_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    ComTableExpectation_t StandardComTablesExpectable{"Try"};

    /* elements */
    QMap<IDREF, ApplicationProgramStatic_DeviceCompare_ExcludeMemory_t *> ExcludeMemory; // key: CodeSegment
    QVector<ApplicationProgramStatic_DeviceCompare_ExcludeProperty_t *> ExcludeProperty;
};

ApplicationProgramStatic_DeviceCompare_t * make_ApplicationProgramStatic_DeviceCompare_t(Base * parent);

} // namespace knx
} // namespace v12
} // namespace Project
