/* This file is generated. */

#pragma once

#include <Project/xs/String.h>

namespace Project {
namespace v12 {
namespace knx {

using ApplicationProgramStatic_Options_TextParameterEncodingSelector_t = xs::String;

} // namespace knx
} // namespace v12
} // namespace Project
