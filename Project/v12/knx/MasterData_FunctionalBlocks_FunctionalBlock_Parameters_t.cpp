/* This file is generated. */

#include <Project/v12/knx/MasterData_FunctionalBlocks_FunctionalBlock_Parameters_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v12/knx/KNX_t.h>
#include <Project/v12/knx/MasterData_InterfaceObjectTypes_InterfaceObjectType_t.h>

namespace Project {
namespace v12 {
namespace knx {

MasterData_FunctionalBlocks_FunctionalBlock_Parameters_t::MasterData_FunctionalBlocks_FunctionalBlock_Parameters_t(Base * parent) :
    Base(parent)
{
}

MasterData_FunctionalBlocks_FunctionalBlock_Parameters_t::~MasterData_FunctionalBlocks_FunctionalBlock_Parameters_t()
{
}

void MasterData_FunctionalBlocks_FunctionalBlock_Parameters_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("ObjectType")) {
            ObjectType = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("Parameter")) {
            QString newProperty = reader.attributes().value("Property").toString();
            Q_ASSERT(!newProperty.isEmpty());
            MasterData_FunctionalBlocks_FunctionalBlock_Parameters_Parameter_t * newParameter;
            if (Parameter.contains(newProperty)) {
                newParameter = Parameter[newProperty];
            } else {
                newParameter = make_MasterData_FunctionalBlocks_FunctionalBlock_Parameters_Parameter_t(this);
                Parameter[newProperty] = newParameter;
            }
            newParameter->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int MasterData_FunctionalBlocks_FunctionalBlock_Parameters_t::tableColumnCount() const
{
    return 2;
}

QVariant MasterData_FunctionalBlocks_FunctionalBlock_Parameters_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "MasterData_FunctionalBlocks_FunctionalBlock_Parameters";
        }
        if (qualifiedName == QString("ObjectType")) {
            return ObjectType;
        }
        break;
    }
    return QVariant();
}

QVariant MasterData_FunctionalBlocks_FunctionalBlock_Parameters_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "ObjectType";
            }
        }
    }
    return QVariant();
}

QVariant MasterData_FunctionalBlocks_FunctionalBlock_Parameters_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "Parameters";
    case Qt::DecorationRole:
        return QIcon::fromTheme("MasterData_FunctionalBlocks_FunctionalBlock_Parameters");
    }
    return QVariant();
}

MasterData_InterfaceObjectTypes_InterfaceObjectType_t * MasterData_FunctionalBlocks_FunctionalBlock_Parameters_t::getInterfaceObjectType() const
{
    if (ObjectType.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<MasterData_InterfaceObjectTypes_InterfaceObjectType_t *>(knx->ids[ObjectType]);
}

MasterData_FunctionalBlocks_FunctionalBlock_Parameters_t * make_MasterData_FunctionalBlocks_FunctionalBlock_Parameters_t(Base * parent)
{
    return new MasterData_FunctionalBlocks_FunctionalBlock_Parameters_t(parent);
}

} // namespace knx
} // namespace v12
} // namespace Project
