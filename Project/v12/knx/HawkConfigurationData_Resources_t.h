/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v12/knx/HawkConfigurationData_Resources_Resource_t.h>

namespace Project {
namespace v12 {
namespace knx {

class HawkConfigurationData_Resources_t : public Base
{
    Q_OBJECT

public:
    explicit HawkConfigurationData_Resources_t(Base * parent = nullptr);
    virtual ~HawkConfigurationData_Resources_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* elements */
    QVector<HawkConfigurationData_Resources_Resource_t *> Resource;
};

HawkConfigurationData_Resources_t * make_HawkConfigurationData_Resources_t(Base * parent);

} // namespace knx
} // namespace v12
} // namespace Project
