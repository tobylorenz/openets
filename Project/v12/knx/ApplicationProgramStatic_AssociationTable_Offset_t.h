/* This file is generated. */

#pragma once

#include <Project/xs/UnsignedInt.h>

namespace Project {
namespace v12 {
namespace knx {

using ApplicationProgramStatic_AssociationTable_Offset_t = xs::UnsignedInt;

} // namespace knx
} // namespace v12
} // namespace Project
