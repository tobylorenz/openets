/* This file is generated. */

#include <Project/v12/knx/ParameterType_TypeColor_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v12/knx/KNX_t.h>

namespace Project {
namespace v12 {
namespace knx {

ParameterType_TypeColor_t::ParameterType_TypeColor_t(Base * parent) :
    Base(parent)
{
}

ParameterType_TypeColor_t::~ParameterType_TypeColor_t()
{
}

void ParameterType_TypeColor_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("Space")) {
            Space = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ParameterType_TypeColor_t::tableColumnCount() const
{
    return 2;
}

QVariant ParameterType_TypeColor_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ParameterType_TypeColor";
        }
        if (qualifiedName == QString("Space")) {
            return Space;
        }
        break;
    }
    return QVariant();
}

QVariant ParameterType_TypeColor_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Space";
            }
        }
    }
    return QVariant();
}

QVariant ParameterType_TypeColor_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "TypeColor";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ParameterType_TypeColor");
    }
    return QVariant();
}

ParameterType_TypeColor_t * make_ParameterType_TypeColor_t(Base * parent)
{
    return new ParameterType_TypeColor_t(parent);
}

} // namespace knx
} // namespace v12
} // namespace Project
