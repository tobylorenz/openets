/* This file is generated. */

#include <Project/v12/knx/MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_Bit_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v12/knx/KNX_t.h>

namespace Project {
namespace v12 {
namespace knx {

MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_Bit_t::MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_Bit_t(Base * parent) :
    Base(parent)
{
}

MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_Bit_t::~MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_Bit_t()
{
}

void MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_Bit_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == "Id") {
            Id = attribute.value().toString();
            KNX_t * knx = findParent<KNX_t *>();
            Q_ASSERT(knx);
            knx->ids[Id] = this;
            continue;
        }
        if (name == QString("Name")) {
            Name = attribute.value().toString();
            continue;
        }
        if (name == QString("Set")) {
            Set = attribute.value().toString();
            continue;
        }
        if (name == QString("Cleared")) {
            Cleared = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_Bit_t::tableColumnCount() const
{
    return 5;
}

QVariant MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_Bit_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_Bit";
        }
        if (qualifiedName == QString("Id")) {
            return Id;
        }
        if (qualifiedName == QString("Name")) {
            return Name;
        }
        if (qualifiedName == QString("Set")) {
            return Set;
        }
        if (qualifiedName == QString("Cleared")) {
            return Cleared;
        }
        break;
    }
    return QVariant();
}

QVariant MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_Bit_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Id";
            case 2:
                return "Name";
            case 3:
                return "Set";
            case 4:
                return "Cleared";
            }
        }
    }
    return QVariant();
}

QVariant MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_Bit_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return !Name.isEmpty() ? Name : "Bit";
    case Qt::DecorationRole:
        return QIcon::fromTheme("MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_Bit");
    }
    return QVariant();
}

MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_Bit_t * make_MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_Bit_t(Base * parent)
{
    return new MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_Format_Bit_t(parent);
}

} // namespace knx
} // namespace v12
} // namespace Project
