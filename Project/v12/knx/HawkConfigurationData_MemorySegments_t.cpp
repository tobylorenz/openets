/* This file is generated. */

#include <Project/v12/knx/HawkConfigurationData_MemorySegments_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v12/knx/KNX_t.h>

namespace Project {
namespace v12 {
namespace knx {

HawkConfigurationData_MemorySegments_t::HawkConfigurationData_MemorySegments_t(Base * parent) :
    Base(parent)
{
}

HawkConfigurationData_MemorySegments_t::~HawkConfigurationData_MemorySegments_t()
{
}

void HawkConfigurationData_MemorySegments_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("MemorySegment")) {
            auto * newMemorySegment = make_HawkConfigurationData_MemorySegments_MemorySegment_t(this);
            newMemorySegment->read(reader);
            MemorySegment.append(newMemorySegment);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int HawkConfigurationData_MemorySegments_t::tableColumnCount() const
{
    return 1;
}

QVariant HawkConfigurationData_MemorySegments_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "HawkConfigurationData_MemorySegments";
        }
        break;
    }
    return QVariant();
}

QVariant HawkConfigurationData_MemorySegments_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant HawkConfigurationData_MemorySegments_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "MemorySegments";
    case Qt::DecorationRole:
        return QIcon::fromTheme("HawkConfigurationData_MemorySegments");
    }
    return QVariant();
}

HawkConfigurationData_MemorySegments_t * make_HawkConfigurationData_MemorySegments_t(Base * parent)
{
    return new HawkConfigurationData_MemorySegments_t(parent);
}

} // namespace knx
} // namespace v12
} // namespace Project
