/* This file is generated. */

#include <Project/v12/knx/ApplicationProgramStatic_ParameterCalculations_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v12/knx/KNX_t.h>

namespace Project {
namespace v12 {
namespace knx {

ApplicationProgramStatic_ParameterCalculations_t::ApplicationProgramStatic_ParameterCalculations_t(Base * parent) :
    Base(parent)
{
}

ApplicationProgramStatic_ParameterCalculations_t::~ApplicationProgramStatic_ParameterCalculations_t()
{
}

void ApplicationProgramStatic_ParameterCalculations_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("ParameterCalculation")) {
            QString newId = reader.attributes().value("Id").toString();
            Q_ASSERT(!newId.isEmpty());
            ParameterCalculation_t * newParameterCalculation;
            if (ParameterCalculation.contains(newId)) {
                newParameterCalculation = ParameterCalculation[newId];
            } else {
                newParameterCalculation = make_ParameterCalculation_t(this);
                ParameterCalculation[newId] = newParameterCalculation;
            }
            newParameterCalculation->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ApplicationProgramStatic_ParameterCalculations_t::tableColumnCount() const
{
    return 1;
}

QVariant ApplicationProgramStatic_ParameterCalculations_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ApplicationProgramStatic_ParameterCalculations";
        }
        break;
    }
    return QVariant();
}

QVariant ApplicationProgramStatic_ParameterCalculations_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant ApplicationProgramStatic_ParameterCalculations_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "ParameterCalculations";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ApplicationProgramStatic_ParameterCalculations");
    }
    return QVariant();
}

ApplicationProgramStatic_ParameterCalculations_t * make_ApplicationProgramStatic_ParameterCalculations_t(Base * parent)
{
    return new ApplicationProgramStatic_ParameterCalculations_t(parent);
}

} // namespace knx
} // namespace v12
} // namespace Project
