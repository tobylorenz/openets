/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v12/knx/LanguageDependentString255_t.h>
#include <Project/v12/knx/LanguageDependentString_t.h>
#include <Project/v12/knx/String50_t.h>
#include <Project/xs/ID.h>
#include <Project/xs/Base64Binary.h>
#include <Project/xs/Boolean.h>
#include <Project/xs/Float.h>
#include <Project/xs/Language.h>
#include <Project/xs/String.h>
#include <Project/xs/UnsignedShort.h>

namespace Project {
namespace v12 {
namespace knx {

/* forward declarations */
class Hardware_Products_Product_Attributes_t;
class Hardware_Products_Product_Baggages_t;
class RegistrationInfo_t;

class Hardware_Products_Product_t : public Base
{
    Q_OBJECT

public:
    explicit Hardware_Products_Product_t(Base * parent = nullptr);
    virtual ~Hardware_Products_Product_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::ID Id{};
    LanguageDependentString255_t Text{};
    String50_t OrderNumber{};
    xs::Boolean IsRailMounted{};
    xs::Float WidthInMillimeter{};
    LanguageDependentString_t VisibleDescription{};
    xs::Language DefaultLanguage{};
    xs::UnsignedShort NonRegRelevantDataVersion{"0"};
    xs::Base64Binary Hash{};
    xs::String InternalDescription{};

    /* elements */
    Hardware_Products_Product_Baggages_t * Baggages{};
    Hardware_Products_Product_Attributes_t * Attributes{};
    RegistrationInfo_t * RegistrationInfo{};
};

Hardware_Products_Product_t * make_Hardware_Products_Product_t(Base * parent);

} // namespace knx
} // namespace v12
} // namespace Project
