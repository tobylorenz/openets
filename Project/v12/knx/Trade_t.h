/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v12/knx/CompletionStatus_t.h>
#include <Project/v12/knx/DeviceInstanceRef_t.h>
#include <Project/v12/knx/String255_t.h>
#include <Project/v12/knx/Trade_t.h>
#include <Project/xs/ID.h>
#include <Project/xs/Int.h>
#include <Project/xs/String.h>

namespace Project {
namespace v12 {
namespace knx {

class Trade_t : public Base
{
    Q_OBJECT

public:
    explicit Trade_t(Base * parent = nullptr);
    virtual ~Trade_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::ID Id{};
    String255_t Name{};
    String255_t Number{};
    xs::String Comment{};
    CompletionStatus_t CompletionStatus{"Undefined"};
    xs::String Description{};
    xs::Int Puid{};

    /* elements */
    QVector<Trade_t *> Trade;
    QMap<IDREF, DeviceInstanceRef_t *> DeviceInstanceRef; // key: RefId
};

Trade_t * make_Trade_t(Base * parent);

} // namespace knx
} // namespace v12
} // namespace Project
