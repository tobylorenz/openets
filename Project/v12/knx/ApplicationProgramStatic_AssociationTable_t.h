/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v12/knx/ApplicationProgramStatic_AssociationTable_Offset_t.h>
#include <Project/v12/knx/IDREF.h>
#include <Project/xs/UnsignedInt.h>

namespace Project {
namespace v12 {
namespace knx {

/* forward declarations */
class ApplicationProgramStatic_Code_AbsoluteSegment_t;

class ApplicationProgramStatic_AssociationTable_t : public Base
{
    Q_OBJECT

public:
    explicit ApplicationProgramStatic_AssociationTable_t(Base * parent = nullptr);
    virtual ~ApplicationProgramStatic_AssociationTable_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    IDREF CodeSegment{};
    ApplicationProgramStatic_AssociationTable_Offset_t Offset{};
    xs::UnsignedInt MaxEntries{};

    /* getters */
    ApplicationProgramStatic_Code_AbsoluteSegment_t * getCodeSegment() const; // attribute: CodeSegment
};

ApplicationProgramStatic_AssociationTable_t * make_ApplicationProgramStatic_AssociationTable_t(Base * parent);

} // namespace knx
} // namespace v12
} // namespace Project
