/* This file is generated. */

#include <Project/v12/knx/MaskVersion_MaskEntries_MaskEntry_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v12/knx/KNX_t.h>

namespace Project {
namespace v12 {
namespace knx {

MaskVersion_MaskEntries_MaskEntry_t::MaskVersion_MaskEntries_MaskEntry_t(Base * parent) :
    Base(parent)
{
}

MaskVersion_MaskEntries_MaskEntry_t::~MaskVersion_MaskEntries_MaskEntry_t()
{
}

void MaskVersion_MaskEntries_MaskEntry_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == "Id") {
            Id = attribute.value().toString();
            KNX_t * knx = findParent<KNX_t *>();
            Q_ASSERT(knx);
            knx->ids[Id] = this;
            continue;
        }
        if (name == QString("Address")) {
            Address = attribute.value().toString();
            continue;
        }
        if (name == QString("Name")) {
            Name = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int MaskVersion_MaskEntries_MaskEntry_t::tableColumnCount() const
{
    return 4;
}

QVariant MaskVersion_MaskEntries_MaskEntry_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "MaskVersion_MaskEntries_MaskEntry";
        }
        if (qualifiedName == QString("Id")) {
            return Id;
        }
        if (qualifiedName == QString("Address")) {
            return Address;
        }
        if (qualifiedName == QString("Name")) {
            return Name;
        }
        break;
    }
    return QVariant();
}

QVariant MaskVersion_MaskEntries_MaskEntry_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Id";
            case 2:
                return "Address";
            case 3:
                return "Name";
            }
        }
    }
    return QVariant();
}

QVariant MaskVersion_MaskEntries_MaskEntry_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return !Name.isEmpty() ? Name : "MaskEntry";
    case Qt::DecorationRole:
        return QIcon::fromTheme("MaskVersion_MaskEntries_MaskEntry");
    }
    return QVariant();
}

MaskVersion_MaskEntries_MaskEntry_t * make_MaskVersion_MaskEntries_MaskEntry_t(Base * parent)
{
    return new MaskVersion_MaskEntries_MaskEntry_t(parent);
}

} // namespace knx
} // namespace v12
} // namespace Project
