/* This file is generated. */

#pragma once

#include <Project/Base.h>

namespace Project {
namespace v12 {
namespace knx {

/* forward declarations */
class GroupAddresses_GroupRanges_t;

class GroupAddresses_t : public Base
{
    Q_OBJECT

public:
    explicit GroupAddresses_t(Base * parent = nullptr);
    virtual ~GroupAddresses_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* elements */
    GroupAddresses_GroupRanges_t * GroupRanges{};
};

GroupAddresses_t * make_GroupAddresses_t(Base * parent);

} // namespace knx
} // namespace v12
} // namespace Project
