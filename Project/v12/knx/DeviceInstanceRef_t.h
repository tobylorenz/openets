/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v12/knx/IDREF.h>

namespace Project {
namespace v12 {
namespace knx {

/* forward declarations */
class DeviceInstance_t;

class DeviceInstanceRef_t : public Base
{
    Q_OBJECT

public:
    explicit DeviceInstanceRef_t(Base * parent = nullptr);
    virtual ~DeviceInstanceRef_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    IDREF RefId{};

    /* getters */
    DeviceInstance_t * getDeviceInstance() const; // attribute: RefId
};

DeviceInstanceRef_t * make_DeviceInstanceRef_t(Base * parent);

} // namespace knx
} // namespace v12
} // namespace Project
