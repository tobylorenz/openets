/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v12/knx/String50_t.h>
#include <Project/xs/ID.h>

namespace Project {
namespace v12 {
namespace knx {

class AddinData_t : public Base
{
    Q_OBJECT

public:
    explicit AddinData_t(Base * parent = nullptr);
    virtual ~AddinData_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::ID AddinId{};
    String50_t Name{};
};

AddinData_t * make_AddinData_t(Base * parent);

} // namespace knx
} // namespace v12
} // namespace Project
