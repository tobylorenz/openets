/* This file is generated. */

#include <Project/v12/knx/ComObject_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v12/knx/KNX_t.h>

namespace Project {
namespace v12 {
namespace knx {

ComObject_t::ComObject_t(Base * parent) :
    Base(parent)
{
}

ComObject_t::~ComObject_t()
{
}

void ComObject_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == "Id") {
            Id = attribute.value().toString();
            KNX_t * knx = findParent<KNX_t *>();
            Q_ASSERT(knx);
            knx->ids[Id] = this;
            continue;
        }
        if (name == QString("Name")) {
            Name = attribute.value().toString();
            continue;
        }
        if (name == QString("Text")) {
            Text = attribute.value().toString();
            continue;
        }
        if (name == QString("Number")) {
            Number = attribute.value().toString();
            continue;
        }
        if (name == QString("FunctionText")) {
            FunctionText = attribute.value().toString();
            continue;
        }
        if (name == QString("Priority")) {
            Priority = attribute.value().toString();
            continue;
        }
        if (name == QString("ObjectSize")) {
            ObjectSize = attribute.value().toString();
            continue;
        }
        if (name == QString("ReadFlag")) {
            ReadFlag = attribute.value().toString();
            continue;
        }
        if (name == QString("WriteFlag")) {
            WriteFlag = attribute.value().toString();
            continue;
        }
        if (name == QString("CommunicationFlag")) {
            CommunicationFlag = attribute.value().toString();
            continue;
        }
        if (name == QString("TransmitFlag")) {
            TransmitFlag = attribute.value().toString();
            continue;
        }
        if (name == QString("UpdateFlag")) {
            UpdateFlag = attribute.value().toString();
            continue;
        }
        if (name == QString("ReadOnInitFlag")) {
            ReadOnInitFlag = attribute.value().toString();
            continue;
        }
        if (name == QString("DatapointType")) {
            DatapointType = attribute.value().toString().split(' ');
            continue;
        }
        if (name == QString("InternalDescription")) {
            InternalDescription = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ComObject_t::tableColumnCount() const
{
    return 16;
}

QVariant ComObject_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ComObject";
        }
        if (qualifiedName == QString("Id")) {
            return Id;
        }
        if (qualifiedName == QString("Name")) {
            return Name;
        }
        if (qualifiedName == QString("Text")) {
            return Text;
        }
        if (qualifiedName == QString("Number")) {
            return Number;
        }
        if (qualifiedName == QString("FunctionText")) {
            return FunctionText;
        }
        if (qualifiedName == QString("Priority")) {
            return Priority;
        }
        if (qualifiedName == QString("ObjectSize")) {
            return ObjectSize;
        }
        if (qualifiedName == QString("ReadFlag")) {
            return ReadFlag;
        }
        if (qualifiedName == QString("WriteFlag")) {
            return WriteFlag;
        }
        if (qualifiedName == QString("CommunicationFlag")) {
            return CommunicationFlag;
        }
        if (qualifiedName == QString("TransmitFlag")) {
            return TransmitFlag;
        }
        if (qualifiedName == QString("UpdateFlag")) {
            return UpdateFlag;
        }
        if (qualifiedName == QString("ReadOnInitFlag")) {
            return ReadOnInitFlag;
        }
        if (qualifiedName == QString("DatapointType")) {
            return DatapointType.join(' ');
        }
        if (qualifiedName == QString("InternalDescription")) {
            return InternalDescription;
        }
        break;
    }
    return QVariant();
}

QVariant ComObject_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Id";
            case 2:
                return "Name";
            case 3:
                return "Text";
            case 4:
                return "Number";
            case 5:
                return "FunctionText";
            case 6:
                return "Priority";
            case 7:
                return "ObjectSize";
            case 8:
                return "ReadFlag";
            case 9:
                return "WriteFlag";
            case 10:
                return "CommunicationFlag";
            case 11:
                return "TransmitFlag";
            case 12:
                return "UpdateFlag";
            case 13:
                return "ReadOnInitFlag";
            case 14:
                return "DatapointType";
            case 15:
                return "InternalDescription";
            }
        }
    }
    return QVariant();
}

QVariant ComObject_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return QString("[%1] %2-%3").arg(Number).arg(!Name.isEmpty() ? Name : Text).arg(FunctionText);
    case Qt::DecorationRole:
        return QIcon::fromTheme("ComObject");
    }
    return QVariant();
}

QList<MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_t *> ComObject_t::getDatapointType() const
{
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    QList<MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_t *> retVal;
    for (QString id : DatapointType) {
        retVal << qobject_cast<MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_t *>(knx->ids[id]);
    }
    return retVal;
}

ComObject_t * make_ComObject_t(Base * parent)
{
    return new ComObject_t(parent);
}

} // namespace knx
} // namespace v12
} // namespace Project
