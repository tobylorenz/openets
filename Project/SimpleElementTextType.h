#pragma once

#include <QString>

#include <Project/Base.h>

namespace Project {

/** @todo ideale knx_xsd_to_qt should generate a complex type class instead */
class SimpleElementTextType : public Base
{
    Q_OBJECT

public:
    explicit SimpleElementTextType(QString name, Base * parent = nullptr);

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* element text */
    QString text{};

private:
    QString name{};
};

SimpleElementTextType * make_SimpleElementTextType(const QString & name, Base * parent);

} // namespace Project
