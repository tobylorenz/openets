#pragma once

#include <QMimeData>
#include <QObject>
#include <QString>
#include <QVariant>
#include <QXmlStreamReader>

namespace Project {

class Base : public QObject
{
    Q_OBJECT

public:
    explicit Base(Base * parent = nullptr);
    virtual ~Base();

    /** read attributes and elements */
    virtual void read(QXmlStreamReader & reader) = 0;

    /* support for QAbstractItemModel */
    virtual bool canDropMimeData(const QMimeData *data, Qt::DropAction action) const;
    virtual Base * child(int row) const;
    virtual int childCount() const;
    virtual bool dropMimeData(const QMimeData *data, Qt::DropAction action);
    virtual Qt::ItemFlags flags(Qt::ItemFlags defaultFlags) const;
    virtual QMimeData *mimeData() const;
    virtual int tableColumnCount() const = 0;
    virtual QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const = 0;
    virtual QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const = 0;
    virtual QVariant treeData(int role = Qt::DisplayRole) const = 0;
    virtual int row() const;
    virtual Base * parentItem() const;

    /** get closest parent with type T */
    template<typename T>
    inline T findParent() const {
        QObject * obj = parent();
        while (obj && !qobject_cast<T>(obj)) {
            obj = obj->parent();
        };

        return qobject_cast<T>(obj);
    }

public Q_SLOTS:
    void update();

Q_SIGNALS:
    void updated();
};

} // namespace Project
