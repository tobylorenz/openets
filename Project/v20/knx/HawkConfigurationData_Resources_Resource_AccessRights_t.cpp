/* This file is generated. */

#include <Project/v20/knx/HawkConfigurationData_Resources_Resource_AccessRights_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/KNX_t.h>

namespace Project {
namespace v20 {
namespace knx {

HawkConfigurationData_Resources_Resource_AccessRights_t::HawkConfigurationData_Resources_Resource_AccessRights_t(Base * parent) :
    Base(parent)
{
}

HawkConfigurationData_Resources_Resource_AccessRights_t::~HawkConfigurationData_Resources_Resource_AccessRights_t()
{
}

void HawkConfigurationData_Resources_Resource_AccessRights_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("Read")) {
            Read = attribute.value().toString();
            continue;
        }
        if (name == QString("Write")) {
            Write = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int HawkConfigurationData_Resources_Resource_AccessRights_t::tableColumnCount() const
{
    return 3;
}

QVariant HawkConfigurationData_Resources_Resource_AccessRights_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "HawkConfigurationData_Resources_Resource_AccessRights";
        }
        if (qualifiedName == QString("Read")) {
            return Read;
        }
        if (qualifiedName == QString("Write")) {
            return Write;
        }
        break;
    }
    return QVariant();
}

QVariant HawkConfigurationData_Resources_Resource_AccessRights_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Read";
            case 2:
                return "Write";
            }
        }
    }
    return QVariant();
}

QVariant HawkConfigurationData_Resources_Resource_AccessRights_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "AccessRights";
    case Qt::DecorationRole:
        return QIcon::fromTheme("HawkConfigurationData_Resources_Resource_AccessRights");
    }
    return QVariant();
}

HawkConfigurationData_Resources_Resource_AccessRights_t * make_HawkConfigurationData_Resources_Resource_AccessRights_t(Base * parent)
{
    return new HawkConfigurationData_Resources_Resource_AccessRights_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
