/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/xs/Int.h>

namespace Project {
namespace v20 {
namespace knx {

class DatapointType_DatapointSubtypes_DatapointSubtype_Format_Reserved_t : public Base
{
    Q_OBJECT

public:
    explicit DatapointType_DatapointSubtypes_DatapointSubtype_Format_Reserved_t(Base * parent = nullptr);
    virtual ~DatapointType_DatapointSubtypes_DatapointSubtype_Format_Reserved_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::Int Width{};
};

DatapointType_DatapointSubtypes_DatapointSubtype_Format_Reserved_t * make_DatapointType_DatapointSubtypes_DatapointSubtype_Format_Reserved_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
