/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v20/knx/Access_t.h>
#include <Project/v20/knx/IDREF.h>
#include <Project/v20/knx/IDREFS.h>
#include <Project/v20/knx/LanguageDependentString20_t.h>
#include <Project/v20/knx/LanguageDependentString255_t.h>
#include <Project/v20/knx/LanguageDependentString_t.h>
#include <Project/v20/knx/String255_t.h>
#include <Project/v20/knx/Value_t.h>
#include <Project/xs/ID.h>
#include <Project/xs/Boolean.h>
#include <Project/xs/String.h>

namespace Project {
namespace v20 {
namespace knx {

/* forward declarations */
class MemoryParameter_t;
class ParameterType_t;
class PropertyParameter_t;

class ApplicationProgramStatic_Parameters_Parameter_t : public Base
{
    Q_OBJECT

public:
    explicit ApplicationProgramStatic_Parameters_Parameter_t(Base * parent = nullptr);
    virtual ~ApplicationProgramStatic_Parameters_Parameter_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::ID Id{};
    String255_t Name{};
    IDREF ParameterType{};
    IDREFS ParameterTypeParams{};
    LanguageDependentString255_t Text{};
    LanguageDependentString20_t SuffixText{};
    Access_t Access{"ReadWrite"};
    Value_t Value{};
    LanguageDependentString_t InitialValue{};
    xs::Boolean CustomerAdjustable{"false"};
    xs::String InternalDescription{};
    xs::Boolean LegacyPatchAlways{"false"};

    /* elements */
    // xs:choice MemoryParameter_t * Memory{};
    // xs:choice PropertyParameter_t * Property{};

    /* getters */
    ParameterType_t * getParameterType() const; // attribute: ParameterType
    // Base * getTODO() const; // attribute: ParameterTypeParams
};

ApplicationProgramStatic_Parameters_Parameter_t * make_ApplicationProgramStatic_Parameters_Parameter_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
