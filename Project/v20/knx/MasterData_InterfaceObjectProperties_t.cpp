/* This file is generated. */

#include <Project/v20/knx/MasterData_InterfaceObjectProperties_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/KNX_t.h>

namespace Project {
namespace v20 {
namespace knx {

MasterData_InterfaceObjectProperties_t::MasterData_InterfaceObjectProperties_t(Base * parent) :
    Base(parent)
{
}

MasterData_InterfaceObjectProperties_t::~MasterData_InterfaceObjectProperties_t()
{
}

void MasterData_InterfaceObjectProperties_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("InterfaceObjectProperty")) {
            QString newId = reader.attributes().value("Id").toString();
            Q_ASSERT(!newId.isEmpty());
            MasterData_InterfaceObjectProperties_InterfaceObjectProperty_t * newInterfaceObjectProperty;
            if (InterfaceObjectProperty.contains(newId)) {
                newInterfaceObjectProperty = InterfaceObjectProperty[newId];
            } else {
                newInterfaceObjectProperty = make_MasterData_InterfaceObjectProperties_InterfaceObjectProperty_t(this);
                InterfaceObjectProperty[newId] = newInterfaceObjectProperty;
            }
            newInterfaceObjectProperty->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int MasterData_InterfaceObjectProperties_t::tableColumnCount() const
{
    return 1;
}

QVariant MasterData_InterfaceObjectProperties_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "MasterData_InterfaceObjectProperties";
        }
        break;
    }
    return QVariant();
}

QVariant MasterData_InterfaceObjectProperties_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant MasterData_InterfaceObjectProperties_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "InterfaceObjectProperties";
    case Qt::DecorationRole:
        return QIcon::fromTheme("MasterData_InterfaceObjectProperties");
    }
    return QVariant();
}

MasterData_InterfaceObjectProperties_t * make_MasterData_InterfaceObjectProperties_t(Base * parent)
{
    return new MasterData_InterfaceObjectProperties_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
