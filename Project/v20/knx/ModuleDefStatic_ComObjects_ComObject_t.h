/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v20/knx/ComObjectPriority_t.h>
#include <Project/v20/knx/ComObjectSecurityRequirements_t.h>
#include <Project/v20/knx/ComObjectSize_t.h>
#include <Project/v20/knx/DatapointType_DatapointSubtypes_DatapointSubtype_t.h>
#include <Project/v20/knx/Enable_t.h>
#include <Project/v20/knx/IDREF.h>
#include <Project/v20/knx/IDREFS.h>
#include <Project/v20/knx/LanguageDependentString255_t.h>
#include <Project/v20/knx/String255_t.h>
#include <Project/xs/ID.h>
#include <Project/xs/Boolean.h>
#include <Project/xs/String.h>
#include <Project/xs/UnsignedInt.h>

namespace Project {
namespace v20 {
namespace knx {

/* forward declarations */
class ModuleDef_Arguments_Argument_t;

class ModuleDefStatic_ComObjects_ComObject_t : public Base
{
    Q_OBJECT

public:
    explicit ModuleDefStatic_ComObjects_ComObject_t(Base * parent = nullptr);
    virtual ~ModuleDefStatic_ComObjects_ComObject_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::ID Id{};
    String255_t Name{};
    LanguageDependentString255_t Text{};
    xs::UnsignedInt Number{};
    LanguageDependentString255_t FunctionText{};
    ComObjectPriority_t Priority{"Low"};
    ComObjectSize_t ObjectSize{};
    Enable_t ReadFlag{};
    Enable_t WriteFlag{};
    Enable_t CommunicationFlag{};
    Enable_t TransmitFlag{};
    Enable_t UpdateFlag{};
    Enable_t ReadOnInitFlag{};
    IDREFS DatapointType{};
    xs::String InternalDescription{};
    ComObjectSecurityRequirements_t SecurityRequired{"None"};
    xs::Boolean MayRead{};
    xs::Boolean ReadFlagLocked{"false"};
    xs::Boolean WriteFlagLocked{"false"};
    xs::Boolean TransmitFlagLocked{"false"};
    xs::Boolean UpdateFlagLocked{"false"};
    xs::Boolean ReadOnInitFlagLocked{"false"};
    IDREF BaseNumber{};

    /* getters */
    QList<DatapointType_DatapointSubtypes_DatapointSubtype_t *> getDatapointType() const; // attribute: DatapointType
    ModuleDef_Arguments_Argument_t * getBaseNumber() const; // attribute: BaseNumber
};

ModuleDefStatic_ComObjects_ComObject_t * make_ModuleDefStatic_ComObjects_ComObject_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
