/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v20/knx/IDREF.h>
#include <Project/v20/knx/IDREFS.h>

namespace Project {
namespace v20 {
namespace knx {

class P2PLinkDeviceEndpoint_t : public Base
{
    Q_OBJECT

public:
    explicit P2PLinkDeviceEndpoint_t(Base * parent = nullptr);
    virtual ~P2PLinkDeviceEndpoint_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    IDREF DeviceRefId{};
    IDREFS SecurityRoles{};

    /* getters */
    Base * getDevice() const; // attribute: DeviceRefId
    // Base * getTODO() const; // attribute: SecurityRoles
};

P2PLinkDeviceEndpoint_t * make_P2PLinkDeviceEndpoint_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
