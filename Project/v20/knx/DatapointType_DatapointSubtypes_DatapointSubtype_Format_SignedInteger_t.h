/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v20/knx/LanguageDependentString_t.h>
#include <Project/xs/ID.h>
#include <Project/xs/Float.h>
#include <Project/xs/Long.h>
#include <Project/xs/UnsignedInt.h>

namespace Project {
namespace v20 {
namespace knx {

class DatapointType_DatapointSubtypes_DatapointSubtype_Format_SignedInteger_t : public Base
{
    Q_OBJECT

public:
    explicit DatapointType_DatapointSubtypes_DatapointSubtype_Format_SignedInteger_t(Base * parent = nullptr);
    virtual ~DatapointType_DatapointSubtypes_DatapointSubtype_Format_SignedInteger_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::ID Id{};
    xs::UnsignedInt Width{};
    LanguageDependentString_t Name{};
    LanguageDependentString_t Unit{};
    xs::Long MinInclusive{};
    xs::Long MaxInclusive{};
    xs::Float Coefficient{};
    xs::Long Offset{};
};

DatapointType_DatapointSubtypes_DatapointSubtype_Format_SignedInteger_t * make_DatapointType_DatapointSubtypes_DatapointSubtype_Format_SignedInteger_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
