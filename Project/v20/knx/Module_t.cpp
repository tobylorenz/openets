/* This file is generated. */

#include <Project/v20/knx/Module_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/KNX_t.h>
#include <Project/v20/knx/Module_NumericArg_t.h>
#include <Project/v20/knx/Module_TextArg_t.h>

namespace Project {
namespace v20 {
namespace knx {

Module_t::Module_t(Base * parent) :
    Base(parent)
{
}

Module_t::~Module_t()
{
}

void Module_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == "Id") {
            Id = attribute.value().toString();
            KNX_t * knx = findParent<KNX_t *>();
            Q_ASSERT(knx);
            knx->ids[Id] = this;
            continue;
        }
        if (name == QString("RefId")) {
            RefId = attribute.value().toString();
            continue;
        }
        if (name == QString("Name")) {
            Name = attribute.value().toString();
            continue;
        }
        if (name == QString("InternalDescription")) {
            InternalDescription = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("NumericArg")) {
            auto * NumericArg = make_Module_NumericArg_t(this);
            NumericArg->read(reader);
            continue;
        }
        if (reader.name() == QString("TextArg")) {
            auto * TextArg = make_Module_TextArg_t(this);
            TextArg->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int Module_t::tableColumnCount() const
{
    return 5;
}

QVariant Module_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "Module";
        }
        if (qualifiedName == QString("Id")) {
            return Id;
        }
        if (qualifiedName == QString("RefId")) {
            return RefId;
        }
        if (qualifiedName == QString("Name")) {
            return Name;
        }
        if (qualifiedName == QString("InternalDescription")) {
            return InternalDescription;
        }
        break;
    }
    return QVariant();
}

QVariant Module_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Id";
            case 2:
                return "RefId";
            case 3:
                return "Name";
            case 4:
                return "InternalDescription";
            }
        }
    }
    return QVariant();
}

QVariant Module_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return !Name.isEmpty() ? Name : "Module";
    case Qt::DecorationRole:
        return QIcon::fromTheme("Module");
    }
    return QVariant();
}

Module_t * make_Module_t(Base * parent)
{
    return new Module_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
