/* This file is generated. */

#include <Project/v20/knx/Hardware_Hardware2Programs_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/KNX_t.h>

namespace Project {
namespace v20 {
namespace knx {

Hardware_Hardware2Programs_t::Hardware_Hardware2Programs_t(Base * parent) :
    Base(parent)
{
}

Hardware_Hardware2Programs_t::~Hardware_Hardware2Programs_t()
{
}

void Hardware_Hardware2Programs_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("Hardware2Program")) {
            QString newId = reader.attributes().value("Id").toString();
            Q_ASSERT(!newId.isEmpty());
            Hardware2Program_t * newHardware2Program;
            if (Hardware2Program.contains(newId)) {
                newHardware2Program = Hardware2Program[newId];
            } else {
                newHardware2Program = make_Hardware2Program_t(this);
                Hardware2Program[newId] = newHardware2Program;
            }
            newHardware2Program->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int Hardware_Hardware2Programs_t::tableColumnCount() const
{
    return 1;
}

QVariant Hardware_Hardware2Programs_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "Hardware_Hardware2Programs";
        }
        break;
    }
    return QVariant();
}

QVariant Hardware_Hardware2Programs_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant Hardware_Hardware2Programs_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "Hardware2Programs";
    case Qt::DecorationRole:
        return QIcon::fromTheme("Hardware_Hardware2Programs");
    }
    return QVariant();
}

Hardware_Hardware2Programs_t * make_Hardware_Hardware2Programs_t(Base * parent)
{
    return new Hardware_Hardware2Programs_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
