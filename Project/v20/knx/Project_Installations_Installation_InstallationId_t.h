/* This file is generated. */

#pragma once

#include <Project/xs/UnsignedShort.h>

namespace Project {
namespace v20 {
namespace knx {

using Project_Installations_Installation_InstallationId_t = xs::UnsignedShort;

} // namespace knx
} // namespace v20
} // namespace Project
