/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v20/knx/IDREF.h>
#include <Project/v20/knx/String255_t.h>
#include <Project/xs/ID.h>
#include <Project/xs/String.h>
#include <Project/xs/UnsignedInt.h>

namespace Project {
namespace v20 {
namespace knx {

/* forward declarations */
class ComObjectParameterChoose_t;
class Module_t;
class Repeat_t;

class Repeat_t : public Base
{
    Q_OBJECT

public:
    explicit Repeat_t(Base * parent = nullptr);
    virtual ~Repeat_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::ID Id{};
    String255_t Name{};
    xs::String InternalDescription{};
    IDREF ParameterRefId{};
    xs::UnsignedInt Count{"0"};

    /* elements */
    // xs:choice ComObjectParameterChoose_t * Choose{};
    // xs:choice Module_t * Module{};
    // xs:choice Repeat_t * Repeat{};

    /* getters */
    Base * getParameter() const; // attribute: ParameterRefId
};

Repeat_t * make_Repeat_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
