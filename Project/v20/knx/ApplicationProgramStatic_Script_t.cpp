/* This file is generated. */

#include <Project/v20/knx/ApplicationProgramStatic_Script_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/KNX_t.h>

namespace Project {
namespace v20 {
namespace knx {

ApplicationProgramStatic_Script_t::ApplicationProgramStatic_Script_t(Base * parent) :
    Base(parent)
{
}

ApplicationProgramStatic_Script_t::~ApplicationProgramStatic_Script_t()
{
}

void ApplicationProgramStatic_Script_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }

    /* parse text element */
    text = reader.readElementText();
}

int ApplicationProgramStatic_Script_t::tableColumnCount() const
{
    return 1;
}

QVariant ApplicationProgramStatic_Script_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ApplicationProgramStatic_Script";
        }
        break;
    }
    return QVariant();
}

QVariant ApplicationProgramStatic_Script_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant ApplicationProgramStatic_Script_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "Script";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ApplicationProgramStatic_Script");
    }
    return QVariant();
}

ApplicationProgramStatic_Script_t * make_ApplicationProgramStatic_Script_t(Base * parent)
{
    return new ApplicationProgramStatic_Script_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
