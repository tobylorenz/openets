/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v20/knx/LanguageDependentString255_t.h>
#include <Project/v20/knx/String255_t.h>
#include <Project/xs/ID.h>
#include <Project/xs/String.h>

namespace Project {
namespace v20 {
namespace knx {

class ApplicationProgramStatic_Messages_Message_t : public Base
{
    Q_OBJECT

public:
    explicit ApplicationProgramStatic_Messages_Message_t(Base * parent = nullptr);
    virtual ~ApplicationProgramStatic_Messages_Message_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::ID Id{};
    String255_t Name{};
    xs::String InternalDescription{};
    LanguageDependentString255_t Text{};
};

ApplicationProgramStatic_Messages_Message_t * make_ApplicationProgramStatic_Messages_Message_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
