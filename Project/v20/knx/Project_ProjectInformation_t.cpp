/* This file is generated. */

#include <Project/v20/knx/Project_ProjectInformation_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/KNX_t.h>
#include <Project/v20/knx/Project_ProjectInformation_DeviceCertificates_t.h>
#include <Project/v20/knx/Project_ProjectInformation_HistoryEntries_t.h>
#include <Project/v20/knx/Project_ProjectInformation_ProjectTraces_t.h>
#include <Project/v20/knx/Project_ProjectInformation_ToDoItems_t.h>

namespace Project {
namespace v20 {
namespace knx {

Project_ProjectInformation_t::Project_ProjectInformation_t(Base * parent) :
    Base(parent)
{
}

Project_ProjectInformation_t::~Project_ProjectInformation_t()
{
}

void Project_ProjectInformation_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("Name")) {
            Name = attribute.value().toString();
            continue;
        }
        if (name == QString("GroupAddressStyle")) {
            GroupAddressStyle = attribute.value().toString();
            continue;
        }
        if (name == QString("ProjectNumber")) {
            ProjectNumber = attribute.value().toString();
            continue;
        }
        if (name == QString("ContractNumber")) {
            ContractNumber = attribute.value().toString();
            continue;
        }
        if (name == QString("LastModified")) {
            LastModified = attribute.value().toString();
            continue;
        }
        if (name == QString("ProjectStart")) {
            ProjectStart = attribute.value().toString();
            continue;
        }
        if (name == QString("ProjectEnd")) {
            ProjectEnd = attribute.value().toString();
            continue;
        }
        if (name == QString("ProjectId")) {
            ProjectId = attribute.value().toString();
            continue;
        }
        if (name == QString("ProjectPassword")) {
            ProjectPassword = attribute.value().toString();
            continue;
        }
        if (name == QString("Comment")) {
            Comment = attribute.value().toString();
            continue;
        }
        if (name == QString("CompletionStatus")) {
            CompletionStatus = attribute.value().toString();
            continue;
        }
        if (name == QString("ProjectTracingLevel")) {
            ProjectTracingLevel = attribute.value().toString();
            continue;
        }
        if (name == QString("ProjectTracingPassword")) {
            ProjectTracingPassword = attribute.value().toString();
            continue;
        }
        if (name == QString("Hide16BitGroupsFromLegacyPlugins")) {
            Hide16BitGroupsFromLegacyPlugins = attribute.value().toString();
            continue;
        }
        if (name == QString("CodePage")) {
            CodePage = attribute.value().toString();
            continue;
        }
        if (name == QString("BusAccessLegacyMode")) {
            BusAccessLegacyMode = attribute.value().toString();
            continue;
        }
        if (name == QString("Guid")) {
            Guid = attribute.value().toString();
            continue;
        }
        if (name == QString("LastUsedPuid")) {
            LastUsedPuid = attribute.value().toString();
            continue;
        }
        if (name == QString("Security")) {
            Security = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("HistoryEntries")) {
            if (!HistoryEntries) {
                HistoryEntries = make_Project_ProjectInformation_HistoryEntries_t(this);
            }
            HistoryEntries->read(reader);
            continue;
        }
        if (reader.name() == QString("ToDoItems")) {
            if (!ToDoItems) {
                ToDoItems = make_Project_ProjectInformation_ToDoItems_t(this);
            }
            ToDoItems->read(reader);
            continue;
        }
        if (reader.name() == QString("ProjectTraces")) {
            if (!ProjectTraces) {
                ProjectTraces = make_Project_ProjectInformation_ProjectTraces_t(this);
            }
            ProjectTraces->read(reader);
            continue;
        }
        if (reader.name() == QString("DeviceCertificates")) {
            if (!DeviceCertificates) {
                DeviceCertificates = make_Project_ProjectInformation_DeviceCertificates_t(this);
            }
            DeviceCertificates->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int Project_ProjectInformation_t::tableColumnCount() const
{
    return 20;
}

QVariant Project_ProjectInformation_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "Project_ProjectInformation";
        }
        if (qualifiedName == QString("Name")) {
            return Name;
        }
        if (qualifiedName == QString("GroupAddressStyle")) {
            return GroupAddressStyle;
        }
        if (qualifiedName == QString("ProjectNumber")) {
            return ProjectNumber;
        }
        if (qualifiedName == QString("ContractNumber")) {
            return ContractNumber;
        }
        if (qualifiedName == QString("LastModified")) {
            return LastModified;
        }
        if (qualifiedName == QString("ProjectStart")) {
            return ProjectStart;
        }
        if (qualifiedName == QString("ProjectEnd")) {
            return ProjectEnd;
        }
        if (qualifiedName == QString("ProjectId")) {
            return ProjectId;
        }
        if (qualifiedName == QString("ProjectPassword")) {
            return ProjectPassword;
        }
        if (qualifiedName == QString("Comment")) {
            return Comment;
        }
        if (qualifiedName == QString("CompletionStatus")) {
            return CompletionStatus;
        }
        if (qualifiedName == QString("ProjectTracingLevel")) {
            return ProjectTracingLevel;
        }
        if (qualifiedName == QString("ProjectTracingPassword")) {
            return ProjectTracingPassword;
        }
        if (qualifiedName == QString("Hide16BitGroupsFromLegacyPlugins")) {
            return Hide16BitGroupsFromLegacyPlugins;
        }
        if (qualifiedName == QString("CodePage")) {
            return CodePage;
        }
        if (qualifiedName == QString("BusAccessLegacyMode")) {
            return BusAccessLegacyMode;
        }
        if (qualifiedName == QString("Guid")) {
            return Guid;
        }
        if (qualifiedName == QString("LastUsedPuid")) {
            return LastUsedPuid;
        }
        if (qualifiedName == QString("Security")) {
            return Security;
        }
        break;
    }
    return QVariant();
}

QVariant Project_ProjectInformation_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Name";
            case 2:
                return "GroupAddressStyle";
            case 3:
                return "ProjectNumber";
            case 4:
                return "ContractNumber";
            case 5:
                return "LastModified";
            case 6:
                return "ProjectStart";
            case 7:
                return "ProjectEnd";
            case 8:
                return "ProjectId";
            case 9:
                return "ProjectPassword";
            case 10:
                return "Comment";
            case 11:
                return "CompletionStatus";
            case 12:
                return "ProjectTracingLevel";
            case 13:
                return "ProjectTracingPassword";
            case 14:
                return "Hide16BitGroupsFromLegacyPlugins";
            case 15:
                return "CodePage";
            case 16:
                return "BusAccessLegacyMode";
            case 17:
                return "Guid";
            case 18:
                return "LastUsedPuid";
            case 19:
                return "Security";
            }
        }
    }
    return QVariant();
}

QVariant Project_ProjectInformation_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return !Name.isEmpty() ? Name : "ProjectInformation";
    case Qt::DecorationRole:
        return QIcon::fromTheme("Project_ProjectInformation");
    }
    return QVariant();
}

Project_ProjectInformation_t * make_Project_ProjectInformation_t(Base * parent)
{
    return new Project_ProjectInformation_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
