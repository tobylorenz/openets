/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v20/knx/IDREF.h>
#include <Project/xs/IDREF.h>

namespace Project {
namespace v20 {
namespace knx {

class P2PLinkBusInterfaceEndpoint_t : public Base
{
    Q_OBJECT

public:
    explicit P2PLinkBusInterfaceEndpoint_t(Base * parent = nullptr);
    virtual ~P2PLinkBusInterfaceEndpoint_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    IDREF DeviceRefId{};
    xs::IDREF BusInterfaceRefId{};

    /* getters */
    Base * getDevice() const; // attribute: DeviceRefId
    Base * getBusInterface() const; // attribute: BusInterfaceRefId
};

P2PLinkBusInterfaceEndpoint_t * make_P2PLinkBusInterfaceEndpoint_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
