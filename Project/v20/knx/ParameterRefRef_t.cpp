/* This file is generated. */

#include <Project/v20/knx/ParameterRefRef_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/KNX_t.h>
#include <Project/v20/knx/ParameterRef_t.h>

namespace Project {
namespace v20 {
namespace knx {

ParameterRefRef_t::ParameterRefRef_t(Base * parent) :
    Base(parent)
{
}

ParameterRefRef_t::~ParameterRefRef_t()
{
}

void ParameterRefRef_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("RefId")) {
            RefId = attribute.value().toString();
            continue;
        }
        if (name == QString("HelpContext")) {
            HelpContext = attribute.value().toString();
            continue;
        }
        if (name == QString("IndentLevel")) {
            IndentLevel = attribute.value().toString();
            continue;
        }
        if (name == QString("InternalDescription")) {
            InternalDescription = attribute.value().toString();
            continue;
        }
        if (name == QString("Cell")) {
            Cell = attribute.value().toString();
            continue;
        }
        if (name == QString("Icon")) {
            Icon = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ParameterRefRef_t::tableColumnCount() const
{
    return 7;
}

QVariant ParameterRefRef_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ParameterRefRef";
        }
        if (qualifiedName == QString("RefId")) {
            return RefId;
        }
        if (qualifiedName == QString("HelpContext")) {
            return HelpContext;
        }
        if (qualifiedName == QString("IndentLevel")) {
            return IndentLevel;
        }
        if (qualifiedName == QString("InternalDescription")) {
            return InternalDescription;
        }
        if (qualifiedName == QString("Cell")) {
            return Cell;
        }
        if (qualifiedName == QString("Icon")) {
            return Icon;
        }
        break;
    }
    return QVariant();
}

QVariant ParameterRefRef_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "RefId";
            case 2:
                return "HelpContext";
            case 3:
                return "IndentLevel";
            case 4:
                return "InternalDescription";
            case 5:
                return "Cell";
            case 6:
                return "Icon";
            }
        }
    }
    return QVariant();
}

QVariant ParameterRefRef_t::treeData(int role) const
{
    auto * ref = getParameterRef();
    if (ref) {
        return ref->treeData(role);
    }

    switch (role) {
    case Qt::DisplayRole:
        return "ParameterRefRef";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ParameterRefRef");
    }
    return QVariant();
}

ParameterRef_t * ParameterRefRef_t::getParameterRef() const
{
    if (RefId.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<ParameterRef_t *>(knx->ids[RefId]);
}

ParameterRefRef_t * make_ParameterRefRef_t(Base * parent)
{
    return new ParameterRefRef_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
