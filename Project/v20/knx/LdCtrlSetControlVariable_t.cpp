/* This file is generated. */

#include <Project/v20/knx/LdCtrlSetControlVariable_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/KNX_t.h>

namespace Project {
namespace v20 {
namespace knx {

LdCtrlSetControlVariable_t::LdCtrlSetControlVariable_t(Base * parent) :
    Base(parent)
{
}

LdCtrlSetControlVariable_t::~LdCtrlSetControlVariable_t()
{
}

void LdCtrlSetControlVariable_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("AppliesTo")) {
            AppliesTo = attribute.value().toString();
            continue;
        }
        if (name == QString("InternalDescription")) {
            InternalDescription = attribute.value().toString();
            continue;
        }
        if (name == QString("Name")) {
            Name = attribute.value().toString();
            continue;
        }
        if (name == QString("Value")) {
            Value = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("OnError")) {
            auto * newOnError = make_LdCtrlBase_OnError_t(this);
            newOnError->read(reader);
            OnError.append(newOnError);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int LdCtrlSetControlVariable_t::tableColumnCount() const
{
    return 5;
}

QVariant LdCtrlSetControlVariable_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "LdCtrlSetControlVariable";
        }
        if (qualifiedName == QString("AppliesTo")) {
            return AppliesTo;
        }
        if (qualifiedName == QString("InternalDescription")) {
            return InternalDescription;
        }
        if (qualifiedName == QString("Name")) {
            return Name;
        }
        if (qualifiedName == QString("Value")) {
            return Value;
        }
        break;
    }
    return QVariant();
}

QVariant LdCtrlSetControlVariable_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "AppliesTo";
            case 2:
                return "InternalDescription";
            case 3:
                return "Name";
            case 4:
                return "Value";
            }
        }
    }
    return QVariant();
}

QVariant LdCtrlSetControlVariable_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return !Name.isEmpty() ? Name : "LdCtrlSetControlVariable";
    case Qt::DecorationRole:
        return QIcon::fromTheme("LdCtrlSetControlVariable");
    }
    return QVariant();
}

LdCtrlSetControlVariable_t * make_LdCtrlSetControlVariable_t(Base * parent)
{
    return new LdCtrlSetControlVariable_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
