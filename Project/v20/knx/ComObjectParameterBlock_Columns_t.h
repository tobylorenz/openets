/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/xs/ID.h>

namespace Project {
namespace v20 {
namespace knx {

/* forward declarations */
class ComObjectParameterBlock_Columns_Column_t;

class ComObjectParameterBlock_Columns_t : public Base
{
    Q_OBJECT

public:
    explicit ComObjectParameterBlock_Columns_t(Base * parent = nullptr);
    virtual ~ComObjectParameterBlock_Columns_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* elements */
    ComObjectParameterBlock_Columns_Column_t * Column{};
};

ComObjectParameterBlock_Columns_t * make_ComObjectParameterBlock_Columns_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
