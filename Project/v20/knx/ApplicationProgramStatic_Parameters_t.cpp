/* This file is generated. */

#include <Project/v20/knx/ApplicationProgramStatic_Parameters_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/ApplicationProgramStatic_Parameters_Parameter_t.h>
#include <Project/v20/knx/ApplicationProgramStatic_Parameters_Union_t.h>
#include <Project/v20/knx/KNX_t.h>

namespace Project {
namespace v20 {
namespace knx {

ApplicationProgramStatic_Parameters_t::ApplicationProgramStatic_Parameters_t(Base * parent) :
    Base(parent)
{
}

ApplicationProgramStatic_Parameters_t::~ApplicationProgramStatic_Parameters_t()
{
}

void ApplicationProgramStatic_Parameters_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("Parameter")) {
            auto * Parameter = make_ApplicationProgramStatic_Parameters_Parameter_t(this);
            Parameter->read(reader);
            continue;
        }
        if (reader.name() == QString("Union")) {
            auto * Union = make_ApplicationProgramStatic_Parameters_Union_t(this);
            Union->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ApplicationProgramStatic_Parameters_t::tableColumnCount() const
{
    return 1;
}

QVariant ApplicationProgramStatic_Parameters_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ApplicationProgramStatic_Parameters";
        }
        break;
    }
    return QVariant();
}

QVariant ApplicationProgramStatic_Parameters_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant ApplicationProgramStatic_Parameters_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "Parameters";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ApplicationProgramStatic_Parameters");
    }
    return QVariant();
}

ApplicationProgramStatic_Parameters_t * make_ApplicationProgramStatic_Parameters_t(Base * parent)
{
    return new ApplicationProgramStatic_Parameters_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
