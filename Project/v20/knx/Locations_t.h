/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v20/knx/Space_t.h>

namespace Project {
namespace v20 {
namespace knx {

class Locations_t : public Base
{
    Q_OBJECT

public:
    explicit Locations_t(Base * parent = nullptr);
    virtual ~Locations_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* elements */
    QMap<xs::ID, Space_t *> Space; // key: Id
};

Locations_t * make_Locations_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
