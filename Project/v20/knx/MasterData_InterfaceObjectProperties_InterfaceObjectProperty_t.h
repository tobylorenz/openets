/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v20/knx/AccessPolicy_t.h>
#include <Project/v20/knx/IDREF.h>
#include <Project/v20/knx/IDREFS.h>
#include <Project/v20/knx/LanguageDependentString255_t.h>
#include <Project/v20/knx/MasterData_PropertyDataTypes_PropertyDataType_t.h>
#include <Project/v20/knx/String255_t.h>
#include <Project/xs/ID.h>
#include <Project/xs/Boolean.h>
#include <Project/xs/UnsignedInt.h>

namespace Project {
namespace v20 {
namespace knx {

/* forward declarations */
class DatapointType_DatapointSubtypes_DatapointSubtype_t;
class MasterData_InterfaceObjectTypes_InterfaceObjectType_t;

class MasterData_InterfaceObjectProperties_InterfaceObjectProperty_t : public Base
{
    Q_OBJECT

public:
    explicit MasterData_InterfaceObjectProperties_InterfaceObjectProperty_t(Base * parent = nullptr);
    virtual ~MasterData_InterfaceObjectProperties_InterfaceObjectProperty_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::ID Id{};
    xs::UnsignedInt Number{};
    IDREF ObjectType{};
    String255_t Name{};
    LanguageDependentString255_t Text{};
    IDREFS PDT{};
    IDREF DPT{};
    xs::Boolean Array{"false"};
    AccessPolicy_t AccessPolicy{};

    /* getters */
    MasterData_InterfaceObjectTypes_InterfaceObjectType_t * getInterfaceObjectType() const; // attribute: ObjectType
    QList<MasterData_PropertyDataTypes_PropertyDataType_t *> getPropertyDataType() const; // attribute: PDT
    DatapointType_DatapointSubtypes_DatapointSubtype_t * getDatapointType() const; // attribute: DPT
};

MasterData_InterfaceObjectProperties_InterfaceObjectProperty_t * make_MasterData_InterfaceObjectProperties_InterfaceObjectProperty_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
