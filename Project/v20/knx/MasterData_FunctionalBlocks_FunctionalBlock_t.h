/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v20/knx/IDREF.h>
#include <Project/v20/knx/MasterData_FunctionalBlocks_FunctionalBlock_Parameters_t.h>
#include <Project/xs/ID.h>
#include <Project/xs/String.h>

namespace Project {
namespace v20 {
namespace knx {

class MasterData_FunctionalBlocks_FunctionalBlock_t : public Base
{
    Q_OBJECT

public:
    explicit MasterData_FunctionalBlocks_FunctionalBlock_t(Base * parent = nullptr);
    virtual ~MasterData_FunctionalBlocks_FunctionalBlock_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::String Name{};
    xs::ID Id{};

    /* elements */
    QMap<IDREF, MasterData_FunctionalBlocks_FunctionalBlock_Parameters_t *> Parameters; // key: ObjectType
};

MasterData_FunctionalBlocks_FunctionalBlock_t * make_MasterData_FunctionalBlocks_FunctionalBlock_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
