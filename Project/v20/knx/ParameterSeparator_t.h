/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v20/knx/Access_t.h>
#include <Project/v20/knx/CellRef_t.h>
#include <Project/v20/knx/IDREF.h>
#include <Project/v20/knx/LanguageDependentString255_t.h>
#include <Project/v20/knx/ParameterSeparator_UIHint_t.h>
#include <Project/v20/knx/String255_t.h>
#include <Project/xs/ID.h>
#include <Project/xs/String.h>

namespace Project {
namespace v20 {
namespace knx {

/* forward declarations */
class ParameterRef_t;

class ParameterSeparator_t : public Base
{
    Q_OBJECT

public:
    explicit ParameterSeparator_t(Base * parent = nullptr);
    virtual ~ParameterSeparator_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::ID Id{};
    String255_t Name{};
    LanguageDependentString255_t Text{};
    Access_t Access{"ReadWrite"};
    ParameterSeparator_UIHint_t UIHint{};
    IDREF TextParameterRefId{};
    xs::String InternalDescription{};
    CellRef_t Cell{};
    xs::String Icon{};

    /* getters */
    ParameterRef_t * getTextParameterRef() const; // attribute: TextParameterRefId
};

ParameterSeparator_t * make_ParameterSeparator_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
