/* This file is generated. */

#include <Project/v20/knx/LdCtrlCompareRelMem_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/KNX_t.h>

namespace Project {
namespace v20 {
namespace knx {

LdCtrlCompareRelMem_t::LdCtrlCompareRelMem_t(Base * parent) :
    Base(parent)
{
}

LdCtrlCompareRelMem_t::~LdCtrlCompareRelMem_t()
{
}

void LdCtrlCompareRelMem_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("AppliesTo")) {
            AppliesTo = attribute.value().toString();
            continue;
        }
        if (name == QString("InternalDescription")) {
            InternalDescription = attribute.value().toString();
            continue;
        }
        if (name == QString("AllowCachedValue")) {
            AllowCachedValue = attribute.value().toString();
            continue;
        }
        if (name == QString("InlineData")) {
            InlineData = attribute.value().toString();
            continue;
        }
        if (name == QString("Mask")) {
            Mask = attribute.value().toString();
            continue;
        }
        if (name == QString("Range")) {
            Range = attribute.value().toString();
            continue;
        }
        if (name == QString("Invert")) {
            Invert = attribute.value().toString();
            continue;
        }
        if (name == QString("RetryInterval")) {
            RetryInterval = attribute.value().toString();
            continue;
        }
        if (name == QString("TimeOut")) {
            TimeOut = attribute.value().toString();
            continue;
        }
        if (name == QString("ObjIdx")) {
            ObjIdx = attribute.value().toString();
            continue;
        }
        if (name == QString("ObjType")) {
            ObjType = attribute.value().toString();
            continue;
        }
        if (name == QString("Occurrence")) {
            Occurrence = attribute.value().toString();
            continue;
        }
        if (name == QString("Offset")) {
            Offset = attribute.value().toString();
            continue;
        }
        if (name == QString("Size")) {
            Size = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("OnError")) {
            auto * newOnError = make_LdCtrlBase_OnError_t(this);
            newOnError->read(reader);
            OnError.append(newOnError);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int LdCtrlCompareRelMem_t::tableColumnCount() const
{
    return 15;
}

QVariant LdCtrlCompareRelMem_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "LdCtrlCompareRelMem";
        }
        if (qualifiedName == QString("AppliesTo")) {
            return AppliesTo;
        }
        if (qualifiedName == QString("InternalDescription")) {
            return InternalDescription;
        }
        if (qualifiedName == QString("AllowCachedValue")) {
            return AllowCachedValue;
        }
        if (qualifiedName == QString("InlineData")) {
            return InlineData;
        }
        if (qualifiedName == QString("Mask")) {
            return Mask;
        }
        if (qualifiedName == QString("Range")) {
            return Range;
        }
        if (qualifiedName == QString("Invert")) {
            return Invert;
        }
        if (qualifiedName == QString("RetryInterval")) {
            return RetryInterval;
        }
        if (qualifiedName == QString("TimeOut")) {
            return TimeOut;
        }
        if (qualifiedName == QString("ObjIdx")) {
            return ObjIdx;
        }
        if (qualifiedName == QString("ObjType")) {
            return ObjType;
        }
        if (qualifiedName == QString("Occurrence")) {
            return Occurrence;
        }
        if (qualifiedName == QString("Offset")) {
            return Offset;
        }
        if (qualifiedName == QString("Size")) {
            return Size;
        }
        break;
    }
    return QVariant();
}

QVariant LdCtrlCompareRelMem_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "AppliesTo";
            case 2:
                return "InternalDescription";
            case 3:
                return "AllowCachedValue";
            case 4:
                return "InlineData";
            case 5:
                return "Mask";
            case 6:
                return "Range";
            case 7:
                return "Invert";
            case 8:
                return "RetryInterval";
            case 9:
                return "TimeOut";
            case 10:
                return "ObjIdx";
            case 11:
                return "ObjType";
            case 12:
                return "Occurrence";
            case 13:
                return "Offset";
            case 14:
                return "Size";
            }
        }
    }
    return QVariant();
}

QVariant LdCtrlCompareRelMem_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "LdCtrlCompareRelMem";
    case Qt::DecorationRole:
        return QIcon::fromTheme("LdCtrlCompareRelMem");
    }
    return QVariant();
}

LdCtrlCompareRelMem_t * make_LdCtrlCompareRelMem_t(Base * parent)
{
    return new LdCtrlCompareRelMem_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
