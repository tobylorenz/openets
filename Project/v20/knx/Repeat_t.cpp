/* This file is generated. */

#include <Project/v20/knx/Repeat_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/ComObjectParameterChoose_t.h>
#include <Project/v20/knx/KNX_t.h>
#include <Project/v20/knx/Module_t.h>
#include <Project/v20/knx/Repeat_t.h>

namespace Project {
namespace v20 {
namespace knx {

Repeat_t::Repeat_t(Base * parent) :
    Base(parent)
{
}

Repeat_t::~Repeat_t()
{
}

void Repeat_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == "Id") {
            Id = attribute.value().toString();
            KNX_t * knx = findParent<KNX_t *>();
            Q_ASSERT(knx);
            knx->ids[Id] = this;
            continue;
        }
        if (name == QString("Name")) {
            Name = attribute.value().toString();
            continue;
        }
        if (name == QString("InternalDescription")) {
            InternalDescription = attribute.value().toString();
            continue;
        }
        if (name == QString("ParameterRefId")) {
            QString value = attribute.value().toString();
            if (!value.isEmpty())
                qWarning() << "knx:Repeat_t attribute ParameterRefId references to" << value;
        }
        if (name == QString("ParameterRefId")) {
            ParameterRefId = attribute.value().toString();
            continue;
        }
        if (name == QString("Count")) {
            Count = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("choose")) {
            auto * Choose = make_ComObjectParameterChoose_t(this);
            Choose->read(reader);
            continue;
        }
        if (reader.name() == QString("Module")) {
            auto * Module = make_Module_t(this);
            Module->read(reader);
            continue;
        }
        if (reader.name() == QString("Repeat")) {
            auto * Repeat = make_Repeat_t(this);
            Repeat->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int Repeat_t::tableColumnCount() const
{
    return 6;
}

QVariant Repeat_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "Repeat";
        }
        if (qualifiedName == QString("Id")) {
            return Id;
        }
        if (qualifiedName == QString("Name")) {
            return Name;
        }
        if (qualifiedName == QString("InternalDescription")) {
            return InternalDescription;
        }
        if (qualifiedName == QString("ParameterRefId")) {
            return ParameterRefId;
        }
        if (qualifiedName == QString("Count")) {
            return Count;
        }
        break;
    }
    return QVariant();
}

QVariant Repeat_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Id";
            case 2:
                return "Name";
            case 3:
                return "InternalDescription";
            case 4:
                return "ParameterRefId";
            case 5:
                return "Count";
            }
        }
    }
    return QVariant();
}

QVariant Repeat_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return !Name.isEmpty() ? Name : "Repeat";
    case Qt::DecorationRole:
        return QIcon::fromTheme("Repeat");
    }
    return QVariant();
}

Base * Repeat_t::getParameter() const
{
    if (ParameterRefId.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<Base *>(knx->ids[ParameterRefId]);
}

Repeat_t * make_Repeat_t(Base * parent)
{
    return new Repeat_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
