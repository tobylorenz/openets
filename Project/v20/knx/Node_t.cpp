/* This file is generated. */

#include <Project/v20/knx/Node_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/KNX_t.h>
#include <Project/v20/knx/Node_Nodes_t.h>

namespace Project {
namespace v20 {
namespace knx {

Node_t::Node_t(Base * parent) :
    Base(parent)
{
}

Node_t::~Node_t()
{
}

void Node_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("Type")) {
            Type = attribute.value().toString();
            continue;
        }
        if (name == QString("RefId")) {
            RefId = attribute.value().toString();
            continue;
        }
        if (name == QString("Text")) {
            Text = attribute.value().toString();
            continue;
        }
        if (name == QString("GroupObjectInstances")) {
            GroupObjectInstances = attribute.value().toString().split(' ');
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("Nodes")) {
            if (!Nodes) {
                Nodes = make_Node_Nodes_t(this);
            }
            Nodes->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int Node_t::tableColumnCount() const
{
    return 5;
}

QVariant Node_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "Node";
        }
        if (qualifiedName == QString("Type")) {
            return Type;
        }
        if (qualifiedName == QString("RefId")) {
            return RefId;
        }
        if (qualifiedName == QString("Text")) {
            return Text;
        }
        if (qualifiedName == QString("GroupObjectInstances")) {
            return GroupObjectInstances.join(' ');
        }
        break;
    }
    return QVariant();
}

QVariant Node_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Type";
            case 2:
                return "RefId";
            case 3:
                return "Text";
            case 4:
                return "GroupObjectInstances";
            }
        }
    }
    return QVariant();
}

QVariant Node_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "Node";
    case Qt::DecorationRole:
        return QIcon::fromTheme("Node");
    }
    return QVariant();
}

QList<ComObjectRef_t *> Node_t::getGroupObjectInstances() const
{
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    QList<ComObjectRef_t *> retVal;
    for (QString id : GroupObjectInstances) {
        retVal << qobject_cast<ComObjectRef_t *>(knx->ids[id]);
    }
    return retVal;
}

Node_t * make_Node_t(Base * parent)
{
    return new Node_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
