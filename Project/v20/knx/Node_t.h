/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v20/knx/ComObjectRef_t.h>
#include <Project/v20/knx/Node_Type_t.h>
#include <Project/v20/knx/RELIDREF.h>
#include <Project/v20/knx/RELIDREFS.h>
#include <Project/xs/String.h>

namespace Project {
namespace v20 {
namespace knx {

/* forward declarations */
class Node_Nodes_t;

class Node_t : public Base
{
    Q_OBJECT

public:
    explicit Node_t(Base * parent = nullptr);
    virtual ~Node_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    Node_Type_t Type{};
    RELIDREF RefId{};
    xs::String Text{};
    RELIDREFS GroupObjectInstances{};

    /* elements */
    Node_Nodes_t * Nodes{};

    /* getters */
    // Base * getTODO() const; // attribute: RefId
    QList<ComObjectRef_t *> getGroupObjectInstances() const; // attribute: GroupObjectInstances
};

Node_t * make_Node_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
