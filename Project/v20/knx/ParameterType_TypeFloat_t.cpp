/* This file is generated. */

#include <Project/v20/knx/ParameterType_TypeFloat_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/KNX_t.h>

namespace Project {
namespace v20 {
namespace knx {

ParameterType_TypeFloat_t::ParameterType_TypeFloat_t(Base * parent) :
    Base(parent)
{
}

ParameterType_TypeFloat_t::~ParameterType_TypeFloat_t()
{
}

void ParameterType_TypeFloat_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("Encoding")) {
            Encoding = attribute.value().toString();
            continue;
        }
        if (name == QString("minInclusive")) {
            minInclusive = attribute.value().toString();
            continue;
        }
        if (name == QString("maxInclusive")) {
            maxInclusive = attribute.value().toString();
            continue;
        }
        if (name == QString("Increment")) {
            Increment = attribute.value().toString();
            continue;
        }
        if (name == QString("UIHint")) {
            UIHint = attribute.value().toString();
            continue;
        }
        if (name == QString("DisplayFormat")) {
            DisplayFormat = attribute.value().toString();
            continue;
        }
        if (name == QString("DisplayOffset")) {
            DisplayOffset = attribute.value().toString();
            continue;
        }
        if (name == QString("DisplayFactor")) {
            DisplayFactor = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ParameterType_TypeFloat_t::tableColumnCount() const
{
    return 9;
}

QVariant ParameterType_TypeFloat_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ParameterType_TypeFloat";
        }
        if (qualifiedName == QString("Encoding")) {
            return Encoding;
        }
        if (qualifiedName == QString("minInclusive")) {
            return minInclusive;
        }
        if (qualifiedName == QString("maxInclusive")) {
            return maxInclusive;
        }
        if (qualifiedName == QString("Increment")) {
            return Increment;
        }
        if (qualifiedName == QString("UIHint")) {
            return UIHint;
        }
        if (qualifiedName == QString("DisplayFormat")) {
            return DisplayFormat;
        }
        if (qualifiedName == QString("DisplayOffset")) {
            return DisplayOffset;
        }
        if (qualifiedName == QString("DisplayFactor")) {
            return DisplayFactor;
        }
        break;
    }
    return QVariant();
}

QVariant ParameterType_TypeFloat_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Encoding";
            case 2:
                return "minInclusive";
            case 3:
                return "maxInclusive";
            case 4:
                return "Increment";
            case 5:
                return "UIHint";
            case 6:
                return "DisplayFormat";
            case 7:
                return "DisplayOffset";
            case 8:
                return "DisplayFactor";
            }
        }
    }
    return QVariant();
}

QVariant ParameterType_TypeFloat_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "TypeFloat";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ParameterType_TypeFloat");
    }
    return QVariant();
}

ParameterType_TypeFloat_t * make_ParameterType_TypeFloat_t(Base * parent)
{
    return new ParameterType_TypeFloat_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
