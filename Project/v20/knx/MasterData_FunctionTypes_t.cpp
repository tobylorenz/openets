/* This file is generated. */

#include <Project/v20/knx/MasterData_FunctionTypes_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/KNX_t.h>

namespace Project {
namespace v20 {
namespace knx {

MasterData_FunctionTypes_t::MasterData_FunctionTypes_t(Base * parent) :
    Base(parent)
{
}

MasterData_FunctionTypes_t::~MasterData_FunctionTypes_t()
{
}

void MasterData_FunctionTypes_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("FunctionsGroup")) {
            QString newId = reader.attributes().value("Id").toString();
            Q_ASSERT(!newId.isEmpty());
            FunctionsGroup_t * newFunctionsGroup;
            if (FunctionsGroup.contains(newId)) {
                newFunctionsGroup = FunctionsGroup[newId];
            } else {
                newFunctionsGroup = make_FunctionsGroup_t(this);
                FunctionsGroup[newId] = newFunctionsGroup;
            }
            newFunctionsGroup->read(reader);
            continue;
        }
        if (reader.name() == QString("FunctionType")) {
            QString newId = reader.attributes().value("Id").toString();
            Q_ASSERT(!newId.isEmpty());
            FunctionType_t * newFunctionType;
            if (FunctionType.contains(newId)) {
                newFunctionType = FunctionType[newId];
            } else {
                newFunctionType = make_FunctionType_t(this);
                FunctionType[newId] = newFunctionType;
            }
            newFunctionType->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int MasterData_FunctionTypes_t::tableColumnCount() const
{
    return 1;
}

QVariant MasterData_FunctionTypes_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "MasterData_FunctionTypes";
        }
        break;
    }
    return QVariant();
}

QVariant MasterData_FunctionTypes_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant MasterData_FunctionTypes_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "FunctionTypes";
    case Qt::DecorationRole:
        return QIcon::fromTheme("MasterData_FunctionTypes");
    }
    return QVariant();
}

MasterData_FunctionTypes_t * make_MasterData_FunctionTypes_t(Base * parent)
{
    return new MasterData_FunctionTypes_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
