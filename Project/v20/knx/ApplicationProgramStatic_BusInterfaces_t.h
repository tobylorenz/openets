/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v20/knx/ApplicationProgramStatic_BusInterfaces_BusInterface_t.h>
#include <Project/xs/ID.h>

namespace Project {
namespace v20 {
namespace knx {

class ApplicationProgramStatic_BusInterfaces_t : public Base
{
    Q_OBJECT

public:
    explicit ApplicationProgramStatic_BusInterfaces_t(Base * parent = nullptr);
    virtual ~ApplicationProgramStatic_BusInterfaces_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* elements */
    QMap<xs::ID, ApplicationProgramStatic_BusInterfaces_BusInterface_t *> BusInterface; // key: Id
};

ApplicationProgramStatic_BusInterfaces_t * make_ApplicationProgramStatic_BusInterfaces_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
