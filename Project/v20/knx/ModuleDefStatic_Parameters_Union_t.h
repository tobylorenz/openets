/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v20/knx/IDREF.h>
#include <Project/v20/knx/ModuleDefStatic_Parameters_Union_SizeInBit_t.h>
#include <Project/v20/knx/UnionParameter_t.h>

namespace Project {
namespace v20 {
namespace knx {

/* forward declarations */
class ModuleDefStatic_Parameters_Union_Memory_t;
class ModuleDefStatic_Parameters_Union_Property_t;

class ModuleDefStatic_Parameters_Union_t : public Base
{
    Q_OBJECT

public:
    explicit ModuleDefStatic_Parameters_Union_t(Base * parent = nullptr);
    virtual ~ModuleDefStatic_Parameters_Union_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    ModuleDefStatic_Parameters_Union_SizeInBit_t SizeInBit{};

    /* elements */
    // xs:choice ModuleDefStatic_Parameters_Union_Memory_t * Memory{};
    // xs:choice ModuleDefStatic_Parameters_Union_Property_t * Property{};
    QMap<xs::ID, UnionParameter_t *> Parameter; // key: Id
};

ModuleDefStatic_Parameters_Union_t * make_ModuleDefStatic_Parameters_Union_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
