/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v20/knx/IDREF.h>
#include <Project/v20/knx/ModuleDefLdCtrlBaseChoose_when_t.h>
#include <Project/xs/String.h>

namespace Project {
namespace v20 {
namespace knx {

class ModuleDefLdCtrlBaseChoose_t : public Base
{
    Q_OBJECT

public:
    explicit ModuleDefLdCtrlBaseChoose_t(Base * parent = nullptr);
    virtual ~ModuleDefLdCtrlBaseChoose_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    IDREF ParamRefId{};
    xs::String InternalDescription{};

    /* elements */
    QVector<ModuleDefLdCtrlBaseChoose_when_t *> When;

    /* getters */
    Base * getParameter() const; // attribute: ParamRefId
};

ModuleDefLdCtrlBaseChoose_t * make_ModuleDefLdCtrlBaseChoose_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
