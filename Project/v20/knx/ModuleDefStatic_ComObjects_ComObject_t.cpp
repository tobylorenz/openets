/* This file is generated. */

#include <Project/v20/knx/ModuleDefStatic_ComObjects_ComObject_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/KNX_t.h>
#include <Project/v20/knx/ModuleDef_Arguments_Argument_t.h>

namespace Project {
namespace v20 {
namespace knx {

ModuleDefStatic_ComObjects_ComObject_t::ModuleDefStatic_ComObjects_ComObject_t(Base * parent) :
    Base(parent)
{
}

ModuleDefStatic_ComObjects_ComObject_t::~ModuleDefStatic_ComObjects_ComObject_t()
{
}

void ModuleDefStatic_ComObjects_ComObject_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == "Id") {
            Id = attribute.value().toString();
            KNX_t * knx = findParent<KNX_t *>();
            Q_ASSERT(knx);
            knx->ids[Id] = this;
            continue;
        }
        if (name == QString("Name")) {
            Name = attribute.value().toString();
            continue;
        }
        if (name == QString("Text")) {
            Text = attribute.value().toString();
            continue;
        }
        if (name == QString("Number")) {
            Number = attribute.value().toString();
            continue;
        }
        if (name == QString("FunctionText")) {
            FunctionText = attribute.value().toString();
            continue;
        }
        if (name == QString("Priority")) {
            Priority = attribute.value().toString();
            continue;
        }
        if (name == QString("ObjectSize")) {
            ObjectSize = attribute.value().toString();
            continue;
        }
        if (name == QString("ReadFlag")) {
            ReadFlag = attribute.value().toString();
            continue;
        }
        if (name == QString("WriteFlag")) {
            WriteFlag = attribute.value().toString();
            continue;
        }
        if (name == QString("CommunicationFlag")) {
            CommunicationFlag = attribute.value().toString();
            continue;
        }
        if (name == QString("TransmitFlag")) {
            TransmitFlag = attribute.value().toString();
            continue;
        }
        if (name == QString("UpdateFlag")) {
            UpdateFlag = attribute.value().toString();
            continue;
        }
        if (name == QString("ReadOnInitFlag")) {
            ReadOnInitFlag = attribute.value().toString();
            continue;
        }
        if (name == QString("DatapointType")) {
            DatapointType = attribute.value().toString().split(' ');
            continue;
        }
        if (name == QString("InternalDescription")) {
            InternalDescription = attribute.value().toString();
            continue;
        }
        if (name == QString("SecurityRequired")) {
            SecurityRequired = attribute.value().toString();
            continue;
        }
        if (name == QString("MayRead")) {
            MayRead = attribute.value().toString();
            continue;
        }
        if (name == QString("ReadFlagLocked")) {
            ReadFlagLocked = attribute.value().toString();
            continue;
        }
        if (name == QString("WriteFlagLocked")) {
            WriteFlagLocked = attribute.value().toString();
            continue;
        }
        if (name == QString("TransmitFlagLocked")) {
            TransmitFlagLocked = attribute.value().toString();
            continue;
        }
        if (name == QString("UpdateFlagLocked")) {
            UpdateFlagLocked = attribute.value().toString();
            continue;
        }
        if (name == QString("ReadOnInitFlagLocked")) {
            ReadOnInitFlagLocked = attribute.value().toString();
            continue;
        }
        if (name == QString("BaseNumber")) {
            BaseNumber = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ModuleDefStatic_ComObjects_ComObject_t::tableColumnCount() const
{
    return 24;
}

QVariant ModuleDefStatic_ComObjects_ComObject_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ModuleDefStatic_ComObjects_ComObject";
        }
        if (qualifiedName == QString("Id")) {
            return Id;
        }
        if (qualifiedName == QString("Name")) {
            return Name;
        }
        if (qualifiedName == QString("Text")) {
            return Text;
        }
        if (qualifiedName == QString("Number")) {
            return Number;
        }
        if (qualifiedName == QString("FunctionText")) {
            return FunctionText;
        }
        if (qualifiedName == QString("Priority")) {
            return Priority;
        }
        if (qualifiedName == QString("ObjectSize")) {
            return ObjectSize;
        }
        if (qualifiedName == QString("ReadFlag")) {
            return ReadFlag;
        }
        if (qualifiedName == QString("WriteFlag")) {
            return WriteFlag;
        }
        if (qualifiedName == QString("CommunicationFlag")) {
            return CommunicationFlag;
        }
        if (qualifiedName == QString("TransmitFlag")) {
            return TransmitFlag;
        }
        if (qualifiedName == QString("UpdateFlag")) {
            return UpdateFlag;
        }
        if (qualifiedName == QString("ReadOnInitFlag")) {
            return ReadOnInitFlag;
        }
        if (qualifiedName == QString("DatapointType")) {
            return DatapointType.join(' ');
        }
        if (qualifiedName == QString("InternalDescription")) {
            return InternalDescription;
        }
        if (qualifiedName == QString("SecurityRequired")) {
            return SecurityRequired;
        }
        if (qualifiedName == QString("MayRead")) {
            return MayRead;
        }
        if (qualifiedName == QString("ReadFlagLocked")) {
            return ReadFlagLocked;
        }
        if (qualifiedName == QString("WriteFlagLocked")) {
            return WriteFlagLocked;
        }
        if (qualifiedName == QString("TransmitFlagLocked")) {
            return TransmitFlagLocked;
        }
        if (qualifiedName == QString("UpdateFlagLocked")) {
            return UpdateFlagLocked;
        }
        if (qualifiedName == QString("ReadOnInitFlagLocked")) {
            return ReadOnInitFlagLocked;
        }
        if (qualifiedName == QString("BaseNumber")) {
            return BaseNumber;
        }
        break;
    }
    return QVariant();
}

QVariant ModuleDefStatic_ComObjects_ComObject_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Id";
            case 2:
                return "Name";
            case 3:
                return "Text";
            case 4:
                return "Number";
            case 5:
                return "FunctionText";
            case 6:
                return "Priority";
            case 7:
                return "ObjectSize";
            case 8:
                return "ReadFlag";
            case 9:
                return "WriteFlag";
            case 10:
                return "CommunicationFlag";
            case 11:
                return "TransmitFlag";
            case 12:
                return "UpdateFlag";
            case 13:
                return "ReadOnInitFlag";
            case 14:
                return "DatapointType";
            case 15:
                return "InternalDescription";
            case 16:
                return "SecurityRequired";
            case 17:
                return "MayRead";
            case 18:
                return "ReadFlagLocked";
            case 19:
                return "WriteFlagLocked";
            case 20:
                return "TransmitFlagLocked";
            case 21:
                return "UpdateFlagLocked";
            case 22:
                return "ReadOnInitFlagLocked";
            case 23:
                return "BaseNumber";
            }
        }
    }
    return QVariant();
}

QVariant ModuleDefStatic_ComObjects_ComObject_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return !Name.isEmpty() ? Name : "ComObject";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ModuleDefStatic_ComObjects_ComObject");
    }
    return QVariant();
}

QList<DatapointType_DatapointSubtypes_DatapointSubtype_t *> ModuleDefStatic_ComObjects_ComObject_t::getDatapointType() const
{
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    QList<DatapointType_DatapointSubtypes_DatapointSubtype_t *> retVal;
    for (QString id : DatapointType) {
        retVal << qobject_cast<DatapointType_DatapointSubtypes_DatapointSubtype_t *>(knx->ids[id]);
    }
    return retVal;
}

ModuleDef_Arguments_Argument_t * ModuleDefStatic_ComObjects_ComObject_t::getBaseNumber() const
{
    if (BaseNumber.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<ModuleDef_Arguments_Argument_t *>(knx->ids[BaseNumber]);
}

ModuleDefStatic_ComObjects_ComObject_t * make_ModuleDefStatic_ComObjects_ComObject_t(Base * parent)
{
    return new ModuleDefStatic_ComObjects_ComObject_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
