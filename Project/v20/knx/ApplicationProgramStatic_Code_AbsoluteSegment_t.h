/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/SimpleElementTextType.h>
#include <Project/v20/knx/ApplicationProgramStatic_Code_AbsoluteSegment_Address_t.h>
#include <Project/v20/knx/MemoryType_t.h>
#include <Project/v20/knx/SegmentBase_Size_t.h>
#include <Project/v20/knx/String255_t.h>
#include <Project/xs/ID.h>
#include <Project/xs/Base64Binary.h>
#include <Project/xs/Boolean.h>
#include <Project/xs/String.h>

namespace Project {
namespace v20 {
namespace knx {

class ApplicationProgramStatic_Code_AbsoluteSegment_t : public Base
{
    Q_OBJECT

public:
    explicit ApplicationProgramStatic_Code_AbsoluteSegment_t(Base * parent = nullptr);
    virtual ~ApplicationProgramStatic_Code_AbsoluteSegment_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::ID Id{};
    String255_t Name{};
    SegmentBase_Size_t Size{};
    xs::String InternalDescription{};
    MemoryType_t MemoryType{};
    ApplicationProgramStatic_Code_AbsoluteSegment_Address_t Address{};
    xs::Boolean UserMemory{"false"};

    /* elements */
    SimpleElementTextType * Data{};
    SimpleElementTextType * Mask{};
};

ApplicationProgramStatic_Code_AbsoluteSegment_t * make_ApplicationProgramStatic_Code_AbsoluteSegment_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
