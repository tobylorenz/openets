/* This file is generated. */

#include <Project/v20/knx/ApplicationProgramStatic_ComObjectTable_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/ApplicationProgramStatic_Code_AbsoluteSegment_t.h>
#include <Project/v20/knx/KNX_t.h>

namespace Project {
namespace v20 {
namespace knx {

ApplicationProgramStatic_ComObjectTable_t::ApplicationProgramStatic_ComObjectTable_t(Base * parent) :
    Base(parent)
{
}

ApplicationProgramStatic_ComObjectTable_t::~ApplicationProgramStatic_ComObjectTable_t()
{
}

void ApplicationProgramStatic_ComObjectTable_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("CodeSegment")) {
            CodeSegment = attribute.value().toString();
            continue;
        }
        if (name == QString("Offset")) {
            Offset = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("ComObject")) {
            QString newId = reader.attributes().value("Id").toString();
            Q_ASSERT(!newId.isEmpty());
            ComObject_t * newComObject;
            if (ComObject.contains(newId)) {
                newComObject = ComObject[newId];
            } else {
                newComObject = make_ComObject_t(this);
                ComObject[newId] = newComObject;
            }
            newComObject->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ApplicationProgramStatic_ComObjectTable_t::tableColumnCount() const
{
    return 3;
}

QVariant ApplicationProgramStatic_ComObjectTable_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ApplicationProgramStatic_ComObjectTable";
        }
        if (qualifiedName == QString("CodeSegment")) {
            return CodeSegment;
        }
        if (qualifiedName == QString("Offset")) {
            return Offset;
        }
        break;
    }
    return QVariant();
}

QVariant ApplicationProgramStatic_ComObjectTable_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "CodeSegment";
            case 2:
                return "Offset";
            }
        }
    }
    return QVariant();
}

QVariant ApplicationProgramStatic_ComObjectTable_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "ComObjectTable";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ApplicationProgramStatic_ComObjectTable");
    }
    return QVariant();
}

ApplicationProgramStatic_Code_AbsoluteSegment_t * ApplicationProgramStatic_ComObjectTable_t::getCodeSegment() const
{
    if (CodeSegment.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<ApplicationProgramStatic_Code_AbsoluteSegment_t *>(knx->ids[CodeSegment]);
}

ApplicationProgramStatic_ComObjectTable_t * make_ApplicationProgramStatic_ComObjectTable_t(Base * parent)
{
    return new ApplicationProgramStatic_ComObjectTable_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
