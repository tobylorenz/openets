/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v20/knx/ComObjectParameterChoose_when_t.h>
#include <Project/v20/knx/IDREF.h>
#include <Project/xs/String.h>

namespace Project {
namespace v20 {
namespace knx {

/* forward declarations */
class ParameterRef_t;

class ComObjectParameterChoose_t : public Base
{
    Q_OBJECT

public:
    explicit ComObjectParameterChoose_t(Base * parent = nullptr);
    virtual ~ComObjectParameterChoose_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    IDREF ParamRefId{};
    xs::String InternalDescription{};

    /* elements */
    QVector<ComObjectParameterChoose_when_t *> When;

    /* getters */
    ParameterRef_t * getParameterRef() const; // attribute: ParamRefId
};

ComObjectParameterChoose_t * make_ComObjectParameterChoose_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
