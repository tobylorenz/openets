/* This file is generated. */

#include <Project/v20/knx/PropertyUnion_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/KNX_t.h>

namespace Project {
namespace v20 {
namespace knx {

PropertyUnion_t::PropertyUnion_t(Base * parent) :
    Base(parent)
{
}

PropertyUnion_t::~PropertyUnion_t()
{
}

void PropertyUnion_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("ObjectIndex")) {
            ObjectIndex = attribute.value().toString();
            continue;
        }
        if (name == QString("ObjectType")) {
            ObjectType = attribute.value().toString();
            continue;
        }
        if (name == QString("Occurrence")) {
            Occurrence = attribute.value().toString();
            continue;
        }
        if (name == QString("PropertyId")) {
            PropertyId = attribute.value().toString();
            continue;
        }
        if (name == QString("Offset")) {
            Offset = attribute.value().toString();
            continue;
        }
        if (name == QString("BitOffset")) {
            BitOffset = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int PropertyUnion_t::tableColumnCount() const
{
    return 7;
}

QVariant PropertyUnion_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "PropertyUnion";
        }
        if (qualifiedName == QString("ObjectIndex")) {
            return ObjectIndex;
        }
        if (qualifiedName == QString("ObjectType")) {
            return ObjectType;
        }
        if (qualifiedName == QString("Occurrence")) {
            return Occurrence;
        }
        if (qualifiedName == QString("PropertyId")) {
            return PropertyId;
        }
        if (qualifiedName == QString("Offset")) {
            return Offset;
        }
        if (qualifiedName == QString("BitOffset")) {
            return BitOffset;
        }
        break;
    }
    return QVariant();
}

QVariant PropertyUnion_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "ObjectIndex";
            case 2:
                return "ObjectType";
            case 3:
                return "Occurrence";
            case 4:
                return "PropertyId";
            case 5:
                return "Offset";
            case 6:
                return "BitOffset";
            }
        }
    }
    return QVariant();
}

QVariant PropertyUnion_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "PropertyUnion";
    case Qt::DecorationRole:
        return QIcon::fromTheme("PropertyUnion");
    }
    return QVariant();
}

PropertyUnion_t * make_PropertyUnion_t(Base * parent)
{
    return new PropertyUnion_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
