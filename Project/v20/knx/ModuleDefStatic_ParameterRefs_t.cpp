/* This file is generated. */

#include <Project/v20/knx/ModuleDefStatic_ParameterRefs_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/KNX_t.h>

namespace Project {
namespace v20 {
namespace knx {

ModuleDefStatic_ParameterRefs_t::ModuleDefStatic_ParameterRefs_t(Base * parent) :
    Base(parent)
{
}

ModuleDefStatic_ParameterRefs_t::~ModuleDefStatic_ParameterRefs_t()
{
}

void ModuleDefStatic_ParameterRefs_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("ParameterRef")) {
            QString newId = reader.attributes().value("Id").toString();
            Q_ASSERT(!newId.isEmpty());
            ParameterRef_t * newParameterRef;
            if (ParameterRef.contains(newId)) {
                newParameterRef = ParameterRef[newId];
            } else {
                newParameterRef = make_ParameterRef_t(this);
                ParameterRef[newId] = newParameterRef;
            }
            newParameterRef->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ModuleDefStatic_ParameterRefs_t::tableColumnCount() const
{
    return 1;
}

QVariant ModuleDefStatic_ParameterRefs_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ModuleDefStatic_ParameterRefs";
        }
        break;
    }
    return QVariant();
}

QVariant ModuleDefStatic_ParameterRefs_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant ModuleDefStatic_ParameterRefs_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "ParameterRefs";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ModuleDefStatic_ParameterRefs");
    }
    return QVariant();
}

ModuleDefStatic_ParameterRefs_t * make_ModuleDefStatic_ParameterRefs_t(Base * parent)
{
    return new ModuleDefStatic_ParameterRefs_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
