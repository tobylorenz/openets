/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/SimpleElementTextType.h>
#include <Project/v20/knx/SegmentBase_Size_t.h>
#include <Project/v20/knx/String255_t.h>
#include <Project/xs/ID.h>
#include <Project/xs/Base64Binary.h>
#include <Project/xs/String.h>

namespace Project {
namespace v20 {
namespace knx {

class SegmentBase_t : public Base
{
    Q_OBJECT

public:
    explicit SegmentBase_t(Base * parent = nullptr);
    virtual ~SegmentBase_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::ID Id{};
    String255_t Name{};
    SegmentBase_Size_t Size{};
    xs::String InternalDescription{};

    /* elements */
    SimpleElementTextType * Data{};
    SimpleElementTextType * Mask{};
};

SegmentBase_t * make_SegmentBase_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
