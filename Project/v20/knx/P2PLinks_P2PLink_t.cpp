/* This file is generated. */

#include <Project/v20/knx/P2PLinks_P2PLink_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/KNX_t.h>
#include <Project/v20/knx/P2PLinkBusInterfaceEndpoint_t.h>
#include <Project/v20/knx/P2PLinkDeviceEndpoint_t.h>

namespace Project {
namespace v20 {
namespace knx {

P2PLinks_P2PLink_t::P2PLinks_P2PLink_t(Base * parent) :
    Base(parent)
{
}

P2PLinks_P2PLink_t::~P2PLinks_P2PLink_t()
{
}

void P2PLinks_P2PLink_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == "Id") {
            Id = attribute.value().toString();
            KNX_t * knx = findParent<KNX_t *>();
            Q_ASSERT(knx);
            knx->ids[Id] = this;
            continue;
        }
        if (name == QString("Name")) {
            Name = attribute.value().toString();
            continue;
        }
        if (name == QString("Description")) {
            Description = attribute.value().toString();
            continue;
        }
        if (name == QString("Comment")) {
            Comment = attribute.value().toString();
            continue;
        }
        if (name == QString("Puid")) {
            Puid = attribute.value().toString();
            continue;
        }
        if (name == QString("Key")) {
            Key = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("DeviceEndpoint")) {
            auto * DeviceEndpoint = make_P2PLinkDeviceEndpoint_t(this);
            DeviceEndpoint->read(reader);
            continue;
        }
        if (reader.name() == QString("BusInterfaceEndpoint")) {
            auto * BusInterfaceEndpoint = make_P2PLinkBusInterfaceEndpoint_t(this);
            BusInterfaceEndpoint->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int P2PLinks_P2PLink_t::tableColumnCount() const
{
    return 7;
}

QVariant P2PLinks_P2PLink_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "P2PLinks_P2PLink";
        }
        if (qualifiedName == QString("Id")) {
            return Id;
        }
        if (qualifiedName == QString("Name")) {
            return Name;
        }
        if (qualifiedName == QString("Description")) {
            return Description;
        }
        if (qualifiedName == QString("Comment")) {
            return Comment;
        }
        if (qualifiedName == QString("Puid")) {
            return Puid;
        }
        if (qualifiedName == QString("Key")) {
            return Key;
        }
        break;
    }
    return QVariant();
}

QVariant P2PLinks_P2PLink_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Id";
            case 2:
                return "Name";
            case 3:
                return "Description";
            case 4:
                return "Comment";
            case 5:
                return "Puid";
            case 6:
                return "Key";
            }
        }
    }
    return QVariant();
}

QVariant P2PLinks_P2PLink_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return !Name.isEmpty() ? Name : "P2PLink";
    case Qt::DecorationRole:
        return QIcon::fromTheme("P2PLinks_P2PLink");
    }
    return QVariant();
}

P2PLinks_P2PLink_t * make_P2PLinks_P2PLink_t(Base * parent)
{
    return new P2PLinks_P2PLink_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
