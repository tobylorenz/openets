/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v20/knx/ModuleInstance_RepeatIndex_t.h>
#include <Project/v20/knx/RELID.h>
#include <Project/v20/knx/RELIDREF.h>

namespace Project {
namespace v20 {
namespace knx {

/* forward declarations */
class ModuleInstance_Arguments_t;

class ModuleInstance_t : public Base
{
    Q_OBJECT

public:
    explicit ModuleInstance_t(Base * parent = nullptr);
    virtual ~ModuleInstance_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    RELID Id{};
    RELIDREF RefId{};
    ModuleInstance_RepeatIndex_t RepeatIndex{};

    /* elements */
    ModuleInstance_Arguments_t * Arguments{};

    /* getters */
    // Base * getTODO() const; // attribute: RefId
};

ModuleInstance_t * make_ModuleInstance_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
