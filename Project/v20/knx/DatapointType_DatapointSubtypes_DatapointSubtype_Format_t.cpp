/* This file is generated. */

#include <Project/v20/knx/DatapointType_DatapointSubtypes_DatapointSubtype_Format_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/DatapointType_DatapointSubtypes_DatapointSubtype_Format_Bit_t.h>
#include <Project/v20/knx/DatapointType_DatapointSubtypes_DatapointSubtype_Format_Enumeration_t.h>
#include <Project/v20/knx/DatapointType_DatapointSubtypes_DatapointSubtype_Format_Float_t.h>
#include <Project/v20/knx/DatapointType_DatapointSubtypes_DatapointSubtype_Format_RefType_t.h>
#include <Project/v20/knx/DatapointType_DatapointSubtypes_DatapointSubtype_Format_Reserved_t.h>
#include <Project/v20/knx/DatapointType_DatapointSubtypes_DatapointSubtype_Format_SignedInteger_t.h>
#include <Project/v20/knx/DatapointType_DatapointSubtypes_DatapointSubtype_Format_String_t.h>
#include <Project/v20/knx/DatapointType_DatapointSubtypes_DatapointSubtype_Format_UnsignedInteger_t.h>
#include <Project/v20/knx/KNX_t.h>

namespace Project {
namespace v20 {
namespace knx {

DatapointType_DatapointSubtypes_DatapointSubtype_Format_t::DatapointType_DatapointSubtypes_DatapointSubtype_Format_t(Base * parent) :
    Base(parent)
{
}

DatapointType_DatapointSubtypes_DatapointSubtype_Format_t::~DatapointType_DatapointSubtypes_DatapointSubtype_Format_t()
{
}

void DatapointType_DatapointSubtypes_DatapointSubtype_Format_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("Bit")) {
            auto * Bit = make_DatapointType_DatapointSubtypes_DatapointSubtype_Format_Bit_t(this);
            Bit->read(reader);
            continue;
        }
        if (reader.name() == QString("UnsignedInteger")) {
            auto * UnsignedInteger = make_DatapointType_DatapointSubtypes_DatapointSubtype_Format_UnsignedInteger_t(this);
            UnsignedInteger->read(reader);
            continue;
        }
        if (reader.name() == QString("SignedInteger")) {
            auto * SignedInteger = make_DatapointType_DatapointSubtypes_DatapointSubtype_Format_SignedInteger_t(this);
            SignedInteger->read(reader);
            continue;
        }
        if (reader.name() == QString("String")) {
            auto * String = make_DatapointType_DatapointSubtypes_DatapointSubtype_Format_String_t(this);
            String->read(reader);
            continue;
        }
        if (reader.name() == QString("Float")) {
            auto * Float = make_DatapointType_DatapointSubtypes_DatapointSubtype_Format_Float_t(this);
            Float->read(reader);
            continue;
        }
        if (reader.name() == QString("Enumeration")) {
            auto * Enumeration = make_DatapointType_DatapointSubtypes_DatapointSubtype_Format_Enumeration_t(this);
            Enumeration->read(reader);
            continue;
        }
        if (reader.name() == QString("Reserved")) {
            auto * Reserved = make_DatapointType_DatapointSubtypes_DatapointSubtype_Format_Reserved_t(this);
            Reserved->read(reader);
            continue;
        }
        if (reader.name() == QString("RefType")) {
            auto * RefType = make_DatapointType_DatapointSubtypes_DatapointSubtype_Format_RefType_t(this);
            RefType->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int DatapointType_DatapointSubtypes_DatapointSubtype_Format_t::tableColumnCount() const
{
    return 1;
}

QVariant DatapointType_DatapointSubtypes_DatapointSubtype_Format_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "DatapointType_DatapointSubtypes_DatapointSubtype_Format";
        }
        break;
    }
    return QVariant();
}

QVariant DatapointType_DatapointSubtypes_DatapointSubtype_Format_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant DatapointType_DatapointSubtypes_DatapointSubtype_Format_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "Format";
    case Qt::DecorationRole:
        return QIcon::fromTheme("DatapointType_DatapointSubtypes_DatapointSubtype_Format");
    }
    return QVariant();
}

DatapointType_DatapointSubtypes_DatapointSubtype_Format_t * make_DatapointType_DatapointSubtypes_DatapointSubtype_Format_t(Base * parent)
{
    return new DatapointType_DatapointSubtypes_DatapointSubtype_Format_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
