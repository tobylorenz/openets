/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v20/knx/BitOffset_t.h>
#include <Project/xs/UnsignedByte.h>
#include <Project/xs/UnsignedInt.h>
#include <Project/xs/UnsignedShort.h>

namespace Project {
namespace v20 {
namespace knx {

class PropertyUnion_t : public Base
{
    Q_OBJECT

public:
    explicit PropertyUnion_t(Base * parent = nullptr);
    virtual ~PropertyUnion_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::UnsignedByte ObjectIndex{};
    xs::UnsignedShort ObjectType{};
    xs::UnsignedShort Occurrence{"0"};
    xs::UnsignedShort PropertyId{};
    xs::UnsignedInt Offset{};
    BitOffset_t BitOffset{};
};

PropertyUnion_t * make_PropertyUnion_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
