/* This file is generated. */

#include <Project/v20/knx/FunctionType_FunctionPoint_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/DatapointRole_t.h>
#include <Project/v20/knx/DatapointType_DatapointSubtypes_DatapointSubtype_t.h>
#include <Project/v20/knx/KNX_t.h>

namespace Project {
namespace v20 {
namespace knx {

FunctionType_FunctionPoint_t::FunctionType_FunctionPoint_t(Base * parent) :
    Base(parent)
{
}

FunctionType_FunctionPoint_t::~FunctionType_FunctionPoint_t()
{
}

void FunctionType_FunctionPoint_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == "Id") {
            Id = attribute.value().toString();
            KNX_t * knx = findParent<KNX_t *>();
            Q_ASSERT(knx);
            knx->ids[Id] = this;
            continue;
        }
        if (name == QString("Text")) {
            Text = attribute.value().toString();
            continue;
        }
        if (name == QString("Role")) {
            Role = attribute.value().toString();
            continue;
        }
        if (name == QString("DatapointType")) {
            DatapointType = attribute.value().toString();
            continue;
        }
        if (name == QString("Characteristics")) {
            Characteristics = attribute.value().toString().split(' ');
            continue;
        }
        if (name == QString("Semantics")) {
            Semantics = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int FunctionType_FunctionPoint_t::tableColumnCount() const
{
    return 7;
}

QVariant FunctionType_FunctionPoint_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "FunctionType_FunctionPoint";
        }
        if (qualifiedName == QString("Id")) {
            return Id;
        }
        if (qualifiedName == QString("Text")) {
            return Text;
        }
        if (qualifiedName == QString("Role")) {
            return Role;
        }
        if (qualifiedName == QString("DatapointType")) {
            return DatapointType;
        }
        if (qualifiedName == QString("Characteristics")) {
            return Characteristics.join(' ');
        }
        if (qualifiedName == QString("Semantics")) {
            return Semantics;
        }
        break;
    }
    return QVariant();
}

QVariant FunctionType_FunctionPoint_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Id";
            case 2:
                return "Text";
            case 3:
                return "Role";
            case 4:
                return "DatapointType";
            case 5:
                return "Characteristics";
            case 6:
                return "Semantics";
            }
        }
    }
    return QVariant();
}

QVariant FunctionType_FunctionPoint_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "FunctionPoint";
    case Qt::DecorationRole:
        return QIcon::fromTheme("FunctionType_FunctionPoint");
    }
    return QVariant();
}

DatapointRole_t * FunctionType_FunctionPoint_t::getRole() const
{
    if (Role.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<DatapointRole_t *>(knx->ids[Role]);
}

DatapointType_DatapointSubtypes_DatapointSubtype_t * FunctionType_FunctionPoint_t::getDatapointType() const
{
    if (DatapointType.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<DatapointType_DatapointSubtypes_DatapointSubtype_t *>(knx->ids[DatapointType]);
}

FunctionType_FunctionPoint_t * make_FunctionType_FunctionPoint_t(Base * parent)
{
    return new FunctionType_FunctionPoint_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
