/* This file is generated. */

#include <Project/v20/knx/ParameterType_TypeRestriction_Enumeration_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/KNX_t.h>

namespace Project {
namespace v20 {
namespace knx {

ParameterType_TypeRestriction_Enumeration_t::ParameterType_TypeRestriction_Enumeration_t(Base * parent) :
    Base(parent)
{
}

ParameterType_TypeRestriction_Enumeration_t::~ParameterType_TypeRestriction_Enumeration_t()
{
}

void ParameterType_TypeRestriction_Enumeration_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("Text")) {
            Text = attribute.value().toString();
            continue;
        }
        if (name == QString("Icon")) {
            Icon = attribute.value().toString();
            continue;
        }
        if (name == QString("PictureAlignment")) {
            PictureAlignment = attribute.value().toString();
            continue;
        }
        if (name == QString("Value")) {
            Value = attribute.value().toString();
            continue;
        }
        if (name == "Id") {
            Id = attribute.value().toString();
            KNX_t * knx = findParent<KNX_t *>();
            Q_ASSERT(knx);
            knx->ids[Id] = this;
            continue;
        }
        if (name == QString("DisplayOrder")) {
            DisplayOrder = attribute.value().toString();
            continue;
        }
        if (name == QString("BinaryValue")) {
            BinaryValue = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ParameterType_TypeRestriction_Enumeration_t::tableColumnCount() const
{
    return 8;
}

QVariant ParameterType_TypeRestriction_Enumeration_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ParameterType_TypeRestriction_Enumeration";
        }
        if (qualifiedName == QString("Text")) {
            return Text;
        }
        if (qualifiedName == QString("Icon")) {
            return Icon;
        }
        if (qualifiedName == QString("PictureAlignment")) {
            return PictureAlignment;
        }
        if (qualifiedName == QString("Value")) {
            return Value;
        }
        if (qualifiedName == QString("Id")) {
            return Id;
        }
        if (qualifiedName == QString("DisplayOrder")) {
            return DisplayOrder;
        }
        if (qualifiedName == QString("BinaryValue")) {
            return BinaryValue;
        }
        break;
    }
    return QVariant();
}

QVariant ParameterType_TypeRestriction_Enumeration_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Text";
            case 2:
                return "Icon";
            case 3:
                return "PictureAlignment";
            case 4:
                return "Value";
            case 5:
                return "Id";
            case 6:
                return "DisplayOrder";
            case 7:
                return "BinaryValue";
            }
        }
    }
    return QVariant();
}

QVariant ParameterType_TypeRestriction_Enumeration_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return QString("[%1] %2").arg(Value).arg(Text);
    case Qt::DecorationRole:
        return QIcon::fromTheme("ParameterType_TypeRestriction_Enumeration");
    }
    return QVariant();
}

ParameterType_TypeRestriction_Enumeration_t * make_ParameterType_TypeRestriction_Enumeration_t(Base * parent)
{
    return new ParameterType_TypeRestriction_Enumeration_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
