/* This file is generated. */

#include <Project/v20/knx/CatalogSection_CatalogItem_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/Hardware2Program_t.h>
#include <Project/v20/knx/Hardware_Products_Product_t.h>
#include <Project/v20/knx/KNX_t.h>

namespace Project {
namespace v20 {
namespace knx {

CatalogSection_CatalogItem_t::CatalogSection_CatalogItem_t(Base * parent) :
    Base(parent)
{
}

CatalogSection_CatalogItem_t::~CatalogSection_CatalogItem_t()
{
}

void CatalogSection_CatalogItem_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == "Id") {
            Id = attribute.value().toString();
            KNX_t * knx = findParent<KNX_t *>();
            Q_ASSERT(knx);
            knx->ids[Id] = this;
            continue;
        }
        if (name == QString("Name")) {
            Name = attribute.value().toString();
            continue;
        }
        if (name == QString("Number")) {
            Number = attribute.value().toString();
            continue;
        }
        if (name == QString("VisibleDescription")) {
            VisibleDescription = attribute.value().toString();
            continue;
        }
        if (name == QString("ProductRefId")) {
            ProductRefId = attribute.value().toString();
            continue;
        }
        if (name == QString("Hardware2ProgramRefId")) {
            Hardware2ProgramRefId = attribute.value().toString();
            continue;
        }
        if (name == QString("DefaultLanguage")) {
            DefaultLanguage = attribute.value().toString();
            continue;
        }
        if (name == QString("NonRegRelevantDataVersion")) {
            NonRegRelevantDataVersion = attribute.value().toString();
            continue;
        }
        if (name == QString("InternalDescription")) {
            InternalDescription = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int CatalogSection_CatalogItem_t::tableColumnCount() const
{
    return 10;
}

QVariant CatalogSection_CatalogItem_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "CatalogSection_CatalogItem";
        }
        if (qualifiedName == QString("Id")) {
            return Id;
        }
        if (qualifiedName == QString("Name")) {
            return Name;
        }
        if (qualifiedName == QString("Number")) {
            return Number;
        }
        if (qualifiedName == QString("VisibleDescription")) {
            return VisibleDescription;
        }
        if (qualifiedName == QString("ProductRefId")) {
            return ProductRefId;
        }
        if (qualifiedName == QString("Hardware2ProgramRefId")) {
            return Hardware2ProgramRefId;
        }
        if (qualifiedName == QString("DefaultLanguage")) {
            return DefaultLanguage;
        }
        if (qualifiedName == QString("NonRegRelevantDataVersion")) {
            return NonRegRelevantDataVersion;
        }
        if (qualifiedName == QString("InternalDescription")) {
            return InternalDescription;
        }
        break;
    }
    return QVariant();
}

QVariant CatalogSection_CatalogItem_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Id";
            case 2:
                return "Name";
            case 3:
                return "Number";
            case 4:
                return "VisibleDescription";
            case 5:
                return "ProductRefId";
            case 6:
                return "Hardware2ProgramRefId";
            case 7:
                return "DefaultLanguage";
            case 8:
                return "NonRegRelevantDataVersion";
            case 9:
                return "InternalDescription";
            }
        }
    }
    return QVariant();
}

QVariant CatalogSection_CatalogItem_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return !Name.isEmpty() ? Name : "CatalogItem";
    case Qt::DecorationRole:
        return QIcon::fromTheme("CatalogSection_CatalogItem");
    }
    return QVariant();
}

Hardware_Products_Product_t * CatalogSection_CatalogItem_t::getProduct() const
{
    if (ProductRefId.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<Hardware_Products_Product_t *>(knx->ids[ProductRefId]);
}

Hardware2Program_t * CatalogSection_CatalogItem_t::getHardware2Program() const
{
    if (Hardware2ProgramRefId.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<Hardware2Program_t *>(knx->ids[Hardware2ProgramRefId]);
}

CatalogSection_CatalogItem_t * make_CatalogSection_CatalogItem_t(Base * parent)
{
    return new CatalogSection_CatalogItem_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
