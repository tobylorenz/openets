/* This file is generated. */

#include <Project/v20/knx/DeviceInstance_ModuleInstances_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/KNX_t.h>

namespace Project {
namespace v20 {
namespace knx {

DeviceInstance_ModuleInstances_t::DeviceInstance_ModuleInstances_t(Base * parent) :
    Base(parent)
{
}

DeviceInstance_ModuleInstances_t::~DeviceInstance_ModuleInstances_t()
{
}

void DeviceInstance_ModuleInstances_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("ModuleInstance")) {
            auto * newModuleInstance = make_ModuleInstance_t(this);
            newModuleInstance->read(reader);
            ModuleInstance.append(newModuleInstance);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int DeviceInstance_ModuleInstances_t::tableColumnCount() const
{
    return 1;
}

QVariant DeviceInstance_ModuleInstances_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "DeviceInstance_ModuleInstances";
        }
        break;
    }
    return QVariant();
}

QVariant DeviceInstance_ModuleInstances_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant DeviceInstance_ModuleInstances_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "ModuleInstances";
    case Qt::DecorationRole:
        return QIcon::fromTheme("DeviceInstance_ModuleInstances");
    }
    return QVariant();
}

DeviceInstance_ModuleInstances_t * make_DeviceInstance_ModuleInstances_t(Base * parent)
{
    return new DeviceInstance_ModuleInstances_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
