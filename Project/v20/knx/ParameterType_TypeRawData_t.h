/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v20/knx/ParameterType_TypeRawData_MaxSize_t.h>

namespace Project {
namespace v20 {
namespace knx {

class ParameterType_TypeRawData_t : public Base
{
    Q_OBJECT

public:
    explicit ParameterType_TypeRawData_t(Base * parent = nullptr);
    virtual ~ParameterType_TypeRawData_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    ParameterType_TypeRawData_MaxSize_t MaxSize{};
};

ParameterType_TypeRawData_t * make_ParameterType_TypeRawData_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
