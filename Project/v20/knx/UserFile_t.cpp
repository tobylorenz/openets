/* This file is generated. */

#include <Project/v20/knx/UserFile_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/KNX_t.h>

namespace Project {
namespace v20 {
namespace knx {

UserFile_t::UserFile_t(Base * parent) :
    Base(parent)
{
}

UserFile_t::~UserFile_t()
{
}

void UserFile_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("Filename")) {
            Filename = attribute.value().toString();
            continue;
        }
        if (name == QString("Comment")) {
            Comment = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int UserFile_t::tableColumnCount() const
{
    return 3;
}

QVariant UserFile_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "UserFile";
        }
        if (qualifiedName == QString("Filename")) {
            return Filename;
        }
        if (qualifiedName == QString("Comment")) {
            return Comment;
        }
        break;
    }
    return QVariant();
}

QVariant UserFile_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Filename";
            case 2:
                return "Comment";
            }
        }
    }
    return QVariant();
}

QVariant UserFile_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return Filename;
    case Qt::DecorationRole:
        return QIcon::fromTheme("UserFile");
    }
    return QVariant();
}

UserFile_t * make_UserFile_t(Base * parent)
{
    return new UserFile_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
