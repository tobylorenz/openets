/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v20/knx/ModuleDef_t.h>

namespace Project {
namespace v20 {
namespace knx {

class ModuleDef_SubModuleDefs_t : public Base
{
    Q_OBJECT

public:
    explicit ModuleDef_SubModuleDefs_t(Base * parent = nullptr);
    virtual ~ModuleDef_SubModuleDefs_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* elements */
    QMap<xs::ID, ModuleDef_t *> ModuleDef; // key: Id
};

ModuleDef_SubModuleDefs_t * make_ModuleDef_SubModuleDefs_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
