/* This file is generated. */

#pragma once

#include <Project/Base.h>

namespace Project {
namespace v20 {
namespace knx {

/* forward declarations */
class Topology_Area_Line_AdditionalGroupAddresses_GroupAddress_t;

class Topology_Area_Line_AdditionalGroupAddresses_t : public Base
{
    Q_OBJECT

public:
    explicit Topology_Area_Line_AdditionalGroupAddresses_t(Base * parent = nullptr);
    virtual ~Topology_Area_Line_AdditionalGroupAddresses_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* elements */
    Topology_Area_Line_AdditionalGroupAddresses_GroupAddress_t * GroupAddress{};
};

Topology_Area_Line_AdditionalGroupAddresses_t * make_Topology_Area_Line_AdditionalGroupAddresses_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
