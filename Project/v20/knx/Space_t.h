/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v20/knx/CompletionStatus_t.h>
#include <Project/v20/knx/DeviceInstanceRef_t.h>
#include <Project/v20/knx/Function_t.h>
#include <Project/v20/knx/IDREF.h>
#include <Project/v20/knx/SpaceType_t.h>
#include <Project/v20/knx/Space_t.h>
#include <Project/v20/knx/String255_t.h>
#include <Project/xs/ID.h>
#include <Project/xs/Int.h>
#include <Project/xs/String.h>

namespace Project {
namespace v20 {
namespace knx {

/* forward declarations */
class SpaceUsage_t;

class Space_t : public Base
{
    Q_OBJECT

public:
    explicit Space_t(Base * parent = nullptr);
    virtual ~Space_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::ID Id{};
    String255_t Name{};
    SpaceType_t Type{};
    IDREF Usage{};
    String255_t Number{};
    xs::String Comment{};
    xs::String Description{};
    CompletionStatus_t CompletionStatus{"Undefined"};
    xs::String DefaultLine{};
    xs::Int Puid{};
    xs::String Context{};

    /* elements */
    QMap<xs::ID, Space_t *> Space; // key: Id
    QMap<IDREF, DeviceInstanceRef_t *> DeviceInstanceRef; // key: RefId
    QMap<xs::ID, Function_t *> Function; // key: Id

    /* getters */
    SpaceUsage_t * getUsage() const; // attribute: Usage
};

Space_t * make_Space_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
