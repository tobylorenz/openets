/* This file is generated. */

#pragma once

#include <Project/xs/UnsignedByte.h>

namespace Project {
namespace v20 {
namespace knx {

using DeviceInstance_RfFastAckSlots_Slot_Number_t = xs::UnsignedByte;

} // namespace knx
} // namespace v20
} // namespace Project
