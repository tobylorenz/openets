/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v20/knx/LanguageDependentString255_t.h>
#include <Project/xs/ID.h>
#include <Project/xs/UnsignedInt.h>
#include <Project/xs/UnsignedShort.h>

namespace Project {
namespace v20 {
namespace knx {

class ApplicationProgramStatic_SecurityRoles_SecurityRole_t : public Base
{
    Q_OBJECT

public:
    explicit ApplicationProgramStatic_SecurityRoles_SecurityRole_t(Base * parent = nullptr);
    virtual ~ApplicationProgramStatic_SecurityRoles_SecurityRole_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::ID Id{};
    LanguageDependentString255_t Text{};
    xs::UnsignedShort Mask{};
    xs::UnsignedInt RoleID{};
};

ApplicationProgramStatic_SecurityRoles_SecurityRole_t * make_ApplicationProgramStatic_SecurityRoles_SecurityRole_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
