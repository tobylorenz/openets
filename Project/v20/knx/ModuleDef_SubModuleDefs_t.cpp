/* This file is generated. */

#include <Project/v20/knx/ModuleDef_SubModuleDefs_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/KNX_t.h>

namespace Project {
namespace v20 {
namespace knx {

ModuleDef_SubModuleDefs_t::ModuleDef_SubModuleDefs_t(Base * parent) :
    Base(parent)
{
}

ModuleDef_SubModuleDefs_t::~ModuleDef_SubModuleDefs_t()
{
}

void ModuleDef_SubModuleDefs_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("ModuleDef")) {
            QString newId = reader.attributes().value("Id").toString();
            Q_ASSERT(!newId.isEmpty());
            ModuleDef_t * newModuleDef;
            if (ModuleDef.contains(newId)) {
                newModuleDef = ModuleDef[newId];
            } else {
                newModuleDef = make_ModuleDef_t(this);
                ModuleDef[newId] = newModuleDef;
            }
            newModuleDef->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ModuleDef_SubModuleDefs_t::tableColumnCount() const
{
    return 1;
}

QVariant ModuleDef_SubModuleDefs_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ModuleDef_SubModuleDefs";
        }
        break;
    }
    return QVariant();
}

QVariant ModuleDef_SubModuleDefs_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant ModuleDef_SubModuleDefs_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "SubModuleDefs";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ModuleDef_SubModuleDefs");
    }
    return QVariant();
}

ModuleDef_SubModuleDefs_t * make_ModuleDef_SubModuleDefs_t(Base * parent)
{
    return new ModuleDef_SubModuleDefs_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
