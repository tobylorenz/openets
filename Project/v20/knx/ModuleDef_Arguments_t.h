/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v20/knx/ModuleDef_Arguments_Argument_t.h>
#include <Project/xs/ID.h>

namespace Project {
namespace v20 {
namespace knx {

class ModuleDef_Arguments_t : public Base
{
    Q_OBJECT

public:
    explicit ModuleDef_Arguments_t(Base * parent = nullptr);
    virtual ~ModuleDef_Arguments_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* elements */
    QMap<xs::ID, ModuleDef_Arguments_Argument_t *> Argument; // key: Id
};

ModuleDef_Arguments_t * make_ModuleDef_Arguments_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
