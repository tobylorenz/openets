/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v20/knx/ApplicationProgramStatic_SecurityRoles_SecurityRole_t.h>
#include <Project/xs/ID.h>

namespace Project {
namespace v20 {
namespace knx {

class ApplicationProgramStatic_SecurityRoles_t : public Base
{
    Q_OBJECT

public:
    explicit ApplicationProgramStatic_SecurityRoles_t(Base * parent = nullptr);
    virtual ~ApplicationProgramStatic_SecurityRoles_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* elements */
    QMap<xs::ID, ApplicationProgramStatic_SecurityRoles_SecurityRole_t *> SecurityRole; // key: Id
};

ApplicationProgramStatic_SecurityRoles_t * make_ApplicationProgramStatic_SecurityRoles_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
