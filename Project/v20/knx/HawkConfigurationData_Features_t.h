/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v20/knx/HawkConfigurationData_Features_Feature_t.h>

namespace Project {
namespace v20 {
namespace knx {

class HawkConfigurationData_Features_t : public Base
{
    Q_OBJECT

public:
    explicit HawkConfigurationData_Features_t(Base * parent = nullptr);
    virtual ~HawkConfigurationData_Features_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* elements */
    QVector<HawkConfigurationData_Features_Feature_t *> Feature;
};

HawkConfigurationData_Features_t * make_HawkConfigurationData_Features_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
