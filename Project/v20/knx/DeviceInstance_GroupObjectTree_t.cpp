/* This file is generated. */

#include <Project/v20/knx/DeviceInstance_GroupObjectTree_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/DeviceInstance_GroupObjectTree_Nodes_t.h>
#include <Project/v20/knx/KNX_t.h>

namespace Project {
namespace v20 {
namespace knx {

DeviceInstance_GroupObjectTree_t::DeviceInstance_GroupObjectTree_t(Base * parent) :
    Base(parent)
{
}

DeviceInstance_GroupObjectTree_t::~DeviceInstance_GroupObjectTree_t()
{
}

void DeviceInstance_GroupObjectTree_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("GroupObjectInstances")) {
            GroupObjectInstances = attribute.value().toString().split(' ');
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("Nodes")) {
            if (!Nodes) {
                Nodes = make_DeviceInstance_GroupObjectTree_Nodes_t(this);
            }
            Nodes->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int DeviceInstance_GroupObjectTree_t::tableColumnCount() const
{
    return 2;
}

QVariant DeviceInstance_GroupObjectTree_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "DeviceInstance_GroupObjectTree";
        }
        if (qualifiedName == QString("GroupObjectInstances")) {
            return GroupObjectInstances.join(' ');
        }
        break;
    }
    return QVariant();
}

QVariant DeviceInstance_GroupObjectTree_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "GroupObjectInstances";
            }
        }
    }
    return QVariant();
}

QVariant DeviceInstance_GroupObjectTree_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "GroupObjectTree";
    case Qt::DecorationRole:
        return QIcon::fromTheme("DeviceInstance_GroupObjectTree");
    }
    return QVariant();
}

QList<ComObjectRef_t *> DeviceInstance_GroupObjectTree_t::getGroupObjectInstances() const
{
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    QList<ComObjectRef_t *> retVal;
    for (QString id : GroupObjectInstances) {
        retVal << qobject_cast<ComObjectRef_t *>(knx->ids[id]);
    }
    return retVal;
}

DeviceInstance_GroupObjectTree_t * make_DeviceInstance_GroupObjectTree_t(Base * parent)
{
    return new DeviceInstance_GroupObjectTree_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
