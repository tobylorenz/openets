/* This file is generated. */

#pragma once

#include <Project/v20/knx/IDREF.h>
#include <Project/xs/List.h>

namespace Project {
namespace v20 {
namespace knx {

using IDREFS = xs::List<IDREF>;

} // namespace knx
} // namespace v20
} // namespace Project
