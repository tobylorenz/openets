/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v20/knx/DatapointType_DatapointSubtypes_DatapointSubtype_Format_Enumeration_EnumValue_t.h>
#include <Project/v20/knx/LanguageDependentString_t.h>
#include <Project/xs/ID.h>
#include <Project/xs/UnsignedInt.h>

namespace Project {
namespace v20 {
namespace knx {

class DatapointType_DatapointSubtypes_DatapointSubtype_Format_Enumeration_t : public Base
{
    Q_OBJECT

public:
    explicit DatapointType_DatapointSubtypes_DatapointSubtype_Format_Enumeration_t(Base * parent = nullptr);
    virtual ~DatapointType_DatapointSubtypes_DatapointSubtype_Format_Enumeration_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::ID Id{};
    xs::UnsignedInt Width{};
    LanguageDependentString_t Name{};

    /* elements */
    QMap<xs::ID, DatapointType_DatapointSubtypes_DatapointSubtype_Format_Enumeration_EnumValue_t *> EnumValue; // key: Id
};

DatapointType_DatapointSubtypes_DatapointSubtype_Format_Enumeration_t * make_DatapointType_DatapointSubtypes_DatapointSubtype_Format_Enumeration_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
