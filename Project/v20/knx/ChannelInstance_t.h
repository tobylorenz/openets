/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v20/knx/RELIDREF.h>
#include <Project/v20/knx/String255_t.h>
#include <Project/xs/ID.h>
#include <Project/xs/Boolean.h>
#include <Project/xs/String.h>

namespace Project {
namespace v20 {
namespace knx {

/* forward declarations */
class ApplicationProgramChannel_t;

class ChannelInstance_t : public Base
{
    Q_OBJECT

public:
    explicit ChannelInstance_t(Base * parent = nullptr);
    virtual ~ChannelInstance_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::ID Id{};
    RELIDREF RefId{};
    String255_t Name{};
    String255_t Description{};
    xs::Boolean IsActive{};
    xs::String Context{};

    /* getters */
    ApplicationProgramChannel_t * getChannel() const; // attribute: RefId
};

ChannelInstance_t * make_ChannelInstance_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
