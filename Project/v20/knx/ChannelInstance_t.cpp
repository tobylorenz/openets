/* This file is generated. */

#include <Project/v20/knx/ChannelInstance_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/ApplicationProgramChannel_t.h>
#include <Project/v20/knx/KNX_t.h>

namespace Project {
namespace v20 {
namespace knx {

ChannelInstance_t::ChannelInstance_t(Base * parent) :
    Base(parent)
{
}

ChannelInstance_t::~ChannelInstance_t()
{
}

void ChannelInstance_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == "Id") {
            Id = attribute.value().toString();
            KNX_t * knx = findParent<KNX_t *>();
            Q_ASSERT(knx);
            knx->ids[Id] = this;
            continue;
        }
        if (name == QString("RefId")) {
            RefId = attribute.value().toString();
            continue;
        }
        if (name == QString("Name")) {
            Name = attribute.value().toString();
            continue;
        }
        if (name == QString("Description")) {
            Description = attribute.value().toString();
            continue;
        }
        if (name == QString("IsActive")) {
            IsActive = attribute.value().toString();
            continue;
        }
        if (name == QString("Context")) {
            Context = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ChannelInstance_t::tableColumnCount() const
{
    return 7;
}

QVariant ChannelInstance_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ChannelInstance";
        }
        if (qualifiedName == QString("Id")) {
            return Id;
        }
        if (qualifiedName == QString("RefId")) {
            return RefId;
        }
        if (qualifiedName == QString("Name")) {
            return Name;
        }
        if (qualifiedName == QString("Description")) {
            return Description;
        }
        if (qualifiedName == QString("IsActive")) {
            return IsActive;
        }
        if (qualifiedName == QString("Context")) {
            return Context;
        }
        break;
    }
    return QVariant();
}

QVariant ChannelInstance_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Id";
            case 2:
                return "RefId";
            case 3:
                return "Name";
            case 4:
                return "Description";
            case 5:
                return "IsActive";
            case 6:
                return "Context";
            }
        }
    }
    return QVariant();
}

QVariant ChannelInstance_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return !Name.isEmpty() ? Name : "ChannelInstance";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ChannelInstance");
    }
    return QVariant();
}

ApplicationProgramChannel_t * ChannelInstance_t::getChannel() const
{
    if (RefId.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<ApplicationProgramChannel_t *>(knx->ids[RefId]);
}

ChannelInstance_t * make_ChannelInstance_t(Base * parent)
{
    return new ChannelInstance_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
