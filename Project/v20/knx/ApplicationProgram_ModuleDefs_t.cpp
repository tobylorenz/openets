/* This file is generated. */

#include <Project/v20/knx/ApplicationProgram_ModuleDefs_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/KNX_t.h>

namespace Project {
namespace v20 {
namespace knx {

ApplicationProgram_ModuleDefs_t::ApplicationProgram_ModuleDefs_t(Base * parent) :
    Base(parent)
{
}

ApplicationProgram_ModuleDefs_t::~ApplicationProgram_ModuleDefs_t()
{
}

void ApplicationProgram_ModuleDefs_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("ModuleDef")) {
            QString newId = reader.attributes().value("Id").toString();
            Q_ASSERT(!newId.isEmpty());
            ModuleDef_t * newModuleDef;
            if (ModuleDef.contains(newId)) {
                newModuleDef = ModuleDef[newId];
            } else {
                newModuleDef = make_ModuleDef_t(this);
                ModuleDef[newId] = newModuleDef;
            }
            newModuleDef->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ApplicationProgram_ModuleDefs_t::tableColumnCount() const
{
    return 1;
}

QVariant ApplicationProgram_ModuleDefs_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ApplicationProgram_ModuleDefs";
        }
        break;
    }
    return QVariant();
}

QVariant ApplicationProgram_ModuleDefs_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant ApplicationProgram_ModuleDefs_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "ModuleDefs";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ApplicationProgram_ModuleDefs");
    }
    return QVariant();
}

ApplicationProgram_ModuleDefs_t * make_ApplicationProgram_ModuleDefs_t(Base * parent)
{
    return new ApplicationProgram_ModuleDefs_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
