/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v20/knx/IDREF.h>
#include <Project/v20/knx/MaskVersion_DownwardCompatibleMasks_DownwardCompatibleMask_t.h>

namespace Project {
namespace v20 {
namespace knx {

class MaskVersion_DownwardCompatibleMasks_t : public Base
{
    Q_OBJECT

public:
    explicit MaskVersion_DownwardCompatibleMasks_t(Base * parent = nullptr);
    virtual ~MaskVersion_DownwardCompatibleMasks_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* elements */
    QMap<IDREF, MaskVersion_DownwardCompatibleMasks_DownwardCompatibleMask_t *> DownwardCompatibleMask; // key: RefId
};

MaskVersion_DownwardCompatibleMasks_t * make_MaskVersion_DownwardCompatibleMasks_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
