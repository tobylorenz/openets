/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v20/knx/ApplicationProgramStatic_Parameters_Union_SizeInBit_t.h>
#include <Project/v20/knx/UnionParameter_t.h>
#include <Project/xs/String.h>

namespace Project {
namespace v20 {
namespace knx {

/* forward declarations */
class MemoryUnion_t;
class PropertyUnion_t;

class ApplicationProgramStatic_Parameters_Union_t : public Base
{
    Q_OBJECT

public:
    explicit ApplicationProgramStatic_Parameters_Union_t(Base * parent = nullptr);
    virtual ~ApplicationProgramStatic_Parameters_Union_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    ApplicationProgramStatic_Parameters_Union_SizeInBit_t SizeInBit{};
    xs::String InternalDescription{};

    /* elements */
    // xs:choice MemoryUnion_t * Memory{};
    // xs:choice PropertyUnion_t * Property{};
    QMap<xs::ID, UnionParameter_t *> Parameter; // key: Id
};

ApplicationProgramStatic_Parameters_Union_t * make_ApplicationProgramStatic_Parameters_Union_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
