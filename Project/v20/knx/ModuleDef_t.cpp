/* This file is generated. */

#include <Project/v20/knx/ModuleDef_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/KNX_t.h>
#include <Project/v20/knx/ModuleDefDynamic_t.h>
#include <Project/v20/knx/ModuleDefStatic_t.h>
#include <Project/v20/knx/ModuleDef_Arguments_t.h>
#include <Project/v20/knx/ModuleDef_SubModuleDefs_t.h>

namespace Project {
namespace v20 {
namespace knx {

ModuleDef_t::ModuleDef_t(Base * parent) :
    Base(parent)
{
}

ModuleDef_t::~ModuleDef_t()
{
}

void ModuleDef_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == "Id") {
            Id = attribute.value().toString();
            KNX_t * knx = findParent<KNX_t *>();
            Q_ASSERT(knx);
            knx->ids[Id] = this;
            continue;
        }
        if (name == QString("Name")) {
            Name = attribute.value().toString();
            continue;
        }
        if (name == QString("InternalDescription")) {
            InternalDescription = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("Arguments")) {
            if (!Arguments) {
                Arguments = make_ModuleDef_Arguments_t(this);
            }
            Arguments->read(reader);
            continue;
        }
        if (reader.name() == QString("Static")) {
            if (!Static) {
                Static = make_ModuleDefStatic_t(this);
            }
            Static->read(reader);
            continue;
        }
        if (reader.name() == QString("SubModuleDefs")) {
            if (!SubModuleDefs) {
                SubModuleDefs = make_ModuleDef_SubModuleDefs_t(this);
            }
            SubModuleDefs->read(reader);
            continue;
        }
        if (reader.name() == QString("Dynamic")) {
            if (!Dynamic) {
                Dynamic = make_ModuleDefDynamic_t(this);
            }
            Dynamic->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ModuleDef_t::tableColumnCount() const
{
    return 4;
}

QVariant ModuleDef_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ModuleDef";
        }
        if (qualifiedName == QString("Id")) {
            return Id;
        }
        if (qualifiedName == QString("Name")) {
            return Name;
        }
        if (qualifiedName == QString("InternalDescription")) {
            return InternalDescription;
        }
        break;
    }
    return QVariant();
}

QVariant ModuleDef_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Id";
            case 2:
                return "Name";
            case 3:
                return "InternalDescription";
            }
        }
    }
    return QVariant();
}

QVariant ModuleDef_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return !Name.isEmpty() ? Name : "ModuleDef";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ModuleDef");
    }
    return QVariant();
}

ModuleDef_t * make_ModuleDef_t(Base * parent)
{
    return new ModuleDef_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
