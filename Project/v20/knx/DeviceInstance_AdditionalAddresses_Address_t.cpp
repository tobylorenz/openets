/* This file is generated. */

#include <Project/v20/knx/DeviceInstance_AdditionalAddresses_Address_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/KNX_t.h>

namespace Project {
namespace v20 {
namespace knx {

DeviceInstance_AdditionalAddresses_Address_t::DeviceInstance_AdditionalAddresses_Address_t(Base * parent) :
    Base(parent)
{
}

DeviceInstance_AdditionalAddresses_Address_t::~DeviceInstance_AdditionalAddresses_Address_t()
{
}

void DeviceInstance_AdditionalAddresses_Address_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("Address")) {
            Address = attribute.value().toString();
            continue;
        }
        if (name == QString("Name")) {
            Name = attribute.value().toString();
            continue;
        }
        if (name == QString("Description")) {
            Description = attribute.value().toString();
            continue;
        }
        if (name == QString("Comment")) {
            Comment = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int DeviceInstance_AdditionalAddresses_Address_t::tableColumnCount() const
{
    return 5;
}

QVariant DeviceInstance_AdditionalAddresses_Address_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "DeviceInstance_AdditionalAddresses_Address";
        }
        if (qualifiedName == QString("Address")) {
            return Address;
        }
        if (qualifiedName == QString("Name")) {
            return Name;
        }
        if (qualifiedName == QString("Description")) {
            return Description;
        }
        if (qualifiedName == QString("Comment")) {
            return Comment;
        }
        break;
    }
    return QVariant();
}

QVariant DeviceInstance_AdditionalAddresses_Address_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Address";
            case 2:
                return "Name";
            case 3:
                return "Description";
            case 4:
                return "Comment";
            }
        }
    }
    return QVariant();
}

QVariant DeviceInstance_AdditionalAddresses_Address_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return !Name.isEmpty() ? Name : "Address";
    case Qt::DecorationRole:
        return QIcon::fromTheme("DeviceInstance_AdditionalAddresses_Address");
    }
    return QVariant();
}

DeviceInstance_AdditionalAddresses_Address_t * make_DeviceInstance_AdditionalAddresses_Address_t(Base * parent)
{
    return new DeviceInstance_AdditionalAddresses_Address_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
