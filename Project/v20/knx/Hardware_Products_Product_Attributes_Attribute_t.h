/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v20/knx/Hardware_Products_Product_Attributes_Attribute_Name_t.h>
#include <Project/v20/knx/LanguageDependentString255_t.h>
#include <Project/xs/ID.h>

namespace Project {
namespace v20 {
namespace knx {

class Hardware_Products_Product_Attributes_Attribute_t : public Base
{
    Q_OBJECT

public:
    explicit Hardware_Products_Product_Attributes_Attribute_t(Base * parent = nullptr);
    virtual ~Hardware_Products_Product_Attributes_Attribute_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::ID Id{};
    Hardware_Products_Product_Attributes_Attribute_Name_t Name{};
    LanguageDependentString255_t Value{};
};

Hardware_Products_Product_Attributes_Attribute_t * make_Hardware_Products_Product_Attributes_Attribute_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
