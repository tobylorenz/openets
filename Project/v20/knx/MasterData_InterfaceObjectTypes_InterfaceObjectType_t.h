/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v20/knx/LanguageDependentString255_t.h>
#include <Project/v20/knx/String255_t.h>
#include <Project/xs/ID.h>
#include <Project/xs/UnsignedInt.h>

namespace Project {
namespace v20 {
namespace knx {

class MasterData_InterfaceObjectTypes_InterfaceObjectType_t : public Base
{
    Q_OBJECT

public:
    explicit MasterData_InterfaceObjectTypes_InterfaceObjectType_t(Base * parent = nullptr);
    virtual ~MasterData_InterfaceObjectTypes_InterfaceObjectType_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::ID Id{};
    xs::UnsignedInt Number{};
    String255_t Name{};
    LanguageDependentString255_t Text{};
};

MasterData_InterfaceObjectTypes_InterfaceObjectType_t * make_MasterData_InterfaceObjectTypes_InterfaceObjectType_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
