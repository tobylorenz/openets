/* This file is generated. */

#include <Project/v20/knx/DeviceInstance_GroupObjectTree_Nodes_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/KNX_t.h>

namespace Project {
namespace v20 {
namespace knx {

DeviceInstance_GroupObjectTree_Nodes_t::DeviceInstance_GroupObjectTree_Nodes_t(Base * parent) :
    Base(parent)
{
}

DeviceInstance_GroupObjectTree_Nodes_t::~DeviceInstance_GroupObjectTree_Nodes_t()
{
}

void DeviceInstance_GroupObjectTree_Nodes_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("Node")) {
            QString newRefId = reader.attributes().value("RefId").toString();
            Q_ASSERT(!newRefId.isEmpty());
            Node_t * newNode;
            if (Node.contains(newRefId)) {
                newNode = Node[newRefId];
            } else {
                newNode = make_Node_t(this);
                Node[newRefId] = newNode;
            }
            newNode->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int DeviceInstance_GroupObjectTree_Nodes_t::tableColumnCount() const
{
    return 1;
}

QVariant DeviceInstance_GroupObjectTree_Nodes_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "DeviceInstance_GroupObjectTree_Nodes";
        }
        break;
    }
    return QVariant();
}

QVariant DeviceInstance_GroupObjectTree_Nodes_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant DeviceInstance_GroupObjectTree_Nodes_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "Nodes";
    case Qt::DecorationRole:
        return QIcon::fromTheme("DeviceInstance_GroupObjectTree_Nodes");
    }
    return QVariant();
}

DeviceInstance_GroupObjectTree_Nodes_t * make_DeviceInstance_GroupObjectTree_Nodes_t(Base * parent)
{
    return new DeviceInstance_GroupObjectTree_Nodes_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
