/* This file is generated. */

#include <Project/v20/knx/ModuleInstance_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/KNX_t.h>
#include <Project/v20/knx/ModuleInstance_Arguments_t.h>

namespace Project {
namespace v20 {
namespace knx {

ModuleInstance_t::ModuleInstance_t(Base * parent) :
    Base(parent)
{
}

ModuleInstance_t::~ModuleInstance_t()
{
}

void ModuleInstance_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == "Id") {
            Id = attribute.value().toString();
            KNX_t * knx = findParent<KNX_t *>();
            Q_ASSERT(knx);
            knx->ids[Id] = this;
            continue;
        }
        if (name == QString("RefId")) {
            RefId = attribute.value().toString();
            continue;
        }
        if (name == QString("RepeatIndex")) {
            RepeatIndex = attribute.value().toString().split(' ');
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("Arguments")) {
            if (!Arguments) {
                Arguments = make_ModuleInstance_Arguments_t(this);
            }
            Arguments->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ModuleInstance_t::tableColumnCount() const
{
    return 4;
}

QVariant ModuleInstance_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ModuleInstance";
        }
        if (qualifiedName == QString("Id")) {
            return Id;
        }
        if (qualifiedName == QString("RefId")) {
            return RefId;
        }
        if (qualifiedName == QString("RepeatIndex")) {
            return RepeatIndex.join(' ');
        }
        break;
    }
    return QVariant();
}

QVariant ModuleInstance_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Id";
            case 2:
                return "RefId";
            case 3:
                return "RepeatIndex";
            }
        }
    }
    return QVariant();
}

QVariant ModuleInstance_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "ModuleInstance";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ModuleInstance");
    }
    return QVariant();
}

ModuleInstance_t * make_ModuleInstance_t(Base * parent)
{
    return new ModuleInstance_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
