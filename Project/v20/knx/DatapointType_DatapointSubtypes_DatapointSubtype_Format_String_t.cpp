/* This file is generated. */

#include <Project/v20/knx/DatapointType_DatapointSubtypes_DatapointSubtype_Format_String_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/KNX_t.h>

namespace Project {
namespace v20 {
namespace knx {

DatapointType_DatapointSubtypes_DatapointSubtype_Format_String_t::DatapointType_DatapointSubtypes_DatapointSubtype_Format_String_t(Base * parent) :
    Base(parent)
{
}

DatapointType_DatapointSubtypes_DatapointSubtype_Format_String_t::~DatapointType_DatapointSubtypes_DatapointSubtype_Format_String_t()
{
}

void DatapointType_DatapointSubtypes_DatapointSubtype_Format_String_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == "Id") {
            Id = attribute.value().toString();
            KNX_t * knx = findParent<KNX_t *>();
            Q_ASSERT(knx);
            knx->ids[Id] = this;
            continue;
        }
        if (name == QString("Width")) {
            Width = attribute.value().toString();
            continue;
        }
        if (name == QString("Name")) {
            Name = attribute.value().toString();
            continue;
        }
        if (name == QString("Unit")) {
            Unit = attribute.value().toString();
            continue;
        }
        if (name == QString("Encoding")) {
            Encoding = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int DatapointType_DatapointSubtypes_DatapointSubtype_Format_String_t::tableColumnCount() const
{
    return 6;
}

QVariant DatapointType_DatapointSubtypes_DatapointSubtype_Format_String_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "DatapointType_DatapointSubtypes_DatapointSubtype_Format_String";
        }
        if (qualifiedName == QString("Id")) {
            return Id;
        }
        if (qualifiedName == QString("Width")) {
            return Width;
        }
        if (qualifiedName == QString("Name")) {
            return Name;
        }
        if (qualifiedName == QString("Unit")) {
            return Unit;
        }
        if (qualifiedName == QString("Encoding")) {
            return Encoding;
        }
        break;
    }
    return QVariant();
}

QVariant DatapointType_DatapointSubtypes_DatapointSubtype_Format_String_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Id";
            case 2:
                return "Width";
            case 3:
                return "Name";
            case 4:
                return "Unit";
            case 5:
                return "Encoding";
            }
        }
    }
    return QVariant();
}

QVariant DatapointType_DatapointSubtypes_DatapointSubtype_Format_String_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return !Name.isEmpty() ? Name : "String";
    case Qt::DecorationRole:
        return QIcon::fromTheme("DatapointType_DatapointSubtypes_DatapointSubtype_Format_String");
    }
    return QVariant();
}

DatapointType_DatapointSubtypes_DatapointSubtype_Format_String_t * make_DatapointType_DatapointSubtypes_DatapointSubtype_Format_String_t(Base * parent)
{
    return new DatapointType_DatapointSubtypes_DatapointSubtype_Format_String_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
