/* This file is generated. */

#include <Project/v20/knx/BusInterface_Connectors_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/KNX_t.h>

namespace Project {
namespace v20 {
namespace knx {

BusInterface_Connectors_t::BusInterface_Connectors_t(Base * parent) :
    Base(parent)
{
}

BusInterface_Connectors_t::~BusInterface_Connectors_t()
{
}

void BusInterface_Connectors_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("Connector")) {
            QString newGroupAddressRefId = reader.attributes().value("GroupAddressRefId").toString();
            Q_ASSERT(!newGroupAddressRefId.isEmpty());
            BusInterface_Connectors_Connector_t * newConnector;
            if (Connector.contains(newGroupAddressRefId)) {
                newConnector = Connector[newGroupAddressRefId];
            } else {
                newConnector = make_BusInterface_Connectors_Connector_t(this);
                Connector[newGroupAddressRefId] = newConnector;
            }
            newConnector->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int BusInterface_Connectors_t::tableColumnCount() const
{
    return 1;
}

QVariant BusInterface_Connectors_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "BusInterface_Connectors";
        }
        break;
    }
    return QVariant();
}

QVariant BusInterface_Connectors_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant BusInterface_Connectors_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "Connectors";
    case Qt::DecorationRole:
        return QIcon::fromTheme("BusInterface_Connectors");
    }
    return QVariant();
}

BusInterface_Connectors_t * make_BusInterface_Connectors_t(Base * parent)
{
    return new BusInterface_Connectors_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
