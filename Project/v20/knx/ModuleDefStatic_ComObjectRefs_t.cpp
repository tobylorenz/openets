/* This file is generated. */

#include <Project/v20/knx/ModuleDefStatic_ComObjectRefs_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/KNX_t.h>

namespace Project {
namespace v20 {
namespace knx {

ModuleDefStatic_ComObjectRefs_t::ModuleDefStatic_ComObjectRefs_t(Base * parent) :
    Base(parent)
{
}

ModuleDefStatic_ComObjectRefs_t::~ModuleDefStatic_ComObjectRefs_t()
{
}

void ModuleDefStatic_ComObjectRefs_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("ComObjectRef")) {
            QString newId = reader.attributes().value("Id").toString();
            Q_ASSERT(!newId.isEmpty());
            ComObjectRef_t * newComObjectRef;
            if (ComObjectRef.contains(newId)) {
                newComObjectRef = ComObjectRef[newId];
            } else {
                newComObjectRef = make_ComObjectRef_t(this);
                ComObjectRef[newId] = newComObjectRef;
            }
            newComObjectRef->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ModuleDefStatic_ComObjectRefs_t::tableColumnCount() const
{
    return 1;
}

QVariant ModuleDefStatic_ComObjectRefs_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ModuleDefStatic_ComObjectRefs";
        }
        break;
    }
    return QVariant();
}

QVariant ModuleDefStatic_ComObjectRefs_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant ModuleDefStatic_ComObjectRefs_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "ComObjectRefs";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ModuleDefStatic_ComObjectRefs");
    }
    return QVariant();
}

ModuleDefStatic_ComObjectRefs_t * make_ModuleDefStatic_ComObjectRefs_t(Base * parent)
{
    return new ModuleDefStatic_ComObjectRefs_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
