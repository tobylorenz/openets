/* This file is generated. */

#include <Project/v20/knx/ModuleDefStatic_Parameters_Union_Memory_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/ApplicationProgramStatic_Code_AbsoluteSegment_t.h>
#include <Project/v20/knx/KNX_t.h>
#include <Project/v20/knx/ModuleDef_Arguments_Argument_t.h>

namespace Project {
namespace v20 {
namespace knx {

ModuleDefStatic_Parameters_Union_Memory_t::ModuleDefStatic_Parameters_Union_Memory_t(Base * parent) :
    Base(parent)
{
}

ModuleDefStatic_Parameters_Union_Memory_t::~ModuleDefStatic_Parameters_Union_Memory_t()
{
}

void ModuleDefStatic_Parameters_Union_Memory_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("CodeSegment")) {
            CodeSegment = attribute.value().toString();
            continue;
        }
        if (name == QString("Offset")) {
            Offset = attribute.value().toString();
            continue;
        }
        if (name == QString("BitOffset")) {
            BitOffset = attribute.value().toString();
            continue;
        }
        if (name == QString("BaseOffset")) {
            BaseOffset = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ModuleDefStatic_Parameters_Union_Memory_t::tableColumnCount() const
{
    return 5;
}

QVariant ModuleDefStatic_Parameters_Union_Memory_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ModuleDefStatic_Parameters_Union_Memory";
        }
        if (qualifiedName == QString("CodeSegment")) {
            return CodeSegment;
        }
        if (qualifiedName == QString("Offset")) {
            return Offset;
        }
        if (qualifiedName == QString("BitOffset")) {
            return BitOffset;
        }
        if (qualifiedName == QString("BaseOffset")) {
            return BaseOffset;
        }
        break;
    }
    return QVariant();
}

QVariant ModuleDefStatic_Parameters_Union_Memory_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "CodeSegment";
            case 2:
                return "Offset";
            case 3:
                return "BitOffset";
            case 4:
                return "BaseOffset";
            }
        }
    }
    return QVariant();
}

QVariant ModuleDefStatic_Parameters_Union_Memory_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "Memory";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ModuleDefStatic_Parameters_Union_Memory");
    }
    return QVariant();
}

ApplicationProgramStatic_Code_AbsoluteSegment_t * ModuleDefStatic_Parameters_Union_Memory_t::getCodeSegment() const
{
    if (CodeSegment.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<ApplicationProgramStatic_Code_AbsoluteSegment_t *>(knx->ids[CodeSegment]);
}

ModuleDef_Arguments_Argument_t * ModuleDefStatic_Parameters_Union_Memory_t::getBaseOffset() const
{
    if (BaseOffset.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<ModuleDef_Arguments_Argument_t *>(knx->ids[BaseOffset]);
}

ModuleDefStatic_Parameters_Union_Memory_t * make_ModuleDefStatic_Parameters_Union_Memory_t(Base * parent)
{
    return new ModuleDefStatic_Parameters_Union_Memory_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
