/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v20/knx/ComObjectPriority_t.h>
#include <Project/v20/knx/ComObjectRef_Roles_t.h>
#include <Project/v20/knx/ComObjectSecurityRequirements_t.h>
#include <Project/v20/knx/ComObjectSize_t.h>
#include <Project/v20/knx/DatapointType_DatapointSubtypes_DatapointSubtype_t.h>
#include <Project/v20/knx/Enable_t.h>
#include <Project/v20/knx/IDREF.h>
#include <Project/v20/knx/IDREFS.h>
#include <Project/v20/knx/LanguageDependentString255_t.h>
#include <Project/v20/knx/String255_t.h>
#include <Project/v20/knx/String50_t.h>
#include <Project/xs/ID.h>
#include <Project/xs/Boolean.h>
#include <Project/xs/String.h>

namespace Project {
namespace v20 {
namespace knx {

/* forward declarations */
class ComObject_t;
class ParameterRef_t;

class ComObjectRef_t : public Base
{
    Q_OBJECT

public:
    explicit ComObjectRef_t(Base * parent = nullptr);
    virtual ~ComObjectRef_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::ID Id{};
    IDREF RefId{};
    String255_t Name{};
    LanguageDependentString255_t Text{};
    String50_t Tag{};
    LanguageDependentString255_t FunctionText{};
    ComObjectPriority_t Priority{};
    ComObjectSize_t ObjectSize{};
    Enable_t ReadFlag{};
    Enable_t WriteFlag{};
    Enable_t CommunicationFlag{};
    Enable_t TransmitFlag{};
    Enable_t UpdateFlag{};
    Enable_t ReadOnInitFlag{};
    IDREFS DatapointType{};
    IDREF TextParameterRefId{};
    xs::String InternalDescription{};
    ComObjectRef_Roles_t Roles{};
    ComObjectSecurityRequirements_t SecurityRequired{};
    xs::Boolean MayRead{};
    xs::Boolean ReadFlagLocked{};
    xs::Boolean WriteFlagLocked{};
    xs::Boolean TransmitFlagLocked{};
    xs::Boolean UpdateFlagLocked{};
    xs::Boolean ReadOnInitFlagLocked{};
    xs::String Semantics{};

    /* getters */
    ComObject_t * getComObject() const; // attribute: RefId
    QList<DatapointType_DatapointSubtypes_DatapointSubtype_t *> getDatapointType() const; // attribute: DatapointType
    ParameterRef_t * getTextParameterRef() const; // attribute: TextParameterRefId
};

ComObjectRef_t * make_ComObjectRef_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
