/* This file is generated. */

#include <Project/v20/knx/ModuleDef_Arguments_Argument_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/KNX_t.h>

namespace Project {
namespace v20 {
namespace knx {

ModuleDef_Arguments_Argument_t::ModuleDef_Arguments_Argument_t(Base * parent) :
    Base(parent)
{
}

ModuleDef_Arguments_Argument_t::~ModuleDef_Arguments_Argument_t()
{
}

void ModuleDef_Arguments_Argument_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == "Id") {
            Id = attribute.value().toString();
            KNX_t * knx = findParent<KNX_t *>();
            Q_ASSERT(knx);
            knx->ids[Id] = this;
            continue;
        }
        if (name == QString("Name")) {
            Name = attribute.value().toString();
            continue;
        }
        if (name == QString("Type")) {
            Type = attribute.value().toString();
            continue;
        }
        if (name == QString("InternalDescription")) {
            InternalDescription = attribute.value().toString();
            continue;
        }
        if (name == QString("Allocates")) {
            Allocates = attribute.value().toString();
            continue;
        }
        if (name == QString("Alignment")) {
            Alignment = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ModuleDef_Arguments_Argument_t::tableColumnCount() const
{
    return 7;
}

QVariant ModuleDef_Arguments_Argument_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ModuleDef_Arguments_Argument";
        }
        if (qualifiedName == QString("Id")) {
            return Id;
        }
        if (qualifiedName == QString("Name")) {
            return Name;
        }
        if (qualifiedName == QString("Type")) {
            return Type;
        }
        if (qualifiedName == QString("InternalDescription")) {
            return InternalDescription;
        }
        if (qualifiedName == QString("Allocates")) {
            return Allocates;
        }
        if (qualifiedName == QString("Alignment")) {
            return Alignment;
        }
        break;
    }
    return QVariant();
}

QVariant ModuleDef_Arguments_Argument_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Id";
            case 2:
                return "Name";
            case 3:
                return "Type";
            case 4:
                return "InternalDescription";
            case 5:
                return "Allocates";
            case 6:
                return "Alignment";
            }
        }
    }
    return QVariant();
}

QVariant ModuleDef_Arguments_Argument_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return !Name.isEmpty() ? Name : "Argument";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ModuleDef_Arguments_Argument");
    }
    return QVariant();
}

ModuleDef_Arguments_Argument_t * make_ModuleDef_Arguments_Argument_t(Base * parent)
{
    return new ModuleDef_Arguments_Argument_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
