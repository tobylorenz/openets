/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v20/knx/Condition_t.h>
#include <Project/xs/Boolean.h>
#include <Project/xs/String.h>

namespace Project {
namespace v20 {
namespace knx {

/* forward declarations */
class ApplicationProgramChannel_t;
class DependentChannelChoose_t;
class Module_t;
class Rename_t;
class Repeat_t;

class DependentChannelChoose_when_t : public Base
{
    Q_OBJECT

public:
    explicit DependentChannelChoose_when_t(Base * parent = nullptr);
    virtual ~DependentChannelChoose_when_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    Condition_t Test{};
    xs::Boolean Default{"false"};
    xs::String InternalDescription{};

    /* elements */
    // xs:choice ApplicationProgramChannel_t * Channel{};
    // xs:choice DependentChannelChoose_t * Choose{};
    // xs:choice Rename_t * Rename{};
    // xs:choice Module_t * Module{};
    // xs:choice Repeat_t * Repeat{};
};

DependentChannelChoose_when_t * make_DependentChannelChoose_when_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
