/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v20/knx/ComObjectRef_t.h>

namespace Project {
namespace v20 {
namespace knx {

class ApplicationProgramStatic_ComObjectRefs_t : public Base
{
    Q_OBJECT

public:
    explicit ApplicationProgramStatic_ComObjectRefs_t(Base * parent = nullptr);
    virtual ~ApplicationProgramStatic_ComObjectRefs_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* elements */
    QMap<xs::ID, ComObjectRef_t *> ComObjectRef; // key: Id
};

ApplicationProgramStatic_ComObjectRefs_t * make_ApplicationProgramStatic_ComObjectRefs_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
