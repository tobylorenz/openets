/* This file is generated. */

#include <Project/v20/knx/ParameterSeparator_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/KNX_t.h>
#include <Project/v20/knx/ParameterRef_t.h>

namespace Project {
namespace v20 {
namespace knx {

ParameterSeparator_t::ParameterSeparator_t(Base * parent) :
    Base(parent)
{
}

ParameterSeparator_t::~ParameterSeparator_t()
{
}

void ParameterSeparator_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == "Id") {
            Id = attribute.value().toString();
            KNX_t * knx = findParent<KNX_t *>();
            Q_ASSERT(knx);
            knx->ids[Id] = this;
            continue;
        }
        if (name == QString("Name")) {
            Name = attribute.value().toString();
            continue;
        }
        if (name == QString("Text")) {
            Text = attribute.value().toString();
            continue;
        }
        if (name == QString("Access")) {
            Access = attribute.value().toString();
            continue;
        }
        if (name == QString("UIHint")) {
            UIHint = attribute.value().toString();
            continue;
        }
        if (name == QString("TextParameterRefId")) {
            TextParameterRefId = attribute.value().toString();
            continue;
        }
        if (name == QString("InternalDescription")) {
            InternalDescription = attribute.value().toString();
            continue;
        }
        if (name == QString("Cell")) {
            Cell = attribute.value().toString();
            continue;
        }
        if (name == QString("Icon")) {
            Icon = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ParameterSeparator_t::tableColumnCount() const
{
    return 10;
}

QVariant ParameterSeparator_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ParameterSeparator";
        }
        if (qualifiedName == QString("Id")) {
            return Id;
        }
        if (qualifiedName == QString("Name")) {
            return Name;
        }
        if (qualifiedName == QString("Text")) {
            return Text;
        }
        if (qualifiedName == QString("Access")) {
            return Access;
        }
        if (qualifiedName == QString("UIHint")) {
            return UIHint;
        }
        if (qualifiedName == QString("TextParameterRefId")) {
            return TextParameterRefId;
        }
        if (qualifiedName == QString("InternalDescription")) {
            return InternalDescription;
        }
        if (qualifiedName == QString("Cell")) {
            return Cell;
        }
        if (qualifiedName == QString("Icon")) {
            return Icon;
        }
        break;
    }
    return QVariant();
}

QVariant ParameterSeparator_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Id";
            case 2:
                return "Name";
            case 3:
                return "Text";
            case 4:
                return "Access";
            case 5:
                return "UIHint";
            case 6:
                return "TextParameterRefId";
            case 7:
                return "InternalDescription";
            case 8:
                return "Cell";
            case 9:
                return "Icon";
            }
        }
    }
    return QVariant();
}

QVariant ParameterSeparator_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return Text;
    case Qt::DecorationRole:
        return QIcon::fromTheme("ParameterSeparator");
    }
    return QVariant();
}

ParameterRef_t * ParameterSeparator_t::getTextParameterRef() const
{
    if (TextParameterRefId.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<ParameterRef_t *>(knx->ids[TextParameterRefId]);
}

ParameterSeparator_t * make_ParameterSeparator_t(Base * parent)
{
    return new ParameterSeparator_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
