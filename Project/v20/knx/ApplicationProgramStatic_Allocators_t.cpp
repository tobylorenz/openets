/* This file is generated. */

#include <Project/v20/knx/ApplicationProgramStatic_Allocators_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/KNX_t.h>

namespace Project {
namespace v20 {
namespace knx {

ApplicationProgramStatic_Allocators_t::ApplicationProgramStatic_Allocators_t(Base * parent) :
    Base(parent)
{
}

ApplicationProgramStatic_Allocators_t::~ApplicationProgramStatic_Allocators_t()
{
}

void ApplicationProgramStatic_Allocators_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("Allocator")) {
            QString newId = reader.attributes().value("Id").toString();
            Q_ASSERT(!newId.isEmpty());
            Allocator_t * newAllocator;
            if (Allocator.contains(newId)) {
                newAllocator = Allocator[newId];
            } else {
                newAllocator = make_Allocator_t(this);
                Allocator[newId] = newAllocator;
            }
            newAllocator->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ApplicationProgramStatic_Allocators_t::tableColumnCount() const
{
    return 1;
}

QVariant ApplicationProgramStatic_Allocators_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ApplicationProgramStatic_Allocators";
        }
        break;
    }
    return QVariant();
}

QVariant ApplicationProgramStatic_Allocators_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant ApplicationProgramStatic_Allocators_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "Allocators";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ApplicationProgramStatic_Allocators");
    }
    return QVariant();
}

ApplicationProgramStatic_Allocators_t * make_ApplicationProgramStatic_Allocators_t(Base * parent)
{
    return new ApplicationProgramStatic_Allocators_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
