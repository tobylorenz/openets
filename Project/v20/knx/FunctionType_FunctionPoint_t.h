/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v20/knx/FunctionType_FunctionPoint_Characteristics_t.h>
#include <Project/v20/knx/IDREF.h>
#include <Project/v20/knx/LanguageDependentString255_t.h>
#include <Project/xs/ID.h>
#include <Project/xs/String.h>

namespace Project {
namespace v20 {
namespace knx {

/* forward declarations */
class DatapointRole_t;
class DatapointType_DatapointSubtypes_DatapointSubtype_t;

class FunctionType_FunctionPoint_t : public Base
{
    Q_OBJECT

public:
    explicit FunctionType_FunctionPoint_t(Base * parent = nullptr);
    virtual ~FunctionType_FunctionPoint_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::ID Id{};
    LanguageDependentString255_t Text{};
    IDREF Role{};
    IDREF DatapointType{};
    FunctionType_FunctionPoint_Characteristics_t Characteristics{};
    xs::String Semantics{};

    /* getters */
    DatapointRole_t * getRole() const; // attribute: Role
    DatapointType_DatapointSubtypes_DatapointSubtype_t * getDatapointType() const; // attribute: DatapointType
};

FunctionType_FunctionPoint_t * make_FunctionType_FunctionPoint_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
