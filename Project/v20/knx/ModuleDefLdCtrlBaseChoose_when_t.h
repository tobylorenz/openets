/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v20/knx/Condition_t.h>
#include <Project/xs/Boolean.h>
#include <Project/xs/String.h>

namespace Project {
namespace v20 {
namespace knx {

/* forward declarations */
class LdCtrlBaseChoose_t;
class LdCtrlDeclarePropDesc_t;
class LdCtrlDelay_t;
class LdCtrlMerge_t;
class LdCtrlProgressText_t;
class ModuleDefLdCtrlCompareProp_t;
class ModuleDefLdCtrlInvokeFunctionProp_t;
class ModuleDefLdCtrlReadFunctionProp_t;
class ModuleDefLdCtrlWriteProp_t;

class ModuleDefLdCtrlBaseChoose_when_t : public Base
{
    Q_OBJECT

public:
    explicit ModuleDefLdCtrlBaseChoose_when_t(Base * parent = nullptr);
    virtual ~ModuleDefLdCtrlBaseChoose_when_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    Condition_t Test{};
    xs::Boolean Default{"false"};
    xs::String InternalDescription{};

    /* elements */
    // xs:choice ModuleDefLdCtrlWriteProp_t * LdCtrlWriteProp{};
    // xs:choice ModuleDefLdCtrlCompareProp_t * LdCtrlCompareProp{};
    // xs:choice ModuleDefLdCtrlInvokeFunctionProp_t * LdCtrlInvokeFunctionProp{};
    // xs:choice ModuleDefLdCtrlReadFunctionProp_t * LdCtrlReadFunctionProp{};
    // xs:choice LdCtrlDelay_t * LdCtrlDelay{};
    // xs:choice LdCtrlProgressText_t * LdCtrlProgressText{};
    // xs:choice LdCtrlDeclarePropDesc_t * LdCtrlDeclarePropDesc{};
    // xs:choice LdCtrlMerge_t * LdCtrlMerge{};
    // xs:choice LdCtrlBaseChoose_t * Choose{};
};

ModuleDefLdCtrlBaseChoose_when_t * make_ModuleDefLdCtrlBaseChoose_when_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
