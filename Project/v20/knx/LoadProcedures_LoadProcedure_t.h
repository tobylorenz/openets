/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/xs/UnsignedByte.h>

namespace Project {
namespace v20 {
namespace knx {

/* forward declarations */
class LdCtrlAbsSegment_t;
class LdCtrlBaseChoose_t;
class LdCtrlClearCachedObjectTypes_t;
class LdCtrlClearLCFilterTable_t;
class LdCtrlCompareMem_t;
class LdCtrlCompareProp_t;
class LdCtrlCompareRelMem_t;
class LdCtrlConnect_t;
class LdCtrlDeclarePropDesc_t;
class LdCtrlDelay_t;
class LdCtrlDisconnect_t;
class LdCtrlInvokeFunctionProp_t;
class LdCtrlLoadCompleted_t;
class LdCtrlLoadImageMem_t;
class LdCtrlLoadImageProp_t;
class LdCtrlLoadImageRelMem_t;
class LdCtrlLoad_t;
class LdCtrlMapError_t;
class LdCtrlMasterReset_t;
class LdCtrlMaxLength_t;
class LdCtrlMerge_t;
class LdCtrlProgressText_t;
class LdCtrlReadFunctionProp_t;
class LdCtrlRelSegment_t;
class LdCtrlRestart_t;
class LdCtrlSetControlVariable_t;
class LdCtrlTaskCtrl1_t;
class LdCtrlTaskCtrl2_t;
class LdCtrlTaskPtr_t;
class LdCtrlTaskSegment_t;
class LdCtrlUnload_t;
class LdCtrlWriteMem_t;
class LdCtrlWriteProp_t;
class LdCtrlWriteRelMem_t;

class LoadProcedures_LoadProcedure_t : public Base
{
    Q_OBJECT

public:
    explicit LoadProcedures_LoadProcedure_t(Base * parent = nullptr);
    virtual ~LoadProcedures_LoadProcedure_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::UnsignedByte MergeId{};

    /* elements */
    // xs:choice LdCtrlUnload_t * LdCtrlUnload{};
    // xs:choice LdCtrlLoad_t * LdCtrlLoad{};
    // xs:choice LdCtrlMaxLength_t * LdCtrlMaxLength{};
    // xs:choice LdCtrlClearCachedObjectTypes_t * LdCtrlClearCachedObjectTypes{};
    // xs:choice LdCtrlLoadCompleted_t * LdCtrlLoadCompleted{};
    // xs:choice LdCtrlAbsSegment_t * LdCtrlAbsSegment{};
    // xs:choice LdCtrlRelSegment_t * LdCtrlRelSegment{};
    // xs:choice LdCtrlTaskSegment_t * LdCtrlTaskSegment{};
    // xs:choice LdCtrlTaskPtr_t * LdCtrlTaskPtr{};
    // xs:choice LdCtrlTaskCtrl1_t * LdCtrlTaskCtrl1{};
    // xs:choice LdCtrlTaskCtrl2_t * LdCtrlTaskCtrl2{};
    // xs:choice LdCtrlWriteProp_t * LdCtrlWriteProp{};
    // xs:choice LdCtrlCompareProp_t * LdCtrlCompareProp{};
    // xs:choice LdCtrlLoadImageProp_t * LdCtrlLoadImageProp{};
    // xs:choice LdCtrlInvokeFunctionProp_t * LdCtrlInvokeFunctionProp{};
    // xs:choice LdCtrlReadFunctionProp_t * LdCtrlReadFunctionProp{};
    // xs:choice LdCtrlWriteMem_t * LdCtrlWriteMem{};
    // xs:choice LdCtrlCompareMem_t * LdCtrlCompareMem{};
    // xs:choice LdCtrlLoadImageMem_t * LdCtrlLoadImageMem{};
    // xs:choice LdCtrlWriteRelMem_t * LdCtrlWriteRelMem{};
    // xs:choice LdCtrlCompareRelMem_t * LdCtrlCompareRelMem{};
    // xs:choice LdCtrlLoadImageRelMem_t * LdCtrlLoadImageRelMem{};
    // xs:choice LdCtrlConnect_t * LdCtrlConnect{};
    // xs:choice LdCtrlDisconnect_t * LdCtrlDisconnect{};
    // xs:choice LdCtrlRestart_t * LdCtrlRestart{};
    // xs:choice LdCtrlMasterReset_t * LdCtrlMasterReset{};
    // xs:choice LdCtrlDelay_t * LdCtrlDelay{};
    // xs:choice LdCtrlSetControlVariable_t * LdCtrlSetControlVariable{};
    // xs:choice LdCtrlMapError_t * LdCtrlMapError{};
    // xs:choice LdCtrlProgressText_t * LdCtrlProgressText{};
    // xs:choice LdCtrlDeclarePropDesc_t * LdCtrlDeclarePropDesc{};
    // xs:choice LdCtrlClearLCFilterTable_t * LdCtrlClearLCFilterTable{};
    // xs:choice LdCtrlMerge_t * LdCtrlMerge{};
    // xs:choice LdCtrlBaseChoose_t * Choose{};
};

LoadProcedures_LoadProcedure_t * make_LoadProcedures_LoadProcedure_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
