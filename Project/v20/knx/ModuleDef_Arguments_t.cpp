/* This file is generated. */

#include <Project/v20/knx/ModuleDef_Arguments_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/KNX_t.h>

namespace Project {
namespace v20 {
namespace knx {

ModuleDef_Arguments_t::ModuleDef_Arguments_t(Base * parent) :
    Base(parent)
{
}

ModuleDef_Arguments_t::~ModuleDef_Arguments_t()
{
}

void ModuleDef_Arguments_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("Argument")) {
            QString newId = reader.attributes().value("Id").toString();
            Q_ASSERT(!newId.isEmpty());
            ModuleDef_Arguments_Argument_t * newArgument;
            if (Argument.contains(newId)) {
                newArgument = Argument[newId];
            } else {
                newArgument = make_ModuleDef_Arguments_Argument_t(this);
                Argument[newId] = newArgument;
            }
            newArgument->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ModuleDef_Arguments_t::tableColumnCount() const
{
    return 1;
}

QVariant ModuleDef_Arguments_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ModuleDef_Arguments";
        }
        break;
    }
    return QVariant();
}

QVariant ModuleDef_Arguments_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant ModuleDef_Arguments_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "Arguments";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ModuleDef_Arguments");
    }
    return QVariant();
}

ModuleDef_Arguments_t * make_ModuleDef_Arguments_t(Base * parent)
{
    return new ModuleDef_Arguments_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
