/* This file is generated. */

#pragma once

#include <Project/v20/knx/String255_t.h>

namespace Project {
namespace v20 {
namespace knx {

using ManufacturerData_Manufacturer_Baggages_Baggage_Name_t = String255_t;

} // namespace knx
} // namespace v20
} // namespace Project
