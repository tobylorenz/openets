/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v20/knx/Guid_t.h>
#include <Project/xs/Int.h>

namespace Project {
namespace v20 {
namespace knx {

/* forward declarations */
class HawkConfigurationData_Features_t;
class HawkConfigurationData_InterfaceObjects_t;
class HawkConfigurationData_MemorySegments_t;
class HawkConfigurationData_Procedures_t;
class HawkConfigurationData_Resources_t;

class HawkConfigurationData_t : public Base
{
    Q_OBJECT

public:
    explicit HawkConfigurationData_t(Base * parent = nullptr);
    virtual ~HawkConfigurationData_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    Guid_t Ets3SystemPlugin{};
    xs::Int LegacyVersion{};

    /* elements */
    HawkConfigurationData_Features_t * Features{};
    HawkConfigurationData_Resources_t * Resources{};
    HawkConfigurationData_Procedures_t * Procedures{};
    HawkConfigurationData_MemorySegments_t * MemorySegments{};
    HawkConfigurationData_InterfaceObjects_t * InterfaceObjects{};
};

HawkConfigurationData_t * make_HawkConfigurationData_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
