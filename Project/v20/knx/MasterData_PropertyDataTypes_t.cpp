/* This file is generated. */

#include <Project/v20/knx/MasterData_PropertyDataTypes_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/KNX_t.h>

namespace Project {
namespace v20 {
namespace knx {

MasterData_PropertyDataTypes_t::MasterData_PropertyDataTypes_t(Base * parent) :
    Base(parent)
{
}

MasterData_PropertyDataTypes_t::~MasterData_PropertyDataTypes_t()
{
}

void MasterData_PropertyDataTypes_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("PropertyDataType")) {
            QString newId = reader.attributes().value("Id").toString();
            Q_ASSERT(!newId.isEmpty());
            MasterData_PropertyDataTypes_PropertyDataType_t * newPropertyDataType;
            if (PropertyDataType.contains(newId)) {
                newPropertyDataType = PropertyDataType[newId];
            } else {
                newPropertyDataType = make_MasterData_PropertyDataTypes_PropertyDataType_t(this);
                PropertyDataType[newId] = newPropertyDataType;
            }
            newPropertyDataType->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int MasterData_PropertyDataTypes_t::tableColumnCount() const
{
    return 1;
}

QVariant MasterData_PropertyDataTypes_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "MasterData_PropertyDataTypes";
        }
        break;
    }
    return QVariant();
}

QVariant MasterData_PropertyDataTypes_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant MasterData_PropertyDataTypes_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "PropertyDataTypes";
    case Qt::DecorationRole:
        return QIcon::fromTheme("MasterData_PropertyDataTypes");
    }
    return QVariant();
}

MasterData_PropertyDataTypes_t * make_MasterData_PropertyDataTypes_t(Base * parent)
{
    return new MasterData_PropertyDataTypes_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
