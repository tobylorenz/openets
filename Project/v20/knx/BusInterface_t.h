/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v20/knx/IDREF.h>
#include <Project/v20/knx/String20_t.h>
#include <Project/v20/knx/String255_t.h>
#include <Project/xs/Base64Binary.h>
#include <Project/xs/String.h>

namespace Project {
namespace v20 {
namespace knx {

/* forward declarations */
class BusInterface_Connectors_t;

class BusInterface_t : public Base
{
    Q_OBJECT

public:
    explicit BusInterface_t(Base * parent = nullptr);
    virtual ~BusInterface_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    IDREF RefId{};
    String255_t Name{};
    xs::String Description{};
    xs::String Comment{};
    String20_t Password{};
    xs::Base64Binary PasswordHash{};

    /* elements */
    BusInterface_Connectors_t * Connectors{};

    /* getters */
    // Base * getTODO() const; // attribute: RefId
};

BusInterface_t * make_BusInterface_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
