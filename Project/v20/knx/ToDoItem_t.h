/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v20/knx/ToDoStatus_t.h>
#include <Project/xs/String.h>

namespace Project {
namespace v20 {
namespace knx {

class ToDoItem_t : public Base
{
    Q_OBJECT

public:
    explicit ToDoItem_t(Base * parent = nullptr);
    virtual ~ToDoItem_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::String Description{};
    xs::String ObjectPath{};
    ToDoStatus_t Status{};
};

ToDoItem_t * make_ToDoItem_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
