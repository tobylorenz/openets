/* This file is generated. */

#include <Project/v20/knx/ModuleDefStatic_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/KNX_t.h>
#include <Project/v20/knx/ModuleDefLoadProcedures_t.h>
#include <Project/v20/knx/ModuleDefStatic_Allocators_t.h>
#include <Project/v20/knx/ModuleDefStatic_ComObjectRefs_t.h>
#include <Project/v20/knx/ModuleDefStatic_ComObjects_t.h>
#include <Project/v20/knx/ModuleDefStatic_ParameterCalculations_t.h>
#include <Project/v20/knx/ModuleDefStatic_ParameterRefs_t.h>
#include <Project/v20/knx/ModuleDefStatic_ParameterValidations_t.h>
#include <Project/v20/knx/ModuleDefStatic_Parameters_t.h>

namespace Project {
namespace v20 {
namespace knx {

ModuleDefStatic_t::ModuleDefStatic_t(Base * parent) :
    Base(parent)
{
}

ModuleDefStatic_t::~ModuleDefStatic_t()
{
}

void ModuleDefStatic_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("Parameters")) {
            if (!Parameters) {
                Parameters = make_ModuleDefStatic_Parameters_t(this);
            }
            Parameters->read(reader);
            continue;
        }
        if (reader.name() == QString("ParameterRefs")) {
            if (!ParameterRefs) {
                ParameterRefs = make_ModuleDefStatic_ParameterRefs_t(this);
            }
            ParameterRefs->read(reader);
            continue;
        }
        if (reader.name() == QString("ParameterCalculations")) {
            if (!ParameterCalculations) {
                ParameterCalculations = make_ModuleDefStatic_ParameterCalculations_t(this);
            }
            ParameterCalculations->read(reader);
            continue;
        }
        if (reader.name() == QString("ParameterValidations")) {
            if (!ParameterValidations) {
                ParameterValidations = make_ModuleDefStatic_ParameterValidations_t(this);
            }
            ParameterValidations->read(reader);
            continue;
        }
        if (reader.name() == QString("ComObjects")) {
            if (!ComObjects) {
                ComObjects = make_ModuleDefStatic_ComObjects_t(this);
            }
            ComObjects->read(reader);
            continue;
        }
        if (reader.name() == QString("ComObjectRefs")) {
            if (!ComObjectRefs) {
                ComObjectRefs = make_ModuleDefStatic_ComObjectRefs_t(this);
            }
            ComObjectRefs->read(reader);
            continue;
        }
        if (reader.name() == QString("LoadProcedures")) {
            if (!LoadProcedures) {
                LoadProcedures = make_ModuleDefLoadProcedures_t(this);
            }
            LoadProcedures->read(reader);
            continue;
        }
        if (reader.name() == QString("Allocators")) {
            if (!Allocators) {
                Allocators = make_ModuleDefStatic_Allocators_t(this);
            }
            Allocators->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ModuleDefStatic_t::tableColumnCount() const
{
    return 1;
}

QVariant ModuleDefStatic_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ModuleDefStatic";
        }
        break;
    }
    return QVariant();
}

QVariant ModuleDefStatic_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant ModuleDefStatic_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "ModuleDefStatic";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ModuleDefStatic");
    }
    return QVariant();
}

ModuleDefStatic_t * make_ModuleDefStatic_t(Base * parent)
{
    return new ModuleDefStatic_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
