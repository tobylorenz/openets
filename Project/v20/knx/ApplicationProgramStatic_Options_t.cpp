/* This file is generated. */

#include <Project/v20/knx/ApplicationProgramStatic_Options_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/KNX_t.h>

namespace Project {
namespace v20 {
namespace knx {

ApplicationProgramStatic_Options_t::ApplicationProgramStatic_Options_t(Base * parent) :
    Base(parent)
{
}

ApplicationProgramStatic_Options_t::~ApplicationProgramStatic_Options_t()
{
}

void ApplicationProgramStatic_Options_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("PreferPartialDownloadIfApplicationLoaded")) {
            PreferPartialDownloadIfApplicationLoaded = attribute.value().toString();
            continue;
        }
        if (name == QString("EasyCtrlModeModeStyleEmptyGroupComTables")) {
            EasyCtrlModeModeStyleEmptyGroupComTables = attribute.value().toString();
            continue;
        }
        if (name == QString("SetObjectTableLengthAlwaysToOne")) {
            SetObjectTableLengthAlwaysToOne = attribute.value().toString();
            continue;
        }
        if (name == QString("TextParameterEncoding")) {
            TextParameterEncoding = attribute.value().toString();
            continue;
        }
        if (name == QString("TextParameterEncodingSelector")) {
            TextParameterEncodingSelector = attribute.value().toString();
            continue;
        }
        if (name == QString("TextParameterZeroTerminate")) {
            TextParameterZeroTerminate = attribute.value().toString();
            continue;
        }
        if (name == QString("ParameterByteOrder")) {
            ParameterByteOrder = attribute.value().toString();
            continue;
        }
        if (name == QString("PartialDownloadOnlyVisibleParameters")) {
            PartialDownloadOnlyVisibleParameters = attribute.value().toString();
            continue;
        }
        if (name == QString("LegacyNoPartialDownload")) {
            LegacyNoPartialDownload = attribute.value().toString();
            continue;
        }
        if (name == QString("LegacyNoMemoryVerifyMode")) {
            LegacyNoMemoryVerifyMode = attribute.value().toString();
            continue;
        }
        if (name == QString("LegacyNoOptimisticWrite")) {
            LegacyNoOptimisticWrite = attribute.value().toString();
            continue;
        }
        if (name == QString("LegacyDoNotReportPropertyWriteErrors")) {
            LegacyDoNotReportPropertyWriteErrors = attribute.value().toString();
            continue;
        }
        if (name == QString("LegacyNoBackgroundDownload")) {
            LegacyNoBackgroundDownload = attribute.value().toString();
            continue;
        }
        if (name == QString("LegacyDoNotCheckManufacturerId")) {
            LegacyDoNotCheckManufacturerId = attribute.value().toString();
            continue;
        }
        if (name == QString("LegacyAlwaysReloadAppIfCoVisibilityChanged")) {
            LegacyAlwaysReloadAppIfCoVisibilityChanged = attribute.value().toString();
            continue;
        }
        if (name == QString("LegacyNeverReloadAppIfCoVisibilityChanged")) {
            LegacyNeverReloadAppIfCoVisibilityChanged = attribute.value().toString();
            continue;
        }
        if (name == QString("LegacyDoNotSupportUndoDelete")) {
            LegacyDoNotSupportUndoDelete = attribute.value().toString();
            continue;
        }
        if (name == QString("LegacyAllowPartialDownloadIfAp2Mismatch")) {
            LegacyAllowPartialDownloadIfAp2Mismatch = attribute.value().toString();
            continue;
        }
        if (name == QString("LegacyKeepObjectTableGaps")) {
            LegacyKeepObjectTableGaps = attribute.value().toString();
            continue;
        }
        if (name == QString("LegacyProxyCommunicationObjects")) {
            LegacyProxyCommunicationObjects = attribute.value().toString();
            continue;
        }
        if (name == QString("DeviceInfoIgnoreRunState")) {
            DeviceInfoIgnoreRunState = attribute.value().toString();
            continue;
        }
        if (name == QString("DeviceInfoIgnoreLoadedState")) {
            DeviceInfoIgnoreLoadedState = attribute.value().toString();
            continue;
        }
        if (name == QString("DeviceCompareAllowCompatibleManufacturerId")) {
            DeviceCompareAllowCompatibleManufacturerId = attribute.value().toString();
            continue;
        }
        if (name == QString("LineCoupler0912NewProgrammingStyle")) {
            LineCoupler0912NewProgrammingStyle = attribute.value().toString();
            continue;
        }
        if (name == QString("MaxRoutingApduLength")) {
            MaxRoutingApduLength = attribute.value().toString();
            continue;
        }
        if (name == QString("Comparable")) {
            Comparable = attribute.value().toString();
            continue;
        }
        if (name == QString("Reconstructable")) {
            Reconstructable = attribute.value().toString();
            continue;
        }
        if (name == QString("DownloadInvisibleParameters")) {
            DownloadInvisibleParameters = attribute.value().toString();
            continue;
        }
        if (name == QString("SupportsExtendedMemoryServices")) {
            SupportsExtendedMemoryServices = attribute.value().toString();
            continue;
        }
        if (name == QString("SupportsExtendedPropertyServices")) {
            SupportsExtendedPropertyServices = attribute.value().toString();
            continue;
        }
        if (name == QString("SupportsIpSystemBroadcast")) {
            SupportsIpSystemBroadcast = attribute.value().toString();
            continue;
        }
        if (name == QString("NotLoadable")) {
            NotLoadable = attribute.value().toString();
            continue;
        }
        if (name == QString("NotLoadableMessageRef")) {
            QString value = attribute.value().toString();
            if (!value.isEmpty())
                qWarning() << "knx:ApplicationProgramStatic_Options_t attribute NotLoadableMessageRef references to" << value;
        }
        if (name == QString("NotLoadableMessageRef")) {
            NotLoadableMessageRef = attribute.value().toString();
            continue;
        }
        if (name == QString("CustomerAdjustableParameters")) {
            CustomerAdjustableParameters = attribute.value().toString();
            continue;
        }
        if (name == QString("MasterResetOnCRCMismatch")) {
            MasterResetOnCRCMismatch = attribute.value().toString();
            continue;
        }
        if (name == QString("PromptBeforeFullDownload")) {
            PromptBeforeFullDownload = attribute.value().toString();
            continue;
        }
        if (name == QString("LegacyPatchManufacturerIdInTaskSegment")) {
            LegacyPatchManufacturerIdInTaskSegment = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ApplicationProgramStatic_Options_t::tableColumnCount() const
{
    return 38;
}

QVariant ApplicationProgramStatic_Options_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ApplicationProgramStatic_Options";
        }
        if (qualifiedName == QString("PreferPartialDownloadIfApplicationLoaded")) {
            return PreferPartialDownloadIfApplicationLoaded;
        }
        if (qualifiedName == QString("EasyCtrlModeModeStyleEmptyGroupComTables")) {
            return EasyCtrlModeModeStyleEmptyGroupComTables;
        }
        if (qualifiedName == QString("SetObjectTableLengthAlwaysToOne")) {
            return SetObjectTableLengthAlwaysToOne;
        }
        if (qualifiedName == QString("TextParameterEncoding")) {
            return TextParameterEncoding;
        }
        if (qualifiedName == QString("TextParameterEncodingSelector")) {
            return TextParameterEncodingSelector;
        }
        if (qualifiedName == QString("TextParameterZeroTerminate")) {
            return TextParameterZeroTerminate;
        }
        if (qualifiedName == QString("ParameterByteOrder")) {
            return ParameterByteOrder;
        }
        if (qualifiedName == QString("PartialDownloadOnlyVisibleParameters")) {
            return PartialDownloadOnlyVisibleParameters;
        }
        if (qualifiedName == QString("LegacyNoPartialDownload")) {
            return LegacyNoPartialDownload;
        }
        if (qualifiedName == QString("LegacyNoMemoryVerifyMode")) {
            return LegacyNoMemoryVerifyMode;
        }
        if (qualifiedName == QString("LegacyNoOptimisticWrite")) {
            return LegacyNoOptimisticWrite;
        }
        if (qualifiedName == QString("LegacyDoNotReportPropertyWriteErrors")) {
            return LegacyDoNotReportPropertyWriteErrors;
        }
        if (qualifiedName == QString("LegacyNoBackgroundDownload")) {
            return LegacyNoBackgroundDownload;
        }
        if (qualifiedName == QString("LegacyDoNotCheckManufacturerId")) {
            return LegacyDoNotCheckManufacturerId;
        }
        if (qualifiedName == QString("LegacyAlwaysReloadAppIfCoVisibilityChanged")) {
            return LegacyAlwaysReloadAppIfCoVisibilityChanged;
        }
        if (qualifiedName == QString("LegacyNeverReloadAppIfCoVisibilityChanged")) {
            return LegacyNeverReloadAppIfCoVisibilityChanged;
        }
        if (qualifiedName == QString("LegacyDoNotSupportUndoDelete")) {
            return LegacyDoNotSupportUndoDelete;
        }
        if (qualifiedName == QString("LegacyAllowPartialDownloadIfAp2Mismatch")) {
            return LegacyAllowPartialDownloadIfAp2Mismatch;
        }
        if (qualifiedName == QString("LegacyKeepObjectTableGaps")) {
            return LegacyKeepObjectTableGaps;
        }
        if (qualifiedName == QString("LegacyProxyCommunicationObjects")) {
            return LegacyProxyCommunicationObjects;
        }
        if (qualifiedName == QString("DeviceInfoIgnoreRunState")) {
            return DeviceInfoIgnoreRunState;
        }
        if (qualifiedName == QString("DeviceInfoIgnoreLoadedState")) {
            return DeviceInfoIgnoreLoadedState;
        }
        if (qualifiedName == QString("DeviceCompareAllowCompatibleManufacturerId")) {
            return DeviceCompareAllowCompatibleManufacturerId;
        }
        if (qualifiedName == QString("LineCoupler0912NewProgrammingStyle")) {
            return LineCoupler0912NewProgrammingStyle;
        }
        if (qualifiedName == QString("MaxRoutingApduLength")) {
            return MaxRoutingApduLength;
        }
        if (qualifiedName == QString("Comparable")) {
            return Comparable;
        }
        if (qualifiedName == QString("Reconstructable")) {
            return Reconstructable;
        }
        if (qualifiedName == QString("DownloadInvisibleParameters")) {
            return DownloadInvisibleParameters;
        }
        if (qualifiedName == QString("SupportsExtendedMemoryServices")) {
            return SupportsExtendedMemoryServices;
        }
        if (qualifiedName == QString("SupportsExtendedPropertyServices")) {
            return SupportsExtendedPropertyServices;
        }
        if (qualifiedName == QString("SupportsIpSystemBroadcast")) {
            return SupportsIpSystemBroadcast;
        }
        if (qualifiedName == QString("NotLoadable")) {
            return NotLoadable;
        }
        if (qualifiedName == QString("NotLoadableMessageRef")) {
            return NotLoadableMessageRef;
        }
        if (qualifiedName == QString("CustomerAdjustableParameters")) {
            return CustomerAdjustableParameters;
        }
        if (qualifiedName == QString("MasterResetOnCRCMismatch")) {
            return MasterResetOnCRCMismatch;
        }
        if (qualifiedName == QString("PromptBeforeFullDownload")) {
            return PromptBeforeFullDownload;
        }
        if (qualifiedName == QString("LegacyPatchManufacturerIdInTaskSegment")) {
            return LegacyPatchManufacturerIdInTaskSegment;
        }
        break;
    }
    return QVariant();
}

QVariant ApplicationProgramStatic_Options_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "PreferPartialDownloadIfApplicationLoaded";
            case 2:
                return "EasyCtrlModeModeStyleEmptyGroupComTables";
            case 3:
                return "SetObjectTableLengthAlwaysToOne";
            case 4:
                return "TextParameterEncoding";
            case 5:
                return "TextParameterEncodingSelector";
            case 6:
                return "TextParameterZeroTerminate";
            case 7:
                return "ParameterByteOrder";
            case 8:
                return "PartialDownloadOnlyVisibleParameters";
            case 9:
                return "LegacyNoPartialDownload";
            case 10:
                return "LegacyNoMemoryVerifyMode";
            case 11:
                return "LegacyNoOptimisticWrite";
            case 12:
                return "LegacyDoNotReportPropertyWriteErrors";
            case 13:
                return "LegacyNoBackgroundDownload";
            case 14:
                return "LegacyDoNotCheckManufacturerId";
            case 15:
                return "LegacyAlwaysReloadAppIfCoVisibilityChanged";
            case 16:
                return "LegacyNeverReloadAppIfCoVisibilityChanged";
            case 17:
                return "LegacyDoNotSupportUndoDelete";
            case 18:
                return "LegacyAllowPartialDownloadIfAp2Mismatch";
            case 19:
                return "LegacyKeepObjectTableGaps";
            case 20:
                return "LegacyProxyCommunicationObjects";
            case 21:
                return "DeviceInfoIgnoreRunState";
            case 22:
                return "DeviceInfoIgnoreLoadedState";
            case 23:
                return "DeviceCompareAllowCompatibleManufacturerId";
            case 24:
                return "LineCoupler0912NewProgrammingStyle";
            case 25:
                return "MaxRoutingApduLength";
            case 26:
                return "Comparable";
            case 27:
                return "Reconstructable";
            case 28:
                return "DownloadInvisibleParameters";
            case 29:
                return "SupportsExtendedMemoryServices";
            case 30:
                return "SupportsExtendedPropertyServices";
            case 31:
                return "SupportsIpSystemBroadcast";
            case 32:
                return "NotLoadable";
            case 33:
                return "NotLoadableMessageRef";
            case 34:
                return "CustomerAdjustableParameters";
            case 35:
                return "MasterResetOnCRCMismatch";
            case 36:
                return "PromptBeforeFullDownload";
            case 37:
                return "LegacyPatchManufacturerIdInTaskSegment";
            }
        }
    }
    return QVariant();
}

QVariant ApplicationProgramStatic_Options_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "Options";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ApplicationProgramStatic_Options");
    }
    return QVariant();
}

Base * ApplicationProgramStatic_Options_t::getNotLoadableMessage() const
{
    if (NotLoadableMessageRef.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<Base *>(knx->ids[NotLoadableMessageRef]);
}

ApplicationProgramStatic_Options_t * make_ApplicationProgramStatic_Options_t(Base * parent)
{
    return new ApplicationProgramStatic_Options_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
