/* This file is generated. */

#include <Project/v20/knx/SegmentBase_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/KNX_t.h>

namespace Project {
namespace v20 {
namespace knx {

SegmentBase_t::SegmentBase_t(Base * parent) :
    Base(parent)
{
}

SegmentBase_t::~SegmentBase_t()
{
}

void SegmentBase_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == "Id") {
            Id = attribute.value().toString();
            KNX_t * knx = findParent<KNX_t *>();
            Q_ASSERT(knx);
            knx->ids[Id] = this;
            continue;
        }
        if (name == QString("Name")) {
            Name = attribute.value().toString();
            continue;
        }
        if (name == QString("Size")) {
            Size = attribute.value().toString();
            continue;
        }
        if (name == QString("InternalDescription")) {
            InternalDescription = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("Data")) {
            if (!Data) {
                Data = make_SimpleElementTextType("Data", this);
            }
            Data->read(reader);
            continue;
        }
        if (reader.name() == QString("Mask")) {
            if (!Mask) {
                Mask = make_SimpleElementTextType("Mask", this);
            }
            Mask->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int SegmentBase_t::tableColumnCount() const
{
    return 5;
}

QVariant SegmentBase_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "SegmentBase";
        }
        if (qualifiedName == QString("Id")) {
            return Id;
        }
        if (qualifiedName == QString("Name")) {
            return Name;
        }
        if (qualifiedName == QString("Size")) {
            return Size;
        }
        if (qualifiedName == QString("InternalDescription")) {
            return InternalDescription;
        }
        break;
    }
    return QVariant();
}

QVariant SegmentBase_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Id";
            case 2:
                return "Name";
            case 3:
                return "Size";
            case 4:
                return "InternalDescription";
            }
        }
    }
    return QVariant();
}

QVariant SegmentBase_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return !Name.isEmpty() ? Name : "SegmentBase";
    case Qt::DecorationRole:
        return QIcon::fromTheme("SegmentBase");
    }
    return QVariant();
}

SegmentBase_t * make_SegmentBase_t(Base * parent)
{
    return new SegmentBase_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
