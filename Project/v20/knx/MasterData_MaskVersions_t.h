/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v20/knx/MaskVersion_t.h>

namespace Project {
namespace v20 {
namespace knx {

class MasterData_MaskVersions_t : public Base
{
    Q_OBJECT

public:
    explicit MasterData_MaskVersions_t(Base * parent = nullptr);
    virtual ~MasterData_MaskVersions_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* elements */
    QMap<xs::ID, MaskVersion_t *> MaskVersion; // key: Id
};

MasterData_MaskVersions_t * make_MasterData_MaskVersions_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
