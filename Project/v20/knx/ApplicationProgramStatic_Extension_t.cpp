/* This file is generated. */

#include <Project/v20/knx/ApplicationProgramStatic_Extension_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/KNX_t.h>

namespace Project {
namespace v20 {
namespace knx {

ApplicationProgramStatic_Extension_t::ApplicationProgramStatic_Extension_t(Base * parent) :
    Base(parent)
{
}

ApplicationProgramStatic_Extension_t::~ApplicationProgramStatic_Extension_t()
{
}

void ApplicationProgramStatic_Extension_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("EtsDownloadPlugin")) {
            EtsDownloadPlugin = attribute.value().toString();
            continue;
        }
        if (name == QString("EtsUiPlugin")) {
            EtsUiPlugin = attribute.value().toString();
            continue;
        }
        if (name == QString("EtsDataHandler")) {
            EtsDataHandler = attribute.value().toString();
            continue;
        }
        if (name == QString("EtsDataHandlerCapabilities")) {
            EtsDataHandlerCapabilities = attribute.value().toString().split(' ');
            continue;
        }
        if (name == QString("RequiresExternalSoftware")) {
            RequiresExternalSoftware = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("Baggage")) {
            QString newRefId = reader.attributes().value("RefId").toString();
            Q_ASSERT(!newRefId.isEmpty());
            ApplicationProgramStatic_Extension_Baggage_t * newBaggage;
            if (Baggage.contains(newRefId)) {
                newBaggage = Baggage[newRefId];
            } else {
                newBaggage = make_ApplicationProgramStatic_Extension_Baggage_t(this);
                Baggage[newRefId] = newBaggage;
            }
            newBaggage->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ApplicationProgramStatic_Extension_t::tableColumnCount() const
{
    return 6;
}

QVariant ApplicationProgramStatic_Extension_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ApplicationProgramStatic_Extension";
        }
        if (qualifiedName == QString("EtsDownloadPlugin")) {
            return EtsDownloadPlugin;
        }
        if (qualifiedName == QString("EtsUiPlugin")) {
            return EtsUiPlugin;
        }
        if (qualifiedName == QString("EtsDataHandler")) {
            return EtsDataHandler;
        }
        if (qualifiedName == QString("EtsDataHandlerCapabilities")) {
            return EtsDataHandlerCapabilities.join(' ');
        }
        if (qualifiedName == QString("RequiresExternalSoftware")) {
            return RequiresExternalSoftware;
        }
        break;
    }
    return QVariant();
}

QVariant ApplicationProgramStatic_Extension_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "EtsDownloadPlugin";
            case 2:
                return "EtsUiPlugin";
            case 3:
                return "EtsDataHandler";
            case 4:
                return "EtsDataHandlerCapabilities";
            case 5:
                return "RequiresExternalSoftware";
            }
        }
    }
    return QVariant();
}

QVariant ApplicationProgramStatic_Extension_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "Extension";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ApplicationProgramStatic_Extension");
    }
    return QVariant();
}

ApplicationProgramStatic_Extension_t * make_ApplicationProgramStatic_Extension_t(Base * parent)
{
    return new ApplicationProgramStatic_Extension_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
