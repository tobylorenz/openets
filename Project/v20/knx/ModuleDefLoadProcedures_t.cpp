/* This file is generated. */

#include <Project/v20/knx/ModuleDefLoadProcedures_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/KNX_t.h>

namespace Project {
namespace v20 {
namespace knx {

ModuleDefLoadProcedures_t::ModuleDefLoadProcedures_t(Base * parent) :
    Base(parent)
{
}

ModuleDefLoadProcedures_t::~ModuleDefLoadProcedures_t()
{
}

void ModuleDefLoadProcedures_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("LoadProcedure")) {
            auto * newLoadProcedure = make_ModuleDefLoadProcedure_t(this);
            newLoadProcedure->read(reader);
            LoadProcedure.append(newLoadProcedure);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ModuleDefLoadProcedures_t::tableColumnCount() const
{
    return 1;
}

QVariant ModuleDefLoadProcedures_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ModuleDefLoadProcedures";
        }
        break;
    }
    return QVariant();
}

QVariant ModuleDefLoadProcedures_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant ModuleDefLoadProcedures_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "ModuleDefLoadProcedures";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ModuleDefLoadProcedures");
    }
    return QVariant();
}

ModuleDefLoadProcedures_t * make_ModuleDefLoadProcedures_t(Base * parent)
{
    return new ModuleDefLoadProcedures_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
