/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v20/knx/ModuleDefLoadProcedure_t.h>

namespace Project {
namespace v20 {
namespace knx {

class ModuleDefLoadProcedures_t : public Base
{
    Q_OBJECT

public:
    explicit ModuleDefLoadProcedures_t(Base * parent = nullptr);
    virtual ~ModuleDefLoadProcedures_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* elements */
    QVector<ModuleDefLoadProcedure_t *> LoadProcedure;
};

ModuleDefLoadProcedures_t * make_ModuleDefLoadProcedures_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
