/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/SimpleElementTextType.h>
#include <Project/v20/knx/MasterData_Manufacturers_Manufacturer_ImportGroup_t.h>
#include <Project/v20/knx/MasterData_Manufacturers_Manufacturer_ImportRestriction_t.h>
#include <Project/v20/knx/MasterData_Manufacturers_Manufacturer_OrderNumberWildcardCharacter_t.h>
#include <Project/v20/knx/MemberStatus_t.h>
#include <Project/v20/knx/String255_t.h>
#include <Project/xs/ID.h>
#include <Project/xs/Language.h>
#include <Project/xs/String.h>
#include <Project/xs/UnsignedShort.h>

namespace Project {
namespace v20 {
namespace knx {

/* forward declarations */
class MasterData_Manufacturers_Manufacturer_DatapointRoles_t;
class MasterData_Manufacturers_Manufacturer_DatapointTypes_t;
class MasterData_Manufacturers_Manufacturer_FunctionTypes_t;
class MasterData_Manufacturers_Manufacturer_PublicKeys_t;
class MasterData_Manufacturers_Manufacturer_SpaceUsages_t;

class MasterData_Manufacturers_Manufacturer_t : public Base
{
    Q_OBJECT

public:
    explicit MasterData_Manufacturers_Manufacturer_t(Base * parent = nullptr);
    virtual ~MasterData_Manufacturers_Manufacturer_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::ID Id{};
    String255_t Name{};
    xs::UnsignedShort KnxManufacturerId{};
    xs::Language DefaultLanguage{};
    xs::UnsignedShort CompatibilityGroup{};
    MasterData_Manufacturers_Manufacturer_ImportRestriction_t ImportRestriction{"Own"};
    MasterData_Manufacturers_Manufacturer_ImportGroup_t ImportGroup{};
    MasterData_Manufacturers_Manufacturer_OrderNumberWildcardCharacter_t OrderNumberWildcardCharacter{};
    MemberStatus_t MemberStatus{"Active"};

    /* elements */
    SimpleElementTextType * OrderNumberFormattingScript{};
    MasterData_Manufacturers_Manufacturer_PublicKeys_t * PublicKeys{};
    MasterData_Manufacturers_Manufacturer_DatapointTypes_t * DatapointTypes{};
    MasterData_Manufacturers_Manufacturer_DatapointRoles_t * DatapointRoles{};
    MasterData_Manufacturers_Manufacturer_FunctionTypes_t * FunctionTypes{};
    MasterData_Manufacturers_Manufacturer_SpaceUsages_t * SpaceUsages{};
};

MasterData_Manufacturers_Manufacturer_t * make_MasterData_Manufacturers_Manufacturer_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
