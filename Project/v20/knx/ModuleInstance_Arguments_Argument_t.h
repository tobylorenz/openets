/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v20/knx/RELIDREF.h>
#include <Project/v20/knx/Value_t.h>

namespace Project {
namespace v20 {
namespace knx {

class ModuleInstance_Arguments_Argument_t : public Base
{
    Q_OBJECT

public:
    explicit ModuleInstance_Arguments_Argument_t(Base * parent = nullptr);
    virtual ~ModuleInstance_Arguments_Argument_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    RELIDREF RefId{};
    Value_t Value{};

    /* getters */
    // Base * getTODO() const; // attribute: RefId
};

ModuleInstance_Arguments_Argument_t * make_ModuleInstance_Arguments_Argument_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
