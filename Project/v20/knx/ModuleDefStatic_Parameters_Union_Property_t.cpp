/* This file is generated. */

#include <Project/v20/knx/ModuleDefStatic_Parameters_Union_Property_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/KNX_t.h>
#include <Project/v20/knx/ModuleDef_Arguments_Argument_t.h>

namespace Project {
namespace v20 {
namespace knx {

ModuleDefStatic_Parameters_Union_Property_t::ModuleDefStatic_Parameters_Union_Property_t(Base * parent) :
    Base(parent)
{
}

ModuleDefStatic_Parameters_Union_Property_t::~ModuleDefStatic_Parameters_Union_Property_t()
{
}

void ModuleDefStatic_Parameters_Union_Property_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("ObjectIndex")) {
            ObjectIndex = attribute.value().toString();
            continue;
        }
        if (name == QString("ObjectType")) {
            ObjectType = attribute.value().toString();
            continue;
        }
        if (name == QString("Occurrence")) {
            Occurrence = attribute.value().toString();
            continue;
        }
        if (name == QString("PropertyId")) {
            PropertyId = attribute.value().toString();
            continue;
        }
        if (name == QString("Offset")) {
            Offset = attribute.value().toString();
            continue;
        }
        if (name == QString("BitOffset")) {
            BitOffset = attribute.value().toString();
            continue;
        }
        if (name == QString("BaseOffset")) {
            BaseOffset = attribute.value().toString();
            continue;
        }
        if (name == QString("BaseIndex")) {
            QString value = attribute.value().toString();
            if (!value.isEmpty())
                qWarning() << "knx:ModuleDefStatic_Parameters_Union_Property_t attribute BaseIndex references to" << value;
        }
        if (name == QString("BaseIndex")) {
            BaseIndex = attribute.value().toString();
            continue;
        }
        if (name == QString("BaseOccurrence")) {
            QString value = attribute.value().toString();
            if (!value.isEmpty())
                qWarning() << "knx:ModuleDefStatic_Parameters_Union_Property_t attribute BaseOccurrence references to" << value;
        }
        if (name == QString("BaseOccurrence")) {
            BaseOccurrence = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ModuleDefStatic_Parameters_Union_Property_t::tableColumnCount() const
{
    return 10;
}

QVariant ModuleDefStatic_Parameters_Union_Property_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ModuleDefStatic_Parameters_Union_Property";
        }
        if (qualifiedName == QString("ObjectIndex")) {
            return ObjectIndex;
        }
        if (qualifiedName == QString("ObjectType")) {
            return ObjectType;
        }
        if (qualifiedName == QString("Occurrence")) {
            return Occurrence;
        }
        if (qualifiedName == QString("PropertyId")) {
            return PropertyId;
        }
        if (qualifiedName == QString("Offset")) {
            return Offset;
        }
        if (qualifiedName == QString("BitOffset")) {
            return BitOffset;
        }
        if (qualifiedName == QString("BaseOffset")) {
            return BaseOffset;
        }
        if (qualifiedName == QString("BaseIndex")) {
            return BaseIndex;
        }
        if (qualifiedName == QString("BaseOccurrence")) {
            return BaseOccurrence;
        }
        break;
    }
    return QVariant();
}

QVariant ModuleDefStatic_Parameters_Union_Property_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "ObjectIndex";
            case 2:
                return "ObjectType";
            case 3:
                return "Occurrence";
            case 4:
                return "PropertyId";
            case 5:
                return "Offset";
            case 6:
                return "BitOffset";
            case 7:
                return "BaseOffset";
            case 8:
                return "BaseIndex";
            case 9:
                return "BaseOccurrence";
            }
        }
    }
    return QVariant();
}

QVariant ModuleDefStatic_Parameters_Union_Property_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "Property";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ModuleDefStatic_Parameters_Union_Property");
    }
    return QVariant();
}

ModuleDef_Arguments_Argument_t * ModuleDefStatic_Parameters_Union_Property_t::getBaseOffset() const
{
    if (BaseOffset.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<ModuleDef_Arguments_Argument_t *>(knx->ids[BaseOffset]);
}

Base * ModuleDefStatic_Parameters_Union_Property_t::getBaseIndex() const
{
    if (BaseIndex.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<Base *>(knx->ids[BaseIndex]);
}

Base * ModuleDefStatic_Parameters_Union_Property_t::getBaseOccurrence() const
{
    if (BaseOccurrence.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<Base *>(knx->ids[BaseOccurrence]);
}

ModuleDefStatic_Parameters_Union_Property_t * make_ModuleDefStatic_Parameters_Union_Property_t(Base * parent)
{
    return new ModuleDefStatic_Parameters_Union_Property_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
