/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v20/knx/Identifier50_t.h>
#include <Project/v20/knx/ModuleDefArgType_t.h>
#include <Project/v20/knx/ModuleDef_Arguments_Argument_Alignment_t.h>
#include <Project/xs/ID.h>
#include <Project/xs/String.h>
#include <Project/xs/UnsignedLong.h>

namespace Project {
namespace v20 {
namespace knx {

class ModuleDef_Arguments_Argument_t : public Base
{
    Q_OBJECT

public:
    explicit ModuleDef_Arguments_Argument_t(Base * parent = nullptr);
    virtual ~ModuleDef_Arguments_Argument_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::ID Id{};
    Identifier50_t Name{};
    ModuleDefArgType_t Type{"Numeric"};
    xs::String InternalDescription{};
    xs::UnsignedLong Allocates{};
    ModuleDef_Arguments_Argument_Alignment_t Alignment{"1"};
};

ModuleDef_Arguments_Argument_t * make_ModuleDef_Arguments_Argument_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
