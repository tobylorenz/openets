/* This file is generated. */

#pragma once

#include <Project/xs/UnsignedInt.h>

namespace Project {
namespace v20 {
namespace knx {

using ApplicationProgramStatic_ComObjectTable_Offset_t = xs::UnsignedInt;

} // namespace knx
} // namespace v20
} // namespace Project
