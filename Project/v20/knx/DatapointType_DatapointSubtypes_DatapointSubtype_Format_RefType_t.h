/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v20/knx/IDREF.h>

namespace Project {
namespace v20 {
namespace knx {

class DatapointType_DatapointSubtypes_DatapointSubtype_Format_RefType_t : public Base
{
    Q_OBJECT

public:
    explicit DatapointType_DatapointSubtypes_DatapointSubtype_Format_RefType_t(Base * parent = nullptr);
    virtual ~DatapointType_DatapointSubtypes_DatapointSubtype_Format_RefType_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    IDREF RefId{};

    /* getters */
    Base * getType() const; // attribute: RefId
};

DatapointType_DatapointSubtypes_DatapointSubtype_Format_RefType_t * make_DatapointType_DatapointSubtypes_DatapointSubtype_Format_RefType_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
