/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v20/knx/P2PLinks_P2PLink_t.h>
#include <Project/xs/ID.h>

namespace Project {
namespace v20 {
namespace knx {

class P2PLinks_t : public Base
{
    Q_OBJECT

public:
    explicit P2PLinks_t(Base * parent = nullptr);
    virtual ~P2PLinks_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* elements */
    QMap<xs::ID, P2PLinks_P2PLink_t *> P2PLink; // key: Id
};

P2PLinks_t * make_P2PLinks_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
