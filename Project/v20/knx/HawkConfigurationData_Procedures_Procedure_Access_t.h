/* This file is generated. */

#pragma once

#include <Project/v20/knx/ResourceAccess_t.h>
#include <Project/xs/List.h>

namespace Project {
namespace v20 {
namespace knx {

using HawkConfigurationData_Procedures_Procedure_Access_t = xs::List<ResourceAccess_t>;

} // namespace knx
} // namespace v20
} // namespace Project
