/* This file is generated. */

#include <Project/v20/knx/ComObjectParameterBlock_Rows_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/ComObjectParameterBlock_Rows_Row_t.h>
#include <Project/v20/knx/KNX_t.h>

namespace Project {
namespace v20 {
namespace knx {

ComObjectParameterBlock_Rows_t::ComObjectParameterBlock_Rows_t(Base * parent) :
    Base(parent)
{
}

ComObjectParameterBlock_Rows_t::~ComObjectParameterBlock_Rows_t()
{
}

void ComObjectParameterBlock_Rows_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("Row")) {
            if (!Row) {
                Row = make_ComObjectParameterBlock_Rows_Row_t(this);
            }
            Row->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ComObjectParameterBlock_Rows_t::tableColumnCount() const
{
    return 1;
}

QVariant ComObjectParameterBlock_Rows_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ComObjectParameterBlock_Rows";
        }
        break;
    }
    return QVariant();
}

QVariant ComObjectParameterBlock_Rows_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant ComObjectParameterBlock_Rows_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "Rows";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ComObjectParameterBlock_Rows");
    }
    return QVariant();
}

ComObjectParameterBlock_Rows_t * make_ComObjectParameterBlock_Rows_t(Base * parent)
{
    return new ComObjectParameterBlock_Rows_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
