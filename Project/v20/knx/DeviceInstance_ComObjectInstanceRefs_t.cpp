/* This file is generated. */

#include <Project/v20/knx/DeviceInstance_ComObjectInstanceRefs_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/KNX_t.h>

namespace Project {
namespace v20 {
namespace knx {

DeviceInstance_ComObjectInstanceRefs_t::DeviceInstance_ComObjectInstanceRefs_t(Base * parent) :
    Base(parent)
{
}

DeviceInstance_ComObjectInstanceRefs_t::~DeviceInstance_ComObjectInstanceRefs_t()
{
}

void DeviceInstance_ComObjectInstanceRefs_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("ComObjectInstanceRef")) {
            QString newRefId = reader.attributes().value("RefId").toString();
            Q_ASSERT(!newRefId.isEmpty());
            ComObjectInstanceRef_t * newComObjectInstanceRef;
            if (ComObjectInstanceRef.contains(newRefId)) {
                newComObjectInstanceRef = ComObjectInstanceRef[newRefId];
            } else {
                newComObjectInstanceRef = make_ComObjectInstanceRef_t(this);
                ComObjectInstanceRef[newRefId] = newComObjectInstanceRef;
            }
            newComObjectInstanceRef->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int DeviceInstance_ComObjectInstanceRefs_t::tableColumnCount() const
{
    return 1;
}

QVariant DeviceInstance_ComObjectInstanceRefs_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "DeviceInstance_ComObjectInstanceRefs";
        }
        break;
    }
    return QVariant();
}

QVariant DeviceInstance_ComObjectInstanceRefs_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant DeviceInstance_ComObjectInstanceRefs_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "ComObjectInstanceRefs";
    case Qt::DecorationRole:
        return QIcon::fromTheme("DeviceInstance_ComObjectInstanceRefs");
    }
    return QVariant();
}

DeviceInstance_ComObjectInstanceRefs_t * make_DeviceInstance_ComObjectInstanceRefs_t(Base * parent)
{
    return new DeviceInstance_ComObjectInstanceRefs_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
