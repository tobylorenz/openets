/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v20/knx/FunctionType_t.h>
#include <Project/v20/knx/FunctionsGroup_t.h>

namespace Project {
namespace v20 {
namespace knx {

class MasterData_FunctionTypes_t : public Base
{
    Q_OBJECT

public:
    explicit MasterData_FunctionTypes_t(Base * parent = nullptr);
    virtual ~MasterData_FunctionTypes_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* elements */
    QMap<xs::ID, FunctionsGroup_t *> FunctionsGroup; // key: Id
    QMap<xs::ID, FunctionType_t *> FunctionType; // key: Id
};

MasterData_FunctionTypes_t * make_MasterData_FunctionTypes_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
