/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v20/knx/Condition_t.h>
#include <Project/xs/Boolean.h>
#include <Project/xs/String.h>

namespace Project {
namespace v20 {
namespace knx {

class When_t : public Base
{
    Q_OBJECT

public:
    explicit When_t(Base * parent = nullptr);
    virtual ~When_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    Condition_t Test{};
    xs::Boolean Default{"false"};
    xs::String InternalDescription{};
};

When_t * make_When_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
