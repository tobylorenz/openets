/* This file is generated. */

#include <Project/v20/knx/MasterData_Manufacturers_Manufacturer_DatapointRoles_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/KNX_t.h>

namespace Project {
namespace v20 {
namespace knx {

MasterData_Manufacturers_Manufacturer_DatapointRoles_t::MasterData_Manufacturers_Manufacturer_DatapointRoles_t(Base * parent) :
    Base(parent)
{
}

MasterData_Manufacturers_Manufacturer_DatapointRoles_t::~MasterData_Manufacturers_Manufacturer_DatapointRoles_t()
{
}

void MasterData_Manufacturers_Manufacturer_DatapointRoles_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("DatapointRole")) {
            QString newId = reader.attributes().value("Id").toString();
            Q_ASSERT(!newId.isEmpty());
            DatapointRole_t * newDatapointRole;
            if (DatapointRole.contains(newId)) {
                newDatapointRole = DatapointRole[newId];
            } else {
                newDatapointRole = make_DatapointRole_t(this);
                DatapointRole[newId] = newDatapointRole;
            }
            newDatapointRole->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int MasterData_Manufacturers_Manufacturer_DatapointRoles_t::tableColumnCount() const
{
    return 1;
}

QVariant MasterData_Manufacturers_Manufacturer_DatapointRoles_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "MasterData_Manufacturers_Manufacturer_DatapointRoles";
        }
        break;
    }
    return QVariant();
}

QVariant MasterData_Manufacturers_Manufacturer_DatapointRoles_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant MasterData_Manufacturers_Manufacturer_DatapointRoles_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "DatapointRoles";
    case Qt::DecorationRole:
        return QIcon::fromTheme("MasterData_Manufacturers_Manufacturer_DatapointRoles");
    }
    return QVariant();
}

MasterData_Manufacturers_Manufacturer_DatapointRoles_t * make_MasterData_Manufacturers_Manufacturer_DatapointRoles_t(Base * parent)
{
    return new MasterData_Manufacturers_Manufacturer_DatapointRoles_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
