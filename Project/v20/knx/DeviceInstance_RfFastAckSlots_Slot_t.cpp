/* This file is generated. */

#include <Project/v20/knx/DeviceInstance_RfFastAckSlots_Slot_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/KNX_t.h>

namespace Project {
namespace v20 {
namespace knx {

DeviceInstance_RfFastAckSlots_Slot_t::DeviceInstance_RfFastAckSlots_Slot_t(Base * parent) :
    Base(parent)
{
}

DeviceInstance_RfFastAckSlots_Slot_t::~DeviceInstance_RfFastAckSlots_Slot_t()
{
}

void DeviceInstance_RfFastAckSlots_Slot_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("GroupAddressRefId")) {
            QString value = attribute.value().toString();
            if (!value.isEmpty())
                qWarning() << "knx:DeviceInstance_RfFastAckSlots_Slot_t attribute GroupAddressRefId references to" << value;
        }
        if (name == QString("GroupAddressRefId")) {
            GroupAddressRefId = attribute.value().toString();
            continue;
        }
        if (name == QString("Number")) {
            Number = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int DeviceInstance_RfFastAckSlots_Slot_t::tableColumnCount() const
{
    return 3;
}

QVariant DeviceInstance_RfFastAckSlots_Slot_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "DeviceInstance_RfFastAckSlots_Slot";
        }
        if (qualifiedName == QString("GroupAddressRefId")) {
            return GroupAddressRefId;
        }
        if (qualifiedName == QString("Number")) {
            return Number;
        }
        break;
    }
    return QVariant();
}

QVariant DeviceInstance_RfFastAckSlots_Slot_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "GroupAddressRefId";
            case 2:
                return "Number";
            }
        }
    }
    return QVariant();
}

QVariant DeviceInstance_RfFastAckSlots_Slot_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "Slot";
    case Qt::DecorationRole:
        return QIcon::fromTheme("DeviceInstance_RfFastAckSlots_Slot");
    }
    return QVariant();
}

Base * DeviceInstance_RfFastAckSlots_Slot_t::getGroupAddress() const
{
    if (GroupAddressRefId.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<Base *>(knx->ids[GroupAddressRefId]);
}

DeviceInstance_RfFastAckSlots_Slot_t * make_DeviceInstance_RfFastAckSlots_Slot_t(Base * parent)
{
    return new DeviceInstance_RfFastAckSlots_Slot_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
