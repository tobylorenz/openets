/* This file is generated. */

#pragma once

#include <Project/xs/UnsignedInt.h>

namespace Project {
namespace v20 {
namespace knx {

using ModuleDefStatic_Parameters_Union_SizeInBit_t = xs::UnsignedInt;

} // namespace knx
} // namespace v20
} // namespace Project
