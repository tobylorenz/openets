/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v20/knx/IDREF.h>
#include <Project/xs/Long.h>

namespace Project {
namespace v20 {
namespace knx {

class Module_NumericArg_t : public Base
{
    Q_OBJECT

public:
    explicit Module_NumericArg_t(Base * parent = nullptr);
    virtual ~Module_NumericArg_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    IDREF RefId{};
    xs::Long Value{};
    IDREF AllocatorRefId{};
    IDREF BaseValue{};

    /* getters */
    // Base * getTODO() const; // attribute: RefId
    Base * getAllocator() const; // attribute: AllocatorRefId
    Base * getBaseValue() const; // attribute: BaseValue
};

Module_NumericArg_t * make_Module_NumericArg_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
