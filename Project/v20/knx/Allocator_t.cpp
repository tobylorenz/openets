/* This file is generated. */

#include <Project/v20/knx/Allocator_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/KNX_t.h>

namespace Project {
namespace v20 {
namespace knx {

Allocator_t::Allocator_t(Base * parent) :
    Base(parent)
{
}

Allocator_t::~Allocator_t()
{
}

void Allocator_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == "Id") {
            Id = attribute.value().toString();
            KNX_t * knx = findParent<KNX_t *>();
            Q_ASSERT(knx);
            knx->ids[Id] = this;
            continue;
        }
        if (name == QString("Name")) {
            Name = attribute.value().toString();
            continue;
        }
        if (name == QString("InternalDescription")) {
            InternalDescription = attribute.value().toString();
            continue;
        }
        if (name == QString("Start")) {
            Start = attribute.value().toString();
            continue;
        }
        if (name == QString("maxInclusive")) {
            maxInclusive = attribute.value().toString();
            continue;
        }
        if (name == QString("ErrorMessageRef")) {
            QString value = attribute.value().toString();
            if (!value.isEmpty())
                qWarning() << "knx:Allocator_t attribute ErrorMessageRef references to" << value;
        }
        if (name == QString("ErrorMessageRef")) {
            ErrorMessageRef = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int Allocator_t::tableColumnCount() const
{
    return 7;
}

QVariant Allocator_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "Allocator";
        }
        if (qualifiedName == QString("Id")) {
            return Id;
        }
        if (qualifiedName == QString("Name")) {
            return Name;
        }
        if (qualifiedName == QString("InternalDescription")) {
            return InternalDescription;
        }
        if (qualifiedName == QString("Start")) {
            return Start;
        }
        if (qualifiedName == QString("maxInclusive")) {
            return maxInclusive;
        }
        if (qualifiedName == QString("ErrorMessageRef")) {
            return ErrorMessageRef;
        }
        break;
    }
    return QVariant();
}

QVariant Allocator_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Id";
            case 2:
                return "Name";
            case 3:
                return "InternalDescription";
            case 4:
                return "Start";
            case 5:
                return "maxInclusive";
            case 6:
                return "ErrorMessageRef";
            }
        }
    }
    return QVariant();
}

QVariant Allocator_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return !Name.isEmpty() ? Name : "Allocator";
    case Qt::DecorationRole:
        return QIcon::fromTheme("Allocator");
    }
    return QVariant();
}

Base * Allocator_t::getErrorMessage() const
{
    if (ErrorMessageRef.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<Base *>(knx->ids[ErrorMessageRef]);
}

Allocator_t * make_Allocator_t(Base * parent)
{
    return new Allocator_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
