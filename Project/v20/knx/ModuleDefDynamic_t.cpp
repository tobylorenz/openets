/* This file is generated. */

#include <Project/v20/knx/ModuleDefDynamic_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/ApplicationProgramChannel_t.h>
#include <Project/v20/knx/ChannelIndependentBlock_t.h>
#include <Project/v20/knx/ComObjectParameterBlock_t.h>
#include <Project/v20/knx/DependentChannelChoose_t.h>
#include <Project/v20/knx/KNX_t.h>
#include <Project/v20/knx/Module_t.h>
#include <Project/v20/knx/Repeat_t.h>

namespace Project {
namespace v20 {
namespace knx {

ModuleDefDynamic_t::ModuleDefDynamic_t(Base * parent) :
    Base(parent)
{
}

ModuleDefDynamic_t::~ModuleDefDynamic_t()
{
}

void ModuleDefDynamic_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("ChannelIndependentBlock")) {
            auto * ChannelIndependentBlock = make_ChannelIndependentBlock_t(this);
            ChannelIndependentBlock->read(reader);
            continue;
        }
        if (reader.name() == QString("Channel")) {
            auto * Channel = make_ApplicationProgramChannel_t(this);
            Channel->read(reader);
            continue;
        }
        if (reader.name() == QString("choose")) {
            auto * Choose = make_DependentChannelChoose_t(this);
            Choose->read(reader);
            continue;
        }
        if (reader.name() == QString("Module")) {
            auto * Module = make_Module_t(this);
            Module->read(reader);
            continue;
        }
        if (reader.name() == QString("Repeat")) {
            auto * Repeat = make_Repeat_t(this);
            Repeat->read(reader);
            continue;
        }
        if (reader.name() == QString("ParameterBlock")) {
            auto * ParameterBlock = make_ComObjectParameterBlock_t(this);
            ParameterBlock->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ModuleDefDynamic_t::tableColumnCount() const
{
    return 1;
}

QVariant ModuleDefDynamic_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ModuleDefDynamic";
        }
        break;
    }
    return QVariant();
}

QVariant ModuleDefDynamic_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant ModuleDefDynamic_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "ModuleDefDynamic";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ModuleDefDynamic");
    }
    return QVariant();
}

ModuleDefDynamic_t * make_ModuleDefDynamic_t(Base * parent)
{
    return new ModuleDefDynamic_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
