/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v20/knx/LdCtrlBase_OnError_t.h>
#include <Project/v20/knx/LdCtrlCompareBase_Range_t.h>
#include <Project/v20/knx/LdCtrlProcType_t.h>
#include <Project/xs/Boolean.h>
#include <Project/xs/HexBinary.h>
#include <Project/xs/String.h>
#include <Project/xs/UnsignedShort.h>

namespace Project {
namespace v20 {
namespace knx {

class LdCtrlCompareBase_t : public Base
{
    Q_OBJECT

public:
    explicit LdCtrlCompareBase_t(Base * parent = nullptr);
    virtual ~LdCtrlCompareBase_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    LdCtrlProcType_t AppliesTo{"auto"};
    xs::String InternalDescription{};
    xs::Boolean AllowCachedValue{"false"};
    xs::HexBinary InlineData{};
    xs::HexBinary Mask{};
    LdCtrlCompareBase_Range_t Range{};
    xs::Boolean Invert{"false"};
    xs::UnsignedShort RetryInterval{"0"};
    xs::UnsignedShort TimeOut{"0"};

    /* elements */
    QVector<LdCtrlBase_OnError_t *> OnError;
};

LdCtrlCompareBase_t * make_LdCtrlCompareBase_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
