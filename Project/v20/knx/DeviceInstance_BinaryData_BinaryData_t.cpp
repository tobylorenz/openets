/* This file is generated. */

#include <Project/v20/knx/DeviceInstance_BinaryData_BinaryData_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/KNX_t.h>

namespace Project {
namespace v20 {
namespace knx {

DeviceInstance_BinaryData_BinaryData_t::DeviceInstance_BinaryData_BinaryData_t(Base * parent) :
    Base(parent)
{
}

DeviceInstance_BinaryData_BinaryData_t::~DeviceInstance_BinaryData_BinaryData_t()
{
}

void DeviceInstance_BinaryData_BinaryData_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == "Id") {
            Id = attribute.value().toString();
            KNX_t * knx = findParent<KNX_t *>();
            Q_ASSERT(knx);
            knx->ids[Id] = this;
            continue;
        }
        if (name == QString("RefId")) {
            RefId = attribute.value().toString();
            continue;
        }
        if (name == QString("Name")) {
            Name = attribute.value().toString();
            continue;
        }
        if (name == QString("AutoCopy")) {
            AutoCopy = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("Data")) {
            if (!Data) {
                Data = make_SimpleElementTextType("Data", this);
            }
            Data->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int DeviceInstance_BinaryData_BinaryData_t::tableColumnCount() const
{
    return 5;
}

QVariant DeviceInstance_BinaryData_BinaryData_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "DeviceInstance_BinaryData_BinaryData";
        }
        if (qualifiedName == QString("Id")) {
            return Id;
        }
        if (qualifiedName == QString("RefId")) {
            return RefId;
        }
        if (qualifiedName == QString("Name")) {
            return Name;
        }
        if (qualifiedName == QString("AutoCopy")) {
            return AutoCopy;
        }
        break;
    }
    return QVariant();
}

QVariant DeviceInstance_BinaryData_BinaryData_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Id";
            case 2:
                return "RefId";
            case 3:
                return "Name";
            case 4:
                return "AutoCopy";
            }
        }
    }
    return QVariant();
}

QVariant DeviceInstance_BinaryData_BinaryData_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return !Name.isEmpty() ? Name : "BinaryData";
    case Qt::DecorationRole:
        return QIcon::fromTheme("DeviceInstance_BinaryData_BinaryData");
    }
    return QVariant();
}

Base * DeviceInstance_BinaryData_BinaryData_t::getBinaryData() const
{
    if (RefId.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<Base *>(knx->ids[RefId]);
}

DeviceInstance_BinaryData_BinaryData_t * make_DeviceInstance_BinaryData_BinaryData_t(Base * parent)
{
    return new DeviceInstance_BinaryData_BinaryData_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
