/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v20/knx/IDREF.h>
#include <Project/v20/knx/String255_t.h>
#include <Project/xs/ID.h>
#include <Project/xs/String.h>

namespace Project {
namespace v20 {
namespace knx {

/* forward declarations */
class Module_NumericArg_t;
class Module_TextArg_t;

class Module_t : public Base
{
    Q_OBJECT

public:
    explicit Module_t(Base * parent = nullptr);
    virtual ~Module_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::ID Id{};
    IDREF RefId{};
    String255_t Name{};
    xs::String InternalDescription{};

    /* elements */
    // xs:choice Module_NumericArg_t * NumericArg{};
    // xs:choice Module_TextArg_t * TextArg{};

    /* getters */
    // Base * getTODO() const; // attribute: RefId
};

Module_t * make_Module_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
