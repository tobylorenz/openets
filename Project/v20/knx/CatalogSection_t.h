/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v20/knx/CatalogSection_CatalogItem_t.h>
#include <Project/v20/knx/CatalogSection_t.h>
#include <Project/v20/knx/LanguageDependentString255_t.h>
#include <Project/v20/knx/LanguageDependentString_t.h>
#include <Project/v20/knx/String20_t.h>
#include <Project/xs/ID.h>
#include <Project/xs/Language.h>
#include <Project/xs/String.h>
#include <Project/xs/UnsignedShort.h>

namespace Project {
namespace v20 {
namespace knx {

class CatalogSection_t : public Base
{
    Q_OBJECT

public:
    explicit CatalogSection_t(Base * parent = nullptr);
    virtual ~CatalogSection_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::ID Id{};
    LanguageDependentString255_t Name{};
    String20_t Number{};
    LanguageDependentString_t VisibleDescription{};
    xs::Language DefaultLanguage{};
    xs::UnsignedShort NonRegRelevantDataVersion{"0"};
    xs::String InternalDescription{};

    /* elements */
    QMap<xs::ID, CatalogSection_t *> CatalogSection; // key: Id
    QMap<xs::ID, CatalogSection_CatalogItem_t *> CatalogItem; // key: Id
};

CatalogSection_t * make_CatalogSection_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
