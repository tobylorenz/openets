/* This file is generated. */

#pragma once

#include <Project/v20/knx/RELIDREF.h>
#include <Project/xs/List.h>

namespace Project {
namespace v20 {
namespace knx {

using RELIDREFS = xs::List<RELIDREF>;

} // namespace knx
} // namespace v20
} // namespace Project
