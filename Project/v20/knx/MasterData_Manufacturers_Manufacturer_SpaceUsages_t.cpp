/* This file is generated. */

#include <Project/v20/knx/MasterData_Manufacturers_Manufacturer_SpaceUsages_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/KNX_t.h>

namespace Project {
namespace v20 {
namespace knx {

MasterData_Manufacturers_Manufacturer_SpaceUsages_t::MasterData_Manufacturers_Manufacturer_SpaceUsages_t(Base * parent) :
    Base(parent)
{
}

MasterData_Manufacturers_Manufacturer_SpaceUsages_t::~MasterData_Manufacturers_Manufacturer_SpaceUsages_t()
{
}

void MasterData_Manufacturers_Manufacturer_SpaceUsages_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("SpaceUsage")) {
            QString newId = reader.attributes().value("Id").toString();
            Q_ASSERT(!newId.isEmpty());
            SpaceUsage_t * newSpaceUsage;
            if (SpaceUsage.contains(newId)) {
                newSpaceUsage = SpaceUsage[newId];
            } else {
                newSpaceUsage = make_SpaceUsage_t(this);
                SpaceUsage[newId] = newSpaceUsage;
            }
            newSpaceUsage->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int MasterData_Manufacturers_Manufacturer_SpaceUsages_t::tableColumnCount() const
{
    return 1;
}

QVariant MasterData_Manufacturers_Manufacturer_SpaceUsages_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "MasterData_Manufacturers_Manufacturer_SpaceUsages";
        }
        break;
    }
    return QVariant();
}

QVariant MasterData_Manufacturers_Manufacturer_SpaceUsages_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant MasterData_Manufacturers_Manufacturer_SpaceUsages_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "SpaceUsages";
    case Qt::DecorationRole:
        return QIcon::fromTheme("MasterData_Manufacturers_Manufacturer_SpaceUsages");
    }
    return QVariant();
}

MasterData_Manufacturers_Manufacturer_SpaceUsages_t * make_MasterData_Manufacturers_Manufacturer_SpaceUsages_t(Base * parent)
{
    return new MasterData_Manufacturers_Manufacturer_SpaceUsages_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
