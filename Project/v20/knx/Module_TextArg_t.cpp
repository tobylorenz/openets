/* This file is generated. */

#include <Project/v20/knx/Module_TextArg_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/KNX_t.h>

namespace Project {
namespace v20 {
namespace knx {

Module_TextArg_t::Module_TextArg_t(Base * parent) :
    Base(parent)
{
}

Module_TextArg_t::~Module_TextArg_t()
{
}

void Module_TextArg_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("RefId")) {
            RefId = attribute.value().toString();
            continue;
        }
        if (name == "Id") {
            Id = attribute.value().toString();
            KNX_t * knx = findParent<KNX_t *>();
            Q_ASSERT(knx);
            knx->ids[Id] = this;
            continue;
        }
        if (name == QString("Value")) {
            Value = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int Module_TextArg_t::tableColumnCount() const
{
    return 4;
}

QVariant Module_TextArg_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "Module_TextArg";
        }
        if (qualifiedName == QString("RefId")) {
            return RefId;
        }
        if (qualifiedName == QString("Id")) {
            return Id;
        }
        if (qualifiedName == QString("Value")) {
            return Value;
        }
        break;
    }
    return QVariant();
}

QVariant Module_TextArg_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "RefId";
            case 2:
                return "Id";
            case 3:
                return "Value";
            }
        }
    }
    return QVariant();
}

QVariant Module_TextArg_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "TextArg";
    case Qt::DecorationRole:
        return QIcon::fromTheme("Module_TextArg");
    }
    return QVariant();
}

Module_TextArg_t * make_Module_TextArg_t(Base * parent)
{
    return new Module_TextArg_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
