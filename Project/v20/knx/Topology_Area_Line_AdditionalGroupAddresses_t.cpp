/* This file is generated. */

#include <Project/v20/knx/Topology_Area_Line_AdditionalGroupAddresses_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/KNX_t.h>
#include <Project/v20/knx/Topology_Area_Line_AdditionalGroupAddresses_GroupAddress_t.h>

namespace Project {
namespace v20 {
namespace knx {

Topology_Area_Line_AdditionalGroupAddresses_t::Topology_Area_Line_AdditionalGroupAddresses_t(Base * parent) :
    Base(parent)
{
}

Topology_Area_Line_AdditionalGroupAddresses_t::~Topology_Area_Line_AdditionalGroupAddresses_t()
{
}

void Topology_Area_Line_AdditionalGroupAddresses_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("GroupAddress")) {
            if (!GroupAddress) {
                GroupAddress = make_Topology_Area_Line_AdditionalGroupAddresses_GroupAddress_t(this);
            }
            GroupAddress->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int Topology_Area_Line_AdditionalGroupAddresses_t::tableColumnCount() const
{
    return 1;
}

QVariant Topology_Area_Line_AdditionalGroupAddresses_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "Topology_Area_Line_AdditionalGroupAddresses";
        }
        break;
    }
    return QVariant();
}

QVariant Topology_Area_Line_AdditionalGroupAddresses_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant Topology_Area_Line_AdditionalGroupAddresses_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "AdditionalGroupAddresses";
    case Qt::DecorationRole:
        return QIcon::fromTheme("Topology_Area_Line_AdditionalGroupAddresses");
    }
    return QVariant();
}

Topology_Area_Line_AdditionalGroupAddresses_t * make_Topology_Area_Line_AdditionalGroupAddresses_t(Base * parent)
{
    return new Topology_Area_Line_AdditionalGroupAddresses_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
