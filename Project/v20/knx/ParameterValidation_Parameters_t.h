/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v20/knx/CalculationParameterRef_t.h>

namespace Project {
namespace v20 {
namespace knx {

class ParameterValidation_Parameters_t : public Base
{
    Q_OBJECT

public:
    explicit ParameterValidation_Parameters_t(Base * parent = nullptr);
    virtual ~ParameterValidation_Parameters_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* elements */
    QMap<IDREF, CalculationParameterRef_t *> ParameterRefRef; // key: RefId
};

ParameterValidation_Parameters_t * make_ParameterValidation_Parameters_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
