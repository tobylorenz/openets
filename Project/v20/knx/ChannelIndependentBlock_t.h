/* This file is generated. */

#pragma once

#include <Project/Base.h>

namespace Project {
namespace v20 {
namespace knx {

/* forward declarations */
class BinaryDataRef_t;
class ChannelChoose_t;
class ComObjectParameterBlock_t;
class ComObjectRefRef_t;
class Module_t;
class Repeat_t;

class ChannelIndependentBlock_t : public Base
{
    Q_OBJECT

public:
    explicit ChannelIndependentBlock_t(Base * parent = nullptr);
    virtual ~ChannelIndependentBlock_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* elements */
    // xs:choice ComObjectParameterBlock_t * ParameterBlock{};
    // xs:choice ChannelChoose_t * Choose{};
    // xs:choice BinaryDataRef_t * BinaryDataRef{};
    // xs:choice ComObjectRefRef_t * ComObjectRefRef{};
    // xs:choice Module_t * Module{};
    // xs:choice Repeat_t * Repeat{};
};

ChannelIndependentBlock_t * make_ChannelIndependentBlock_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
