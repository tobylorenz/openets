/* This file is generated. */

#pragma once

#include <Project/Base.h>

namespace Project {
namespace v20 {
namespace knx {

/* forward declarations */
class ModuleDefLoadProcedures_t;
class ModuleDefStatic_Allocators_t;
class ModuleDefStatic_ComObjectRefs_t;
class ModuleDefStatic_ComObjects_t;
class ModuleDefStatic_ParameterCalculations_t;
class ModuleDefStatic_ParameterRefs_t;
class ModuleDefStatic_ParameterValidations_t;
class ModuleDefStatic_Parameters_t;

class ModuleDefStatic_t : public Base
{
    Q_OBJECT

public:
    explicit ModuleDefStatic_t(Base * parent = nullptr);
    virtual ~ModuleDefStatic_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* elements */
    ModuleDefStatic_Parameters_t * Parameters{};
    ModuleDefStatic_ParameterRefs_t * ParameterRefs{};
    ModuleDefStatic_ParameterCalculations_t * ParameterCalculations{};
    ModuleDefStatic_ParameterValidations_t * ParameterValidations{};
    ModuleDefStatic_ComObjects_t * ComObjects{};
    ModuleDefStatic_ComObjectRefs_t * ComObjectRefs{};
    ModuleDefLoadProcedures_t * LoadProcedures{};
    ModuleDefStatic_Allocators_t * Allocators{};
};

ModuleDefStatic_t * make_ModuleDefStatic_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
