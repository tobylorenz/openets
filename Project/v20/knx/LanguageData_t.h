/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v20/knx/IDREF.h>
#include <Project/v20/knx/LanguageData_TranslationUnit_t.h>
#include <Project/xs/Language.h>

namespace Project {
namespace v20 {
namespace knx {

class LanguageData_t : public Base
{
    Q_OBJECT

public:
    explicit LanguageData_t(Base * parent = nullptr);
    virtual ~LanguageData_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::Language Identifier{};

    /* elements */
    QMap<IDREF, LanguageData_TranslationUnit_t *> TranslationUnit; // key: RefId
};

LanguageData_t * make_LanguageData_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
