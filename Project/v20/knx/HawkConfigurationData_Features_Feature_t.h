/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v20/knx/HawkConfigurationData_Features_Feature_Name_t.h>
#include <Project/xs/Int.h>

namespace Project {
namespace v20 {
namespace knx {

class HawkConfigurationData_Features_Feature_t : public Base
{
    Q_OBJECT

public:
    explicit HawkConfigurationData_Features_Feature_t(Base * parent = nullptr);
    virtual ~HawkConfigurationData_Features_Feature_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    HawkConfigurationData_Features_Feature_Name_t Name{};
    xs::Int Value{};
};

HawkConfigurationData_Features_Feature_t * make_HawkConfigurationData_Features_Feature_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
