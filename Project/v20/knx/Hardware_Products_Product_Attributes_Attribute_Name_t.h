/* This file is generated. */

#pragma once

#include <Project/xs/String.h>

namespace Project {
namespace v20 {
namespace knx {

using Hardware_Products_Product_Attributes_Attribute_Name_t = xs::String;

} // namespace knx
} // namespace v20
} // namespace Project
