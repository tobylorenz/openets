/* This file is generated. */

#include <Project/v20/knx/LdCtrlClearLCFilterTable_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/KNX_t.h>

namespace Project {
namespace v20 {
namespace knx {

LdCtrlClearLCFilterTable_t::LdCtrlClearLCFilterTable_t(Base * parent) :
    Base(parent)
{
}

LdCtrlClearLCFilterTable_t::~LdCtrlClearLCFilterTable_t()
{
}

void LdCtrlClearLCFilterTable_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("AppliesTo")) {
            AppliesTo = attribute.value().toString();
            continue;
        }
        if (name == QString("InternalDescription")) {
            InternalDescription = attribute.value().toString();
            continue;
        }
        if (name == QString("UseFunctionProp")) {
            UseFunctionProp = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("OnError")) {
            auto * newOnError = make_LdCtrlBase_OnError_t(this);
            newOnError->read(reader);
            OnError.append(newOnError);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int LdCtrlClearLCFilterTable_t::tableColumnCount() const
{
    return 4;
}

QVariant LdCtrlClearLCFilterTable_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "LdCtrlClearLCFilterTable";
        }
        if (qualifiedName == QString("AppliesTo")) {
            return AppliesTo;
        }
        if (qualifiedName == QString("InternalDescription")) {
            return InternalDescription;
        }
        if (qualifiedName == QString("UseFunctionProp")) {
            return UseFunctionProp;
        }
        break;
    }
    return QVariant();
}

QVariant LdCtrlClearLCFilterTable_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "AppliesTo";
            case 2:
                return "InternalDescription";
            case 3:
                return "UseFunctionProp";
            }
        }
    }
    return QVariant();
}

QVariant LdCtrlClearLCFilterTable_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "LdCtrlClearLCFilterTable";
    case Qt::DecorationRole:
        return QIcon::fromTheme("LdCtrlClearLCFilterTable");
    }
    return QVariant();
}

LdCtrlClearLCFilterTable_t * make_LdCtrlClearLCFilterTable_t(Base * parent)
{
    return new LdCtrlClearLCFilterTable_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
