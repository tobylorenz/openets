/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v20/knx/MasterData_InterfaceObjectTypes_InterfaceObjectType_t.h>
#include <Project/xs/ID.h>

namespace Project {
namespace v20 {
namespace knx {

class MasterData_InterfaceObjectTypes_t : public Base
{
    Q_OBJECT

public:
    explicit MasterData_InterfaceObjectTypes_t(Base * parent = nullptr);
    virtual ~MasterData_InterfaceObjectTypes_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* elements */
    QMap<xs::ID, MasterData_InterfaceObjectTypes_InterfaceObjectType_t *> InterfaceObjectType; // key: Id
};

MasterData_InterfaceObjectTypes_t * make_MasterData_InterfaceObjectTypes_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
