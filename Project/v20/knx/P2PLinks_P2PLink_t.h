/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v20/knx/Aes128Key_t.h>
#include <Project/v20/knx/String255_t.h>
#include <Project/xs/ID.h>
#include <Project/xs/Int.h>
#include <Project/xs/String.h>

namespace Project {
namespace v20 {
namespace knx {

/* forward declarations */
class P2PLinkBusInterfaceEndpoint_t;
class P2PLinkDeviceEndpoint_t;

class P2PLinks_P2PLink_t : public Base
{
    Q_OBJECT

public:
    explicit P2PLinks_P2PLink_t(Base * parent = nullptr);
    virtual ~P2PLinks_P2PLink_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::ID Id{};
    String255_t Name{};
    xs::String Description{};
    xs::String Comment{};
    xs::Int Puid{};
    Aes128Key_t Key{};

    /* elements */
    // xs:choice P2PLinkDeviceEndpoint_t * DeviceEndpoint{};
    // xs:choice P2PLinkBusInterfaceEndpoint_t * BusInterfaceEndpoint{};
};

P2PLinks_P2PLink_t * make_P2PLinks_P2PLink_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
