/* This file is generated. */

#pragma once

#include <Project/xs/NCName.h>

namespace Project {
namespace v20 {
namespace knx {

using RELIDREF = xs::NCName;

} // namespace knx
} // namespace v20
} // namespace Project
