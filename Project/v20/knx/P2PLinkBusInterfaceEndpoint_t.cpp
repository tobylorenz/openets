/* This file is generated. */

#include <Project/v20/knx/P2PLinkBusInterfaceEndpoint_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/KNX_t.h>

namespace Project {
namespace v20 {
namespace knx {

P2PLinkBusInterfaceEndpoint_t::P2PLinkBusInterfaceEndpoint_t(Base * parent) :
    Base(parent)
{
}

P2PLinkBusInterfaceEndpoint_t::~P2PLinkBusInterfaceEndpoint_t()
{
}

void P2PLinkBusInterfaceEndpoint_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("DeviceRefId")) {
            QString value = attribute.value().toString();
            if (!value.isEmpty())
                qWarning() << "knx:P2PLinkBusInterfaceEndpoint_t attribute DeviceRefId references to" << value;
        }
        if (name == QString("DeviceRefId")) {
            DeviceRefId = attribute.value().toString();
            continue;
        }
        if (name == QString("BusInterfaceRefId")) {
            QString value = attribute.value().toString();
            if (!value.isEmpty())
                qWarning() << "knx:P2PLinkBusInterfaceEndpoint_t attribute BusInterfaceRefId references to" << value;
        }
        if (name == QString("BusInterfaceRefId")) {
            BusInterfaceRefId = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int P2PLinkBusInterfaceEndpoint_t::tableColumnCount() const
{
    return 3;
}

QVariant P2PLinkBusInterfaceEndpoint_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "P2PLinkBusInterfaceEndpoint";
        }
        if (qualifiedName == QString("DeviceRefId")) {
            return DeviceRefId;
        }
        if (qualifiedName == QString("BusInterfaceRefId")) {
            return BusInterfaceRefId;
        }
        break;
    }
    return QVariant();
}

QVariant P2PLinkBusInterfaceEndpoint_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "DeviceRefId";
            case 2:
                return "BusInterfaceRefId";
            }
        }
    }
    return QVariant();
}

QVariant P2PLinkBusInterfaceEndpoint_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "P2PLinkBusInterfaceEndpoint";
    case Qt::DecorationRole:
        return QIcon::fromTheme("P2PLinkBusInterfaceEndpoint");
    }
    return QVariant();
}

Base * P2PLinkBusInterfaceEndpoint_t::getDevice() const
{
    if (DeviceRefId.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<Base *>(knx->ids[DeviceRefId]);
}

Base * P2PLinkBusInterfaceEndpoint_t::getBusInterface() const
{
    if (BusInterfaceRefId.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<Base *>(knx->ids[BusInterfaceRefId]);
}

P2PLinkBusInterfaceEndpoint_t * make_P2PLinkBusInterfaceEndpoint_t(Base * parent)
{
    return new P2PLinkBusInterfaceEndpoint_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
