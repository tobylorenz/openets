/* This file is generated. */

#include <Project/v20/knx/MemoryUnion_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/ApplicationProgramStatic_Code_AbsoluteSegment_t.h>
#include <Project/v20/knx/KNX_t.h>

namespace Project {
namespace v20 {
namespace knx {

MemoryUnion_t::MemoryUnion_t(Base * parent) :
    Base(parent)
{
}

MemoryUnion_t::~MemoryUnion_t()
{
}

void MemoryUnion_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("CodeSegment")) {
            CodeSegment = attribute.value().toString();
            continue;
        }
        if (name == QString("Offset")) {
            Offset = attribute.value().toString();
            continue;
        }
        if (name == QString("BitOffset")) {
            BitOffset = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int MemoryUnion_t::tableColumnCount() const
{
    return 4;
}

QVariant MemoryUnion_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "MemoryUnion";
        }
        if (qualifiedName == QString("CodeSegment")) {
            return CodeSegment;
        }
        if (qualifiedName == QString("Offset")) {
            return Offset;
        }
        if (qualifiedName == QString("BitOffset")) {
            return BitOffset;
        }
        break;
    }
    return QVariant();
}

QVariant MemoryUnion_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "CodeSegment";
            case 2:
                return "Offset";
            case 3:
                return "BitOffset";
            }
        }
    }
    return QVariant();
}

QVariant MemoryUnion_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "MemoryUnion";
    case Qt::DecorationRole:
        return QIcon::fromTheme("MemoryUnion");
    }
    return QVariant();
}

ApplicationProgramStatic_Code_AbsoluteSegment_t * MemoryUnion_t::getCodeSegment() const
{
    if (CodeSegment.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<ApplicationProgramStatic_Code_AbsoluteSegment_t *>(knx->ids[CodeSegment]);
}

MemoryUnion_t * make_MemoryUnion_t(Base * parent)
{
    return new MemoryUnion_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
