/* This file is generated. */

#include <Project/v20/knx/ApplicationProgramRef_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/ApplicationProgram_t.h>
#include <Project/v20/knx/KNX_t.h>

namespace Project {
namespace v20 {
namespace knx {

ApplicationProgramRef_t::ApplicationProgramRef_t(Base * parent) :
    Base(parent)
{
}

ApplicationProgramRef_t::~ApplicationProgramRef_t()
{
}

void ApplicationProgramRef_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("RefId")) {
            RefId = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ApplicationProgramRef_t::tableColumnCount() const
{
    return 2;
}

QVariant ApplicationProgramRef_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ApplicationProgramRef";
        }
        if (qualifiedName == QString("RefId")) {
            return RefId;
        }
        break;
    }
    return QVariant();
}

QVariant ApplicationProgramRef_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "RefId";
            }
        }
    }
    return QVariant();
}

QVariant ApplicationProgramRef_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "ApplicationProgramRef";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ApplicationProgramRef");
    }
    return QVariant();
}

ApplicationProgram_t * ApplicationProgramRef_t::getApplicationProgram() const
{
    if (RefId.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<ApplicationProgram_t *>(knx->ids[RefId]);
}

ApplicationProgramRef_t * make_ApplicationProgramRef_t(Base * parent)
{
    return new ApplicationProgramRef_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
