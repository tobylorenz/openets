/* This file is generated. */

#include <Project/v20/knx/LdCtrlBase_OnError_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/KNX_t.h>

namespace Project {
namespace v20 {
namespace knx {

LdCtrlBase_OnError_t::LdCtrlBase_OnError_t(Base * parent) :
    Base(parent)
{
}

LdCtrlBase_OnError_t::~LdCtrlBase_OnError_t()
{
}

void LdCtrlBase_OnError_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("Cause")) {
            Cause = attribute.value().toString();
            continue;
        }
        if (name == QString("Ignore")) {
            Ignore = attribute.value().toString();
            continue;
        }
        if (name == QString("MessageRef")) {
            QString value = attribute.value().toString();
            if (!value.isEmpty())
                qWarning() << "knx:LdCtrlBase_OnError_t attribute MessageRef references to" << value;
        }
        if (name == QString("MessageRef")) {
            MessageRef = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int LdCtrlBase_OnError_t::tableColumnCount() const
{
    return 4;
}

QVariant LdCtrlBase_OnError_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "LdCtrlBase_OnError";
        }
        if (qualifiedName == QString("Cause")) {
            return Cause;
        }
        if (qualifiedName == QString("Ignore")) {
            return Ignore;
        }
        if (qualifiedName == QString("MessageRef")) {
            return MessageRef;
        }
        break;
    }
    return QVariant();
}

QVariant LdCtrlBase_OnError_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Cause";
            case 2:
                return "Ignore";
            case 3:
                return "MessageRef";
            }
        }
    }
    return QVariant();
}

QVariant LdCtrlBase_OnError_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "OnError";
    case Qt::DecorationRole:
        return QIcon::fromTheme("LdCtrlBase_OnError");
    }
    return QVariant();
}

Base * LdCtrlBase_OnError_t::getMessage() const
{
    if (MessageRef.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<Base *>(knx->ids[MessageRef]);
}

LdCtrlBase_OnError_t * make_LdCtrlBase_OnError_t(Base * parent)
{
    return new LdCtrlBase_OnError_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
