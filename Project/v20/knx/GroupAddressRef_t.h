/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v20/knx/IDREF.h>
#include <Project/v20/knx/String255_t.h>
#include <Project/xs/ID.h>
#include <Project/xs/Int.h>
#include <Project/xs/String.h>

namespace Project {
namespace v20 {
namespace knx {

/* forward declarations */
class GroupAddress_t;

class GroupAddressRef_t : public Base
{
    Q_OBJECT

public:
    explicit GroupAddressRef_t(Base * parent = nullptr);
    virtual ~GroupAddressRef_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::ID Id{};
    String255_t Name{};
    IDREF RefId{};
    String255_t Role{};
    xs::Int Puid{};
    xs::String Context{};

    /* getters */
    GroupAddress_t * getGroupAddress() const; // attribute: RefId
};

GroupAddressRef_t * make_GroupAddressRef_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
