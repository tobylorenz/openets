/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v20/knx/String255_t.h>
#include <Project/xs/ID.h>
#include <Project/xs/String.h>

namespace Project {
namespace v20 {
namespace knx {

/* forward declarations */
class ModuleDefDynamic_t;
class ModuleDefStatic_t;
class ModuleDef_Arguments_t;
class ModuleDef_SubModuleDefs_t;

class ModuleDef_t : public Base
{
    Q_OBJECT

public:
    explicit ModuleDef_t(Base * parent = nullptr);
    virtual ~ModuleDef_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::ID Id{};
    String255_t Name{};
    xs::String InternalDescription{};

    /* elements */
    ModuleDef_Arguments_t * Arguments{};
    ModuleDefStatic_t * Static{};
    ModuleDef_SubModuleDefs_t * SubModuleDefs{};
    ModuleDefDynamic_t * Dynamic{};
};

ModuleDef_t * make_ModuleDef_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
