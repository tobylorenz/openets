/* This file is generated. */

#include <Project/v20/knx/ParameterType_TypeRestriction_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/KNX_t.h>

namespace Project {
namespace v20 {
namespace knx {

ParameterType_TypeRestriction_t::ParameterType_TypeRestriction_t(Base * parent) :
    Base(parent)
{
}

ParameterType_TypeRestriction_t::~ParameterType_TypeRestriction_t()
{
}

void ParameterType_TypeRestriction_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("Base")) {
            Base_ = attribute.value().toString();
            continue;
        }
        if (name == QString("SizeInBit")) {
            SizeInBit = attribute.value().toString();
            continue;
        }
        if (name == QString("UIHint")) {
            UIHint = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("Enumeration")) {
            QString newId = reader.attributes().value("Id").toString();
            Q_ASSERT(!newId.isEmpty());
            ParameterType_TypeRestriction_Enumeration_t * newEnumeration;
            if (Enumeration.contains(newId)) {
                newEnumeration = Enumeration[newId];
            } else {
                newEnumeration = make_ParameterType_TypeRestriction_Enumeration_t(this);
                Enumeration[newId] = newEnumeration;
            }
            newEnumeration->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ParameterType_TypeRestriction_t::tableColumnCount() const
{
    return 4;
}

QVariant ParameterType_TypeRestriction_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ParameterType_TypeRestriction";
        }
        if (qualifiedName == QString("Base")) {
            return Base_;
        }
        if (qualifiedName == QString("SizeInBit")) {
            return SizeInBit;
        }
        if (qualifiedName == QString("UIHint")) {
            return UIHint;
        }
        break;
    }
    return QVariant();
}

QVariant ParameterType_TypeRestriction_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Base_";
            case 2:
                return "SizeInBit";
            case 3:
                return "UIHint";
            }
        }
    }
    return QVariant();
}

QVariant ParameterType_TypeRestriction_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "TypeRestriction";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ParameterType_TypeRestriction");
    }
    return QVariant();
}

ParameterType_TypeRestriction_t * make_ParameterType_TypeRestriction_t(Base * parent)
{
    return new ParameterType_TypeRestriction_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
