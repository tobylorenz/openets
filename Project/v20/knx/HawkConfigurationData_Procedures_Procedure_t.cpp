/* This file is generated. */

#include <Project/v20/knx/HawkConfigurationData_Procedures_Procedure_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/KNX_t.h>
#include <Project/v20/knx/LdCtrlAbsSegment_t.h>
#include <Project/v20/knx/LdCtrlBaseChoose_t.h>
#include <Project/v20/knx/LdCtrlClearCachedObjectTypes_t.h>
#include <Project/v20/knx/LdCtrlClearLCFilterTable_t.h>
#include <Project/v20/knx/LdCtrlCompareMem_t.h>
#include <Project/v20/knx/LdCtrlCompareProp_t.h>
#include <Project/v20/knx/LdCtrlCompareRelMem_t.h>
#include <Project/v20/knx/LdCtrlConnect_t.h>
#include <Project/v20/knx/LdCtrlDeclarePropDesc_t.h>
#include <Project/v20/knx/LdCtrlDelay_t.h>
#include <Project/v20/knx/LdCtrlDisconnect_t.h>
#include <Project/v20/knx/LdCtrlInvokeFunctionProp_t.h>
#include <Project/v20/knx/LdCtrlLoadCompleted_t.h>
#include <Project/v20/knx/LdCtrlLoadImageMem_t.h>
#include <Project/v20/knx/LdCtrlLoadImageProp_t.h>
#include <Project/v20/knx/LdCtrlLoadImageRelMem_t.h>
#include <Project/v20/knx/LdCtrlLoad_t.h>
#include <Project/v20/knx/LdCtrlMapError_t.h>
#include <Project/v20/knx/LdCtrlMasterReset_t.h>
#include <Project/v20/knx/LdCtrlMaxLength_t.h>
#include <Project/v20/knx/LdCtrlMerge_t.h>
#include <Project/v20/knx/LdCtrlProgressText_t.h>
#include <Project/v20/knx/LdCtrlReadFunctionProp_t.h>
#include <Project/v20/knx/LdCtrlRelSegment_t.h>
#include <Project/v20/knx/LdCtrlRestart_t.h>
#include <Project/v20/knx/LdCtrlSetControlVariable_t.h>
#include <Project/v20/knx/LdCtrlTaskCtrl1_t.h>
#include <Project/v20/knx/LdCtrlTaskCtrl2_t.h>
#include <Project/v20/knx/LdCtrlTaskPtr_t.h>
#include <Project/v20/knx/LdCtrlTaskSegment_t.h>
#include <Project/v20/knx/LdCtrlUnload_t.h>
#include <Project/v20/knx/LdCtrlWriteMem_t.h>
#include <Project/v20/knx/LdCtrlWriteProp_t.h>
#include <Project/v20/knx/LdCtrlWriteRelMem_t.h>

namespace Project {
namespace v20 {
namespace knx {

HawkConfigurationData_Procedures_Procedure_t::HawkConfigurationData_Procedures_Procedure_t(Base * parent) :
    Base(parent)
{
}

HawkConfigurationData_Procedures_Procedure_t::~HawkConfigurationData_Procedures_Procedure_t()
{
}

void HawkConfigurationData_Procedures_Procedure_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("ProcedureType")) {
            ProcedureType = attribute.value().toString();
            continue;
        }
        if (name == QString("ProcedureSubType")) {
            ProcedureSubType = attribute.value().toString();
            continue;
        }
        if (name == QString("Access")) {
            Access = attribute.value().toString().split(' ');
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("LdCtrlUnload")) {
            auto * LdCtrlUnload = make_LdCtrlUnload_t(this);
            LdCtrlUnload->read(reader);
            continue;
        }
        if (reader.name() == QString("LdCtrlLoad")) {
            auto * LdCtrlLoad = make_LdCtrlLoad_t(this);
            LdCtrlLoad->read(reader);
            continue;
        }
        if (reader.name() == QString("LdCtrlMaxLength")) {
            auto * LdCtrlMaxLength = make_LdCtrlMaxLength_t(this);
            LdCtrlMaxLength->read(reader);
            continue;
        }
        if (reader.name() == QString("LdCtrlClearCachedObjectTypes")) {
            auto * LdCtrlClearCachedObjectTypes = make_LdCtrlClearCachedObjectTypes_t(this);
            LdCtrlClearCachedObjectTypes->read(reader);
            continue;
        }
        if (reader.name() == QString("LdCtrlLoadCompleted")) {
            auto * LdCtrlLoadCompleted = make_LdCtrlLoadCompleted_t(this);
            LdCtrlLoadCompleted->read(reader);
            continue;
        }
        if (reader.name() == QString("LdCtrlAbsSegment")) {
            auto * LdCtrlAbsSegment = make_LdCtrlAbsSegment_t(this);
            LdCtrlAbsSegment->read(reader);
            continue;
        }
        if (reader.name() == QString("LdCtrlRelSegment")) {
            auto * LdCtrlRelSegment = make_LdCtrlRelSegment_t(this);
            LdCtrlRelSegment->read(reader);
            continue;
        }
        if (reader.name() == QString("LdCtrlTaskSegment")) {
            auto * LdCtrlTaskSegment = make_LdCtrlTaskSegment_t(this);
            LdCtrlTaskSegment->read(reader);
            continue;
        }
        if (reader.name() == QString("LdCtrlTaskPtr")) {
            auto * LdCtrlTaskPtr = make_LdCtrlTaskPtr_t(this);
            LdCtrlTaskPtr->read(reader);
            continue;
        }
        if (reader.name() == QString("LdCtrlTaskCtrl1")) {
            auto * LdCtrlTaskCtrl1 = make_LdCtrlTaskCtrl1_t(this);
            LdCtrlTaskCtrl1->read(reader);
            continue;
        }
        if (reader.name() == QString("LdCtrlTaskCtrl2")) {
            auto * LdCtrlTaskCtrl2 = make_LdCtrlTaskCtrl2_t(this);
            LdCtrlTaskCtrl2->read(reader);
            continue;
        }
        if (reader.name() == QString("LdCtrlWriteProp")) {
            auto * LdCtrlWriteProp = make_LdCtrlWriteProp_t(this);
            LdCtrlWriteProp->read(reader);
            continue;
        }
        if (reader.name() == QString("LdCtrlCompareProp")) {
            auto * LdCtrlCompareProp = make_LdCtrlCompareProp_t(this);
            LdCtrlCompareProp->read(reader);
            continue;
        }
        if (reader.name() == QString("LdCtrlLoadImageProp")) {
            auto * LdCtrlLoadImageProp = make_LdCtrlLoadImageProp_t(this);
            LdCtrlLoadImageProp->read(reader);
            continue;
        }
        if (reader.name() == QString("LdCtrlInvokeFunctionProp")) {
            auto * LdCtrlInvokeFunctionProp = make_LdCtrlInvokeFunctionProp_t(this);
            LdCtrlInvokeFunctionProp->read(reader);
            continue;
        }
        if (reader.name() == QString("LdCtrlReadFunctionProp")) {
            auto * LdCtrlReadFunctionProp = make_LdCtrlReadFunctionProp_t(this);
            LdCtrlReadFunctionProp->read(reader);
            continue;
        }
        if (reader.name() == QString("LdCtrlWriteMem")) {
            auto * LdCtrlWriteMem = make_LdCtrlWriteMem_t(this);
            LdCtrlWriteMem->read(reader);
            continue;
        }
        if (reader.name() == QString("LdCtrlCompareMem")) {
            auto * LdCtrlCompareMem = make_LdCtrlCompareMem_t(this);
            LdCtrlCompareMem->read(reader);
            continue;
        }
        if (reader.name() == QString("LdCtrlLoadImageMem")) {
            auto * LdCtrlLoadImageMem = make_LdCtrlLoadImageMem_t(this);
            LdCtrlLoadImageMem->read(reader);
            continue;
        }
        if (reader.name() == QString("LdCtrlWriteRelMem")) {
            auto * LdCtrlWriteRelMem = make_LdCtrlWriteRelMem_t(this);
            LdCtrlWriteRelMem->read(reader);
            continue;
        }
        if (reader.name() == QString("LdCtrlCompareRelMem")) {
            auto * LdCtrlCompareRelMem = make_LdCtrlCompareRelMem_t(this);
            LdCtrlCompareRelMem->read(reader);
            continue;
        }
        if (reader.name() == QString("LdCtrlLoadImageRelMem")) {
            auto * LdCtrlLoadImageRelMem = make_LdCtrlLoadImageRelMem_t(this);
            LdCtrlLoadImageRelMem->read(reader);
            continue;
        }
        if (reader.name() == QString("LdCtrlConnect")) {
            auto * LdCtrlConnect = make_LdCtrlConnect_t(this);
            LdCtrlConnect->read(reader);
            continue;
        }
        if (reader.name() == QString("LdCtrlDisconnect")) {
            auto * LdCtrlDisconnect = make_LdCtrlDisconnect_t(this);
            LdCtrlDisconnect->read(reader);
            continue;
        }
        if (reader.name() == QString("LdCtrlRestart")) {
            auto * LdCtrlRestart = make_LdCtrlRestart_t(this);
            LdCtrlRestart->read(reader);
            continue;
        }
        if (reader.name() == QString("LdCtrlMasterReset")) {
            auto * LdCtrlMasterReset = make_LdCtrlMasterReset_t(this);
            LdCtrlMasterReset->read(reader);
            continue;
        }
        if (reader.name() == QString("LdCtrlDelay")) {
            auto * LdCtrlDelay = make_LdCtrlDelay_t(this);
            LdCtrlDelay->read(reader);
            continue;
        }
        if (reader.name() == QString("LdCtrlSetControlVariable")) {
            auto * LdCtrlSetControlVariable = make_LdCtrlSetControlVariable_t(this);
            LdCtrlSetControlVariable->read(reader);
            continue;
        }
        if (reader.name() == QString("LdCtrlMapError")) {
            auto * LdCtrlMapError = make_LdCtrlMapError_t(this);
            LdCtrlMapError->read(reader);
            continue;
        }
        if (reader.name() == QString("LdCtrlProgressText")) {
            auto * LdCtrlProgressText = make_LdCtrlProgressText_t(this);
            LdCtrlProgressText->read(reader);
            continue;
        }
        if (reader.name() == QString("LdCtrlDeclarePropDesc")) {
            auto * LdCtrlDeclarePropDesc = make_LdCtrlDeclarePropDesc_t(this);
            LdCtrlDeclarePropDesc->read(reader);
            continue;
        }
        if (reader.name() == QString("LdCtrlClearLCFilterTable")) {
            auto * LdCtrlClearLCFilterTable = make_LdCtrlClearLCFilterTable_t(this);
            LdCtrlClearLCFilterTable->read(reader);
            continue;
        }
        if (reader.name() == QString("LdCtrlMerge")) {
            auto * LdCtrlMerge = make_LdCtrlMerge_t(this);
            LdCtrlMerge->read(reader);
            continue;
        }
        if (reader.name() == QString("choose")) {
            auto * Choose = make_LdCtrlBaseChoose_t(this);
            Choose->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int HawkConfigurationData_Procedures_Procedure_t::tableColumnCount() const
{
    return 4;
}

QVariant HawkConfigurationData_Procedures_Procedure_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "HawkConfigurationData_Procedures_Procedure";
        }
        if (qualifiedName == QString("ProcedureType")) {
            return ProcedureType;
        }
        if (qualifiedName == QString("ProcedureSubType")) {
            return ProcedureSubType;
        }
        if (qualifiedName == QString("Access")) {
            return Access.join(' ');
        }
        break;
    }
    return QVariant();
}

QVariant HawkConfigurationData_Procedures_Procedure_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "ProcedureType";
            case 2:
                return "ProcedureSubType";
            case 3:
                return "Access";
            }
        }
    }
    return QVariant();
}

QVariant HawkConfigurationData_Procedures_Procedure_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "Procedure";
    case Qt::DecorationRole:
        return QIcon::fromTheme("HawkConfigurationData_Procedures_Procedure");
    }
    return QVariant();
}

HawkConfigurationData_Procedures_Procedure_t * make_HawkConfigurationData_Procedures_Procedure_t(Base * parent)
{
    return new HawkConfigurationData_Procedures_Procedure_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
