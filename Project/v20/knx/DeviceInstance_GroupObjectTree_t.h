/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v20/knx/ComObjectRef_t.h>
#include <Project/v20/knx/RELIDREFS.h>

namespace Project {
namespace v20 {
namespace knx {

/* forward declarations */
class DeviceInstance_GroupObjectTree_Nodes_t;

class DeviceInstance_GroupObjectTree_t : public Base
{
    Q_OBJECT

public:
    explicit DeviceInstance_GroupObjectTree_t(Base * parent = nullptr);
    virtual ~DeviceInstance_GroupObjectTree_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    RELIDREFS GroupObjectInstances{};

    /* elements */
    DeviceInstance_GroupObjectTree_Nodes_t * Nodes{};

    /* getters */
    QList<ComObjectRef_t *> getGroupObjectInstances() const; // attribute: GroupObjectInstances
};

DeviceInstance_GroupObjectTree_t * make_DeviceInstance_GroupObjectTree_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
