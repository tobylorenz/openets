/* This file is generated. */

#include <Project/v20/knx/ModuleDefStatic_ParameterValidations_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/KNX_t.h>

namespace Project {
namespace v20 {
namespace knx {

ModuleDefStatic_ParameterValidations_t::ModuleDefStatic_ParameterValidations_t(Base * parent) :
    Base(parent)
{
}

ModuleDefStatic_ParameterValidations_t::~ModuleDefStatic_ParameterValidations_t()
{
}

void ModuleDefStatic_ParameterValidations_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("ParameterValidation")) {
            QString newId = reader.attributes().value("Id").toString();
            Q_ASSERT(!newId.isEmpty());
            ParameterValidation_t * newParameterValidation;
            if (ParameterValidation.contains(newId)) {
                newParameterValidation = ParameterValidation[newId];
            } else {
                newParameterValidation = make_ParameterValidation_t(this);
                ParameterValidation[newId] = newParameterValidation;
            }
            newParameterValidation->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ModuleDefStatic_ParameterValidations_t::tableColumnCount() const
{
    return 1;
}

QVariant ModuleDefStatic_ParameterValidations_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ModuleDefStatic_ParameterValidations";
        }
        break;
    }
    return QVariant();
}

QVariant ModuleDefStatic_ParameterValidations_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant ModuleDefStatic_ParameterValidations_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "ParameterValidations";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ModuleDefStatic_ParameterValidations");
    }
    return QVariant();
}

ModuleDefStatic_ParameterValidations_t * make_ModuleDefStatic_ParameterValidations_t(Base * parent)
{
    return new ModuleDefStatic_ParameterValidations_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
