/* This file is generated. */

#include <Project/v20/knx/ModuleDefStatic_ComObjects_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/KNX_t.h>

namespace Project {
namespace v20 {
namespace knx {

ModuleDefStatic_ComObjects_t::ModuleDefStatic_ComObjects_t(Base * parent) :
    Base(parent)
{
}

ModuleDefStatic_ComObjects_t::~ModuleDefStatic_ComObjects_t()
{
}

void ModuleDefStatic_ComObjects_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("ComObject")) {
            QString newId = reader.attributes().value("Id").toString();
            Q_ASSERT(!newId.isEmpty());
            ModuleDefStatic_ComObjects_ComObject_t * newComObject;
            if (ComObject.contains(newId)) {
                newComObject = ComObject[newId];
            } else {
                newComObject = make_ModuleDefStatic_ComObjects_ComObject_t(this);
                ComObject[newId] = newComObject;
            }
            newComObject->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ModuleDefStatic_ComObjects_t::tableColumnCount() const
{
    return 1;
}

QVariant ModuleDefStatic_ComObjects_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ModuleDefStatic_ComObjects";
        }
        break;
    }
    return QVariant();
}

QVariant ModuleDefStatic_ComObjects_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant ModuleDefStatic_ComObjects_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "ComObjects";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ModuleDefStatic_ComObjects");
    }
    return QVariant();
}

ModuleDefStatic_ComObjects_t * make_ModuleDefStatic_ComObjects_t(Base * parent)
{
    return new ModuleDefStatic_ComObjects_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
