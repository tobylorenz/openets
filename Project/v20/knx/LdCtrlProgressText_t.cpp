/* This file is generated. */

#include <Project/v20/knx/LdCtrlProgressText_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/KNX_t.h>

namespace Project {
namespace v20 {
namespace knx {

LdCtrlProgressText_t::LdCtrlProgressText_t(Base * parent) :
    Base(parent)
{
}

LdCtrlProgressText_t::~LdCtrlProgressText_t()
{
}

void LdCtrlProgressText_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("AppliesTo")) {
            AppliesTo = attribute.value().toString();
            continue;
        }
        if (name == QString("InternalDescription")) {
            InternalDescription = attribute.value().toString();
            continue;
        }
        if (name == QString("TextId")) {
            TextId = attribute.value().toString();
            continue;
        }
        if (name == QString("MessageRef")) {
            QString value = attribute.value().toString();
            if (!value.isEmpty())
                qWarning() << "knx:LdCtrlProgressText_t attribute MessageRef references to" << value;
        }
        if (name == QString("MessageRef")) {
            MessageRef = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("OnError")) {
            auto * newOnError = make_LdCtrlBase_OnError_t(this);
            newOnError->read(reader);
            OnError.append(newOnError);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int LdCtrlProgressText_t::tableColumnCount() const
{
    return 5;
}

QVariant LdCtrlProgressText_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "LdCtrlProgressText";
        }
        if (qualifiedName == QString("AppliesTo")) {
            return AppliesTo;
        }
        if (qualifiedName == QString("InternalDescription")) {
            return InternalDescription;
        }
        if (qualifiedName == QString("TextId")) {
            return TextId;
        }
        if (qualifiedName == QString("MessageRef")) {
            return MessageRef;
        }
        break;
    }
    return QVariant();
}

QVariant LdCtrlProgressText_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "AppliesTo";
            case 2:
                return "InternalDescription";
            case 3:
                return "TextId";
            case 4:
                return "MessageRef";
            }
        }
    }
    return QVariant();
}

QVariant LdCtrlProgressText_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "LdCtrlProgressText";
    case Qt::DecorationRole:
        return QIcon::fromTheme("LdCtrlProgressText");
    }
    return QVariant();
}

Base * LdCtrlProgressText_t::getMessage() const
{
    if (MessageRef.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<Base *>(knx->ids[MessageRef]);
}

LdCtrlProgressText_t * make_LdCtrlProgressText_t(Base * parent)
{
    return new LdCtrlProgressText_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
