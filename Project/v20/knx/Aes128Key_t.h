/* This file is generated. */

#pragma once

#include <Project/xs/String.h>

namespace Project {
namespace v20 {
namespace knx {

using Aes128Key_t = xs::String;

} // namespace knx
} // namespace v20
} // namespace Project
