/* This file is generated. */

#include <Project/v20/knx/DatapointType_DatapointSubtypes_DatapointSubtype_Format_Enumeration_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/KNX_t.h>

namespace Project {
namespace v20 {
namespace knx {

DatapointType_DatapointSubtypes_DatapointSubtype_Format_Enumeration_t::DatapointType_DatapointSubtypes_DatapointSubtype_Format_Enumeration_t(Base * parent) :
    Base(parent)
{
}

DatapointType_DatapointSubtypes_DatapointSubtype_Format_Enumeration_t::~DatapointType_DatapointSubtypes_DatapointSubtype_Format_Enumeration_t()
{
}

void DatapointType_DatapointSubtypes_DatapointSubtype_Format_Enumeration_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == "Id") {
            Id = attribute.value().toString();
            KNX_t * knx = findParent<KNX_t *>();
            Q_ASSERT(knx);
            knx->ids[Id] = this;
            continue;
        }
        if (name == QString("Width")) {
            Width = attribute.value().toString();
            continue;
        }
        if (name == QString("Name")) {
            Name = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("EnumValue")) {
            QString newId = reader.attributes().value("Id").toString();
            Q_ASSERT(!newId.isEmpty());
            DatapointType_DatapointSubtypes_DatapointSubtype_Format_Enumeration_EnumValue_t * newEnumValue;
            if (EnumValue.contains(newId)) {
                newEnumValue = EnumValue[newId];
            } else {
                newEnumValue = make_DatapointType_DatapointSubtypes_DatapointSubtype_Format_Enumeration_EnumValue_t(this);
                EnumValue[newId] = newEnumValue;
            }
            newEnumValue->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int DatapointType_DatapointSubtypes_DatapointSubtype_Format_Enumeration_t::tableColumnCount() const
{
    return 4;
}

QVariant DatapointType_DatapointSubtypes_DatapointSubtype_Format_Enumeration_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "DatapointType_DatapointSubtypes_DatapointSubtype_Format_Enumeration";
        }
        if (qualifiedName == QString("Id")) {
            return Id;
        }
        if (qualifiedName == QString("Width")) {
            return Width;
        }
        if (qualifiedName == QString("Name")) {
            return Name;
        }
        break;
    }
    return QVariant();
}

QVariant DatapointType_DatapointSubtypes_DatapointSubtype_Format_Enumeration_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Id";
            case 2:
                return "Width";
            case 3:
                return "Name";
            }
        }
    }
    return QVariant();
}

QVariant DatapointType_DatapointSubtypes_DatapointSubtype_Format_Enumeration_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return !Name.isEmpty() ? Name : "Enumeration";
    case Qt::DecorationRole:
        return QIcon::fromTheme("DatapointType_DatapointSubtypes_DatapointSubtype_Format_Enumeration");
    }
    return QVariant();
}

DatapointType_DatapointSubtypes_DatapointSubtype_Format_Enumeration_t * make_DatapointType_DatapointSubtypes_DatapointSubtype_Format_Enumeration_t(Base * parent)
{
    return new DatapointType_DatapointSubtypes_DatapointSubtype_Format_Enumeration_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
