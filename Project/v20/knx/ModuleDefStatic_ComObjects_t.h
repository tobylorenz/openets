/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v20/knx/ModuleDefStatic_ComObjects_ComObject_t.h>
#include <Project/xs/ID.h>

namespace Project {
namespace v20 {
namespace knx {

class ModuleDefStatic_ComObjects_t : public Base
{
    Q_OBJECT

public:
    explicit ModuleDefStatic_ComObjects_t(Base * parent = nullptr);
    virtual ~ModuleDefStatic_ComObjects_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* elements */
    QMap<xs::ID, ModuleDefStatic_ComObjects_ComObject_t *> ComObject; // key: Id
};

ModuleDefStatic_ComObjects_t * make_ModuleDefStatic_ComObjects_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
