/* This file is generated. */

#include <Project/v20/knx/ModuleDefStatic_Parameters_Parameter_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/KNX_t.h>
#include <Project/v20/knx/ModuleDefStatic_Parameters_Parameter_Memory_t.h>
#include <Project/v20/knx/ModuleDefStatic_Parameters_Parameter_Property_t.h>
#include <Project/v20/knx/ParameterType_t.h>

namespace Project {
namespace v20 {
namespace knx {

ModuleDefStatic_Parameters_Parameter_t::ModuleDefStatic_Parameters_Parameter_t(Base * parent) :
    Base(parent)
{
}

ModuleDefStatic_Parameters_Parameter_t::~ModuleDefStatic_Parameters_Parameter_t()
{
}

void ModuleDefStatic_Parameters_Parameter_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == "Id") {
            Id = attribute.value().toString();
            KNX_t * knx = findParent<KNX_t *>();
            Q_ASSERT(knx);
            knx->ids[Id] = this;
            continue;
        }
        if (name == QString("Name")) {
            Name = attribute.value().toString();
            continue;
        }
        if (name == QString("ParameterType")) {
            ParameterType = attribute.value().toString();
            continue;
        }
        if (name == QString("ParameterTypeParams")) {
            QString value = attribute.value().toString();
            if (!value.isEmpty())
                qWarning() << "knx:ModuleDefStatic_Parameters_Parameter_t attribute ParameterTypeParams references to" << value;
        }
        if (name == QString("ParameterTypeParams")) {
            ParameterTypeParams = attribute.value().toString().split(' ');
            continue;
        }
        if (name == QString("Text")) {
            Text = attribute.value().toString();
            continue;
        }
        if (name == QString("SuffixText")) {
            SuffixText = attribute.value().toString();
            continue;
        }
        if (name == QString("Access")) {
            Access = attribute.value().toString();
            continue;
        }
        if (name == QString("Value")) {
            Value = attribute.value().toString();
            continue;
        }
        if (name == QString("InitialValue")) {
            InitialValue = attribute.value().toString();
            continue;
        }
        if (name == QString("CustomerAdjustable")) {
            CustomerAdjustable = attribute.value().toString();
            continue;
        }
        if (name == QString("InternalDescription")) {
            InternalDescription = attribute.value().toString();
            continue;
        }
        if (name == QString("BaseValue")) {
            QString value = attribute.value().toString();
            if (!value.isEmpty())
                qWarning() << "knx:ModuleDefStatic_Parameters_Parameter_t attribute BaseValue references to" << value;
        }
        if (name == QString("BaseValue")) {
            BaseValue = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("Memory")) {
            auto * Memory = make_ModuleDefStatic_Parameters_Parameter_Memory_t(this);
            Memory->read(reader);
            continue;
        }
        if (reader.name() == QString("Property")) {
            auto * Property = make_ModuleDefStatic_Parameters_Parameter_Property_t(this);
            Property->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ModuleDefStatic_Parameters_Parameter_t::tableColumnCount() const
{
    return 13;
}

QVariant ModuleDefStatic_Parameters_Parameter_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ModuleDefStatic_Parameters_Parameter";
        }
        if (qualifiedName == QString("Id")) {
            return Id;
        }
        if (qualifiedName == QString("Name")) {
            return Name;
        }
        if (qualifiedName == QString("ParameterType")) {
            return ParameterType;
        }
        if (qualifiedName == QString("ParameterTypeParams")) {
            return ParameterTypeParams.join(' ');
        }
        if (qualifiedName == QString("Text")) {
            return Text;
        }
        if (qualifiedName == QString("SuffixText")) {
            return SuffixText;
        }
        if (qualifiedName == QString("Access")) {
            return Access;
        }
        if (qualifiedName == QString("Value")) {
            return Value;
        }
        if (qualifiedName == QString("InitialValue")) {
            return InitialValue;
        }
        if (qualifiedName == QString("CustomerAdjustable")) {
            return CustomerAdjustable;
        }
        if (qualifiedName == QString("InternalDescription")) {
            return InternalDescription;
        }
        if (qualifiedName == QString("BaseValue")) {
            return BaseValue;
        }
        break;
    }
    return QVariant();
}

QVariant ModuleDefStatic_Parameters_Parameter_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Id";
            case 2:
                return "Name";
            case 3:
                return "ParameterType";
            case 4:
                return "ParameterTypeParams";
            case 5:
                return "Text";
            case 6:
                return "SuffixText";
            case 7:
                return "Access";
            case 8:
                return "Value";
            case 9:
                return "InitialValue";
            case 10:
                return "CustomerAdjustable";
            case 11:
                return "InternalDescription";
            case 12:
                return "BaseValue";
            }
        }
    }
    return QVariant();
}

QVariant ModuleDefStatic_Parameters_Parameter_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return !Name.isEmpty() ? Name : "Parameter";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ModuleDefStatic_Parameters_Parameter");
    }
    return QVariant();
}

ParameterType_t * ModuleDefStatic_Parameters_Parameter_t::getParameterType() const
{
    if (ParameterType.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<ParameterType_t *>(knx->ids[ParameterType]);
}

Base * ModuleDefStatic_Parameters_Parameter_t::getBaseValue() const
{
    if (BaseValue.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<Base *>(knx->ids[BaseValue]);
}

ModuleDefStatic_Parameters_Parameter_t * make_ModuleDefStatic_Parameters_Parameter_t(Base * parent)
{
    return new ModuleDefStatic_Parameters_Parameter_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
