/* This file is generated. */

#pragma once

#include <Project/xs/Int.h>

namespace Project {
namespace v20 {
namespace knx {

using Topology_Area_Address_t = xs::Int;

} // namespace knx
} // namespace v20
} // namespace Project
