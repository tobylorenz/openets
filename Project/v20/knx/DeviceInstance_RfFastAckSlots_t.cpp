/* This file is generated. */

#include <Project/v20/knx/DeviceInstance_RfFastAckSlots_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/KNX_t.h>

namespace Project {
namespace v20 {
namespace knx {

DeviceInstance_RfFastAckSlots_t::DeviceInstance_RfFastAckSlots_t(Base * parent) :
    Base(parent)
{
}

DeviceInstance_RfFastAckSlots_t::~DeviceInstance_RfFastAckSlots_t()
{
}

void DeviceInstance_RfFastAckSlots_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("Slot")) {
            QString newGroupAddressRefId = reader.attributes().value("GroupAddressRefId").toString();
            Q_ASSERT(!newGroupAddressRefId.isEmpty());
            DeviceInstance_RfFastAckSlots_Slot_t * newSlot;
            if (Slot.contains(newGroupAddressRefId)) {
                newSlot = Slot[newGroupAddressRefId];
            } else {
                newSlot = make_DeviceInstance_RfFastAckSlots_Slot_t(this);
                Slot[newGroupAddressRefId] = newSlot;
            }
            newSlot->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int DeviceInstance_RfFastAckSlots_t::tableColumnCount() const
{
    return 1;
}

QVariant DeviceInstance_RfFastAckSlots_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "DeviceInstance_RfFastAckSlots";
        }
        break;
    }
    return QVariant();
}

QVariant DeviceInstance_RfFastAckSlots_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant DeviceInstance_RfFastAckSlots_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "RfFastAckSlots";
    case Qt::DecorationRole:
        return QIcon::fromTheme("DeviceInstance_RfFastAckSlots");
    }
    return QVariant();
}

DeviceInstance_RfFastAckSlots_t * make_DeviceInstance_RfFastAckSlots_t(Base * parent)
{
    return new DeviceInstance_RfFastAckSlots_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
