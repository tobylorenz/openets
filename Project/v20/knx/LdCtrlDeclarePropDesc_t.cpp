/* This file is generated. */

#include <Project/v20/knx/LdCtrlDeclarePropDesc_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/KNX_t.h>

namespace Project {
namespace v20 {
namespace knx {

LdCtrlDeclarePropDesc_t::LdCtrlDeclarePropDesc_t(Base * parent) :
    Base(parent)
{
}

LdCtrlDeclarePropDesc_t::~LdCtrlDeclarePropDesc_t()
{
}

void LdCtrlDeclarePropDesc_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("AppliesTo")) {
            AppliesTo = attribute.value().toString();
            continue;
        }
        if (name == QString("InternalDescription")) {
            InternalDescription = attribute.value().toString();
            continue;
        }
        if (name == QString("ObjIdx")) {
            ObjIdx = attribute.value().toString();
            continue;
        }
        if (name == QString("ObjType")) {
            ObjType = attribute.value().toString();
            continue;
        }
        if (name == QString("Occurrence")) {
            Occurrence = attribute.value().toString();
            continue;
        }
        if (name == QString("PropId")) {
            PropId = attribute.value().toString();
            continue;
        }
        if (name == QString("PropType")) {
            PropType = attribute.value().toString();
            continue;
        }
        if (name == QString("MaxElements")) {
            MaxElements = attribute.value().toString();
            continue;
        }
        if (name == QString("ReadAccess")) {
            ReadAccess = attribute.value().toString();
            continue;
        }
        if (name == QString("WriteAccess")) {
            WriteAccess = attribute.value().toString();
            continue;
        }
        if (name == QString("Writable")) {
            Writable = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("OnError")) {
            auto * newOnError = make_LdCtrlBase_OnError_t(this);
            newOnError->read(reader);
            OnError.append(newOnError);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int LdCtrlDeclarePropDesc_t::tableColumnCount() const
{
    return 12;
}

QVariant LdCtrlDeclarePropDesc_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "LdCtrlDeclarePropDesc";
        }
        if (qualifiedName == QString("AppliesTo")) {
            return AppliesTo;
        }
        if (qualifiedName == QString("InternalDescription")) {
            return InternalDescription;
        }
        if (qualifiedName == QString("ObjIdx")) {
            return ObjIdx;
        }
        if (qualifiedName == QString("ObjType")) {
            return ObjType;
        }
        if (qualifiedName == QString("Occurrence")) {
            return Occurrence;
        }
        if (qualifiedName == QString("PropId")) {
            return PropId;
        }
        if (qualifiedName == QString("PropType")) {
            return PropType;
        }
        if (qualifiedName == QString("MaxElements")) {
            return MaxElements;
        }
        if (qualifiedName == QString("ReadAccess")) {
            return ReadAccess;
        }
        if (qualifiedName == QString("WriteAccess")) {
            return WriteAccess;
        }
        if (qualifiedName == QString("Writable")) {
            return Writable;
        }
        break;
    }
    return QVariant();
}

QVariant LdCtrlDeclarePropDesc_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "AppliesTo";
            case 2:
                return "InternalDescription";
            case 3:
                return "ObjIdx";
            case 4:
                return "ObjType";
            case 5:
                return "Occurrence";
            case 6:
                return "PropId";
            case 7:
                return "PropType";
            case 8:
                return "MaxElements";
            case 9:
                return "ReadAccess";
            case 10:
                return "WriteAccess";
            case 11:
                return "Writable";
            }
        }
    }
    return QVariant();
}

QVariant LdCtrlDeclarePropDesc_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "LdCtrlDeclarePropDesc";
    case Qt::DecorationRole:
        return QIcon::fromTheme("LdCtrlDeclarePropDesc");
    }
    return QVariant();
}

LdCtrlDeclarePropDesc_t * make_LdCtrlDeclarePropDesc_t(Base * parent)
{
    return new LdCtrlDeclarePropDesc_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
