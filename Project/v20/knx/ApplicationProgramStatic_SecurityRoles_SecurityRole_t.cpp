/* This file is generated. */

#include <Project/v20/knx/ApplicationProgramStatic_SecurityRoles_SecurityRole_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/KNX_t.h>

namespace Project {
namespace v20 {
namespace knx {

ApplicationProgramStatic_SecurityRoles_SecurityRole_t::ApplicationProgramStatic_SecurityRoles_SecurityRole_t(Base * parent) :
    Base(parent)
{
}

ApplicationProgramStatic_SecurityRoles_SecurityRole_t::~ApplicationProgramStatic_SecurityRoles_SecurityRole_t()
{
}

void ApplicationProgramStatic_SecurityRoles_SecurityRole_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == "Id") {
            Id = attribute.value().toString();
            KNX_t * knx = findParent<KNX_t *>();
            Q_ASSERT(knx);
            knx->ids[Id] = this;
            continue;
        }
        if (name == QString("Text")) {
            Text = attribute.value().toString();
            continue;
        }
        if (name == QString("Mask")) {
            Mask = attribute.value().toString();
            continue;
        }
        if (name == QString("RoleID")) {
            RoleID = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ApplicationProgramStatic_SecurityRoles_SecurityRole_t::tableColumnCount() const
{
    return 5;
}

QVariant ApplicationProgramStatic_SecurityRoles_SecurityRole_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ApplicationProgramStatic_SecurityRoles_SecurityRole";
        }
        if (qualifiedName == QString("Id")) {
            return Id;
        }
        if (qualifiedName == QString("Text")) {
            return Text;
        }
        if (qualifiedName == QString("Mask")) {
            return Mask;
        }
        if (qualifiedName == QString("RoleID")) {
            return RoleID;
        }
        break;
    }
    return QVariant();
}

QVariant ApplicationProgramStatic_SecurityRoles_SecurityRole_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Id";
            case 2:
                return "Text";
            case 3:
                return "Mask";
            case 4:
                return "RoleID";
            }
        }
    }
    return QVariant();
}

QVariant ApplicationProgramStatic_SecurityRoles_SecurityRole_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "SecurityRole";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ApplicationProgramStatic_SecurityRoles_SecurityRole");
    }
    return QVariant();
}

ApplicationProgramStatic_SecurityRoles_SecurityRole_t * make_ApplicationProgramStatic_SecurityRoles_SecurityRole_t(Base * parent)
{
    return new ApplicationProgramStatic_SecurityRoles_SecurityRole_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
