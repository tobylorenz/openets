/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v20/knx/LdCtrlBase_OnError_t.h>
#include <Project/v20/knx/LdCtrlProcType_t.h>
#include <Project/xs/String.h>
#include <Project/xs/UnsignedByte.h>
#include <Project/xs/UnsignedShort.h>

namespace Project {
namespace v20 {
namespace knx {

class LdCtrlTaskCtrl1_t : public Base
{
    Q_OBJECT

public:
    explicit LdCtrlTaskCtrl1_t(Base * parent = nullptr);
    virtual ~LdCtrlTaskCtrl1_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    LdCtrlProcType_t AppliesTo{"auto"};
    xs::String InternalDescription{};
    xs::UnsignedByte LsmIdx{};
    xs::UnsignedShort ObjType{};
    xs::UnsignedShort Occurrence{"0"};
    xs::UnsignedShort Address{};
    xs::UnsignedByte Count{};

    /* elements */
    QVector<LdCtrlBase_OnError_t *> OnError;
};

LdCtrlTaskCtrl1_t * make_LdCtrlTaskCtrl1_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
