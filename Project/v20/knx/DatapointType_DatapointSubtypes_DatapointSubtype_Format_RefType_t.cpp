/* This file is generated. */

#include <Project/v20/knx/DatapointType_DatapointSubtypes_DatapointSubtype_Format_RefType_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/KNX_t.h>

namespace Project {
namespace v20 {
namespace knx {

DatapointType_DatapointSubtypes_DatapointSubtype_Format_RefType_t::DatapointType_DatapointSubtypes_DatapointSubtype_Format_RefType_t(Base * parent) :
    Base(parent)
{
}

DatapointType_DatapointSubtypes_DatapointSubtype_Format_RefType_t::~DatapointType_DatapointSubtypes_DatapointSubtype_Format_RefType_t()
{
}

void DatapointType_DatapointSubtypes_DatapointSubtype_Format_RefType_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("RefId")) {
            RefId = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int DatapointType_DatapointSubtypes_DatapointSubtype_Format_RefType_t::tableColumnCount() const
{
    return 2;
}

QVariant DatapointType_DatapointSubtypes_DatapointSubtype_Format_RefType_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "DatapointType_DatapointSubtypes_DatapointSubtype_Format_RefType";
        }
        if (qualifiedName == QString("RefId")) {
            return RefId;
        }
        break;
    }
    return QVariant();
}

QVariant DatapointType_DatapointSubtypes_DatapointSubtype_Format_RefType_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "RefId";
            }
        }
    }
    return QVariant();
}

QVariant DatapointType_DatapointSubtypes_DatapointSubtype_Format_RefType_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "RefType";
    case Qt::DecorationRole:
        return QIcon::fromTheme("DatapointType_DatapointSubtypes_DatapointSubtype_Format_RefType");
    }
    return QVariant();
}

Base * DatapointType_DatapointSubtypes_DatapointSubtype_Format_RefType_t::getType() const
{
    if (RefId.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<Base *>(knx->ids[RefId]);
}

DatapointType_DatapointSubtypes_DatapointSubtype_Format_RefType_t * make_DatapointType_DatapointSubtypes_DatapointSubtype_Format_RefType_t(Base * parent)
{
    return new DatapointType_DatapointSubtypes_DatapointSubtype_Format_RefType_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
