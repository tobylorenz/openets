/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v20/knx/IDREF.h>
#include <Project/v20/knx/LanguageData_TranslationUnit_TranslationElement_t.h>
#include <Project/xs/Int.h>

namespace Project {
namespace v20 {
namespace knx {

/* forward declarations */
class CatalogSection_t;

class LanguageData_TranslationUnit_t : public Base
{
    Q_OBJECT

public:
    explicit LanguageData_TranslationUnit_t(Base * parent = nullptr);
    virtual ~LanguageData_TranslationUnit_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    IDREF RefId{};
    xs::Int Version{"0"};

    /* elements */
    QMap<IDREF, LanguageData_TranslationUnit_TranslationElement_t *> TranslationElement; // key: RefId

    /* getters */
    CatalogSection_t * getCatalogSection() const; // attribute: RefId
};

LanguageData_TranslationUnit_t * make_LanguageData_TranslationUnit_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
