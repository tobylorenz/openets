/* This file is generated. */

#include <Project/v20/knx/BinaryDataRef_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/BinaryData_t.h>
#include <Project/v20/knx/KNX_t.h>

namespace Project {
namespace v20 {
namespace knx {

BinaryDataRef_t::BinaryDataRef_t(Base * parent) :
    Base(parent)
{
}

BinaryDataRef_t::~BinaryDataRef_t()
{
}

void BinaryDataRef_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("RefId")) {
            RefId = attribute.value().toString();
            continue;
        }
        if (name == QString("InternalDescription")) {
            InternalDescription = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("Data")) {
            if (!Data) {
                Data = make_SimpleElementTextType("Data", this);
            }
            Data->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int BinaryDataRef_t::tableColumnCount() const
{
    return 3;
}

QVariant BinaryDataRef_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "BinaryDataRef";
        }
        if (qualifiedName == QString("RefId")) {
            return RefId;
        }
        if (qualifiedName == QString("InternalDescription")) {
            return InternalDescription;
        }
        break;
    }
    return QVariant();
}

QVariant BinaryDataRef_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "RefId";
            case 2:
                return "InternalDescription";
            }
        }
    }
    return QVariant();
}

QVariant BinaryDataRef_t::treeData(int role) const
{
    auto * ref = getBinaryData();
    if (ref) {
        return ref->treeData(role);
    }

    switch (role) {
    case Qt::DisplayRole:
        return "BinaryDataRef";
    case Qt::DecorationRole:
        return QIcon::fromTheme("BinaryDataRef");
    }
    return QVariant();
}

BinaryData_t * BinaryDataRef_t::getBinaryData() const
{
    if (RefId.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<BinaryData_t *>(knx->ids[RefId]);
}

BinaryDataRef_t * make_BinaryDataRef_t(Base * parent)
{
    return new BinaryDataRef_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
