/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v20/knx/LdCtrlBase_OnError_t.h>
#include <Project/v20/knx/LdCtrlProcType_t.h>
#include <Project/xs/Boolean.h>
#include <Project/xs/HexBinary.h>
#include <Project/xs/String.h>
#include <Project/xs/UnsignedByte.h>
#include <Project/xs/UnsignedInt.h>
#include <Project/xs/UnsignedShort.h>

namespace Project {
namespace v20 {
namespace knx {

class LdCtrlWriteRelMem_t : public Base
{
    Q_OBJECT

public:
    explicit LdCtrlWriteRelMem_t(Base * parent = nullptr);
    virtual ~LdCtrlWriteRelMem_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    LdCtrlProcType_t AppliesTo{"auto"};
    xs::String InternalDescription{};
    xs::UnsignedByte ObjIdx{};
    xs::UnsignedShort ObjType{};
    xs::UnsignedShort Occurrence{"0"};
    xs::UnsignedInt Offset{};
    xs::UnsignedInt Size{};
    xs::Boolean Verify{};
    xs::HexBinary InlineData{};

    /* elements */
    QVector<LdCtrlBase_OnError_t *> OnError;
};

LdCtrlWriteRelMem_t * make_LdCtrlWriteRelMem_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
