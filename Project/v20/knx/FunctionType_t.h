/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v20/knx/DeprecationStatus_t.h>
#include <Project/v20/knx/FunctionType_FunctionPoint_t.h>
#include <Project/v20/knx/LanguageDependentString255_t.h>
#include <Project/xs/ID.h>
#include <Project/xs/String.h>
#include <Project/xs/UnsignedInt.h>

namespace Project {
namespace v20 {
namespace knx {

class FunctionType_t : public Base
{
    Q_OBJECT

public:
    explicit FunctionType_t(Base * parent = nullptr);
    virtual ~FunctionType_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::ID Id{};
    xs::UnsignedInt Number{};
    LanguageDependentString255_t Text{};
    LanguageDependentString255_t Description{};
    DeprecationStatus_t Status{"active"};
    xs::String Semantics{};

    /* elements */
    QMap<xs::ID, FunctionType_FunctionPoint_t *> FunctionPoint; // key: Id
};

FunctionType_t * make_FunctionType_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
