/* This file is generated. */

#include <Project/v20/knx/ModuleDefLdCtrlBaseChoose_when_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/KNX_t.h>
#include <Project/v20/knx/LdCtrlBaseChoose_t.h>
#include <Project/v20/knx/LdCtrlDeclarePropDesc_t.h>
#include <Project/v20/knx/LdCtrlDelay_t.h>
#include <Project/v20/knx/LdCtrlMerge_t.h>
#include <Project/v20/knx/LdCtrlProgressText_t.h>
#include <Project/v20/knx/ModuleDefLdCtrlCompareProp_t.h>
#include <Project/v20/knx/ModuleDefLdCtrlInvokeFunctionProp_t.h>
#include <Project/v20/knx/ModuleDefLdCtrlReadFunctionProp_t.h>
#include <Project/v20/knx/ModuleDefLdCtrlWriteProp_t.h>

namespace Project {
namespace v20 {
namespace knx {

ModuleDefLdCtrlBaseChoose_when_t::ModuleDefLdCtrlBaseChoose_when_t(Base * parent) :
    Base(parent)
{
}

ModuleDefLdCtrlBaseChoose_when_t::~ModuleDefLdCtrlBaseChoose_when_t()
{
}

void ModuleDefLdCtrlBaseChoose_when_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("test")) {
            Test = attribute.value().toString();
            continue;
        }
        if (name == QString("default")) {
            Default = attribute.value().toString();
            continue;
        }
        if (name == QString("InternalDescription")) {
            InternalDescription = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("LdCtrlWriteProp")) {
            auto * LdCtrlWriteProp = make_ModuleDefLdCtrlWriteProp_t(this);
            LdCtrlWriteProp->read(reader);
            continue;
        }
        if (reader.name() == QString("LdCtrlCompareProp")) {
            auto * LdCtrlCompareProp = make_ModuleDefLdCtrlCompareProp_t(this);
            LdCtrlCompareProp->read(reader);
            continue;
        }
        if (reader.name() == QString("LdCtrlInvokeFunctionProp")) {
            auto * LdCtrlInvokeFunctionProp = make_ModuleDefLdCtrlInvokeFunctionProp_t(this);
            LdCtrlInvokeFunctionProp->read(reader);
            continue;
        }
        if (reader.name() == QString("LdCtrlReadFunctionProp")) {
            auto * LdCtrlReadFunctionProp = make_ModuleDefLdCtrlReadFunctionProp_t(this);
            LdCtrlReadFunctionProp->read(reader);
            continue;
        }
        if (reader.name() == QString("LdCtrlDelay")) {
            auto * LdCtrlDelay = make_LdCtrlDelay_t(this);
            LdCtrlDelay->read(reader);
            continue;
        }
        if (reader.name() == QString("LdCtrlProgressText")) {
            auto * LdCtrlProgressText = make_LdCtrlProgressText_t(this);
            LdCtrlProgressText->read(reader);
            continue;
        }
        if (reader.name() == QString("LdCtrlDeclarePropDesc")) {
            auto * LdCtrlDeclarePropDesc = make_LdCtrlDeclarePropDesc_t(this);
            LdCtrlDeclarePropDesc->read(reader);
            continue;
        }
        if (reader.name() == QString("LdCtrlMerge")) {
            auto * LdCtrlMerge = make_LdCtrlMerge_t(this);
            LdCtrlMerge->read(reader);
            continue;
        }
        if (reader.name() == QString("choose")) {
            auto * Choose = make_LdCtrlBaseChoose_t(this);
            Choose->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ModuleDefLdCtrlBaseChoose_when_t::tableColumnCount() const
{
    return 4;
}

QVariant ModuleDefLdCtrlBaseChoose_when_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ModuleDefLdCtrlBaseChoose_when";
        }
        if (qualifiedName == QString("test")) {
            return Test;
        }
        if (qualifiedName == QString("default")) {
            return Default;
        }
        if (qualifiedName == QString("InternalDescription")) {
            return InternalDescription;
        }
        break;
    }
    return QVariant();
}

QVariant ModuleDefLdCtrlBaseChoose_when_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Test";
            case 2:
                return "Default";
            case 3:
                return "InternalDescription";
            }
        }
    }
    return QVariant();
}

QVariant ModuleDefLdCtrlBaseChoose_when_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "when";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ModuleDefLdCtrlBaseChoose_when");
    }
    return QVariant();
}

ModuleDefLdCtrlBaseChoose_when_t * make_ModuleDefLdCtrlBaseChoose_when_t(Base * parent)
{
    return new ModuleDefLdCtrlBaseChoose_when_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
