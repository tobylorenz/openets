/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v20/knx/BitOffset_t.h>
#include <Project/v20/knx/IDREF.h>
#include <Project/v20/knx/MemoryParameter_Offset_t.h>

namespace Project {
namespace v20 {
namespace knx {

/* forward declarations */
class ApplicationProgramStatic_Code_AbsoluteSegment_t;

class MemoryParameter_t : public Base
{
    Q_OBJECT

public:
    explicit MemoryParameter_t(Base * parent = nullptr);
    virtual ~MemoryParameter_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    IDREF CodeSegment{};
    MemoryParameter_Offset_t Offset{};
    BitOffset_t BitOffset{};

    /* getters */
    ApplicationProgramStatic_Code_AbsoluteSegment_t * getCodeSegment() const; // attribute: CodeSegment
};

MemoryParameter_t * make_MemoryParameter_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
