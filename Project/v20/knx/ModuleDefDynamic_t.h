/* This file is generated. */

#pragma once

#include <Project/Base.h>

namespace Project {
namespace v20 {
namespace knx {

/* forward declarations */
class ApplicationProgramChannel_t;
class ChannelIndependentBlock_t;
class ComObjectParameterBlock_t;
class DependentChannelChoose_t;
class Module_t;
class Repeat_t;

class ModuleDefDynamic_t : public Base
{
    Q_OBJECT

public:
    explicit ModuleDefDynamic_t(Base * parent = nullptr);
    virtual ~ModuleDefDynamic_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* elements */
    // xs:choice ChannelIndependentBlock_t * ChannelIndependentBlock{};
    // xs:choice ApplicationProgramChannel_t * Channel{};
    // xs:choice DependentChannelChoose_t * Choose{};
    // xs:choice Module_t * Module{};
    // xs:choice Repeat_t * Repeat{};
    // xs:choice ComObjectParameterBlock_t * ParameterBlock{};
};

ModuleDefDynamic_t * make_ModuleDefDynamic_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
