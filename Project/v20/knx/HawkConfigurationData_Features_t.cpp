/* This file is generated. */

#include <Project/v20/knx/HawkConfigurationData_Features_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/KNX_t.h>

namespace Project {
namespace v20 {
namespace knx {

HawkConfigurationData_Features_t::HawkConfigurationData_Features_t(Base * parent) :
    Base(parent)
{
}

HawkConfigurationData_Features_t::~HawkConfigurationData_Features_t()
{
}

void HawkConfigurationData_Features_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("Feature")) {
            auto * newFeature = make_HawkConfigurationData_Features_Feature_t(this);
            newFeature->read(reader);
            Feature.append(newFeature);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int HawkConfigurationData_Features_t::tableColumnCount() const
{
    return 1;
}

QVariant HawkConfigurationData_Features_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "HawkConfigurationData_Features";
        }
        break;
    }
    return QVariant();
}

QVariant HawkConfigurationData_Features_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant HawkConfigurationData_Features_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "Features";
    case Qt::DecorationRole:
        return QIcon::fromTheme("HawkConfigurationData_Features");
    }
    return QVariant();
}

HawkConfigurationData_Features_t * make_HawkConfigurationData_Features_t(Base * parent)
{
    return new HawkConfigurationData_Features_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
