/* This file is generated. */

#include <Project/v20/knx/ManufacturerData_Manufacturer_ApplicationPrograms_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/KNX_t.h>

namespace Project {
namespace v20 {
namespace knx {

ManufacturerData_Manufacturer_ApplicationPrograms_t::ManufacturerData_Manufacturer_ApplicationPrograms_t(Base * parent) :
    Base(parent)
{
}

ManufacturerData_Manufacturer_ApplicationPrograms_t::~ManufacturerData_Manufacturer_ApplicationPrograms_t()
{
}

void ManufacturerData_Manufacturer_ApplicationPrograms_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("ApplicationProgram")) {
            QString newId = reader.attributes().value("Id").toString();
            Q_ASSERT(!newId.isEmpty());
            ApplicationProgram_t * newApplicationProgram;
            if (ApplicationProgram.contains(newId)) {
                newApplicationProgram = ApplicationProgram[newId];
            } else {
                newApplicationProgram = make_ApplicationProgram_t(this);
                ApplicationProgram[newId] = newApplicationProgram;
            }
            newApplicationProgram->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ManufacturerData_Manufacturer_ApplicationPrograms_t::tableColumnCount() const
{
    return 1;
}

QVariant ManufacturerData_Manufacturer_ApplicationPrograms_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ManufacturerData_Manufacturer_ApplicationPrograms";
        }
        break;
    }
    return QVariant();
}

QVariant ManufacturerData_Manufacturer_ApplicationPrograms_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant ManufacturerData_Manufacturer_ApplicationPrograms_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "ApplicationPrograms";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ManufacturerData_Manufacturer_ApplicationPrograms");
    }
    return QVariant();
}

ManufacturerData_Manufacturer_ApplicationPrograms_t * make_ManufacturerData_Manufacturer_ApplicationPrograms_t(Base * parent)
{
    return new ManufacturerData_Manufacturer_ApplicationPrograms_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
