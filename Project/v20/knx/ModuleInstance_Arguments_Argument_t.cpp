/* This file is generated. */

#include <Project/v20/knx/ModuleInstance_Arguments_Argument_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/KNX_t.h>

namespace Project {
namespace v20 {
namespace knx {

ModuleInstance_Arguments_Argument_t::ModuleInstance_Arguments_Argument_t(Base * parent) :
    Base(parent)
{
}

ModuleInstance_Arguments_Argument_t::~ModuleInstance_Arguments_Argument_t()
{
}

void ModuleInstance_Arguments_Argument_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("RefId")) {
            RefId = attribute.value().toString();
            continue;
        }
        if (name == QString("Value")) {
            Value = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ModuleInstance_Arguments_Argument_t::tableColumnCount() const
{
    return 3;
}

QVariant ModuleInstance_Arguments_Argument_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ModuleInstance_Arguments_Argument";
        }
        if (qualifiedName == QString("RefId")) {
            return RefId;
        }
        if (qualifiedName == QString("Value")) {
            return Value;
        }
        break;
    }
    return QVariant();
}

QVariant ModuleInstance_Arguments_Argument_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "RefId";
            case 2:
                return "Value";
            }
        }
    }
    return QVariant();
}

QVariant ModuleInstance_Arguments_Argument_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "Argument";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ModuleInstance_Arguments_Argument");
    }
    return QVariant();
}

ModuleInstance_Arguments_Argument_t * make_ModuleInstance_Arguments_Argument_t(Base * parent)
{
    return new ModuleInstance_Arguments_Argument_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
