/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/xs/ID.h>

namespace Project {
namespace v20 {
namespace knx {

/* forward declarations */
class ModuleDefStatic_Parameters_Parameter_t;
class ModuleDefStatic_Parameters_Union_t;

class ModuleDefStatic_Parameters_t : public Base
{
    Q_OBJECT

public:
    explicit ModuleDefStatic_Parameters_t(Base * parent = nullptr);
    virtual ~ModuleDefStatic_Parameters_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* elements */
    // xs:choice ModuleDefStatic_Parameters_Parameter_t * Parameter{};
    // xs:choice ModuleDefStatic_Parameters_Union_t * Union{};
};

ModuleDefStatic_Parameters_t * make_ModuleDefStatic_Parameters_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
