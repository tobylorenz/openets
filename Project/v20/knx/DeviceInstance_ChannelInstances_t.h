/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v20/knx/ChannelInstance_t.h>

namespace Project {
namespace v20 {
namespace knx {

class DeviceInstance_ChannelInstances_t : public Base
{
    Q_OBJECT

public:
    explicit DeviceInstance_ChannelInstances_t(Base * parent = nullptr);
    virtual ~DeviceInstance_ChannelInstances_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* elements */
    QMap<xs::ID, ChannelInstance_t *> ChannelInstance; // key: Id
};

DeviceInstance_ChannelInstances_t * make_DeviceInstance_ChannelInstances_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
