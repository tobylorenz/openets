/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v20/knx/Condition_t.h>
#include <Project/xs/Boolean.h>
#include <Project/xs/String.h>

namespace Project {
namespace v20 {
namespace knx {

/* forward declarations */
class Assign_t;
class BinaryDataRef_t;
class Button_t;
class ComObjectParameterBlock_t;
class ComObjectParameterChoose_t;
class ComObjectRefRef_t;
class Module_t;
class ParameterRefRef_t;
class ParameterSeparator_t;
class Rename_t;
class Repeat_t;

class ComObjectParameterChoose_when_t : public Base
{
    Q_OBJECT

public:
    explicit ComObjectParameterChoose_when_t(Base * parent = nullptr);
    virtual ~ComObjectParameterChoose_when_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    Condition_t Test{};
    xs::Boolean Default{"false"};
    xs::String InternalDescription{};

    /* elements */
    // xs:choice ComObjectParameterBlock_t * ParameterBlock{};
    // xs:choice ParameterSeparator_t * ParameterSeparator{};
    // xs:choice ParameterRefRef_t * ParameterRefRef{};
    // xs:choice Button_t * Button{};
    // xs:choice ComObjectParameterChoose_t * Choose{};
    // xs:choice BinaryDataRef_t * BinaryDataRef{};
    // xs:choice ComObjectRefRef_t * ComObjectRefRef{};
    // xs:choice Module_t * Module{};
    // xs:choice Repeat_t * Repeat{};
    // xs:choice Assign_t * Assign{};
    // xs:choice Rename_t * Rename{};
};

ComObjectParameterChoose_when_t * make_ComObjectParameterChoose_when_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
