/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v20/knx/ParameterValidation_t.h>

namespace Project {
namespace v20 {
namespace knx {

class ModuleDefStatic_ParameterValidations_t : public Base
{
    Q_OBJECT

public:
    explicit ModuleDefStatic_ParameterValidations_t(Base * parent = nullptr);
    virtual ~ModuleDefStatic_ParameterValidations_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* elements */
    QMap<xs::ID, ParameterValidation_t *> ParameterValidation; // key: Id
};

ModuleDefStatic_ParameterValidations_t * make_ModuleDefStatic_ParameterValidations_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
