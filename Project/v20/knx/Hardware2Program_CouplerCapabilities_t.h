/* This file is generated. */

#pragma once

#include <Project/v20/knx/CouplerCapability_t.h>
#include <Project/xs/List.h>

namespace Project {
namespace v20 {
namespace knx {

using Hardware2Program_CouplerCapabilities_t = xs::List<CouplerCapability_t>;

} // namespace knx
} // namespace v20
} // namespace Project
