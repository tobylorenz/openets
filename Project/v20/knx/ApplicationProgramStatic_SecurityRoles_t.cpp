/* This file is generated. */

#include <Project/v20/knx/ApplicationProgramStatic_SecurityRoles_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/KNX_t.h>

namespace Project {
namespace v20 {
namespace knx {

ApplicationProgramStatic_SecurityRoles_t::ApplicationProgramStatic_SecurityRoles_t(Base * parent) :
    Base(parent)
{
}

ApplicationProgramStatic_SecurityRoles_t::~ApplicationProgramStatic_SecurityRoles_t()
{
}

void ApplicationProgramStatic_SecurityRoles_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("SecurityRole")) {
            QString newId = reader.attributes().value("Id").toString();
            Q_ASSERT(!newId.isEmpty());
            ApplicationProgramStatic_SecurityRoles_SecurityRole_t * newSecurityRole;
            if (SecurityRole.contains(newId)) {
                newSecurityRole = SecurityRole[newId];
            } else {
                newSecurityRole = make_ApplicationProgramStatic_SecurityRoles_SecurityRole_t(this);
                SecurityRole[newId] = newSecurityRole;
            }
            newSecurityRole->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ApplicationProgramStatic_SecurityRoles_t::tableColumnCount() const
{
    return 1;
}

QVariant ApplicationProgramStatic_SecurityRoles_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ApplicationProgramStatic_SecurityRoles";
        }
        break;
    }
    return QVariant();
}

QVariant ApplicationProgramStatic_SecurityRoles_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant ApplicationProgramStatic_SecurityRoles_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "SecurityRoles";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ApplicationProgramStatic_SecurityRoles");
    }
    return QVariant();
}

ApplicationProgramStatic_SecurityRoles_t * make_ApplicationProgramStatic_SecurityRoles_t(Base * parent)
{
    return new ApplicationProgramStatic_SecurityRoles_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
