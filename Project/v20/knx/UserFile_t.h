/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v20/knx/String255_t.h>
#include <Project/xs/String.h>

namespace Project {
namespace v20 {
namespace knx {

class UserFile_t : public Base
{
    Q_OBJECT

public:
    explicit UserFile_t(Base * parent = nullptr);
    virtual ~UserFile_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    String255_t Filename{};
    xs::String Comment{};
};

UserFile_t * make_UserFile_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
