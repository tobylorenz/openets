/* This file is generated. */

#include <Project/v20/knx/P2PLinks_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/KNX_t.h>

namespace Project {
namespace v20 {
namespace knx {

P2PLinks_t::P2PLinks_t(Base * parent) :
    Base(parent)
{
}

P2PLinks_t::~P2PLinks_t()
{
}

void P2PLinks_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("P2PLink")) {
            QString newId = reader.attributes().value("Id").toString();
            Q_ASSERT(!newId.isEmpty());
            P2PLinks_P2PLink_t * newP2PLink;
            if (P2PLink.contains(newId)) {
                newP2PLink = P2PLink[newId];
            } else {
                newP2PLink = make_P2PLinks_P2PLink_t(this);
                P2PLink[newId] = newP2PLink;
            }
            newP2PLink->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int P2PLinks_t::tableColumnCount() const
{
    return 1;
}

QVariant P2PLinks_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "P2PLinks";
        }
        break;
    }
    return QVariant();
}

QVariant P2PLinks_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant P2PLinks_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "P2PLinks";
    case Qt::DecorationRole:
        return QIcon::fromTheme("P2PLinks");
    }
    return QVariant();
}

P2PLinks_t * make_P2PLinks_t(Base * parent)
{
    return new P2PLinks_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
