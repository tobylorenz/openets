/* This file is generated. */

#include <Project/v20/knx/CalculationParameterRef_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/KNX_t.h>

namespace Project {
namespace v20 {
namespace knx {

CalculationParameterRef_t::CalculationParameterRef_t(Base * parent) :
    Base(parent)
{
}

CalculationParameterRef_t::~CalculationParameterRef_t()
{
}

void CalculationParameterRef_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("RefId")) {
            RefId = attribute.value().toString();
            continue;
        }
        if (name == QString("InternalDescription")) {
            InternalDescription = attribute.value().toString();
            continue;
        }
        if (name == QString("AliasName")) {
            AliasName = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int CalculationParameterRef_t::tableColumnCount() const
{
    return 4;
}

QVariant CalculationParameterRef_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "CalculationParameterRef";
        }
        if (qualifiedName == QString("RefId")) {
            return RefId;
        }
        if (qualifiedName == QString("InternalDescription")) {
            return InternalDescription;
        }
        if (qualifiedName == QString("AliasName")) {
            return AliasName;
        }
        break;
    }
    return QVariant();
}

QVariant CalculationParameterRef_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "RefId";
            case 2:
                return "InternalDescription";
            case 3:
                return "AliasName";
            }
        }
    }
    return QVariant();
}

QVariant CalculationParameterRef_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "CalculationParameterRef";
    case Qt::DecorationRole:
        return QIcon::fromTheme("CalculationParameterRef");
    }
    return QVariant();
}

Base * CalculationParameterRef_t::getCalculationParameter() const
{
    if (RefId.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<Base *>(knx->ids[RefId]);
}

CalculationParameterRef_t * make_CalculationParameterRef_t(Base * parent)
{
    return new CalculationParameterRef_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
