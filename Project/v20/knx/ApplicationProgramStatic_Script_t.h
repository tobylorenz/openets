/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/xs/String.h>

namespace Project {
namespace v20 {
namespace knx {

class ApplicationProgramStatic_Script_t : public Base
{
    Q_OBJECT

public:
    explicit ApplicationProgramStatic_Script_t(Base * parent = nullptr);
    virtual ~ApplicationProgramStatic_Script_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* element text */
    xs::String text{};
};

ApplicationProgramStatic_Script_t * make_ApplicationProgramStatic_Script_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
