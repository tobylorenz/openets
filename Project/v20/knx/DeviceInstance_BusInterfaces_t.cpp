/* This file is generated. */

#include <Project/v20/knx/DeviceInstance_BusInterfaces_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/KNX_t.h>

namespace Project {
namespace v20 {
namespace knx {

DeviceInstance_BusInterfaces_t::DeviceInstance_BusInterfaces_t(Base * parent) :
    Base(parent)
{
}

DeviceInstance_BusInterfaces_t::~DeviceInstance_BusInterfaces_t()
{
}

void DeviceInstance_BusInterfaces_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("BusInterface")) {
            QString newRefId = reader.attributes().value("RefId").toString();
            Q_ASSERT(!newRefId.isEmpty());
            BusInterface_t * newBusInterface;
            if (BusInterface.contains(newRefId)) {
                newBusInterface = BusInterface[newRefId];
            } else {
                newBusInterface = make_BusInterface_t(this);
                BusInterface[newRefId] = newBusInterface;
            }
            newBusInterface->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int DeviceInstance_BusInterfaces_t::tableColumnCount() const
{
    return 1;
}

QVariant DeviceInstance_BusInterfaces_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "DeviceInstance_BusInterfaces";
        }
        break;
    }
    return QVariant();
}

QVariant DeviceInstance_BusInterfaces_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant DeviceInstance_BusInterfaces_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "BusInterfaces";
    case Qt::DecorationRole:
        return QIcon::fromTheme("DeviceInstance_BusInterfaces");
    }
    return QVariant();
}

DeviceInstance_BusInterfaces_t * make_DeviceInstance_BusInterfaces_t(Base * parent)
{
    return new DeviceInstance_BusInterfaces_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
