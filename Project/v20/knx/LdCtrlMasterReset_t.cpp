/* This file is generated. */

#include <Project/v20/knx/LdCtrlMasterReset_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/KNX_t.h>

namespace Project {
namespace v20 {
namespace knx {

LdCtrlMasterReset_t::LdCtrlMasterReset_t(Base * parent) :
    Base(parent)
{
}

LdCtrlMasterReset_t::~LdCtrlMasterReset_t()
{
}

void LdCtrlMasterReset_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("AppliesTo")) {
            AppliesTo = attribute.value().toString();
            continue;
        }
        if (name == QString("InternalDescription")) {
            InternalDescription = attribute.value().toString();
            continue;
        }
        if (name == QString("EraseCode")) {
            EraseCode = attribute.value().toString();
            continue;
        }
        if (name == QString("ChannelNumber")) {
            ChannelNumber = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("OnError")) {
            auto * newOnError = make_LdCtrlBase_OnError_t(this);
            newOnError->read(reader);
            OnError.append(newOnError);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int LdCtrlMasterReset_t::tableColumnCount() const
{
    return 5;
}

QVariant LdCtrlMasterReset_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "LdCtrlMasterReset";
        }
        if (qualifiedName == QString("AppliesTo")) {
            return AppliesTo;
        }
        if (qualifiedName == QString("InternalDescription")) {
            return InternalDescription;
        }
        if (qualifiedName == QString("EraseCode")) {
            return EraseCode;
        }
        if (qualifiedName == QString("ChannelNumber")) {
            return ChannelNumber;
        }
        break;
    }
    return QVariant();
}

QVariant LdCtrlMasterReset_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "AppliesTo";
            case 2:
                return "InternalDescription";
            case 3:
                return "EraseCode";
            case 4:
                return "ChannelNumber";
            }
        }
    }
    return QVariant();
}

QVariant LdCtrlMasterReset_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "LdCtrlMasterReset";
    case Qt::DecorationRole:
        return QIcon::fromTheme("LdCtrlMasterReset");
    }
    return QVariant();
}

LdCtrlMasterReset_t * make_LdCtrlMasterReset_t(Base * parent)
{
    return new LdCtrlMasterReset_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
