/* This file is generated. */

#pragma once

#include <Project/v20/knx/RepeatIndex_t.h>
#include <Project/xs/List.h>

namespace Project {
namespace v20 {
namespace knx {

using ModuleInstance_RepeatIndex_t = xs::List<RepeatIndex_t>;

} // namespace knx
} // namespace v20
} // namespace Project
