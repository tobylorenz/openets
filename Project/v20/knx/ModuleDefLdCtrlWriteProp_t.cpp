/* This file is generated. */

#include <Project/v20/knx/ModuleDefLdCtrlWriteProp_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/KNX_t.h>

namespace Project {
namespace v20 {
namespace knx {

ModuleDefLdCtrlWriteProp_t::ModuleDefLdCtrlWriteProp_t(Base * parent) :
    Base(parent)
{
}

ModuleDefLdCtrlWriteProp_t::~ModuleDefLdCtrlWriteProp_t()
{
}

void ModuleDefLdCtrlWriteProp_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("AppliesTo")) {
            AppliesTo = attribute.value().toString();
            continue;
        }
        if (name == QString("InternalDescription")) {
            InternalDescription = attribute.value().toString();
            continue;
        }
        if (name == QString("ObjIdx")) {
            ObjIdx = attribute.value().toString();
            continue;
        }
        if (name == QString("ObjType")) {
            ObjType = attribute.value().toString();
            continue;
        }
        if (name == QString("Occurrence")) {
            Occurrence = attribute.value().toString();
            continue;
        }
        if (name == QString("PropId")) {
            PropId = attribute.value().toString();
            continue;
        }
        if (name == QString("StartElement")) {
            StartElement = attribute.value().toString();
            continue;
        }
        if (name == QString("Count")) {
            Count = attribute.value().toString();
            continue;
        }
        if (name == QString("Verify")) {
            Verify = attribute.value().toString();
            continue;
        }
        if (name == QString("InlineData")) {
            InlineData = attribute.value().toString();
            continue;
        }
        if (name == QString("BaseObjIdx")) {
            QString value = attribute.value().toString();
            if (!value.isEmpty())
                qWarning() << "knx:ModuleDefLdCtrlWriteProp_t attribute BaseObjIdx references to" << value;
        }
        if (name == QString("BaseObjIdx")) {
            BaseObjIdx = attribute.value().toString();
            continue;
        }
        if (name == QString("BaseOccurrence")) {
            QString value = attribute.value().toString();
            if (!value.isEmpty())
                qWarning() << "knx:ModuleDefLdCtrlWriteProp_t attribute BaseOccurrence references to" << value;
        }
        if (name == QString("BaseOccurrence")) {
            BaseOccurrence = attribute.value().toString();
            continue;
        }
        if (name == QString("BaseStartElement")) {
            QString value = attribute.value().toString();
            if (!value.isEmpty())
                qWarning() << "knx:ModuleDefLdCtrlWriteProp_t attribute BaseStartElement references to" << value;
        }
        if (name == QString("BaseStartElement")) {
            BaseStartElement = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("OnError")) {
            auto * newOnError = make_LdCtrlBase_OnError_t(this);
            newOnError->read(reader);
            OnError.append(newOnError);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ModuleDefLdCtrlWriteProp_t::tableColumnCount() const
{
    return 14;
}

QVariant ModuleDefLdCtrlWriteProp_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ModuleDefLdCtrlWriteProp";
        }
        if (qualifiedName == QString("AppliesTo")) {
            return AppliesTo;
        }
        if (qualifiedName == QString("InternalDescription")) {
            return InternalDescription;
        }
        if (qualifiedName == QString("ObjIdx")) {
            return ObjIdx;
        }
        if (qualifiedName == QString("ObjType")) {
            return ObjType;
        }
        if (qualifiedName == QString("Occurrence")) {
            return Occurrence;
        }
        if (qualifiedName == QString("PropId")) {
            return PropId;
        }
        if (qualifiedName == QString("StartElement")) {
            return StartElement;
        }
        if (qualifiedName == QString("Count")) {
            return Count;
        }
        if (qualifiedName == QString("Verify")) {
            return Verify;
        }
        if (qualifiedName == QString("InlineData")) {
            return InlineData;
        }
        if (qualifiedName == QString("BaseObjIdx")) {
            return BaseObjIdx;
        }
        if (qualifiedName == QString("BaseOccurrence")) {
            return BaseOccurrence;
        }
        if (qualifiedName == QString("BaseStartElement")) {
            return BaseStartElement;
        }
        break;
    }
    return QVariant();
}

QVariant ModuleDefLdCtrlWriteProp_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "AppliesTo";
            case 2:
                return "InternalDescription";
            case 3:
                return "ObjIdx";
            case 4:
                return "ObjType";
            case 5:
                return "Occurrence";
            case 6:
                return "PropId";
            case 7:
                return "StartElement";
            case 8:
                return "Count";
            case 9:
                return "Verify";
            case 10:
                return "InlineData";
            case 11:
                return "BaseObjIdx";
            case 12:
                return "BaseOccurrence";
            case 13:
                return "BaseStartElement";
            }
        }
    }
    return QVariant();
}

QVariant ModuleDefLdCtrlWriteProp_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "ModuleDefLdCtrlWriteProp";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ModuleDefLdCtrlWriteProp");
    }
    return QVariant();
}

Base * ModuleDefLdCtrlWriteProp_t::getBaseObjIdx() const
{
    if (BaseObjIdx.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<Base *>(knx->ids[BaseObjIdx]);
}

Base * ModuleDefLdCtrlWriteProp_t::getBaseOccurrence() const
{
    if (BaseOccurrence.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<Base *>(knx->ids[BaseOccurrence]);
}

Base * ModuleDefLdCtrlWriteProp_t::getBaseStartElement() const
{
    if (BaseStartElement.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<Base *>(knx->ids[BaseStartElement]);
}

ModuleDefLdCtrlWriteProp_t * make_ModuleDefLdCtrlWriteProp_t(Base * parent)
{
    return new ModuleDefLdCtrlWriteProp_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
