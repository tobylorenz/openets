/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v20/knx/ResourceAccessRights_t.h>

namespace Project {
namespace v20 {
namespace knx {

class HawkConfigurationData_Resources_Resource_AccessRights_t : public Base
{
    Q_OBJECT

public:
    explicit HawkConfigurationData_Resources_Resource_AccessRights_t(Base * parent = nullptr);
    virtual ~HawkConfigurationData_Resources_Resource_AccessRights_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    ResourceAccessRights_t Read{};
    ResourceAccessRights_t Write{};
};

HawkConfigurationData_Resources_Resource_AccessRights_t * make_HawkConfigurationData_Resources_Resource_AccessRights_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
