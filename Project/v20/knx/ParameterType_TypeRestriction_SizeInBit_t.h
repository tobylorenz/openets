/* This file is generated. */

#pragma once

#include <Project/xs/UnsignedInt.h>

namespace Project {
namespace v20 {
namespace knx {

using ParameterType_TypeRestriction_SizeInBit_t = xs::UnsignedInt;

} // namespace knx
} // namespace v20
} // namespace Project
