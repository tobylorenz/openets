/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v20/knx/BitOffset_t.h>
#include <Project/v20/knx/IDREF.h>
#include <Project/v20/knx/MemoryUnion_Offset_t.h>

namespace Project {
namespace v20 {
namespace knx {

/* forward declarations */
class ApplicationProgramStatic_Code_AbsoluteSegment_t;

class MemoryUnion_t : public Base
{
    Q_OBJECT

public:
    explicit MemoryUnion_t(Base * parent = nullptr);
    virtual ~MemoryUnion_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    IDREF CodeSegment{};
    MemoryUnion_Offset_t Offset{};
    BitOffset_t BitOffset{};

    /* getters */
    ApplicationProgramStatic_Code_AbsoluteSegment_t * getCodeSegment() const; // attribute: CodeSegment
};

MemoryUnion_t * make_MemoryUnion_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
