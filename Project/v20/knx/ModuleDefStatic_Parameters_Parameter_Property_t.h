/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v20/knx/BitOffset_t.h>
#include <Project/v20/knx/IDREF.h>
#include <Project/xs/UnsignedByte.h>
#include <Project/xs/UnsignedInt.h>
#include <Project/xs/UnsignedShort.h>

namespace Project {
namespace v20 {
namespace knx {

/* forward declarations */
class ModuleDef_Arguments_Argument_t;

class ModuleDefStatic_Parameters_Parameter_Property_t : public Base
{
    Q_OBJECT

public:
    explicit ModuleDefStatic_Parameters_Parameter_Property_t(Base * parent = nullptr);
    virtual ~ModuleDefStatic_Parameters_Parameter_Property_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::UnsignedByte ObjectIndex{};
    xs::UnsignedShort ObjectType{};
    xs::UnsignedShort Occurrence{"0"};
    xs::UnsignedShort PropertyId{};
    xs::UnsignedInt Offset{};
    BitOffset_t BitOffset{};
    IDREF BaseOffset{};
    IDREF BaseIndex{};
    IDREF BaseOccurrence{};

    /* getters */
    ModuleDef_Arguments_Argument_t * getBaseOffset() const; // attribute: BaseOffset
    Base * getBaseIndex() const; // attribute: BaseIndex
    Base * getBaseOccurrence() const; // attribute: BaseOccurrence
};

ModuleDefStatic_Parameters_Parameter_Property_t * make_ModuleDefStatic_Parameters_Parameter_Property_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
