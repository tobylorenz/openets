/* This file is generated. */

#include <Project/v20/knx/DatapointType_DatapointSubtypes_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/KNX_t.h>

namespace Project {
namespace v20 {
namespace knx {

DatapointType_DatapointSubtypes_t::DatapointType_DatapointSubtypes_t(Base * parent) :
    Base(parent)
{
}

DatapointType_DatapointSubtypes_t::~DatapointType_DatapointSubtypes_t()
{
}

void DatapointType_DatapointSubtypes_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("DatapointSubtype")) {
            QString newId = reader.attributes().value("Id").toString();
            Q_ASSERT(!newId.isEmpty());
            DatapointType_DatapointSubtypes_DatapointSubtype_t * newDatapointSubtype;
            if (DatapointSubtype.contains(newId)) {
                newDatapointSubtype = DatapointSubtype[newId];
            } else {
                newDatapointSubtype = make_DatapointType_DatapointSubtypes_DatapointSubtype_t(this);
                DatapointSubtype[newId] = newDatapointSubtype;
            }
            newDatapointSubtype->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int DatapointType_DatapointSubtypes_t::tableColumnCount() const
{
    return 1;
}

QVariant DatapointType_DatapointSubtypes_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "DatapointType_DatapointSubtypes";
        }
        break;
    }
    return QVariant();
}

QVariant DatapointType_DatapointSubtypes_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant DatapointType_DatapointSubtypes_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "DatapointSubtypes";
    case Qt::DecorationRole:
        return QIcon::fromTheme("DatapointType_DatapointSubtypes");
    }
    return QVariant();
}

DatapointType_DatapointSubtypes_t * make_DatapointType_DatapointSubtypes_t(Base * parent)
{
    return new DatapointType_DatapointSubtypes_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
