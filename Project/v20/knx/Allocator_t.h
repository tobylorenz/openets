/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v20/knx/IDREF.h>
#include <Project/v20/knx/String255_t.h>
#include <Project/xs/ID.h>
#include <Project/xs/Long.h>
#include <Project/xs/String.h>

namespace Project {
namespace v20 {
namespace knx {

class Allocator_t : public Base
{
    Q_OBJECT

public:
    explicit Allocator_t(Base * parent = nullptr);
    virtual ~Allocator_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::ID Id{};
    String255_t Name{};
    xs::String InternalDescription{};
    xs::Long Start{};
    xs::Long maxInclusive{};
    IDREF ErrorMessageRef{};

    /* getters */
    Base * getErrorMessage() const; // attribute: ErrorMessageRef
};

Allocator_t * make_Allocator_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
