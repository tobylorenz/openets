/* This file is generated. */

#include <Project/v20/knx/Module_NumericArg_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/KNX_t.h>

namespace Project {
namespace v20 {
namespace knx {

Module_NumericArg_t::Module_NumericArg_t(Base * parent) :
    Base(parent)
{
}

Module_NumericArg_t::~Module_NumericArg_t()
{
}

void Module_NumericArg_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("RefId")) {
            RefId = attribute.value().toString();
            continue;
        }
        if (name == QString("Value")) {
            Value = attribute.value().toString();
            continue;
        }
        if (name == QString("AllocatorRefId")) {
            QString value = attribute.value().toString();
            if (!value.isEmpty())
                qWarning() << "knx:Module_NumericArg_t attribute AllocatorRefId references to" << value;
        }
        if (name == QString("AllocatorRefId")) {
            AllocatorRefId = attribute.value().toString();
            continue;
        }
        if (name == QString("BaseValue")) {
            QString value = attribute.value().toString();
            if (!value.isEmpty())
                qWarning() << "knx:Module_NumericArg_t attribute BaseValue references to" << value;
        }
        if (name == QString("BaseValue")) {
            BaseValue = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int Module_NumericArg_t::tableColumnCount() const
{
    return 5;
}

QVariant Module_NumericArg_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "Module_NumericArg";
        }
        if (qualifiedName == QString("RefId")) {
            return RefId;
        }
        if (qualifiedName == QString("Value")) {
            return Value;
        }
        if (qualifiedName == QString("AllocatorRefId")) {
            return AllocatorRefId;
        }
        if (qualifiedName == QString("BaseValue")) {
            return BaseValue;
        }
        break;
    }
    return QVariant();
}

QVariant Module_NumericArg_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "RefId";
            case 2:
                return "Value";
            case 3:
                return "AllocatorRefId";
            case 4:
                return "BaseValue";
            }
        }
    }
    return QVariant();
}

QVariant Module_NumericArg_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "NumericArg";
    case Qt::DecorationRole:
        return QIcon::fromTheme("Module_NumericArg");
    }
    return QVariant();
}

Base * Module_NumericArg_t::getAllocator() const
{
    if (AllocatorRefId.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<Base *>(knx->ids[AllocatorRefId]);
}

Base * Module_NumericArg_t::getBaseValue() const
{
    if (BaseValue.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<Base *>(knx->ids[BaseValue]);
}

Module_NumericArg_t * make_Module_NumericArg_t(Base * parent)
{
    return new Module_NumericArg_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
