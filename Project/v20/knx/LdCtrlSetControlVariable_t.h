/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v20/knx/LdCtrlBase_OnError_t.h>
#include <Project/v20/knx/LdCtrlControlVariable_t.h>
#include <Project/v20/knx/LdCtrlProcType_t.h>
#include <Project/xs/Boolean.h>
#include <Project/xs/String.h>

namespace Project {
namespace v20 {
namespace knx {

class LdCtrlSetControlVariable_t : public Base
{
    Q_OBJECT

public:
    explicit LdCtrlSetControlVariable_t(Base * parent = nullptr);
    virtual ~LdCtrlSetControlVariable_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    LdCtrlProcType_t AppliesTo{"auto"};
    xs::String InternalDescription{};
    LdCtrlControlVariable_t Name{};
    xs::Boolean Value{};

    /* elements */
    QVector<LdCtrlBase_OnError_t *> OnError;
};

LdCtrlSetControlVariable_t * make_LdCtrlSetControlVariable_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
