/* This file is generated. */

#include <Project/v20/knx/DeviceCertificate_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/KNX_t.h>

namespace Project {
namespace v20 {
namespace knx {

DeviceCertificate_t::DeviceCertificate_t(Base * parent) :
    Base(parent)
{
}

DeviceCertificate_t::~DeviceCertificate_t()
{
}

void DeviceCertificate_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("SerialNumber")) {
            SerialNumber = attribute.value().toString();
            continue;
        }
        if (name == QString("FDSK")) {
            FDSK = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int DeviceCertificate_t::tableColumnCount() const
{
    return 3;
}

QVariant DeviceCertificate_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "DeviceCertificate";
        }
        if (qualifiedName == QString("SerialNumber")) {
            return SerialNumber;
        }
        if (qualifiedName == QString("FDSK")) {
            return FDSK;
        }
        break;
    }
    return QVariant();
}

QVariant DeviceCertificate_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "SerialNumber";
            case 2:
                return "FDSK";
            }
        }
    }
    return QVariant();
}

QVariant DeviceCertificate_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "DeviceCertificate";
    case Qt::DecorationRole:
        return QIcon::fromTheme("DeviceCertificate");
    }
    return QVariant();
}

DeviceCertificate_t * make_DeviceCertificate_t(Base * parent)
{
    return new DeviceCertificate_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
