/* This file is generated. */

#include <Project/v20/knx/AddinData_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/KNX_t.h>

namespace Project {
namespace v20 {
namespace knx {

AddinData_t::AddinData_t(Base * parent) :
    Base(parent)
{
}

AddinData_t::~AddinData_t()
{
}

void AddinData_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("AddinId")) {
            AddinId = attribute.value().toString();
            continue;
        }
        if (name == QString("Name")) {
            Name = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int AddinData_t::tableColumnCount() const
{
    return 3;
}

QVariant AddinData_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "AddinData";
        }
        if (qualifiedName == QString("AddinId")) {
            return AddinId;
        }
        if (qualifiedName == QString("Name")) {
            return Name;
        }
        break;
    }
    return QVariant();
}

QVariant AddinData_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "AddinId";
            case 2:
                return "Name";
            }
        }
    }
    return QVariant();
}

QVariant AddinData_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return !Name.isEmpty() ? Name : "AddinData";
    case Qt::DecorationRole:
        return QIcon::fromTheme("AddinData");
    }
    return QVariant();
}

AddinData_t * make_AddinData_t(Base * parent)
{
    return new AddinData_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
