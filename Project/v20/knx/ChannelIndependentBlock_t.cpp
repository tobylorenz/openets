/* This file is generated. */

#include <Project/v20/knx/ChannelIndependentBlock_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/BinaryDataRef_t.h>
#include <Project/v20/knx/ChannelChoose_t.h>
#include <Project/v20/knx/ComObjectParameterBlock_t.h>
#include <Project/v20/knx/ComObjectRefRef_t.h>
#include <Project/v20/knx/KNX_t.h>
#include <Project/v20/knx/Module_t.h>
#include <Project/v20/knx/Repeat_t.h>

namespace Project {
namespace v20 {
namespace knx {

ChannelIndependentBlock_t::ChannelIndependentBlock_t(Base * parent) :
    Base(parent)
{
}

ChannelIndependentBlock_t::~ChannelIndependentBlock_t()
{
}

void ChannelIndependentBlock_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("ParameterBlock")) {
            auto * ParameterBlock = make_ComObjectParameterBlock_t(this);
            ParameterBlock->read(reader);
            continue;
        }
        if (reader.name() == QString("choose")) {
            auto * Choose = make_ChannelChoose_t(this);
            Choose->read(reader);
            continue;
        }
        if (reader.name() == QString("BinaryDataRef")) {
            auto * BinaryDataRef = make_BinaryDataRef_t(this);
            BinaryDataRef->read(reader);
            continue;
        }
        if (reader.name() == QString("ComObjectRefRef")) {
            auto * ComObjectRefRef = make_ComObjectRefRef_t(this);
            ComObjectRefRef->read(reader);
            continue;
        }
        if (reader.name() == QString("Module")) {
            auto * Module = make_Module_t(this);
            Module->read(reader);
            continue;
        }
        if (reader.name() == QString("Repeat")) {
            auto * Repeat = make_Repeat_t(this);
            Repeat->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ChannelIndependentBlock_t::tableColumnCount() const
{
    return 1;
}

QVariant ChannelIndependentBlock_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ChannelIndependentBlock";
        }
        break;
    }
    return QVariant();
}

QVariant ChannelIndependentBlock_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant ChannelIndependentBlock_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "ChannelIndependentBlock";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ChannelIndependentBlock");
    }
    return QVariant();
}

ChannelIndependentBlock_t * make_ChannelIndependentBlock_t(Base * parent)
{
    return new ChannelIndependentBlock_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
