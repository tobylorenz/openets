/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v20/knx/Node_t.h>

namespace Project {
namespace v20 {
namespace knx {

class DeviceInstance_GroupObjectTree_Nodes_t : public Base
{
    Q_OBJECT

public:
    explicit DeviceInstance_GroupObjectTree_Nodes_t(Base * parent = nullptr);
    virtual ~DeviceInstance_GroupObjectTree_Nodes_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* elements */
    QMap<RELIDREF, Node_t *> Node; // key: RefId
};

DeviceInstance_GroupObjectTree_Nodes_t * make_DeviceInstance_GroupObjectTree_Nodes_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
