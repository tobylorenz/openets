/* This file is generated. */

#pragma once

#include <Project/v20/knx/ResourceMgmtStyle_t.h>
#include <Project/xs/List.h>

namespace Project {
namespace v20 {
namespace knx {

using HawkConfigurationData_Resources_Resource_MgmtStyle_t = xs::List<ResourceMgmtStyle_t>;

} // namespace knx
} // namespace v20
} // namespace Project
