/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v20/knx/DeviceInstance_RfFastAckSlots_Slot_Number_t.h>
#include <Project/v20/knx/IDREF.h>

namespace Project {
namespace v20 {
namespace knx {

class DeviceInstance_RfFastAckSlots_Slot_t : public Base
{
    Q_OBJECT

public:
    explicit DeviceInstance_RfFastAckSlots_Slot_t(Base * parent = nullptr);
    virtual ~DeviceInstance_RfFastAckSlots_Slot_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    IDREF GroupAddressRefId{};
    DeviceInstance_RfFastAckSlots_Slot_Number_t Number{};

    /* getters */
    Base * getGroupAddress() const; // attribute: GroupAddressRefId
};

DeviceInstance_RfFastAckSlots_Slot_t * make_DeviceInstance_RfFastAckSlots_Slot_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
