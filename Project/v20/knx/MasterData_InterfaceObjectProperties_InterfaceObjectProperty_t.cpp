/* This file is generated. */

#include <Project/v20/knx/MasterData_InterfaceObjectProperties_InterfaceObjectProperty_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/DatapointType_DatapointSubtypes_DatapointSubtype_t.h>
#include <Project/v20/knx/KNX_t.h>
#include <Project/v20/knx/MasterData_InterfaceObjectTypes_InterfaceObjectType_t.h>

namespace Project {
namespace v20 {
namespace knx {

MasterData_InterfaceObjectProperties_InterfaceObjectProperty_t::MasterData_InterfaceObjectProperties_InterfaceObjectProperty_t(Base * parent) :
    Base(parent)
{
}

MasterData_InterfaceObjectProperties_InterfaceObjectProperty_t::~MasterData_InterfaceObjectProperties_InterfaceObjectProperty_t()
{
}

void MasterData_InterfaceObjectProperties_InterfaceObjectProperty_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == "Id") {
            Id = attribute.value().toString();
            KNX_t * knx = findParent<KNX_t *>();
            Q_ASSERT(knx);
            knx->ids[Id] = this;
            continue;
        }
        if (name == QString("Number")) {
            Number = attribute.value().toString();
            continue;
        }
        if (name == QString("ObjectType")) {
            ObjectType = attribute.value().toString();
            continue;
        }
        if (name == QString("Name")) {
            Name = attribute.value().toString();
            continue;
        }
        if (name == QString("Text")) {
            Text = attribute.value().toString();
            continue;
        }
        if (name == QString("PDT")) {
            PDT = attribute.value().toString().split(' ');
            continue;
        }
        if (name == QString("DPT")) {
            DPT = attribute.value().toString();
            continue;
        }
        if (name == QString("Array")) {
            Array = attribute.value().toString();
            continue;
        }
        if (name == QString("AccessPolicy")) {
            AccessPolicy = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int MasterData_InterfaceObjectProperties_InterfaceObjectProperty_t::tableColumnCount() const
{
    return 10;
}

QVariant MasterData_InterfaceObjectProperties_InterfaceObjectProperty_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "MasterData_InterfaceObjectProperties_InterfaceObjectProperty";
        }
        if (qualifiedName == QString("Id")) {
            return Id;
        }
        if (qualifiedName == QString("Number")) {
            return Number;
        }
        if (qualifiedName == QString("ObjectType")) {
            return ObjectType;
        }
        if (qualifiedName == QString("Name")) {
            return Name;
        }
        if (qualifiedName == QString("Text")) {
            return Text;
        }
        if (qualifiedName == QString("PDT")) {
            return PDT.join(' ');
        }
        if (qualifiedName == QString("DPT")) {
            return DPT;
        }
        if (qualifiedName == QString("Array")) {
            return Array;
        }
        if (qualifiedName == QString("AccessPolicy")) {
            return AccessPolicy;
        }
        break;
    }
    return QVariant();
}

QVariant MasterData_InterfaceObjectProperties_InterfaceObjectProperty_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Id";
            case 2:
                return "Number";
            case 3:
                return "ObjectType";
            case 4:
                return "Name";
            case 5:
                return "Text";
            case 6:
                return "PDT";
            case 7:
                return "DPT";
            case 8:
                return "Array";
            case 9:
                return "AccessPolicy";
            }
        }
    }
    return QVariant();
}

QVariant MasterData_InterfaceObjectProperties_InterfaceObjectProperty_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return QString("%1: %2").arg(Id).arg(Name);
    case Qt::DecorationRole:
        return QIcon::fromTheme("MasterData_InterfaceObjectProperties_InterfaceObjectProperty");
    }
    return QVariant();
}

MasterData_InterfaceObjectTypes_InterfaceObjectType_t * MasterData_InterfaceObjectProperties_InterfaceObjectProperty_t::getInterfaceObjectType() const
{
    if (ObjectType.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<MasterData_InterfaceObjectTypes_InterfaceObjectType_t *>(knx->ids[ObjectType]);
}

QList<MasterData_PropertyDataTypes_PropertyDataType_t *> MasterData_InterfaceObjectProperties_InterfaceObjectProperty_t::getPropertyDataType() const
{
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    QList<MasterData_PropertyDataTypes_PropertyDataType_t *> retVal;
    for (QString id : PDT) {
        retVal << qobject_cast<MasterData_PropertyDataTypes_PropertyDataType_t *>(knx->ids[id]);
    }
    return retVal;
}

DatapointType_DatapointSubtypes_DatapointSubtype_t * MasterData_InterfaceObjectProperties_InterfaceObjectProperty_t::getDatapointType() const
{
    if (DPT.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<DatapointType_DatapointSubtypes_DatapointSubtype_t *>(knx->ids[DPT]);
}

MasterData_InterfaceObjectProperties_InterfaceObjectProperty_t * make_MasterData_InterfaceObjectProperties_InterfaceObjectProperty_t(Base * parent)
{
    return new MasterData_InterfaceObjectProperties_InterfaceObjectProperty_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
