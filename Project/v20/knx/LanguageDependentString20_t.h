/* This file is generated. */

#pragma once

#include <Project/v20/knx/LanguageDependentString_t.h>

namespace Project {
namespace v20 {
namespace knx {

using LanguageDependentString20_t = LanguageDependentString_t;

} // namespace knx
} // namespace v20
} // namespace Project
