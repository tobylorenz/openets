/* This file is generated. */

#include <Project/v20/knx/DependentChannelChoose_when_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/ApplicationProgramChannel_t.h>
#include <Project/v20/knx/DependentChannelChoose_t.h>
#include <Project/v20/knx/KNX_t.h>
#include <Project/v20/knx/Module_t.h>
#include <Project/v20/knx/Rename_t.h>
#include <Project/v20/knx/Repeat_t.h>

namespace Project {
namespace v20 {
namespace knx {

DependentChannelChoose_when_t::DependentChannelChoose_when_t(Base * parent) :
    Base(parent)
{
}

DependentChannelChoose_when_t::~DependentChannelChoose_when_t()
{
}

void DependentChannelChoose_when_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("test")) {
            Test = attribute.value().toString();
            continue;
        }
        if (name == QString("default")) {
            Default = attribute.value().toString();
            continue;
        }
        if (name == QString("InternalDescription")) {
            InternalDescription = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("Channel")) {
            auto * Channel = make_ApplicationProgramChannel_t(this);
            Channel->read(reader);
            continue;
        }
        if (reader.name() == QString("choose")) {
            auto * Choose = make_DependentChannelChoose_t(this);
            Choose->read(reader);
            continue;
        }
        if (reader.name() == QString("Rename")) {
            auto * Rename = make_Rename_t(this);
            Rename->read(reader);
            continue;
        }
        if (reader.name() == QString("Module")) {
            auto * Module = make_Module_t(this);
            Module->read(reader);
            continue;
        }
        if (reader.name() == QString("Repeat")) {
            auto * Repeat = make_Repeat_t(this);
            Repeat->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int DependentChannelChoose_when_t::tableColumnCount() const
{
    return 4;
}

QVariant DependentChannelChoose_when_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "DependentChannelChoose_when";
        }
        if (qualifiedName == QString("test")) {
            return Test;
        }
        if (qualifiedName == QString("default")) {
            return Default;
        }
        if (qualifiedName == QString("InternalDescription")) {
            return InternalDescription;
        }
        break;
    }
    return QVariant();
}

QVariant DependentChannelChoose_when_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Test";
            case 2:
                return "Default";
            case 3:
                return "InternalDescription";
            }
        }
    }
    return QVariant();
}

QVariant DependentChannelChoose_when_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "when";
    case Qt::DecorationRole:
        return QIcon::fromTheme("DependentChannelChoose_when");
    }
    return QVariant();
}

DependentChannelChoose_when_t * make_DependentChannelChoose_when_t(Base * parent)
{
    return new DependentChannelChoose_when_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
