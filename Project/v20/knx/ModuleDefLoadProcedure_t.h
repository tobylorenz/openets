/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/xs/UnsignedByte.h>

namespace Project {
namespace v20 {
namespace knx {

/* forward declarations */
class LdCtrlDeclarePropDesc_t;
class LdCtrlDelay_t;
class LdCtrlMerge_t;
class LdCtrlProgressText_t;
class ModuleDefLdCtrlBaseChoose_t;
class ModuleDefLdCtrlCompareProp_t;
class ModuleDefLdCtrlInvokeFunctionProp_t;
class ModuleDefLdCtrlReadFunctionProp_t;
class ModuleDefLdCtrlWriteProp_t;

class ModuleDefLoadProcedure_t : public Base
{
    Q_OBJECT

public:
    explicit ModuleDefLoadProcedure_t(Base * parent = nullptr);
    virtual ~ModuleDefLoadProcedure_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::UnsignedByte MergeId{};

    /* elements */
    // xs:choice ModuleDefLdCtrlWriteProp_t * LdCtrlWriteProp{};
    // xs:choice ModuleDefLdCtrlCompareProp_t * LdCtrlCompareProp{};
    // xs:choice ModuleDefLdCtrlInvokeFunctionProp_t * LdCtrlInvokeFunctionProp{};
    // xs:choice ModuleDefLdCtrlReadFunctionProp_t * LdCtrlReadFunctionProp{};
    // xs:choice LdCtrlDelay_t * LdCtrlDelay{};
    // xs:choice LdCtrlProgressText_t * LdCtrlProgressText{};
    // xs:choice LdCtrlDeclarePropDesc_t * LdCtrlDeclarePropDesc{};
    // xs:choice LdCtrlMerge_t * LdCtrlMerge{};
    // xs:choice ModuleDefLdCtrlBaseChoose_t * Choose{};
};

ModuleDefLoadProcedure_t * make_ModuleDefLoadProcedure_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
