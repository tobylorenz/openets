/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v20/knx/IDREF.h>
#include <Project/v20/knx/String50_t.h>
#include <Project/xs/String.h>

namespace Project {
namespace v20 {
namespace knx {

class CalculationParameterRef_t : public Base
{
    Q_OBJECT

public:
    explicit CalculationParameterRef_t(Base * parent = nullptr);
    virtual ~CalculationParameterRef_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    IDREF RefId{};
    xs::String InternalDescription{};
    String50_t AliasName{};

    /* getters */
    Base * getCalculationParameter() const; // attribute: RefId
};

CalculationParameterRef_t * make_CalculationParameterRef_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
