/* This file is generated. */

#include <Project/v20/knx/ModuleDefLdCtrlBaseChoose_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v20/knx/KNX_t.h>

namespace Project {
namespace v20 {
namespace knx {

ModuleDefLdCtrlBaseChoose_t::ModuleDefLdCtrlBaseChoose_t(Base * parent) :
    Base(parent)
{
}

ModuleDefLdCtrlBaseChoose_t::~ModuleDefLdCtrlBaseChoose_t()
{
}

void ModuleDefLdCtrlBaseChoose_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("ParamRefId")) {
            QString value = attribute.value().toString();
            if (!value.isEmpty())
                qWarning() << "knx:ModuleDefLdCtrlBaseChoose_t attribute ParamRefId references to" << value;
        }
        if (name == QString("ParamRefId")) {
            ParamRefId = attribute.value().toString();
            continue;
        }
        if (name == QString("InternalDescription")) {
            InternalDescription = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("when")) {
            auto * newWhen = make_ModuleDefLdCtrlBaseChoose_when_t(this);
            newWhen->read(reader);
            When.append(newWhen);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ModuleDefLdCtrlBaseChoose_t::tableColumnCount() const
{
    return 3;
}

QVariant ModuleDefLdCtrlBaseChoose_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ModuleDefLdCtrlBaseChoose";
        }
        if (qualifiedName == QString("ParamRefId")) {
            return ParamRefId;
        }
        if (qualifiedName == QString("InternalDescription")) {
            return InternalDescription;
        }
        break;
    }
    return QVariant();
}

QVariant ModuleDefLdCtrlBaseChoose_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "ParamRefId";
            case 2:
                return "InternalDescription";
            }
        }
    }
    return QVariant();
}

QVariant ModuleDefLdCtrlBaseChoose_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "ModuleDefLdCtrlBaseChoose";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ModuleDefLdCtrlBaseChoose");
    }
    return QVariant();
}

Base * ModuleDefLdCtrlBaseChoose_t::getParameter() const
{
    if (ParamRefId.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<Base *>(knx->ids[ParamRefId]);
}

ModuleDefLdCtrlBaseChoose_t * make_ModuleDefLdCtrlBaseChoose_t(Base * parent)
{
    return new ModuleDefLdCtrlBaseChoose_t(parent);
}

} // namespace knx
} // namespace v20
} // namespace Project
