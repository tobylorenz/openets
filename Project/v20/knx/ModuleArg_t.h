/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v20/knx/IDREF.h>

namespace Project {
namespace v20 {
namespace knx {

class ModuleArg_t : public Base
{
    Q_OBJECT

public:
    explicit ModuleArg_t(Base * parent = nullptr);
    virtual ~ModuleArg_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    IDREF RefId{};

    /* getters */
    Base * getRefId() const; // attribute: RefId
};

ModuleArg_t * make_ModuleArg_t(Base * parent);

} // namespace knx
} // namespace v20
} // namespace Project
