#pragma once

#include <QMdiArea>
#include <QMenu>

#include <Project/v20/knx/DeviceInstance_t.h>

/* actions */
QList<QAction *> getActions(QMdiArea * mdiArea, Project::v20::knx::DeviceInstance_t * deviceInstance);

/* context menus */
QMenu * getCustomContextMenu(QMdiArea * mdiArea, Project::v20::knx::DeviceInstance_t * deviceInstance);
