#pragma once

#include <QWidget>

#include <Project/v20/knx/ApplicationProgramChannel_t.h>
#include <Project/v20/knx/ApplicationProgramDynamic_t.h>
#include <Project/v20/knx/ApplicationProgramStatic_ParameterRefs_t.h>
#include <Project/v20/knx/ApplicationProgramStatic_Parameters_Parameter_t.h>
#include <Project/v20/knx/Assign_t.h>
#include <Project/v20/knx/BinaryDataRef_t.h>
#include <Project/v20/knx/Button_t.h>
#include <Project/v20/knx/ChannelChoose_t.h>
#include <Project/v20/knx/ChannelIndependentBlock_t.h>
#include <Project/v20/knx/ComObjectParameterBlock_t.h>
#include <Project/v20/knx/ComObjectParameterChoose_t.h>
#include <Project/v20/knx/ComObjectRefRef_t.h>
#include <Project/v20/knx/DependentChannelChoose_t.h>
#include <Project/v20/knx/DeviceInstance_t.h>
#include <Project/v20/knx/Module_NumericArg_t.h>
#include <Project/v20/knx/Module_TextArg_t.h>
#include <Project/v20/knx/Module_t.h>
#include <Project/v20/knx/ParameterRefRef_t.h>
#include <Project/v20/knx/ParameterRef_t.h>
#include <Project/v20/knx/ParameterSeparator_t.h>
#include <Project/v20/knx/Rename_t.h>
#include <Project/v20/knx/Repeat_t.h>

/* common parameter widgets */
QWidget * parameterWidget(Project::v20::knx::ApplicationProgramChannel_t * applicationProgramChannel, Project::v20::knx::DeviceInstance_t * deviceInstance);
QWidget * parameterWidget(Project::v20::knx::ApplicationProgramDynamic_t * applicationProgramDynamic, Project::v20::knx::DeviceInstance_t * deviceInstance);
QWidget * parameterWidget(Project::v20::knx::ApplicationProgramStatic_ParameterRefs_t * parameterRefs, Project::v20::knx::DeviceInstance_t * deviceInstance);
QWidget * parameterWidget(Project::v20::knx::Assign_t * assign, Project::v20::knx::DeviceInstance_t * deviceInstance);
QWidget * parameterWidget(Project::v20::knx::BinaryDataRef_t * binaryDataRef, Project::v20::knx::DeviceInstance_t * deviceInstance);
QWidget * parameterWidget(Project::v20::knx::Button_t * button, Project::v20::knx::DeviceInstance_t * deviceInstance);
QWidget * parameterWidget(Project::v20::knx::ChannelChoose_t * channelChoose, Project::v20::knx::DeviceInstance_t * deviceInstance);
QWidget * parameterWidget(Project::v20::knx::ChannelIndependentBlock_t * channelIndependentBlock, Project::v20::knx::DeviceInstance_t * deviceInstance);
QWidget * parameterWidget(Project::v20::knx::ComObjectParameterBlock_t * comObjectParameterBlock, Project::v20::knx::DeviceInstance_t * deviceInstance);
QWidget * parameterWidget(Project::v20::knx::ComObjectParameterChoose_t * comObjectParameterChoose, Project::v20::knx::DeviceInstance_t * deviceInstance);
QWidget * parameterWidget(Project::v20::knx::ComObjectRefRef_t * comObjectRefRef, Project::v20::knx::DeviceInstance_t * deviceInstance);
QWidget * parameterWidget(Project::v20::knx::DependentChannelChoose_t * dependentChannelChoose, Project::v20::knx::DeviceInstance_t * deviceInstance);
QWidget * parameterWidget(Project::v20::knx::Module_NumericArg_t * numericArg, Project::v20::knx::DeviceInstance_t * deviceInstance);
QWidget * parameterWidget(Project::v20::knx::Module_TextArg_t * textArg, Project::v20::knx::DeviceInstance_t * deviceInstance);
QWidget * parameterWidget(Project::v20::knx::Module_t * module, Project::v20::knx::DeviceInstance_t * deviceInstance);
QWidget * parameterWidget(Project::v20::knx::ParameterRefRef_t * parameterRefRef, Project::v20::knx::DeviceInstance_t * deviceInstance);
QWidget * parameterWidget(Project::v20::knx::ParameterRef_t * parameterRef, Project::v20::knx::DeviceInstance_t * deviceInstance);
QWidget * parameterWidget(Project::v20::knx::ParameterSeparator_t * parameterSeparator, Project::v20::knx::DeviceInstance_t * deviceInstance);
QWidget * parameterWidget(Project::v20::knx::Rename_t * rename, Project::v20::knx::DeviceInstance_t * deviceInstance);
QWidget * parameterWidget(Project::v20::knx::Repeat_t * repeat, Project::v20::knx::DeviceInstance_t * deviceInstance);
