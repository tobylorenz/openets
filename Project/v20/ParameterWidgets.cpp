﻿#include "ParameterWidgets.h"

#include <QBoxLayout>
#include <QComboBox>
#include <QDebug>
#include <QFrame>
#include <QGroupBox>
#include <QLabel>
#include <QLineEdit>
#include <QRadioButton>
#include <QRegularExpression>
#include <QRegularExpressionValidator>
#include <QSpinBox>
#include <QTabWidget>

#include <Project/Base.h>
#include <Project/ProjectBundle.h>
#include <Project/v20/Helpers.h>
#include <Project/v20/knx/ChannelChoose_when_t.h>
#include <Project/v20/knx/ComObjectParameterChoose_when_t.h>
#include <Project/v20/knx/ComObjectRef_t.h>
#include <Project/v20/knx/ComObject_t.h>
#include <Project/v20/knx/DeviceInstance_ComObjectInstanceRefs_t.h>
#include <Project/v20/knx/DeviceInstance_ParameterInstanceRefs_t.h>
#include <Project/v20/knx/ManufacturerData_Manufacturer_Baggages_Baggage_t.h>
#include <Project/v20/knx/ManufacturerData_Manufacturer_t.h>
#include <Project/v20/knx/MemoryParameter_t.h>
#include <Project/v20/knx/ParameterInstanceRef_t.h>
#include <Project/v20/knx/ParameterType_TypeColor_t.h>
#include <Project/v20/knx/ParameterType_TypeDate_t.h>
#include <Project/v20/knx/ParameterType_TypeFloat_t.h>
#include <Project/v20/knx/ParameterType_TypeIPAddress_t.h>
#include <Project/v20/knx/ParameterType_TypeNone_t.h>
#include <Project/v20/knx/ParameterType_TypeNumber_t.h>
#include <Project/v20/knx/ParameterType_TypePicture_t.h>
#include <Project/v20/knx/ParameterType_TypeRawData_t.h>
#include <Project/v20/knx/ParameterType_TypeRestriction_Enumeration_t.h>
#include <Project/v20/knx/ParameterType_TypeRestriction_t.h>
#include <Project/v20/knx/ParameterType_TypeText_t.h>
#include <Project/v20/knx/ParameterType_TypeTime_t.h>
#include <Project/v20/knx/ParameterType_t.h>
#include <Project/v20/knx/PropertyParameter_t.h>
#include <Project/v20/knx/UnionParameter_t.h>

QWidget * parameterWidget(Project::v20::knx::ApplicationProgramChannel_t * applicationProgramChannel, Project::v20::knx::DeviceInstance_t * deviceInstance)
{
    Q_ASSERT(applicationProgramChannel);
    Q_ASSERT(deviceInstance);

    /* ManufacturerData */
    //Project::v20::knx::ParameterRef_t * textParameterRef = applicationProgramChannel->getTextParameterRef();    Q_ASSERT(text)

    /* Widget / Layout */
    QGroupBox * mainWidget = new QGroupBox();
    Q_ASSERT(mainWidget);
    mainWidget->setTitle("Channel " + applicationProgramChannel->Name + " (" + applicationProgramChannel->Number + ")");
    QVBoxLayout * mainLayout = new QVBoxLayout();
    Q_ASSERT(mainLayout);
    mainWidget->setLayout(mainLayout);
    mainWidget->setToolTip("ApplicationProgramChannel " + applicationProgramChannel->Id);

    /* Content */
    for (QObject * base : applicationProgramChannel->children()) {
        QWidget * subWidget{nullptr};

        Project::v20::knx::ComObjectParameterBlock_t * comObjectParameterBlock = qobject_cast<Project::v20::knx::ComObjectParameterBlock_t *>(base);
        if (comObjectParameterBlock) {
            subWidget = parameterWidget(comObjectParameterBlock, deviceInstance);
            Q_ASSERT(subWidget);
        }

        Project::v20::knx::ComObjectRefRef_t * comObjectRefRef = qobject_cast<Project::v20::knx::ComObjectRefRef_t *>(base);
        if (comObjectRefRef) {
            subWidget = parameterWidget(comObjectRefRef, deviceInstance);
            Q_ASSERT(subWidget);
        }

        Project::v20::knx::BinaryDataRef_t * binaryDataRef = qobject_cast<Project::v20::knx::BinaryDataRef_t *>(base);
        if (binaryDataRef) {
            subWidget = parameterWidget(binaryDataRef, deviceInstance);
            Q_ASSERT(subWidget);
        }

        Project::v20::knx::Module_t * module = qobject_cast<Project::v20::knx::Module_t *>(base);
        if (module) {
            subWidget = parameterWidget(module, deviceInstance);
            Q_ASSERT(subWidget);
        }

        Project::v20::knx::Repeat_t * repeat = qobject_cast<Project::v20::knx::Repeat_t *>(base);
        if (repeat) {
            subWidget = parameterWidget(repeat, deviceInstance);
            Q_ASSERT(subWidget);
        }

        Project::v20::knx::ChannelChoose_t * channelChoose = qobject_cast<Project::v20::knx::ChannelChoose_t *>(base);
        if (channelChoose) {
            subWidget = parameterWidget(channelChoose, deviceInstance);
            Q_ASSERT(subWidget);
        }

        Q_ASSERT(comObjectParameterBlock || comObjectRefRef || binaryDataRef || module || repeat || channelChoose);

        if (subWidget) {
            mainLayout->addWidget(subWidget);
        }
    }

    return mainWidget;
}

QWidget * parameterWidget(Project::v20::knx::ApplicationProgramDynamic_t * applicationProgramDynamic, Project::v20::knx::DeviceInstance_t * deviceInstance)
{
    Q_ASSERT(applicationProgramDynamic);
    Q_ASSERT(deviceInstance);

    /* Widget / Layout */
    QWidget * mainWidget = new QFrame();
    Q_ASSERT(mainWidget);
    QVBoxLayout * mainLayout = new QVBoxLayout();
    Q_ASSERT(mainLayout);
    mainWidget->setLayout(mainLayout);

    /* Content */
    for (QObject * base : applicationProgramDynamic->children()) {
        QWidget * subWidget{nullptr};

        Project::v20::knx::ChannelIndependentBlock_t * channelIndependentBlock = qobject_cast<Project::v20::knx::ChannelIndependentBlock_t *>(base);
        if (channelIndependentBlock) {
            subWidget = parameterWidget(channelIndependentBlock, deviceInstance);
            Q_ASSERT(subWidget);
        }

        Project::v20::knx::ApplicationProgramChannel_t * applicationProgramChannel = qobject_cast<Project::v20::knx::ApplicationProgramChannel_t *>(base);
        if (applicationProgramChannel) {
            subWidget = parameterWidget(applicationProgramChannel, deviceInstance);
            Q_ASSERT(subWidget);
        }

        Project::v20::knx::DependentChannelChoose_t * dependentChannelChoose = qobject_cast<Project::v20::knx::DependentChannelChoose_t *>(base);
        if (dependentChannelChoose) {
            subWidget = parameterWidget(dependentChannelChoose, deviceInstance);
            Q_ASSERT(subWidget);
        }

        Project::v20::knx::Module_t * module = qobject_cast<Project::v20::knx::Module_t *>(base);
        if (module) {
            subWidget = parameterWidget(module, deviceInstance);
            Q_ASSERT(subWidget);
        }

        Project::v20::knx::Repeat_t * repeat = qobject_cast<Project::v20::knx::Repeat_t *>(base);
        if (repeat) {
            subWidget = parameterWidget(repeat, deviceInstance);
            Q_ASSERT(subWidget);
        }

        Q_ASSERT(channelIndependentBlock || applicationProgramChannel || dependentChannelChoose || module || repeat);

        if (subWidget) {
            mainLayout->addWidget(subWidget);
        }
    }

    return mainWidget;
}

QWidget * parameterWidget(Project::v20::knx::ApplicationProgramStatic_ParameterRefs_t * parameterRefs, Project::v20::knx::DeviceInstance_t * deviceInstance)
{
    Q_ASSERT(parameterRefs);
    Q_ASSERT(deviceInstance);

    /* Widget / Layout */
    QWidget * mainWidget = new QFrame();
    Q_ASSERT(mainWidget);
    QVBoxLayout * mainLayout = new QVBoxLayout();
    Q_ASSERT(mainLayout);
    mainWidget->setLayout(mainLayout);

    /* Content */
    for (Project::v20::knx::ParameterRef_t * parameterRef : parameterRefs->ParameterRef) {
        QWidget * subWidget = parameterWidget(parameterRef, deviceInstance);
        Q_ASSERT(subWidget);

        mainLayout->addWidget(subWidget);
    }

    return mainWidget;
}

QWidget * parameterWidget(Project::v20::knx::Assign_t * assign, Project::v20::knx::DeviceInstance_t * deviceInstance)
{
    Q_ASSERT(assign);
    Q_ASSERT(deviceInstance);

    /* ManufacturerData */
    // Project::v20::knx::ParameterRefRef_t * targetParameterRefRef = assign->getTargetParameterRefRef();
    // Project::v20::knx::ParameterRefRef_t * sourceParameterRefRef = assign->getSourceParameterRefRef();

    /* Widget / Layout */
    QWidget * widget = new QFrame();
    Q_ASSERT(widget);
    QHBoxLayout * layout = new QHBoxLayout();
    Q_ASSERT(layout);
    widget->setLayout(layout);

    // @todo parameterWidget Assign
    layout->addWidget(new QLabel("Assign t.b.d."), 2);

    return widget;
}

QWidget * parameterWidget(Project::v20::knx::BinaryDataRef_t * binaryDataRef, Project::v20::knx::DeviceInstance_t * deviceInstance)
{
    Q_ASSERT(binaryDataRef);
    Q_ASSERT(deviceInstance);

    /* ManufacturerData */
    //Project::SimpleElementTextType * d = binaryDataRef->getBinaryData();

    /* Widget / Layout */
    QWidget * mainWidget = new QFrame();
    Q_ASSERT(mainWidget);
    QVBoxLayout * mainLayout = new QVBoxLayout();
    Q_ASSERT(mainLayout);
    mainWidget->setLayout(mainLayout);

    // @todo parameterWidget BinaryDataRef
    mainLayout->addWidget(new QLabel("BinaryDataRef t.b.d."), 2);

    return mainWidget;
}

QWidget * parameterWidget(Project::v20::knx::Button_t * button, Project::v20::knx::DeviceInstance_t * deviceInstance)
{
    Q_ASSERT(button);
    Q_ASSERT(deviceInstance);

    /* ManufacturerData */
    //void * textParameter = button->getTextParameter();

    /* Widget / Layout */
    QWidget * widget = new QFrame();
    Q_ASSERT(widget);
    QHBoxLayout * layout = new QHBoxLayout();
    Q_ASSERT(layout);
    widget->setLayout(layout);

    // @todo parameterWidget Button
    layout->addWidget(new QLabel("Button t.b.d."), 2);

    return widget;
}

QWidget * parameterWidget(Project::v20::knx::ChannelChoose_t * channelChoose, Project::v20::knx::DeviceInstance_t * deviceInstance)
{
    Q_ASSERT(channelChoose);
    Q_ASSERT(deviceInstance);

    /* ManufacturerData */
    Project::v20::knx::ParameterRef_t * parameterRef = channelChoose->getParameterRef();
    Q_ASSERT(parameterRef);

    /* Project */
    Project::v20::ParameterValueInfo parameterValueInfo = Project::v20::getParameterValueInfo(parameterRef, deviceInstance);

    /* Widget / Layout */
    QWidget * mainWidget = new QFrame();
    Q_ASSERT(mainWidget);
    QVBoxLayout * mainLayout = new QVBoxLayout();
    Q_ASSERT(mainLayout);
    mainWidget->setLayout(mainLayout);

    /* Content */
    for (Project::v20::knx::ChannelChoose_when_t * when : channelChoose->When) {
        QWidget * whenWidget = new QFrame();
        Q_ASSERT(whenWidget);
        QVBoxLayout * whenLayout = new QVBoxLayout();
        Q_ASSERT(whenLayout);
        whenWidget->setLayout(whenLayout);
        whenWidget->setVisible(
            parameterValueInfo.parameterValue.isEmpty() ||
            ((when->Default == "true") && parameterValueInfo.isDefault()) ||
            (when->Test == parameterValueInfo.getValue()));
        mainLayout->addWidget(whenWidget);

        for (QObject * base : when->children()) {
            QWidget * subWidget{nullptr};

            Project::v20::knx::ComObjectParameterBlock_t * parameterBlock = qobject_cast<Project::v20::knx::ComObjectParameterBlock_t *>(base);
            if (parameterBlock) {
                subWidget = parameterWidget(parameterBlock, deviceInstance);
                Q_ASSERT(subWidget);
            }

            Project::v20::knx::ComObjectRefRef_t * comObjectRefRef = qobject_cast<Project::v20::knx::ComObjectRefRef_t *>(base);
            if (comObjectRefRef) {
                subWidget = parameterWidget(comObjectRefRef, deviceInstance);
                Q_ASSERT(subWidget);
            }

            Project::v20::knx::BinaryDataRef_t * binaryDataRef = qobject_cast<Project::v20::knx::BinaryDataRef_t *>(base);
            if (binaryDataRef) {
                subWidget = parameterWidget(binaryDataRef, deviceInstance);
                Q_ASSERT(subWidget);
            }

            Project::v20::knx::Module_t * module = qobject_cast<Project::v20::knx::Module_t *>(base);
            if (module) {
                subWidget = parameterWidget(module, deviceInstance);
                Q_ASSERT(subWidget);
            }

            Project::v20::knx::Repeat_t * repeat = qobject_cast<Project::v20::knx::Repeat_t *>(base);
            if (repeat) {
                subWidget = parameterWidget(repeat, deviceInstance);
                Q_ASSERT(subWidget);
            }

            Project::v20::knx::ChannelChoose_t * subChannelChoose = qobject_cast<Project::v20::knx::ChannelChoose_t *>(base);
            if (subChannelChoose) {
                subWidget = parameterWidget(subChannelChoose, deviceInstance);
                Q_ASSERT(subWidget);
            }

            Project::v20::knx::Rename_t * rename = qobject_cast<Project::v20::knx::Rename_t *>(base);
            if (rename) {
                subWidget = parameterWidget(rename, deviceInstance);
                Q_ASSERT(subWidget);
            }

            Q_ASSERT(parameterBlock || comObjectRefRef || binaryDataRef || module || repeat || subChannelChoose || rename);

            if (subWidget) {
                whenLayout->addWidget(subWidget);
            }
        }
    }

    return mainWidget;
}

QWidget * parameterWidget(Project::v20::knx::ChannelIndependentBlock_t * channelIndependentBlock, Project::v20::knx::DeviceInstance_t * deviceInstance)
{
    Q_ASSERT(channelIndependentBlock);
    Q_ASSERT(deviceInstance);

    /* Widget / Layout */
    QGroupBox * mainWidget = new QGroupBox();
    Q_ASSERT(mainWidget);
    mainWidget->setTitle("Channel Independent");
    QVBoxLayout * mainLayout = new QVBoxLayout();
    Q_ASSERT(mainLayout);
    mainWidget->setLayout(mainLayout);

    /* Content */
    for (QObject * base : channelIndependentBlock->children()) {
        QWidget * subWidget{nullptr};

        Project::v20::knx::ComObjectParameterBlock_t * comObjectParameterBlock = qobject_cast<Project::v20::knx::ComObjectParameterBlock_t *>(base);
        if (comObjectParameterBlock) {
            subWidget = parameterWidget(comObjectParameterBlock, deviceInstance);
            Q_ASSERT(subWidget);
        }

        Project::v20::knx::ChannelChoose_t * channelChoose = qobject_cast<Project::v20::knx::ChannelChoose_t *>(base);
        if (channelChoose) {
            subWidget = parameterWidget(channelChoose, deviceInstance);
            Q_ASSERT(subWidget);
        }

        Project::v20::knx::BinaryDataRef_t * binaryDataRef = qobject_cast<Project::v20::knx::BinaryDataRef_t *>(base);
        if (binaryDataRef) {
            subWidget = parameterWidget(binaryDataRef, deviceInstance);
            Q_ASSERT(subWidget);
        }

        Project::v20::knx::ComObjectRefRef_t * comObjectRefRef = qobject_cast<Project::v20::knx::ComObjectRefRef_t *>(base);
        if (comObjectRefRef) {
            subWidget = parameterWidget(comObjectRefRef, deviceInstance);
            Q_ASSERT(subWidget);
        }

        Project::v20::knx::Module_t * module = qobject_cast<Project::v20::knx::Module_t *>(base);
        if (module) {
            subWidget = parameterWidget(module, deviceInstance);
            Q_ASSERT(subWidget);
        }

        Project::v20::knx::Repeat_t * repeat = qobject_cast<Project::v20::knx::Repeat_t *>(base);
        if (repeat) {
            subWidget = parameterWidget(repeat, deviceInstance);
            Q_ASSERT(subWidget);
        }

        Q_ASSERT(comObjectParameterBlock || channelChoose || binaryDataRef || comObjectRefRef || module || repeat);

        if (subWidget) {
            mainLayout->addWidget(subWidget);
        }
    }

    return mainWidget;
}

QWidget * parameterWidget(Project::v20::knx::ComObjectParameterBlock_t * comObjectParameterBlock, Project::v20::knx::DeviceInstance_t * deviceInstance)
{
    Q_ASSERT(comObjectParameterBlock);
    Q_ASSERT(deviceInstance);

    /* ManufacturerData */
    // Project::v20::knx::ParameterRef_t * parameterRef = comObjectParameterBlock->getParameterRef();
    // Project::v20::knx::ParameterRef_t * textParameterRef = comObjectParameterBlock->getTextParameterRef();

    /* Widget / Layout */
    QGroupBox * mainWidget = new QGroupBox(comObjectParameterBlock->Text);
    Q_ASSERT(mainWidget);
    QVBoxLayout * mainLayout = new QVBoxLayout();
    Q_ASSERT(mainLayout);
    mainWidget->setLayout(mainLayout);
    // @todo widget->setToolTip("ComObjectParameterBlock " + comObjectParameterBlock->Id);

    /* Content */
    //Project::v20::knx::ComObjectParameterBlock_Rows_t * rows = comObjectParameterBlock->Rows;
    //Project::v20::knx::ComObjectParameterBlock_Columns_t * columns = comObjectParameterBlock->Columns;
    for (QObject * base : comObjectParameterBlock->children()) {
        QWidget * subWidget{nullptr};

        Project::v20::knx::ComObjectParameterBlock_t * subComObjectParameterBlock = qobject_cast<Project::v20::knx::ComObjectParameterBlock_t *>(base);
        if (subComObjectParameterBlock) {
            subWidget = parameterWidget(subComObjectParameterBlock, deviceInstance);
            Q_ASSERT(subWidget);
        }

        Project::v20::knx::ParameterSeparator_t * parameterSeparator = qobject_cast<Project::v20::knx::ParameterSeparator_t *>(base);
        if (parameterSeparator) {
            subWidget = parameterWidget(parameterSeparator, deviceInstance);
            Q_ASSERT(subWidget);
        }

        Project::v20::knx::ParameterRefRef_t * parameterRefRef = qobject_cast<Project::v20::knx::ParameterRefRef_t *>(base);
        if (parameterRefRef) {
            subWidget = parameterWidget(parameterRefRef, deviceInstance);
            Q_ASSERT(subWidget);
        }

        Project::v20::knx::Button_t * button = qobject_cast<Project::v20::knx::Button_t *>(base);
        if (button) {
            subWidget = parameterWidget(button, deviceInstance);
            Q_ASSERT(subWidget);
        }

        Project::v20::knx::ComObjectParameterChoose_t * comObjectParameterChoose = qobject_cast<Project::v20::knx::ComObjectParameterChoose_t *>(base);
        if (comObjectParameterChoose) {
            subWidget = parameterWidget(comObjectParameterChoose, deviceInstance);
            Q_ASSERT(subWidget);
        }

        Project::v20::knx::BinaryDataRef_t * binaryDataRef = qobject_cast<Project::v20::knx::BinaryDataRef_t *>(base);
        if (binaryDataRef) {
            subWidget = parameterWidget(binaryDataRef, deviceInstance);
            Q_ASSERT(subWidget);
        }

        Project::v20::knx::ComObjectRefRef_t * comObjectRefRef = qobject_cast<Project::v20::knx::ComObjectRefRef_t *>(base);
        if (comObjectRefRef) {
            // @note ComObjectRefRef is not shown in ParameterBlock
            //            subWidget = parameterWidget(comObjectRefRef, deviceInstance);
            //            Q_ASSERT(subWidget);
        }

        Project::v20::knx::Module_t * module = qobject_cast<Project::v20::knx::Module_t *>(base);
        if (module) {
            subWidget = parameterWidget(module, deviceInstance);
            Q_ASSERT(subWidget);
        }

        Project::v20::knx::Repeat_t * repeat = qobject_cast<Project::v20::knx::Repeat_t *>(base);
        if (repeat) {
            subWidget = parameterWidget(repeat, deviceInstance);
            Q_ASSERT(subWidget);
        }

        Project::v20::knx::Assign_t * assign = qobject_cast<Project::v20::knx::Assign_t *>(base);
        if (assign) {
            subWidget = parameterWidget(assign, deviceInstance);
            Q_ASSERT(subWidget);
        }

        Project::v20::knx::ApplicationProgramChannel_t * applicationProgramChannel = qobject_cast<Project::v20::knx::ApplicationProgramChannel_t *>(base);
        if (assign) {
            subWidget = parameterWidget(applicationProgramChannel, deviceInstance);
            Q_ASSERT(subWidget);
        }

        Q_ASSERT(comObjectParameterBlock || parameterSeparator || parameterRefRef || button || comObjectParameterChoose || binaryDataRef || comObjectRefRef || module || repeat || assign || applicationProgramChannel);

        if (subWidget) {
            mainLayout->addWidget(subWidget);
        }
    }

    return mainWidget;
}

QWidget * parameterWidget(Project::v20::knx::ComObjectParameterChoose_t * comObjectParameterChoose, Project::v20::knx::DeviceInstance_t * deviceInstance)
{
    Q_ASSERT(comObjectParameterChoose);
    Q_ASSERT(deviceInstance);

    /* ManufacturerData */
    Project::v20::knx::ParameterRef_t * parameterRef = comObjectParameterChoose->getParameterRef();
    Q_ASSERT(parameterRef);

    /* Project */
    Project::v20::ParameterValueInfo parameterValueInfo = Project::v20::getParameterValueInfo(parameterRef, deviceInstance);

    /* Widget / Layout */
    QFrame * mainWidget = new QFrame();
    Q_ASSERT(mainWidget);
    QVBoxLayout * mainLayout = new QVBoxLayout();
    Q_ASSERT(mainLayout);
    mainWidget->setLayout(mainLayout);

    /* Content */
    for (Project::v20::knx::ComObjectParameterChoose_when_t * when : comObjectParameterChoose->When) {
        QFrame * whenWidget = new QFrame();
        Q_ASSERT(whenWidget);
        QVBoxLayout * whenLayout = new QVBoxLayout();
        Q_ASSERT(whenLayout);
        whenWidget->setLayout(whenLayout);
        whenWidget->setVisible(
            parameterValueInfo.parameterValue.isEmpty() ||
            ((when->Default == "true") && parameterValueInfo.isDefault()) ||
            (when->Test == parameterValueInfo.getValue()));
        mainLayout->addWidget(whenWidget);

        for (QObject * base : when->children()) {
            QWidget * subWidget{nullptr};

            Project::v20::knx::ComObjectParameterBlock_t * comObjectParameterBlock = qobject_cast<Project::v20::knx::ComObjectParameterBlock_t *>(base);
            if (comObjectParameterBlock) {
                subWidget = parameterWidget(comObjectParameterBlock, deviceInstance);
                Q_ASSERT(subWidget);
            }

            Project::v20::knx::ParameterSeparator_t * parameterSeparator = qobject_cast<Project::v20::knx::ParameterSeparator_t *>(base);
            if (parameterSeparator) {
                subWidget = parameterWidget(parameterSeparator, deviceInstance);
                Q_ASSERT(subWidget);
            }

            Project::v20::knx::ParameterRefRef_t * parameterRefRef = qobject_cast<Project::v20::knx::ParameterRefRef_t *>(base);
            if (parameterRefRef) {
                subWidget = parameterWidget(parameterRefRef, deviceInstance);
                Q_ASSERT(subWidget);
            }

            Project::v20::knx::Button_t * button = qobject_cast<Project::v20::knx::Button_t *>(base);
            if (button) {
                subWidget = parameterWidget(button, deviceInstance);
                Q_ASSERT(subWidget);
            }

            Project::v20::knx::ComObjectParameterChoose_t * subComObjectParameterChoose = qobject_cast<Project::v20::knx::ComObjectParameterChoose_t *>(base);
            if (subComObjectParameterChoose) {
                subWidget = parameterWidget(subComObjectParameterChoose, deviceInstance);
                Q_ASSERT(subWidget);
            }

            Project::v20::knx::BinaryDataRef_t * binaryDataRef = qobject_cast<Project::v20::knx::BinaryDataRef_t *>(base);
            if (binaryDataRef) {
                subWidget = parameterWidget(binaryDataRef, deviceInstance);
                Q_ASSERT(subWidget);
            }

            Project::v20::knx::ComObjectRefRef_t * comObjectRefRef = qobject_cast<Project::v20::knx::ComObjectRefRef_t *>(base);
            if (comObjectRefRef) {
                // @note ComObjectRefRef is not shown in ParameterBlock
                //                subWidget = parameterWidget(comObjectRefRef, deviceInstance);
                //                Q_ASSERT(subWidget);
            }

            Project::v20::knx::Module_t * module = qobject_cast<Project::v20::knx::Module_t *>(base);
            if (module) {
                subWidget = parameterWidget(module, deviceInstance);
                Q_ASSERT(subWidget);
            }

            Project::v20::knx::Repeat_t * repeat = qobject_cast<Project::v20::knx::Repeat_t *>(base);
            if (repeat) {
                subWidget = parameterWidget(repeat, deviceInstance);
                Q_ASSERT(subWidget);
            }

            Project::v20::knx::Assign_t * assign = qobject_cast<Project::v20::knx::Assign_t *>(base);
            if (assign) {
                subWidget = parameterWidget(assign, deviceInstance);
                Q_ASSERT(subWidget);
            }

            Project::v20::knx::Rename_t * rename = qobject_cast<Project::v20::knx::Rename_t *>(base);
            if (rename) {
                subWidget = parameterWidget(rename, deviceInstance);
                Q_ASSERT(subWidget);
            }

            Q_ASSERT(comObjectParameterBlock || parameterSeparator || parameterRefRef || button || comObjectParameterChoose || binaryDataRef || comObjectRefRef || module || repeat || assign || rename);

            if (subWidget) {
                whenLayout->addWidget(subWidget);
            }
        }
    }

    return mainWidget;
}

QWidget * parameterWidget(Project::v20::knx::ComObjectRefRef_t * comObjectRefRef, Project::v20::knx::DeviceInstance_t * deviceInstance)
{
    Q_ASSERT(comObjectRefRef);
    Q_ASSERT(deviceInstance);

    /* ManufacturerData */
    Project::v20::knx::ComObjectRef_t * comObjectRef = comObjectRefRef->getComObjectRef();
    Q_ASSERT(comObjectRef);
    Project::v20::knx::ComObject_t * comObject = comObjectRef->getComObject();
    Q_ASSERT(comObject);

    /* Project */
    bool hasLinks{false};
    Project::v20::knx::DeviceInstance_ComObjectInstanceRefs_t * comObjectInstanceRefs = deviceInstance->ComObjectInstanceRefs;
    if (comObjectInstanceRefs) {
        Project::v20::knx::ComObjectInstanceRef_t * comObjectInstanceRef = comObjectInstanceRefs->ComObjectInstanceRef.first(); // @todo multiple com object instance references?
        if (comObjectInstanceRef) {
            hasLinks = !comObjectInstanceRef->Links.empty();
        }
    }

    /* Widget / Layout */
    QWidget * widget = new QFrame();
    Q_ASSERT(widget);
    QHBoxLayout * layout = new QHBoxLayout();
    Q_ASSERT(layout);
    widget->setLayout(layout);

    /* label */
    layout->addWidget(new QLabel(comObject->Text + " # " + comObject->FunctionText));

    /* field */
    QLineEdit * field = new QLineEdit();
    Q_ASSERT(field);
    if (hasLinks) {
        field->setText("Active");
        field->setModified(true);
    } else {
        field->setText("Inactive");
        field->setModified(false);
    }
    layout->addWidget(field);

    return widget;
}

QWidget * parameterWidget(Project::v20::knx::DependentChannelChoose_t * dependentChannelChoose, Project::v20::knx::DeviceInstance_t * deviceInstance)
{
    Q_ASSERT(dependentChannelChoose);
    Q_ASSERT(deviceInstance);

    /* ManufacturerData */
    Project::v20::knx::ParameterRef_t * parameterRef = dependentChannelChoose->getParameterRef();
    Q_ASSERT(parameterRef);

    /* Project */
    Project::v20::ParameterValueInfo parameterValueInfo = Project::v20::getParameterValueInfo(parameterRef, deviceInstance);

    /* Widget / Layout */
    QWidget * mainWidget = new QFrame();
    Q_ASSERT(mainWidget);
    QVBoxLayout * mainLayout = new QVBoxLayout();
    Q_ASSERT(mainLayout);
    mainWidget->setLayout(mainLayout);

    /* Content */
    for (Project::v20::knx::DependentChannelChoose_when_t * when : dependentChannelChoose->When) {
        QFrame * whenWidget = new QFrame();
        Q_ASSERT(whenWidget);
        QVBoxLayout * whenLayout = new QVBoxLayout();
        Q_ASSERT(whenLayout);
        whenWidget->setLayout(whenLayout);
        whenWidget->setVisible(
            parameterValueInfo.parameterValue.isEmpty() ||
            ((when->Default == "true") && parameterValueInfo.isDefault()) ||
            (when->Test == parameterValueInfo.getValue()));
        mainLayout->addWidget(whenWidget);

        for (QObject * base : when->children()) {
            QWidget * subWidget{nullptr};

            Project::v20::knx::ApplicationProgramChannel_t * applicationProgramChannel = qobject_cast<Project::v20::knx::ApplicationProgramChannel_t *>(base);
            if (applicationProgramChannel) {
                subWidget = parameterWidget(applicationProgramChannel, deviceInstance);
                Q_ASSERT(subWidget);
            }

            Project::v20::knx::DependentChannelChoose_t * subDependentChannelChoose = qobject_cast<Project::v20::knx::DependentChannelChoose_t *>(base);
            if (subDependentChannelChoose) {
                subWidget = parameterWidget(subDependentChannelChoose, deviceInstance);
                Q_ASSERT(subWidget);
            }

            Project::v20::knx::Rename_t * rename = qobject_cast<Project::v20::knx::Rename_t *>(base);
            if (rename) {
                subWidget = parameterWidget(rename, deviceInstance);
                Q_ASSERT(subWidget);
            }

            Project::v20::knx::Module_t * module = qobject_cast<Project::v20::knx::Module_t *>(base);
            if (module) {
                subWidget = parameterWidget(module, deviceInstance);
                Q_ASSERT(subWidget);
            }

            Project::v20::knx::Repeat_t * repeat = qobject_cast<Project::v20::knx::Repeat_t *>(base);
            if (repeat) {
                subWidget = parameterWidget(repeat, deviceInstance);
                Q_ASSERT(subWidget);
            }

            Q_ASSERT(applicationProgramChannel || dependentChannelChoose || rename || module || repeat);

            if (subWidget) {
                whenLayout->addWidget(subWidget);
            }
        }
    }

    return mainWidget;
}

QWidget * parameterWidget(Project::v20::knx::Module_NumericArg_t * numericArg, Project::v20::knx::DeviceInstance_t * deviceInstance)
{
    Q_ASSERT(numericArg);
    Q_ASSERT(deviceInstance);

    /* ManufacturerData */
    // @todo RefId
    //void * allocator = numericArg->getAllocator();
    //void * baseValue = numericArg->getBaseValue();

    /* Widget / Layout */
    QWidget * widget = new QFrame();
    Q_ASSERT(widget);
    QHBoxLayout * layout = new QHBoxLayout();
    Q_ASSERT(layout);
    widget->setLayout(layout);

    // @todo parameterWidget numericArg
    layout->addWidget(new QLabel("Module NumericArg t.b.d."), 2);

    return widget;
}

QWidget * parameterWidget(Project::v20::knx::Module_TextArg_t * textArg, Project::v20::knx::DeviceInstance_t * deviceInstance)
{
    Q_ASSERT(textArg);
    Q_ASSERT(deviceInstance);
    QString toolTip = "TextArg " + textArg->Id;

    /* ManufacturerData */
    // @todo RefId

    /* Widget / Layout */
    QWidget * widget = new QFrame();
    Q_ASSERT(widget);
    QHBoxLayout * layout = new QHBoxLayout();
    Q_ASSERT(layout);
    widget->setLayout(layout);

    // @todo parameterWidget textArg
    layout->addWidget(new QLabel("Module TextArg t.b.d."), 2);

    widget->setToolTip(toolTip);
    return widget;
}

QWidget * parameterWidget(Project::v20::knx::Module_t * module, Project::v20::knx::DeviceInstance_t * deviceInstance)
{
    Q_ASSERT(module);
    Q_ASSERT(deviceInstance);
    QString toolTip = "Module " + module->Id;

    /* ManufacturerData */
    // @todo RefId

    /* Widget / Layout */
    QWidget * mainWidget = new QFrame();
    Q_ASSERT(mainWidget);
    QVBoxLayout * mainLayout = new QVBoxLayout();
    mainWidget->setLayout(mainLayout);

    /* Content */
    for (QObject * base : module->children()) {
        QWidget * subWidget{nullptr};

        Project::v20::knx::Module_NumericArg_t * numericArg = qobject_cast<Project::v20::knx::Module_NumericArg_t *>(base);
        if (numericArg) {
            subWidget = parameterWidget(numericArg, deviceInstance);
            Q_ASSERT(subWidget);
        }

        Project::v20::knx::Module_TextArg_t * textArg = qobject_cast<Project::v20::knx::Module_TextArg_t *>(base);
        if (textArg) {
            subWidget = parameterWidget(textArg, deviceInstance);
            Q_ASSERT(subWidget);
        }

        Q_ASSERT(numericArg || textArg);

        if (subWidget) {
            mainLayout->addWidget(subWidget);
        }
    }

    mainWidget->setToolTip(toolTip);
    return mainWidget;
}

QWidget * parameterWidget(Project::v20::knx::ParameterRefRef_t * parameterRefRef, Project::v20::knx::DeviceInstance_t * deviceInstance)
{
    Q_ASSERT(parameterRefRef);
    Q_ASSERT(deviceInstance);
    QString toolTip = "ParameterRefRef";

    /* ManufacturerData */
    Project::v20::knx::ParameterRef_t * parameterRef = parameterRefRef->getParameterRef();
    toolTip += "<br><br>ParameterRef " + parameterRef->Id;
    Q_ASSERT(parameterRef);

    /* Widget / Layout */
    QWidget * widget = parameterWidget(parameterRef, deviceInstance);
    Q_ASSERT(widget);

    widget->setToolTip(toolTip);
    return widget;
}

QWidget * parameterWidget(Project::v20::knx::ParameterRef_t * parameterRef, Project::v20::knx::DeviceInstance_t * deviceInstance)
{
    Q_ASSERT(parameterRef);
    Q_ASSERT(deviceInstance);
    QString toolTip = "ParameterRef " + parameterRef->Id;
    toolTip += "<br>  Text = " + parameterRef->Text;
    toolTip += "<br>  SuffixText = " + parameterRef->SuffixText;
    toolTip += "<br>  Tag = " + parameterRef->Tag;
    toolTip += "<br>  Access = " + parameterRef->Access;
    toolTip += "<br>  Value = " + parameterRef->Value;

    /* ManufacturerData */
    //Project::v20::knx::ParameterRef_t * textParameterRef = parameterRef->getTextParameterRef();
    Project::v20::knx::ParameterType_t * parameterType{nullptr};
    QString parameterText = parameterRef->Text;
    QString parameterSuffixText = parameterRef->SuffixText;
    QString parameterAccess = parameterRef->Access;
    QString parameterValue = parameterRef->Value;
    Project::Base * parameter = parameterRef->getParameter();
    Q_ASSERT(parameter);
    Project::v20::knx::ApplicationProgramStatic_Parameters_Parameter_t * normalParameter = qobject_cast<Project::v20::knx::ApplicationProgramStatic_Parameters_Parameter_t *>(parameter);
    if (normalParameter) {
        toolTip += "<br><br>Parameter " + normalParameter->Id;
        toolTip += "<br>  Text = " + normalParameter->Text;
        toolTip += "<br>  SuffixText = " + normalParameter->SuffixText;
        toolTip += "<br>  Access = " + normalParameter->Access;
        toolTip += "<br>  Value = " + normalParameter->Value;
        parameterType = normalParameter->getParameterType();
        toolTip += "<br><br>ParameterType " + parameterType->Id;
        parameterText = parameterText.isEmpty() ? normalParameter->Text : parameterText;
        parameterSuffixText = parameterSuffixText.isEmpty() ? normalParameter->SuffixText : parameterSuffixText;
        parameterAccess = parameterAccess.isEmpty() ? normalParameter->Access : parameterAccess;
        parameterValue = parameterValue.isEmpty() ? normalParameter->Value : parameterValue;

        for (QObject * base : normalParameter->children()) {
            Project::v20::knx::MemoryParameter_t * memory = qobject_cast<Project::v20::knx::MemoryParameter_t *>(base);
            if (memory) {
                toolTip += "<br><br>Memory";
                toolTip += "<br>  CodeSegment = " + memory->CodeSegment;
                toolTip += "<br>  Offset = " + memory->Offset;
                toolTip += "<br>  BitOffset = " + memory->BitOffset;

                // @todo Project::v20::knx::ApplicationProgramStatic_Code_AbsoluteSegment_t * codeSegment = memory->getCodeSegment();
            }

            Project::v20::knx::PropertyParameter_t * property = qobject_cast<Project::v20::knx::PropertyParameter_t *>(base);
            if (property) {
                toolTip += "<br><br>Property";
                toolTip += "<br>  ObjectIndex = " + property->ObjectIndex;
                toolTip += "<br>  ObjectType = " + property->ObjectType;
                toolTip += "<br>  Occurrence = " + property->Occurrence;
                toolTip += "<br>  PropertyId = " + property->PropertyId;
                toolTip += "<br>  Offset = " + property->Offset;
                toolTip += "<br>  BitOffset = " + property->BitOffset;
            }

            Q_ASSERT(memory || property);
        }
    }
    Project::v20::knx::UnionParameter_t * unionParameter = qobject_cast<Project::v20::knx::UnionParameter_t *>(parameter);
    if (unionParameter) {
        toolTip += "<br><br>UnionParameter " + unionParameter->Id;
        toolTip += "<br>  Text = " + unionParameter->Text;
        toolTip += "<br>  SuffixText = " + unionParameter->SuffixText;
        toolTip += "<br>  Access = " + unionParameter->Access;
        toolTip += "<br>  Value = " + unionParameter->Value;
        parameterType = unionParameter->getParameterType();
        toolTip += "<br><br>ParameterType " + parameterType->Id;
        parameterText = parameterText.isEmpty() ? unionParameter->Text : parameterText;
        parameterSuffixText = parameterSuffixText.isEmpty() ? unionParameter->SuffixText : parameterSuffixText;
        parameterAccess = parameterAccess.isEmpty() ? unionParameter->Access : parameterAccess;
        parameterValue = parameterValue.isEmpty() ? unionParameter->Value : parameterValue;
    }
    Q_ASSERT(normalParameter || unionParameter);
    Q_ASSERT(parameterType);

    /* Project */
    bool isModified = false;
    Project::v20::knx::DeviceInstance_ParameterInstanceRefs_t * parameterInstanceRefs = deviceInstance->ParameterInstanceRefs;
    if (parameterInstanceRefs) {
        Project::v20::knx::ParameterInstanceRef_t * parameterInstanceRef = parameterInstanceRefs->ParameterInstanceRef.value(parameterRef->Id, nullptr);
        if (parameterInstanceRef) {
            toolTip += "<br><br>ParameterInstanceRef " + parameterInstanceRef->Id;
            parameterValue = parameterInstanceRef->Value;
            isModified = true;
        }
    }

    /* Widget / Layout */
    QWidget * widget = new QFrame();
    Q_ASSERT(widget);
    QHBoxLayout * layout = new QHBoxLayout();
    Q_ASSERT(layout);
    widget->setLayout(layout);

    /* label */
    QLabel * label = new QLabel(parameterText);
    layout->addWidget(label);

    /* field */
    QWidget * field{nullptr};
    for (QObject * base : parameterType->children()) {
        Project::v20::knx::ParameterType_TypeNone_t * typeNone = qobject_cast<Project::v20::knx::ParameterType_TypeNone_t *>(base);
        if (typeNone) {
            Q_ASSERT(typeNone);
        }

        Project::v20::knx::ParameterType_TypeNumber_t * typeNumber = qobject_cast<Project::v20::knx::ParameterType_TypeNumber_t *>(base);
        if (typeNumber) {
            Q_ASSERT(typeNumber);
            QSpinBox * numberField = new QSpinBox();
            Q_ASSERT(numberField);
            if (!parameterSuffixText.isEmpty()) {
                numberField->setSuffix(" " + parameterSuffixText);
            }
            if (!typeNumber->minInclusive.isEmpty()) {
                numberField->setMinimum(typeNumber->minInclusive.toInt());
            }
            if (!typeNumber->maxInclusive.isEmpty()) {
                numberField->setMaximum(typeNumber->maxInclusive.toInt());
            }
            numberField->setValue(parameterValue.toInt());
            field = numberField;
        }

        Project::v20::knx::ParameterType_TypeRestriction_t * typeRestriction = qobject_cast<Project::v20::knx::ParameterType_TypeRestriction_t *>(base);
        if (typeRestriction) {
            Q_ASSERT(typeRestriction);
            if (typeRestriction->Enumeration.count() == 2) {
                QWidget * restrictionField = new QWidget();
                Q_ASSERT(restrictionField);
                QHBoxLayout * restrictionFieldLayout = new QHBoxLayout();
                Q_ASSERT(restrictionFieldLayout);
                restrictionField->setLayout(restrictionFieldLayout);
                for (Project::v20::knx::ParameterType_TypeRestriction_Enumeration_t * Enumeration : typeRestriction->Enumeration) {
                    QRadioButton * radioButton = new QRadioButton(Enumeration->Text);
                    Q_ASSERT(radioButton);
                    if (parameterValue == Enumeration->Value) {
                        radioButton->setChecked(true);
                    }
                    restrictionFieldLayout->addWidget(radioButton);
                }
                field = restrictionField;
            } else {
                QComboBox * restrictionField = new QComboBox();
                Q_ASSERT(restrictionField);
                for (Project::v20::knx::ParameterType_TypeRestriction_Enumeration_t * Enumeration : typeRestriction->Enumeration) {
                    restrictionField->addItem(Enumeration->Text, Enumeration->Value);
                }
                restrictionField->setCurrentIndex(restrictionField->findData(parameterValue));
                field = restrictionField;
            }
        }

        Project::v20::knx::ParameterType_TypeText_t * typeText = qobject_cast<Project::v20::knx::ParameterType_TypeText_t *>(base);
        if (typeText) {
            Q_ASSERT(typeText);
            QLineEdit * textField = new QLineEdit();
            Q_ASSERT(textField);
            if (!typeText->SizeInBit.isEmpty()) {
                textField->setMaxLength(typeText->SizeInBit.toInt() / 8);
            }
            textField->setText(parameterValue);
            field = textField;
        }

        Project::v20::knx::ParameterType_TypeFloat_t * typeFloat = qobject_cast<Project::v20::knx::ParameterType_TypeFloat_t *>(base);
        if (typeFloat) {
            Q_ASSERT(typeFloat);
            // QDoubleSpinBox * floatField = new QDoubleSpinBox();
            // @todo Project::v20::knx::ParameterType_TypeFloat_t -> QDoubleSpinBox
            // field = floatField;
        }

        Project::v20::knx::ParameterType_TypeTime_t * typeTime = qobject_cast<Project::v20::knx::ParameterType_TypeTime_t *>(base);
        if (typeTime) {
            Q_ASSERT(typeTime);
            // QTimeEdit * timeField = new QTimeEdit();
            // @todo Project::v20::knx::ParameterType_TypeTime_t -> QTimeEdit
            // field = timeField;
        }

        Project::v20::knx::ParameterType_TypeDate_t * typeDate = qobject_cast<Project::v20::knx::ParameterType_TypeDate_t *>(base);
        if (typeDate) {
            Q_ASSERT(typeDate);
            // QDateEdit * dateField = new QDateEdit();
            // @todo Project::v20::knx::ParameterType_TypeDate_t -> QDateEdit
            // field = dateField;
        }

        Project::v20::knx::ParameterType_TypeIPAddress_t * typeIPAddress = qobject_cast<Project::v20::knx::ParameterType_TypeIPAddress_t *>(base);
        if (typeIPAddress) {
            Q_ASSERT(typeIPAddress);
            QLineEdit * ipAddressField = new QLineEdit();
            Q_ASSERT(ipAddressField);
            QRegularExpression ipAddressRegExp("(([ 0]+)|([ 0]*[0-9] *)|([0-9][0-9] )|([ 0][0-9][0-9])|(1[0-9][0-9])|([2][0-4][0-9])|(25[0-5]))");
            QRegularExpressionValidator * ipAddressValidator = new QRegularExpressionValidator(ipAddressRegExp, ipAddressField);
            Q_ASSERT(ipAddressValidator);
            ipAddressField->setValidator(ipAddressValidator);
            ipAddressField->setInputMask("000.000.000.000");
            ipAddressField->setText(parameterValue);
            field = ipAddressField;
        }

        Project::v20::knx::ParameterType_TypePicture_t * typePicture = qobject_cast<Project::v20::knx::ParameterType_TypePicture_t *>(base);
        if (typePicture) {
            Q_ASSERT(typePicture);
            Project::v20::knx::ManufacturerData_Manufacturer_Baggages_Baggage_t * baggage = typePicture->getBaggage();
            Q_ASSERT(baggage);
            Project::v20::knx::ManufacturerData_Manufacturer_t * manufacturer = baggage->findParent<Project::v20::knx::ManufacturerData_Manufacturer_t *>();
            Q_ASSERT(manufacturer);
            ProjectBundle * projectBundle = manufacturer->findParent<ProjectBundle *>();
            Q_ASSERT(projectBundle);
            const QString fileName = manufacturer->RefId + "/Baggages/" + baggage->Name;
            Q_ASSERT(projectBundle->baggages.contains(fileName));
            QPixmap pixmap;
            pixmap.loadFromData(projectBundle->baggages.value(fileName));
            QLabel * pictureField = new QLabel();
            Q_ASSERT(pictureField);
            pictureField->setPixmap(pixmap);
            field = pictureField;
        }

        Project::v20::knx::ParameterType_TypeColor_t * typeColor = qobject_cast<Project::v20::knx::ParameterType_TypeColor_t *>(base);
        if (typeColor) {
            Q_ASSERT(typeColor);
            // QColorDialog * colorField = new QColorDialog();
            // @todo Project::v20::knx::ParameterType_TypeColor_t -> QLineEdit/QColorDialog
            // field = colorField;
        }

        Project::v20::knx::ParameterType_TypeRawData_t * typeRawData = qobject_cast<Project::v20::knx::ParameterType_TypeRawData_t *>(base);
        if (typeRawData) {
            Q_ASSERT(typeRawData);
            // QTextEdit * rawDataField = new QTextEdit();
            // @todo Project::v20::knx::ParameterType_TypeRawData_t -> QTextEdit?
            // field = rawDataField;
        }

        Q_ASSERT(typeNone || typeNumber || typeRestriction || typeText || typeFloat || typeTime || typeDate || typeIPAddress || typePicture || typeColor || typeRawData);
    }

    if (field) {
        if (isModified) {
            field->setStyleSheet("background-color: #FFFFCC");
        }
        layout->addWidget(field);
    }

    if (parameterAccess == "None") {
        widget->setVisible(false);
    } else if (parameterAccess == "Read") {
        widget->setEnabled(false);
    } else if (parameterAccess == "ReadWrite") {
        // do nothing
    }

    widget->setToolTip(toolTip);
    return widget;
}

QWidget * parameterWidget(Project::v20::knx::ParameterSeparator_t * parameterSeparator, Project::v20::knx::DeviceInstance_t * deviceInstance)
{
    Q_ASSERT(parameterSeparator);
    Q_ASSERT(deviceInstance);
    QString toolTip = "ParameterSeparator " + parameterSeparator->Id;

    /* ManufacturerData */
    //Project::v20::knx::ParameterRef_t * textParameterRef = parameterSeparator->getTextParameterRef();

    /* Widget / Layout */
    QWidget * widget = new QWidget();
    Q_ASSERT(widget);
    QHBoxLayout * layout = new QHBoxLayout();
    Q_ASSERT(layout);
    widget->setLayout(layout);

    /* label */
    QLabel * label = new QLabel(parameterSeparator->Text);
    Q_ASSERT(label);
    layout->addWidget(label);
    if (parameterSeparator->UIHint == "HorizontalRuler") {
        // @todo HorizontalRuler above or below?
        QFrame * horizontalRuler = new QFrame();
        Q_ASSERT(horizontalRuler);
        horizontalRuler->setFrameShape(QFrame::HLine);
        layout->addWidget(horizontalRuler);
    } else if (parameterSeparator->UIHint == "Headline") {
        // @todo Headline
    } else if (parameterSeparator->UIHint == "Information") {
        // @todo Information
    } else if (parameterSeparator->UIHint == "Error") {
        // @todo Error
    }

    widget->setToolTip(toolTip);
    return widget;
}

QWidget * parameterWidget(Project::v20::knx::Rename_t * rename, Project::v20::knx::DeviceInstance_t * deviceInstance)
{
    Q_ASSERT(rename);
    Q_ASSERT(deviceInstance);
    QString toolTip = "Rename" + rename->Id;

    /* ManufacturerData */
    //Project::v20::knx::ComObjectParameterBlock_t * comObjectParameterBlock = rename->getComObjectParameterBlock();

    /* Widget / Layout */
    QWidget * widget = new QFrame();
    Q_ASSERT(widget);
    QHBoxLayout * layout = new QHBoxLayout();
    Q_ASSERT(layout);
    widget->setLayout(layout);

    // @todo parameterWidget rename
    layout->addWidget(new QLabel("Rename t.b.d."), 2);

    widget->setToolTip(toolTip);
    return widget;
}

QWidget * parameterWidget(Project::v20::knx::Repeat_t * repeat, Project::v20::knx::DeviceInstance_t * deviceInstance)
{
    Q_ASSERT(repeat);
    Q_ASSERT(deviceInstance);
    QString toolTip = "Repeat " + repeat->Id;

    /* ManufacturerData */
    //void * parameter = repeat->getParameter();

    /* Widget / Layout */
    QWidget * mainWidget = new QFrame();
    Q_ASSERT(mainWidget);
    QVBoxLayout * mainLayout = new QVBoxLayout();
    mainWidget->setLayout(mainLayout);

    /* Content */
    for (QObject * base : repeat->children()) {
        QWidget * subWidget{nullptr};

        Project::v20::knx::ComObjectParameterChoose_t * comObjectParameterChoose = qobject_cast<Project::v20::knx::ComObjectParameterChoose_t *>(base);
        if (comObjectParameterChoose) {
            subWidget = parameterWidget(comObjectParameterChoose, deviceInstance);
            Q_ASSERT(subWidget);
        }

        Project::v20::knx::Module_t * module = qobject_cast<Project::v20::knx::Module_t *>(base);
        if (module) {
            subWidget = parameterWidget(module, deviceInstance);
            Q_ASSERT(subWidget);
        }

        Project::v20::knx::Repeat_t * subRepeat = qobject_cast<Project::v20::knx::Repeat_t *>(base);
        if (subRepeat) {
            subWidget = parameterWidget(subRepeat, deviceInstance);
            Q_ASSERT(subWidget);
        }

        Q_ASSERT(comObjectParameterChoose || module || repeat);

        if (subWidget) {
            mainLayout->addWidget(subWidget);
        }
    }

    mainWidget->setToolTip(toolTip);
    return mainWidget;
}
