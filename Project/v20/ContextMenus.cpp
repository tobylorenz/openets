#include "ContextMenus.h"

#include <QApplication>
#include <QDebug>
#include <QMdiArea>
#include <QMdiSubWindow>
#include <QScrollArea>

#include <Project/v20/ParameterWidgets.h>
#include <Project/v20/knx/ApplicationProgramRef_t.h>
#include <Project/v20/knx/ApplicationProgramStatic_Parameters_Parameter_t.h>
#include <Project/v20/knx/ApplicationProgramStatic_t.h>
#include <Project/v20/knx/ApplicationProgram_t.h>
#include <Project/v20/knx/Hardware2Program_t.h>

static void showParameterRefsWidget(QMdiArea * mdiArea, Project::v20::knx::DeviceInstance_t * deviceInstance)
{
    Q_ASSERT(mdiArea);
    Q_ASSERT(deviceInstance);

    /* ManufacturerData */
    // Product * product = deviceInstance->getProduct();
    // Q_ASSERT(product);
    Project::v20::knx::Hardware2Program_t * hardware2program = deviceInstance->getHardware2Program();
    Q_ASSERT(hardware2program);
    if (hardware2program->ApplicationProgramRef.count() > 1) {
        qWarning() << "Multiple Application Program Refs found for Device Instance ID" << deviceInstance->Id;
    }
    // @todo multiple application program references?
    Project::v20::knx::ApplicationProgramRef_t * applicationProgramRef = hardware2program->ApplicationProgramRef.first();
    Q_ASSERT(applicationProgramRef);
    Project::v20::knx::ApplicationProgram_t * applicationProgram = applicationProgramRef->getApplicationProgram();
    Q_ASSERT(applicationProgram);
    Project::v20::knx::ApplicationProgramStatic_t * static_ = applicationProgram->Static;
    Q_ASSERT(static_);
    Project::v20::knx::ApplicationProgramStatic_ParameterRefs_t * parameterRefs = static_->ParameterRefs;
    Q_ASSERT(parameterRefs);

    /* define the window content */
    QScrollArea * scrollArea = new QScrollArea();
    Q_ASSERT(scrollArea);
    QWidget * widget = parameterWidget(parameterRefs, deviceInstance);
    Q_ASSERT(widget);
    scrollArea->setWidgetResizable(true);
    scrollArea->setWidget(widget);

    /* add window to mdi area */
    QMdiSubWindow * subWindow = mdiArea->addSubWindow(scrollArea);
    Q_ASSERT(subWindow);
    subWindow->setAttribute(Qt::WA_DeleteOnClose);
    subWindow->setWindowIcon(deviceInstance->treeData(Qt::DecorationRole).value<QIcon>());
    subWindow->setWindowTitle(QObject::tr("Parameter References"));

    scrollArea->show();
}

static void showParameterBlocksWidget(QMdiArea * mdiArea, Project::v20::knx::DeviceInstance_t * deviceInstance)
{
    Q_ASSERT(mdiArea);
    Q_ASSERT(deviceInstance);

    /* ManufacturerData */
    // Product * product = deviceInstance->getProduct();
    // Q_ASSERT(product);
    Project::v20::knx::Hardware2Program_t * hardware2program = deviceInstance->getHardware2Program();
    Q_ASSERT(hardware2program);
    if (hardware2program->ApplicationProgramRef.count() > 1) {
        qWarning() << "Multiple Application Program Refs found for Device Instance ID" << deviceInstance->Id;
    }
    // @todo multiple application program references?
    Project::v20::knx::ApplicationProgramRef_t * applicationProgramRef = hardware2program->ApplicationProgramRef.first();
    Q_ASSERT(applicationProgramRef);
    Project::v20::knx::ApplicationProgram_t * applicationProgram = applicationProgramRef->getApplicationProgram();
    Q_ASSERT(applicationProgram);
    // ApplicationProgramStatic * static_ = ApplicationProgram->Static;
    // Q_ASSERT(Static);
    Project::v20::knx::ApplicationProgramDynamic_t * dynamic = applicationProgram->Dynamic;
    Q_ASSERT(dynamic);

    /* define the window content */
    QScrollArea * scrollArea = new QScrollArea();
    Q_ASSERT(scrollArea);
    QWidget * widget = parameterWidget(dynamic, deviceInstance);
    Q_ASSERT(widget);
    scrollArea->setWidgetResizable(true);
    scrollArea->setWidget(widget);

    /* add window to mdi area */
    auto * subWindow = mdiArea->addSubWindow(scrollArea);
    Q_ASSERT(subWindow);
    subWindow->setAttribute(Qt::WA_DeleteOnClose);
    subWindow->setWindowIcon(deviceInstance->treeData(Qt::DecorationRole).value<QIcon>());
    subWindow->setWindowTitle(QObject::tr("Parameter Blocks"));

    scrollArea->show();
}

QList<QAction *> getActions(QMdiArea * mdiArea, Project::v20::knx::DeviceInstance_t * deviceInstance)
{
    /* actions */
    QAction * parameterRefsAction = new QAction(QObject::tr("Parameter References"));
    Q_ASSERT(parameterRefsAction);
    QObject::connect(parameterRefsAction, &QAction::triggered, deviceInstance, [mdiArea, deviceInstance] {
        showParameterRefsWidget(mdiArea, deviceInstance);
    });

    QAction * parameterBlockAction = new QAction(QObject::tr("Parameter Blocks"));
    Q_ASSERT(parameterBlockAction);
    QObject::connect(parameterBlockAction, &QAction::triggered, deviceInstance, [mdiArea, deviceInstance] {
        showParameterBlocksWidget(mdiArea, deviceInstance);
    });

    QList<QAction *> actions;
    actions.push_back(parameterRefsAction);
    actions.push_back(parameterBlockAction);
    return actions;
}

QMenu * getCustomContextMenu(QMdiArea * mdiArea, Project::v20::knx::DeviceInstance_t * deviceInstance)
{
    Q_ASSERT(mdiArea);
    Q_ASSERT(deviceInstance);

    /* context menu */
    QMenu * contextMenu = new QMenu(QObject::tr("Context Menu"));
    Q_ASSERT(contextMenu);
    QList<QAction *> actions = getActions(mdiArea, deviceInstance);
    contextMenu->addActions(actions);
    // @todo not sure, where to child the actions to...
    for(auto & action: actions) {
        action->setParent(contextMenu);
    }
    return contextMenu;
}
