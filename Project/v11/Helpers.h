#pragma once

#include <Project/v11/knx/DeviceInstance_t.h>
#include <Project/v11/knx/GroupAddress_t.h>
#include <Project/v11/knx/GroupAddresses_t.h>
#include <Project/v11/knx/GroupAddresses_GroupRanges_t.h>
#include <Project/v11/knx/GroupRange_t.h>
#include <Project/v11/knx/ParameterRef_t.h>

namespace Project {
namespace v11 {

knx::GroupAddress_t * findGroupAddress(const knx::GroupAddresses_t * groupAddresses, const uint16_t groupAddress_);
knx::GroupAddress_t * findGroupAddress(const knx::GroupAddresses_GroupRanges_t * groupRanges_, const uint16_t groupAddress_);
knx::GroupAddress_t * findGroupAddress(const knx::GroupRange_t * groupRange, const uint16_t groupAddress_);

struct ParameterValueInfo {
    knx::Value_t parameterValue{}; // Parameter_t or UnionParameter_t

    knx::Value_t parameterRefValue{}; // ParameterRef_t

    bool hasParameterInstanceRef{false}; // AKA is not default
    knx::Value_t parameterInstanceRefValue{}; // ParameterInstanceRef_t

    bool isDefault() const;
    knx::Value_t getValue() const;
};

ParameterValueInfo getParameterValueInfo(const knx::ParameterRef_t * parameterRef, const knx::DeviceInstance_t * deviceInstance);

} // namespace v11
} // namespace Project
