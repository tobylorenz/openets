/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v11/knx/Guid_t.h>
#include <Project/v11/knx/String50_t.h>

namespace Project {
namespace v11 {
namespace knx {

class AddInData_t : public Base
{
    Q_OBJECT

public:
    explicit AddInData_t(Base * parent = nullptr);
    virtual ~AddInData_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    Guid_t AddInId{};
    String50_t Name{};
};

AddInData_t * make_AddInData_t(Base * parent);

} // namespace knx
} // namespace v11
} // namespace Project
