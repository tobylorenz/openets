/* This file is generated. */

#pragma once

#include <Project/xs/UnsignedInt.h>

namespace Project {
namespace v11 {
namespace knx {

using Parameter_Memory_Offset_t = xs::UnsignedInt;

} // namespace knx
} // namespace v11
} // namespace Project
