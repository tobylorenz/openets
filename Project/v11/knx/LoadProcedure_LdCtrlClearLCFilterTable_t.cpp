/* This file is generated. */

#include <Project/v11/knx/LoadProcedure_LdCtrlClearLCFilterTable_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v11/knx/KNX_t.h>

namespace Project {
namespace v11 {
namespace knx {

LoadProcedure_LdCtrlClearLCFilterTable_t::LoadProcedure_LdCtrlClearLCFilterTable_t(Base * parent) :
    Base(parent)
{
}

LoadProcedure_LdCtrlClearLCFilterTable_t::~LoadProcedure_LdCtrlClearLCFilterTable_t()
{
}

void LoadProcedure_LdCtrlClearLCFilterTable_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("AppliesTo")) {
            AppliesTo = attribute.value().toString();
            continue;
        }
        if (name == QString("UseFunctionProp")) {
            UseFunctionProp = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int LoadProcedure_LdCtrlClearLCFilterTable_t::tableColumnCount() const
{
    return 3;
}

QVariant LoadProcedure_LdCtrlClearLCFilterTable_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "LoadProcedure_LdCtrlClearLCFilterTable";
        }
        if (qualifiedName == QString("AppliesTo")) {
            return AppliesTo;
        }
        if (qualifiedName == QString("UseFunctionProp")) {
            return UseFunctionProp;
        }
        break;
    }
    return QVariant();
}

QVariant LoadProcedure_LdCtrlClearLCFilterTable_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "AppliesTo";
            case 2:
                return "UseFunctionProp";
            }
        }
    }
    return QVariant();
}

QVariant LoadProcedure_LdCtrlClearLCFilterTable_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "LdCtrlClearLCFilterTable";
    case Qt::DecorationRole:
        return QIcon::fromTheme("LoadProcedure_LdCtrlClearLCFilterTable");
    }
    return QVariant();
}

LoadProcedure_LdCtrlClearLCFilterTable_t * make_LoadProcedure_LdCtrlClearLCFilterTable_t(Base * parent)
{
    return new LoadProcedure_LdCtrlClearLCFilterTable_t(parent);
}

} // namespace knx
} // namespace v11
} // namespace Project
