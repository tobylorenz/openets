/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v11/knx/GroupRange_t.h>

namespace Project {
namespace v11 {
namespace knx {

class GroupAddresses_GroupRanges_t : public Base
{
    Q_OBJECT

public:
    explicit GroupAddresses_GroupRanges_t(Base * parent = nullptr);
    virtual ~GroupAddresses_GroupRanges_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* elements */
    QMap<xs::ID, GroupRange_t *> GroupRange; // key: Id
};

GroupAddresses_GroupRanges_t * make_GroupAddresses_GroupRanges_t(Base * parent);

} // namespace knx
} // namespace v11
} // namespace Project
