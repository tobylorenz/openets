/* This file is generated. */

#include <Project/v11/knx/When_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v11/knx/KNX_t.h>

namespace Project {
namespace v11 {
namespace knx {

When_t::When_t(Base * parent) :
    Base(parent)
{
}

When_t::~When_t()
{
}

void When_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("test")) {
            Test = attribute.value().toString();
            continue;
        }
        if (name == QString("default")) {
            Default = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int When_t::tableColumnCount() const
{
    return 3;
}

QVariant When_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "When";
        }
        if (qualifiedName == QString("test")) {
            return Test;
        }
        if (qualifiedName == QString("default")) {
            return Default;
        }
        break;
    }
    return QVariant();
}

QVariant When_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Test";
            case 2:
                return "Default";
            }
        }
    }
    return QVariant();
}

QVariant When_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return Default == "true" ? "default" : Test;
    case Qt::DecorationRole:
        return QIcon::fromTheme("When");
    }
    return QVariant();
}

When_t * make_When_t(Base * parent)
{
    return new When_t(parent);
}

} // namespace knx
} // namespace v11
} // namespace Project
