/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v11/knx/BitOffset_t.h>
#include <Project/v11/knx/IDREF.h>
#include <Project/v11/knx/Parameter_Memory_Offset_t.h>

namespace Project {
namespace v11 {
namespace knx {

/* forward declarations */
class ApplicationProgramStatic_Code_AbsoluteSegment_t;

class Parameter_Memory_t : public Base
{
    Q_OBJECT

public:
    explicit Parameter_Memory_t(Base * parent = nullptr);
    virtual ~Parameter_Memory_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    IDREF CodeSegment{};
    Parameter_Memory_Offset_t Offset{};
    BitOffset_t BitOffset{};

    /* getters */
    ApplicationProgramStatic_Code_AbsoluteSegment_t * getCodeSegment() const; // attribute: CodeSegment
};

Parameter_Memory_t * make_Parameter_Memory_t(Base * parent);

} // namespace knx
} // namespace v11
} // namespace Project
