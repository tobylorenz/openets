/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v11/knx/IDREF.h>
#include <Project/v11/knx/LanguageDependentString255_t.h>
#include <Project/v11/knx/String50_t.h>
#include <Project/xs/ID.h>

namespace Project {
namespace v11 {
namespace knx {

class ParameterBlockRename_t : public Base
{
    Q_OBJECT

public:
    explicit ParameterBlockRename_t(Base * parent = nullptr);
    virtual ~ParameterBlockRename_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::ID Id{};
    IDREF RefId{};
    String50_t Name{};
    LanguageDependentString255_t Text{};
};

ParameterBlockRename_t * make_ParameterBlockRename_t(Base * parent);

} // namespace knx
} // namespace v11
} // namespace Project
