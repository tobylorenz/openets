/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v11/knx/IDREF.h>
#include <Project/xs/Boolean.h>

namespace Project {
namespace v11 {
namespace knx {

/* forward declarations */
class GroupAddress_t;

class ComObjectInstanceRef_Connectors_Send_t : public Base
{
    Q_OBJECT

public:
    explicit ComObjectInstanceRef_Connectors_Send_t(Base * parent = nullptr);
    virtual ~ComObjectInstanceRef_Connectors_Send_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    IDREF GroupAddressRefId{};
    xs::Boolean Acknowledge{"false"};

    /* getters */
    GroupAddress_t * getGroupAddress() const; // attribute: GroupAddressRefId
};

ComObjectInstanceRef_Connectors_Send_t * make_ComObjectInstanceRef_Connectors_Send_t(Base * parent);

} // namespace knx
} // namespace v11
} // namespace Project
