/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v11/knx/LdCtrlMemAddrSpace_t.h>
#include <Project/v11/knx/LdCtrlProcType_t.h>
#include <Project/xs/UnsignedInt.h>

namespace Project {
namespace v11 {
namespace knx {

class LoadProcedure_LdCtrlLoadImageMem_t : public Base
{
    Q_OBJECT

public:
    explicit LoadProcedure_LdCtrlLoadImageMem_t(Base * parent = nullptr);
    virtual ~LoadProcedure_LdCtrlLoadImageMem_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    LdCtrlMemAddrSpace_t AddressSpace{"Standard"};
    xs::UnsignedInt Address{};
    xs::UnsignedInt Size{};
    LdCtrlProcType_t AppliesTo{"auto"};
};

LoadProcedure_LdCtrlLoadImageMem_t * make_LoadProcedure_LdCtrlLoadImageMem_t(Base * parent);

} // namespace knx
} // namespace v11
} // namespace Project
