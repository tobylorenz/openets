/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v11/knx/DeviceInstance_AdditionalAddresses_Address_t.h>

namespace Project {
namespace v11 {
namespace knx {

class DeviceInstance_AdditionalAddresses_t : public Base
{
    Q_OBJECT

public:
    explicit DeviceInstance_AdditionalAddresses_t(Base * parent = nullptr);
    virtual ~DeviceInstance_AdditionalAddresses_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* elements */
    QVector<DeviceInstance_AdditionalAddresses_Address_t *> Address;
};

DeviceInstance_AdditionalAddresses_t * make_DeviceInstance_AdditionalAddresses_t(Base * parent);

} // namespace knx
} // namespace v11
} // namespace Project
