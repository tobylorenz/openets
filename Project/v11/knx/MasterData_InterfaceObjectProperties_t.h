/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v11/knx/MasterData_InterfaceObjectProperties_InterfaceObjectProperty_t.h>
#include <Project/xs/ID.h>

namespace Project {
namespace v11 {
namespace knx {

class MasterData_InterfaceObjectProperties_t : public Base
{
    Q_OBJECT

public:
    explicit MasterData_InterfaceObjectProperties_t(Base * parent = nullptr);
    virtual ~MasterData_InterfaceObjectProperties_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* elements */
    QMap<xs::ID, MasterData_InterfaceObjectProperties_InterfaceObjectProperty_t *> InterfaceObjectProperty; // key: Id
};

MasterData_InterfaceObjectProperties_t * make_MasterData_InterfaceObjectProperties_t(Base * parent);

} // namespace knx
} // namespace v11
} // namespace Project
