/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v11/knx/Hardware2Program_t.h>

namespace Project {
namespace v11 {
namespace knx {

class Hardware_Hardware2Programs_t : public Base
{
    Q_OBJECT

public:
    explicit Hardware_Hardware2Programs_t(Base * parent = nullptr);
    virtual ~Hardware_Hardware2Programs_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* elements */
    QMap<xs::ID, Hardware2Program_t *> Hardware2Program; // key: Id
};

Hardware_Hardware2Programs_t * make_Hardware_Hardware2Programs_t(Base * parent);

} // namespace knx
} // namespace v11
} // namespace Project
