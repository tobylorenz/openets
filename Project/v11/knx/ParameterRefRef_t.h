/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v11/knx/IDREF.h>

namespace Project {
namespace v11 {
namespace knx {

/* forward declarations */
class ParameterRef_t;

class ParameterRefRef_t : public Base
{
    Q_OBJECT

public:
    explicit ParameterRefRef_t(Base * parent = nullptr);
    virtual ~ParameterRefRef_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    IDREF RefId{};

    /* getters */
    ParameterRef_t * getParameterRef() const; // attribute: RefId
};

ParameterRefRef_t * make_ParameterRefRef_t(Base * parent);

} // namespace knx
} // namespace v11
} // namespace Project
