/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v11/knx/IDREF.h>
#include <Project/v11/knx/LanguageData_TranslationUnit_TranslationElement_Translation_t.h>

namespace Project {
namespace v11 {
namespace knx {

class LanguageData_TranslationUnit_TranslationElement_t : public Base
{
    Q_OBJECT

public:
    explicit LanguageData_TranslationUnit_TranslationElement_t(Base * parent = nullptr);
    virtual ~LanguageData_TranslationUnit_TranslationElement_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    IDREF RefId{};

    /* elements */
    QVector<LanguageData_TranslationUnit_TranslationElement_Translation_t *> Translation;

    /* getters */
    Base * getTranslationElement() const; // attribute: RefId
};

LanguageData_TranslationUnit_TranslationElement_t * make_LanguageData_TranslationUnit_TranslationElement_t(Base * parent);

} // namespace knx
} // namespace v11
} // namespace Project
