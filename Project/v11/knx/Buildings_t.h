/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v11/knx/BuildingPart_t.h>

namespace Project {
namespace v11 {
namespace knx {

class Buildings_t : public Base
{
    Q_OBJECT

public:
    explicit Buildings_t(Base * parent = nullptr);
    virtual ~Buildings_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* elements */
    QMap<xs::ID, BuildingPart_t *> BuildingPart; // key: Id
};

Buildings_t * make_Buildings_t(Base * parent);

} // namespace knx
} // namespace v11
} // namespace Project
