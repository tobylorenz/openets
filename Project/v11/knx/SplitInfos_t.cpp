/* This file is generated. */

#include <Project/v11/knx/SplitInfos_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v11/knx/KNX_t.h>

namespace Project {
namespace v11 {
namespace knx {

SplitInfos_t::SplitInfos_t(Base * parent) :
    Base(parent)
{
}

SplitInfos_t::~SplitInfos_t()
{
}

void SplitInfos_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("SplitInfo")) {
            auto * newSplitInfo = make_SplitInfo_t(this);
            newSplitInfo->read(reader);
            SplitInfo.append(newSplitInfo);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int SplitInfos_t::tableColumnCount() const
{
    return 1;
}

QVariant SplitInfos_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "SplitInfos";
        }
        break;
    }
    return QVariant();
}

QVariant SplitInfos_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant SplitInfos_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "SplitInfos";
    case Qt::DecorationRole:
        return QIcon::fromTheme("SplitInfos");
    }
    return QVariant();
}

SplitInfos_t * make_SplitInfos_t(Base * parent)
{
    return new SplitInfos_t(parent);
}

} // namespace knx
} // namespace v11
} // namespace Project
