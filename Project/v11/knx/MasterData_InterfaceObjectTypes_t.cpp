/* This file is generated. */

#include <Project/v11/knx/MasterData_InterfaceObjectTypes_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v11/knx/KNX_t.h>

namespace Project {
namespace v11 {
namespace knx {

MasterData_InterfaceObjectTypes_t::MasterData_InterfaceObjectTypes_t(Base * parent) :
    Base(parent)
{
}

MasterData_InterfaceObjectTypes_t::~MasterData_InterfaceObjectTypes_t()
{
}

void MasterData_InterfaceObjectTypes_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("InterfaceObjectType")) {
            QString newId = reader.attributes().value("Id").toString();
            Q_ASSERT(!newId.isEmpty());
            MasterData_InterfaceObjectTypes_InterfaceObjectType_t * newInterfaceObjectType;
            if (InterfaceObjectType.contains(newId)) {
                newInterfaceObjectType = InterfaceObjectType[newId];
            } else {
                newInterfaceObjectType = make_MasterData_InterfaceObjectTypes_InterfaceObjectType_t(this);
                InterfaceObjectType[newId] = newInterfaceObjectType;
            }
            newInterfaceObjectType->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int MasterData_InterfaceObjectTypes_t::tableColumnCount() const
{
    return 1;
}

QVariant MasterData_InterfaceObjectTypes_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "MasterData_InterfaceObjectTypes";
        }
        break;
    }
    return QVariant();
}

QVariant MasterData_InterfaceObjectTypes_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant MasterData_InterfaceObjectTypes_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "InterfaceObjectTypes";
    case Qt::DecorationRole:
        return QIcon::fromTheme("MasterData_InterfaceObjectTypes");
    }
    return QVariant();
}

MasterData_InterfaceObjectTypes_t * make_MasterData_InterfaceObjectTypes_t(Base * parent)
{
    return new MasterData_InterfaceObjectTypes_t(parent);
}

} // namespace knx
} // namespace v11
} // namespace Project
