/* This file is generated. */

#pragma once

#include <Project/xs/String.h>

namespace Project {
namespace v11 {
namespace knx {

using ParameterType_TypeTime_Unit_t = xs::String;

} // namespace knx
} // namespace v11
} // namespace Project
