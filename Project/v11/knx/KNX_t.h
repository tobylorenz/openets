/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v11/knx/Project_t.h>
#include <Project/xs/String.h>

namespace Project {
namespace v11 {
namespace knx {

/* forward declarations */
class ManufacturerData_t;
class MasterData_t;

class KNX_t : public Base
{
    Q_OBJECT

public:
    explicit KNX_t(Base * parent = nullptr);
    virtual ~KNX_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::String CreatedBy{};
    xs::String ToolVersion{};

    /* elements */
    MasterData_t * MasterData{};
    ManufacturerData_t * ManufacturerData{};
    QMap<xs::ID, Project_t *> Project; // key: Id

    /** map of all known Ids */
    QMap<xs::ID, Base *> ids{};
};

KNX_t * make_KNX_t(Base * parent);

} // namespace knx
} // namespace v11
} // namespace Project
