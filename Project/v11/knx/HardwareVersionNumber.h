/* This file is generated. */

#pragma once

#include <Project/xs/UnsignedShort.h>

namespace Project {
namespace v11 {
namespace knx {

using HardwareVersionNumber = xs::UnsignedShort;

} // namespace knx
} // namespace v11
} // namespace Project
