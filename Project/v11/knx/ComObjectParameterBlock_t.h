/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v11/knx/Access_t.h>
#include <Project/v11/knx/IDREF.h>
#include <Project/v11/knx/LanguageDependentString255_t.h>
#include <Project/v11/knx/String50_t.h>
#include <Project/xs/ID.h>
#include <Project/xs/String.h>
#include <Project/xs/UnsignedInt.h>

namespace Project {
namespace v11 {
namespace knx {

/* forward declarations */
class Assign_t;
class BinaryDataRef_t;
class ComObjectParameterChoose_t;
class ComObjectRefRef_t;
class ParameterRefRef_t;
class ParameterRef_t;
class ParameterSeparator_t;

class ComObjectParameterBlock_t : public Base
{
    Q_OBJECT

public:
    explicit ComObjectParameterBlock_t(Base * parent = nullptr);
    virtual ~ComObjectParameterBlock_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::ID Id{};
    String50_t Name{};
    LanguageDependentString255_t Text{};
    Access_t Access{"ReadWrite"};
    xs::UnsignedInt HelpTopic{};
    xs::String InternalDescription{};
    IDREF ParamRefId{};

    /* elements */
    // xs:choice ParameterSeparator_t * ParameterSeparator{};
    // xs:choice ParameterRefRef_t * ParameterRefRef{};
    // xs:choice ComObjectParameterChoose_t * Choose{};
    // xs:choice BinaryDataRef_t * BinaryDataRef{};
    // xs:choice ComObjectRefRef_t * ComObjectRefRef{};
    // xs:choice Assign_t * Assign{};

    /* getters */
    ParameterRef_t * getParameterRef() const; // attribute: ParamRefId
};

ComObjectParameterBlock_t * make_ComObjectParameterBlock_t(Base * parent);

} // namespace knx
} // namespace v11
} // namespace Project
