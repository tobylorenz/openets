/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v11/knx/IDREF.h>

namespace Project {
namespace v11 {
namespace knx {

/* forward declarations */
class ComObjectRef_t;

class ComObjectRefRef_t : public Base
{
    Q_OBJECT

public:
    explicit ComObjectRefRef_t(Base * parent = nullptr);
    virtual ~ComObjectRefRef_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    IDREF RefId{};

    /* getters */
    ComObjectRef_t * getComObjectRef() const; // attribute: RefId
};

ComObjectRefRef_t * make_ComObjectRefRef_t(Base * parent);

} // namespace knx
} // namespace v11
} // namespace Project
