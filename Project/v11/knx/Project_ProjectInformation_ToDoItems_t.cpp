/* This file is generated. */

#include <Project/v11/knx/Project_ProjectInformation_ToDoItems_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v11/knx/KNX_t.h>

namespace Project {
namespace v11 {
namespace knx {

Project_ProjectInformation_ToDoItems_t::Project_ProjectInformation_ToDoItems_t(Base * parent) :
    Base(parent)
{
}

Project_ProjectInformation_ToDoItems_t::~Project_ProjectInformation_ToDoItems_t()
{
}

void Project_ProjectInformation_ToDoItems_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("ToDoItem")) {
            auto * newToDoItem = make_ToDoItem_t(this);
            newToDoItem->read(reader);
            ToDoItem.append(newToDoItem);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int Project_ProjectInformation_ToDoItems_t::tableColumnCount() const
{
    return 1;
}

QVariant Project_ProjectInformation_ToDoItems_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "Project_ProjectInformation_ToDoItems";
        }
        break;
    }
    return QVariant();
}

QVariant Project_ProjectInformation_ToDoItems_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant Project_ProjectInformation_ToDoItems_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "ToDoItems";
    case Qt::DecorationRole:
        return QIcon::fromTheme("Project_ProjectInformation_ToDoItems");
    }
    return QVariant();
}

Project_ProjectInformation_ToDoItems_t * make_Project_ProjectInformation_ToDoItems_t(Base * parent)
{
    return new Project_ProjectInformation_ToDoItems_t(parent);
}

} // namespace knx
} // namespace v11
} // namespace Project
