/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v11/knx/Hardware_t.h>

namespace Project {
namespace v11 {
namespace knx {

class ManufacturerData_Manufacturer_Hardware_t : public Base
{
    Q_OBJECT

public:
    explicit ManufacturerData_Manufacturer_Hardware_t(Base * parent = nullptr);
    virtual ~ManufacturerData_Manufacturer_Hardware_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* elements */
    QMap<xs::ID, Hardware_t *> Hardware; // key: Id
};

ManufacturerData_Manufacturer_Hardware_t * make_ManufacturerData_Manufacturer_Hardware_t(Base * parent);

} // namespace knx
} // namespace v11
} // namespace Project
