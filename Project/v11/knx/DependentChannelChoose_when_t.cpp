/* This file is generated. */

#include <Project/v11/knx/DependentChannelChoose_when_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v11/knx/ApplicationProgramChannel_t.h>
#include <Project/v11/knx/DependentChannelChoose_t.h>
#include <Project/v11/knx/KNX_t.h>
#include <Project/v11/knx/ParameterBlockRename_t.h>

namespace Project {
namespace v11 {
namespace knx {

DependentChannelChoose_when_t::DependentChannelChoose_when_t(Base * parent) :
    Base(parent)
{
}

DependentChannelChoose_when_t::~DependentChannelChoose_when_t()
{
}

void DependentChannelChoose_when_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("test")) {
            Test = attribute.value().toString();
            continue;
        }
        if (name == QString("default")) {
            Default = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("Channel")) {
            auto * Channel = make_ApplicationProgramChannel_t(this);
            Channel->read(reader);
            continue;
        }
        if (reader.name() == QString("choose")) {
            auto * Choose = make_DependentChannelChoose_t(this);
            Choose->read(reader);
            continue;
        }
        if (reader.name() == QString("ParameterBlockRename")) {
            auto * ParameterBlockRename = make_ParameterBlockRename_t(this);
            ParameterBlockRename->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int DependentChannelChoose_when_t::tableColumnCount() const
{
    return 3;
}

QVariant DependentChannelChoose_when_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "DependentChannelChoose_when";
        }
        if (qualifiedName == QString("test")) {
            return Test;
        }
        if (qualifiedName == QString("default")) {
            return Default;
        }
        break;
    }
    return QVariant();
}

QVariant DependentChannelChoose_when_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Test";
            case 2:
                return "Default";
            }
        }
    }
    return QVariant();
}

QVariant DependentChannelChoose_when_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "when";
    case Qt::DecorationRole:
        return QIcon::fromTheme("DependentChannelChoose_when");
    }
    return QVariant();
}

DependentChannelChoose_when_t * make_DependentChannelChoose_when_t(Base * parent)
{
    return new DependentChannelChoose_when_t(parent);
}

} // namespace knx
} // namespace v11
} // namespace Project
