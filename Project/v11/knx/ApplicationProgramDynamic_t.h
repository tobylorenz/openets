/* This file is generated. */

#pragma once

#include <Project/Base.h>

namespace Project {
namespace v11 {
namespace knx {

/* forward declarations */
class ApplicationProgramChannel_t;
class ApplicationProgramDynamic_ChannelIndependentBlock_t;
class DependentChannelChoose_t;

class ApplicationProgramDynamic_t : public Base
{
    Q_OBJECT

public:
    explicit ApplicationProgramDynamic_t(Base * parent = nullptr);
    virtual ~ApplicationProgramDynamic_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* elements */
    // xs:choice ApplicationProgramDynamic_ChannelIndependentBlock_t * ChannelIndependentBlock{};
    // xs:choice ApplicationProgramChannel_t * Channel{};
    // xs:choice DependentChannelChoose_t * Choose{};
};

ApplicationProgramDynamic_t * make_ApplicationProgramDynamic_t(Base * parent);

} // namespace knx
} // namespace v11
} // namespace Project
