/* This file is generated. */

#include <Project/v11/knx/ParameterType_TypeDate_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v11/knx/KNX_t.h>

namespace Project {
namespace v11 {
namespace knx {

ParameterType_TypeDate_t::ParameterType_TypeDate_t(Base * parent) :
    Base(parent)
{
}

ParameterType_TypeDate_t::~ParameterType_TypeDate_t()
{
}

void ParameterType_TypeDate_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("Encoding")) {
            Encoding = attribute.value().toString();
            continue;
        }
        if (name == QString("DisplayTheYear")) {
            DisplayTheYear = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ParameterType_TypeDate_t::tableColumnCount() const
{
    return 3;
}

QVariant ParameterType_TypeDate_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ParameterType_TypeDate";
        }
        if (qualifiedName == QString("Encoding")) {
            return Encoding;
        }
        if (qualifiedName == QString("DisplayTheYear")) {
            return DisplayTheYear;
        }
        break;
    }
    return QVariant();
}

QVariant ParameterType_TypeDate_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Encoding";
            case 2:
                return "DisplayTheYear";
            }
        }
    }
    return QVariant();
}

QVariant ParameterType_TypeDate_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "TypeDate";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ParameterType_TypeDate");
    }
    return QVariant();
}

ParameterType_TypeDate_t * make_ParameterType_TypeDate_t(Base * parent)
{
    return new ParameterType_TypeDate_t(parent);
}

} // namespace knx
} // namespace v11
} // namespace Project
