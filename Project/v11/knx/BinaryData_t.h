/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/SimpleElementTextType.h>
#include <Project/v11/knx/String50_t.h>
#include <Project/xs/ID.h>
#include <Project/xs/Base64Binary.h>

namespace Project {
namespace v11 {
namespace knx {

class BinaryData_t : public Base
{
    Q_OBJECT

public:
    explicit BinaryData_t(Base * parent = nullptr);
    virtual ~BinaryData_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    String50_t Name{};
    xs::ID Id{};

    /* elements */
    SimpleElementTextType * Data{};
};

BinaryData_t * make_BinaryData_t(Base * parent);

} // namespace knx
} // namespace v11
} // namespace Project
