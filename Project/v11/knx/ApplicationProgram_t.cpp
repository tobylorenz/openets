/* This file is generated. */

#include <Project/v11/knx/ApplicationProgram_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v11/knx/ApplicationProgramDynamic_t.h>
#include <Project/v11/knx/ApplicationProgramStatic_t.h>
#include <Project/v11/knx/KNX_t.h>
#include <Project/v11/knx/MaskVersion_t.h>
#include <Project/v11/knx/MasterData_Manufacturers_Manufacturer_t.h>

namespace Project {
namespace v11 {
namespace knx {

ApplicationProgram_t::ApplicationProgram_t(Base * parent) :
    Base(parent)
{
}

ApplicationProgram_t::~ApplicationProgram_t()
{
}

void ApplicationProgram_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == "Id") {
            Id = attribute.value().toString();
            KNX_t * knx = findParent<KNX_t *>();
            Q_ASSERT(knx);
            knx->ids[Id] = this;
            continue;
        }
        if (name == QString("ApplicationNumber")) {
            ApplicationNumber = attribute.value().toString();
            continue;
        }
        if (name == QString("ApplicationVersion")) {
            ApplicationVersion = attribute.value().toString();
            continue;
        }
        if (name == QString("ProgramType")) {
            ProgramType = attribute.value().toString();
            continue;
        }
        if (name == QString("MaskVersion")) {
            MaskVersion = attribute.value().toString();
            continue;
        }
        if (name == QString("VisibleDescription")) {
            VisibleDescription = attribute.value().toString();
            continue;
        }
        if (name == QString("Name")) {
            Name = attribute.value().toString();
            continue;
        }
        if (name == QString("LoadProcedureStyle")) {
            LoadProcedureStyle = attribute.value().toString();
            continue;
        }
        if (name == QString("PeiType")) {
            PeiType = attribute.value().toString();
            continue;
        }
        if (name == QString("HelpTopic")) {
            HelpTopic = attribute.value().toString();
            continue;
        }
        if (name == QString("HelpFile")) {
            HelpFile = attribute.value().toString();
            continue;
        }
        if (name == QString("DefaultLanguage")) {
            DefaultLanguage = attribute.value().toString();
            continue;
        }
        if (name == QString("DynamicTableManagement")) {
            DynamicTableManagement = attribute.value().toString();
            continue;
        }
        if (name == QString("Linkable")) {
            Linkable = attribute.value().toString();
            continue;
        }
        if (name == QString("MinEtsVersion")) {
            MinEtsVersion = attribute.value().toString();
            continue;
        }
        if (name == QString("OriginalManufacturer")) {
            OriginalManufacturer = attribute.value().toString();
            continue;
        }
        if (name == QString("PreEts4Style")) {
            PreEts4Style = attribute.value().toString();
            continue;
        }
        if (name == QString("ConvertedFromPreEts4Data")) {
            ConvertedFromPreEts4Data = attribute.value().toString();
            continue;
        }
        if (name == QString("CreatedFromLegacySchemaVersion")) {
            CreatedFromLegacySchemaVersion = attribute.value().toString();
            continue;
        }
        if (name == QString("IPConfig")) {
            IPConfig = attribute.value().toString();
            continue;
        }
        if (name == QString("AdditionalAddressesCount")) {
            AdditionalAddressesCount = attribute.value().toString();
            continue;
        }
        if (name == QString("NonRegRelevantDataVersion")) {
            NonRegRelevantDataVersion = attribute.value().toString();
            continue;
        }
        if (name == QString("Broken")) {
            Broken = attribute.value().toString();
            continue;
        }
        if (name == QString("DownloadInfoIncomplete")) {
            DownloadInfoIncomplete = attribute.value().toString();
            continue;
        }
        if (name == QString("ReplacesVersions")) {
            ReplacesVersions = attribute.value().toString().split(' ');
            continue;
        }
        if (name == QString("Hash")) {
            Hash = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("Static")) {
            if (!Static) {
                Static = make_ApplicationProgramStatic_t(this);
            }
            Static->read(reader);
            continue;
        }
        if (reader.name() == QString("Dynamic")) {
            if (!Dynamic) {
                Dynamic = make_ApplicationProgramDynamic_t(this);
            }
            Dynamic->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ApplicationProgram_t::tableColumnCount() const
{
    return 27;
}

QVariant ApplicationProgram_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ApplicationProgram";
        }
        if (qualifiedName == QString("Id")) {
            return Id;
        }
        if (qualifiedName == QString("ApplicationNumber")) {
            return ApplicationNumber;
        }
        if (qualifiedName == QString("ApplicationVersion")) {
            return ApplicationVersion;
        }
        if (qualifiedName == QString("ProgramType")) {
            return ProgramType;
        }
        if (qualifiedName == QString("MaskVersion")) {
            return MaskVersion;
        }
        if (qualifiedName == QString("VisibleDescription")) {
            return VisibleDescription;
        }
        if (qualifiedName == QString("Name")) {
            return Name;
        }
        if (qualifiedName == QString("LoadProcedureStyle")) {
            return LoadProcedureStyle;
        }
        if (qualifiedName == QString("PeiType")) {
            return PeiType;
        }
        if (qualifiedName == QString("HelpTopic")) {
            return HelpTopic;
        }
        if (qualifiedName == QString("HelpFile")) {
            return HelpFile;
        }
        if (qualifiedName == QString("DefaultLanguage")) {
            return DefaultLanguage;
        }
        if (qualifiedName == QString("DynamicTableManagement")) {
            return DynamicTableManagement;
        }
        if (qualifiedName == QString("Linkable")) {
            return Linkable;
        }
        if (qualifiedName == QString("MinEtsVersion")) {
            return MinEtsVersion;
        }
        if (qualifiedName == QString("OriginalManufacturer")) {
            return OriginalManufacturer;
        }
        if (qualifiedName == QString("PreEts4Style")) {
            return PreEts4Style;
        }
        if (qualifiedName == QString("ConvertedFromPreEts4Data")) {
            return ConvertedFromPreEts4Data;
        }
        if (qualifiedName == QString("CreatedFromLegacySchemaVersion")) {
            return CreatedFromLegacySchemaVersion;
        }
        if (qualifiedName == QString("IPConfig")) {
            return IPConfig;
        }
        if (qualifiedName == QString("AdditionalAddressesCount")) {
            return AdditionalAddressesCount;
        }
        if (qualifiedName == QString("NonRegRelevantDataVersion")) {
            return NonRegRelevantDataVersion;
        }
        if (qualifiedName == QString("Broken")) {
            return Broken;
        }
        if (qualifiedName == QString("DownloadInfoIncomplete")) {
            return DownloadInfoIncomplete;
        }
        if (qualifiedName == QString("ReplacesVersions")) {
            return ReplacesVersions.join(' ');
        }
        if (qualifiedName == QString("Hash")) {
            return Hash;
        }
        break;
    }
    return QVariant();
}

QVariant ApplicationProgram_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Id";
            case 2:
                return "ApplicationNumber";
            case 3:
                return "ApplicationVersion";
            case 4:
                return "ProgramType";
            case 5:
                return "MaskVersion";
            case 6:
                return "VisibleDescription";
            case 7:
                return "Name";
            case 8:
                return "LoadProcedureStyle";
            case 9:
                return "PeiType";
            case 10:
                return "HelpTopic";
            case 11:
                return "HelpFile";
            case 12:
                return "DefaultLanguage";
            case 13:
                return "DynamicTableManagement";
            case 14:
                return "Linkable";
            case 15:
                return "MinEtsVersion";
            case 16:
                return "OriginalManufacturer";
            case 17:
                return "PreEts4Style";
            case 18:
                return "ConvertedFromPreEts4Data";
            case 19:
                return "CreatedFromLegacySchemaVersion";
            case 20:
                return "IPConfig";
            case 21:
                return "AdditionalAddressesCount";
            case 22:
                return "NonRegRelevantDataVersion";
            case 23:
                return "Broken";
            case 24:
                return "DownloadInfoIncomplete";
            case 25:
                return "ReplacesVersions";
            case 26:
                return "Hash";
            }
        }
    }
    return QVariant();
}

QVariant ApplicationProgram_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return QString("[%1 %2] %3").arg(ApplicationNumber).arg(ApplicationVersion).arg(Name);
    case Qt::DecorationRole:
        return QIcon::fromTheme(PeiType == "1" ? "ApplicationProgram_Pei" : "ApplicationProgram");
    }
    return QVariant();
}

MaskVersion_t * ApplicationProgram_t::getMaskVersion() const
{
    if (MaskVersion.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<MaskVersion_t *>(knx->ids[MaskVersion]);
}

MasterData_Manufacturers_Manufacturer_t * ApplicationProgram_t::getOriginalManufacturer() const
{
    if (OriginalManufacturer.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<MasterData_Manufacturers_Manufacturer_t *>(knx->ids[OriginalManufacturer]);
}

ApplicationProgram_t * make_ApplicationProgram_t(Base * parent)
{
    return new ApplicationProgram_t(parent);
}

} // namespace knx
} // namespace v11
} // namespace Project
