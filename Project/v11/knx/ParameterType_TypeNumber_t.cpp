/* This file is generated. */

#include <Project/v11/knx/ParameterType_TypeNumber_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v11/knx/KNX_t.h>

namespace Project {
namespace v11 {
namespace knx {

ParameterType_TypeNumber_t::ParameterType_TypeNumber_t(Base * parent) :
    Base(parent)
{
}

ParameterType_TypeNumber_t::~ParameterType_TypeNumber_t()
{
}

void ParameterType_TypeNumber_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("SizeInBit")) {
            SizeInBit = attribute.value().toString();
            continue;
        }
        if (name == QString("Type")) {
            Type = attribute.value().toString();
            continue;
        }
        if (name == QString("minInclusive")) {
            minInclusive = attribute.value().toString();
            continue;
        }
        if (name == QString("maxInclusive")) {
            maxInclusive = attribute.value().toString();
            continue;
        }
        if (name == QString("UIHint")) {
            UIHint = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ParameterType_TypeNumber_t::tableColumnCount() const
{
    return 6;
}

QVariant ParameterType_TypeNumber_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ParameterType_TypeNumber";
        }
        if (qualifiedName == QString("SizeInBit")) {
            return SizeInBit;
        }
        if (qualifiedName == QString("Type")) {
            return Type;
        }
        if (qualifiedName == QString("minInclusive")) {
            return minInclusive;
        }
        if (qualifiedName == QString("maxInclusive")) {
            return maxInclusive;
        }
        if (qualifiedName == QString("UIHint")) {
            return UIHint;
        }
        break;
    }
    return QVariant();
}

QVariant ParameterType_TypeNumber_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "SizeInBit";
            case 2:
                return "Type";
            case 3:
                return "minInclusive";
            case 4:
                return "maxInclusive";
            case 5:
                return "UIHint";
            }
        }
    }
    return QVariant();
}

QVariant ParameterType_TypeNumber_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "TypeNumber";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ParameterType_TypeNumber");
    }
    return QVariant();
}

ParameterType_TypeNumber_t * make_ParameterType_TypeNumber_t(Base * parent)
{
    return new ParameterType_TypeNumber_t(parent);
}

} // namespace knx
} // namespace v11
} // namespace Project
