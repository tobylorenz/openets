/* This file is generated. */

#include <Project/v11/knx/ParameterType_TypeIPAddress_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v11/knx/KNX_t.h>

namespace Project {
namespace v11 {
namespace knx {

ParameterType_TypeIPAddress_t::ParameterType_TypeIPAddress_t(Base * parent) :
    Base(parent)
{
}

ParameterType_TypeIPAddress_t::~ParameterType_TypeIPAddress_t()
{
}

void ParameterType_TypeIPAddress_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("AddressType")) {
            AddressType = attribute.value().toString();
            continue;
        }
        if (name == QString("Version")) {
            Version = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ParameterType_TypeIPAddress_t::tableColumnCount() const
{
    return 3;
}

QVariant ParameterType_TypeIPAddress_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ParameterType_TypeIPAddress";
        }
        if (qualifiedName == QString("AddressType")) {
            return AddressType;
        }
        if (qualifiedName == QString("Version")) {
            return Version;
        }
        break;
    }
    return QVariant();
}

QVariant ParameterType_TypeIPAddress_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "AddressType";
            case 2:
                return "Version";
            }
        }
    }
    return QVariant();
}

QVariant ParameterType_TypeIPAddress_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "TypeIPAddress";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ParameterType_TypeIPAddress");
    }
    return QVariant();
}

ParameterType_TypeIPAddress_t * make_ParameterType_TypeIPAddress_t(Base * parent)
{
    return new ParameterType_TypeIPAddress_t(parent);
}

} // namespace knx
} // namespace v11
} // namespace Project
