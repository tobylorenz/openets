/* This file is generated. */

#pragma once

#include <Project/xs/List.h>
#include <Project/xs/String.h>

namespace Project {
namespace v11 {
namespace knx {

using MasterData_Manufacturers_Manufacturer_ImportGroup_t = xs::List<xs::String>;

} // namespace knx
} // namespace v11
} // namespace Project
