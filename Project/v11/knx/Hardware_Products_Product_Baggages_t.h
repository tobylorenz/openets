/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v11/knx/Hardware_Products_Product_Baggages_Baggage_t.h>
#include <Project/v11/knx/IDREF.h>

namespace Project {
namespace v11 {
namespace knx {

class Hardware_Products_Product_Baggages_t : public Base
{
    Q_OBJECT

public:
    explicit Hardware_Products_Product_Baggages_t(Base * parent = nullptr);
    virtual ~Hardware_Products_Product_Baggages_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* elements */
    QMap<IDREF, Hardware_Products_Product_Baggages_Baggage_t *> Baggage; // key: RefId
};

Hardware_Products_Product_Baggages_t * make_Hardware_Products_Product_Baggages_t(Base * parent);

} // namespace knx
} // namespace v11
} // namespace Project
