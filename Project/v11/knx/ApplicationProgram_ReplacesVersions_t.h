/* This file is generated. */

#pragma once

#include <Project/xs/List.h>
#include <Project/xs/UnsignedByte.h>

namespace Project {
namespace v11 {
namespace knx {

using ApplicationProgram_ReplacesVersions_t = xs::List<xs::UnsignedByte>;

} // namespace knx
} // namespace v11
} // namespace Project
