/* This file is generated. */

#pragma once

#include <Project/xs/UnsignedByte.h>

namespace Project {
namespace v11 {
namespace knx {

using DeviceInstance_AdditionalAddresses_Address_Address_t = xs::UnsignedByte;

} // namespace knx
} // namespace v11
} // namespace Project
