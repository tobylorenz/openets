/* This file is generated. */

#pragma once

#include <Project/xs/String.h>

namespace Project {
namespace v11 {
namespace knx {

using HawkConfigurationData_Resources_Resource_ResourceType_Flavour_t = xs::String;

} // namespace knx
} // namespace v11
} // namespace Project
