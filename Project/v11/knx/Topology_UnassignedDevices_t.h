/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v11/knx/DeviceInstance_t.h>

namespace Project {
namespace v11 {
namespace knx {

class Topology_UnassignedDevices_t : public Base
{
    Q_OBJECT

public:
    explicit Topology_UnassignedDevices_t(Base * parent = nullptr);
    virtual ~Topology_UnassignedDevices_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* elements */
    QMap<xs::ID, DeviceInstance_t *> DeviceInstance; // key: Id
};

Topology_UnassignedDevices_t * make_Topology_UnassignedDevices_t(Base * parent);

} // namespace knx
} // namespace v11
} // namespace Project
