/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v11/knx/LdCtrlProcType_t.h>
#include <Project/xs/HexBinary.h>
#include <Project/xs/UnsignedByte.h>
#include <Project/xs/UnsignedInt.h>

namespace Project {
namespace v11 {
namespace knx {

class LoadProcedure_LdCtrlCompareRelMem_t : public Base
{
    Q_OBJECT

public:
    explicit LoadProcedure_LdCtrlCompareRelMem_t(Base * parent = nullptr);
    virtual ~LoadProcedure_LdCtrlCompareRelMem_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::UnsignedByte ObjIdx{};
    xs::UnsignedInt Offset{};
    xs::UnsignedInt Size{};
    xs::HexBinary InlineData{};
    LdCtrlProcType_t AppliesTo{"auto"};
};

LoadProcedure_LdCtrlCompareRelMem_t * make_LoadProcedure_LdCtrlCompareRelMem_t(Base * parent);

} // namespace knx
} // namespace v11
} // namespace Project
