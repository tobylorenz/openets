/* This file is generated. */

#include <Project/v11/knx/ParameterBlockRename_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v11/knx/KNX_t.h>

namespace Project {
namespace v11 {
namespace knx {

ParameterBlockRename_t::ParameterBlockRename_t(Base * parent) :
    Base(parent)
{
}

ParameterBlockRename_t::~ParameterBlockRename_t()
{
}

void ParameterBlockRename_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == "Id") {
            Id = attribute.value().toString();
            KNX_t * knx = findParent<KNX_t *>();
            Q_ASSERT(knx);
            knx->ids[Id] = this;
            continue;
        }
        if (name == QString("RefId")) {
            RefId = attribute.value().toString();
            continue;
        }
        if (name == QString("Name")) {
            Name = attribute.value().toString();
            continue;
        }
        if (name == QString("Text")) {
            Text = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ParameterBlockRename_t::tableColumnCount() const
{
    return 5;
}

QVariant ParameterBlockRename_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ParameterBlockRename";
        }
        if (qualifiedName == QString("Id")) {
            return Id;
        }
        if (qualifiedName == QString("RefId")) {
            return RefId;
        }
        if (qualifiedName == QString("Name")) {
            return Name;
        }
        if (qualifiedName == QString("Text")) {
            return Text;
        }
        break;
    }
    return QVariant();
}

QVariant ParameterBlockRename_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Id";
            case 2:
                return "RefId";
            case 3:
                return "Name";
            case 4:
                return "Text";
            }
        }
    }
    return QVariant();
}

QVariant ParameterBlockRename_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return !Name.isEmpty() ? Name : "ParameterBlockRename";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ParameterBlockRename");
    }
    return QVariant();
}

ParameterBlockRename_t * make_ParameterBlockRename_t(Base * parent)
{
    return new ParameterBlockRename_t(parent);
}

} // namespace knx
} // namespace v11
} // namespace Project
