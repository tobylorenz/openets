/* This file is generated. */

#pragma once

#include <Project/v11/knx/Capability_t.h>
#include <Project/xs/List.h>

namespace Project {
namespace v11 {
namespace knx {

using Capabilities_t = xs::List<Capability_t>;

} // namespace knx
} // namespace v11
} // namespace Project
