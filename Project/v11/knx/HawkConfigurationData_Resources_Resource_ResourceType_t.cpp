/* This file is generated. */

#include <Project/v11/knx/HawkConfigurationData_Resources_Resource_ResourceType_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v11/knx/KNX_t.h>

namespace Project {
namespace v11 {
namespace knx {

HawkConfigurationData_Resources_Resource_ResourceType_t::HawkConfigurationData_Resources_Resource_ResourceType_t(Base * parent) :
    Base(parent)
{
}

HawkConfigurationData_Resources_Resource_ResourceType_t::~HawkConfigurationData_Resources_Resource_ResourceType_t()
{
}

void HawkConfigurationData_Resources_Resource_ResourceType_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("Length")) {
            Length = attribute.value().toString();
            continue;
        }
        if (name == QString("Flavour")) {
            Flavour = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int HawkConfigurationData_Resources_Resource_ResourceType_t::tableColumnCount() const
{
    return 3;
}

QVariant HawkConfigurationData_Resources_Resource_ResourceType_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "HawkConfigurationData_Resources_Resource_ResourceType";
        }
        if (qualifiedName == QString("Length")) {
            return Length;
        }
        if (qualifiedName == QString("Flavour")) {
            return Flavour;
        }
        break;
    }
    return QVariant();
}

QVariant HawkConfigurationData_Resources_Resource_ResourceType_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Length";
            case 2:
                return "Flavour";
            }
        }
    }
    return QVariant();
}

QVariant HawkConfigurationData_Resources_Resource_ResourceType_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "ResourceType";
    case Qt::DecorationRole:
        return QIcon::fromTheme("HawkConfigurationData_Resources_Resource_ResourceType");
    }
    return QVariant();
}

HawkConfigurationData_Resources_Resource_ResourceType_t * make_HawkConfigurationData_Resources_Resource_ResourceType_t(Base * parent)
{
    return new HawkConfigurationData_Resources_Resource_ResourceType_t(parent);
}

} // namespace knx
} // namespace v11
} // namespace Project
