/* This file is generated. */

#include <Project/v11/knx/ParameterCalculation_RParameters_ParameterRefRef_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v11/knx/KNX_t.h>
#include <Project/v11/knx/ParameterRef_t.h>

namespace Project {
namespace v11 {
namespace knx {

ParameterCalculation_RParameters_ParameterRefRef_t::ParameterCalculation_RParameters_ParameterRefRef_t(Base * parent) :
    Base(parent)
{
}

ParameterCalculation_RParameters_ParameterRefRef_t::~ParameterCalculation_RParameters_ParameterRefRef_t()
{
}

void ParameterCalculation_RParameters_ParameterRefRef_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("RefId")) {
            RefId = attribute.value().toString();
            continue;
        }
        if (name == QString("AliasName")) {
            AliasName = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ParameterCalculation_RParameters_ParameterRefRef_t::tableColumnCount() const
{
    return 3;
}

QVariant ParameterCalculation_RParameters_ParameterRefRef_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ParameterCalculation_RParameters_ParameterRefRef";
        }
        if (qualifiedName == QString("RefId")) {
            return RefId;
        }
        if (qualifiedName == QString("AliasName")) {
            return AliasName;
        }
        break;
    }
    return QVariant();
}

QVariant ParameterCalculation_RParameters_ParameterRefRef_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "RefId";
            case 2:
                return "AliasName";
            }
        }
    }
    return QVariant();
}

QVariant ParameterCalculation_RParameters_ParameterRefRef_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "ParameterRefRef";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ParameterCalculation_RParameters_ParameterRefRef");
    }
    return QVariant();
}

ParameterRef_t * ParameterCalculation_RParameters_ParameterRefRef_t::getParameterRef() const
{
    if (RefId.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<ParameterRef_t *>(knx->ids[RefId]);
}

ParameterCalculation_RParameters_ParameterRefRef_t * make_ParameterCalculation_RParameters_ParameterRefRef_t(Base * parent)
{
    return new ParameterCalculation_RParameters_ParameterRefRef_t(parent);
}

} // namespace knx
} // namespace v11
} // namespace Project
