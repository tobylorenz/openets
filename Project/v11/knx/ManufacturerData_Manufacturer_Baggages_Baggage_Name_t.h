/* This file is generated. */

#pragma once

#include <Project/v11/knx/String255_t.h>

namespace Project {
namespace v11 {
namespace knx {

using ManufacturerData_Manufacturer_Baggages_Baggage_Name_t = String255_t;

} // namespace knx
} // namespace v11
} // namespace Project
