/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v11/knx/ApplicationProgramStatic_Parameters_Union_SizeInBit_t.h>
#include <Project/v11/knx/IDREF.h>
#include <Project/v11/knx/UnionParameter_t.h>

namespace Project {
namespace v11 {
namespace knx {

/* forward declarations */
class ApplicationProgramStatic_Parameters_Union_Memory_t;
class ApplicationProgramStatic_Parameters_Union_Property_t;

class ApplicationProgramStatic_Parameters_Union_t : public Base
{
    Q_OBJECT

public:
    explicit ApplicationProgramStatic_Parameters_Union_t(Base * parent = nullptr);
    virtual ~ApplicationProgramStatic_Parameters_Union_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    ApplicationProgramStatic_Parameters_Union_SizeInBit_t SizeInBit{};

    /* elements */
    // xs:choice ApplicationProgramStatic_Parameters_Union_Memory_t * Memory{};
    // xs:choice ApplicationProgramStatic_Parameters_Union_Property_t * Property{};
    QMap<xs::ID, UnionParameter_t *> Parameter; // key: Id
};

ApplicationProgramStatic_Parameters_Union_t * make_ApplicationProgramStatic_Parameters_Union_t(Base * parent);

} // namespace knx
} // namespace v11
} // namespace Project
