/* This file is generated. */

#include <Project/v11/knx/Hardware_Products_Product_Baggages_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v11/knx/KNX_t.h>

namespace Project {
namespace v11 {
namespace knx {

Hardware_Products_Product_Baggages_t::Hardware_Products_Product_Baggages_t(Base * parent) :
    Base(parent)
{
}

Hardware_Products_Product_Baggages_t::~Hardware_Products_Product_Baggages_t()
{
}

void Hardware_Products_Product_Baggages_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("Baggage")) {
            QString newRefId = reader.attributes().value("RefId").toString();
            Q_ASSERT(!newRefId.isEmpty());
            Hardware_Products_Product_Baggages_Baggage_t * newBaggage;
            if (Baggage.contains(newRefId)) {
                newBaggage = Baggage[newRefId];
            } else {
                newBaggage = make_Hardware_Products_Product_Baggages_Baggage_t(this);
                Baggage[newRefId] = newBaggage;
            }
            newBaggage->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int Hardware_Products_Product_Baggages_t::tableColumnCount() const
{
    return 1;
}

QVariant Hardware_Products_Product_Baggages_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "Hardware_Products_Product_Baggages";
        }
        break;
    }
    return QVariant();
}

QVariant Hardware_Products_Product_Baggages_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant Hardware_Products_Product_Baggages_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "Baggages";
    case Qt::DecorationRole:
        return QIcon::fromTheme("Hardware_Products_Product_Baggages");
    }
    return QVariant();
}

Hardware_Products_Product_Baggages_t * make_Hardware_Products_Product_Baggages_t(Base * parent)
{
    return new Hardware_Products_Product_Baggages_t(parent);
}

} // namespace knx
} // namespace v11
} // namespace Project
