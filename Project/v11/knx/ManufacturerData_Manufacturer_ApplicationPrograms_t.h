/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v11/knx/ApplicationProgram_t.h>

namespace Project {
namespace v11 {
namespace knx {

class ManufacturerData_Manufacturer_ApplicationPrograms_t : public Base
{
    Q_OBJECT

public:
    explicit ManufacturerData_Manufacturer_ApplicationPrograms_t(Base * parent = nullptr);
    virtual ~ManufacturerData_Manufacturer_ApplicationPrograms_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* elements */
    QMap<xs::ID, ApplicationProgram_t *> ApplicationProgram; // key: Id
};

ManufacturerData_Manufacturer_ApplicationPrograms_t * make_ManufacturerData_Manufacturer_ApplicationPrograms_t(Base * parent);

} // namespace knx
} // namespace v11
} // namespace Project
