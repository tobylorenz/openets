/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v11/knx/HawkConfigurationData_Procedures_Procedure_t.h>

namespace Project {
namespace v11 {
namespace knx {

class HawkConfigurationData_Procedures_t : public Base
{
    Q_OBJECT

public:
    explicit HawkConfigurationData_Procedures_t(Base * parent = nullptr);
    virtual ~HawkConfigurationData_Procedures_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* elements */
    QVector<HawkConfigurationData_Procedures_Procedure_t *> Procedure;
};

HawkConfigurationData_Procedures_t * make_HawkConfigurationData_Procedures_t(Base * parent);

} // namespace knx
} // namespace v11
} // namespace Project
