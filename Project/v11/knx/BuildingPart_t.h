/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v11/knx/BuildingPartType_t.h>
#include <Project/v11/knx/BuildingPart_t.h>
#include <Project/v11/knx/CompletionStatus_t.h>
#include <Project/v11/knx/DeviceInstanceRef_t.h>
#include <Project/v11/knx/String255_t.h>
#include <Project/xs/ID.h>
#include <Project/xs/String.h>

namespace Project {
namespace v11 {
namespace knx {

class BuildingPart_t : public Base
{
    Q_OBJECT

public:
    explicit BuildingPart_t(Base * parent = nullptr);
    virtual ~BuildingPart_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::ID Id{};
    String255_t Name{};
    BuildingPartType_t Type{};
    String255_t Number{};
    xs::String Comment{};
    CompletionStatus_t CompletionStatus{"Undefined"};
    xs::String DefaultLine{};
    xs::String Description{};

    /* elements */
    QMap<xs::ID, BuildingPart_t *> BuildingPart; // key: Id
    QMap<IDREF, DeviceInstanceRef_t *> DeviceInstanceRef; // key: RefId
};

BuildingPart_t * make_BuildingPart_t(Base * parent);

} // namespace knx
} // namespace v11
} // namespace Project
