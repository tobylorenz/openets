/* This file is generated. */

#pragma once

#include <Project/v11/knx/LanguageDependentString_t.h>

namespace Project {
namespace v11 {
namespace knx {

using LanguageDependentString20_t = LanguageDependentString_t;

} // namespace knx
} // namespace v11
} // namespace Project
