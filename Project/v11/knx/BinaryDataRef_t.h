/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/SimpleElementTextType.h>
#include <Project/v11/knx/IDREF.h>
#include <Project/xs/Base64Binary.h>

namespace Project {
namespace v11 {
namespace knx {

/* forward declarations */
class BinaryData_t;

class BinaryDataRef_t : public Base
{
    Q_OBJECT

public:
    explicit BinaryDataRef_t(Base * parent = nullptr);
    virtual ~BinaryDataRef_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    IDREF RefId{};

    /* elements */
    SimpleElementTextType * Data{};

    /* getters */
    BinaryData_t * getBinaryData() const; // attribute: RefId
};

BinaryDataRef_t * make_BinaryDataRef_t(Base * parent);

} // namespace knx
} // namespace v11
} // namespace Project
