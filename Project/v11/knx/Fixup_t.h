/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/SimpleElementTextType.h>
#include <Project/v11/knx/Fixup_Offset_t.h>
#include <Project/v11/knx/IDREF.h>

namespace Project {
namespace v11 {
namespace knx {

/* forward declarations */
class ApplicationProgramStatic_Code_AbsoluteSegment_t;

class Fixup_t : public Base
{
    Q_OBJECT

public:
    explicit Fixup_t(Base * parent = nullptr);
    virtual ~Fixup_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    IDREF FunctionRef{};
    IDREF CodeSegment{};

    /* elements */
    QVector<SimpleElementTextType *> Offset;

    /* getters */
    ApplicationProgramStatic_Code_AbsoluteSegment_t * getCodeSegment() const; // attribute: CodeSegment
};

Fixup_t * make_Fixup_t(Base * parent);

} // namespace knx
} // namespace v11
} // namespace Project
