/* This file is generated. */

#include <Project/v11/knx/Trades_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v11/knx/KNX_t.h>

namespace Project {
namespace v11 {
namespace knx {

Trades_t::Trades_t(Base * parent) :
    Base(parent)
{
}

Trades_t::~Trades_t()
{
}

void Trades_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("Trade")) {
            auto * newTrade = make_Trade_t(this);
            newTrade->read(reader);
            Trade.append(newTrade);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int Trades_t::tableColumnCount() const
{
    return 1;
}

QVariant Trades_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "Trades";
        }
        break;
    }
    return QVariant();
}

QVariant Trades_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant Trades_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "Trades";
    case Qt::DecorationRole:
        return QIcon::fromTheme("Trades");
    }
    return QVariant();
}

Trades_t * make_Trades_t(Base * parent)
{
    return new Trades_t(parent);
}

} // namespace knx
} // namespace v11
} // namespace Project
