/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v11/knx/ComObjectPriority_t.h>
#include <Project/v11/knx/ComObjectSize_t.h>
#include <Project/v11/knx/Enable_t.h>
#include <Project/v11/knx/IDREFS.h>
#include <Project/v11/knx/LanguageDependentString255_t.h>
#include <Project/v11/knx/LanguageDependentString_t.h>
#include <Project/v11/knx/MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_t.h>
#include <Project/v11/knx/String50_t.h>
#include <Project/xs/ID.h>
#include <Project/xs/UnsignedInt.h>

namespace Project {
namespace v11 {
namespace knx {

class ComObject_t : public Base
{
    Q_OBJECT

public:
    explicit ComObject_t(Base * parent = nullptr);
    virtual ~ComObject_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::ID Id{};
    String50_t Name{};
    LanguageDependentString255_t Text{};
    xs::UnsignedInt Number{};
    LanguageDependentString255_t FunctionText{};
    LanguageDependentString_t VisibleDescription{};
    ComObjectPriority_t Priority{"Low"};
    ComObjectSize_t ObjectSize{};
    Enable_t ReadFlag{};
    Enable_t WriteFlag{};
    Enable_t CommunicationFlag{};
    Enable_t TransmitFlag{};
    Enable_t UpdateFlag{};
    Enable_t ReadOnInitFlag{};
    IDREFS DatapointType{};

    /* getters */
    QList<MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_t *> getDatapointType() const; // attribute: DatapointType
};

ComObject_t * make_ComObject_t(Base * parent);

} // namespace knx
} // namespace v11
} // namespace Project
