/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v11/knx/Condition_t.h>
#include <Project/xs/Boolean.h>

namespace Project {
namespace v11 {
namespace knx {

/* forward declarations */
class ApplicationProgramChannel_t;
class DependentChannelChoose_t;
class ParameterBlockRename_t;

class DependentChannelChoose_when_t : public Base
{
    Q_OBJECT

public:
    explicit DependentChannelChoose_when_t(Base * parent = nullptr);
    virtual ~DependentChannelChoose_when_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    Condition_t Test{};
    xs::Boolean Default{"false"};

    /* elements */
    // xs:choice ApplicationProgramChannel_t * Channel{};
    // xs:choice DependentChannelChoose_t * Choose{};
    // xs:choice ParameterBlockRename_t * ParameterBlockRename{};
};

DependentChannelChoose_when_t * make_DependentChannelChoose_when_t(Base * parent);

} // namespace knx
} // namespace v11
} // namespace Project
