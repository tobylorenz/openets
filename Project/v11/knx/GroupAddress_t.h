/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v11/knx/GroupAddress_Address_t.h>
#include <Project/v11/knx/IDREFS.h>
#include <Project/v11/knx/MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_t.h>
#include <Project/v11/knx/String255_t.h>
#include <Project/xs/ID.h>
#include <Project/xs/Boolean.h>
#include <Project/xs/String.h>

namespace Project {
namespace v11 {
namespace knx {

class GroupAddress_t : public Base
{
    Q_OBJECT

public:
    explicit GroupAddress_t(Base * parent = nullptr);
    virtual ~GroupAddress_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::ID Id{};
    GroupAddress_Address_t Address{};
    String255_t Name{};
    xs::Boolean Unfiltered{"false"};
    xs::Boolean Central{"false"};
    xs::Boolean Global{"false"};
    IDREFS DatapointType{};
    xs::String Description{};
    xs::String Comment{};

    /* getters */
    QList<MasterData_DatapointTypes_DatapointType_DatapointSubtypes_DatapointSubtype_t *> getDatapointType() const; // attribute: DatapointType

    QString getStyledAddress() const;
};

GroupAddress_t * make_GroupAddress_t(Base * parent);

} // namespace knx
} // namespace v11
} // namespace Project
