/* This file is generated. */

#pragma once

#include <Project/xs/UnsignedInt.h>

namespace Project {
namespace v11 {
namespace knx {

using ApplicationProgramStatic_Code_RelativeSegment_Size_t = xs::UnsignedInt;

} // namespace knx
} // namespace v11
} // namespace Project
