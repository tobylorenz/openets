/* This file is generated. */

#pragma once

#include <Project/xs/String.h>

namespace Project {
namespace v11 {
namespace knx {

using ApplicationProgramType_t = xs::String;

} // namespace knx
} // namespace v11
} // namespace Project
