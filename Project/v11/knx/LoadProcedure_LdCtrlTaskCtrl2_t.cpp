/* This file is generated. */

#include <Project/v11/knx/LoadProcedure_LdCtrlTaskCtrl2_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v11/knx/KNX_t.h>

namespace Project {
namespace v11 {
namespace knx {

LoadProcedure_LdCtrlTaskCtrl2_t::LoadProcedure_LdCtrlTaskCtrl2_t(Base * parent) :
    Base(parent)
{
}

LoadProcedure_LdCtrlTaskCtrl2_t::~LoadProcedure_LdCtrlTaskCtrl2_t()
{
}

void LoadProcedure_LdCtrlTaskCtrl2_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("LsmIdx")) {
            LsmIdx = attribute.value().toString();
            continue;
        }
        if (name == QString("ObjType")) {
            ObjType = attribute.value().toString();
            continue;
        }
        if (name == QString("Occurrence")) {
            Occurrence = attribute.value().toString();
            continue;
        }
        if (name == QString("Callback")) {
            Callback = attribute.value().toString();
            continue;
        }
        if (name == QString("Address")) {
            Address = attribute.value().toString();
            continue;
        }
        if (name == QString("Seg0")) {
            Seg0 = attribute.value().toString();
            continue;
        }
        if (name == QString("Seg1")) {
            Seg1 = attribute.value().toString();
            continue;
        }
        if (name == QString("AppliesTo")) {
            AppliesTo = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int LoadProcedure_LdCtrlTaskCtrl2_t::tableColumnCount() const
{
    return 9;
}

QVariant LoadProcedure_LdCtrlTaskCtrl2_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "LoadProcedure_LdCtrlTaskCtrl2";
        }
        if (qualifiedName == QString("LsmIdx")) {
            return LsmIdx;
        }
        if (qualifiedName == QString("ObjType")) {
            return ObjType;
        }
        if (qualifiedName == QString("Occurrence")) {
            return Occurrence;
        }
        if (qualifiedName == QString("Callback")) {
            return Callback;
        }
        if (qualifiedName == QString("Address")) {
            return Address;
        }
        if (qualifiedName == QString("Seg0")) {
            return Seg0;
        }
        if (qualifiedName == QString("Seg1")) {
            return Seg1;
        }
        if (qualifiedName == QString("AppliesTo")) {
            return AppliesTo;
        }
        break;
    }
    return QVariant();
}

QVariant LoadProcedure_LdCtrlTaskCtrl2_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "LsmIdx";
            case 2:
                return "ObjType";
            case 3:
                return "Occurrence";
            case 4:
                return "Callback";
            case 5:
                return "Address";
            case 6:
                return "Seg0";
            case 7:
                return "Seg1";
            case 8:
                return "AppliesTo";
            }
        }
    }
    return QVariant();
}

QVariant LoadProcedure_LdCtrlTaskCtrl2_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "LdCtrlTaskCtrl2";
    case Qt::DecorationRole:
        return QIcon::fromTheme("LoadProcedure_LdCtrlTaskCtrl2");
    }
    return QVariant();
}

LoadProcedure_LdCtrlTaskCtrl2_t * make_LoadProcedure_LdCtrlTaskCtrl2_t(Base * parent)
{
    return new LoadProcedure_LdCtrlTaskCtrl2_t(parent);
}

} // namespace knx
} // namespace v11
} // namespace Project
