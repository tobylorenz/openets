/* This file is generated. */

#include <Project/v11/knx/LoadProcedure_LdCtrlMapError_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v11/knx/KNX_t.h>

namespace Project {
namespace v11 {
namespace knx {

LoadProcedure_LdCtrlMapError_t::LoadProcedure_LdCtrlMapError_t(Base * parent) :
    Base(parent)
{
}

LoadProcedure_LdCtrlMapError_t::~LoadProcedure_LdCtrlMapError_t()
{
}

void LoadProcedure_LdCtrlMapError_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("LdCtrlFilter")) {
            LdCtrlFilter = attribute.value().toString();
            continue;
        }
        if (name == QString("OriginalError")) {
            OriginalError = attribute.value().toString();
            continue;
        }
        if (name == QString("MappedError")) {
            MappedError = attribute.value().toString();
            continue;
        }
        if (name == QString("AppliesTo")) {
            AppliesTo = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int LoadProcedure_LdCtrlMapError_t::tableColumnCount() const
{
    return 5;
}

QVariant LoadProcedure_LdCtrlMapError_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "LoadProcedure_LdCtrlMapError";
        }
        if (qualifiedName == QString("LdCtrlFilter")) {
            return LdCtrlFilter;
        }
        if (qualifiedName == QString("OriginalError")) {
            return OriginalError;
        }
        if (qualifiedName == QString("MappedError")) {
            return MappedError;
        }
        if (qualifiedName == QString("AppliesTo")) {
            return AppliesTo;
        }
        break;
    }
    return QVariant();
}

QVariant LoadProcedure_LdCtrlMapError_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "LdCtrlFilter";
            case 2:
                return "OriginalError";
            case 3:
                return "MappedError";
            case 4:
                return "AppliesTo";
            }
        }
    }
    return QVariant();
}

QVariant LoadProcedure_LdCtrlMapError_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "LdCtrlMapError";
    case Qt::DecorationRole:
        return QIcon::fromTheme("LoadProcedure_LdCtrlMapError");
    }
    return QVariant();
}

LoadProcedure_LdCtrlMapError_t * make_LoadProcedure_LdCtrlMapError_t(Base * parent)
{
    return new LoadProcedure_LdCtrlMapError_t(parent);
}

} // namespace knx
} // namespace v11
} // namespace Project
