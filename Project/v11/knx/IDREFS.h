/* This file is generated. */

#pragma once

#include <Project/v11/knx/IDREF.h>
#include <Project/xs/List.h>

namespace Project {
namespace v11 {
namespace knx {

using IDREFS = xs::List<IDREF>;

} // namespace knx
} // namespace v11
} // namespace Project
