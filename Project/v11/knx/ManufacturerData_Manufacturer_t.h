/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v11/knx/IDREF.h>

namespace Project {
namespace v11 {
namespace knx {

/* forward declarations */
class ManufacturerData_Manufacturer_ApplicationPrograms_t;
class ManufacturerData_Manufacturer_Baggages_t;
class ManufacturerData_Manufacturer_Catalog_t;
class ManufacturerData_Manufacturer_Hardware_t;
class ManufacturerData_Manufacturer_Languages_t;
class MasterData_Manufacturers_Manufacturer_t;

class ManufacturerData_Manufacturer_t : public Base
{
    Q_OBJECT

public:
    explicit ManufacturerData_Manufacturer_t(Base * parent = nullptr);
    virtual ~ManufacturerData_Manufacturer_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    IDREF RefId{};

    /* elements */
    ManufacturerData_Manufacturer_Catalog_t * Catalog{};
    ManufacturerData_Manufacturer_ApplicationPrograms_t * ApplicationPrograms{};
    ManufacturerData_Manufacturer_Baggages_t * Baggages{};
    ManufacturerData_Manufacturer_Hardware_t * Hardware{};
    ManufacturerData_Manufacturer_Languages_t * Languages{};

    /* getters */
    MasterData_Manufacturers_Manufacturer_t * getManufacturer() const; // attribute: RefId
};

ManufacturerData_Manufacturer_t * make_ManufacturerData_Manufacturer_t(Base * parent);

} // namespace knx
} // namespace v11
} // namespace Project
