/* This file is generated. */

#include <Project/v11/knx/LoadProcedure_LdCtrlTaskPtr_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v11/knx/KNX_t.h>

namespace Project {
namespace v11 {
namespace knx {

LoadProcedure_LdCtrlTaskPtr_t::LoadProcedure_LdCtrlTaskPtr_t(Base * parent) :
    Base(parent)
{
}

LoadProcedure_LdCtrlTaskPtr_t::~LoadProcedure_LdCtrlTaskPtr_t()
{
}

void LoadProcedure_LdCtrlTaskPtr_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("LsmIdx")) {
            LsmIdx = attribute.value().toString();
            continue;
        }
        if (name == QString("ObjType")) {
            ObjType = attribute.value().toString();
            continue;
        }
        if (name == QString("Occurrence")) {
            Occurrence = attribute.value().toString();
            continue;
        }
        if (name == QString("InitPtr")) {
            InitPtr = attribute.value().toString();
            continue;
        }
        if (name == QString("SavePtr")) {
            SavePtr = attribute.value().toString();
            continue;
        }
        if (name == QString("SerialPtr")) {
            SerialPtr = attribute.value().toString();
            continue;
        }
        if (name == QString("AppliesTo")) {
            AppliesTo = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int LoadProcedure_LdCtrlTaskPtr_t::tableColumnCount() const
{
    return 8;
}

QVariant LoadProcedure_LdCtrlTaskPtr_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "LoadProcedure_LdCtrlTaskPtr";
        }
        if (qualifiedName == QString("LsmIdx")) {
            return LsmIdx;
        }
        if (qualifiedName == QString("ObjType")) {
            return ObjType;
        }
        if (qualifiedName == QString("Occurrence")) {
            return Occurrence;
        }
        if (qualifiedName == QString("InitPtr")) {
            return InitPtr;
        }
        if (qualifiedName == QString("SavePtr")) {
            return SavePtr;
        }
        if (qualifiedName == QString("SerialPtr")) {
            return SerialPtr;
        }
        if (qualifiedName == QString("AppliesTo")) {
            return AppliesTo;
        }
        break;
    }
    return QVariant();
}

QVariant LoadProcedure_LdCtrlTaskPtr_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "LsmIdx";
            case 2:
                return "ObjType";
            case 3:
                return "Occurrence";
            case 4:
                return "InitPtr";
            case 5:
                return "SavePtr";
            case 6:
                return "SerialPtr";
            case 7:
                return "AppliesTo";
            }
        }
    }
    return QVariant();
}

QVariant LoadProcedure_LdCtrlTaskPtr_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "LdCtrlTaskPtr";
    case Qt::DecorationRole:
        return QIcon::fromTheme("LoadProcedure_LdCtrlTaskPtr");
    }
    return QVariant();
}

LoadProcedure_LdCtrlTaskPtr_t * make_LoadProcedure_LdCtrlTaskPtr_t(Base * parent)
{
    return new LoadProcedure_LdCtrlTaskPtr_t(parent);
}

} // namespace knx
} // namespace v11
} // namespace Project
