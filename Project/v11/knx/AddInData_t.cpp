/* This file is generated. */

#include <Project/v11/knx/AddInData_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v11/knx/KNX_t.h>

namespace Project {
namespace v11 {
namespace knx {

AddInData_t::AddInData_t(Base * parent) :
    Base(parent)
{
}

AddInData_t::~AddInData_t()
{
}

void AddInData_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("AddInId")) {
            AddInId = attribute.value().toString();
            continue;
        }
        if (name == QString("Name")) {
            Name = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int AddInData_t::tableColumnCount() const
{
    return 3;
}

QVariant AddInData_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "AddInData";
        }
        if (qualifiedName == QString("AddInId")) {
            return AddInId;
        }
        if (qualifiedName == QString("Name")) {
            return Name;
        }
        break;
    }
    return QVariant();
}

QVariant AddInData_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "AddInId";
            case 2:
                return "Name";
            }
        }
    }
    return QVariant();
}

QVariant AddInData_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return !Name.isEmpty() ? Name : "AddInData";
    case Qt::DecorationRole:
        return QIcon::fromTheme("AddInData");
    }
    return QVariant();
}

AddInData_t * make_AddInData_t(Base * parent)
{
    return new AddInData_t(parent);
}

} // namespace knx
} // namespace v11
} // namespace Project
