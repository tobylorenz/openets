/* This file is generated. */

#include <Project/v11/knx/Parameter_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v11/knx/KNX_t.h>
#include <Project/v11/knx/ParameterType_t.h>
#include <Project/v11/knx/Parameter_Memory_t.h>
#include <Project/v11/knx/Parameter_Property_t.h>

namespace Project {
namespace v11 {
namespace knx {

Parameter_t::Parameter_t(Base * parent) :
    Base(parent)
{
}

Parameter_t::~Parameter_t()
{
}

void Parameter_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("LegacyPatchAlways")) {
            LegacyPatchAlways = attribute.value().toString();
            continue;
        }
        if (name == "Id") {
            Id = attribute.value().toString();
            KNX_t * knx = findParent<KNX_t *>();
            Q_ASSERT(knx);
            knx->ids[Id] = this;
            continue;
        }
        if (name == QString("Name")) {
            Name = attribute.value().toString();
            continue;
        }
        if (name == QString("ParameterType")) {
            ParameterType = attribute.value().toString();
            continue;
        }
        if (name == QString("Text")) {
            Text = attribute.value().toString();
            continue;
        }
        if (name == QString("SuffixText")) {
            SuffixText = attribute.value().toString();
            continue;
        }
        if (name == QString("Access")) {
            Access = attribute.value().toString();
            continue;
        }
        if (name == QString("Value")) {
            Value = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("Memory")) {
            auto * Memory = make_Parameter_Memory_t(this);
            Memory->read(reader);
            continue;
        }
        if (reader.name() == QString("Property")) {
            auto * Property = make_Parameter_Property_t(this);
            Property->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int Parameter_t::tableColumnCount() const
{
    return 9;
}

QVariant Parameter_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "Parameter";
        }
        if (qualifiedName == QString("LegacyPatchAlways")) {
            return LegacyPatchAlways;
        }
        if (qualifiedName == QString("Id")) {
            return Id;
        }
        if (qualifiedName == QString("Name")) {
            return Name;
        }
        if (qualifiedName == QString("ParameterType")) {
            return ParameterType;
        }
        if (qualifiedName == QString("Text")) {
            return Text;
        }
        if (qualifiedName == QString("SuffixText")) {
            return SuffixText;
        }
        if (qualifiedName == QString("Access")) {
            return Access;
        }
        if (qualifiedName == QString("Value")) {
            return Value;
        }
        break;
    }
    return QVariant();
}

QVariant Parameter_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "LegacyPatchAlways";
            case 2:
                return "Id";
            case 3:
                return "Name";
            case 4:
                return "ParameterType";
            case 5:
                return "Text";
            case 6:
                return "SuffixText";
            case 7:
                return "Access";
            case 8:
                return "Value";
            }
        }
    }
    return QVariant();
}

QVariant Parameter_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
    {
        QString uniqueNumber = Id.mid(Id.lastIndexOf("_P-") + 3);
        return QString("[%1] %2").arg(uniqueNumber).arg(Name);
    }
    case Qt::DecorationRole:
        return QIcon::fromTheme("Parameter");
    }
    return QVariant();
}

ParameterType_t * Parameter_t::getParameterType() const
{
    if (ParameterType.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<ParameterType_t *>(knx->ids[ParameterType]);
}

Parameter_t * make_Parameter_t(Base * parent)
{
    return new Parameter_t(parent);
}

} // namespace knx
} // namespace v11
} // namespace Project
