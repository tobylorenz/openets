/* This file is generated. */

#include <Project/v11/knx/MaskVersion_MaskEntries_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v11/knx/KNX_t.h>

namespace Project {
namespace v11 {
namespace knx {

MaskVersion_MaskEntries_t::MaskVersion_MaskEntries_t(Base * parent) :
    Base(parent)
{
}

MaskVersion_MaskEntries_t::~MaskVersion_MaskEntries_t()
{
}

void MaskVersion_MaskEntries_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("MaskEntry")) {
            QString newId = reader.attributes().value("Id").toString();
            Q_ASSERT(!newId.isEmpty());
            MaskVersion_MaskEntries_MaskEntry_t * newMaskEntry;
            if (MaskEntry.contains(newId)) {
                newMaskEntry = MaskEntry[newId];
            } else {
                newMaskEntry = make_MaskVersion_MaskEntries_MaskEntry_t(this);
                MaskEntry[newId] = newMaskEntry;
            }
            newMaskEntry->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int MaskVersion_MaskEntries_t::tableColumnCount() const
{
    return 1;
}

QVariant MaskVersion_MaskEntries_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "MaskVersion_MaskEntries";
        }
        break;
    }
    return QVariant();
}

QVariant MaskVersion_MaskEntries_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant MaskVersion_MaskEntries_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "MaskEntries";
    case Qt::DecorationRole:
        return QIcon::fromTheme("MaskVersion_MaskEntries");
    }
    return QVariant();
}

MaskVersion_MaskEntries_t * make_MaskVersion_MaskEntries_t(Base * parent)
{
    return new MaskVersion_MaskEntries_t(parent);
}

} // namespace knx
} // namespace v11
} // namespace Project
