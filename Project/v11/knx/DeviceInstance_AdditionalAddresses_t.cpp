/* This file is generated. */

#include <Project/v11/knx/DeviceInstance_AdditionalAddresses_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v11/knx/KNX_t.h>

namespace Project {
namespace v11 {
namespace knx {

DeviceInstance_AdditionalAddresses_t::DeviceInstance_AdditionalAddresses_t(Base * parent) :
    Base(parent)
{
}

DeviceInstance_AdditionalAddresses_t::~DeviceInstance_AdditionalAddresses_t()
{
}

void DeviceInstance_AdditionalAddresses_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("Address")) {
            auto * newAddress = make_DeviceInstance_AdditionalAddresses_Address_t(this);
            newAddress->read(reader);
            Address.append(newAddress);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int DeviceInstance_AdditionalAddresses_t::tableColumnCount() const
{
    return 1;
}

QVariant DeviceInstance_AdditionalAddresses_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "DeviceInstance_AdditionalAddresses";
        }
        break;
    }
    return QVariant();
}

QVariant DeviceInstance_AdditionalAddresses_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant DeviceInstance_AdditionalAddresses_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "AdditionalAddresses";
    case Qt::DecorationRole:
        return QIcon::fromTheme("DeviceInstance_AdditionalAddresses");
    }
    return QVariant();
}

DeviceInstance_AdditionalAddresses_t * make_DeviceInstance_AdditionalAddresses_t(Base * parent)
{
    return new DeviceInstance_AdditionalAddresses_t(parent);
}

} // namespace knx
} // namespace v11
} // namespace Project
