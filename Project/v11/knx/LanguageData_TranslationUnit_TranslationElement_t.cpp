/* This file is generated. */

#include <Project/v11/knx/LanguageData_TranslationUnit_TranslationElement_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v11/knx/KNX_t.h>

namespace Project {
namespace v11 {
namespace knx {

LanguageData_TranslationUnit_TranslationElement_t::LanguageData_TranslationUnit_TranslationElement_t(Base * parent) :
    Base(parent)
{
}

LanguageData_TranslationUnit_TranslationElement_t::~LanguageData_TranslationUnit_TranslationElement_t()
{
}

void LanguageData_TranslationUnit_TranslationElement_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("RefId")) {
            RefId = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("Translation")) {
            auto * newTranslation = make_LanguageData_TranslationUnit_TranslationElement_Translation_t(this);
            newTranslation->read(reader);
            Translation.append(newTranslation);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int LanguageData_TranslationUnit_TranslationElement_t::tableColumnCount() const
{
    return 2;
}

QVariant LanguageData_TranslationUnit_TranslationElement_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "LanguageData_TranslationUnit_TranslationElement";
        }
        if (qualifiedName == QString("RefId")) {
            return RefId;
        }
        break;
    }
    return QVariant();
}

QVariant LanguageData_TranslationUnit_TranslationElement_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "RefId";
            }
        }
    }
    return QVariant();
}

QVariant LanguageData_TranslationUnit_TranslationElement_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "TranslationElement";
    case Qt::DecorationRole:
        return QIcon::fromTheme("LanguageData_TranslationUnit_TranslationElement");
    }
    return QVariant();
}

Base * LanguageData_TranslationUnit_TranslationElement_t::getTranslationElement() const
{
    if (RefId.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<Base *>(knx->ids[RefId]);
}

LanguageData_TranslationUnit_TranslationElement_t * make_LanguageData_TranslationUnit_TranslationElement_t(Base * parent)
{
    return new LanguageData_TranslationUnit_TranslationElement_t(parent);
}

} // namespace knx
} // namespace v11
} // namespace Project
