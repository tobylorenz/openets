/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v11/knx/Project_Installations_Installation_t.h>

namespace Project {
namespace v11 {
namespace knx {

class Project_Installations_t : public Base
{
    Q_OBJECT

public:
    explicit Project_Installations_t(Base * parent = nullptr);
    virtual ~Project_Installations_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* elements */
    QVector<Project_Installations_Installation_t *> Installation;
};

Project_Installations_t * make_Project_Installations_t(Base * parent);

} // namespace knx
} // namespace v11
} // namespace Project
