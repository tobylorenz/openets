/* This file is generated. */

#include <Project/v11/knx/IPConfig_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v11/knx/KNX_t.h>

namespace Project {
namespace v11 {
namespace knx {

IPConfig_t::IPConfig_t(Base * parent) :
    Base(parent)
{
}

IPConfig_t::~IPConfig_t()
{
}

void IPConfig_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("Assign")) {
            Assign = attribute.value().toString();
            continue;
        }
        if (name == QString("IPAddress")) {
            IPAddress = attribute.value().toString();
            continue;
        }
        if (name == QString("SubnetMask")) {
            SubnetMask = attribute.value().toString();
            continue;
        }
        if (name == QString("DefaultGateway")) {
            DefaultGateway = attribute.value().toString();
            continue;
        }
        if (name == QString("MACAddress")) {
            MACAddress = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int IPConfig_t::tableColumnCount() const
{
    return 6;
}

QVariant IPConfig_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "IPConfig";
        }
        if (qualifiedName == QString("Assign")) {
            return Assign;
        }
        if (qualifiedName == QString("IPAddress")) {
            return IPAddress;
        }
        if (qualifiedName == QString("SubnetMask")) {
            return SubnetMask;
        }
        if (qualifiedName == QString("DefaultGateway")) {
            return DefaultGateway;
        }
        if (qualifiedName == QString("MACAddress")) {
            return MACAddress;
        }
        break;
    }
    return QVariant();
}

QVariant IPConfig_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Assign";
            case 2:
                return "IPAddress";
            case 3:
                return "SubnetMask";
            case 4:
                return "DefaultGateway";
            case 5:
                return "MACAddress";
            }
        }
    }
    return QVariant();
}

QVariant IPConfig_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "IPConfig";
    case Qt::DecorationRole:
        return QIcon::fromTheme("IPConfig");
    }
    return QVariant();
}

IPConfig_t * make_IPConfig_t(Base * parent)
{
    return new IPConfig_t(parent);
}

} // namespace knx
} // namespace v11
} // namespace Project
