/* This file is generated. */

#include <Project/v11/knx/DeviceInstance_BinaryData_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v11/knx/KNX_t.h>

namespace Project {
namespace v11 {
namespace knx {

DeviceInstance_BinaryData_t::DeviceInstance_BinaryData_t(Base * parent) :
    Base(parent)
{
}

DeviceInstance_BinaryData_t::~DeviceInstance_BinaryData_t()
{
}

void DeviceInstance_BinaryData_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("BinaryData")) {
            auto * newBinaryData = make_DeviceInstance_BinaryData_BinaryData_t(this);
            newBinaryData->read(reader);
            BinaryData.append(newBinaryData);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int DeviceInstance_BinaryData_t::tableColumnCount() const
{
    return 1;
}

QVariant DeviceInstance_BinaryData_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "DeviceInstance_BinaryData";
        }
        break;
    }
    return QVariant();
}

QVariant DeviceInstance_BinaryData_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            }
        }
    }
    return QVariant();
}

QVariant DeviceInstance_BinaryData_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "BinaryData";
    case Qt::DecorationRole:
        return QIcon::fromTheme("DeviceInstance_BinaryData");
    }
    return QVariant();
}

DeviceInstance_BinaryData_t * make_DeviceInstance_BinaryData_t(Base * parent)
{
    return new DeviceInstance_BinaryData_t(parent);
}

} // namespace knx
} // namespace v11
} // namespace Project
