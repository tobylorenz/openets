/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/v11/knx/IDREF.h>
#include <Project/v11/knx/Value_t.h>

namespace Project {
namespace v11 {
namespace knx {

/* forward declarations */
class ParameterRefRef_t;

class Assign_t : public Base
{
    Q_OBJECT

public:
    explicit Assign_t(Base * parent = nullptr);
    virtual ~Assign_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    IDREF TargetParamRefRef{};
    IDREF SourceParamRefRef{};
    Value_t Value{};

    /* getters */
    ParameterRefRef_t * getTargetParameterRefRef() const; // attribute: TargetParamRefRef
    ParameterRefRef_t * getSourceParameterRefRef() const; // attribute: SourceParamRefRef
};

Assign_t * make_Assign_t(Base * parent);

} // namespace knx
} // namespace v11
} // namespace Project
