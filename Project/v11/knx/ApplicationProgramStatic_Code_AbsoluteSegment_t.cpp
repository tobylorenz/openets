/* This file is generated. */

#include <Project/v11/knx/ApplicationProgramStatic_Code_AbsoluteSegment_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v11/knx/KNX_t.h>

namespace Project {
namespace v11 {
namespace knx {

ApplicationProgramStatic_Code_AbsoluteSegment_t::ApplicationProgramStatic_Code_AbsoluteSegment_t(Base * parent) :
    Base(parent)
{
}

ApplicationProgramStatic_Code_AbsoluteSegment_t::~ApplicationProgramStatic_Code_AbsoluteSegment_t()
{
}

void ApplicationProgramStatic_Code_AbsoluteSegment_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == "Id") {
            Id = attribute.value().toString();
            KNX_t * knx = findParent<KNX_t *>();
            Q_ASSERT(knx);
            knx->ids[Id] = this;
            continue;
        }
        if (name == QString("Name")) {
            Name = attribute.value().toString();
            continue;
        }
        if (name == QString("MemoryType")) {
            MemoryType = attribute.value().toString();
            continue;
        }
        if (name == QString("Address")) {
            Address = attribute.value().toString();
            continue;
        }
        if (name == QString("Size")) {
            Size = attribute.value().toString();
            continue;
        }
        if (name == QString("UserMemory")) {
            UserMemory = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        if (reader.name() == QString("Data")) {
            if (!Data) {
                Data = make_SimpleElementTextType("Data", this);
            }
            Data->read(reader);
            continue;
        }
        if (reader.name() == QString("Mask")) {
            if (!Mask) {
                Mask = make_SimpleElementTextType("Mask", this);
            }
            Mask->read(reader);
            continue;
        }
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ApplicationProgramStatic_Code_AbsoluteSegment_t::tableColumnCount() const
{
    return 7;
}

QVariant ApplicationProgramStatic_Code_AbsoluteSegment_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ApplicationProgramStatic_Code_AbsoluteSegment";
        }
        if (qualifiedName == QString("Id")) {
            return Id;
        }
        if (qualifiedName == QString("Name")) {
            return Name;
        }
        if (qualifiedName == QString("MemoryType")) {
            return MemoryType;
        }
        if (qualifiedName == QString("Address")) {
            return Address;
        }
        if (qualifiedName == QString("Size")) {
            return Size;
        }
        if (qualifiedName == QString("UserMemory")) {
            return UserMemory;
        }
        break;
    }
    return QVariant();
}

QVariant ApplicationProgramStatic_Code_AbsoluteSegment_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "Id";
            case 2:
                return "Name";
            case 3:
                return "MemoryType";
            case 4:
                return "Address";
            case 5:
                return "Size";
            case 6:
                return "UserMemory";
            }
        }
    }
    return QVariant();
}

QVariant ApplicationProgramStatic_Code_AbsoluteSegment_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return QString(UserMemory == "false" ? "[%1] %2" : "[%1] %2 (User memory)").arg(Address).arg(Name);
    case Qt::DecorationRole:
        return QIcon::fromTheme("ApplicationProgramStatic_Code_AbsoluteSegment");
    }
    return QVariant();
}

ApplicationProgramStatic_Code_AbsoluteSegment_t * make_ApplicationProgramStatic_Code_AbsoluteSegment_t(Base * parent)
{
    return new ApplicationProgramStatic_Code_AbsoluteSegment_t(parent);
}

} // namespace knx
} // namespace v11
} // namespace Project
