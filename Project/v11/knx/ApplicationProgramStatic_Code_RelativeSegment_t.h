/* This file is generated. */

#pragma once

#include <Project/Base.h>
#include <Project/SimpleElementTextType.h>
#include <Project/v11/knx/ApplicationProgramStatic_Code_RelativeSegment_Offset_t.h>
#include <Project/v11/knx/ApplicationProgramStatic_Code_RelativeSegment_Size_t.h>
#include <Project/v11/knx/String50_t.h>
#include <Project/xs/ID.h>
#include <Project/xs/Base64Binary.h>
#include <Project/xs/UnsignedByte.h>

namespace Project {
namespace v11 {
namespace knx {

class ApplicationProgramStatic_Code_RelativeSegment_t : public Base
{
    Q_OBJECT

public:
    explicit ApplicationProgramStatic_Code_RelativeSegment_t(Base * parent = nullptr);
    virtual ~ApplicationProgramStatic_Code_RelativeSegment_t();

    void read(QXmlStreamReader & reader) override;
    int tableColumnCount() const override;
    QVariant tableData(const QString qualifiedName, int role = Qt::DisplayRole) const override;
    QVariant tableHeaderData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant treeData(int role = Qt::DisplayRole) const override;

    /* attributes */
    xs::ID Id{};
    String50_t Name{};
    ApplicationProgramStatic_Code_RelativeSegment_Offset_t Offset{};
    ApplicationProgramStatic_Code_RelativeSegment_Size_t Size{};
    xs::UnsignedByte LoadStateMachine{};

    /* elements */
    SimpleElementTextType * Data{};
    SimpleElementTextType * Mask{};
};

ApplicationProgramStatic_Code_RelativeSegment_t * make_ApplicationProgramStatic_Code_RelativeSegment_t(Base * parent);

} // namespace knx
} // namespace v11
} // namespace Project
