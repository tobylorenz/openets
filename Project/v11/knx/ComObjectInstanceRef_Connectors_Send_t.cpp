/* This file is generated. */

#include <Project/v11/knx/ComObjectInstanceRef_Connectors_Send_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v11/knx/GroupAddress_t.h>
#include <Project/v11/knx/KNX_t.h>

namespace Project {
namespace v11 {
namespace knx {

ComObjectInstanceRef_Connectors_Send_t::ComObjectInstanceRef_Connectors_Send_t(Base * parent) :
    Base(parent)
{
}

ComObjectInstanceRef_Connectors_Send_t::~ComObjectInstanceRef_Connectors_Send_t()
{
}

void ComObjectInstanceRef_Connectors_Send_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("GroupAddressRefId")) {
            GroupAddressRefId = attribute.value().toString();
            continue;
        }
        if (name == QString("Acknowledge")) {
            Acknowledge = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ComObjectInstanceRef_Connectors_Send_t::tableColumnCount() const
{
    return 3;
}

QVariant ComObjectInstanceRef_Connectors_Send_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ComObjectInstanceRef_Connectors_Send";
        }
        if (qualifiedName == QString("GroupAddressRefId")) {
            return GroupAddressRefId;
        }
        if (qualifiedName == QString("Acknowledge")) {
            return Acknowledge;
        }
        break;
    }
    return QVariant();
}

QVariant ComObjectInstanceRef_Connectors_Send_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "GroupAddressRefId";
            case 2:
                return "Acknowledge";
            }
        }
    }
    return QVariant();
}

QVariant ComObjectInstanceRef_Connectors_Send_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "Send";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ComObjectInstanceRef_Connectors_Send");
    }
    return QVariant();
}

GroupAddress_t * ComObjectInstanceRef_Connectors_Send_t::getGroupAddress() const
{
    if (GroupAddressRefId.isEmpty()) {
        return nullptr;
    }
    KNX_t * knx = findParent<KNX_t *>();
    Q_ASSERT(knx);
    return qobject_cast<GroupAddress_t *>(knx->ids[GroupAddressRefId]);
}

ComObjectInstanceRef_Connectors_Send_t * make_ComObjectInstanceRef_Connectors_Send_t(Base * parent)
{
    return new ComObjectInstanceRef_Connectors_Send_t(parent);
}

} // namespace knx
} // namespace v11
} // namespace Project
