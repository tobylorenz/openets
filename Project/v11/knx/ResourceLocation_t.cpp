/* This file is generated. */

#include <Project/v11/knx/ResourceLocation_t.h>

#include <QDebug>
#include <QIcon>

#include <Project/v11/knx/KNX_t.h>

namespace Project {
namespace v11 {
namespace knx {

ResourceLocation_t::ResourceLocation_t(Base * parent) :
    Base(parent)
{
}

ResourceLocation_t::~ResourceLocation_t()
{
}

void ResourceLocation_t::read(QXmlStreamReader & reader)
{
    Q_ASSERT(reader.isStartElement());

    Base::read(reader);

    /* parse attributes */
    for (const QXmlStreamAttribute & attribute : reader.attributes()) {
        QString name = attribute.name().toString();
        if (name == QString("AddressSpace")) {
            AddressSpace = attribute.value().toString();
            continue;
        }
        if (name == QString("InterfaceObjectRef")) {
            InterfaceObjectRef = attribute.value().toString();
            continue;
        }
        if (name == QString("PropertyID")) {
            PropertyID = attribute.value().toString();
            continue;
        }
        if (name == QString("StartAddress")) {
            StartAddress = attribute.value().toString();
            continue;
        }
        if (name == QString("Occurrence")) {
            Occurrence = attribute.value().toString();
            continue;
        }
        if (name == QString("PtrResource")) {
            PtrResource = attribute.value().toString();
            continue;
        }
        qWarning() << metaObject()->className() << "Unexpected XML attribute" << name;
    }

    /* parse elements */
    while (reader.readNextStartElement()) {
        qCritical() << metaObject()->className() << "Unexpected XML element" << reader.name();
    }
}

int ResourceLocation_t::tableColumnCount() const
{
    return 7;
}

QVariant ResourceLocation_t::tableData(const QString qualifiedName, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (qualifiedName == "ElementType") {
            return "ResourceLocation";
        }
        if (qualifiedName == QString("AddressSpace")) {
            return AddressSpace;
        }
        if (qualifiedName == QString("InterfaceObjectRef")) {
            return InterfaceObjectRef;
        }
        if (qualifiedName == QString("PropertyID")) {
            return PropertyID;
        }
        if (qualifiedName == QString("StartAddress")) {
            return StartAddress;
        }
        if (qualifiedName == QString("Occurrence")) {
            return Occurrence;
        }
        if (qualifiedName == QString("PtrResource")) {
            return PtrResource;
        }
        break;
    }
    return QVariant();
}

QVariant ResourceLocation_t::tableHeaderData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            switch (section) {
            case 0:
                return "ElementType";
            case 1:
                return "AddressSpace";
            case 2:
                return "InterfaceObjectRef";
            case 3:
                return "PropertyID";
            case 4:
                return "StartAddress";
            case 5:
                return "Occurrence";
            case 6:
                return "PtrResource";
            }
        }
    }
    return QVariant();
}

QVariant ResourceLocation_t::treeData(int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return "ResourceLocation";
    case Qt::DecorationRole:
        return QIcon::fromTheme("ResourceLocation");
    }
    return QVariant();
}

ResourceLocation_t * make_ResourceLocation_t(Base * parent)
{
    return new ResourceLocation_t(parent);
}

} // namespace knx
} // namespace v11
} // namespace Project
