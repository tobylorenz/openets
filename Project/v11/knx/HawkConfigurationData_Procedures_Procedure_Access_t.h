/* This file is generated. */

#pragma once

#include <Project/v11/knx/ResourceAccess_t.h>
#include <Project/xs/List.h>

namespace Project {
namespace v11 {
namespace knx {

using HawkConfigurationData_Procedures_Procedure_Access_t = xs::List<ResourceAccess_t>;

} // namespace knx
} // namespace v11
} // namespace Project
