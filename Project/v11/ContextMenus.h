#pragma once

#include <QMdiArea>
#include <QMenu>

#include <Project/v11/knx/DeviceInstance_t.h>

/* context menus */
QMenu * getCustomContextMenu(QMdiArea * mdiArea, Project::v11::knx::DeviceInstance_t * deviceInstance);
