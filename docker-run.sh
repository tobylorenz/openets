#!/bin/sh

# SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
#
# SPDX-License-Identifier: GPL-3.0-or-later

docker build -t knx .

docker run \
    -e DISPLAY=$DISPLAY \
    -e HOME=$HOME \
    -i \
    -t \
    -v /home:/home \
    -v /tmp/.X11-unix:/tmp/.X11-unix \
    knx \
    /bin/bash
