#pragma once

#include <QAction>
#include <QList>
#include <QMainWindow>
#include <QMdiArea>
#include <QMenu>

#include <Connection/Connection.h>
#include <KNX/KNXnet.h>

#include "config.h"

/** @todo Language Strings */
/** @todo Right Click to open tree in new window */
/** @todo Graphical View (graphviz) to show relationships */
/** @todo Implement change history */

/**
 * @brief Main Window
 */
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow();
    virtual ~MainWindow();

    QMdiArea * mdiArea{nullptr};

private Q_SLOTS:
    void applicationExit();
    void documentNew();
    void documentOpenFile();
    void documentOpenDir();
    void documentOpenRecent();
    void documentSave();
    void documentSaveAs();
    void documentClose();
    void documentImportKnxProd();
#ifdef OPTION_EXPORT_OpenHAB
    void documentExportAsOpenHAB();
#endif
#ifdef OPTION_EXPORT_SmartHomeNG
    void documentExportAsSmartHomeNG();
#endif
    //    void editCopy();
    //    void editCut();
    //    void editDelete();
    //    void editPaste();
    void helpDebug();
    void helpAbout();
    void helpAboutQt();
    void knxBusInterfaces();
    void knxADCView();
    void knxAuthorizeView();
    void knxDeviceDescriptorView();
    void knxDomainAddressView();
    void knxFileStreamView();
    void knxGroupValueView();
    void knxKeyView();
    void knxLinkView();
    void knxMemoryView();
    void knxNetworkParameterView();
    void knxPropertyView();
    void knxRestartView();
    void knxSystemNetworkParameterView();
    void knxUserManufacturerInfoView();
    void knxNetworkManagementIndividualAddressView();
    void projectKNX();
    void projectInstallation();
    void projectTopology();
    void projectLocations();
    void projectGroupAddresses();
    void projectManufacturerData();
    void projectMasterData();
    void projectTrades();
    void windowClose();

    void on_knxnet_connectionCreated(const KNX::IP_Host_Protocol_Address_Information remote_control_endpoint);

private:
    /* actions */
    void createActions();
    QAction * applicationExitAction{nullptr};
    QAction * documentNewAction{nullptr};
    QAction * documentOpenFileAction{nullptr};
    QAction * documentOpenDirAction{nullptr};
    QAction * documentSaveAction{nullptr};
    QAction * documentSaveAsAction{nullptr};
    QAction * documentCloseAction{nullptr};
    QAction * documentImportKnxProdAction{nullptr};
#ifdef OPTION_EXPORT_OpenHAB
    QAction * documentExportAsOpenHABAction{nullptr};
#endif
#ifdef OPTION_EXPORT_SmartHomeNG
    QAction * documentExportAsSmartHomeNGAction{nullptr};
#endif
    QAction * editCopyAction {nullptr};
    QAction * editCutAction{nullptr};
    QAction * editDeleteAction{nullptr};
    QAction * editPasteAction{nullptr};
    QAction * projectKNXAction{nullptr};
    QAction * projectInstallationAction{nullptr};
    QAction * projectTopologyAction{nullptr};
    QAction * projectLocationsAction{nullptr};
    QAction * projectGroupAddressesAction{nullptr};
    QAction * projectTradesAction{nullptr};
    QAction * projectManufacturerDataAction{nullptr};
    QAction * projectMasterDataAction{nullptr};
    QAction * knxBusInterfacesAction{nullptr};
    // Application Layer
    QAction * knxApplicationLayerADCViewAction{nullptr};
    QAction * knxApplicationLayerAuthorizeViewAction{nullptr};
    QAction * knxApplicationLayerDeviceDescriptorViewAction{nullptr};
    QAction * knxApplicationLayerDomainAddressViewAction{nullptr};
    QAction * knxApplicationLayerFileStreamViewAction{nullptr};
    QAction * knxApplicationLayerGroupValueViewAction{nullptr};
    QAction * knxApplicationLayerApplicationLayerIndividualAddressViewAction{nullptr};
    QAction * knxApplicationLayerKeyViewAction{nullptr};
    QAction * knxApplicationLayerLinkViewAction{nullptr};
    QAction * knxApplicationLayerMemoryViewAction{nullptr};
    QAction * knxApplicationLayerNetworkParameterViewAction{nullptr};
    QAction * knxApplicationLayerPropertyViewAction{nullptr};
    QAction * knxApplicationLayerRestartViewAction{nullptr};
    QAction * knxApplicationLayerSystemNetworkParameterViewAction{nullptr};
    QAction * knxApplicationLayerUserManufacturerInfoViewAction{nullptr};
    // Network Management
    /* NM_IndividualAddress_Read */
    /* NM_IndividualAddress_Write */
    /* NM_IndividualAddress_SerialNumber_Read */
    /* NM_IndividualAddress_SerialNumber_Write */
    /* NM_IndividualAddress_SerialNumber_Write2 */
    /* NM_IndividualAddress_SerialNumber_Write3 */
    QAction * knxNetworkManagementIndividualAddressViewAction{nullptr};
    /* NM_DomainAddress_Read */
    /* NM_DomainAndIndividualAddress_Read */
    /* NM_DomainAndIndividualAddress_Write */
    /* NM_DomainAndIndividualAddress_Write2 */
    /* NM_DomainAndIndividualAddress_Write3 */
    /* NM_DomainAddress_Scan2 */
    /* NM_Router_Scan */
    /* NM_SubnetworkDevices_Scan */
    /* NM_IndividualAddress_Reset */
    /* NM_IndividualAddress_Check */
    /* NM_Read_SerialNumber_by_ProgrammingMode */
    /* NM_Read_SerialNumber_By_ExFactoryState */
    /* NM_Read_SerialNumber_By_PowerReset */
    /* NM_Read_SerialNumber_Manufacturer */
    /* NM_NetworkParameter_Write_R */
    /* NM_IndividualAddress_Check_LocalSubNetwork */
    /* NM_IndividualAddress_SerialNumber_Report */
    /* NM_NetworkParameter_Read_R */
    /* NM_GroupAddress_Scan */
    /* NM_ObjectIndex_Read */
    /* NM_SerialNumberDefaultIA_Scan */
    // Device Management
    /* DM_Connect */
    /* DM_DeviceDescriptor_InfoReport */
    /* DM_Disconnect */
    /* DM_Identify */
    /* DM_Authorize */
    /* DM_Authorize2_RCo */
    /* DM_SetKey */
    /* DM_SetKey_RCo */
    /* DM_Restart */
    /* DM_Restart_RCl */
    /* DM_Restart_RCo */
    /* DM_Delay */
    /* DM_IndividualAddressRead */
    /* DM_IndividualAddressWrite */
    /* DM_DomainAddress_Read */
    /* DM_DomainAddressWrite */
    /* DM_ProgMode_Switch */
    /* DM_PeiTypeVerify */
    /* DM_PeiTypeRead */
    /* DM_MemWrite */
    /* DM_MemVerify */
    /* DM_MemRead */
    /* DM_UserMemWrite */
    /* DM_UserMemVerify */
    /* DM_UserMemRead */
    /* DM_InterfaceObjectWrite */
    /* DM_InterfaceObjectVerify */
    /* DM_InterfaceObjectRead */
    /* DM_InterfaceObjectScan */
    /* DM_InterfaceObjectInfoReport */
    /* DM_FunctionProperty_Write_R */
    /* DM_LoadStateMachineWrite */
    /* DM_LoadStateMachineVerify */
    /* DM_LoadStateMachineVerify_RCo_Mem */
    /* DM_LoadStateMachineVerify_R_IO */
    /* DM_LoadStateMachineRead */
    /* DM_RunStateMachineWrite */
    /* DM_RunStateMachineVerify */
    /* DM_RunStateMachineRead */
    /* DM_GroupObjectLink_Read */
    /* DM_GroupObjectLink_Write */
    /* DM_GroupObjectLink_Read_RCl */
    /* DM_GroupObjectLink_Write_RCl */
    /* DM_LCSlaceMemWrite */
    /* DM_LCSlaveMemVerify */
    /* DM_LCSlaveMemRead */
    /* DM_LCSlaveMemWrite */
    /* DM_LCExtMemVerify */
    /* DM_LCExtMemRead */
    /* DM_LCExtMemOpen */
    /* DM_LCRouteTableStateWrite */
    /* DM_LCRouteTableStateVerify */
    /* DM_LCRouteTableStateRead */
    QAction * windowCloseAction{nullptr};
    QAction * helpDebugAction{nullptr};
    QAction * helpAboutAction{nullptr};
    QAction * helpAboutQtAction{nullptr};

    /* menus */
    void createMenus();
    QMenu * fileMenu{nullptr};
    QMenu * recentPathsMenu{nullptr};
    QMenu * importMenu{nullptr};
#if defined(OPTION_EXPORT_OpenHAB) || defined(OPTION_EXPORT_SmartHomeNG)
    QMenu * exportMenu {nullptr};
#endif
    QMenu * editMenu {nullptr};
    QMenu * knxMenu{nullptr};
    QMenu * knxApplicationLayerMenu{nullptr};
    QMenu * knxNetworkManagementMenu{nullptr};
    QMenu * knxDeviceManagementMenu{nullptr};
    QMenu * etsProjectMenu{nullptr};
    QMenu * windowMenu{nullptr};
    QMenu * helpMenu{nullptr};

    /* tool bars */
    void createToolBars();
    QToolBar * fileToolBar{nullptr};
    QToolBar * knxToolBar{nullptr};
    QToolBar * etsProjectToolBar{nullptr};

    /* status bar */
    void createStatusBar();

    void loadPath(const QString & path);
    void savePath(const QString & path);
    void setCurrentPath(const QString & path);
    void updateRecentPathActions();

    /* recent paths */
    const int maxRecentPaths{10};
    QList<QAction *> recentPathActions{};

    /** KNX/IP network stack */
    KNXnet * knxnet{nullptr};

    /** Connection */
    Connection * connection{nullptr}; // @todo support multiple connections
};
