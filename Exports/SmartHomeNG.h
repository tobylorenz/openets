#pragma once

#include <QObject>
#include <QString>

#include <Project/v20/knx/Locations_t.h>

class SmartHomeNG : public QObject
{
    Q_OBJECT

public:
    explicit SmartHomeNG(QObject * parent = nullptr);

    void save(const QString & fileName, const Project::v20::knx::Locations_t * locations);
};
