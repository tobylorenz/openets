#include "SmartHomeNG.h"

#include <QDebug>
#include <QFile>
#include <QMap>
#include <QString>

#include <yaml-cpp/yaml.h>

#include <Project/v20/knx/ComObjectInstanceRef_t.h>
#include <Project/v20/knx/ComObjectRef_t.h>
#include <Project/v20/knx/DatapointType_DatapointSubtypes_DatapointSubtype_t.h>
#include <Project/v20/knx/DatapointType_t.h>
#include <Project/v20/knx/DeviceInstance_ComObjectInstanceRefs_t.h>
#include <Project/v20/knx/GroupAddress_t.h>
#include <Project/v20/knx/Hardware_Products_Product_t.h>
#include <Project/v20/knx/KNX_t.h>
#include <Project/v20/knx/ManufacturerData_Manufacturer_t.h>
#include <Project/v20/knx/MasterData_Manufacturers_Manufacturer_t.h>
#include <Project/v20/knx/Project_Installations_Installation_t.h>
#include <Project/v20/knx/Topology_Area_Line_t.h>
#include <Project/v20/knx/Topology_Area_t.h>

static QString toIdentifier(const QString & str)
{
    return str
           .trimmed()
           .replace(' ', '_')
           .replace("ä", "ae")
           .replace("ö", "öe")
           .replace("ü", "ue")
           .replace("ß", "ss");
}

static YAML::Emitter & operator<<(YAML::Emitter & out, const Project::v20::knx::GroupAddress_t * groupAddress)
{
    Q_ASSERT(groupAddress);

    uint16_t address = groupAddress->Address.toUInt();
    int address1 = (address >> 11) & 0x1f;
    int address2 = (address >>  8) & 0x07;
    int address3 = (address >>  0) & 0xff;
    QString nodeId = QString("GroupAddress_%1_%2_%3").arg(address1).arg(address2).arg(address3);
    out << YAML::Key << nodeId.toStdString();
    out << YAML::BeginMap;
    if (!groupAddress->Name.isEmpty()) {
        out << YAML::Key << "name" << YAML::Value << groupAddress->Name.toStdString();
    }
    if (groupAddress->DatapointType.isEmpty()) {
        qWarning() << "GroupAddress" << QString("%1/%2/%3").arg(address1).arg(address2).arg(address3) << "(" + groupAddress->Id + ")" << "has no datapointSubType";
    } else {
        Project::v20::knx::DatapointType_DatapointSubtypes_DatapointSubtype_t * datapointSubtype = groupAddress->getDatapointType();
        if (!datapointSubtype) {
            qWarning() << "GroupAddress" << QString("%1/%2/%3").arg(address1).arg(address2).arg(address3) << "(" + groupAddress->Id + ")" << "has invalid datapointSubtype" << groupAddress->DatapointType;
        } else {
            Q_ASSERT(datapointSubtype);
            Project::v20::knx::DatapointType_t * datapointType = datapointSubtype->findParent<Project::v20::knx::DatapointType_t *>();
            Q_ASSERT(datapointType);
            switch (datapointType->Number.toUInt()) {
            case 1:
                out << YAML::Key << "type" << YAML::Value << "bool";
                break;
            case 2:
                out << YAML::Key << "type" << YAML::Value << "list";
                break;
            case 3:
                out << YAML::Key << "type" << YAML::Value << "list";
                break;
            case 4:
                out << YAML::Key << "type" << YAML::Value << "str";
                break;
            case 5:
                out << YAML::Key << "type" << YAML::Value << "num";
                break;
            case 6:
                out << YAML::Key << "type" << YAML::Value << "num";
                break;
            case 7:
                out << YAML::Key << "type" << YAML::Value << "num";
                break;
            case 8:
                out << YAML::Key << "type" << YAML::Value << "num";
                break;
            case 9:
                out << YAML::Key << "type" << YAML::Value << "num";
                break;
            case 10:
                out << YAML::Key << "type" << YAML::Value << "foo";
                break;
            case 11:
                out << YAML::Key << "type" << YAML::Value << "foo";
                break;
            case 12:
                out << YAML::Key << "type" << YAML::Value << "num";
                break;
            case 13:
                out << YAML::Key << "type" << YAML::Value << "num";
                break;
            case 14:
                out << YAML::Key << "type" << YAML::Value << "num";
                break;
            case 16:
                out << YAML::Key << "type" << YAML::Value << "str";
                break;
            case 17:
                out << YAML::Key << "type" << YAML::Value << "num";
                break;
            case 18:
                out << YAML::Key << "type" << YAML::Value << "num";
                break;
            case 20:
                out << YAML::Key << "type" << YAML::Value << "num";
                break;
            case 24:
                out << YAML::Key << "type" << YAML::Value << "str";
                break;
            case 229:
                out << YAML::Key << "type" << YAML::Value << "list";
                break;
            case 232:
                out << YAML::Key << "type" << YAML::Value << "list";
                break;
            case 275:
                out << YAML::Key << "type" << YAML::Value << "list";
                break;
            default:
                qWarning() << "GroupAddress" << QString("%1/%2/%3").arg(address1).arg(address2).arg(address3) << "(" + groupAddress->Id + ")" << " datapointSubtype" << groupAddress->DatapointType << "cannot be mapped to SmartHomeNG";
            }
            out << YAML::Key << "knx_dpt" << YAML::Value << QString("%1.%2").arg(datapointType->Number).arg(datapointSubtype->Number.toUInt(), 3, 10, QChar('0')).toStdString();
        }
    }
    out << YAML::Key << "knx_listen" << YAML::Value << QString("%1/%2/%3").arg(address1).arg(address2).arg(address3).toStdString(); // @todo knx_init, knx_listen, knx_poll, knx_reply, knx_send, knx_status
    out << YAML::Key << "visu_acl" << YAML::Value << "rw"; // @todo visu_acl can be rw, ro, no
    out << YAML::EndMap;
    return out;
}

static YAML::Emitter & operator<<(YAML::Emitter & out, const Project::v20::knx::DeviceInstance_t * deviceInstance)
{
    Q_ASSERT(deviceInstance);

    Project::v20::knx::Topology_Area_Line_t * line = deviceInstance->findParent<Project::v20::knx::Topology_Area_Line_t *>();
    Q_ASSERT(line);
    Project::v20::knx::Topology_Area_t * area = line->findParent<Project::v20::knx::Topology_Area_t *>();
    Q_ASSERT(area);
    QString nodeId = QString("Device_%1_%2_%3").arg(area->Address).arg(line->Address).arg(deviceInstance->Address);
    out << YAML::Key << nodeId.toStdString();
    out << YAML::BeginMap;
    if (!deviceInstance->Name.isEmpty()) {
        out << YAML::Key << "name" << YAML::Value << deviceInstance->Name.toStdString();
    }
    QString remark;
    Project::v20::knx::Hardware_Products_Product_t * product = deviceInstance->getProduct();
    Q_ASSERT(product);
    Project::v20::knx::ManufacturerData_Manufacturer_t * manufacturer1 = product->findParent<Project::v20::knx::ManufacturerData_Manufacturer_t *>();
    Project::v20::knx::MasterData_Manufacturers_Manufacturer_t * manufacturer2 = manufacturer1->getManufacturer();
    if (manufacturer2) {
        remark = "Manufacturer: " + manufacturer2->Name + "; ";
    }
    remark += "Product: " + product->Text;
    out << YAML::Key << "remark" << YAML::Value << remark.toStdString();
    Project::v20::knx::DeviceInstance_ComObjectInstanceRefs_t * comObjectInstanceRefs = deviceInstance->ComObjectInstanceRefs;
    if (comObjectInstanceRefs) {
        Project::v20::knx::Project_Installations_Installation_t * installation = area->findParent<Project::v20::knx::Project_Installations_Installation_t *>();
        Q_ASSERT(installation);
        Project::v20::knx::Project_t * project = installation->findParent<Project::v20::knx::Project_t *>();
        Q_ASSERT(project);
        Project::v20::knx::KNX_t * knx = project->findParent<Project::v20::knx::KNX_t *>();
        Q_ASSERT(knx);
        Q_FOREACH (const Project::v20::knx::ComObjectInstanceRef_t * comObjectInstanceRef, comObjectInstanceRefs->ComObjectInstanceRef) {
            Q_ASSERT(comObjectInstanceRef);
            Q_FOREACH (const QString & link, comObjectInstanceRef->Links) {
                // Links is of form GA-17
                Project::v20::knx::IDREF groupAddressRefId = project->Id + "-" + (installation->InstallationId.isEmpty() ? "0" : installation->InstallationId) +  "_" + link;
                // now we have an Id of form P-03FD-0_GA-17
                Project::v20::knx::GroupAddress_t * groupAddress = qobject_cast<Project::v20::knx::GroupAddress_t *>(knx->ids[groupAddressRefId]);
                Q_ASSERT(groupAddress);
                out << groupAddress;
            }
        }
    }
    out << YAML::EndMap;
    return out;
}

static YAML::Emitter & operator<<(YAML::Emitter & out, const Project::v20::knx::Space_t * space)
{
    Q_ASSERT(space);

    out << YAML::Key << toIdentifier(space->Name).toStdString();
    out << YAML::BeginMap;
    out << YAML::Key << "name" << YAML::Value << space->Name.toStdString();
    Q_FOREACH (const Project::v20::knx::Space_t * subSpace, space->Space) {
        Q_ASSERT(subSpace);
        out << subSpace;
    }
    Q_FOREACH (const Project::v20::knx::DeviceInstanceRef_t * deviceInstanceRef, space->DeviceInstanceRef) {
        Q_ASSERT(deviceInstanceRef);
        Project::v20::knx::DeviceInstance_t * deviceInstance = deviceInstanceRef->getDeviceInstance();
        if (deviceInstance) {
            out << deviceInstance;
        }
    }
    out << YAML::EndMap;
    return out;
}

static YAML::Emitter & operator<<(YAML::Emitter & out, const Project::v20::knx::Locations_t * locations)
{
    Q_ASSERT(locations);

    out << YAML::BeginMap;
    Q_FOREACH (const Project::v20::knx::Space_t * space, locations->Space) {
        Q_ASSERT(space);
        out << space;
    }
    out << YAML::EndMap;
    return out;
}

SmartHomeNG::SmartHomeNG(QObject * parent) :
    QObject(parent)
{
}

void SmartHomeNG::save(const QString & fileName, const Project::v20::knx::Locations_t * locations)
{
    Q_ASSERT(locations);

    YAML::Emitter emitter;
    emitter << locations;

    QFile file(fileName);
    file.open(QIODevice::WriteOnly);
    if (!file.isOpen()) {
        qWarning() << "Unable to open file" << fileName << "for writing";
        return;
    }
    file.write(emitter.c_str());
    file.close();
}
