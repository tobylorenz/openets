#pragma once

#include <QObject>
#include <QString>

#include <Project/v20/knx/Topology_Area_Line_t.h>

class OpenHAB : public QObject
{
    Q_OBJECT

public:
    explicit OpenHAB(QObject * parent = nullptr);

    void saveThings(const QString & fileName, const Project::v20::knx::Topology_Area_Line_t * line, const Project::v20::knx::DeviceInstance_t * router);
    void saveItems(const QString & fileName, const Project::v20::knx::Topology_Area_Line_t * line);
};
