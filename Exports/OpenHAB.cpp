#include "OpenHAB.h"

#include <QFile>

#include <Project/v20/knx/DatapointType_DatapointSubtypes_DatapointSubtype_t.h>
#include <Project/v20/knx/DatapointType_t.h>
#include <Project/v20/knx/GroupAddress_t.h>
#include <Project/v20/knx/GroupAddresses_GroupRanges_t.h>
#include <Project/v20/knx/GroupAddresses_t.h>
#include <Project/v20/knx/GroupRange_t.h>
#include <Project/v20/knx/Hardware_Products_Product_t.h>
#include <Project/v20/knx/Locations_t.h>
#include <Project/v20/knx/ManufacturerData_Manufacturer_t.h>
#include <Project/v20/knx/MasterData_Manufacturers_Manufacturer_t.h>
#include <Project/v20/knx/Project_Installations_Installation_t.h>
#include <Project/v20/knx/Topology_Area_t.h>
#include <Project/v20/knx/Topology_t.h>

static QString toIdentifier(const QString & str)
{
    return str
        .trimmed()
        .replace('-', '_')
        .replace(' ', '_')
        .replace("ä", "ae")
        .replace("ö", "öe")
        .replace("ü", "ue")
        .replace("ß", "ss");
}

OpenHAB::OpenHAB(QObject * parent) :
    QObject(parent)
{
}

void OpenHAB::saveThings(const QString & fileName, const Project::v20::knx::Topology_Area_Line_t * line, const Project::v20::knx::DeviceInstance_t * router)
{
    Q_ASSERT(line);
    Q_ASSERT(router);

    QFile file(fileName);
    file.open(QIODevice::WriteOnly);
    if (!file.isOpen()) {
        qWarning() << "Unable to open file" << fileName << "for writing";
        return;
    }
    //file.write(emitter.c_str());

    const Project::v20::knx::Topology_Area_t * area = line->findParent<Project::v20::knx::Topology_Area_t *>();
    Q_ASSERT(area);
    const Project::v20::knx::Project_Installations_Installation_t * installation = line->findParent<Project::v20::knx::Project_Installations_Installation_t *>();
    Q_ASSERT(installation);
    const Project::v20::knx::Locations_t * locations = installation->Locations;
    Q_ASSERT(locations);
    const Project::v20::knx::GroupAddresses_t * groupAddresses = installation->GroupAddresses;
    Q_ASSERT(groupAddresses);
    const Project::v20::knx::GroupAddresses_GroupRanges_t * groupRanges = groupAddresses->GroupRanges;
    Q_ASSERT(groupRanges);

    // OpenHAB Bridge resp. KNX Router
    const Project::v20::knx::Hardware_Products_Product_t * product = router->getProduct();
    Q_ASSERT(product);
    const Project::v20::knx::ManufacturerData_Manufacturer_t * manufacturer1 = product->findParent<Project::v20::knx::ManufacturerData_Manufacturer_t *>();
    Q_ASSERT(manufacturer1);
    const Project::v20::knx::MasterData_Manufacturers_Manufacturer_t * manufacturer2 = manufacturer1->getManufacturer();
    Q_ASSERT(manufacturer2);
    const Project::v20::knx::Space_t * space{nullptr};
    for (const Project::v20::knx::DeviceInstanceRef_t * deviceInstanceRef : locations->findChildren<Project::v20::knx::DeviceInstanceRef_t *>()) {
        Q_ASSERT(deviceInstanceRef);
        if (deviceInstanceRef->RefId == router->Id) {
            space = deviceInstanceRef->findParent<Project::v20::knx::Space_t *>();
            break;
        }
    }
    if (space) {
        file.write("Bridge knx:ip:bridge \"" + manufacturer2->Name.toUtf8() + " " + product->OrderNumber.toUtf8() + "\" @ \"" + space->Name.toUtf8() + "\" [\n");
    } else {
        file.write("Bridge knx:ip:bridge \"" + manufacturer2->Name.toUtf8() + " " + product->OrderNumber.toUtf8() + "\" [\n");
    }
    file.write("    type=\"TUNNEL\",\n");
    file.write("    ipAddress=\"knx-iprt-haus\",\n"); // @todo ip address (hostname only available, if not default set)
    file.write("    portNumber=3671,\n");
    file.write("//    localIp=\"x.x.x.x\",\n");
    file.write("//    localSourceAddr=\"x.x.x\",\n");
    file.write("    useNAT=false,\n");
    file.write("    readingPause=50,\n");
    file.write("    responseTimeout=10,\n");
    file.write("    readRetriesLimit=3,\n");
    file.write("    autoReconnectPeriod=60\n");
    file.write("] {\n");

    // OpenHAB Things resp. KNX Devices
    QMap<uint16_t, const Project::v20::knx::DeviceInstance_t *> lineDevicesByAddress{};
    for (const Project::v20::knx::DeviceInstance_t * deviceInstance : line->DeviceInstance) {
        Q_ASSERT(deviceInstance);
        lineDevicesByAddress[deviceInstance->Address.toUInt()] = deviceInstance;
    }
    for (const Project::v20::knx::DeviceInstance_t * deviceInstance : lineDevicesByAddress) {
        const Project::v20::knx::Hardware_Products_Product_t * product = deviceInstance->getProduct();
        Q_ASSERT(product);
        const Project::v20::knx::ManufacturerData_Manufacturer_t * manufacturer1 = product->findParent<Project::v20::knx::ManufacturerData_Manufacturer_t *>();
        Q_ASSERT(manufacturer1);
        const Project::v20::knx::MasterData_Manufacturers_Manufacturer_t * manufacturer2 = manufacturer1->getManufacturer();
        Q_ASSERT(manufacturer2);
        const Project::v20::knx::Space_t * space{nullptr};
        for (const Project::v20::knx::DeviceInstanceRef_t * deviceInstanceRef : locations->findChildren<Project::v20::knx::DeviceInstanceRef_t *>()) {
            Q_ASSERT(deviceInstanceRef);
            if (deviceInstanceRef->RefId == deviceInstance->Id) {
                space = deviceInstanceRef->findParent<Project::v20::knx::Space_t *>();
                break;
            }
        }

        if (space) {
            file.write("    Thing device knx_device_" + area->Address.toUtf8() + "_" + line->Address.toUtf8() + "_" + deviceInstance->Address.toUtf8() + " \"" + manufacturer2->Name.toUtf8() + " " + product->OrderNumber.toUtf8() + "\" @ \"" + space->Name.toUtf8() + "\" [ address=\"" + area->Address.toUtf8() + "." + line->Address.toUtf8() + "." + deviceInstance->Address.toUtf8() + "\", fetch=true, pingInterval=600, readInterval=0 ] { }\n");
        } else {
            file.write("    Thing device knx_device_" + area->Address.toUtf8() + "_" + line->Address.toUtf8() + "_" + deviceInstance->Address.toUtf8() + " \"" + manufacturer2->Name.toUtf8() + " " + product->OrderNumber.toUtf8() + "\" [ address=\"" + area->Address.toUtf8() + "." + line->Address.toUtf8() + "." + deviceInstance->Address.toUtf8() + "\", fetch=true, pingInterval=600, readInterval=0 ] { }\n");
        }
    }

    // OpenHAB Types resp. KNX Group Addresses
    file.write("    Thing device knx_device_generic \"KNX Group Addresses\" \[ ] {\n");
    // @todo three layers are assumed
    QMap<uint16_t, const Project::v20::knx::GroupRange_t *> mainGroupByAddress{};
    for (const Project::v20::knx::GroupRange_t * groupRange1 : groupRanges->GroupRange) {
        Q_ASSERT(groupRange1);
        mainGroupByAddress[groupRange1->RangeStart.toUInt()] = groupRange1;
    }
    for (const Project::v20::knx::GroupRange_t * groupRange1 : mainGroupByAddress) {
        Q_ASSERT(groupRange1);
        const uint16_t mainGroup = (groupRange1->RangeStart.toUInt() >> 11) & 0x1f;
        file.write("        // " + QString::number(mainGroup).toUtf8() + " " + groupRange1->Name.toUtf8() + "\n");
        QMap<uint16_t, const Project::v20::knx::GroupRange_t *> middleGroupByAddress{};
        for (const Project::v20::knx::GroupRange_t * groupRange2 : groupRange1->GroupRange) {
            Q_ASSERT(groupRange2);
            middleGroupByAddress[groupRange2->RangeStart.toUInt()] = groupRange2;
        }
        for (const Project::v20::knx::GroupRange_t * groupRange2 : middleGroupByAddress) {
            Q_ASSERT(groupRange2);
            const uint16_t middleGroup = (groupRange2->RangeStart.toUInt() >> 8) & 0x7;
            file.write("        // " + QString::number(mainGroup).toUtf8() + "/" + QString::number(middleGroup).toUtf8() + " " + groupRange2->Name.toUtf8() + "\n");

            QMap<uint16_t, const Project::v20::knx::GroupAddress_t *> groupAddressByAddress{};
            for (const Project::v20::knx::GroupAddress_t * groupAddress : groupRange2->GroupAddress) {
                Q_ASSERT(groupAddress);
                groupAddressByAddress[groupAddress->Address.toUInt()] = groupAddress;
            }
            for (const Project::v20::knx::GroupAddress_t * groupAddress : groupAddressByAddress) {
                Q_ASSERT(groupAddress);
                const uint16_t subGroup = groupAddress->Address.toUInt() & 0xff;
                const Project::v20::knx::DatapointType_DatapointSubtypes_DatapointSubtype_t * datapointSubtype = groupAddress->getDatapointType();
                Q_ASSERT(datapointSubtype);
                const Project::v20::knx::DatapointType_t * datapointType = datapointSubtype->findParent<Project::v20::knx::DatapointType_t *>();
                Q_ASSERT(datapointType);
                QString type{"foo"};
                // - switch
                // - dimmer
                // - rollershutter
                // - contact
                // - number
                // - string
                // - datetime
                switch (datapointType->Number.toUInt()) {
                case 1:
                    type = "switch";
                    break;
                case 4:
                    type = "string";
                    break;
                case 5:
                    type = "number";
                    break;
                case 6:
                    type = "number";
                    break;
                case 7:
                    type = "number";
                    break;
                case 8:
                    type = "number";
                    break;
                case 9:
                    type = "number";
                    break;
                case 10:
                    type = "datetime";
                    break;
                case 11:
                    type = "datetime";
                    break;
                case 12:
                    type = "number";
                    break;
                case 13:
                    type = "number";
                    break;
                case 14:
                    type = "number";
                    break;
                case 16:
                    type = "string";
                    break;
                case 17:
                    type = "number";
                    break;
                case 18:
                    type = "number";
                    break;
                case 19:
                    type = "datetime";
                    break;
                case 20:
                    type = "number";
                    break;
                case 24:
                    type = "string";
                    break;
                default:
                    qWarning() << "GroupAddress" << QString("%1/%2/%3").arg(mainGroup).arg(middleGroup).arg(subGroup) << "(" + groupAddress->Id + ")" << " datapointSubtype" << groupAddress->DatapointType << "cannot be mapped to OpenHAB";
                }
                file.write("        Type " + type.toUtf8() + " : knx_" + toIdentifier(groupRange1->Name).toUtf8() + "_" + toIdentifier(groupRange2->Name).toUtf8() + "_" + toIdentifier(groupAddress->Name).toUtf8() + " \"" + groupAddress->Name.toUtf8() + "\" [ ga=\"" + QString("%1.%2").arg(datapointType->Number).arg(datapointSubtype->Number.toUInt(), 3, 10, QChar('0')).toUtf8() + ":</" + QString::number(mainGroup).toUtf8() + "/" + QString::number(middleGroup).toUtf8() + "/" + QString::number(subGroup).toUtf8() + "\" ]\n");
            }
        }
    }
    file.write("    } // Thing\n");
    file.write("] // Bridge\n");
    file.close();
}

void OpenHAB::saveItems(const QString & fileName, const Project::v20::knx::Topology_Area_Line_t * line)
{
    QFile file(fileName);
    file.open(QIODevice::WriteOnly);
    if (!file.isOpen()) {
        qWarning() << "Unable to open file" << fileName << "for writing";
        return;
    }

    const Project::v20::knx::Project_Installations_Installation_t * installation = line->findParent<Project::v20::knx::Project_Installations_Installation_t *>();
    Q_ASSERT(installation);
    const Project::v20::knx::Locations_t * locations = installation->Locations;
    Q_ASSERT(locations);
    const Project::v20::knx::GroupAddresses_t * groupAddresses = installation->GroupAddresses;
    Q_ASSERT(groupAddresses);
    const Project::v20::knx::GroupAddresses_GroupRanges_t * groupRanges = groupAddresses->GroupRanges;
    Q_ASSERT(groupRanges);

    // @todo three layers are assumed
    QMap<uint16_t, const Project::v20::knx::GroupRange_t *> mainGroupByAddress{};
    for (const Project::v20::knx::GroupRange_t * groupRange1 : groupRanges->GroupRange) {
        Q_ASSERT(groupRange1);
        mainGroupByAddress[groupRange1->RangeStart.toUInt()] = groupRange1;
    }
    for (const Project::v20::knx::GroupRange_t * groupRange1 : mainGroupByAddress) {
        Q_ASSERT(groupRange1);
        const uint16_t mainGroup = (groupRange1->RangeStart.toUInt() >> 11) & 0x1f;
        file.write("// " + QString::number(mainGroup).toUtf8() + " " + groupRange1->Name.toUtf8() + "\n");
        QMap<uint16_t, const Project::v20::knx::GroupRange_t *> middleGroupByAddress{};
        for (const Project::v20::knx::GroupRange_t * groupRange2 : groupRange1->GroupRange) {
            Q_ASSERT(groupRange2);
            middleGroupByAddress[groupRange2->RangeStart.toUInt()] = groupRange2;
        }
        for (const Project::v20::knx::GroupRange_t * groupRange2 : middleGroupByAddress) {
            Q_ASSERT(groupRange2);
            const uint16_t middleGroup = (groupRange2->RangeStart.toUInt() >> 8) & 0x7;
            file.write("// " + QString::number(mainGroup).toUtf8() + "/" + QString::number(middleGroup).toUtf8() + " " + groupRange2->Name.toUtf8() + "\n");

            QMap<uint16_t, const Project::v20::knx::GroupAddress_t *> groupAddressByAddress{};
            for (const Project::v20::knx::GroupAddress_t * groupAddress : groupRange2->GroupAddress) {
                Q_ASSERT(groupAddress);
                groupAddressByAddress[groupAddress->Address.toUInt()] = groupAddress;
            }
            for (const Project::v20::knx::GroupAddress_t * groupAddress : groupAddressByAddress) {
                Q_ASSERT(groupAddress);
                const uint16_t subGroup = groupAddress->Address.toUInt() & 0xff;
                const Project::v20::knx::DatapointType_DatapointSubtypes_DatapointSubtype_t * datapointSubtype = groupAddress->getDatapointType();
                Q_ASSERT(datapointSubtype);
                const Project::v20::knx::DatapointType_t * datapointType = datapointSubtype->findParent<Project::v20::knx::DatapointType_t *>();
                Q_ASSERT(datapointType);
                QString type{"foo"};
                // - Call
                // - Color
                // - Contact
                // - DateTime
                // - Dimmer
                // - Image
                // - Location
                // - Number
                // - Player
                // - Rollershutter
                // - String
                // - Switch
                // - Group
                switch (datapointType->Number.toUInt()) {
                case 1:
                    type = "Switch";
                    break;
                case 4:
                    type = "String";
                    break;
                case 5:
                    type = "Number";
                    break;
                case 6:
                    type = "Number";
                    break;
                case 7:
                    type = "Number";
                    break;
                case 8:
                    type = "Number";
                    break;
                case 9:
                    type = "Number";
                    break;
                case 10:
                    type = "DateTime";
                    break;
                case 11:
                    type = "DateTime";
                    break;
                case 12:
                    type = "Number";
                    break;
                case 13:
                    type = "Number";
                    break;
                case 14:
                    type = "Number";
                    break;
                case 16:
                    type = "string";
                    break;
                case 17:
                    type = "Number";
                    break;
                case 18:
                    type = "Number";
                    break;
                case 19:
                    type = "DateTime";
                    break;
                case 20:
                    type = "Number";
                    break;
                case 24:
                    type = "String";
                    break;
                default:
                    qWarning() << "GroupAddress" << QString("%1/%2/%3").arg(mainGroup).arg(middleGroup).arg(subGroup) << "(" + groupAddress->Id + ")" << " datapointSubtype" << groupAddress->DatapointType << "cannot be mapped to OpenHAB";
                }
                // @todo Numbers can have :Dimensions
                file.write(type.toUtf8() + " " + toIdentifier(groupRange1->Name).toUtf8() + "_" + toIdentifier(groupRange2->Name).toUtf8() + "_" + toIdentifier(groupAddress->Name).toUtf8() + " \"" + groupRange1->Name.toUtf8() + " - " + groupRange2->Name.toUtf8() + " - " + groupAddress->Name.toUtf8() + "\" <> () [] {\n");
                // @todo <> is for the icon, e.g. temperature
                // @todo () is for the group, e.g. location
                // @todo [] is for for semantic class (e.g. Measurement) and semantic property (e.g. Temperature)
                file.write("    channel=\"knx:device:bridge:knx_device_generic:knx_" + toIdentifier(groupRange1->Name).toUtf8() + "_" + toIdentifier(groupRange2->Name).toUtf8() + "_" + toIdentifier(groupAddress->Name).toUtf8() + "\" }\n");
            }
        }
    }
    file.close();
}
