#include "GroupData.h"

#include <Connection/Connection.h>

GroupData::GroupData(QObject * parent) :
    QObject(parent)
{
}

GroupData::~GroupData()
{
}

Stack * GroupData::getStack() const
{
    Connection * connection = qobject_cast<Connection *>(parent());
    Q_ASSERT(connection);

    return connection->getStack();
}

void GroupData::readGroupValue()
{
    getStack()->A_GroupValue_Read_req(KNX::Ack_Request::dont_care, address, KNX::Priority::low, KNX::Network_Layer_Parameter, [](const KNX::Status /*a_status*/){
        // Lcon
    });
}

void GroupData::writeGroupValue(const std::vector<uint8_t> groupObjectValue)
{
    getStack()->A_GroupValue_Write_req(KNX::Ack_Request::dont_care, address, KNX::Priority::low, KNX::Network_Layer_Parameter, groupObjectValue, [this, groupObjectValue](const KNX::Status a_status){
        if (a_status == KNX::Status::ok) {
            objectValues[QDateTime::currentDateTime()] = groupObjectValue;
            Q_EMIT groupValueChanged(groupObjectValue);
        }
    });
}
