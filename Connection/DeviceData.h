#pragma once

#include <QByteArray>
#include <QSet>

#include <KNX/03/05/01/Interface_Objects.h>
#include <KNX/Stack.h>
#include <Project/v20/knx/ApplicationProgram_t.h>
#include <Project/v20/knx/DeviceInstance_t.h>
#include <Project/v20/knx/Hardware2Program_t.h>
#include <Project/v20/knx/Hardware_Products_Product_t.h>

/**
 * This class contains data for a specific individual device
 * adressed by an individual address.
 */
class DeviceData :
    public QObject
{
    Q_OBJECT

public:
    explicit DeviceData(QObject * parent = nullptr);
    virtual ~DeviceData();

    /* Links to KNX stack */

    Stack * getStack() const;

    /** Individual Address */
    KNX::Individual_Address address{}; // ASAP_Individual, ASAP_Connected

    /** transport channel is connected (false=T_Data_Individual, true=T_Data_Connected) */
    bool transport_layer_connected{false};

    /* Device Descriptor */

    /** Device Descriptor */
    QMap<KNX::Descriptor_Type, std::vector<uint8_t>> device_descriptors{};

    /* File Stream */

    /* Interface Object Property Description/Value */

    /** Interface Objects */
    KNX::Interface_Objects interface_objects{};

    /** Interface Object Property Discovery State */
    enum Property_Discovery_State {
        Stopped,
        Scan_Objects,
        Scan_Properties,
    } property_discovery_state { Stopped };

    /* ADC */

    /** ADC */
    struct AdcData {
        uint8_t read_count;
        uint16_t sum;
    };
    QMap<KNX::ADC_Channel_Nr, AdcData> adc_data{}; // @todo time line, similar to group data

    /* Memory */

    /** Memory (64 kB) */
    QByteArray memory{}; // addressed via KNX::Memory_Address

    /** Memory Mask (64 kB) */
    QByteArray memory_mask{}; // 0x01 if synced with ETS project. 0x02 if synced with KNX device.

    /** user memory read request */
    struct User_Memory_Read_Request {
        /** request */
        bool request{false};

        /** address */
        KNX::UserMemory_Address address{0};

        /** number */
        KNX::UserMemory_Address size{0}; // @todo introduce KNX::UserMemory_Size?
    } user_memory_read_request;

    /* User Memory */

    /** User Memory (1 MB) */
    QByteArray user_memory{}; // addressed via KNX::UserMemory_Address

    /** User Memory Mask (64 kB) */
    QByteArray user_memory_mask{}; // 0x01 if synced with ETS project. 0x02 if synced with KNX device.

    /** memory read request */
    struct Memory_Read_Request {
        /** request */
        bool request{false};

        /** address */
        KNX::Memory_Address address{0};

        /** number */
        KNX::Memory_Address size{0}; // @todo introduce KNX::Memory_Size?
    } memory_read_request;

    /* User Manufacturer Info */

    /* Authorization */

    /** Authorization */
    uint8_t authorization_level{0};

    /* Key */

    /* Links to ETS Project */

    /** ETS Device Instance */
    const Project::v20::knx::DeviceInstance_t * ets_DeviceInstance{nullptr};

    /** ETS Product */
    const Project::v20::knx::Hardware_Products_Product_t * ets_Product{nullptr};

    /** ETS Hardware2Program */
    const Project::v20::knx::Hardware2Program_t * ets_Hardware2Program{nullptr};

    /** ETS ApplicationProgram */
    const Project::v20::knx::ApplicationProgram_t * ets_ApplicationProgram{nullptr};

public Q_SLOTS:
    /* high level slots */
    void transport_layer_connect();
    void transport_layer_disconnect();
    void read_device_descriptor(const KNX::Descriptor_Type descriptor_type);
    void start_property_discovery();
    void read_data_property(const KNX::Object_Index object_index, const KNX::Property_Index property_index);

    /* LdCtrl */
    void LdCtrlUnload(uint8_t lsmIdx, uint16_t objType, uint16_t occurrence);
    void LdCtrlLoad(uint8_t lsmIdx, uint16_t objType, uint16_t occurrence);
    void LdCtrlMaxLength(uint8_t lsmIdx, uint16_t objType, uint16_t occurrence, uint32_t size);
    void LdCtrlClearCachedObjectTypes();
    void LdCtrlLoadCompleted(uint8_t lsmIdx, uint16_t objType, uint16_t occurrence);
    void LdCtrlAbsSegment(uint8_t lsmIdx, uint16_t objType, uint16_t occurrence, uint8_t segType, uint16_t address, uint16_t size, uint8_t access, uint8_t memType, uint8_t segFlags);
    void LdCtrlRelSegment(uint8_t lsmIdx, uint16_t objType, uint16_t occurrence, uint32_t size, uint8_t mode, uint8_t fill);
    void LdCtrlTaskSegment(uint8_t lsmIdx, uint16_t objType, uint16_t occurrence, uint16_t address);
    void LdCtrlTaskPtr(uint8_t lsmIdx, uint16_t objType, uint16_t occurrence, uint16_t initPtr, uint16_t savePtr, uint16_t serialPtr);
    void LdCtrlTaskCtrl1(uint8_t lsmIdx, uint16_t objType, uint16_t occurrence, uint16_t address, uint8_t count);
    void LdCtrlTaskCtrl2(uint8_t lsmIdx, uint16_t objType, uint16_t occurrence, uint16_t callback, uint16_t address, uint16_t seg0, uint16_t seg1);
    void LdCtrlWriteProp(uint8_t lsmIdx, uint16_t objType, uint16_t occurrence, uint16_t propId, uint16_t startElement, uint16_t count, bool verify, std::vector<uint8_t> inlineData);
    void LdCtrlCompareProp(bool allowCachedValue, std::vector<uint8_t> inlineData, std::vector<uint8_t> mask, std::string range, bool invert, uint16_t retryInterval, uint16_t timeOut, uint8_t objIdx, uint16_t objType, uint16_t occurrence, uint16_t propId, uint16_t startElement, uint16_t count);
    void LdCtrlLoadImageProp(uint8_t objIdx, uint16_t objType, uint16_t occurrence, uint16_t propId, uint16_t startElement, uint16_t count);
    void LdCtrlInvokeFunctionProp(uint8_t objIdx, uint16_t objType, uint16_t occurrence, uint16_t propId, std::vector<uint8_t> inlineData);
    void LdCtrlReadFunctionProp(uint8_t objIdx, uint16_t objType, uint16_t occurrence, uint16_t propId);
    void LdCtrlWriteMem(std::string addressSpace, uint32_t address, uint32_t size, bool verify, std::vector<uint8_t> inlineData);
    void LdCtrlCompareMem(bool allowCachedValue, std::vector<uint8_t> inlineData, std::vector<uint8_t> mask, std::string range, bool invert, uint16_t retryInterval, uint16_t timeOut, std::string addressSpace, uint32_t address, uint32_t size);
    void LdCtrlLoadImageMem(std::string addressSpace, uint32_t address, uint32_t size);
    void LdCtrlWriteRelMem(uint8_t objIdx, uint16_t objType, uint16_t occurrence, uint32_t offset, uint32_t size, bool verify, std::vector<uint8_t> inlineData);
    void LdCtrlCompareRelMem(bool allowCachedValue, std::vector<uint8_t> inlineData, std::vector<uint8_t> mask, std::string range, bool invert, uint16_t retryInterval, uint16_t timeOut, uint8_t objIdx, uint16_t objType, uint16_t occurrence, uint32_t offset, uint32_t size);
    void LdCtrlLoadImageRelMem(uint8_t objIdx, uint16_t objType, uint16_t occurrence, uint32_t offset, uint32_t size);
    void LdCtrlConnect();
    void LdCtrlDisconnect();
    void LdCtrlRestart();
    void LdCtrlMasterReset(uint8_t eraseCode, uint8_t channelNumber);
    void LdCtrlDelay(uint16_t milliSeconds);
    void LdCtrlSetControlVariable(std::string name, bool value);
    void LdCtrlMapError(uint8_t ldCtrlFilter, uint32_t originalError, uint32_t mappedError);
    void LdCtrlProgressText(uint32_t textId, std::string messageRef);
    void LdCtrlDeclarePropDesc(uint8_t objIdx, uint16_t objType, uint16_t occurrence, uint16_t propId, std::string propType, uint16_t maxElements, uint8_t readAccess, uint8_t writeAccess, bool writeable);
    void LdCtrlClearLCFilterTable(bool useFunctionProp);
    void LdCtrlMerge(uint8_t mergeId);
    void LdCtrlBaseChoose();

Q_SIGNALS:
    /* high level signals */
    void transport_layer_connection_state_changed();
    void device_descriptor_changed(const KNX::Descriptor_Type descriptor_type, const std::vector<uint8_t> & device_descriptor);
    void property_updated(const KNX::Object_Index object_index, const KNX::Property_Index property_index);
    void adc_changed(const KNX::ADC_Channel_Nr channel_nr, const uint8_t read_count, const uint16_t sum);
    void memory_changed(const KNX::Memory_Address address);
    void user_memory_changed(const KNX::UserMemory_Address address);
    void authorization_level_changed(const KNX::Level level);

    /* ui signals */
    void memory_selected(bool userMemory, KNX::Memory_Address address, KNX::Memory_Address size);
};
