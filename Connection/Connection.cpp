#include "Connection.h"

#include <QDebug>

#include <KNX/03/05/01/Unknown_Interface_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>
#include <Project/v20/Helpers.h>
#include <Project/v20/knx/ApplicationProgramStatic_Code_AbsoluteSegment_t.h>
#include <Project/v20/knx/ApplicationProgramStatic_Code_t.h>
#include <Project/v20/knx/ApplicationProgramStatic_t.h>
#include <Project/v20/knx/DatapointType_DatapointSubtypes_t.h>
#include <Project/v20/knx/DeviceInstance_t.h>
#include <Project/v20/knx/GroupAddress_t.h>
#include <Project/v20/knx/GroupAddresses_GroupRanges_t.h>
#include <Project/v20/knx/GroupAddresses_t.h>
#include <Project/v20/knx/GroupRange_t.h>
#include <Project/v20/knx/Hardware2Program_t.h>
#include <Project/v20/knx/IDREF.h>
#include <Project/v20/knx/MasterData_t.h>
#include <Project/v20/knx/Project_Installations_t.h>
#include <Project/v20/knx/Project_ProjectInformation_t.h>
#include <Project/v20/knx/Project_t.h>
#include <Project/v20/knx/Topology_t.h>

Connection::Connection(QObject * parent) :
    QObject(parent)
{
    delete m_projectBundle;
}

Connection::~Connection()
{
}

void Connection::setStack(Stack * stack)
{
    Q_ASSERT(stack);
    this->m_stack = stack;

    connect(stack, &Stack::A_Connect_ind, this, &Connection::A_Connect_ind);
    connect(stack, &Stack::A_Disconnect_ind, this, &Connection::A_Disconnect_ind);

    /* Services (Group) */
    connect(stack, &Stack::A_GroupValue_Read_ind, this, &Connection::A_GroupValue_Read_ind);
    connect(stack, &Stack::A_GroupValue_Read_Acon, this, &Connection::A_GroupValue_Read_Acon);
    connect(stack, &Stack::A_GroupValue_Write_ind, this, &Connection::A_GroupValue_Write_ind);

    /* Services (Broadcast) */
    connect(stack, &Stack::A_IndividualAddress_Write_ind, this, &Connection::A_IndividualAddress_Write_ind);
    connect(stack, &Stack::A_IndividualAddress_Read_ind, this, &Connection::A_IndividualAddress_Read_ind);
    connect(stack, &Stack::A_IndividualAddress_Read_Acon, this, &Connection::A_IndividualAddress_Read_Acon);
    connect(stack, &Stack::A_IndividualAddressSerialNumber_Read_ind, this, &Connection::A_IndividualAddressSerialNumber_Read_ind);
    connect(stack, &Stack::A_IndividualAddressSerialNumber_Read_Acon, this, &Connection::A_IndividualAddressSerialNumber_Read_Acon);
    connect(stack, &Stack::A_IndividualAddressSerialNumber_Write_ind, this, &Connection::A_IndividualAddressSerialNumber_Write_ind);
    connect(stack, &Stack::A_NetworkParameter_Read_ind, this, &Connection::A_NetworkParameter_Read_ind);
    connect(stack, &Stack::A_NetworkParameter_Read_Acon, this, &Connection::A_NetworkParameter_Read_Acon);
    connect(stack, &Stack::A_NetworkParameter_Write_ind, this, &Connection::A_NetworkParameter_Write_ind);
    connect(stack, &Stack::A_NetworkParameter_InfoReport_ind, this, &Connection::A_NetworkParameter_InfoReport_ind);

    /* Services (System Broadcast) */
    connect(stack, &Stack::A_DeviceDescriptor_InfoReport_ind, this, &Connection::A_DeviceDescriptor_InfoReport_ind);
    connect(stack, &Stack::A_DomainAddress_Write_ind, this, &Connection::A_DomainAddress_Write_ind);
    connect(stack, &Stack::A_DomainAddress_Read_ind, this, &Connection::A_DomainAddress_Read_ind);
    connect(stack, &Stack::A_DomainAddress_Read_Acon, this, &Connection::A_DomainAddress_Read_Acon);
    connect(stack, &Stack::A_DomainAddressSelective_Read_ind, this, &Connection::A_DomainAddressSelective_Read_ind);
    connect(stack, &Stack::A_DomainAddressSerialNumber_Read_ind, this, &Connection::A_DomainAddressSerialNumber_Read_ind);
    connect(stack, &Stack::A_DomainAddressSerialNumber_Read_Acon, this, &Connection::A_DomainAddressSerialNumber_Read_Acon);
    connect(stack, &Stack::A_DomainAddressSerialNumber_Write_ind, this, &Connection::A_DomainAddressSerialNumber_Write_ind);
    connect(stack, &Stack::A_SystemNetworkParameter_Read_ind, this, &Connection::A_SystemNetworkParameter_Read_ind);
    connect(stack, &Stack::A_SystemNetworkParameter_Read_Acon, this, &Connection::A_SystemNetworkParameter_Read_Acon);
    connect(stack, &Stack::A_SystemNetworkParameter_Write_ind, this, &Connection::A_SystemNetworkParameter_Write_ind);

    /* Services (Individual) */
    connect(stack, &Stack::A_DeviceDescriptor_Read_ind, this, &Connection::A_DeviceDescriptor_Read_ind);
    connect(stack, &Stack::A_DeviceDescriptor_Read_Acon, this, &Connection::A_DeviceDescriptor_Read_Acon);
    // A_Restart_Lcon
    connect(stack, &Stack::A_Restart_ind, this, &Connection::A_Restart_ind);
    // A_Restart_Acon
    connect(stack, &Stack::A_FileStream_InfoReport_ind, this, &Connection::A_FileStream_InfoReport_ind);
    connect(stack, &Stack::A_PropertyValue_Read_ind, this, &Connection::A_PropertyValue_Read_ind);
    connect(stack, &Stack::A_PropertyValue_Read_Acon, this, &Connection::A_PropertyValue_Read_Acon);
    connect(stack, &Stack::A_PropertyValue_Write_ind, this, &Connection::A_PropertyValue_Write_ind);
    connect(stack, &Stack::A_PropertyDescription_Read_ind, this, &Connection::A_PropertyDescription_Read_ind);
    connect(stack, &Stack::A_PropertyDescription_Read_Acon, this, &Connection::A_PropertyDescription_Read_Acon);
    connect(stack, &Stack::A_FunctionPropertyCommand_ind, this, &Connection::A_FunctionPropertyCommand_ind);
    connect(stack, &Stack::A_FunctionPropertyCommand_Acon, this, &Connection::A_FunctionPropertyCommand_Acon);
    connect(stack, &Stack::A_FunctionPropertyState_Read_ind, this, &Connection::A_FunctionPropertyState_Read_ind);
    connect(stack, &Stack::A_FunctionPropertyState_Read_Acon, this, &Connection::A_FunctionPropertyState_Read_Acon);

    /* Services (Connected) */
    connect(stack, &Stack::A_ADC_Read_ind, this, &Connection::A_ADC_Read_ind);
    connect(stack, &Stack::A_ADC_Read_Acon, this, &Connection::A_ADC_Read_Acon);
    connect(stack, &Stack::A_Memory_Read_ind, this, &Connection::A_Memory_Read_ind);
    connect(stack, &Stack::A_Memory_Read_Acon, this, &Connection::A_Memory_Read_Acon);
    connect(stack, &Stack::A_Memory_Write_ind, this, &Connection::A_Memory_Write_ind);
    connect(stack, &Stack::A_Memory_Write_Acon, this, &Connection::A_Memory_Write_Acon);
    connect(stack, &Stack::A_MemoryBit_Write_ind, this, &Connection::A_MemoryBit_Write_ind);
    connect(stack, &Stack::A_UserMemory_Read_ind, this, &Connection::A_UserMemory_Read_ind);
    connect(stack, &Stack::A_UserMemory_Read_Acon, this, &Connection::A_UserMemory_Read_Acon);
    connect(stack, &Stack::A_UserMemory_Write_ind, this, &Connection::A_UserMemory_Write_ind);
    connect(stack, &Stack::A_UserMemory_Write_Acon, this, &Connection::A_UserMemory_Write_Acon);
    connect(stack, &Stack::A_UserMemoryBit_Write_ind, this, &Connection::A_UserMemoryBit_Write_ind);
    connect(stack, &Stack::A_UserMemoryBit_Write_Acon, this, &Connection::A_UserMemoryBit_Write_Acon);
    connect(stack, &Stack::A_UserManufacturerInfo_Read_ind, this, &Connection::A_UserManufacturerInfo_Read_ind);
    connect(stack, &Stack::A_UserManufacturerInfo_Read_Acon, this, &Connection::A_UserManufacturerInfo_Read_Acon);
    connect(stack, &Stack::A_Authorize_Request_ind, this, &Connection::A_Authorize_Request_ind);
    connect(stack, &Stack::A_Authorize_Request_Acon, this, &Connection::A_Authorize_Request_Acon);
    connect(stack, &Stack::A_Key_Write_ind, this, &Connection::A_Key_Write_ind);
    connect(stack, &Stack::A_Key_Write_Acon, this, &Connection::A_Key_Write_Acon);
}

Stack * Connection::getStack() const
{
    return m_stack;
}

void Connection::setProjectBundle(ProjectBundle * projectBundle)
{
    m_projectBundle = projectBundle;
    Q_ASSERT(m_projectBundle);
    m_etsKnx = m_projectBundle->etsProject20;
}

ProjectBundle * Connection::getProjectBundle() const
{
    return m_projectBundle;
}

void Connection::clearProjectBundle()
{
    delete m_projectBundle;
    m_projectBundle = nullptr;
}

Project::v20::knx::KNX_t * Connection::getEtsKnx() const
{
    return m_etsKnx;
}

void Connection::setEtsProject(Project::v20::knx::Project_t * etsProject)
{
    m_etsProject = etsProject;
    Q_ASSERT(m_etsProject);

    /* project information */
    const Project::v20::knx::Project_ProjectInformation_t * etsProjectInformation = m_etsProject->ProjectInformation;
    Q_ASSERT(etsProjectInformation);
    m_etsGroupAddressStyle = etsProjectInformation->GroupAddressStyle;

    /* installation */
    const Project::v20::knx::Project_Installations_t * etsInstallations = m_etsProject->Installations;
    Q_ASSERT(etsInstallations);
    m_etsInstallation = etsInstallations->Installation[0]; // @todo support multiple installation
    if (etsInstallations->Installation.count() > 1) {
        qWarning() << "File has multiple installations. Using first.";
    }
    Q_ASSERT(m_etsInstallation);
}

Project::v20::knx::Project_t * Connection::getEtsProject() const
{
    return m_etsProject;
}

void Connection::clearEtsProject()
{
    m_etsProject = nullptr;
    m_etsGroupAddressStyle = "ThreeLevel";
    m_etsInstallation = nullptr;
    m_groups.clear();
    m_devices.clear();
}

Project::v20::knx::GroupAddressStyle_t Connection::getEtsGroupAddressStyle() const
{
    return m_etsGroupAddressStyle;
}

Project::v20::knx::Project_Installations_Installation_t * Connection::getEtsInstallation() const
{
    return m_etsInstallation;
}

GroupData * Connection::group(const KNX::Group_Address groupAddress)
{
    if (!m_groups.contains(groupAddress)) {
        GroupData * newGroup = new GroupData(this);
        newGroup->address = groupAddress;
        m_groups[groupAddress] = newGroup;

        if (m_etsInstallation) {
            Project::v20::knx::GroupAddresses_t * etsGroupAddresses = m_etsInstallation->GroupAddresses;
            Q_ASSERT(etsGroupAddresses);
            newGroup->etsGroupAddress = Project::v20::findGroupAddress(etsGroupAddresses, static_cast<uint16_t>(groupAddress));
            if (newGroup->etsGroupAddress) {
                Project::v20::knx::IDREF datapointTypeId = newGroup->etsGroupAddress->DatapointType;
                if (!datapointTypeId.isEmpty()) {
                    newGroup->etsDatapointSubtype = qobject_cast<Project::v20::knx::DatapointType_DatapointSubtypes_DatapointSubtype_t *>(m_etsKnx->ids[datapointTypeId]);
                    Q_ASSERT(newGroup->etsDatapointSubtype);
                    Project::v20::knx::DatapointType_DatapointSubtypes_t * etsDatapointSubtypes = qobject_cast<Project::v20::knx::DatapointType_DatapointSubtypes_t *>(newGroup->etsDatapointSubtype->parent());
                    Q_ASSERT(etsDatapointSubtypes);
                    newGroup->etsDatapointType = qobject_cast<Project::v20::knx::DatapointType_t *>(etsDatapointSubtypes->parent());
                    Q_ASSERT(newGroup->etsDatapointType);
                }
            }
        }
    }

    return m_groups[groupAddress];
}

DeviceData * Connection::device(const KNX::Individual_Address individualAddress_)
{
    if (!m_devices.contains(individualAddress_)) {
        /* create device data */
        DeviceData * newDevice = new DeviceData(this);
        newDevice->address = individualAddress_;
        m_devices[individualAddress_] = newDevice;

        /* get the ETS project links */
        for (const Project::v20::knx::DeviceInstance_t * etsDeviceInstance : getEtsInstallation()->findChildren<Project::v20::knx::DeviceInstance_t *>()) {
            Q_ASSERT(etsDeviceInstance);
            Project::v20::knx::Topology_Area_Line_t * etsLine = etsDeviceInstance->findParent<Project::v20::knx::Topology_Area_Line_t *>();
            if (etsLine) {
                Project::v20::knx::Topology_Area_t * etsArea = etsLine->findParent<Project::v20::knx::Topology_Area_t *>();
                if (etsArea) {
                    uint16_t individual_address =
                        (etsArea->Address.toUShort() << 12) |
                        (etsLine->Address.toUShort() << 8) |
                        (etsDeviceInstance->Address.toUShort());
                    if (individual_address == individualAddress_) {
                        newDevice->ets_DeviceInstance = etsDeviceInstance;
                        newDevice->ets_Product = newDevice->ets_DeviceInstance->getProduct();
                        newDevice->ets_Hardware2Program = newDevice->ets_DeviceInstance->getHardware2Program();
                        switch (newDevice->ets_Hardware2Program->ApplicationProgramRef.count()) {
                        case 0:
                            qWarning() << "Hardware2Program " << newDevice->ets_Hardware2Program->Id << " has no referenced ApplicationProgram";
                            break;
                        case 1: {
                            Project::v20::knx::ApplicationProgramRef_t * etsApplicationProgramRef = newDevice->ets_Hardware2Program->ApplicationProgramRef.first();
                            newDevice->ets_ApplicationProgram = etsApplicationProgramRef->getApplicationProgram();
                        }
                        break;
                        default:
                            qWarning() << "Hardware2Program " << newDevice->ets_Hardware2Program->Id << " has multiple referenced ApplicationPrograms";
                            break;
                        }
                    }
                }
            }
        }

        /* load default memory content */
        if (newDevice->ets_ApplicationProgram) {
            const Project::v20::knx::ApplicationProgramStatic_t * etsApplicationProgramStatic = newDevice->ets_ApplicationProgram->Static;
            Q_ASSERT(etsApplicationProgramStatic);
            const Project::v20::knx::ApplicationProgramStatic_Code_t * etsCode = etsApplicationProgramStatic->Code;
            for (const Project::v20::knx::ApplicationProgramStatic_Code_AbsoluteSegment_t * etsAbsoluteSegment : etsCode->AbsoluteSegment) {
                Q_ASSERT(etsAbsoluteSegment);
                uint16_t address = etsAbsoluteSegment->Address.toUShort();
                uint16_t size = etsAbsoluteSegment->Size.toUShort();

                /* init data */
                if (etsAbsoluteSegment->Data) {
                    newDevice->memory.replace(address, size, QByteArray::fromBase64(etsAbsoluteSegment->Data->text.toUtf8()));
                    for (auto a = address; a < address + size; a++) {
                        newDevice->memory_mask[a] = newDevice->memory_mask[a] | 0x01; // synced with ETS project
                    }
                }
            }
        }
    }

    return m_devices[individualAddress_];
}

void Connection::A_Connect_ind(const KNX::ASAP_Connected asap)
{
    DeviceData * deviceData = device(asap.address);
    Q_ASSERT(deviceData);

    deviceData->transport_layer_connected = true;
    Q_EMIT deviceData->transport_layer_connection_state_changed();
}

void Connection::A_Disconnect_ind(const KNX::ASAP_Connected asap)
{
    DeviceData * deviceData = device(asap.address);
    Q_ASSERT(deviceData);

    deviceData->transport_layer_connected = false;
    deviceData->property_discovery_state = DeviceData::Property_Discovery_State::Stopped;
    deviceData->memory_read_request.request = false;
    Q_EMIT deviceData->transport_layer_connection_state_changed();
}

/* Services (Group) */

void Connection::A_GroupValue_Read_ind(const KNX::ASAP_Group /*asap*/, const KNX::Priority /*priority*/, const KNX::Hop_Count_Type /*hop_count_type*/)
{
}

void Connection::A_GroupValue_Read_Acon(const KNX::ASAP_Group asap, const KNX::Priority /*priority*/, const KNX::Hop_Count_Type /*hop_count_type*/, const std::vector<uint8_t> data)
{
    GroupData * groupData = group(asap);
    Q_ASSERT(groupData);

    groupData->objectValues[QDateTime::currentDateTime()] = data;
    Q_EMIT groupDataChanged(asap);
}

void Connection::A_GroupValue_Write_ind(const KNX::ASAP_Group asap, const KNX::Priority /*priority*/, const KNX::Hop_Count_Type /*hop_count_type*/, const std::vector<uint8_t> data)
{
    GroupData * groupData = group(asap);
    Q_ASSERT(groupData);

    groupData->objectValues[QDateTime::currentDateTime()] = data;
    Q_EMIT groupDataChanged(asap);
}

/* Services (Broadcast) */

void Connection::A_IndividualAddress_Write_ind(const KNX::Priority /*priority*/, const KNX::Hop_Count_Type /*hop_count_type*/, const KNX::Individual_Address /*newaddress*/)
{

}

void Connection::A_IndividualAddress_Read_ind(const KNX::Priority /*priority*/, const KNX::Hop_Count_Type /*hop_count_type*/)
{

}

void Connection::A_IndividualAddress_Read_Acon(const KNX::Priority /*priority*/, const KNX::Hop_Count_Type /*hop_count_type*/, const KNX::Individual_Address /*individual_address*/)
{

}

void Connection::A_IndividualAddressSerialNumber_Read_ind(const KNX::Priority /*priority*/, const KNX::Hop_Count_Type /*hop_count_type*/, const KNX::Serial_Number /*serial_number*/)
{

}

void Connection::A_IndividualAddressSerialNumber_Read_Acon(const KNX::Priority /*priority*/, const KNX::Hop_Count_Type /*hop_count_type*/, const KNX::Serial_Number /*serial_number*/, const KNX::Individual_Address /*individual_address*/, const KNX::Domain_Address_2 /*domain_address*/)
{

}

void Connection::A_IndividualAddressSerialNumber_Write_ind(const KNX::Priority /*priority*/, const KNX::Hop_Count_Type /*hop_count_type*/, const KNX::Serial_Number /*serial_number*/, const KNX::Individual_Address /*newaddress*/)
{

}

void Connection::A_NetworkParameter_Read_ind(const KNX::ASAP_Individual asap, const KNX::Comm_Mode /*comm_mode_req*/, const KNX::Hop_Count_Type /*hop_count_type*/, const KNX::Parameter_Type /*parameter_type*/, const KNX::Priority /*priority*/, const std::vector<uint8_t> /*test_info*/)
{
    DeviceData * deviceData = device(asap.address);
    Q_ASSERT(deviceData);

}

void Connection::A_NetworkParameter_Read_Acon(const KNX::ASAP_Individual asap, const KNX::Hop_Count_Type /*hop_count_type*/, const KNX::Individual_Address /*individual_address*/, const KNX::Parameter_Type /*parameter_type*/, const KNX::Priority /*priority*/, const KNX::Parameter_Test_Info_Result /*test_info_result*/)
{
    DeviceData * deviceData = device(asap.address);
    Q_ASSERT(deviceData);

}

void Connection::A_NetworkParameter_Write_ind(const KNX::ASAP_Individual asap, const KNX::Parameter_Type /*parameter_type*/, const KNX::Priority /*priority*/, const std::vector<uint8_t> /*value*/)
{
    DeviceData * deviceData = device(asap.address);
    Q_ASSERT(deviceData);

}

void Connection::A_NetworkParameter_InfoReport_ind(const KNX::ASAP_Individual asap, const KNX::Comm_Mode /*comm_mode_req*/, const KNX::Hop_Count_Type /*hop_count_type*/, const KNX::Parameter_Type /*parameter_type*/, const KNX::Priority /*priority*/, const KNX::Parameter_Test_Info_Result /*test_info_result*/)
{
    DeviceData * deviceData = device(asap.address);
    Q_ASSERT(deviceData);

}

/* Services (System Broadcast) */

void Connection::A_DeviceDescriptor_InfoReport_ind(const KNX::Descriptor_Type /*descriptor_type*/, const std::vector<uint8_t> /*device_descriptor*/, const KNX::Hop_Count_Type /*hop_count_type*/, const KNX::Priority /*priority*/)
{

}

void Connection::A_DomainAddress_Write_ind(const KNX::Priority /*priority*/, const KNX::Hop_Count_Type /*hop_count_type*/, const KNX::Domain_Address /*domain_address_new*/)
{

}

void Connection::A_DomainAddress_Read_ind(const KNX::Priority /*priority*/, const KNX::Hop_Count_Type /*hop_count_type*/)
{

}

void Connection::A_DomainAddress_Read_Acon(const KNX::Priority /*priority*/, const KNX::Hop_Count_Type /*hop_count_type*/, const KNX::ASAP_Individual /*asap*/, const KNX::Domain_Address /*domain_address*/)
{

}

void Connection::A_DomainAddressSelective_Read_ind(const KNX::Priority /*priority*/, const KNX::Hop_Count_Type /*hop_count_type*/, const KNX::ASDU /*asdu*/)
{

}

void Connection::A_DomainAddressSerialNumber_Read_ind(const KNX::Hop_Count_Type /*hop_count_type*/, const KNX::Priority /*priority*/, const KNX::Serial_Number /*serial_number*/)
{

}

void Connection::A_DomainAddressSerialNumber_Read_Acon(const KNX::Domain_Address /*domain_address*/, const KNX::Hop_Count_Type /*hop_count_type*/, const KNX::Priority /*priority*/, const KNX::Serial_Number /*serial_number*/)
{

}

void Connection::A_DomainAddressSerialNumber_Write_ind(const KNX::Domain_Address /*domain_address*/, const KNX::Hop_Count_Type /*hop_count_type*/, const KNX::Priority /*priority*/, const KNX::Serial_Number /*serial_number*/)
{

}

void Connection::A_SystemNetworkParameter_Read_ind(const KNX::Hop_Count_Type /*hop_count_type*/, const KNX::System_Parameter_Type /*parameter_type*/, const KNX::Priority /*priority*/, const std::vector<uint8_t> /*test_info*/)
{

}

void Connection::A_SystemNetworkParameter_Read_Acon(const KNX::ASAP_Individual asap, const KNX::Hop_Count_Type /*hop_count_type*/, const KNX::System_Parameter_Type /*parameter_type*/, const KNX::Priority /*priority*/, const KNX::Parameter_Test_Info_Result /*test_info_result*/)
{
    DeviceData * deviceData = device(asap.address);
    Q_ASSERT(deviceData);

}

void Connection::A_SystemNetworkParameter_Write_ind(const KNX::Hop_Count_Type /*hop_count_type*/, const KNX::System_Parameter_Type /*parameter_type*/, const KNX::Priority /*priority*/, const std::vector<uint8_t> /*value*/)
{

}

/* Services (Individual) */

void Connection::A_DeviceDescriptor_Read_ind(const KNX::Priority /*priority*/, const KNX::Hop_Count_Type /*hop_count_type*/, const KNX::ASAP_Individual /*asap*/, const KNX::Descriptor_Type /*descriptor_type*/)
{
}

void Connection::A_DeviceDescriptor_Read_Acon(const KNX::Priority /*priority*/, const KNX::Hop_Count_Type /*hop_count_type*/, const KNX::ASAP_Individual asap, const KNX::Descriptor_Type descriptor_type, const std::vector<uint8_t> device_descriptor)
{
    DeviceData * deviceData = device(asap.address);
    Q_ASSERT(deviceData);

    if (descriptor_type != 0x3F) {
        if (!deviceData->device_descriptors.contains(descriptor_type) || (deviceData->device_descriptors[descriptor_type] != device_descriptor)) {
            deviceData->device_descriptors[descriptor_type] = device_descriptor;
            Q_EMIT deviceData->device_descriptor_changed(descriptor_type, device_descriptor);
        }
    }
}

void Connection::A_Restart_ind(const KNX::Restart_Erase_Code /*erase_code*/, const uint8_t /*channel_number*/, const KNX::Priority /*priority*/, const KNX::Hop_Count_Type /*hop_count_type*/, const KNX::ASAP_Individual /*asap*/)
{
}

void Connection::A_FileStream_InfoReport_ind(const KNX::ASAP_Individual /*asap*/, const KNX::File_Block /*file_block*/, const KNX::File_Block_Sequence_Number /*file_block_sequence_number*/, const KNX::File_Handle /*file_handle*/, const KNX::Hop_Count_Type /*hop_count_type*/)
{
}

void Connection::A_PropertyValue_Read_ind(const KNX::Priority /*priority*/, const KNX::Hop_Count_Type /*hop_count_type*/, const KNX::ASAP_Individual /*asap*/, const KNX::Object_Index /*object_index*/, const KNX::Property_Id /*property_id*/, const KNX::PropertyValue_Nr_Of_Elem /*nr_of_elem*/, const KNX::PropertyValue_Start_Index /*start_index*/)
{
}

void Connection::A_PropertyValue_Read_Acon(const KNX::Priority /*priority*/, const KNX::Hop_Count_Type /*hop_count_type*/, const KNX::ASAP_Individual asap, const KNX::Object_Index object_index, const KNX::Property_Id property_id, const KNX::PropertyValue_Nr_Of_Elem nr_of_elem, const KNX::PropertyValue_Start_Index start_index, const KNX::Property_Value data)
{
    DeviceData * deviceData = device(asap.address);
    Q_ASSERT(deviceData);

    if (data.size() > 0) {
        /* interface object */
        std::shared_ptr<KNX::Interface_Object> interface_object;
        if (!deviceData->interface_objects.objects.count(object_index)) {
            interface_object = std::make_shared<KNX::Unknown_Interface_Object>(0);
            deviceData->interface_objects.objects[object_index] = interface_object;
        } else {
            interface_object = deviceData->interface_objects.objects[object_index];
        }
        Q_ASSERT(interface_object);

        /* replace Unknown Interface Object with known one */
        if (std::dynamic_pointer_cast<KNX::Unknown_Interface_Object>(interface_object)) {
            if (property_id == KNX::Interface_Object::PID_OBJECT_TYPE) {
                const KNX::Object_Type object_type = (data[0] << 8) | (data[1]);
                std::shared_ptr<KNX::Interface_Object> new_interface_object = deviceData->interface_objects.make_object(object_type);
                new_interface_object->properties = interface_object->properties;
                deviceData->interface_objects.objects[object_index] = new_interface_object;
                interface_object = new_interface_object;
            }
        }
        Q_ASSERT(interface_object);

        /* Property */
        std::shared_ptr<KNX::Property> property = interface_object->property_by_id(property_id);
        Q_ASSERT(property);
        std::shared_ptr<KNX::Data_Property> data_property = std::dynamic_pointer_cast<KNX::Data_Property>(property);
        Q_ASSERT(data_property);
        const KNX::Property_Index property_index = interface_object->property_index(property);

        /* save the received values */
        const uint8_t property_datatype_size = size(property->description.property_datatype);
        std::vector<uint8_t> new_data = data_property->toData();
        const size_t min_size = (start_index - 1) * property_datatype_size + data.size();
        if (new_data.size() < min_size) {
            new_data.resize(min_size);
        }
        std::copy(std::cbegin(data), std::cend(data), std::begin(new_data) + (start_index - 1) * property_datatype_size);
        data_property->fromData(std::cbegin(new_data), std::cend(new_data));
        if (new_data != data_property->toData()) {
            qWarning() << "write_PropertyValue: value != data";
        }
        Q_EMIT deviceData->property_updated(object_index, property_index);
    }

    switch (deviceData->property_discovery_state) {
    case DeviceData::Property_Discovery_State::Stopped:
        break;
    case DeviceData::Property_Discovery_State::Scan_Objects:
        if (property_id == KNX::Interface_Object::PID_OBJECT_TYPE) {
            // scan next object
            const KNX::Object_Index new_object_index = object_index + 1;
            const KNX::Property_Index new_property_index = 0;
            getStack()->A_PropertyDescription_Read_req(KNX::Ack_Request::dont_care, KNX::Priority::low, KNX::Network_Layer_Parameter, asap, new_object_index, property_id, new_property_index, [](const KNX::Status /*a_status*/){
                // Lcon
            });
        }
        break;
    case DeviceData::Property_Discovery_State::Scan_Properties: {
        bool read_more_data{false};
        if (data.size() > 0) {
            std::shared_ptr<KNX::Interface_Object> interface_object = deviceData->interface_objects.objects[object_index];
            Q_ASSERT(interface_object);
            std::shared_ptr<KNX::Property> property = interface_object->property_by_id(property_id);
            Q_ASSERT(property);
            if ((start_index - 1) + nr_of_elem < property->description.max_nr_of_elem) {
                // more data to retrieve on same property
                const KNX::PropertyValue_Start_Index new_start_index = start_index + nr_of_elem;
                const uint8_t property_datatype_size = size(property->description.property_datatype);
                uint16_t new_nr_of_elem = property->description.max_nr_of_elem - (new_start_index - 1);
                // TP1: octets 12..21 contain data, 10 total.
                if ((new_nr_of_elem * property_datatype_size) > 10) {
                    new_nr_of_elem = 10 / property_datatype_size;
                }
                getStack()->A_PropertyValue_Read_req(KNX::Ack_Request::dont_care, KNX::Priority::low, KNX::Network_Layer_Parameter, asap, object_index, property_id, new_nr_of_elem, new_start_index, [](const KNX::Status /*a_status*/){
                    // Lcon
                });
                read_more_data = true;
            }
        }

        // scan next property
        if (!read_more_data) {
            std::shared_ptr<KNX::Interface_Object> interface_object = deviceData->interface_objects.objects[object_index];
            Q_ASSERT(interface_object);
            const KNX::Property_Id new_property_id = 0; // 0 to use property_index
            const KNX::Property_Index new_property_index = interface_object->properties.size(); // next property
            getStack()->A_PropertyDescription_Read_req(KNX::Ack_Request::dont_care, KNX::Priority::low, KNX::Network_Layer_Parameter, asap, object_index, new_property_id, new_property_index, [](const KNX::Status /*a_status*/){
                // Lcon
            });
        }
    }
    break;
    }
}

void Connection::A_PropertyValue_Write_ind(const KNX::Priority /*priority*/, const KNX::Hop_Count_Type /*hop_count_type*/, const KNX::ASAP_Individual /*asap*/, const KNX::Object_Index /*object_index*/, const KNX::Property_Id /*property_id*/, const KNX::PropertyValue_Nr_Of_Elem /*nr_of_elem*/, const KNX::PropertyValue_Start_Index /*start_index*/, const KNX::Property_Value /*data*/)
{
}

void Connection::A_PropertyDescription_Read_ind(const KNX::Priority /*priority*/, const KNX::Hop_Count_Type /*hop_count_type*/, const KNX::ASAP_Individual /*asap*/, const KNX::Object_Index /*object_index*/, const KNX::Property_Id /*property_id*/, const KNX::Property_Index /*property_index*/)
{
}

void Connection::A_PropertyDescription_Read_Acon(const KNX::Priority /*priority*/, const KNX::Hop_Count_Type /*hop_count_type*/, const KNX::ASAP_Individual asap, const KNX::Object_Index object_index, const KNX::Property_Id property_id, const KNX::Property_Index property_index, const bool write_enable, const KNX::Property_Type type, const KNX::Max_Nr_Of_Elem max_nr_of_elem, const KNX::Access access)
{
    DeviceData * deviceData = device(asap.address);
    Q_ASSERT(deviceData);

    if (max_nr_of_elem > 0) {
        /* interface object */
        std::shared_ptr<KNX::Interface_Object> interface_object;
        if (!deviceData->interface_objects.objects.count(object_index)) {
            interface_object = std::make_shared<KNX::Unknown_Interface_Object>(0);
            deviceData->interface_objects.objects[object_index] = interface_object;
        } else {
            interface_object = deviceData->interface_objects.objects[object_index];
        }
        Q_ASSERT(interface_object);

        /* property */
        std::shared_ptr<KNX::Property> property;
        if (!interface_object->properties.count(property_index)) {
            property = interface_object->make_property(property_id);
            interface_object->properties[property_index] = property;
        } else {
            property = interface_object->properties[property_index];
        }
        Q_ASSERT(property);

        /* write description */
        property->description.write_enable = write_enable;
        property->description.property_datatype = type;
        property->description.max_nr_of_elem = max_nr_of_elem;
        property->description.access = access;
        Q_EMIT deviceData->property_updated(object_index, property_index);
    }

    switch (deviceData->property_discovery_state) {
    case DeviceData::Property_Discovery_State::Stopped:
        break;
    case DeviceData::Property_Discovery_State::Scan_Objects:
        if (max_nr_of_elem > 0) {
            /* object exists. Get ObjectType */
            const KNX::PropertyValue_Nr_Of_Elem new_nr_of_elem = 1;
            const KNX::PropertyValue_Start_Index new_start_index = 1; // 0 is size, 1 is data
            getStack()->A_PropertyValue_Read_req(KNX::Ack_Request::dont_care, KNX::Priority::low, KNX::Network_Layer_Parameter, asap, object_index, property_id, new_nr_of_elem, new_start_index, [](const KNX::Status /*a_status*/){
                // Lcon
            });
        } else {
            /* object does not exist. Start property scan. */
            deviceData->property_discovery_state = DeviceData::Property_Discovery_State::Scan_Properties;

            const KNX::Object_Index new_object_index = 0; // first object
            const KNX::Property_Id new_property_id = 0; // use property index
            const KNX::Property_Index new_property_index = 1; // second property
            getStack()->A_PropertyDescription_Read_req(KNX::Ack_Request::dont_care, KNX::Priority::low, KNX::Network_Layer_Parameter, asap, new_object_index, new_property_id, new_property_index, [](const KNX::Status /*a_status*/){
                // Lcon
            });
        }
        break;
    case DeviceData::Property_Discovery_State::Scan_Properties:
        if (max_nr_of_elem > 0) {
            // get property
            std::shared_ptr<KNX::Interface_Object> interface_object = deviceData->interface_objects.objects[object_index];
            Q_ASSERT(interface_object);
            std::shared_ptr<KNX::Property> property = interface_object->properties[property_index];
            Q_ASSERT(property);
            const uint8_t property_datatype_size = size(property->description.property_datatype);
            uint16_t nr_of_elem = max_nr_of_elem;
            // TP1: octets 12..21 contain data, 10 total.
            if ((nr_of_elem * property_datatype_size) > 10) {
                nr_of_elem = 10 / property_datatype_size;
            }
            const KNX::PropertyValue_Start_Index new_start_index = 1; // 0 is size, 1 is data
            getStack()->A_PropertyValue_Read_req(KNX::Ack_Request::dont_care, KNX::Priority::low, KNX::Network_Layer_Parameter, asap, object_index, property_id, nr_of_elem, new_start_index, [](const KNX::Status /*a_status*/){
                // Lcon
            });
        } else {
            // property invalid.
            if (object_index < deviceData->interface_objects.objects.size()) {
                // scan next object.
                const KNX::Object_Index new_object_index = object_index + 1;
                const KNX::Property_Id new_property_id = 0; // 0 to use property_index
                const KNX::Property_Index new_property_index = 1; // second property
                getStack()->A_PropertyDescription_Read_req(KNX::Ack_Request::dont_care, KNX::Priority::low, KNX::Network_Layer_Parameter, asap, new_object_index, new_property_id, new_property_index, [](const KNX::Status /*a_status*/){
                    // Lcon
                });
            } else {
                // last object scanned.
                deviceData->property_discovery_state = DeviceData::Property_Discovery_State::Stopped;
            }
        }
        break;
    }
}

void Connection::A_FunctionPropertyCommand_ind(const KNX::ASAP_Individual asap, const KNX::Comm_Mode /*comm_mode*/, const std::vector<uint8_t> /*data*/, const KNX::Hop_Count_Type /*hop_count_type*/, const KNX::Object_Index object_index, const KNX::Priority /*priority*/, const KNX::Property_Id property_id)
{
    DeviceData * deviceData = device(asap.address);
    Q_ASSERT(deviceData);

    std::shared_ptr<KNX::Interface_Object> interface_object = deviceData->interface_objects.object_by_type(object_index);
    Q_ASSERT(interface_object);

    std::shared_ptr<KNX::Property> property = interface_object->property_by_id(property_id);
    Q_ASSERT(property);

    const KNX::Property_Index property_index = interface_object->property_index(property);
    property->description.property_id = property_id;
    // @todo data
    Q_EMIT deviceData->property_updated(object_index, property_index);
}

void Connection::A_FunctionPropertyCommand_Acon(const KNX::ASAP_Individual asap, const KNX::Comm_Mode /*comm_mode*/, const std::vector<uint8_t> /*data*/, const KNX::Hop_Count_Type /*hop_count_type*/, const KNX::Object_Index object_index, const KNX::Priority /*priority*/, const KNX::Property_Id property_id, const std::optional<KNX::Property_Return_Code> /*return_code*/)
{
    DeviceData * deviceData = device(asap.address);
    Q_ASSERT(deviceData);

    std::shared_ptr<KNX::Interface_Object> interface_object = deviceData->interface_objects.object_by_type(object_index);
    Q_ASSERT(interface_object);

    std::shared_ptr<KNX::Property> property = interface_object->property_by_id(property_id);
    Q_ASSERT(property);

    const KNX::Property_Index property_index = interface_object->property_index(property);
    property->description.property_id = property_id;
    // @todo data
    // @todo return_code
    Q_EMIT deviceData->property_updated(object_index, property_index);
}

void Connection::A_FunctionPropertyState_Read_ind(const KNX::ASAP_Individual asap, const KNX::Comm_Mode /*comm_mode*/, const std::vector<uint8_t> /*data*/, const KNX::Hop_Count_Type /*hop_count_type*/, const KNX::Object_Index object_index, const KNX::Priority /*priority*/, const KNX::Property_Id property_id)
{
    DeviceData * deviceData = device(asap.address);
    Q_ASSERT(deviceData);

    std::shared_ptr<KNX::Interface_Object> interface_object = deviceData->interface_objects.object_by_type(object_index);
    Q_ASSERT(interface_object);

    std::shared_ptr<KNX::Property> property = interface_object->property_by_id(property_id);
    Q_ASSERT(property);

    const KNX::Property_Index property_index = interface_object->property_index(property);
    property->description.property_id = property_id;
    // @todo data
    Q_EMIT deviceData->property_updated(object_index, property_index);
}

void Connection::A_FunctionPropertyState_Read_Acon(const KNX::ASAP_Individual asap, const KNX::Comm_Mode /*comm_mode*/, const std::vector<uint8_t> /*data*/, const KNX::Hop_Count_Type /*hop_count_type*/, const KNX::Object_Index object_index, const KNX::Priority /*priority*/, const KNX::Property_Id property_id, const std::optional<KNX::Property_Return_Code> /*return_code*/)
{
    DeviceData * deviceData = device(asap.address);
    Q_ASSERT(deviceData);

    std::shared_ptr<KNX::Interface_Object> interface_object = deviceData->interface_objects.object_by_type(object_index);
    Q_ASSERT(interface_object);

    std::shared_ptr<KNX::Property> property = interface_object->property_by_id(property_id);
    Q_ASSERT(property);

    const KNX::Property_Index property_index = interface_object->property_index(property);
    property->description.property_id = property_id;
    // @todo data
    // @todo return_code
    Q_EMIT deviceData->property_updated(object_index, property_index);
}

/* Services (Connected) */

void Connection::A_ADC_Read_ind(const KNX::Priority /*priority*/, const KNX::Hop_Count_Type /*hop_count_type*/, const KNX::ASAP_Connected /*asap*/, const KNX::ADC_Channel_Nr /*channel_nr*/, const uint8_t /*read_count*/)
{
}

void Connection::A_ADC_Read_Acon(const KNX::Priority /*priority*/, const KNX::Hop_Count_Type /*hop_count_type*/, const KNX::ASAP_Connected asap, const KNX::ADC_Channel_Nr channel_nr, const uint8_t read_count, const uint16_t sum)
{
    DeviceData * deviceData = device(asap.address);
    Q_ASSERT(deviceData);

    if (!deviceData->adc_data.contains(channel_nr) ||
            (deviceData->adc_data[channel_nr].read_count != read_count) ||
            (deviceData->adc_data[channel_nr].sum != sum)) {
        deviceData->adc_data[channel_nr] = { read_count, sum };
        Q_EMIT deviceData->adc_changed(channel_nr, read_count, sum);
    }
}

void Connection::A_Memory_Read_ind(const KNX::Priority /*priority*/, const KNX::Hop_Count_Type /*hop_count_type*/, const KNX::ASAP_Connected /*asap*/, const KNX::Memory_Number /*number*/, const KNX::Memory_Address /*memory_address*/)
{
}

void Connection::A_Memory_Read_Acon(const KNX::Priority /*priority*/, const KNX::Hop_Count_Type /*hop_count_type*/, const KNX::ASAP_Connected /*asap*/, const KNX::Memory_Number /*number*/, const KNX::Memory_Address /*memory_address*/, const std::vector<uint8_t> /*data*/)
{
}

void Connection::A_Memory_Write_ind(const KNX::Priority /*priority*/, const KNX::Hop_Count_Type /*hop_count_type*/, const KNX::ASAP_Connected /*asap*/, const KNX::Memory_Number /*number*/, const KNX::Memory_Address /*memory_address*/, const std::vector<uint8_t> /*data*/)
{
}

void Connection::A_Memory_Write_Acon(const KNX::Priority /*priority*/, const KNX::Hop_Count_Type /*hop_count_type*/, const KNX::ASAP_Connected asap, const KNX::Memory_Number number, const KNX::Memory_Address memory_address, const std::vector<uint8_t> data)
{
    DeviceData * deviceData = device(asap.address);
    Q_ASSERT(deviceData);

    /* add/update data */
    deviceData->memory.replace(memory_address, number, QByteArray(reinterpret_cast<const char *>(data.data()), data.size()));
    for (auto a = memory_address; a < memory_address + number; a++) {
        deviceData->memory_mask[a] = deviceData->memory_mask[a] | 0x02; // synced with KNX device
    }

    Q_EMIT deviceData->memory_changed(memory_address);

    /* read next address */
    if (deviceData->memory_read_request.request) {
        const KNX::Memory_Address end_memory_address = deviceData->memory_read_request.address + deviceData->memory_read_request.size;
        const KNX::Memory_Address next_memory_address = memory_address + number;
        if (next_memory_address < end_memory_address) {
            KNX::Memory_Number next_number = 8;
            if (next_memory_address + next_number >= end_memory_address) {
                next_number = end_memory_address - next_memory_address;
            }
            getStack()->A_Memory_Read_req(KNX::Ack_Request::dont_care, KNX::Priority::low, KNX::Network_Layer_Parameter, asap, next_number, next_memory_address, [](const KNX::Status /*a_status*/){
                // Lcon
            });
        } else {
            // all read
            deviceData->memory_read_request.request = false;
        }
    }
}

void Connection::A_MemoryBit_Write_ind(const KNX::Priority /*priority*/, const KNX::Hop_Count_Type /*hop_count_type*/, const KNX::ASAP_Connected /*asap*/, const KNX::MemoryBit_Number /*number*/, const KNX::MemoryBit_Address /*memory_address*/, const std::vector<uint8_t> /*and_data*/, const std::vector<uint8_t> /*xor_data*/)
{
}

void Connection::A_UserMemory_Read_ind(const KNX::Priority /*priority*/, const KNX::Hop_Count_Type /*hop_count_type*/, const KNX::ASAP_Connected /*asap*/, const KNX::UserMemory_Number /*number*/, const KNX::UserMemory_Address /*memory_address*/)
{
}

void Connection::A_UserMemory_Read_Acon(const KNX::Priority /*priority*/, const KNX::Hop_Count_Type /*hop_count_type*/, const KNX::ASAP_Connected /*asap*/, const KNX::UserMemory_Number /*number*/, const KNX::UserMemory_Address /*memory_address*/, const std::vector<uint8_t> /*data*/)
{
}

void Connection::A_UserMemory_Write_ind(const KNX::Priority /*priority*/, const KNX::Hop_Count_Type /*hop_count_type*/, const KNX::ASAP_Connected /*asap*/, const KNX::UserMemory_Number /*number*/, const KNX::UserMemory_Address /*memory_address*/, const std::vector<uint8_t> /*data*/)
{
}

void Connection::A_UserMemory_Write_Acon(const KNX::Priority /*priority*/, const KNX::Hop_Count_Type /*hop_count_type*/, const KNX::ASAP_Connected asap, const KNX::UserMemory_Number number, const KNX::UserMemory_Address memory_address, const std::vector<uint8_t> data)
{
    DeviceData * deviceData = device(asap.address);
    Q_ASSERT(deviceData);

    /* add/update data */
    deviceData->user_memory.replace(memory_address, number, QByteArray(reinterpret_cast<const char *>(data.data()), data.size()));
    for (auto a = memory_address; a < memory_address + number; a++) {
        deviceData->user_memory_mask[a] = deviceData->user_memory_mask[a] | 0x02; // synced with KNX device
    }

    Q_EMIT deviceData->user_memory_changed(memory_address);

    /* read next address */
    if (deviceData->user_memory_read_request.request) {
        const KNX::UserMemory_Address end_memory_address = deviceData->user_memory_read_request.address + deviceData->user_memory_read_request.size;
        const KNX::UserMemory_Address next_memory_address = memory_address + number;
        if (next_memory_address < end_memory_address) {
            KNX::UserMemory_Number next_number = 8;
            if (next_memory_address + next_number >= end_memory_address) {
                next_number = end_memory_address - next_memory_address;
            }
            getStack()->A_UserMemory_Read_req(KNX::Ack_Request::dont_care, KNX::Priority::low, KNX::Network_Layer_Parameter, asap, next_number, next_memory_address, [](const KNX::Status /*a_status*/){
                // Lcon
            });
        } else {
            // all read
            deviceData->user_memory_read_request.request = false;
        }
    }
}

void Connection::A_UserMemoryBit_Write_ind(const KNX::Priority /*priority*/, const KNX::Hop_Count_Type /*hop_count_type*/, const KNX::ASAP_Connected /*asap*/, const KNX::UserMemoryBit_Number /*number*/, const KNX::UserMemoryBit_Address /*memory_address*/, const std::vector<uint8_t> /*and_data*/, const std::vector<uint8_t> /*xor_data*/)
{
}

void Connection::A_UserMemoryBit_Write_Acon(const KNX::Priority /*priority*/, const KNX::Hop_Count_Type /*hop_count_type*/, const KNX::ASAP_Connected /*asap*/, const KNX::UserMemoryBit_Number /*number*/, const KNX::UserMemoryBit_Address /*memory_address*/, const std::vector<uint8_t> /*data*/)
{
}

void Connection::A_UserManufacturerInfo_Read_ind(const KNX::Priority /*priority*/, const KNX::Hop_Count_Type /*hop_count_type*/, const KNX::ASAP_Connected /*asap*/)
{
}

void Connection::A_UserManufacturerInfo_Read_Acon(const KNX::Priority /*priority*/, const KNX::Hop_Count_Type /*hop_count_type*/, const KNX::ASAP_Connected /*asap*/, const KNX::MFact_Info /*mfact_info*/)
{
}

void Connection::A_Authorize_Request_ind(const KNX::Priority /*priority*/, const KNX::Hop_Count_Type /*hop_count_type*/, const KNX::ASAP_Connected /*asap*/, const KNX::Key /*key*/)
{
}

void Connection::A_Authorize_Request_Acon(const KNX::Priority /*priority*/, const KNX::Hop_Count_Type /*hop_count_type*/, const KNX::ASAP_Connected asap, const KNX::Level level)
{
    DeviceData * deviceData = device(asap.address);
    Q_ASSERT(deviceData);

    if (deviceData->authorization_level != level) {
        deviceData->authorization_level = level;
        Q_EMIT deviceData->authorization_level_changed(level);
    }
}

void Connection::A_Key_Write_ind(const KNX::Priority /*priority*/, const KNX::Hop_Count_Type /*hop_count_type*/, const KNX::ASAP_Connected /*asap*/, const KNX::Level /*level*/, const KNX::Key /*key*/)
{
}

void Connection::A_Key_Write_Acon(const KNX::Priority /*priority*/, const KNX::Hop_Count_Type /*hop_count_type*/, const KNX::ASAP_Connected /*asap*/, const KNX::Level /*level*/)
{
}
