#include "DeviceData.h"

#include <QDebug>

#include <KNX/03/07/03/Property_Datatype_ID.h>

#include <Connection/Connection.h>

DeviceData::DeviceData(QObject * parent) :
    QObject(parent),
    memory(0x10000, 0),
    memory_mask(0x10000, 0),
    user_memory(0x100000, 0),
    user_memory_mask(0x100000, 0)
{
}

DeviceData::~DeviceData()
{
}

Stack * DeviceData::getStack() const
{
    Connection * connection = qobject_cast<Connection *>(parent());
    Q_ASSERT(connection);

    return connection->getStack();
}

void DeviceData::transport_layer_connect()
{
    if (!transport_layer_connected) {
        getStack()->A_Connect_req({address, false}, KNX::Priority::low, [this](const KNX::Status a_status){
            if (a_status == KNX::Status::ok) {
                transport_layer_connected = true;
                Q_EMIT transport_layer_connection_state_changed();
            }
        });
    }
}

void DeviceData::transport_layer_disconnect()
{
    if (transport_layer_connected) {
        getStack()->A_Disconnect_req(KNX::Priority::low, {address, false}, [this](const KNX::Status a_status){
            if (a_status == KNX::Status::ok) {
                property_discovery_state = DeviceData::Property_Discovery_State::Stopped;
                memory_read_request.request = false;
                transport_layer_connected = false;
                Q_EMIT transport_layer_connection_state_changed();
            }
        });
    }
}

void DeviceData::read_device_descriptor(const KNX::Descriptor_Type descriptor_type)
{
    getStack()->A_DeviceDescriptor_Read_req(KNX::Ack_Request::dont_care, KNX::Priority::low, KNX::Network_Layer_Parameter, {address, false}, descriptor_type, [](const KNX::Status /*a_status*/){
        // Lcon
    });
}

void DeviceData::start_property_discovery()
{
    property_discovery_state = Property_Discovery_State::Scan_Objects;

    /* request first property description */
    const KNX::Object_Index object_index = 0; // first object
    const KNX::Property_Id property_id = KNX::Interface_Object::PID_OBJECT_TYPE;
    const KNX::Property_Index property_index = 0;
    getStack()->A_PropertyDescription_Read_req(KNX::Ack_Request::dont_care, KNX::Priority::low, KNX::Network_Layer_Parameter, {address, false}, object_index, property_id, property_index, [](const KNX::Status /*a_status*/){
        // Lcon
    });
}

void DeviceData::read_data_property(const KNX::Object_Index object_index, const KNX::Property_Index property_index)
{
    if (!interface_objects.objects.count(object_index)) {
        return;
    }
    std::shared_ptr<KNX::Interface_Object> interface_object = interface_objects.objects[object_index];
    Q_ASSERT(interface_object);

    if (!interface_object->properties.count(property_index)) {
        return;
    }
    std::shared_ptr<KNX::Property> property = interface_object->properties[property_index];
    Q_ASSERT(property);

    const KNX::Property_Id property_id = property->description.property_id;
    const uint8_t property_datatype_size = size(property->description.property_datatype);
    uint16_t nr_of_elem = property->description.max_nr_of_elem;
    // TP1: octets 12..21 contain data, 10 total.
    if ((nr_of_elem * property_datatype_size) > 10) {
        nr_of_elem = 10 / property_datatype_size;
    }
    const KNX::PropertyValue_Start_Index start_index = 1; // 0 is size, 1 is data
    getStack()->A_PropertyValue_Read_req(KNX::Ack_Request::dont_care, KNX::Priority::low, KNX::Network_Layer_Parameter, {address, false}, object_index, property_id, nr_of_elem, start_index, [](const KNX::Status /*a_status*/){
        // Lcon
    });
}

void DeviceData::LdCtrlUnload(uint8_t lsmIdx, uint16_t objType, uint16_t occurrence)
{
    // @todo LdCtrlUnload
}

void DeviceData::LdCtrlLoad(uint8_t lsmIdx, uint16_t objType, uint16_t occurrence)
{
    // @todo LdCtrlLoad
}

void DeviceData::LdCtrlMaxLength(uint8_t lsmIdx, uint16_t objType, uint16_t occurrence, uint32_t size)
{
    // @todo LdCtrlMaxLength
}

void DeviceData::LdCtrlClearCachedObjectTypes()
{
    // @todo LdCtrlClearCachedObjectTypes
}

void DeviceData::LdCtrlLoadCompleted(uint8_t lsmIdx, uint16_t objType, uint16_t occurrence)
{
    // @todo LdCtrlLoadCompleted
}

void DeviceData::LdCtrlAbsSegment(uint8_t lsmIdx, uint16_t objType, uint16_t occurrence, uint8_t segType, uint16_t address, uint16_t size, uint8_t access, uint8_t memType, uint8_t segFlags)
{
    // @todo LdCtrlAbsSegment
}

void DeviceData::LdCtrlRelSegment(uint8_t lsmIdx, uint16_t objType, uint16_t occurrence, uint32_t size, uint8_t mode, uint8_t fill)
{
    // @todo LdCtrlRelSegment
}

void DeviceData::LdCtrlTaskSegment(uint8_t lsmIdx, uint16_t objType, uint16_t occurrence, uint16_t address)
{
    // @todo LdCtrlTaskSegment
}

void DeviceData::LdCtrlTaskPtr(uint8_t lsmIdx, uint16_t objType, uint16_t occurrence, uint16_t initPtr, uint16_t savePtr, uint16_t serialPtr)
{
    // @todo LdCtrlTaskPtr
}

void DeviceData::LdCtrlTaskCtrl1(uint8_t lsmIdx, uint16_t objType, uint16_t occurrence, uint16_t address, uint8_t count)
{
    // @todo LdCtrlTaskCtrl1
}

void DeviceData::LdCtrlTaskCtrl2(uint8_t lsmIdx, uint16_t objType, uint16_t occurrence, uint16_t callback, uint16_t address, uint16_t seg0, uint16_t seg1)
{
    // @todo LdCtrlTaskCtrl2
}

void DeviceData::LdCtrlWriteProp(uint8_t lsmIdx, uint16_t objType, uint16_t occurrence, uint16_t propId, uint16_t startElement, uint16_t count, bool verify, std::vector<uint8_t> inlineData)
{
    // @todo LdCtrlWriteProp
}

void DeviceData::LdCtrlCompareProp(bool allowCachedValue, std::vector<uint8_t> inlineData, std::vector<uint8_t> mask, std::string range, bool invert, uint16_t retryInterval, uint16_t timeOut, uint8_t objIdx, uint16_t objType, uint16_t occurrence, uint16_t propId, uint16_t startElement, uint16_t count)
{
    // @todo LdCtrlCompareProp
}

void DeviceData::LdCtrlLoadImageProp(uint8_t objIdx, uint16_t objType, uint16_t occurrence, uint16_t propId, uint16_t startElement, uint16_t count)
{
    // @todo LdCtrlLoadImageProp
}

void DeviceData::LdCtrlInvokeFunctionProp(uint8_t objIdx, uint16_t objType, uint16_t occurrence, uint16_t propId, std::vector<uint8_t> inlineData)
{
    // @todo LdCtrlInvokeFunctionProp
}

void DeviceData::LdCtrlReadFunctionProp(uint8_t objIdx, uint16_t objType, uint16_t occurrence, uint16_t propId)
{
    // @todo LdCtrlReadFunctionProp
}

void DeviceData::LdCtrlWriteMem(std::string addressSpace, uint32_t address, uint32_t size, bool verify, std::vector<uint8_t> inlineData)
{
    // @todo LdCtrlWriteMem
}

void DeviceData::LdCtrlCompareMem(bool allowCachedValue, std::vector<uint8_t> inlineData, std::vector<uint8_t> mask, std::string range, bool invert, uint16_t retryInterval, uint16_t timeOut, std::string addressSpace, uint32_t address, uint32_t size)
{
    // @todo LdCtrlCompareMem
}

void DeviceData::LdCtrlLoadImageMem(std::string addressSpace, uint32_t address, uint32_t size)
{
    // @todo LdCtrlLoadImageMem
}

void DeviceData::LdCtrlWriteRelMem(uint8_t objIdx, uint16_t objType, uint16_t occurrence, uint32_t offset, uint32_t size, bool verify, std::vector<uint8_t> inlineData)
{
    // @todo LdCtrlWriteRelMem
}

void DeviceData::LdCtrlCompareRelMem(bool allowCachedValue, std::vector<uint8_t> inlineData, std::vector<uint8_t> mask, std::string range, bool invert, uint16_t retryInterval, uint16_t timeOut, uint8_t objIdx, uint16_t objType, uint16_t occurrence, uint32_t offset, uint32_t size)
{
    // @todo LdCtrlCompareRelMem
}

void DeviceData::LdCtrlLoadImageRelMem(uint8_t objIdx, uint16_t objType, uint16_t occurrence, uint32_t offset, uint32_t size)
{
    // @todo LdCtrlLoadImageRelMem
}

void DeviceData::LdCtrlConnect()
{
    transport_layer_connect();
}

void DeviceData::LdCtrlDisconnect()
{
    transport_layer_disconnect();
}

void DeviceData::LdCtrlRestart()
{
    // @todo LdCtrlRestart
}

void DeviceData::LdCtrlMasterReset(uint8_t eraseCode, uint8_t channelNumber)
{
    // @todo LdCtrlMasterReset
}

void DeviceData::LdCtrlDelay(uint16_t milliSeconds)
{
    // @todo LdCtrlDelay
}

void DeviceData::LdCtrlSetControlVariable(std::string name, bool value)
{
    // @todo LdCtrlSetControlVariable
}

void DeviceData::LdCtrlMapError(uint8_t ldCtrlFilter, uint32_t originalError, uint32_t mappedError)
{
    // @todo LdCtrlMapError
}

void DeviceData::LdCtrlProgressText(uint32_t textId, std::string messageRef)
{
    // @todo LdCtrlProgressText
}

void DeviceData::LdCtrlDeclarePropDesc(uint8_t objIdx, uint16_t objType, uint16_t occurrence, uint16_t propId, std::string propType, uint16_t maxElements, uint8_t readAccess, uint8_t writeAccess, bool writeable)
{
    // @todo LdCtrlDeclarePropDesc
}

void DeviceData::LdCtrlClearLCFilterTable(bool useFunctionProp)
{
    // @todo LdCtrlClearLCFilterTable
}

void DeviceData::LdCtrlMerge(uint8_t mergeId)
{
    // @todo LdCtrlMerge
}

void DeviceData::LdCtrlBaseChoose()
{
    // @todo LdCtrlBaseChoose
}
