#pragma once

#include <QByteArray>
#include <QMap>
#include <QObject>

#include <Connection/DeviceData.h>
#include <Connection/GroupData.h>
#include <KNX/Stack.h>
#include <Project/ProjectBundle.h>
#include <Project/v20/Helpers.h>
#include <Project/v20/knx/GroupAddressStyle_t.h>
#include <Project/v20/knx/GroupAddresses_GroupRanges_t.h>
#include <Project/v20/knx/GroupRange_t.h>
#include <Project/v20/knx/KNX_t.h>
#include <Project/v20/knx/Project_Installations_Installation_t.h>
#include <Project/v20/knx/Project_ProjectInformation_t.h>
#include <Project/v20/knx/Project_t.h>
#include <Project/v20/knx/Topology_Area_Line_t.h>
#include <Project/v20/knx/Topology_Area_t.h>

/**
 * This class provides the main business logic of OpenETS.
 * It is also an integration layer across:
 * - KNX Stack
 * - Device and Group Data
 * - ETS Project
 *
 * Ideally all UI widgets should work as far as possible with this class
 * instead of doing direct stack interaction.
 *
 * This class also provides conversion functions between stack (mostly
 * consisting of C++ templates), project (Qt templates) and live data.
 */
class Connection :
    public QObject
{
    Q_OBJECT

public:
    explicit Connection(QObject * parent = nullptr);
    virtual ~Connection();

    /* KNX stack */

    /** set stack */
    void setStack(Stack * stack);

    /** get stack */
    Stack * getStack() const;

    /* ETS Project */

    /** set ETS Project Bundle */
    void setProjectBundle(ProjectBundle * projectBundle);

    /** get ETS Project Bundle */
    ProjectBundle * getProjectBundle() const;

    /** clear ETS Project Bundle */
    void clearProjectBundle();

    /** get ETS KNX (root) */
    Project::v20::knx::KNX_t * getEtsKnx() const;

    /** set ETS Project */
    void setEtsProject(Project::v20::knx::Project_t * etsProject);

    /** get ETS Project */
    Project::v20::knx::Project_t * getEtsProject() const;

    /** clear ETS Project */
    void clearEtsProject();

    /** ETS ProjectInformation GroupAddressStyle */
    Project::v20::knx::GroupAddressStyle_t getEtsGroupAddressStyle() const;

    /** ETS Installation */
    Project::v20::knx::Project_Installations_Installation_t * getEtsInstallation() const;

    /* Live Data */

    /** get group data */
    GroupData * group(const KNX::Group_Address groupAddress);

    /** get device data */
    DeviceData * device(const KNX::Individual_Address individualAddress_);

Q_SIGNALS:
    void groupDataChanged(const KNX::Group_Address address);

private Q_SLOTS:
    void A_Connect_ind(const KNX::ASAP_Connected asap);
    void A_Disconnect_ind(const KNX::ASAP_Connected asap);

    /* Services (Group) */
    void A_GroupValue_Read_ind(const KNX::ASAP_Group asap, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type);
    void A_GroupValue_Read_Acon(const KNX::ASAP_Group asap, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const std::vector<uint8_t> data);
    void A_GroupValue_Write_ind(const KNX::ASAP_Group asap, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const std::vector<uint8_t> data);

    /* Services (Broadcast) */
    void A_IndividualAddress_Write_ind(const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::Individual_Address newaddress);
    void A_IndividualAddress_Read_ind(const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type);
    void A_IndividualAddress_Read_Acon(const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::Individual_Address individual_address);
    void A_IndividualAddressSerialNumber_Read_ind(const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::Serial_Number serial_number);
    void A_IndividualAddressSerialNumber_Read_Acon(const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::Serial_Number serial_number, const KNX::Individual_Address individual_address, const KNX::Domain_Address_2 domain_address);
    void A_IndividualAddressSerialNumber_Write_ind(const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::Serial_Number serial_number, const KNX::Individual_Address newaddress);
    void A_NetworkParameter_Read_ind(const KNX::ASAP_Individual asap, const KNX::Comm_Mode comm_mode_req, const KNX::Hop_Count_Type hop_count_type, const KNX::Parameter_Type parameter_type, const KNX::Priority priority, const KNX::Parameter_Test_Info test_info);
    void A_NetworkParameter_Read_Acon(const KNX::ASAP_Individual asap, const KNX::Hop_Count_Type hop_count_type, const KNX::Individual_Address individual_address, const KNX::Parameter_Type parameter_type, const KNX::Priority priority, const KNX::Parameter_Test_Info_Result test_info_result);
    void A_NetworkParameter_Write_ind(const KNX::ASAP_Individual asap, const KNX::Parameter_Type parameter_type, const KNX::Priority priority, const std::vector<uint8_t> value);
    void A_NetworkParameter_InfoReport_ind(const KNX::ASAP_Individual asap, const KNX::Comm_Mode comm_mode_req, const KNX::Hop_Count_Type hop_count_type, const KNX::Parameter_Type parameter_type, const KNX::Priority priority, const KNX::Parameter_Test_Info_Result test_info_result);

    /* Services (System Broadcast) */
    void A_DeviceDescriptor_InfoReport_ind(const KNX::Descriptor_Type descriptor_type, const std::vector<uint8_t> device_descriptor, const KNX::Hop_Count_Type hop_count_type, const KNX::Priority priority);
    void A_DomainAddress_Write_ind(const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::Domain_Address domain_address_new);
    void A_DomainAddress_Read_ind(const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type);
    void A_DomainAddress_Read_Acon(const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Individual asap, const KNX::Domain_Address domain_address);
    void A_DomainAddressSelective_Read_ind(const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASDU asdu);
    void A_DomainAddressSerialNumber_Read_ind(const KNX::Hop_Count_Type hop_count_type, const KNX::Priority priority, const KNX::Serial_Number serial_number);
    void A_DomainAddressSerialNumber_Read_Acon(const KNX::Domain_Address domain_address, const KNX::Hop_Count_Type hop_count_type, const KNX::Priority priority, const KNX::Serial_Number serial_number);
    void A_DomainAddressSerialNumber_Write_ind(const KNX::Domain_Address domain_address, const KNX::Hop_Count_Type hop_count_type, const KNX::Priority priority, const KNX::Serial_Number serial_number);
    void A_SystemNetworkParameter_Read_ind(const KNX::Hop_Count_Type hop_count_type, const KNX::System_Parameter_Type parameter_type, const KNX::Priority priority, const KNX::Parameter_Test_Info test_info);
    void A_SystemNetworkParameter_Read_Acon(const KNX::ASAP_Individual asap, const KNX::Hop_Count_Type hop_count_type, const KNX::System_Parameter_Type parameter_type, const KNX::Priority priority, const KNX::Parameter_Test_Info_Result test_info_result);
    void A_SystemNetworkParameter_Write_ind(const KNX::Hop_Count_Type hop_count_type, const KNX::System_Parameter_Type parameter_type, const KNX::Priority priority, const std::vector<uint8_t> value);

    /* Services (Individual) */
    void A_DeviceDescriptor_Read_ind(const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Individual asap, const KNX::Descriptor_Type descriptor_type);
    void A_DeviceDescriptor_Read_Acon(const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Individual asap, const KNX::Descriptor_Type descriptor_type, const std::vector<uint8_t> device_descriptor);
    void A_Restart_ind(const KNX::Restart_Erase_Code erase_code, const uint8_t channel_number, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Individual asap);
    // @todo A_Restart_Acon
    void A_FileStream_InfoReport_ind(const KNX::ASAP_Individual asap, const KNX::File_Block file_block, const KNX::File_Block_Sequence_Number file_block_sequence_number, const KNX::File_Handle file_handle, const KNX::Hop_Count_Type hop_count_type);
    void A_PropertyValue_Read_ind(const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Individual asap, const KNX::Object_Index object_index, const KNX::Property_Id property_id, const KNX::PropertyValue_Nr_Of_Elem nr_of_elem, const KNX::PropertyValue_Start_Index start_index);
    void A_PropertyValue_Read_Acon(const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Individual asap, const KNX::Object_Index object_index, const KNX::Property_Id property_id, const KNX::PropertyValue_Nr_Of_Elem nr_of_elem, const KNX::PropertyValue_Start_Index start_index, const KNX::Property_Value data);
    void A_PropertyValue_Write_ind(const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Individual asap, const KNX::Object_Index object_index, const KNX::Property_Id property_id, const KNX::PropertyValue_Nr_Of_Elem nr_of_elem, const KNX::PropertyValue_Start_Index start_index, const KNX::Property_Value data);
    void A_PropertyDescription_Read_ind(const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Individual asap, const KNX::Object_Index object_index, const KNX::Property_Id property_id, const KNX::Property_Index property_index);
    void A_PropertyDescription_Read_Acon(const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Individual asap, const KNX::Object_Index object_index, const KNX::Property_Id property_id, const KNX::Property_Index property_index, const bool write_enable, const KNX::Property_Type type, const KNX::Max_Nr_Of_Elem max_nr_of_elem, const KNX::Access access);
    void A_FunctionPropertyCommand_ind(const KNX::ASAP_Individual asap, const KNX::Comm_Mode comm_mode, const std::vector<uint8_t> data, const KNX::Hop_Count_Type hop_count_type, const KNX::Object_Index object_index, const KNX::Priority priority, const KNX::Property_Id property_id);
    void A_FunctionPropertyCommand_Acon(const KNX::ASAP_Individual asap, const KNX::Comm_Mode comm_mode, const std::vector<uint8_t> data, const KNX::Hop_Count_Type hop_count_type, const KNX::Object_Index object_index, const KNX::Priority priority, const KNX::Property_Id property_id, const std::optional<KNX::Property_Return_Code> return_code);
    void A_FunctionPropertyState_Read_ind(const KNX::ASAP_Individual asap, const KNX::Comm_Mode comm_mode, const std::vector<uint8_t> data, const KNX::Hop_Count_Type hop_count_type, const KNX::Object_Index object_index, const KNX::Priority priority, const KNX::Property_Id property_id);
    void A_FunctionPropertyState_Read_Acon(const KNX::ASAP_Individual asap, const KNX::Comm_Mode comm_mode, const std::vector<uint8_t> data, const KNX::Hop_Count_Type hop_count_type, const KNX::Object_Index object_index, const KNX::Priority priority, const KNX::Property_Id property_id, const std::optional<KNX::Property_Return_Code> return_code);

    /* Services (Connected) */
    void A_ADC_Read_ind(const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::ADC_Channel_Nr channel_nr, const uint8_t read_count);
    void A_ADC_Read_Acon(const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::ADC_Channel_Nr channel_nr, const uint8_t read_count, const uint16_t sum);
    void A_Memory_Read_ind(const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::Memory_Number number, const KNX::Memory_Address memory_address);
    void A_Memory_Read_Acon(const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::Memory_Number number, const KNX::Memory_Address memory_address, const std::vector<uint8_t> data);
    void A_Memory_Write_ind(const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::Memory_Number number, const KNX::Memory_Address memory_address, const std::vector<uint8_t> data);
    void A_Memory_Write_Acon(const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::Memory_Number number, const KNX::Memory_Address memory_address, const std::vector<uint8_t> data);
    void A_MemoryBit_Write_ind(const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::MemoryBit_Number number, const KNX::MemoryBit_Address memory_address, const std::vector<uint8_t> and_data, const std::vector<uint8_t> xor_data);
    void A_UserMemory_Read_ind(const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::UserMemory_Number number, const KNX::UserMemory_Address memory_address);
    void A_UserMemory_Read_Acon(const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::UserMemory_Number number, const KNX::UserMemory_Address memory_address, const std::vector<uint8_t> data);
    void A_UserMemory_Write_ind(const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::UserMemory_Number number, const KNX::UserMemory_Address memory_address, const std::vector<uint8_t> data);
    void A_UserMemory_Write_Acon(const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::UserMemory_Number number, const KNX::UserMemory_Address memory_address, const std::vector<uint8_t> data);
    void A_UserMemoryBit_Write_ind(const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::UserMemoryBit_Number number, const KNX::UserMemoryBit_Address memory_address, const std::vector<uint8_t> and_data, const std::vector<uint8_t> xor_data);
    void A_UserMemoryBit_Write_Acon(const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::UserMemoryBit_Number number, const KNX::UserMemoryBit_Address memory_address, const std::vector<uint8_t> data);
    void A_UserManufacturerInfo_Read_ind(const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap);
    void A_UserManufacturerInfo_Read_Acon(const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::MFact_Info mfact_info);
    void A_Authorize_Request_ind(const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::Key key);
    void A_Authorize_Request_Acon(const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::Level level);
    void A_Key_Write_ind(const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::Level level, const KNX::Key key);
    void A_Key_Write_Acon(const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::Level level);

private:
    /** KNX stack */
    Stack * m_stack{nullptr};

    /** ETS Project Bundle */
    ProjectBundle * m_projectBundle{nullptr};

    /** ETS KNX (root) */
    Project::v20::knx::KNX_t * m_etsKnx{nullptr};

    /** ETS Project */
    Project::v20::knx::Project_t * m_etsProject{nullptr};

    /** ETS ProjectInformation GroupAddressStyle */
    Project::v20::knx::GroupAddressStyle_t m_etsGroupAddressStyle{"ThreeLevel"};

    /** ETS Installation */
    Project::v20::knx::Project_Installations_Installation_t * m_etsInstallation{nullptr};

    /** ETS Area */
    Project::v20::knx::Topology_Area_t * m_etsArea{nullptr};

    /** ETS Line */
    Project::v20::knx::Topology_Area_Line_t * m_etsLine{nullptr};

    /** Live Group Data */
    QMap<KNX::Group_Address, GroupData *> m_groups;

    /** Live Device Data */
    QMap<KNX::Individual_Address, DeviceData *> m_devices;
};
