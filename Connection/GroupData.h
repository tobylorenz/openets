#pragma once

#include <QByteArray>
#include <QMap>

#include <KNX/Stack.h>
#include <Project/v20/knx/DatapointType_DatapointSubtypes_DatapointSubtype_t.h>
#include <Project/v20/knx/DatapointType_t.h>
#include <Project/v20/knx/GroupAddress_t.h>

/**
 * This class contains data for a specific group object
 * adressed by a group address.
 *
 * Group Object Value Diagrams over time
 */
class GroupData :
    public QObject
{
    Q_OBJECT

public:
    explicit GroupData(QObject * parent = nullptr);
    virtual ~GroupData();

    /* Links to KNX stack */

    Stack * getStack() const;

    /** Group Address */
    KNX::Group_Address address{}; // ASAP_Group

    /** Group Value */
    QMap<QDateTime, std::vector<uint8_t>> objectValues;

    /* Links to ETS Project */

    /** ETS Group Address */
    const Project::v20::knx::GroupAddress_t * etsGroupAddress{nullptr};

    /** ETS Datapoint Type */
    Project::v20::knx::DatapointType_t * etsDatapointType{nullptr};

    /** ETS Datapoint Subtype */
    Project::v20::knx::DatapointType_DatapointSubtypes_DatapointSubtype_t * etsDatapointSubtype{nullptr};

public Q_SLOTS:
    void readGroupValue();
    void writeGroupValue(const std::vector<uint8_t> groupObjectValue);

Q_SIGNALS:
    void groupValueChanged(const std::vector<uint8_t> groupObjectValue);
};
