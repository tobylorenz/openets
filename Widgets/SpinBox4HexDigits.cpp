#include "SpinBox4HexDigits.h"

SpinBox4HexDigits::SpinBox4HexDigits(QWidget * parent) :
    QSpinBox(parent)
{
    setDisplayIntegerBase(16);
    setMaximum(0x10000);
    setPrefix("0x");
    setSingleStep(0x10);
}

QString SpinBox4HexDigits::textFromValue (int value) const
{
    return QString("%1").arg(value, 4, 16, QChar('0'));
}
