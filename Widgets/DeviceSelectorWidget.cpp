#include "DeviceSelectorWidget.h"

#include <QBoxLayout>
#include <QDebug>
#include <QLabel>

#include <KNX/yaml_output.h>
#include <Project/v20/knx/DeviceInstance_t.h>
#include <Project/v20/knx/Project_Installations_Installation_t.h>
#include <Project/v20/knx/Project_Installations_t.h>
#include <Project/v20/knx/Project_ProjectInformation_t.h>
#include <Project/v20/knx/Topology_Area_Line_Address_t.h>
#include <Project/v20/knx/Topology_Area_Line_t.h>
#include <Project/v20/knx/Topology_Area_t.h>
#include <Project/v20/knx/Topology_t.h>

DeviceSelectorWidget::DeviceSelectorWidget(QWidget * parent) :
    QDialog(parent)
{
    setWindowTitle("Device Selector Widget");

    /* elements */
    deviceComboBox = new QComboBox(this);
    deviceComboBox->setEditable(true);
    dialogButtonBox = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel, this);
    connect(dialogButtonBox, SIGNAL(accepted()), this, SLOT(accept()));
    connect(dialogButtonBox, SIGNAL(rejected()), this, SLOT(reject()));

    /* layout */
    QVBoxLayout * vBoxLayout = new QVBoxLayout();
    setLayout(vBoxLayout);

    /* assign elements to layout */
    vBoxLayout->addWidget(new QLabel(tr("Device:")));
    vBoxLayout->addWidget(deviceComboBox);
    vBoxLayout->addWidget(dialogButtonBox);
}

void DeviceSelectorWidget::setConnection(Connection * connection)
{
    if (!connection->getEtsInstallation()) {
        return;
    }

    for (const Project::v20::knx::DeviceInstance_t * etsDeviceInstance : connection->getEtsInstallation()->findChildren<Project::v20::knx::DeviceInstance_t *>()) {
        Q_ASSERT(etsDeviceInstance);
        Project::v20::knx::Topology_Area_Line_t * etsLine = etsDeviceInstance->findParent<Project::v20::knx::Topology_Area_Line_t *>();
        if (etsLine) {
            Project::v20::knx::Topology_Area_t * etsArea = etsLine->findParent<Project::v20::knx::Topology_Area_t *>();
            if (etsArea) {
                QString address_str = QString("%1.%2.%3")
                                      .arg(etsArea->Address)
                                      .arg(etsLine->Address)
                                      .arg(etsDeviceInstance->Address);
                QVariant individual_address =
                    (etsArea->Address.toUShort() << 12) |
                    (etsLine->Address.toUShort() << 8) |
                    (etsDeviceInstance->Address.toUShort());
                deviceComboBox->addItem(address_str, individual_address);
            }
        }
    }
}

KNX::Individual_Address DeviceSelectorWidget::selectedAddress() const
{
    return KNX::Individual_Address(deviceComboBox->currentData().toUInt());
}

QString DeviceSelectorWidget::selectedAddressStr() const
{
    return deviceComboBox->currentText();
}
