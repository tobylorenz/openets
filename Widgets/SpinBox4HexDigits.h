#pragma once

#include <QSpinBox>

class SpinBox4HexDigits : public QSpinBox
{
    Q_OBJECT

public:
    SpinBox4HexDigits(QWidget * parent = nullptr);

    QString textFromValue(int value) const override;
};
