#pragma once

#include <asio.hpp>

#include <QComboBox>
#include <QPushButton>
#include <QTextEdit>
#include <QWidget>

#include <KNX/03_System_Specifications.h>

#include "KNX/KNXnet.h"

/**
 * @brief Bus Interfaces Window
 *
 * Provides connection to KNX bus.
 */
class BusInterfacesWindow : public QWidget
{
    Q_OBJECT

public:
    explicit BusInterfacesWindow(QWidget * parent = nullptr);
    virtual ~BusInterfacesWindow();

    void setKNXnet(KNXnet * knxnet);

private Q_SLOTS:
    void on_discoverPushButton_clicked();
    void on_descriptionRequestPushButton_clicked();
    void on_tunnelConnectPushButton_clicked();
    void on_connectionstateRequestPushButton_clicked();
    void on_disconnectRequestPushButton_clicked();

private:
    /** KNX/IP network stack */
    KNXnet * knxnet{nullptr};

    /* discovery channel */
    QComboBox * discoverComboBox{nullptr};
    QPushButton * discoverPushButton{nullptr};
    void search_Lcon(const KNX::Status status);
    void search_Acon(KNX::IP_Host_Protocol_Address_Information search_response_ip_address, std::shared_ptr<KNX::Host_Protocol_Address_Information> control_endpoint, std::shared_ptr<KNX::Device_Information_DIB> device_hardware, std::shared_ptr<KNX::Supported_Service_Families_DIB> supported_service_families);

    /* communication channel */
    QComboBox * communicationComboBox{nullptr};
    QPushButton * descriptionRequestPushButton{nullptr};
    QPushButton * tunnelConnectPushButton{nullptr};
    QPushButton * connectionstateRequestPushButton{nullptr};
    QPushButton * disconnectRequestPushButton{nullptr};
    KNX::IP_Host_Protocol_Address_Information remote_control_endpoint{};
    void connect_Lcon(const KNX::Status status);
    void connect_Acon(const KNX::Error_Code status, const std::shared_ptr<KNX::Host_Protocol_Address_Information> data_endpoint, const std::shared_ptr<KNX::Connection_Response_Data_Block> connection_response_data_block);
    void description_Lcon(const KNX::Status status);
    void description_Acon(const std::shared_ptr<KNX::Device_Information_DIB> device_hardware, const std::shared_ptr<KNX::Supported_Service_Families_DIB> supported_service_families, const std::vector<std::shared_ptr<KNX::Description_Information_Block>> other_device_information);
    void connectionstate_Lcon(const KNX::Status status);
    void connectionstate_Acon(const KNX::Error_Code status);
    void disconnect_Lcon(const KNX::Status status);
    void disconnect_ind(const std::shared_ptr<KNX::Host_Protocol_Address_Information> control_endpoint);
    void disconnect_Acon(const KNX::Error_Code status);

    /* output */
    QTextEdit * textEdit{nullptr};
};
