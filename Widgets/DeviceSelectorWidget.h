#pragma once

#include <QComboBox>
#include <QDialog>
#include <QDialogButtonBox>

#include <Connection/Connection.h>

/**
 * Device Select Widget
 */
class DeviceSelectorWidget : public QDialog
{
    Q_OBJECT

public:
    explicit DeviceSelectorWidget(QWidget * parent = nullptr);

    void setConnection(Connection * connection);

    KNX::Individual_Address selectedAddress() const;
    QString selectedAddressStr() const;

private:
    Connection * connection{nullptr};

    QComboBox * deviceComboBox{nullptr};
    QDialogButtonBox * dialogButtonBox{nullptr};
};
