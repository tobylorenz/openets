#include "SystemNetworkParameterView.h"

SystemNetworkParameterView::SystemNetworkParameterView(QWidget * parent) :
    QWidget(parent)
{
    setWindowTitle("SystemNetworkParameter Read/Write");
}

SystemNetworkParameterView::~SystemNetworkParameterView()
{
}

void SystemNetworkParameterView::setConnection(Connection * connection)
{
    Q_ASSERT(connection);
    this->connection = connection;

    //connect(connection, &Connection::groupObjectValueChanged, this, &GroupValueView::groupObjectValueChanged);
}
