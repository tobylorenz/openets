#pragma once

#include <QWidget>

#include <Connection/Connection.h>

/**
 * Domain Address View
 *
 * Services:
 * - A_DomainAddress_Write (System Broadcast)
 * - A_DomainAddress_Read (System Broadcast)
 * - A_DomainAddressSelective_Read (System Broadcast)
 * - A_DomainAddressSerialNumber_Read (System Broadcast)
 * - A_DomainAddressSerialNumber_Write (System Broadcast)
 */
class DomainAddressView : public QWidget
{
    Q_OBJECT

public:
    explicit DomainAddressView(QWidget * parent = nullptr);
    virtual ~DomainAddressView();

    void setConnection(Connection * connection);

private:
    Connection * connection{nullptr};
};
