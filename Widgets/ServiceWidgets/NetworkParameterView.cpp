#include "NetworkParameterView.h"

NetworkParameterView::NetworkParameterView(QWidget * parent) :
    QWidget(parent)
{
    setWindowTitle("NetworkParameter InfoReport/Read/Write");
}

NetworkParameterView::~NetworkParameterView()
{
}

void NetworkParameterView::setConnection(Connection * connection)
{
    Q_ASSERT(connection);
    this->connection = connection;

    //connect(connection, &Connection::groupObjectValueChanged, this, &GroupValueView::groupObjectValueChanged);
}
