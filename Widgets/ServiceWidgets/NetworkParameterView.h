#pragma once

#include <QWidget>

#include <Connection/Connection.h>

/**
 * Network Parameter View
 *
 * Services:
 * - A_NetworkParameter_Read (Broadcast)
 * - A_NetworkParameter_Write (Broadcast)
 * - A_NetworkParameter_InfoReport (Broadcast)
 */
class NetworkParameterView : public QWidget
{
    Q_OBJECT

public:
    explicit NetworkParameterView(QWidget * parent = nullptr);
    virtual ~NetworkParameterView();

    void setConnection(Connection * connection);

private:
    Connection * connection{nullptr};
};
