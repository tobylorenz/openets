#pragma once

#include <QWidget>

#include <Connection/Connection.h>

/**
 * System Network Parameter View
 *
 * Services:
 * - A_SystemNetworkParameter_Read (System Broadcast)
 * - A_SystemNetworkParameter_Write (System Broadcast)
 */
class SystemNetworkParameterView : public QWidget
{
    Q_OBJECT

public:
    explicit SystemNetworkParameterView(QWidget * parent = nullptr);
    virtual ~SystemNetworkParameterView();

    void setConnection(Connection * connection);

private:
    Connection * connection{nullptr};
};
