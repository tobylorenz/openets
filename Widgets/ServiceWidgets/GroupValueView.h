#pragma once

#include <QComboBox>
#include <QLineEdit>
#include <QMainWindow>
#include <QTableWidget>

#include <Connection/Connection.h>

/**
 * Group Value View
 *
 * Services:
 * - A_GroupValue_Read (Group)
 * - A_GroupValue_Write (Group)
 */
class GroupValueView : public QMainWindow
{
    Q_OBJECT

public:
    explicit GroupValueView(QWidget * parent = nullptr);
    virtual ~GroupValueView();

    void setConnection(Connection * connection);

private Q_SLOTS:
    void groupDataChanged(const KNX::Group_Address groupAddress);
    void populateFromEtsProject();
    void clearEmptyDatapoints();
    void readGroupValue();
    void updateGroupValueEnabled(const QString & groupAddressStr);
    void changeGroupAddressStyle();
    void selectionChanged();

private:
    Connection * connection{nullptr};
    QComboBox * addressStyleComboBox{nullptr};
    QAction * readGroupValueAction{nullptr};
    QLineEdit * groupAddressLineEdit{nullptr};
    QTableWidget * tableWidget{nullptr};

    static QString styledAddress(const Project::v20::knx::GroupAddressStyle_t groupAddressStyle, const KNX::Group_Address groupAddress);
};
