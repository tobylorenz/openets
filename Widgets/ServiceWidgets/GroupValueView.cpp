#include "GroupValueView.h"

#include <QAction>
#include <QDebug>
#include <QRegularExpression>
#include <QRegularExpressionMatch>
#include <QToolBar>
#include <QVBoxLayout>
#include <QValidator>

#include <KNX/03/07/02_Datapoint_Types.h>
#include <KNX/Exceptions.h>

#include <KNX/yaml_output.h>
#include <Project/v20/Helpers.h>
#include <Project/v20/knx/DatapointType_DatapointSubtypes_DatapointSubtype_t.h>
#include <Project/v20/knx/DatapointType_DatapointSubtypes_t.h>
#include <Project/v20/knx/DatapointType_t.h>
#include <Project/v20/knx/GroupAddress_t.h>
#include <Project/v20/knx/MasterData_DatapointTypes_t.h>
#include <Project/v20/knx/Project_Installations_Installation_t.h>
#include <Project/v20/knx/Project_Installations_t.h>

GroupValueView::GroupValueView(QWidget * parent) :
    QMainWindow(parent)
{
    setWindowTitle("GroupValue Read/Write");

    /* create Actions */
    auto * populateFromEtsProjectAction = new QAction("Populate from ETS Project", this);
    connect(populateFromEtsProjectAction, &QAction::triggered, this, &GroupValueView::populateFromEtsProject);
    auto * clearEmptyDatapointsAction = new QAction("Clear empty datapoints", this);
    connect(clearEmptyDatapointsAction, &QAction::triggered, this, &GroupValueView::clearEmptyDatapoints);
    readGroupValueAction = new QAction("Read Group Value", this);
    readGroupValueAction->setEnabled(false);
    connect(readGroupValueAction, &QAction::triggered, this, &GroupValueView::readGroupValue);
    groupAddressLineEdit = new QLineEdit(this);
    connect(groupAddressLineEdit, &QLineEdit::textChanged, this, &GroupValueView::updateGroupValueEnabled);

    /* address style */
    addressStyleComboBox = new QComboBox();
    addressStyleComboBox->setEditable(false);
    addressStyleComboBox->addItem("TwoLevel");
    addressStyleComboBox->addItem("ThreeLevel");
    addressStyleComboBox->addItem("Free");
    connect(addressStyleComboBox, &QComboBox::currentTextChanged, this, &GroupValueView::changeGroupAddressStyle);

    /* create ToolBars */
    auto * toolBar = new QToolBar(this);
    toolBar->addAction(populateFromEtsProjectAction);
    toolBar->addAction(clearEmptyDatapointsAction);
    toolBar->addSeparator();
    toolBar->addWidget(addressStyleComboBox);
    toolBar->addSeparator();
    toolBar->addAction(readGroupValueAction);
    toolBar->addWidget(groupAddressLineEdit);
    addToolBar(toolBar);

    /* create TableWidget */
    tableWidget = new QTableWidget(this);
    tableWidget->setColumnCount(6);
    tableWidget->setHorizontalHeaderLabels({"Group Address (raw)", "Data (raw)", "Group Address", "Name", "Datapoint Subtype", "Datapoint"});
    connect(tableWidget, &QTableWidget::itemSelectionChanged, this, &GroupValueView::selectionChanged);

    setCentralWidget(tableWidget);
}

GroupValueView::~GroupValueView()
{
}

void GroupValueView::setConnection(Connection * connection)
{
    Q_ASSERT(connection);
    this->connection = connection;

    connect(connection, &Connection::groupDataChanged, this, &GroupValueView::groupDataChanged);

    Project::v20::knx::GroupAddressStyle_t addressStyle = connection->getEtsGroupAddressStyle();
    Q_ASSERT(addressStyleComboBox);
    addressStyleComboBox->setCurrentText(addressStyle);
}

void GroupValueView::groupDataChanged(const KNX::Group_Address groupAddress)
{
    Q_ASSERT(connection);
    GroupData * groupData = connection->group(groupAddress);
    Q_ASSERT(groupData);

    const std::vector<uint8_t> groupObjectValue = groupData->objectValues.last();

    /* get row */
    int row;
    for (row = 0; row < tableWidget->rowCount(); row++) {
        if (tableWidget->item(row, 0)->data(Qt::DisplayRole).toInt() == groupAddress) {
            break;
        }
    }
    if (row >= tableWidget->rowCount()) {
        tableWidget->insertRow(row);
    }

    int column = 0;
    QTableWidgetItem * tableWidgetItem;

    /* Group Address */
    tableWidgetItem = new QTableWidgetItem();
    tableWidgetItem->setData(Qt::DisplayRole, static_cast<uint16_t>(groupAddress));
    tableWidget->setItem(row, column++, tableWidgetItem);

    /* Data */
    tableWidget->setItem(row, column++, new QTableWidgetItem(to_string(std::vector<uint8_t>(groupObjectValue.cbegin(), groupObjectValue.cend())).c_str()));

    /* Group Address */
    tableWidget->setItem(row, column++, new QTableWidgetItem(styledAddress(addressStyleComboBox->currentText(), groupAddress)));

    if (groupData->etsGroupAddress) {
        /* Name */
        tableWidget->setItem(row, column++, new QTableWidgetItem(groupData->etsGroupAddress->Name));

        if (groupData->etsDatapointType && groupData->etsDatapointSubtype) {
            /* Datapoint Subtype */
            tableWidget->setItem(row, column++, new QTableWidgetItem(groupData->etsDatapointSubtype->treeData(Qt::DisplayRole).toString()));

            /* Datapoint */
            std::shared_ptr<KNX::Datapoint_Type> knx_datapoint_type = KNX::make_Datapoint_Type(groupData->etsDatapointType->Number.toUShort(), groupData->etsDatapointSubtype->Number.toUShort());
            if (knx_datapoint_type) {
                try {
                    knx_datapoint_type->fromData(std::cbegin(groupObjectValue), std::cend(groupObjectValue));
                    tableWidgetItem = new QTableWidgetItem(knx_datapoint_type->text().c_str());
                    tableWidget->setItem(row, column++, tableWidgetItem);
                } catch (const KNX::DataDoesntMatchDPTException & e) {
                    tableWidgetItem = new QTableWidgetItem("data size doesn't match expected DPT size");
                    QColor color("red"); // error
                    QBrush brush(color);
                    tableWidgetItem->setForeground(brush);
                    tableWidget->setItem(row, column++, tableWidgetItem);
                }
            } else {
                tableWidgetItem = new QTableWidgetItem(QString("DPST-%1-%2 not implemented in KNX library").arg(groupData->etsDatapointType->Number, groupData->etsDatapointSubtype->Number));
                QColor color("yellow"); // warning
                QBrush brush(color);
                tableWidgetItem->setForeground(brush);
                tableWidget->setItem(row, column++, tableWidgetItem);
            }
        } else {
            /* Datapoint Subtype */
            tableWidgetItem = new QTableWidgetItem("Empty DPST");
            QColor color("yellow"); // warning
            QBrush brush(color);
            tableWidgetItem->setForeground(brush);
            tableWidget->setItem(row, column++, tableWidgetItem);
        }
    }

    tableWidget->sortItems(0);
}

void GroupValueView::populateFromEtsProject()
{
    if (connection && connection->getEtsInstallation()) {
        auto * installation = connection->getEtsInstallation();
        Q_ASSERT(installation);
        for (const Project::v20::knx::GroupAddress_t * projectGroupAddress : installation->findChildren<Project::v20::knx::GroupAddress_t *>()) {
            const KNX::Group_Address groupAddress(projectGroupAddress->Address.toUInt());

            /* get row */
            int row;
            for (row = 0; row < tableWidget->rowCount(); row++) {
                if (tableWidget->item(row, 0)->data(Qt::DisplayRole).toInt() == groupAddress) {
                    break;
                }
            }

            if (row >= tableWidget->rowCount()) {
                tableWidget->insertRow(row);
            }

            int column = 0;
            QTableWidgetItem * tableWidgetItem;

            /* Group Address */
            tableWidgetItem = new QTableWidgetItem();
            tableWidgetItem->setData(Qt::DisplayRole, static_cast<uint16_t>(groupAddress));
            tableWidget->setItem(row, column++, tableWidgetItem);

            /* Data */
            column++; // don't change

            /* Group Address */
            tableWidget->setItem(row, column++, new QTableWidgetItem(styledAddress(addressStyleComboBox->currentText(), groupAddress)));

            GroupData * groupData = connection->group(groupAddress);
            Q_ASSERT(groupData);
            if (groupData->etsGroupAddress) {

                /* Name */
                tableWidget->setItem(row, column++, new QTableWidgetItem(groupData->etsGroupAddress->Name));

                if (groupData->etsDatapointType && groupData->etsDatapointSubtype) {
                    /* Datapoint Subtype */
                    tableWidget->setItem(row, column++, new QTableWidgetItem(groupData->etsDatapointSubtype->treeData(Qt::DisplayRole).toString()));
                } else {
                    /* Datapoint Subtype */
                    tableWidgetItem = new QTableWidgetItem("Empty DPST");
                    tableWidget->setItem(row, column++, tableWidgetItem);
                }
            }
        }
    }

    tableWidget->sortItems(0);
}

void GroupValueView::clearEmptyDatapoints()
{
    for (int row = 0; row < tableWidget->rowCount(); row++) {
        if (!tableWidget->item(row, 1)) {
            tableWidget->removeRow(row);
            row--; // repeat line number
        }
    }
}

void GroupValueView::readGroupValue()
{
    Q_ASSERT(groupAddressLineEdit);
    Q_ASSERT(addressStyleComboBox);

    const QString groupAddressStr = groupAddressLineEdit->text();
    KNX::Group_Address groupAddress;
    if (addressStyleComboBox->currentText() == "TwoLevel") {
        QStringList elems = groupAddressStr.split('/');
        int mainGroup = elems[0].toUInt();
        int subGroup = elems[1].toUInt();
        groupAddress = (mainGroup << 11) | subGroup;
    }
    if (addressStyleComboBox->currentText() == "ThreeLevel") {
        QStringList elems = groupAddressStr.split('/');
        int mainGroup = elems[0].toUInt();
        int middleGroup = elems[1].toUInt();
        int subGroup = elems[2].toUInt();
        groupAddress = (mainGroup << 11) | (middleGroup << 8) | subGroup;
    }
    if (addressStyleComboBox->currentText() == "Free") {
        groupAddress = groupAddressStr.toUInt();
    }

    Q_ASSERT(connection);
    Stack * stack = connection->getStack();;
    Q_ASSERT(stack);
    stack->A_GroupValue_Read_req(KNX::Ack_Request::dont_care, groupAddress, KNX::Priority::low, KNX::Network_Layer_Parameter, [](const KNX::Status /*a_status*/){
        // Lcon
    });
}

void GroupValueView::updateGroupValueEnabled(const QString & groupAddressStr)
{
    Q_ASSERT(readGroupValueAction);
    Q_ASSERT(groupAddressLineEdit);

    QRegularExpression re("^[\\d/]+$");
    QRegularExpressionMatch match = re.match(groupAddressStr);
    if (!match.hasMatch()) {
        readGroupValueAction->setEnabled(false);
        return;
    }

    QStringList elems = groupAddressStr.split('/');

    switch (elems.count()) {
    case 1:
        // assume Free
    {
        int mainGroup = elems[0].toUInt();
        if (mainGroup >= 65536) {
            readGroupValueAction->setEnabled(false);
            return;
        }
        readGroupValueAction->setEnabled(true);
    }
    break;
    case 2:
        // assume TwoLevel
    {
        int mainGroup = elems[0].toUInt();
        if (mainGroup >= 32) {
            readGroupValueAction->setEnabled(false);
            return;
        }
        int subGroup = elems[1].toUInt();
        if (subGroup >= 2048) {
            readGroupValueAction->setEnabled(false);
            return;
        }
        readGroupValueAction->setEnabled(true);
    }
    break;
    case 3:
        // assume ThreeLevel
    {
        int mainGroup = elems[0].toUInt();
        if (mainGroup >= 32) {
            readGroupValueAction->setEnabled(false);
            return;
        }
        int middleGroup = elems[1].toUInt();
        if (middleGroup >= 8) {
            readGroupValueAction->setEnabled(false);
            return;
        }
        int subGroup = elems[2].toUInt();
        if (subGroup >= 256) {
            readGroupValueAction->setEnabled(false);
            return;
        }
        readGroupValueAction->setEnabled(true);
    }
    break;
    default:
        readGroupValueAction->setEnabled(false);
        break;
    }
}

void GroupValueView::changeGroupAddressStyle()
{
    Q_ASSERT(tableWidget);

    for (int row = 0; row < tableWidget->rowCount(); row++) {
        const KNX::Group_Address groupAddress(tableWidget->item(row, 0)->data(Qt::DisplayRole).toUInt());
        tableWidget->item(row, 2)->setData(Qt::DisplayRole, styledAddress(addressStyleComboBox->currentText(), groupAddress));
    }

    selectionChanged();
}

void GroupValueView::selectionChanged()
{
    Q_ASSERT(tableWidget);
    Q_ASSERT(groupAddressLineEdit);

    if (tableWidget->currentRow() < 0) {
        return;
    }

    QTableWidgetItem * tableWidgetItem = tableWidget->item(tableWidget->currentRow(), 2);
    Q_ASSERT(tableWidgetItem);
    QString groupAddressStr = tableWidgetItem->text();

    groupAddressLineEdit->setText(groupAddressStr);
}

QString GroupValueView::styledAddress(const Project::v20::knx::GroupAddressStyle_t groupAddressStyle, const KNX::Group_Address groupAddress)
{
    if (groupAddressStyle == "ThreeLevel") {
        // ThreeLevel: Main Group (M) 5 Bit / Middle Group (Mi) 3 Bit / Subgroup (G) 8 Bit
        const uint16_t main = (groupAddress >> 11) & 0x1f;
        const uint16_t middle = (groupAddress >> 8) & 0x07;
        const uint16_t sub = groupAddress & 0xff;
        return QString("%1/%2/%3").arg(main).arg(middle).arg(sub); // 5/3/8
    }
    if (groupAddressStyle == "TwoLevel") {
        // TwoLevel: Main Group (M) 5 Bit / Subgroup (G) 11 Bit
        const uint16_t main = (groupAddress >> 11) & 0x1f;
        const uint16_t sub = groupAddress & 0x07ff;
        return QString("%1/%2").arg(main).arg(sub);
    }
    // Free: 16 Bit
    return QString("%1").arg(groupAddress);
}
