#include "DomainAddressView.h"

DomainAddressView::DomainAddressView(QWidget * parent) :
    QWidget(parent)
{
    setWindowTitle("DomainAddress (Selective/SerialNumber) Read/Write");
}

DomainAddressView::~DomainAddressView()
{
}

void DomainAddressView::setConnection(Connection * connection)
{
    Q_ASSERT(connection);
    this->connection = connection;

    //connect(connection, &Connection::groupObjectValueChanged, this, &GroupValueView::groupObjectValueChanged);
}
