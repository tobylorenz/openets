#pragma once

#include <QLineEdit>
#include <QPushButton>
#include <QTableWidget>
#include <QRegularExpressionValidator>

#include <Connection/Connection.h>

/**
 * Individual Address View
 *
 * Services:
 * - NM_IndividualAddress_Read
 * - NM_IndividualAddress_Write
 * - NM_IndividualAddress_SerialNumber_Read
 * - NM_IndividualAddress_SerialNumber_Write
 * - NM_IndividualAddress_SerialNumber_Write2
 */
class IndividualAddressView : public QWidget
{
    Q_OBJECT

public:
    explicit IndividualAddressView(QWidget * parent = nullptr);
    virtual ~IndividualAddressView();

    void setConnection(Connection * connection);

private Q_SLOTS:
    void on_domainAddressIndividualAddressTableWidget_itemSelectionChanged();
    void on_individualAddressLineEdit_textChanged();
    void on_readPushButton_clicked();
    void on_serialNumberLineEdit_textChanged();
    void on_writePushButton_clicked();

private:
    Connection * connection{nullptr};
    KNX::NM_IndividualAddress_Read * nm_IndividualAddress_Read{nullptr};
    KNX::NM_IndividualAddress_Write * nm_IndividualAddress_Write{nullptr};
    KNX::NM_IndividualAddress_SerialNumber_Read * nm_IndividualAddress_SerialNumber_Read{nullptr};
    KNX::NM_IndividualAddress_SerialNumber_Write * nm_IndividualAddress_SerialNumber_Write{nullptr};
    KNX::NM_IndividualAddress_SerialNumber_Write2 * nm_IndividualAddress_SerialNumber_Write2{nullptr};
    bool requestRunning{false};

    QTableWidget * domainAddressIndividualAddressTableWidget{nullptr};
    QLineEdit * individualAddressLineEdit{nullptr};
    QRegularExpressionValidator * individualAddressValidator{nullptr};
    QPushButton * readPushButton{nullptr};
    QLineEdit * serialNumberLineEdit{nullptr};
    QRegularExpressionValidator * serialNumberValidator{nullptr};
    QPushButton * writePushButton{nullptr};

    bool serialNumberEmpty() const;
    bool serialNumberValid() const;
    bool individualAddressValid() const;
    void updateEnableStates();
};
