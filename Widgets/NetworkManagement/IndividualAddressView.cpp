#include "IndividualAddressView.h"

#include <QGridLayout>
#include <QLabel>

IndividualAddressView::IndividualAddressView(QWidget * parent) :
    QWidget(parent),
    domainAddressIndividualAddressTableWidget(new QTableWidget(0, 2, this)),
    individualAddressLineEdit(new QLineEdit(this)),
    readPushButton(new QPushButton(tr("Read"), this)),
    serialNumberLineEdit(new QLineEdit(this)),
    writePushButton(new QPushButton(tr("Write"), this))
{
    setWindowTitle("Network Management - IndividualAddress (SerialNumber) Read/Write");

    /* elements */
    domainAddressIndividualAddressTableWidget->setColumnCount(0);
    domainAddressIndividualAddressTableWidget->setRowCount(0);
    domainAddressIndividualAddressTableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);
    domainAddressIndividualAddressTableWidget->setSelectionMode(QAbstractItemView::SingleSelection);
    individualAddressLineEdit->setPlaceholderText("_._._");
    const QString byteRangeStr{"(?:[0-1]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])"};
    QRegularExpression individualAddressRegExp(byteRangeStr + "\\." + byteRangeStr + "\\." + byteRangeStr);
    individualAddressValidator = new QRegularExpressionValidator(individualAddressRegExp, this);
    serialNumberLineEdit->setPlaceholderText("__:__:__:__:__:__");
    QRegularExpression serialNumberRegExp("[A-Fa-f0-9]{2}:[A-Fa-f0-9]{2}:[A-Fa-f0-9]{2}:[A-Fa-f0-9]{2}:[A-Fa-f0-9]{2}:[A-Fa-f0-9]{2}");
    serialNumberValidator = new QRegularExpressionValidator(serialNumberRegExp, this);

    /* layout */
    QGridLayout * gridLayout = new QGridLayout();
    setLayout(gridLayout);

    /* assign elements to layout */
    gridLayout->addWidget(readPushButton, 0, 0);
    gridLayout->addWidget(new QLabel(tr("Serial Number (optional):")), 0, 1);
    gridLayout->addWidget(serialNumberLineEdit, 0, 2);
    gridLayout->addWidget(domainAddressIndividualAddressTableWidget, 1, 0, 1, 3);
    gridLayout->addWidget(writePushButton, 2, 0);
    gridLayout->addWidget(new QLabel(tr("New Individual Address:")), 2, 1);
    gridLayout->addWidget(individualAddressLineEdit, 2, 2);

    /** connections */
    connect(domainAddressIndividualAddressTableWidget, &QTableWidget::itemSelectionChanged, this, &IndividualAddressView::on_domainAddressIndividualAddressTableWidget_itemSelectionChanged);
    connect(individualAddressLineEdit, &QLineEdit::textChanged, this, &IndividualAddressView::on_individualAddressLineEdit_textChanged);
    connect(readPushButton, &QPushButton::clicked, this, &IndividualAddressView::on_readPushButton_clicked);
    connect(serialNumberLineEdit, &QLineEdit::textChanged, this, &IndividualAddressView::on_serialNumberLineEdit_textChanged);
    connect(writePushButton, &QPushButton::clicked, this, &IndividualAddressView::on_writePushButton_clicked);

    updateEnableStates();
}

IndividualAddressView::~IndividualAddressView()
{
    delete nm_IndividualAddress_Read;
    nm_IndividualAddress_Read = nullptr;

    delete nm_IndividualAddress_Write;
    nm_IndividualAddress_Write = nullptr;

    delete nm_IndividualAddress_SerialNumber_Read;
    nm_IndividualAddress_SerialNumber_Read = nullptr;

    delete nm_IndividualAddress_SerialNumber_Write;
    nm_IndividualAddress_SerialNumber_Write = nullptr;

    delete nm_IndividualAddress_SerialNumber_Write2;
    nm_IndividualAddress_SerialNumber_Write2 = nullptr;
}

void IndividualAddressView::setConnection(Connection * connection)
{
    Q_ASSERT(connection);
    this->connection = connection;

    Stack * stack = connection->getStack();
    Q_ASSERT(stack);

    nm_IndividualAddress_Read = stack->makeNetworkManagementIndividualAddressRead();
    Q_ASSERT(nm_IndividualAddress_Read);

    nm_IndividualAddress_Write = stack->makeNetworkManagementIndividualAddressWrite();
    Q_ASSERT(nm_IndividualAddress_Write);

    nm_IndividualAddress_SerialNumber_Read = stack->makeNetworkManagementIndividualAddressSerialNumberRead();
    Q_ASSERT(nm_IndividualAddress_SerialNumber_Read);

    nm_IndividualAddress_SerialNumber_Write = stack->makeNetworkManagementIndividualAddressSerialNumberWrite();
    Q_ASSERT(nm_IndividualAddress_SerialNumber_Write);

    nm_IndividualAddress_SerialNumber_Write2 = stack->makeNetworkManagementIndividualAddressSerialNumberWrite2();
    Q_ASSERT(nm_IndividualAddress_SerialNumber_Write2);
}

void IndividualAddressView::on_domainAddressIndividualAddressTableWidget_itemSelectionChanged()
{
    updateEnableStates();
}

void IndividualAddressView::on_individualAddressLineEdit_textChanged()
{
    updateEnableStates();
}

void IndividualAddressView::on_readPushButton_clicked()
{
    requestRunning = true;
    updateEnableStates();

    if (serialNumberEmpty()) {
        domainAddressIndividualAddressTableWidget->setColumnCount(1);
        domainAddressIndividualAddressTableWidget->setHorizontalHeaderLabels({"Individual Address"});
        domainAddressIndividualAddressTableWidget->setRowCount(0);
        domainAddressIndividualAddressTableWidget->resizeColumnsToContents();

        Q_ASSERT(nm_IndividualAddress_Read);
        nm_IndividualAddress_Read->req();
        nm_IndividualAddress_Read->con = [this](KNX::Status nm_status, std::set<KNX::Individual_Address> individual_addresses) -> void {
            if (nm_status == KNX::Status::ok) {
                domainAddressIndividualAddressTableWidget->setRowCount(individual_addresses.size());
                int row = 0;
                for(const KNX::Individual_Address & individual_address: individual_addresses) {
                    domainAddressIndividualAddressTableWidget->setItem(row++, 0, new QTableWidgetItem(QString::fromStdString(individual_address.text())));
                }
                domainAddressIndividualAddressTableWidget->resizeColumnsToContents();
            }
            requestRunning = false;
            updateEnableStates();
        };
    } else {
        domainAddressIndividualAddressTableWidget->setColumnCount(2);
        domainAddressIndividualAddressTableWidget->setHorizontalHeaderLabels({"Domain Address", "Individual Address"});
        domainAddressIndividualAddressTableWidget->setRowCount(0);
        domainAddressIndividualAddressTableWidget->resizeColumnsToContents();

        Q_ASSERT(nm_IndividualAddress_SerialNumber_Read);
        const KNX::Serial_Number serial_number(serialNumberLineEdit->text().toStdString());
        nm_IndividualAddress_SerialNumber_Read->req(serial_number);
        nm_IndividualAddress_SerialNumber_Read->con = [this](KNX::Status nm_status, KNX::Domain_Address_2 domain_address, KNX::Individual_Address individual_address) -> void {
            if (nm_status == KNX::Status::ok) {
                domainAddressIndividualAddressTableWidget->setRowCount(1);
                domainAddressIndividualAddressTableWidget->setItem(0, 0, new QTableWidgetItem(QString::fromStdString(domain_address.text())));
                domainAddressIndividualAddressTableWidget->setItem(0, 1, new QTableWidgetItem(QString::fromStdString(individual_address.text())));
                domainAddressIndividualAddressTableWidget->resizeColumnsToContents();
            }
            requestRunning = false;
            updateEnableStates();
        };
    }
}

void IndividualAddressView::on_serialNumberLineEdit_textChanged()
{
    updateEnableStates();
}

void IndividualAddressView::on_writePushButton_clicked()
{
    requestRunning = true;
    updateEnableStates();

    if (serialNumberEmpty()) {
        const KNX::Individual_Address individual_address(individualAddressLineEdit->text().toStdString());
        nm_IndividualAddress_Write->req(individual_address);
        nm_IndividualAddress_Write->con = [this](const KNX::Status nm_status) -> void {
            if (nm_status == KNX::Status::ok) {
                // @todo show results
            }
            requestRunning = false;
            updateEnableStates();
        };
    } else {
        const KNX::Serial_Number serial_number(serialNumberLineEdit->text().toStdString());
        const KNX::Individual_Address individual_address(individualAddressLineEdit->text().toStdString());
        nm_IndividualAddress_SerialNumber_Write->req(serial_number, individual_address);
        // @todo #2 including enter of config mode enter before, and read DD2 after
        nm_IndividualAddress_SerialNumber_Write->con = [this](const KNX::Status nm_status, const KNX::Domain_Address_2 domain_address) -> void {
            if (nm_status == KNX::Status::ok) {
                // @todo show results incl. domain_address
            }
            requestRunning = false;
            updateEnableStates();
        };
    }
}

bool IndividualAddressView::serialNumberEmpty() const
{
    return serialNumberLineEdit->text().isEmpty();
}

bool IndividualAddressView::serialNumberValid() const
{
    QString input = serialNumberLineEdit->text();
    int pos = 0;
    return serialNumberValidator->validate(input, pos) == QValidator::Acceptable;
}

bool IndividualAddressView::individualAddressValid() const
{
    QString input = individualAddressLineEdit->text();
    int pos = 0;
    return individualAddressValidator->validate(input, pos) == QValidator::Acceptable;
}

void IndividualAddressView::updateEnableStates()
{
    domainAddressIndividualAddressTableWidget->setEnabled(!requestRunning);

    individualAddressLineEdit->setEnabled(!requestRunning);

    readPushButton->setEnabled(!requestRunning && (serialNumberEmpty() || serialNumberValid()));

    serialNumberLineEdit->setEnabled(!requestRunning);

    // no need to select an existing domain address or individual address, write works independently
    writePushButton->setEnabled(!requestRunning && (serialNumberEmpty() || serialNumberValid()) && individualAddressValid());
}
