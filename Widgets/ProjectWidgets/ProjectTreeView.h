#pragma once

#include <QTreeView>

#include <Project/v20/knx/DeviceInstance_t.h>

/**
 * @brief Project Tree View
 *
 * The left side of the ProjectView contains the project tree.
 * This is the view of the model/view/control.
 */
class ProjectTreeView : public QTreeView
{
    Q_OBJECT

public:
    explicit ProjectTreeView(QWidget * parent = nullptr);

private Q_SLOTS:
    void onCustomContextMenuRequested(const QPoint & point);
};
