#include "ProjectTreeModel.h"

ProjectTreeModel::ProjectTreeModel(Project::Base * root, QObject * parent) :
    QAbstractItemModel(parent),
    root(root)
{
    Q_ASSERT(root);
}

bool ProjectTreeModel::canDropMimeData(const QMimeData *data, Qt::DropAction action, int row, int column, const QModelIndex &parent) const
{
    Q_ASSERT(data);

    if (!hasIndex(row, column, parent)) {
        return false;
    }
    QModelIndex i = index(row, column, parent);
    Q_ASSERT(i.isValid());

    const Project::Base * item = static_cast<Project::Base *>(i.internalPointer());
    if (!item) {
        return false;
    }
    return item->canDropMimeData(data, action);
}

int ProjectTreeModel::columnCount(const QModelIndex & /*parent*/) const
{
    return 1;
}

QVariant ProjectTreeModel::data(const QModelIndex & index, int role) const
{
    if (!index.isValid()) {
        return QVariant();
    }

    Project::Base * item = static_cast<Project::Base *>(index.internalPointer());
    Q_ASSERT(item);
    return item->treeData(role);
}

bool ProjectTreeModel::dropMimeData(const QMimeData *data, Qt::DropAction action, int row, int column, const QModelIndex &parent)
{
    if (!hasIndex(row, column, parent)) {
        return false;
    }
    QModelIndex i = index(row, column, parent);
    Q_ASSERT(i.isValid());

    Project::Base * item = static_cast<Project::Base *>(i.internalPointer());
    if (!item) {
        return false;
    }
    return item->dropMimeData(data, action);
}

Qt::ItemFlags ProjectTreeModel::flags(const QModelIndex &index) const
{
    Qt::ItemFlags baseFlags = QAbstractItemModel::flags(index);

    if (index.isValid()) {
        const Project::Base * item = static_cast<Project::Base *>(index.internalPointer());
        if (item) {
            return item->flags(baseFlags);
        }
    }

    return baseFlags;
}

QModelIndex ProjectTreeModel::index(int row, int column, const QModelIndex & parent) const
{
    if (!hasIndex(row, column, parent)) {
        return QModelIndex();
    }

    Project::Base * parentItem;
    if (!parent.isValid()) {
        parentItem = root;
    } else {
        parentItem = static_cast<Project::Base *>(parent.internalPointer());
    }
    Q_ASSERT(parentItem);

    Project::Base * childItem = parentItem->child(row);
    if (childItem) {
        return createIndex(row, column, childItem);
    }

    return QModelIndex();
}

QMimeData *ProjectTreeModel::mimeData(const QModelIndexList &indexes) const
{
    QStringList allIds;
    for (const QModelIndex &index : indexes) {
        if (index.isValid()) {
            Project::Base * item = static_cast<Project::Base *>(index.internalPointer());
            if (item) {
                QMimeData * itemMimeData = item->mimeData();
                if (itemMimeData) {
                    if (itemMimeData->hasFormat("application/x-openets")) {
                        QByteArray encodedItemData = itemMimeData->data("application/x-openets");
                        QDataStream itemDataStream(encodedItemData);
                        QStringList itemIds;
                        itemDataStream >> itemIds;

                        allIds << itemIds;
                    }
                    delete itemMimeData;
                }
            }
        }
    }

    QByteArray encodedData;
    QDataStream stream(&encodedData, QIODevice::WriteOnly);
    stream << allIds;

    QMimeData *mimeData = new QMimeData;
    mimeData->setData("application/x-openets", encodedData);
    return mimeData;
}

QStringList ProjectTreeModel::mimeTypes() const
{
    return { "application/x-openets" };
}

QModelIndex ProjectTreeModel::parent(const QModelIndex & index) const
{
    if (!index.isValid()) {
        return QModelIndex();
    }

    Project::Base * childItem = static_cast<Project::Base *>(index.internalPointer());
    Q_ASSERT(childItem);
    Project::Base * parentItem = childItem->parentItem();
    Q_ASSERT(parentItem);

    if (parentItem == root) {
        return QModelIndex();
    }

    return createIndex(parentItem->row(), 0, parentItem);
}

int ProjectTreeModel::rowCount(const QModelIndex & parent) const
{
    if (parent.column() > 0) {
        return 0;
    }

    Project::Base * parentItem;
    if (!parent.isValid()) {
        parentItem = root;
    } else {
        parentItem = static_cast<Project::Base *>(parent.internalPointer());
    }
    Q_ASSERT(parentItem);

    return parentItem->childCount();
}

Qt::DropActions ProjectTreeModel::supportedDragActions() const
{
    return Qt::MoveAction | Qt::LinkAction;
}

Qt::DropActions ProjectTreeModel::supportedDropActions() const
{
    return Qt::MoveAction | Qt::LinkAction;
}
