#include "ProjectView.h"

#include <QSplitter>

ProjectView::ProjectView(Project::Base * root, QWidget * parent) :
    QSplitter(parent)
{
    Q_ASSERT(root);

    treeModel = new ProjectTreeModel(root, this);
    Q_ASSERT(treeModel);

    treeView = new ProjectTreeView(this);
    Q_ASSERT(treeView);
    treeView->setModel(treeModel);

    tableModel = new ProjectTableModel(root, this);
    Q_ASSERT(tableModel);

    tableView = new ProjectTableView(this);
    Q_ASSERT(tableView);
    tableView->setModel(tableModel);

    addWidget(treeView);
    addWidget(tableView);

    connect(treeView, &QAbstractItemView::clicked, this, &ProjectView::onClicked);
}

void ProjectView::onClicked(const QModelIndex & index)
{
    tableView->setEnabled(false);
    Project::Base * item = static_cast<Project::Base *>(index.internalPointer());
    ProjectTableModel * oldTableModel = tableModel;
    tableModel = new ProjectTableModel(item);
    tableView->setModel(nullptr);
    tableView->reset();
    tableView->setModel(tableModel);
    tableView->update();
    tableView->setEnabled(true);
    delete oldTableModel;
}
