#include "ProjectTreeView.h"

#include <QApplication>
#include <QDebug>
#include <QMenu>

#include <MainWindow.h>
#include <Project/v20/ContextMenus.h>

ProjectTreeView::ProjectTreeView(QWidget * parent) :
    QTreeView(parent)
{
    setHeaderHidden(true);
    setContextMenuPolicy(Qt::CustomContextMenu);
    setSelectionMode(QTreeView::ExtendedSelection);
    setDragEnabled(true);
    setAcceptDrops(true);
    setDropIndicatorShown(true);
    connect(this, &QTreeView::customContextMenuRequested, this, &ProjectTreeView::onCustomContextMenuRequested);
}

void ProjectTreeView::onCustomContextMenuRequested(const QPoint & point)
{
    QModelIndex index = indexAt(point);
    if (index.isValid()) {
        Project::Base * item = static_cast<Project::Base *>(index.internalPointer());
        Q_ASSERT(item);

        Project::v20::knx::DeviceInstance_t * deviceInstance = qobject_cast<Project::v20::knx::DeviceInstance_t *>(item);
        if (deviceInstance) {
            MainWindow * mainWindow = qobject_cast<MainWindow *>(QApplication::activeWindow());
            Q_ASSERT(mainWindow);
            QMdiArea * mdiArea = mainWindow->mdiArea;
            QMenu * contextMenu = getCustomContextMenu(mdiArea, deviceInstance);
            if (contextMenu) {
                contextMenu->exec(viewport()->mapToGlobal(point));
            }
        }
    }
}
