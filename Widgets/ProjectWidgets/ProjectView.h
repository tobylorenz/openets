#pragma once

#include <QSplitter>

#include <Project/Base.h>
#include <Widgets/ProjectWidgets/ProjectTableModel.h>
#include <Widgets/ProjectWidgets/ProjectTableView.h>
#include <Widgets/ProjectWidgets/ProjectTreeModel.h>
#include <Widgets/ProjectWidgets/ProjectTreeView.h>

/**
 * @brief Project View
 *
 * Provides a view on the ETS project, with a given root object.
 * Left side shows a tree. The selected item is shown with a table on
 * the right side.
 */
class ProjectView : public QSplitter
{
    Q_OBJECT

public:
    explicit ProjectView(Project::Base * root, QWidget * parent = nullptr);

private Q_SLOTS:
    void onClicked(const QModelIndex & index);

private:
    ProjectTreeModel * treeModel{nullptr};
    ProjectTreeView * treeView{nullptr};
    ProjectTableModel * tableModel{nullptr};
    ProjectTableView * tableView{nullptr};
};
