#include "ProjectTableModel.h"

#include <QtGlobal>

ProjectTableModel::ProjectTableModel(Project::Base * root, QObject * parent) :
    QAbstractTableModel(parent),
    root(root)
{
    Q_ASSERT(root);

    for (Project::Base * base : root->findChildren<Project::Base *>()) {
        for (int section = 0; section < base->tableColumnCount(); ++section) {
            QVariant tableHeader = base->tableHeaderData(section, Qt::Horizontal, Qt::DisplayRole);
            if (!headers.contains(tableHeader.toString())) {
                headers.append(tableHeader.toString());
            }
        }
    }
}

// bool ProjectTableModel::canDropMimeData(const QMimeData *data, Qt::DropAction action, int row, int column, const QModelIndex &parent) const
// {
//     // @todo canDropMimeData
//     return QAbstractTableModel::canDropMimeData(data, action, row, column, parent);
// }

int ProjectTableModel::columnCount(const QModelIndex & /*parent*/) const
{
    return headers.size();
}

QVariant ProjectTableModel::data(const QModelIndex & index, int role) const
{
    if (!index.isValid()) {
        return QVariant();
    }

    Project::Base * item = static_cast<Project::Base *>(index.internalPointer());
    Q_ASSERT(item);
    return item->tableData(headers[index.column()], role);
}

// bool ProjectTableModel::dropMimeData(const QMimeData *data, Qt::DropAction action, int row, int column, const QModelIndex &parent)
// {
//     // @todo dropMimeData
//     return QAbstractTableModel::dropMimeData(data, action, row, column, parent);
// }

// Qt::ItemFlags ProjectTableModel::flags(const QModelIndex &index) const
// {
//     // @todo flags
//     return QAbstractTableModel::flags(index);
// }

QVariant ProjectTableModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::DisplayRole) {
        if (orientation == Qt::Horizontal) {
            if (section < headers.size()) {
                return headers[section];
            }
        }
    }

    return QVariant();
}

QModelIndex ProjectTableModel::index(int row, int column, const QModelIndex & /*parent*/) const
{
    Q_ASSERT(root);

    Project::Base * childItem = root->child(row);
    if (childItem) {
        return createIndex(row, column, childItem);
    }

    return QModelIndex();
}

// QMimeData *ProjectTableModel::mimeData(const QModelIndexList &indexes) const
// {
//     // @todo mimeData
//     return QAbstractTableModel::mimeData(indexes);
// }

// QStringList ProjectTableModel::mimeTypes() const
// {
//     // @todo mimeTypes
//     return QAbstractTableModel::mimeTypes();
// }

int ProjectTableModel::rowCount(const QModelIndex & /*parent*/) const
{
    return root->childCount();
}

// Qt::DropActions ProjectTableModel::supportedDragActions() const
// {
//     // @todo supportedDragActions
//     return QAbstractTableModel::supportedDragActions();
// }

// Qt::DropActions ProjectTableModel::supportedDropActions() const
// {
//     // @todo supportedDropActions
//     return QAbstractTableModel::supportedDropActions();
// }
