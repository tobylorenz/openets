#pragma once

#include <QAbstractItemModel>

#include <Project/Base.h>

/**
 * @brief Project Tree Model
 *
 * The left side of the ProjectView contains the project tree.
 * This is the model of the model/view/control.
 */
class ProjectTreeModel : public QAbstractItemModel
{
    Q_OBJECT

public:
    explicit ProjectTreeModel(Project::Base * root, QObject * parent = nullptr);

    bool canDropMimeData(const QMimeData *data, Qt::DropAction action, int row, int column, const QModelIndex &parent) const override;
    int columnCount(const QModelIndex & parent = QModelIndex()) const override;
    QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const override;
    bool dropMimeData(const QMimeData *data, Qt::DropAction action, int row, int column, const QModelIndex &parent) override;
    Qt::ItemFlags flags(const QModelIndex &index) const override;
    QModelIndex index(int row, int column, const QModelIndex & parent = QModelIndex()) const override;
    QMimeData *mimeData(const QModelIndexList &indexes) const override;
    QStringList mimeTypes() const override;
    QModelIndex parent(const QModelIndex & child) const override;
    int rowCount(const QModelIndex & parent = QModelIndex()) const override;
    Qt::DropActions supportedDragActions() const override;
    Qt::DropActions supportedDropActions() const override;

private:
    Project::Base * root{nullptr};
};
