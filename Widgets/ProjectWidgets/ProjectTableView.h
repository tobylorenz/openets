#pragma once

#include <QTableView>

/**
 * @brief Project Table View
 *
 * The right side of the ProjectView contains the table of the selected object.
 * This is the view of the model/view/control.
 */
class ProjectTableView : public QTableView
{
    Q_OBJECT

public:
    explicit ProjectTableView(QWidget * parent = nullptr);
};
