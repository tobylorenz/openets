#include "BusInterfacesWindow.h"

#include <QBoxLayout>
#include <QDebug>
#include <QGroupBox>
#include <QRegularExpression>
#include <QRegularExpressionMatch>

#include "config.h"

#include "KNX/yaml_output.h"

BusInterfacesWindow::BusInterfacesWindow(QWidget * parent) :
    QWidget(parent)
{
    setWindowTitle("Bus Interfaces");

    /* elements */
    QGroupBox * discoveryGroupBox = new QGroupBox(tr("Discovery Channel"), this);
    QGroupBox * communicationGroupBox = new QGroupBox(tr("Communication Channel"), this);
    discoverPushButton = new QPushButton(tr("Search Request"), this);
    discoverComboBox = new QComboBox(this);
    discoverComboBox->setEditable(true);
    discoverComboBox->addItem(tr("224.0.23.12:3671"));
    descriptionRequestPushButton = new QPushButton(tr("Description Request"), this);
    //descriptionRequestPushButton->setEnabled(false);
    tunnelConnectPushButton = new QPushButton(tr("Tunnel Connect Request"), this);
    //tunnelConnectPushButton->setEnabled(false);
    connectionstateRequestPushButton = new QPushButton(tr("Connectionstate Request"), this);
    connectionstateRequestPushButton->setEnabled(false);
    disconnectRequestPushButton = new QPushButton(tr("Disconnect Request"), this);
    disconnectRequestPushButton->setEnabled(false);
    communicationComboBox = new QComboBox(this);
    communicationComboBox->setEditable(true);
    textEdit = new QTextEdit(this);

    /* layout */
    QVBoxLayout * vBoxLayout = new QVBoxLayout();
    QVBoxLayout * discoveryVBoxLayout = new QVBoxLayout();
    QVBoxLayout * communicationVBoxLayout = new QVBoxLayout();
    QHBoxLayout * communicationHBoxLayout = new QHBoxLayout();
    setLayout(vBoxLayout);
    discoveryGroupBox->setLayout(discoveryVBoxLayout);
    communicationGroupBox->setLayout(communicationVBoxLayout);

    /* assign elements to layout */
    vBoxLayout->addWidget(discoveryGroupBox);
    vBoxLayout->addWidget(communicationGroupBox);
    vBoxLayout->addWidget(textEdit);
    discoveryVBoxLayout->addWidget(discoverComboBox);
    discoveryVBoxLayout->addWidget(discoverPushButton);
    communicationVBoxLayout->addWidget(communicationComboBox);
    communicationVBoxLayout->addLayout(communicationHBoxLayout);
    communicationHBoxLayout->addWidget(descriptionRequestPushButton);
    communicationHBoxLayout->addWidget(tunnelConnectPushButton);
    communicationHBoxLayout->addWidget(connectionstateRequestPushButton);
    communicationHBoxLayout->addWidget(disconnectRequestPushButton);

    /* connections */
    connect(discoverPushButton, &QPushButton::clicked, this, &BusInterfacesWindow::on_discoverPushButton_clicked);
    connect(descriptionRequestPushButton, &QPushButton::clicked, this, &BusInterfacesWindow::on_descriptionRequestPushButton_clicked);
    connect(tunnelConnectPushButton, &QPushButton::clicked, this, &BusInterfacesWindow::on_tunnelConnectPushButton_clicked);
    connect(connectionstateRequestPushButton, &QPushButton::clicked, this, &BusInterfacesWindow::on_connectionstateRequestPushButton_clicked);
    connect(disconnectRequestPushButton, &QPushButton::clicked, this, &BusInterfacesWindow::on_disconnectRequestPushButton_clicked);
}

BusInterfacesWindow::~BusInterfacesWindow()
{
}

void BusInterfacesWindow::setKNXnet(KNXnet * knxnet)
{
    Q_ASSERT(knxnet);
    this->knxnet = knxnet;

    /* bind response handlers */
    knxnet->discovery_channel.search_Lcon = std::bind(&BusInterfacesWindow::search_Lcon, this, std::placeholders::_1);
    knxnet->discovery_channel.search_Acon = std::bind(&BusInterfacesWindow::search_Acon, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4);
}

/* Search Request/Response */

void BusInterfacesWindow::on_discoverPushButton_clicked()
{
    Q_ASSERT(knxnet);

    /* get selected remote discovery endpoint */
    QString discoverEntry = discoverComboBox->currentText();
    if (discoverEntry.isEmpty()) {
        return;
    }
    QRegularExpression regExp("(\\d+).(\\d+).(\\d+).(\\d+):(\\d+)");
    QRegularExpressionMatch match = regExp.match(discoverEntry);
    if (!match.hasMatch()) {
        return;
    }
    KNX::IP_Host_Protocol_Address_Information remote_discovery_endpoint = KNX::IP_Host_Protocol_Address_Information(KNX::System_Setup_Multicast_Address, KNX::Port_Number); // @todo get this from discoveryComboBox
    remote_discovery_endpoint.ip_address[0] = match.captured(1).toUShort();
    remote_discovery_endpoint.ip_address[1] = match.captured(2).toUShort();
    remote_discovery_endpoint.ip_address[2] = match.captured(3).toUShort();
    remote_discovery_endpoint.ip_address[3] = match.captured(4).toUShort();
    remote_discovery_endpoint.ip_port_number = match.captured(5).toULong();

    /* send request */
    knxnet->discovery_channel.search_req(remote_discovery_endpoint);
}

void BusInterfacesWindow::search_Lcon(const KNX::Status status)
{
    textEdit->insertPlainText("Search Request sent: ");
    textEdit->insertPlainText((status == KNX::Status::ok) ? "ok" : "not ok");
    textEdit->insertPlainText(".\n");
}

void BusInterfacesWindow::search_Acon(
    KNX::IP_Host_Protocol_Address_Information search_response_ip_address,
    std::shared_ptr<KNX::Host_Protocol_Address_Information> control_endpoint,
    std::shared_ptr<KNX::Device_Information_DIB> device_hardware,
    std::shared_ptr<KNX::Supported_Service_Families_DIB> supported_service_families)
{
    textEdit->insertPlainText("Search Response received\n");

    std::shared_ptr<KNX::IP_Host_Protocol_Address_Information> hpai = std::dynamic_pointer_cast<KNX::IP_Host_Protocol_Address_Information>(control_endpoint);

    /* set ip address */
    std::array<uint8_t, 4> ip_address = hpai->ip_address;
    if ((ip_address[0] == 0) &&
            (ip_address[1] == 0) &&
            (ip_address[2] == 0) &&
            (ip_address[3] == 0)) {
        /* no valid HPAI information. Use sender ip instead. */
        ip_address = search_response_ip_address.ip_address;
    }

    /* set port */
    uint16_t port = hpai->ip_port_number;
    if (port == 0) {
        /* No valid HPAI information. Use standard port instead. */
        port = KNX::Port_Number;
    }

    /* add to communication combo box */
    QString communicationEntry = QString("%1.%2.%3.%4:%5")
                                 .arg(ip_address[0])
                                 .arg(ip_address[1])
                                 .arg(ip_address[2])
                                 .arg(ip_address[3])
                                 .arg(port);
    if (communicationComboBox->findText(communicationEntry) == -1) {
        communicationComboBox->addItem(communicationEntry);
    }
    if (communicationComboBox->currentText().isEmpty()) {
        communicationComboBox->setCurrentText(communicationEntry);
    }

#ifdef YAML_CPP_FOUND
    YAML::Emitter emitter;
    emitter << YAML::Key << "Search_Response";
    emitter << YAML::BeginMap;
    emitter << *control_endpoint;
    emitter << *device_hardware;
    emitter << *supported_service_families;
    emitter << YAML::EndMap;
    textEdit->insertPlainText(emitter.c_str());
    textEdit->insertPlainText("\n");
#endif

    descriptionRequestPushButton->setEnabled(true);
}

/* Description Request/Response */

void BusInterfacesWindow::on_descriptionRequestPushButton_clicked()
{
    Q_ASSERT(knxnet);

    /* get selected remote control endpoint */
    QString communicationEntry = communicationComboBox->currentText();
    if (communicationEntry.isEmpty()) {
        return;
    }
    QRegularExpression regExp("(\\d+).(\\d+).(\\d+).(\\d+):(\\d+)");
    QRegularExpressionMatch match = regExp.match(communicationEntry);
    if (!match.hasMatch()) {
        return;
    }
    remote_control_endpoint.ip_address[0] = match.captured(1).toUShort();
    remote_control_endpoint.ip_address[1] = match.captured(2).toUShort();
    remote_control_endpoint.ip_address[2] = match.captured(3).toUShort();
    remote_control_endpoint.ip_address[3] = match.captured(4).toUShort();
    remote_control_endpoint.ip_port_number = match.captured(5).toULong();

    /* get communication channel */
    KNX::IP_Communication_Channel * communication_channel = knxnet->communication_channel(remote_control_endpoint);
    Q_ASSERT(communication_channel);

    /* bind response handlers */
    communication_channel->description_Lcon = std::bind(&BusInterfacesWindow::description_Lcon, this, std::placeholders::_1);
    communication_channel->description_Acon = std::bind(&BusInterfacesWindow::description_Acon, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);
    communication_channel->connect_Lcon = std::bind(&BusInterfacesWindow::connect_Lcon, this, std::placeholders::_1);
    communication_channel->connect_Acon = std::bind(&BusInterfacesWindow::connect_Acon, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);
    communication_channel->connectionstate_Lcon = std::bind(&BusInterfacesWindow::connectionstate_Lcon, this, std::placeholders::_1);
    communication_channel->connectionstate_Acon = std::bind(&BusInterfacesWindow::connectionstate_Acon, this, std::placeholders::_1);
    communication_channel->disconnect_Lcon = std::bind(&BusInterfacesWindow::disconnect_Lcon, this, std::placeholders::_1);
    communication_channel->disconnect_ind = std::bind(&BusInterfacesWindow::disconnect_ind, this, std::placeholders::_1);
    communication_channel->disconnect_Acon = std::bind(&BusInterfacesWindow::disconnect_Acon, this, std::placeholders::_1);

    /* send request */
    communication_channel->description_req();
}

void BusInterfacesWindow::description_Lcon(const KNX::Status status)
{
    textEdit->insertPlainText("Description Request sent: ");
    textEdit->insertPlainText((status == KNX::Status::ok) ? "ok" : "not ok");
    textEdit->insertPlainText(".\n");
}

void BusInterfacesWindow::description_Acon(const std::shared_ptr<KNX::Device_Information_DIB> device_hardware, const std::shared_ptr<KNX::Supported_Service_Families_DIB> supported_service_families, const std::vector<std::shared_ptr<KNX::Description_Information_Block>> other_device_information)
{
    textEdit->insertPlainText("Description Response received.\n");

#ifdef YAML_CPP_FOUND
    YAML::Emitter emitter;
    emitter << YAML::Key << "Description_Response";
    emitter << YAML::BeginMap;
    emitter << *device_hardware;
    emitter << *supported_service_families;
    for (const auto & dib : other_device_information) {
        emitter << *dib;
    }
    emitter << YAML::EndMap;
    textEdit->insertPlainText(emitter.c_str());
    textEdit->insertPlainText("\n");
#endif

    tunnelConnectPushButton->setEnabled(true);
}

/* Connect Request/Response */

void BusInterfacesWindow::on_tunnelConnectPushButton_clicked()
{
    Q_ASSERT(knxnet);

    /* get communication channel */
    KNX::IP_Communication_Channel * communication_channel = knxnet->communication_channel(remote_control_endpoint);
    Q_ASSERT(communication_channel);

    /* send request */
    //communication_channel->stack.group_address_table->individual_address = 0x1100; // @todo should be taken from Device_Information_DIB
    communication_channel->connect_req(std::make_shared<KNX::Tunnelling_Connection_Request_Information>(KNX::Tunnelling_Layer_Type_Code::TUNNEL_LINKLAYER));
}

void BusInterfacesWindow::connect_Lcon(const KNX::Status status)
{
    textEdit->insertPlainText("Tunnel Connect Request sent: ");
    textEdit->insertPlainText((status == KNX::Status::ok) ? "ok" : "not ok");
    textEdit->insertPlainText(".\n");
}

void BusInterfacesWindow::connect_Acon(const KNX::Error_Code status, const std::shared_ptr<KNX::Host_Protocol_Address_Information> data_endpoint, const std::shared_ptr<KNX::Connection_Response_Data_Block> connection_response_data_block)
{
    textEdit->insertPlainText("Connect Response received.\n");

#ifdef YAML_CPP_FOUND
    YAML::Emitter emitter;
    emitter << YAML::Key << "Connect_Response";
    emitter << YAML::BeginMap;
    emitter << YAML::Key << "status" << YAML::Value << to_string(status);
    if (data_endpoint) {
        emitter << *data_endpoint;
    }
    if (connection_response_data_block) {
        emitter << *connection_response_data_block;
    }
    emitter << YAML::EndMap;
    textEdit->insertPlainText(emitter.c_str());
    textEdit->insertPlainText("\n");
#endif

    tunnelConnectPushButton->setEnabled(false);
    connectionstateRequestPushButton->setEnabled(true);
    disconnectRequestPushButton->setEnabled(true);
}

/* Connectionstate Request/Response */

void BusInterfacesWindow::on_connectionstateRequestPushButton_clicked()
{
    Q_ASSERT(knxnet);

    /* get communication channel */
    KNX::IP_Communication_Channel * communication_channel = knxnet->communication_channel(remote_control_endpoint);
    Q_ASSERT(communication_channel);

    /* send request */
    communication_channel->connectionstate_req();
}

void BusInterfacesWindow::connectionstate_Lcon(const KNX::Status status)
{
    textEdit->insertPlainText("Connectionstate Request sent: ");
    textEdit->insertPlainText((status == KNX::Status::ok) ? "ok" : "not ok");
    textEdit->insertPlainText(".\n");
}

void BusInterfacesWindow::connectionstate_Acon(const KNX::Error_Code status)
{
    textEdit->insertPlainText("Connectionstate Response received.\n");

#ifdef YAML_CPP_FOUND
    YAML::Emitter emitter;
    emitter << YAML::Key << "Connectionstate_Response";
    emitter << YAML::BeginMap;
    emitter << YAML::Key << "status" << YAML::Value << to_string(status);
    emitter << YAML::EndMap;
    textEdit->insertPlainText(emitter.c_str());
    textEdit->insertPlainText("\n");
#endif
}

/* Disconnect Request/Response */

void BusInterfacesWindow::on_disconnectRequestPushButton_clicked()
{
    Q_ASSERT(knxnet);

    /* get communication channel */
    KNX::IP_Communication_Channel * communication_channel = knxnet->communication_channel(remote_control_endpoint);
    Q_ASSERT(communication_channel);

    /* send request */
    communication_channel->disconnect_req();
}

void BusInterfacesWindow::disconnect_Lcon(const KNX::Status status)
{
    textEdit->insertPlainText("Disconnect Request sent: ");
    textEdit->insertPlainText((status == KNX::Status::ok) ? "ok" : "not ok");
    textEdit->insertPlainText(".\n");
}

void BusInterfacesWindow::disconnect_ind(const std::shared_ptr<KNX::Host_Protocol_Address_Information> control_endpoint)
{
    textEdit->insertPlainText("Disconnect Request received.\n");

#ifdef YAML_CPP_FOUND
    YAML::Emitter emitter;
    emitter << YAML::Key << "Disconnect_Request";
    emitter << YAML::BeginMap;
    emitter << *control_endpoint;
    emitter << YAML::EndMap;
    textEdit->insertPlainText(emitter.c_str());
    textEdit->insertPlainText("\n");
#endif

    tunnelConnectPushButton->setEnabled(true);
    connectionstateRequestPushButton->setEnabled(false);
    disconnectRequestPushButton->setEnabled(false);
}

void BusInterfacesWindow::disconnect_Acon(const KNX::Error_Code status)
{
    textEdit->insertPlainText("Disconnect Response received.\n");

#ifdef YAML_CPP_FOUND
    YAML::Emitter emitter;
    emitter << YAML::Key << "Disconnect_Response";
    emitter << YAML::BeginMap;
    emitter << YAML::Key << "status" << YAML::Value << to_string(status);
    emitter << YAML::EndMap;
    textEdit->insertPlainText(emitter.c_str());
    textEdit->insertPlainText("\n");
#endif

    tunnelConnectPushButton->setEnabled(true);
    connectionstateRequestPushButton->setEnabled(false);
    disconnectRequestPushButton->setEnabled(false);
}
