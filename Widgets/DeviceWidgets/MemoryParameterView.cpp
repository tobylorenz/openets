#include "MemoryParameterView.h"

#include <QDebug>

#include <Project/v20/knx/ApplicationProgramRef_t.h>
#include <Project/v20/knx/ApplicationProgramStatic_Code_AbsoluteSegment_t.h>
#include <Project/v20/knx/ApplicationProgramStatic_Code_t.h>
#include <Project/v20/knx/ApplicationProgramStatic_Parameters_Parameter_t.h>
#include <Project/v20/knx/ApplicationProgramStatic_Parameters_t.h>
#include <Project/v20/knx/ApplicationProgramStatic_t.h>
#include <Project/v20/knx/ApplicationProgram_t.h>
#include <Project/v20/knx/DeviceInstance_t.h>
#include <Project/v20/knx/Hardware2Program_t.h>
#include <Project/v20/knx/KNX_t.h>
#include <Project/v20/knx/MemoryParameter_t.h>
#include <Project/v20/knx/ParameterType_TypeColor_t.h>
#include <Project/v20/knx/ParameterType_TypeDate_t.h>
#include <Project/v20/knx/ParameterType_TypeFloat_t.h>
#include <Project/v20/knx/ParameterType_TypeIPAddress_t.h>
#include <Project/v20/knx/ParameterType_TypeNone_t.h>
#include <Project/v20/knx/ParameterType_TypeNumber_t.h>
#include <Project/v20/knx/ParameterType_TypePicture_t.h>
#include <Project/v20/knx/ParameterType_TypeRawData_t.h>
#include <Project/v20/knx/ParameterType_TypeRestriction_Enumeration_t.h>
#include <Project/v20/knx/ParameterType_TypeRestriction_t.h>
#include <Project/v20/knx/ParameterType_TypeText_t.h>
#include <Project/v20/knx/ParameterType_TypeTime_t.h>
#include <Project/v20/knx/ParameterType_t.h>
#include <Project/v20/knx/ParameterType_t.h>
#include <Project/v20/knx/Project_Installations_Installation_t.h>
#include <Project/v20/knx/Project_Installations_t.h>
#include <Project/v20/knx/Project_t.h>
#include <Project/v20/knx/PropertyParameter_t.h>
#include <Project/v20/knx/Topology_Area_Line_t.h>
#include <Project/v20/knx/Topology_Area_t.h>
#include <Project/v20/knx/Topology_t.h>
#include <Project/xs/ID.h>

MemoryParameterView::MemoryParameterView(QWidget * parent) :
    QTableWidget(parent)
{
    setColumnCount(3 + 2 + 3 + 7 + 2);
    setHorizontalHeaderLabels({
        "Parameter Id", "Parameter Name", "Parameter Text", // Access, Value, CustomerAdjustable, LegacyPatchAlways, SizeInBit
        "ParameterType Id", "ParameterType Name",
        "Memory Id", "Memory Offset", "Memory BitOffset",
        "Type", "Base", "SizeInBit", "Type", "minInclusive", "maxInclusive", "Increment",
        "Address", "SizeInByte" // calculated
    });
    connect(this, static_cast<void (QTableWidget::*)()>(&QTableWidget::itemSelectionChanged), this, &MemoryParameterView::on_itemSelectionChanged);
}

MemoryParameterView::~MemoryParameterView()
{
}

void MemoryParameterView::setDevice(DeviceData * deviceData)
{
    Q_ASSERT(deviceData);
    this->deviceData = deviceData;

    const Project::v20::knx::ApplicationProgram_t * applicationProgram = deviceData->ets_ApplicationProgram;
    Q_ASSERT(applicationProgram);
    const Project::v20::knx::ApplicationProgramStatic_t * applicationProgramStatic = applicationProgram->Static;
    Q_ASSERT(applicationProgramStatic);
    const Project::v20::knx::ApplicationProgramStatic_Parameters_t * parameters = applicationProgramStatic->Parameters;
    Q_ASSERT(parameters);

    int row = 0;
    setRowCount(0);
    clearContents();

    for (const QObject * parameter : parameters->children()) {
        Q_ASSERT(parameter);
        const Project::v20::knx::ApplicationProgramStatic_Parameters_Parameter_t * normalParameter = qobject_cast<const Project::v20::knx::ApplicationProgramStatic_Parameters_Parameter_t *>(parameter);
        if (normalParameter) {
            const Project::v20::knx::ParameterType_t * parameterType = normalParameter->getParameterType();
            Q_ASSERT(parameterType);

            for (QObject * base : parameter->children()) {
                const Project::v20::knx::MemoryParameter_t * memory = qobject_cast<const Project::v20::knx::MemoryParameter_t *>(base);
                if (memory) {
                    insertRow(row);

                    /* Parameter */
                    setItem(row, 0, new QTableWidgetItem(normalParameter->Id));
                    setItem(row, 1, new QTableWidgetItem(normalParameter->Name));
                    setItem(row, 2, new QTableWidgetItem(normalParameter->Text));
                    // normalParameter->Access
                    // normalParameter->Value
                    // normalParameter->CustomerAdjustable
                    // normalParameter->LegacyPatchAlways
                    // normalParameter->SizeInBit

                    /* Parameter Type */
                    setItem(row, 3, new QTableWidgetItem(parameterType->Id));
                    setItem(row, 4, new QTableWidgetItem(parameterType->Name));

                    /* Memory */
                    QTableWidgetItem * item;
                    setItem(row, 5, new QTableWidgetItem(memory->CodeSegment));
                    item = new QTableWidgetItem();
                    item->setData(Qt::DisplayRole, memory->Offset.toUInt());
                    setItem(row, 6, item);
                    item = new QTableWidgetItem();
                    item->setData(Qt::DisplayRole, memory->BitOffset.toUInt());
                    setItem(row, 7, item);

                    /* Type */
                    unsigned int sizeInByte = 0;
                    for (const QObject * typeBase : parameterType->children()) {
                        const Project::v20::knx::ParameterType_TypeNone_t * typeNone = qobject_cast<const Project::v20::knx::ParameterType_TypeNone_t *>(typeBase);
                        if (typeNone) {
                            Q_ASSERT(typeNone);
                            setItem(row, 8, new QTableWidgetItem("None"));
                        }

                        const Project::v20::knx::ParameterType_TypeNumber_t * typeNumber = qobject_cast<const Project::v20::knx::ParameterType_TypeNumber_t *>(typeBase);
                        if (typeNumber) {
                            Q_ASSERT(typeNumber);
                            setItem(row, 8, new QTableWidgetItem("Number"));
                            setItem(row, 10, new QTableWidgetItem(typeNumber->SizeInBit));
                            setItem(row, 11, new QTableWidgetItem(typeNumber->Type));
                            setItem(row, 12, new QTableWidgetItem(typeNumber->minInclusive));
                            setItem(row, 13, new QTableWidgetItem(typeNumber->maxInclusive));
                            setItem(row, 14, new QTableWidgetItem(typeNumber->Increment));
                            sizeInByte = (typeNumber->SizeInBit.toUInt() + 7) / 8;
                        }

                        const Project::v20::knx::ParameterType_TypeRestriction_t * typeRestriction = qobject_cast<const Project::v20::knx::ParameterType_TypeRestriction_t *>(typeBase);
                        if (typeRestriction) {
                            Q_ASSERT(typeRestriction);
                            setItem(row, 8, new QTableWidgetItem("Restriction"));
                            setItem(row, 9, new QTableWidgetItem(typeRestriction->Base_));
                            setItem(row, 10, new QTableWidgetItem(typeRestriction->SizeInBit));
                            sizeInByte = (typeRestriction->SizeInBit.toUInt() + 7) / 8;
                        }

                        const Project::v20::knx::ParameterType_TypeText_t * typeText = qobject_cast<const Project::v20::knx::ParameterType_TypeText_t *>(typeBase);
                        if (typeText) {
                            Q_ASSERT(typeText);
                            setItem(row, 8, new QTableWidgetItem("Text"));
                            setItem(row, 10, new QTableWidgetItem(typeText->SizeInBit));
                            sizeInByte = (typeText->SizeInBit.toUInt() + 7) / 8;
                        }

                        const Project::v20::knx::ParameterType_TypeFloat_t * typeFloat = qobject_cast<const Project::v20::knx::ParameterType_TypeFloat_t *>(typeBase);
                        if (typeFloat) {
                            Q_ASSERT(typeFloat);
                            setItem(row, 8, new QTableWidgetItem("Float"));
                            setItem(row, 12, new QTableWidgetItem(typeFloat->minInclusive));
                            setItem(row, 13, new QTableWidgetItem(typeFloat->maxInclusive));
                            setItem(row, 14, new QTableWidgetItem(typeFloat->Increment));
                            sizeInByte = 4; // @todo how long is float?
                        }

                        const Project::v20::knx::ParameterType_TypeTime_t * typeTime = qobject_cast<const Project::v20::knx::ParameterType_TypeTime_t *>(typeBase);
                        if (typeTime) {
                            Q_ASSERT(typeTime);
                            setItem(row, 8, new QTableWidgetItem("Time"));
                            setItem(row, 10, new QTableWidgetItem(typeTime->SizeInBit));
                            setItem(row, 12, new QTableWidgetItem(typeTime->minInclusive));
                            setItem(row, 13, new QTableWidgetItem(typeTime->maxInclusive));
                            sizeInByte = 1; // @todo how long is time?
                        }

                        const Project::v20::knx::ParameterType_TypeDate_t * typeDate = qobject_cast<const Project::v20::knx::ParameterType_TypeDate_t *>(typeBase);
                        if (typeDate) {
                            Q_ASSERT(typeDate);
                            setItem(row, 8, new QTableWidgetItem("Date"));
                            sizeInByte = 1; // @todo how long is date?
                        }

                        const Project::v20::knx::ParameterType_TypeIPAddress_t * typeIPAddress = qobject_cast<const Project::v20::knx::ParameterType_TypeIPAddress_t *>(typeBase);
                        if (typeIPAddress) {
                            Q_ASSERT(typeIPAddress);
                            setItem(row, 8, new QTableWidgetItem("IP Address"));
                            if (typeIPAddress->Version == "IPv4") {
                                sizeInByte = 4;
                            }
                            if (typeIPAddress->Version == "IPv6") {
                                sizeInByte = 6;
                            }
                        }

                        const Project::v20::knx::ParameterType_TypePicture_t * typePicture = qobject_cast<const Project::v20::knx::ParameterType_TypePicture_t *>(typeBase);
                        if (typePicture) {
                            Q_ASSERT(typePicture);
                            setItem(row, 8, new QTableWidgetItem("Picture"));
                            sizeInByte = 1; // @todo how long is picture?
                        }

                        const Project::v20::knx::ParameterType_TypeColor_t * typeColor = qobject_cast<const Project::v20::knx::ParameterType_TypeColor_t *>(typeBase);
                        if (typeColor) {
                            Q_ASSERT(typeColor);
                            setItem(row, 8, new QTableWidgetItem("Color"));
                            sizeInByte = 1; // @todo how long is color?
                        }

                        const Project::v20::knx::ParameterType_TypeRawData_t * typeRawData = qobject_cast<const Project::v20::knx::ParameterType_TypeRawData_t *>(typeBase);
                        if (typeRawData) {
                            Q_ASSERT(typeRawData);
                            setItem(row, 8, new QTableWidgetItem("Raw Data"));
                            sizeInByte = typeRawData->MaxSize.toInt(); // @todo how long is raw data?
                        }

                        Q_ASSERT(typeNone || typeNumber || typeRestriction || typeText || typeFloat || typeTime || typeDate || typeIPAddress || typePicture || typeColor || typeRawData);
                    }

                    /* calculated */
                    const Project::v20::knx::ApplicationProgramStatic_Code_AbsoluteSegment_t * codeSegment = memory->getCodeSegment();
                    Q_ASSERT(codeSegment);
                    item = new QTableWidgetItem();
                    item->setData(Qt::DisplayRole, (codeSegment->UserMemory == "true" ? 0x80000000 : 0) + codeSegment->Address.toUInt() + memory->Offset.toUInt());
                    setItem(row, 15, item);
                    item = new QTableWidgetItem();
                    item->setData(Qt::DisplayRole, sizeInByte);
                    setItem(row, 16, item);

                    row++;
                }

                const Project::v20::knx::PropertyParameter_t * property = qobject_cast<const Project::v20::knx::PropertyParameter_t *>(base);

                Q_ASSERT(memory || property);
            }
        }
    }

    sortByColumn(15, Qt::AscendingOrder); // sort by address
    resizeColumnsToContents();
}

void MemoryParameterView::on_itemSelectionChanged()
{
    const int row = currentRow();

    const bool userMemory = item(row, 15)->data(Qt::DisplayRole).toUInt() & 0x80000000;
    const KNX::Memory_Address address = item(row, 15)->data(Qt::DisplayRole).toUInt() & ~0x80000000;
    const KNX::Memory_Address size = item(row, 16)->data(Qt::DisplayRole).toUInt();

    Q_ASSERT(deviceData);
    Q_EMIT deviceData->memory_selected(userMemory, address, size);
}
