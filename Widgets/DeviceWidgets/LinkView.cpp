#include "LinkView.h"

LinkView::LinkView(QWidget * parent) :
    QWidget(parent)
{
    setWindowTitle("Link Read/Write");
}

LinkView::~LinkView()
{
}

void LinkView::setConnection(Connection * connection)
{
    Q_ASSERT(connection);
    this->connection = connection;

    Q_ASSERT(connection->getStack());
}

void LinkView::setDevice(DeviceData * device)
{
    Q_ASSERT(device);
    this->device = device;
}
