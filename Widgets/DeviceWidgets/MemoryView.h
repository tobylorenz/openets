#pragma once

#include <QAction>
#include <QCheckBox>
#include <QComboBox>
#include <QMainWindow>
#include <QTabWidget>
#include <QTableWidget>

#include <Connection/Connection.h>
#include <KNX/Stack.h>
#include <Widgets/SpinBox4HexDigits.h>
#include <Widgets/DeviceWidgets/MemoryAddressTableView.h>
#include <Widgets/DeviceWidgets/MemoryAssociationTableView.h>
#include <Widgets/DeviceWidgets/MemoryComObjectTableView.h>
#include <Widgets/DeviceWidgets/MemoryGroupObjectView.h>
#include <Widgets/DeviceWidgets/MemoryParameterView.h>
#include <Widgets/DeviceWidgets/MemoryResourcesView.h>

/**
 * Memory View
 *
 * Services:
 * - A_Memory_Read (Connected)
 * - A_Memory_Write (Connected)
 * - A_MemoryBit_Write (Connected)
 * - A_UserMemory_Read (Connected)
 * - A_UserMemory_Write (Connected)
 * - A_UserMemoryBit_Write (Connected)
 */
class MemoryView : public QMainWindow
{
    Q_OBJECT

public:
    explicit MemoryView(QWidget * parent = nullptr);
    virtual ~MemoryView();

    void setConnection(Connection * connection);
    void setDevice(DeviceData * deviceData);

public Q_SLOTS:
    void on_etsPresetsComboBox_currentIndexChanged(int index);
    void on_userMemoryChecked_stateChanged(int value);
    void on_memoryStartAddressSpinBox_valueChanged(int value);
    void on_memorySizeSpinBox_valueChanged(int value);
    void on_memoryEndAddressSpinBox_valueChanged(int value);
    void on_readMemoryAction_triggered();
    void on_memory_selected(bool userMemory, KNX::Memory_Address address, KNX::Memory_Address size);

private Q_SLOTS:
    void transportLayerConnectionStateChanged();
    void memoryChanged();

private:
    /* actions */
    QAction * connectAction{nullptr};
    QAction * disconnectAction{nullptr};
    QAction * readMemoryAction{nullptr};

    /* connection */
    Connection * connection{nullptr};

    /* device data */
    DeviceData * deviceData{nullptr};

    /* tool bar */
    QComboBox * etsPresetsComboBox{nullptr};
    QCheckBox * userMemoryCheckBox{nullptr};
    SpinBox4HexDigits * memoryStartAddressSpinBox{nullptr};
    SpinBox4HexDigits * memorySizeSpinBox{nullptr};
    SpinBox4HexDigits * memoryEndAddressSpinBox{nullptr};

    /* memory widget */
    QTableWidget * memoryTableWidget{nullptr};

    /* bottom widgets */
    QTabWidget * tabWidget{nullptr};
    MemoryResourcesView * memoryResourceWidget{nullptr};
    MemoryAddressTableView * memoryAddressTableWidget{nullptr};
    MemoryAssociationTableView * memoryAssociationTableWidget{nullptr};
    MemoryComObjectTableView * memoryComObjectTableWidget{nullptr};
    MemoryGroupObjectView * memoryGroupObjectWidget{nullptr};
    MemoryParameterView * parameterReferencesTableWidget{nullptr};
};
