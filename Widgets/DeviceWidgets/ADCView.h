#pragma once

#include <QWidget>

#include <Connection/Connection.h>

/**
 * ADC View
 *
 * Services:
 * - A_ADC_Read (Connected)
 */
class ADCView : public QWidget
{
    Q_OBJECT

public:
    explicit ADCView(QWidget * parent = nullptr);
    virtual ~ADCView();

    void setConnection(Connection * connection);
    void setDevice(DeviceData * device);

private:
    Connection * connection{nullptr};
    DeviceData * device{nullptr};
};
