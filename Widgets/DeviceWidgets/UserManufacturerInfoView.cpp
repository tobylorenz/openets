#include "UserManufacturerInfoView.h"

UserManufacturerInfoView::UserManufacturerInfoView(QWidget * parent) :
    QWidget(parent)
{
    setWindowTitle("UserMemory(Bit) Read/Write");
}

UserManufacturerInfoView::~UserManufacturerInfoView()
{
}

void UserManufacturerInfoView::setConnection(Connection * connection)
{
    Q_ASSERT(connection);
    this->connection = connection;
}

void UserManufacturerInfoView::setDevice(DeviceData * device)
{
    Q_ASSERT(device);
    this->device = device;
}
