#pragma once

#include <QTableWidget>

#include <Connection/Connection.h>

/**
 * Memory Group Object View
 */
class MemoryGroupObjectView : public QTableWidget
{
    Q_OBJECT

public:
    explicit MemoryGroupObjectView(QWidget * parent = nullptr);
    virtual ~MemoryGroupObjectView();

    void setDevice(DeviceData * deviceData);

private Q_SLOTS:
    void on_itemSelectionChanged();

private:
    /* device data */
    DeviceData * deviceData{nullptr};
};
