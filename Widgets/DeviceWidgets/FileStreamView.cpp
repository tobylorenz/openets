#include "FileStreamView.h"

FileStreamView::FileStreamView(QWidget * parent) :
    QWidget(parent)
{
    setWindowTitle("FileStream InfoReport");
}

FileStreamView::~FileStreamView()
{
}

void FileStreamView::setConnection(Connection * connection)
{
    Q_ASSERT(connection);
    this->connection = connection;

    Q_ASSERT(connection->getStack());
    // connect(connection->getStack(), &Stack::A_ADC_Read_Lcon, this, &ADCView::A_ADC_Read_Lcon);
    // connect(connection->getStack(), &Stack::A_ADC_Read_ind, this, &ADCView::A_ADC_Read_ind);
    // connect(connection->getStack(), &Stack::A_ADC_Read_Acon, this, &ADCView::A_ADC_Read_Acon);
}

void FileStreamView::setDevice(DeviceData * device)
{
    Q_ASSERT(device);
    this->device = device;
}
