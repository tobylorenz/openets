#include "MemoryResourcesView.h"

#include <QDebug>

#include <Project/v20/knx/HawkConfigurationData_Resources_Resource_ResourceType_t.h>
#include <Project/v20/knx/HawkConfigurationData_Resources_Resource_t.h>
#include <Project/v20/knx/HawkConfigurationData_Resources_t.h>
#include <Project/v20/knx/HawkConfigurationData_t.h>
#include <Project/v20/knx/MaskVersion_t.h>
#include <Project/v20/knx/ResourceLocation_t.h>

MemoryResourcesView::MemoryResourcesView(QWidget * parent) :
    QTableWidget(parent)
{
    setColumnCount(1 + 2);
    setHorizontalHeaderLabels({
        "Content",
        "Address", "SizeInByte" // calculated
    });
    connect(this, static_cast<void (QTableWidget::*)()>(&QTableWidget::itemSelectionChanged), this, &MemoryResourcesView::on_itemSelectionChanged);
}

MemoryResourcesView::~MemoryResourcesView()
{
}

void MemoryResourcesView::setDevice(DeviceData * deviceData)
{
    Q_ASSERT(deviceData);
    this->deviceData = deviceData;

    const Project::v20::knx::ApplicationProgram_t * applicationProgram = deviceData->ets_ApplicationProgram;
    Q_ASSERT(applicationProgram);
    const Project::v20::knx::MaskVersion_t * maskVersion = applicationProgram->getMaskVersion();
    Q_ASSERT(maskVersion);

    int row = 0;
    setRowCount(0);
    clearContents();

    for (const Project::v20::knx::HawkConfigurationData_t * hawkConfigurationData : maskVersion->HawkConfigurationData) {
        Q_ASSERT(hawkConfigurationData);
        const Project::v20::knx::HawkConfigurationData_Resources_t * resources = hawkConfigurationData->Resources;
        if (resources) {
            for (const Project::v20::knx::HawkConfigurationData_Resources_Resource_t * resource : resources->Resource) {
                Q_ASSERT(resource);
                const Project::v20::knx::ResourceName_t name = resource->Name; // GroupAddressTable, ...

                const Project::v20::knx::ResourceLocation_t * location = resource->Location;
                Project::v20::knx::ResourceAddrSpace_t addressSpace; // StandardMemory, UserMemory, Pointer, ...
                Project::xs::UnsignedInt startAddress;
                if (location) {
                    addressSpace = location->AddressSpace;
                    startAddress = location->StartAddress;
                }

                const Project::v20::knx::HawkConfigurationData_Resources_Resource_ResourceType_t * resourceType = resource->ResourceType;
                Project::xs::UnsignedInt length;
                Project::v20::knx::HawkConfigurationData_Resources_Resource_ResourceType_Flavour_t flavour; // Ptr_StandardMemory, AddressTable_Bcu1, AssociationTable_M112, LoadControl_M112, ProgrammingMode_Bcu1, ...
                if (resourceType) {
                    length = resourceType->Length;
                    flavour = resourceType->Flavour;
                }
                unsigned int flavourSize = 1;
                if (flavour == "Ptr_StandardMemory") {
                    flavourSize = 2;
                } else if (flavour == "AddressTable_Bcu1") {
                    flavourSize = 1 + 2 + 215 * 2; // = 0x1b1
                } else if (flavour == "AssociationTable_M112") {
                    flavourSize = 1 + 215 * 2; // = 0x1af
                } else if (flavour == "LoadControl_M112") {
                    flavourSize = 1;
                } else if (flavour == "ProgrammingMode_Bcu1") {
                    flavourSize = 1;
                }

                if (addressSpace == "StandardMemory") {
                    QTableWidgetItem * item;

                    insertRow(row);
                    setItem(row, 0, new QTableWidgetItem(name));
                    item = new QTableWidgetItem();
                    item->setData(Qt::DisplayRole, startAddress.toUInt());
                    setItem(row, 1, item);
                    item = new QTableWidgetItem();
                    item->setData(Qt::DisplayRole, length.toUInt() * flavourSize);
                    setItem(row, 2, item);
                    row++;
                } else if (addressSpace == "UserMemory") {
                    QTableWidgetItem * item;

                    insertRow(row);
                    setItem(row, 0, new QTableWidgetItem(name));
                    item = new QTableWidgetItem();
                    item->setData(Qt::DisplayRole, 0x80000000 + startAddress.toUInt());
                    setItem(row, 1, item);
                    item = new QTableWidgetItem();
                    item->setData(Qt::DisplayRole, length.toUInt() * flavourSize);
                    setItem(row, 2, item);
                    row++;
                }
            }
        }
    }

    sortByColumn(1, Qt::AscendingOrder); // sort by address
    resizeColumnsToContents();
}

void MemoryResourcesView::on_itemSelectionChanged()
{
    const int row = currentRow();

    const bool userMemory = item(row, 1)->data(Qt::DisplayRole).toUInt() & 0x80000000;
    const KNX::Memory_Address address = item(row, 1)->data(Qt::DisplayRole).toUInt() & ~0x80000000;
    const KNX::Memory_Address size = item(row, 2)->data(Qt::DisplayRole).toUInt();

    Q_ASSERT(deviceData);
    Q_EMIT deviceData->memory_selected(userMemory, address, size);
}
