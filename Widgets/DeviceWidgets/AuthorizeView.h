#pragma once

#include <QWidget>

#include <Connection/Connection.h>

/**
 * Authorize View
 *
 * Services:
 * - - A_Authorize_Request (Connected)
 */
class AuthorizeView : public QWidget
{
    Q_OBJECT

public:
    explicit AuthorizeView(QWidget * parent = nullptr);
    virtual ~AuthorizeView();

    void setConnection(Connection * connection);
    void setDevice(DeviceData * device);

private:
    Connection * connection{nullptr};
    DeviceData * device{nullptr};
};
