#include "AuthorizeView.h"

AuthorizeView::AuthorizeView(QWidget * parent) :
    QWidget(parent)
{
    setWindowTitle("Authorize Request");
}

AuthorizeView::~AuthorizeView()
{
}

void AuthorizeView::setConnection(Connection * connection)
{
    Q_ASSERT(connection);
    this->connection = connection;
}

void AuthorizeView::setDevice(DeviceData * device)
{
    Q_ASSERT(device);
    this->device = device;
}
