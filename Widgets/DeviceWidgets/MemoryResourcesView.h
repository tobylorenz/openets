#pragma once

#include <QTableWidget>

#include <Connection/Connection.h>

/**
 * Memory Resources View
 */
class MemoryResourcesView : public QTableWidget
{
    Q_OBJECT

public:
    explicit MemoryResourcesView(QWidget * parent = nullptr);
    virtual ~MemoryResourcesView();

    void setDevice(DeviceData * deviceData);

private Q_SLOTS:
    void on_itemSelectionChanged();

private:
    /* device data */
    DeviceData * deviceData{nullptr};
};
