#include "MemoryComObjectTableView.h"

#include <QDebug>

#include <Project/v20/knx/ApplicationProgramStatic_Code_AbsoluteSegment_t.h>
#include <Project/v20/knx/ApplicationProgramStatic_Code_t.h>
#include <Project/v20/knx/ApplicationProgramStatic_ComObjectTable_t.h>
#include <Project/v20/knx/ApplicationProgramStatic_t.h>
#include <Project/v20/knx/ComObject_t.h>
#include <Project/v20/knx/MaskVersion_t.h>

MemoryComObjectTableView::MemoryComObjectTableView(QWidget * parent) :
    QTableWidget(parent)
{
    setColumnCount(1 + 22 + 2);
    setHorizontalHeaderLabels({
        "Content",
        "Id", "Name", "Text", "Number", "FunctionText", "Priority", "ObjectSize", "ReadFlag", "WriteFlag", "CommunicationFlag", "TransmitFlag", "UpdateFlag", "ReadOnInitFlag", "DatapointType", "InternalDescription", "SecurityRequired", "MayRead", "ReadFlagLocked", "WriteFlagLocked", "TransmitFlagLocked", "UpdateFlagLocked", "ReadOnInitFlagLocked",
        "Address", "SizeInByte" // calculated
    });
    connect(this, static_cast<void (QTableWidget::*)()>(&QTableWidget::itemSelectionChanged), this, &MemoryComObjectTableView::on_itemSelectionChanged);
}

MemoryComObjectTableView::~MemoryComObjectTableView()
{
}

void MemoryComObjectTableView::setDevice(DeviceData * deviceData)
{
    Q_ASSERT(deviceData);
    this->deviceData = deviceData;

    const Project::v20::knx::ApplicationProgram_t * applicationProgram = deviceData->ets_ApplicationProgram;
    Q_ASSERT(applicationProgram);
    const Project::v20::knx::ApplicationProgramStatic_t * applicationProgramStatic = applicationProgram->Static;
    Q_ASSERT(applicationProgramStatic);
    const Project::v20::knx::ApplicationProgramStatic_Code_t * code = applicationProgramStatic->Code;
    Q_ASSERT(code);
    const Project::v20::knx::ApplicationProgramStatic_ComObjectTable_t * comObjectTable = applicationProgramStatic->ComObjectTable;
    Q_ASSERT(comObjectTable);

    const Project::v20::knx::MaskVersion_t * maskVersion = applicationProgram->getMaskVersion();
    Q_ASSERT(maskVersion);

    int row = 0;
    setRowCount(0);
    clearContents();

    for (const Project::v20::knx::ApplicationProgramStatic_Code_AbsoluteSegment_t * absoluteSegment : code->AbsoluteSegment) {
        Q_ASSERT(absoluteSegment);
        if (absoluteSegment->Id == comObjectTable->CodeSegment) {
            uint32_t address = (absoluteSegment->UserMemory == "true" ? 0x80000000 : 0) + absoluteSegment->Address.toUShort() + comObjectTable->Offset.toUInt();

            switch (maskVersion->MaskVersion.toUShort()) {
            case 0x701: {
                QTableWidgetItem * item;

                // Current Size
                insertRow(row);
                setItem(row, 0, new QTableWidgetItem("Current Size"));
                item = new QTableWidgetItem();
                item->setData(Qt::DisplayRole, address);
                setItem(row, 23, item);
                item = new QTableWidgetItem();
                item->setData(Qt::DisplayRole, 1);
                setItem(row, 24, item);
                address += 1;
                row++;

                // RAM Flags Table Pointer
                insertRow(row);
                setItem(row, 0, new QTableWidgetItem("RAM Flags Table Pointer"));
                item = new QTableWidgetItem();
                item->setData(Qt::DisplayRole, address);
                setItem(row, 23, item);
                item = new QTableWidgetItem();
                item->setData(Qt::DisplayRole, 2);
                setItem(row, 24, item);
                address += 2;
                row++;

                // Group Object Descriptor
                for (const Project::v20::knx::ComObject_t * comObject : comObjectTable->ComObject) {
                    insertRow(row);
                    setItem(row, 0, new QTableWidgetItem("Group Object Descriptor"));
                    setItem(row, 1, new QTableWidgetItem(comObject->Id));
                    setItem(row, 2, new QTableWidgetItem(comObject->Name));
                    setItem(row, 3, new QTableWidgetItem(comObject->Text));
                    setItem(row, 4, new QTableWidgetItem(comObject->Number));
                    setItem(row, 5, new QTableWidgetItem(comObject->FunctionText));
                    setItem(row, 6, new QTableWidgetItem(comObject->Priority));
                    setItem(row, 7, new QTableWidgetItem(comObject->ObjectSize));
                    setItem(row, 8, new QTableWidgetItem(comObject->ReadFlag));
                    setItem(row, 9, new QTableWidgetItem(comObject->WriteFlag));
                    setItem(row, 10, new QTableWidgetItem(comObject->CommunicationFlag));
                    setItem(row, 11, new QTableWidgetItem(comObject->TransmitFlag));
                    setItem(row, 12, new QTableWidgetItem(comObject->UpdateFlag));
                    setItem(row, 13, new QTableWidgetItem(comObject->ReadOnInitFlag));
                    setItem(row, 14, new QTableWidgetItem(comObject->DatapointType.join(", ")));
                    setItem(row, 15, new QTableWidgetItem(comObject->InternalDescription));
                    setItem(row, 16, new QTableWidgetItem(comObject->SecurityRequired));
                    setItem(row, 17, new QTableWidgetItem(comObject->MayRead));
                    setItem(row, 18, new QTableWidgetItem(comObject->ReadFlagLocked));
                    setItem(row, 19, new QTableWidgetItem(comObject->WriteFlagLocked));
                    setItem(row, 20, new QTableWidgetItem(comObject->TransmitFlagLocked));
                    setItem(row, 21, new QTableWidgetItem(comObject->UpdateFlagLocked));
                    setItem(row, 22, new QTableWidgetItem(comObject->ReadOnInitFlagLocked));

                    item = new QTableWidgetItem();
                    item->setData(Qt::DisplayRole, address + 4 * comObject->Number.toUInt());
                    setItem(row, 23, item);
                    item = new QTableWidgetItem();
                    item->setData(Qt::DisplayRole, 4);
                    setItem(row, 24, item);
                    row++;
                }
            }
            break;
            case 0x705:
                break;
            case 0x7B0: // IP Interface/Router Email and Time server function
                break;
            case 0x91A: // KNX IP Router
                break;
            }
        }
    }

    sortByColumn(23, Qt::AscendingOrder); // sort by address
    resizeColumnsToContents();
}

void MemoryComObjectTableView::on_itemSelectionChanged()
{
    const int row = currentRow();

    const bool userMemory = item(row, 23)->data(Qt::DisplayRole).toUInt() & 0x80000000;
    const KNX::Memory_Address address = item(row, 23)->data(Qt::DisplayRole).toUInt() & ~0x80000000;
    const KNX::Memory_Address size = item(row, 24)->data(Qt::DisplayRole).toUInt();

    Q_ASSERT(deviceData);
    Q_EMIT deviceData->memory_selected(userMemory, address, size);
}
