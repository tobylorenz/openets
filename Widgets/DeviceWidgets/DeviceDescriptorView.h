#pragma once

#include <QComboBox>
#include <QMainWindow>
#include <QTableWidget>

#include <Connection/Connection.h>
#include <Project/v20/knx/MasterData_MaskVersions_t.h>

/**
 * Device Descriptor View
 *
 * Services:
 * - A_DeviceDescriptor_InfoReport (System Broadcast) @todo not to be implemented here.
 * - A_DeviceDescriptor_Read (Individual)
 */
class DeviceDescriptorView : public QMainWindow
{
    Q_OBJECT

public:
    explicit DeviceDescriptorView(QWidget * parent = nullptr);
    virtual ~DeviceDescriptorView();

    void setConnection(Connection * connection);
    void setDevice(DeviceData * deviceData);

private Q_SLOTS:
    void deviceDescriptorChanged(const KNX::Descriptor_Type descriptor_type, const std::vector<uint8_t> & device_descriptor);
    void readRequest();

private:
    /* elements */
    QComboBox * deviceDescriptorComboBox{nullptr};
    QTableWidget * deviceDescriptorTableWidget{nullptr};

    /* connection */
    Connection * connection{nullptr};
    Project::v20::knx::MasterData_MaskVersions_t * etsMaskVersions{nullptr};

    /* device */
    DeviceData * deviceData{nullptr};

    int getRow(const KNX::Descriptor_Type descriptor_type);
};
