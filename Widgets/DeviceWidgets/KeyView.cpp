#include "KeyView.h"

KeyView::KeyView(QWidget * parent) :
    QWidget(parent)
{
    setWindowTitle("Key Write");
}

KeyView::~KeyView()
{
}

void KeyView::setConnection(Connection * connection)
{
    Q_ASSERT(connection);
    this->connection = connection;

    Q_ASSERT(connection->getStack());
    //connect(connection->getStack(), &Stack::A_Key_Write_ind, this, &KeyView::A_Key_Write_ind);
    //connect(connection->getStack(), &Stack::A_Key_Write_Acon, this, &KeyView::A_Key_Write_Acon);
}

void KeyView::setDevice(DeviceData * device)
{
    Q_ASSERT(device);
    this->device = device;
}
