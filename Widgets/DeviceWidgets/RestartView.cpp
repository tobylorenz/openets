#include "RestartView.h"

#include <QBoxLayout>
#include <QDebug>
#include <QLabel>
#include <QMessageBox>
#include <QTableWidgetItem>

RestartView::RestartView(QWidget * parent) :
    QWidget(parent)
{
    setWindowTitle("Device Descriptor");

    /* elements */
    restartTypeGroupBox = new QGroupBox(tr("Restart Type"), this);
    basicRestartRadioButton = new QRadioButton(tr("Basic Restart"), this);
    basicRestartRadioButton->setChecked(true);
    masterRestartRadioButton = new QRadioButton(tr("Master Restart"), this);
    masterRestartRadioButton->setChecked(false);
    eraseCodeSpinBox = new QSpinBox(this);
    eraseCodeSpinBox->setMinimum(0);
    eraseCodeSpinBox->setMaximum(255);
    eraseCodeSpinBox->setEnabled(false);
    channelNumberSpinBox = new QSpinBox(this);
    channelNumberSpinBox->setMinimum(0);
    channelNumberSpinBox->setMaximum(255);
    channelNumberSpinBox->setEnabled(false);
    restartRequestPushButton = new QPushButton(tr("Restart Request"), this);

    /* layout */
    QVBoxLayout * vBoxLayout = new QVBoxLayout();
    QHBoxLayout * hBoxLayoutRestartType = new QHBoxLayout();
    QHBoxLayout * hBoxLayoutEraseCode = new QHBoxLayout();
    QHBoxLayout * hBoxLayoutChannelNumber = new QHBoxLayout();
    setLayout(vBoxLayout);

    /* assign elements to layout */
    hBoxLayoutRestartType->addWidget(basicRestartRadioButton);
    hBoxLayoutRestartType->addWidget(masterRestartRadioButton);
    restartTypeGroupBox->setLayout(hBoxLayoutRestartType);
    vBoxLayout->addWidget(restartTypeGroupBox);
    hBoxLayoutEraseCode->addWidget(new QLabel(tr("Erase Code:")));
    hBoxLayoutEraseCode->addWidget(eraseCodeSpinBox);
    vBoxLayout->addLayout(hBoxLayoutEraseCode);
    hBoxLayoutChannelNumber->addWidget(new QLabel(tr("Channel Number:")));
    hBoxLayoutChannelNumber->addWidget(channelNumberSpinBox);
    vBoxLayout->addLayout(hBoxLayoutChannelNumber);
    vBoxLayout->addWidget(restartRequestPushButton);

    /* connections */
    connect(basicRestartRadioButton, &QPushButton::clicked, this, &RestartView::on_basicRestartRadioButton_clicked);
    connect(masterRestartRadioButton, &QPushButton::clicked, this, &RestartView::on_masterRestartRadioButton_clicked);
    connect(restartRequestPushButton, &QPushButton::clicked, this, &RestartView::on_restartRequestPushButton_clicked);
}

RestartView::~RestartView()
{
}

void RestartView::setConnection(Connection * connection)
{
    Q_ASSERT(connection);
    this->connection = connection;

    Q_ASSERT(connection->getStack());
    connect(connection->getStack(), &Stack::A_Restart_ind, this, &RestartView::A_Restart_ind);
}

void RestartView::setDevice(DeviceData * device)
{
    Q_ASSERT(device);
    this->device = device;
}

void RestartView::A_Restart_ind(const KNX::Restart_Erase_Code /*erase_code*/, const uint8_t /*channel_number*/, const KNX::Priority /*priority*/, const KNX::Hop_Count_Type /*hop_count_type*/, const KNX::ASAP_Individual & /*asap*/)
{
    // do nothing
}

void RestartView::on_basicRestartRadioButton_clicked()
{
    eraseCodeSpinBox->setEnabled(false);
    channelNumberSpinBox->setEnabled(false);
}

void RestartView::on_masterRestartRadioButton_clicked()
{
    eraseCodeSpinBox->setEnabled(true);
    channelNumberSpinBox->setEnabled(true);
}

void RestartView::on_restartRequestPushButton_clicked()
{
    Q_ASSERT(connection);
    Q_ASSERT(connection->getStack());
    Q_ASSERT(device);

    const KNX::Restart_Erase_Code erase_code = static_cast<KNX::Restart_Erase_Code>(eraseCodeSpinBox->value());

    uint8_t restart_type{0};
    if (basicRestartRadioButton->isChecked()) {
        restart_type = 0;
    }
    if (masterRestartRadioButton->isChecked()) {
        restart_type = 1;
    }

    const KNX::ASAP_Individual asap { device->address, false };

    /* send request */
    connection->getStack()->A_Restart_req(KNX::Ack_Request::dont_care, channelNumberSpinBox->value(), erase_code, KNX::Priority::low, KNX::Network_Layer_Parameter, restart_type, asap, [](const KNX::Status /*a_status*/) -> void {
        // do nothing
    });
}
