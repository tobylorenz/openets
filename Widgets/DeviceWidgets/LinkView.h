#pragma once

#include <QWidget>

#include <Connection/Connection.h>

/**
 * Link View
 *
 * Services:
 * - A_Link_Read (Individual)
 * - A_Link_Write (Individual)
 */
class LinkView : public QWidget
{
    Q_OBJECT

public:
    explicit LinkView(QWidget * parent = nullptr);
    virtual ~LinkView();

    void setConnection(Connection * connection);
    void setDevice(DeviceData * device);

private:
    Connection * connection{nullptr};
    DeviceData * device{nullptr};
};
