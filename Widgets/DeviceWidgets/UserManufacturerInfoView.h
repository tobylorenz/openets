#pragma once

#include <QWidget>

#include <Connection/Connection.h>

/**
 * User Manufacturer Info View
 *
 * Services:
 * - A_UserManufacturerInfo_Read (Connected)
 */
class UserManufacturerInfoView : public QWidget
{
    Q_OBJECT

public:
    explicit UserManufacturerInfoView(QWidget * parent = nullptr);
    virtual ~UserManufacturerInfoView();

    void setConnection(Connection * connection);
    void setDevice(DeviceData * device);

private:
    Connection * connection{nullptr};
    DeviceData * device{nullptr};
};
