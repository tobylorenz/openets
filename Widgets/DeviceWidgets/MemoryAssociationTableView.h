#pragma once

#include <QTableWidget>

#include <Connection/Connection.h>

/**
 * Memory Association Table View
 */
class MemoryAssociationTableView : public QTableWidget
{
    Q_OBJECT

public:
    explicit MemoryAssociationTableView(QWidget * parent = nullptr);
    virtual ~MemoryAssociationTableView();

    void setDevice(DeviceData * deviceData);

private Q_SLOTS:
    void on_itemSelectionChanged();

private:
    /* device data */
    DeviceData * deviceData{nullptr};
};
