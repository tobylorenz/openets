#include "DeviceDescriptorView.h"

#include <QAction>
#include <QDebug>
#include <QLabel>
#include <QMessageBox>
#include <QTableWidgetItem>
#include <QToolBar>

#include "KNX/yaml_output.h"
#include <Project/v20/knx/MasterData_t.h>

DeviceDescriptorView::DeviceDescriptorView(QWidget * parent) :
    QMainWindow(parent)
{
    setWindowTitle("Device Descriptor");

    /* create Actions */
    deviceDescriptorComboBox = new QComboBox(this);
    deviceDescriptorComboBox->setEditable(true);
    deviceDescriptorComboBox->addItem("Device Descriptor Type 0 (mask version)", 0);
    deviceDescriptorComboBox->addItem("Device Descriptor Type 2", 2);
    QAction * readRequestAction = new QAction(tr("Read Request"));
    connect(readRequestAction, &QAction::triggered, this, &DeviceDescriptorView::readRequest);

    /* create ToolBars */
    auto * toolBar = new QToolBar(this);
    toolBar->addWidget(new QLabel(tr("Descriptor:")));
    toolBar->addWidget(deviceDescriptorComboBox);
    toolBar->addAction(readRequestAction);
    addToolBar(toolBar);

    /* create Table Widget */
    deviceDescriptorTableWidget = new QTableWidget(this);
    deviceDescriptorTableWidget->setColumnCount(3);
    QStringList headers = { "Type", "Value", "Name" };
    deviceDescriptorTableWidget->setHorizontalHeaderLabels(headers);

    /* assign elements to layout */
    setCentralWidget(deviceDescriptorTableWidget);
}

DeviceDescriptorView::~DeviceDescriptorView()
{
}

void DeviceDescriptorView::setConnection(Connection * connection)
{
    Q_ASSERT(connection);
    this->connection = connection;

    Q_ASSERT(connection->getEtsKnx());
    Project::v20::knx::MasterData_t * etsMasterData = connection->getEtsKnx()->MasterData;
    Q_ASSERT(etsMasterData);
    etsMaskVersions = etsMasterData->MaskVersions;
}

void DeviceDescriptorView::setDevice(DeviceData * deviceData)
{
    Q_ASSERT(deviceData);
    this->deviceData = deviceData;
    connect(deviceData, &DeviceData::device_descriptor_changed, this, &DeviceDescriptorView::deviceDescriptorChanged);

    /* populate from device data */
    for (const KNX::Descriptor_Type descriptor_type : deviceData->device_descriptors.keys()) {
        deviceDescriptorChanged(descriptor_type, deviceData->device_descriptors[descriptor_type]);
    }
}

void DeviceDescriptorView::deviceDescriptorChanged(const KNX::Descriptor_Type descriptor_type, const std::vector<uint8_t> & device_descriptor)
{
    /* get row */
    int row = getRow(descriptor_type);

    /* set item */
    deviceDescriptorTableWidget->item(row, 1)->setText(QString::fromStdString(to_string(device_descriptor)));
    switch (descriptor_type) {
    case 0: {
        Project::xs::ID id = QString("MV-%1%2").arg(static_cast<uint16_t>(device_descriptor[0]), 2, 16, QLatin1Char('0')).arg(static_cast<uint16_t>(device_descriptor[1]), 2, 16, QLatin1Char('0')).toUpper();
        if (etsMaskVersions->MaskVersion.contains(id)) {
            deviceDescriptorTableWidget->item(row, 2)->setText(etsMaskVersions->MaskVersion[id]->Name);
        }
    }
    break;
    case 2:
        // @todo Device Descriptor 2
        break;
    default:
        break;
    }

    /* sort table */
    deviceDescriptorTableWidget->sortItems(0);
}

void DeviceDescriptorView::readRequest()
{
    uint8_t descriptor_type = deviceDescriptorComboBox->currentData().toUInt();
    deviceData->read_device_descriptor(descriptor_type);
}

int DeviceDescriptorView::getRow(const KNX::Descriptor_Type descriptor_type)
{
    QString str = QString("%1").arg(descriptor_type);

    /* find row */
    int row;
    for (row = 0; row < deviceDescriptorTableWidget->rowCount(); row++) {
        if (deviceDescriptorTableWidget->item(row, 0)->data(Qt::DisplayRole).toString() == str) {
            return row;
        }
    }

    /* create row */
    row = deviceDescriptorTableWidget->rowCount();
    deviceDescriptorTableWidget->setRowCount(row + 1);
    deviceDescriptorTableWidget->setItem(row, 0, new QTableWidgetItem(str));
    deviceDescriptorTableWidget->setItem(row, 1, new QTableWidgetItem());
    deviceDescriptorTableWidget->setItem(row, 2, new QTableWidgetItem());
    return row;
}
