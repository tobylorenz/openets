#pragma once

#include <QTabWidget>

#include <Connection/Connection.h>

/**
 * Device Resources View
 */
class DeviceResourcesView : public QTabWidget
{
    Q_OBJECT

public:
    explicit DeviceResourcesView(QWidget * parent = nullptr);
    virtual ~DeviceResourcesView();

    void setConnection(Connection * connection);
    void setDevice(DeviceData * device);

private:
    Connection * connection{nullptr};
    DeviceData * device{nullptr};
};
