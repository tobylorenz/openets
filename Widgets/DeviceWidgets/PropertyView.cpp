#include "PropertyView.h"

#include <QAction>
#include <QBoxLayout>
#include <QDebug>
#include <QLabel>
#include <QPushButton>
#include <QToolBar>
#include <QVBoxLayout>

#include <KNX/03/05/01/Unknown_Interface_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>
#include <KNX/yaml_output.h>
#include <Project/v20/knx/MasterData_InterfaceObjectProperties_InterfaceObjectProperty_t.h>
#include <Project/v20/knx/MasterData_InterfaceObjectTypes_InterfaceObjectType_t.h>
#include <Project/v20/knx/MasterData_PropertyDataTypes_PropertyDataType_t.h>
#include <Project/v20/knx/MasterData_t.h>

PropertyView::PropertyView(QWidget * parent) :
    QMainWindow(parent)
{
    setWindowTitle("Property Value/Description Read/Write");

    /* create Actions */
    startDiscoveryAction = new QAction("Start discovery", this);

    /* create ToolBars */
    auto * toolBar = new QToolBar(this);
    toolBar->addAction(startDiscoveryAction);
    addToolBar(toolBar);

    /* elements */
    dataPropertiesTableWidget = new QTableWidget(this);
    dataPropertiesTableWidget->setColumnCount(14);
    dataPropertiesTableWidget->setHorizontalHeaderLabels({ "Object Index", "Object Type", "Object Name", "Property Index", "Property Id", "Property Name", "Write Enable", "Property Type", "Property Type Name", "R/W Access Level", "Request", "Max Nr of Elem", "Data", "Text" });

    functionPropertiesTableWidget = new QTableWidget(this);
    functionPropertiesTableWidget->setColumnCount(14);
    functionPropertiesTableWidget->setHorizontalHeaderLabels({ "Object Index", "Object Type", "Object Name", "Property Index", "Property Id", "Property Name", "Write Enable", "Property Type", "Property Type Name", "R/W Access Level", "Request", "Input Data", "Return Code", "Output Data" });

    /* layout */
    QVBoxLayout * vBoxLayout = new QVBoxLayout();
    setLayout(vBoxLayout);

    /* assign elements to layout */
    vBoxLayout->addWidget(new QLabel("Data Properties:"));
    vBoxLayout->addWidget(dataPropertiesTableWidget);
    vBoxLayout->addWidget(new QLabel("Function Properties:"));
    vBoxLayout->addWidget(functionPropertiesTableWidget);

    /* central widget */
    QWidget * centralWidget = new QWidget();
    centralWidget->setLayout(vBoxLayout);
    setCentralWidget(centralWidget);
}

PropertyView::~PropertyView()
{
}

void PropertyView::setConnection(Connection * connection)
{
    Q_ASSERT(connection);
    this->connection = connection;

    Q_ASSERT(connection->getEtsKnx());
    Project::v20::knx::MasterData_t * etsMasterData = connection->getEtsKnx()->MasterData;
    Q_ASSERT(etsMasterData);
    etsInterfaceObjectProperties = etsMasterData->InterfaceObjectProperties;
    Q_ASSERT(etsInterfaceObjectProperties);
    etsInterfaceObjectTypes = etsMasterData->InterfaceObjectTypes;
    Q_ASSERT(etsInterfaceObjectTypes);
    etsPropertyDataTypes = etsMasterData->PropertyDataTypes;
    Q_ASSERT(etsPropertyDataTypes);
}

void PropertyView::setDevice(DeviceData * deviceData)
{
    Q_ASSERT(deviceData);
    this->deviceData = deviceData;

    connect(startDiscoveryAction, &QAction::triggered, deviceData, &DeviceData::start_property_discovery);
    connect(deviceData, &DeviceData::property_updated, this, &PropertyView::propertyUpdated);

    /* populate from device data */
    for (auto object_it : deviceData->interface_objects.objects) {
        const KNX::Object_Index object_index = object_it.first;
        std::shared_ptr<KNX::Interface_Object> interface_object = object_it.second;
        for (auto property_it : interface_object->properties) {
            const KNX::Property_Index property_index = property_it.first;
            std::shared_ptr<KNX::Property> property = property_it.second;
            propertyUpdated(object_index, property_index);
        }
    }
}

void PropertyView::propertyUpdated(const KNX::Object_Index object_index, const KNX::Property_Index property_index)
{
    /* Interface Object */
    if (!deviceData->interface_objects.objects.count(object_index)) {
        return;
    }
    std::shared_ptr<KNX::Interface_Object> interface_object = deviceData->interface_objects.objects[object_index];
    Q_ASSERT(interface_object);

    /* Property */
    if (!interface_object->properties.count(property_index)) {
        return;
    }
    std::shared_ptr<KNX::Property> property = interface_object->properties[property_index];
    Q_ASSERT(property);

    /* check for unknown interface object type */
    bool unknown_interface_object{false};
    if (std::dynamic_pointer_cast<KNX::Unknown_Interface_Object>(interface_object)) {
        unknown_interface_object = true;
    }

    if (property->description.property_datatype != KNX::PDT_FUNCTION) {
        std::shared_ptr<KNX::Data_Property> data_property = std::dynamic_pointer_cast<KNX::Data_Property>(property);
        Q_ASSERT(data_property);
        /* Data Property */
        int row = getDataPropertyRow(object_index, property_index);
        int column = 0;
        // Object Index
        Q_ASSERT(column == ObjectIndexColumn);
        dataPropertiesTableWidget->item(row, column++)->setData(Qt::DisplayRole, object_index);
        // Object Type
        dataPropertiesTableWidget->item(row, column++)->setData(Qt::DisplayRole, interface_object->object_type);
        if (unknown_interface_object) {
            // Object Name
            dataPropertiesTableWidget->item(row, column++)->setText("");
        } else {
            // Object Name
            dataPropertiesTableWidget->item(row, column++)->setText(getObjectName(interface_object->object_type));
        }
        // Property Index
        dataPropertiesTableWidget->item(row, column++)->setData(Qt::DisplayRole, property_index);
        // Property Id
        Q_ASSERT(column == PropertyIdColumn);
        dataPropertiesTableWidget->item(row, column++)->setData(Qt::DisplayRole, property->description.property_id);
        if (unknown_interface_object && (property->description.property_id > 50)) {
            // Property Name
            dataPropertiesTableWidget->item(row, column++)->setText("");
        } else {
            // Property Name
            dataPropertiesTableWidget->item(row, column++)->setText(getPropertyName(interface_object->object_type, property->description.property_id));
        }
        // Write Enable
        dataPropertiesTableWidget->item(row, column++)->setText(QString("%1").arg(property->description.write_enable));
        // Property Type
        dataPropertiesTableWidget->item(row, column++)->setText(QString("%1").arg(static_cast<const uint8_t>(property->description.property_datatype)));
        // Property Type Name
        dataPropertiesTableWidget->item(row, column++)->setText(getTypeName(property->description.property_datatype));
        // Access
        dataPropertiesTableWidget->item(row, column++)->setText(QString("%1 / %2").arg(property->description.access.read_level).arg(property->description.access.write_level));
        // Request
        Q_ASSERT(column == RequestColumn);
        column++;
        // Max Nr of Elem
        dataPropertiesTableWidget->item(row, column++)->setText(QString("%1").arg(property->description.max_nr_of_elem));
        // Data (1..N)
        Q_ASSERT(column == DataColumn);
        dataPropertiesTableWidget->item(row, column++)->setText(QString::fromStdString(to_string(data_property->toData())));
        // Text
        dataPropertiesTableWidget->item(row, column++)->setText(QString::fromStdString(data_property->text()));
        dataPropertiesTableWidget->resizeColumnsToContents();
    } else {
        /* Function Property */
        int row = getFunctionPropertyRow(object_index, property_index);
        int column = 0;
        // Object Index
        Q_ASSERT(column == ObjectIndexColumn);
        functionPropertiesTableWidget->item(row, column++)->setData(Qt::DisplayRole, object_index);
        // Object Type
        functionPropertiesTableWidget->item(row, column++)->setData(Qt::DisplayRole, interface_object->object_type);
        if (unknown_interface_object) {
            // Object Name
            functionPropertiesTableWidget->item(row, column++)->setText("");
        } else {
            // Object Name
            functionPropertiesTableWidget->item(row, column++)->setText(getObjectName(interface_object->object_type));
        }
        // Property Index
        functionPropertiesTableWidget->item(row, column++)->setData(Qt::DisplayRole, property_index);
        // Property Id
        Q_ASSERT(column == PropertyIdColumn);
        functionPropertiesTableWidget->item(row, column++)->setData(Qt::DisplayRole, property->description.property_id);
        if (unknown_interface_object && (property->description.property_id > 50)) {
            // Property Name
            functionPropertiesTableWidget->item(row, column++)->setText("");
        } else {
            // Property Name
            functionPropertiesTableWidget->item(row, column++)->setText(getPropertyName(interface_object->object_type, property->description.property_id));
        }
        // Write Enable
        functionPropertiesTableWidget->item(row, column++)->setText(QString("%1").arg(property->description.write_enable));
        // Property Type
        functionPropertiesTableWidget->item(row, column++)->setText(QString("%1").arg(static_cast<const uint8_t>(property->description.property_datatype)));
        // Property Type Name
        functionPropertiesTableWidget->item(row, column++)->setText(getTypeName(property->description.property_datatype));
        // Access
        functionPropertiesTableWidget->item(row, column++)->setText(QString("%1 / %2").arg(property->description.access.read_level).arg(property->description.access.write_level));
        // Request
        Q_ASSERT(column == RequestColumn);
        column++;
        // Input Data
        Q_ASSERT(column == InputDataColumn);
        //functionPropertiesTableWidget->item(row, column++)->setText(QString::fromStdString(to_string(property.input_data)));
        column++;
        // Return Code
        Q_ASSERT(column == ReturnCodeColumn);
        //functionPropertiesTableWidget->item(row, column++)->setText(QString("%1").arg(property.return_code));
        column++;
        // Output Data
        Q_ASSERT(column == OutputDataColumn);
        //functionPropertiesTableWidget->item(row, column++)->setText(QString::fromStdString(to_string(property.output_data)));
        column++;
        functionPropertiesTableWidget->resizeColumnsToContents();
    }
}

int PropertyView::getDataPropertyRow(const KNX::Object_Index object_index, const KNX::Property_Index property_index)
{
    /* search row */
    for (int row = 0; row < dataPropertiesTableWidget->rowCount(); ++row) {
        if ((dataPropertiesTableWidget->item(row, ObjectIndexColumn)->data(Qt::DisplayRole) == object_index) &&
                (dataPropertiesTableWidget->item(row, PropertyIndexColumn)->data(Qt::DisplayRole) == property_index)
           ) {
            return row;
        }
    }

    /* create new row */
    std::shared_ptr<KNX::Interface_Object> interface_object = deviceData->interface_objects.objects[object_index];
    const KNX::Object_Type object_type = interface_object->interface_object_type()->object_type;
    std::shared_ptr<KNX::Property> property = interface_object->properties[property_index];
    const KNX::Property_Id property_id = property->description.property_id;
    std::shared_ptr<KNX::Data_Property> data_property = std::dynamic_pointer_cast<KNX::Data_Property>(property);
    Q_ASSERT(data_property);
    int row = dataPropertiesTableWidget->rowCount();
    dataPropertiesTableWidget->setRowCount(row + 1);
    int column = 0;
    QTableWidgetItem * item;
    // Object Index
    Q_ASSERT(column == ObjectIndexColumn);
    item = new QTableWidgetItem();
    item->setData(Qt::DisplayRole, object_index);
    dataPropertiesTableWidget->setItem(row, column++, item);
    // Object Type
    item = new QTableWidgetItem();
    item->setData(Qt::DisplayRole, object_type);
    dataPropertiesTableWidget->setItem(row, column++, item);
    // Object Name
    item = new QTableWidgetItem();
    item->setText(getObjectName(object_type));
    dataPropertiesTableWidget->setItem(row, column++, item);
    // Property Index
    item = new QTableWidgetItem();
    item->setData(Qt::DisplayRole, property_index);
    dataPropertiesTableWidget->setItem(row, column++, item);
    // Property Id
    Q_ASSERT(column == PropertyIdColumn);
    item = new QTableWidgetItem();
    item->setData(Qt::DisplayRole, property_id);
    dataPropertiesTableWidget->setItem(row, column++, item);
    // Property Name
    item = new QTableWidgetItem();
    item->setText(getPropertyName(object_type, property_id));
    dataPropertiesTableWidget->setItem(row, column++, item);
    // Write Enable
    item = new QTableWidgetItem();
    item->setText(QString("%1").arg(property->description.write_enable));
    dataPropertiesTableWidget->setItem(row, column++, item);
    // Property Type
    item = new QTableWidgetItem();
    item->setData(Qt::DisplayRole, static_cast<uint8_t>(property->description.property_datatype));
    dataPropertiesTableWidget->setItem(row, column++, item);
    // Property Type Name
    item = new QTableWidgetItem();
    item->setText(getTypeName(property->description.property_datatype));
    dataPropertiesTableWidget->setItem(row, column++, item);
    // Access
    item = new QTableWidgetItem();
    item->setText(QString("%1 / %2").arg(property->description.access.read_level).arg(property->description.access.write_level));
    dataPropertiesTableWidget->setItem(row, column++, item);
    // Request
    Q_ASSERT(column == RequestColumn);
    QPushButton * readButton = new QPushButton(tr("Read"));
    connect(readButton, &QPushButton::clicked, [this, object_index, property_id]() {
        deviceData->read_data_property(object_index, property_id);
    });
    QPushButton * writeButton = new QPushButton(tr("Write"));
    writeButton->setEnabled(false); // ... till this is implemented.
    //connect(writeButton, &QPushButton::clicked, [this, object_index, property_id]() {
    // const PropertyValue_Nr_Of_Elem nr_of_elem = 1; // @todo can we write max_nr_of_elem?
    // const PropertyValue_Start_Index start_index = 1;
    // @todo deviceData->A_PropertyValue_Write_req(Ack_Request::dont_care, Priority::low, Network_Layer_Parameter, object_index, property_id, nr_of_elem, start_index, data);
    //});
    QHBoxLayout * layout = new QHBoxLayout();
    layout->addWidget(readButton);
    layout->addWidget(writeButton);
    QWidget * requestButtonWidget = new QWidget();
    requestButtonWidget->setLayout(layout);
    dataPropertiesTableWidget->setCellWidget(row, column++, requestButtonWidget);
    // Max Nr of Elem
    item = new QTableWidgetItem();
    item->setData(Qt::DisplayRole, static_cast<uint16_t>(property->description.max_nr_of_elem));
    dataPropertiesTableWidget->setItem(row, column++, item);
    // Data
    Q_ASSERT(column == DataColumn);
    item = new QTableWidgetItem();
    item->setText(QString::fromStdString(to_string(data_property->toData())));
    dataPropertiesTableWidget->setItem(row, column++, item);
    // Text
    item = new QTableWidgetItem();
    item->setText(QString::fromStdString(data_property->text()));
    dataPropertiesTableWidget->setItem(row, column++, item);

    /* sort */
    dataPropertiesTableWidget->sortItems(PropertyIndexColumn);
    dataPropertiesTableWidget->sortItems(ObjectIndexColumn);

    /* search row */
    for (int row = 0; row < dataPropertiesTableWidget->rowCount(); ++row) {
        if ((dataPropertiesTableWidget->item(row, ObjectIndexColumn)->data(Qt::DisplayRole) == object_index) &&
                (dataPropertiesTableWidget->item(row, PropertyIndexColumn)->data(Qt::DisplayRole) == property_index)
           ) {
            return row;
        }
    }
    return 0;
}

int PropertyView::getFunctionPropertyRow(const KNX::Object_Index object_index, const KNX::Property_Id property_index)
{
    /* search row */
    for (int row = 0; row < functionPropertiesTableWidget->rowCount(); ++row) {
        if ((functionPropertiesTableWidget->item(row, ObjectIndexColumn)->data(Qt::DisplayRole) == object_index) &&
                (functionPropertiesTableWidget->item(row, PropertyIndexColumn)->data(Qt::DisplayRole) == property_index)
           ) {
            return row;
        }
    }

    /* create new row */
    std::shared_ptr<KNX::Interface_Object> interface_object = deviceData->interface_objects.objects[object_index];
    const KNX::Object_Type object_type = interface_object->interface_object_type()->object_type;
    std::shared_ptr<KNX::Property> property = interface_object->properties[property_index];
    const KNX::Property_Id property_id = property->description.property_id;
    int row = functionPropertiesTableWidget->rowCount();
    functionPropertiesTableWidget->setRowCount(row + 1);
    int column = 0;
    QTableWidgetItem * item;
    // Object Index
    Q_ASSERT(column == ObjectIndexColumn);
    item = new QTableWidgetItem();
    item->setData(Qt::DisplayRole, object_index);
    functionPropertiesTableWidget->setItem(row, column++, item);
    // Object Type
    item = new QTableWidgetItem();
    item->setData(Qt::DisplayRole, object_type);
    functionPropertiesTableWidget->setItem(row, column++, item);
    // Object Name
    item = new QTableWidgetItem();
    item->setText(getObjectName(object_index));
    functionPropertiesTableWidget->setItem(row, column++, item);
    // Property Index
    item = new QTableWidgetItem();
    item->setData(Qt::DisplayRole, property_index);
    functionPropertiesTableWidget->setItem(row, column++, item);
    // Property Id
    Q_ASSERT(column == PropertyIdColumn);
    item = new QTableWidgetItem();
    item->setData(Qt::DisplayRole, property_id);
    functionPropertiesTableWidget->setItem(row, column++, item);
    // Property Name
    item = new QTableWidgetItem();
    item->setText(getPropertyName(object_index, property_id));
    functionPropertiesTableWidget->setItem(row, column++, item);
    // Write Enable
    item = new QTableWidgetItem();
    item->setText(QString("%1").arg(property->description.write_enable));
    functionPropertiesTableWidget->setItem(row, column++, item);
    // Property Type
    item = new QTableWidgetItem();
    item->setData(Qt::DisplayRole, static_cast<uint8_t>(property->description.property_datatype));
    functionPropertiesTableWidget->setItem(row, column++, item);
    // Property Type Name
    item = new QTableWidgetItem();
    item->setText(getTypeName(property->description.property_datatype));
    functionPropertiesTableWidget->setItem(row, column++, item);
    // Access
    item = new QTableWidgetItem();
    item->setText(QString("%1 / %2").arg(property->description.access.read_level).arg(property->description.access.write_level));
    functionPropertiesTableWidget->setItem(row, column++, item);
    // Request
    Q_ASSERT(column == RequestColumn);
    QPushButton * commandButton = new QPushButton(tr("Command"));
    commandButton->setEnabled(false); // ... till this is implemented.
#if 0
    connect(commandButton, &QPushButton::clicked, [this, object_index, property_id]() {
    deviceData->A_FunctionPropertyCommand_req(Ack_Request::dont_care, Comm_Mode::Individual, data, Network_Layer_Parameter, object_index, Priority::low, property_id, [](const Status a_status){
    DeviceData * deviceData = device(asap.address);
    Q_ASSERT(deviceData);

    if (a_status == KNX::Status::ok) {
        std::shared_ptr<KNX::Interface_Object> interface_object = deviceData->interface_objects.object_by_type(object_index);
        Q_ASSERT(interface_object);

        std::shared_ptr<KNX::Property> property = interface_object->property_by_id(property_id);
        Q_ASSERT(property);

        property->description.property_id = property_id;
        const KNX::Property_Index property_index = interface_object->property_index(property);
        // @todo data
        Q_EMIT deviceData->property_updated(object_index, property_index);
    }

});
    });
#endif
    QPushButton * stateReadButton = new QPushButton(tr("State Read"));
    stateReadButton->setEnabled(false); // ... till this is implemented.
#if 0
    connect(stateReadButton, &QPushButton::clicked, [this, object_index, property_id]() {
    deviceData->A_FunctionPropertyState_Read_req(Ack_Request::dont_care, Comm_Mode::Individual, data, Network_Layer_Parameter, object_index, Priority::low, property_id, [](const Status a_status){
    DeviceData * deviceData = device(asap.address);
    Q_ASSERT(deviceData);

    if (a_status == KNX::Status::ok) {
        std::shared_ptr<KNX::Interface_Object> interface_object = deviceData->interface_objects.object_by_type(object_index);
        Q_ASSERT(interface_object);

        std::shared_ptr<KNX::Property> property = interface_object->property_by_id(property_id);
        Q_ASSERT(property);

        const KNX::Property_Index property_index = interface_object->property_index(property);
        property->description.property_id = property_id;
        // @todo data
        Q_EMIT deviceData->property_updated(object_index, property_index);
    }
});
    });
#endif
    QHBoxLayout * layout = new QHBoxLayout();
    layout->addWidget(commandButton);
    layout->addWidget(stateReadButton);
    QWidget * requestButtonWidget = new QWidget();
    requestButtonWidget->setLayout(layout);
    functionPropertiesTableWidget->setCellWidget(row, column++, requestButtonWidget);
    // Input Data
    Q_ASSERT(column == InputDataColumn);
    item = new QTableWidgetItem();
    //    item->setText(QString::fromStdString(to_string(property.input_data)));
    functionPropertiesTableWidget->setItem(row, column++, item);
    // Return Code
    Q_ASSERT(column == ReturnCodeColumn);
    item = new QTableWidgetItem();
    //    item->setText(QString("%1").arg(property.return_code));
    functionPropertiesTableWidget->setItem(row, column++, item);
    // Output Data
    Q_ASSERT(column == OutputDataColumn);
    item = new QTableWidgetItem();
    //    item->setText(QString::fromStdString(to_string(property.output_data)));
    functionPropertiesTableWidget->setItem(row, column++, item);

    /* sort */
    functionPropertiesTableWidget->sortItems(PropertyIndexColumn);
    functionPropertiesTableWidget->sortItems(ObjectIndexColumn);

    /* search row */
    for (int row = 0; row < functionPropertiesTableWidget->rowCount(); ++row) {
        if ((functionPropertiesTableWidget->item(row, ObjectIndexColumn)->data(Qt::DisplayRole) == object_index) &&
                (functionPropertiesTableWidget->item(row, PropertyIndexColumn)->data(Qt::DisplayRole) == property_index)
           ) {
            return row;
        }
    }
    return 0;
}

QString PropertyView::getObjectName(const KNX::Object_Type object_type) const
{
    if (!etsInterfaceObjectTypes) {
        return "";
    }

    /* IDs */
    QString object_type_id = QString("OT-%1").arg(object_type);

    if (etsInterfaceObjectTypes->InterfaceObjectType.contains(object_type_id)) {
        Project::v20::knx::MasterData_InterfaceObjectTypes_InterfaceObjectType_t * interfaceObjectType;
        interfaceObjectType = etsInterfaceObjectTypes->InterfaceObjectType[object_type_id];
        if (interfaceObjectType) {
            return interfaceObjectType->Name; // Text or Name?
        }
    }

    return "";
}

QString PropertyView::getPropertyName(const KNX::Object_Type object_type, const KNX::Property_Id property_id) const
{
    if (!etsInterfaceObjectProperties) {
        return "";
    }

    /* IDs */
    QString generic_property_id = QString("PID-G-%1").arg(property_id);
    QString object_property_id = QString("PID-%1-%2").arg(object_type).arg(property_id);

    Project::v20::knx::MasterData_InterfaceObjectProperties_InterfaceObjectProperty_t * interfaceObjectProperty;
    if (etsInterfaceObjectProperties->InterfaceObjectProperty.contains(generic_property_id)) {
        interfaceObjectProperty = etsInterfaceObjectProperties->InterfaceObjectProperty[generic_property_id];
        if (interfaceObjectProperty) {
            return interfaceObjectProperty->Name; // Text or Name?
        }
    }
    if (etsInterfaceObjectProperties->InterfaceObjectProperty.contains(object_property_id)) {
        interfaceObjectProperty = etsInterfaceObjectProperties->InterfaceObjectProperty[object_property_id];
        if (interfaceObjectProperty) {
            return interfaceObjectProperty->Name; // Text or Name?
        }
    }

    return "";
}

QString PropertyView::getTypeName(const KNX::Property_Type type) const
{
    if (!etsPropertyDataTypes) {
        return "";
    }

    /* IDs */
    QString property_type_id = QString("PDT-%1").arg(static_cast<uint8_t>(type));

    if (etsPropertyDataTypes->PropertyDataType.contains(property_type_id)) {
        Project::v20::knx::MasterData_PropertyDataTypes_PropertyDataType_t * propertyDataType;
        propertyDataType = etsPropertyDataTypes->PropertyDataType[property_type_id];
        if (propertyDataType) {
            return propertyDataType->Name;
        }
    }

    return "";
}
