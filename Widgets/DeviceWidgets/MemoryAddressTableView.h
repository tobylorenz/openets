#pragma once

#include <QTableWidget>

#include <Connection/Connection.h>

/**
 * Memory Address Table View
 */
class MemoryAddressTableView : public QTableWidget
{
    Q_OBJECT

public:
    explicit MemoryAddressTableView(QWidget * parent = nullptr);
    virtual ~MemoryAddressTableView();

    void setDevice(DeviceData * deviceData);

private Q_SLOTS:
    void on_itemSelectionChanged();

private:
    /* device data */
    DeviceData * deviceData{nullptr};
};
