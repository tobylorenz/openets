#pragma once

#include <QWidget>

#include <Connection/Connection.h>

/**
 * Key View
 *
 * Services:
 * - A_Key_Write (Connected)
 */
class KeyView : public QWidget
{
    Q_OBJECT

public:
    explicit KeyView(QWidget * parent = nullptr);
    virtual ~KeyView();

    void setConnection(Connection * connection);
    void setDevice(DeviceData * device);

private:
    Connection * connection{nullptr};
    DeviceData * device{nullptr};
};
