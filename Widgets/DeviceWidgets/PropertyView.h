#pragma once

#include <QAction>
#include <QMainWindow>
#include <QTableWidget>

#include <Connection/Connection.h>
#include <Project/v20/knx/MasterData_InterfaceObjectProperties_t.h>
#include <Project/v20/knx/MasterData_InterfaceObjectTypes_t.h>
#include <Project/v20/knx/MasterData_PropertyDataTypes_t.h>

/**
 * Property View
 *
 * Services:
 * - A_PropertyValue_Read (Individual)
 * - A_PropertyValue_Write (Individual)
 * - A_PropertyDescription_Read (Individual)
 * - A_FunctionPropertyCommand (Individual)
 * - A_FunctionPropertyState_Read (Individual)
 *
 * @todo Property descriptions should not be stored only in tableWidgets, but in model (project part) itself!
 */
class PropertyView : public QMainWindow
{
    Q_OBJECT

public:
    explicit PropertyView(QWidget * parent = nullptr);
    virtual ~PropertyView();

    void setConnection(Connection * connection);
    void setDevice(DeviceData * deviceData);

public Q_SLOTS:
    void propertyUpdated(const KNX::Object_Index object_index, const KNX::Property_Index property_index);

private:
    /* actions */
    QAction * startDiscoveryAction{nullptr};

    /* elements */
    QTableWidget * dataPropertiesTableWidget{nullptr};
    QTableWidget * functionPropertiesTableWidget{nullptr};

    /* connection */
    Connection * connection{nullptr};
    Project::v20::knx::MasterData_InterfaceObjectProperties_t * etsInterfaceObjectProperties{nullptr};
    Project::v20::knx::MasterData_InterfaceObjectTypes_t * etsInterfaceObjectTypes{nullptr};
    Project::v20::knx::MasterData_PropertyDataTypes_t * etsPropertyDataTypes{nullptr};

    /* device */
    DeviceData * deviceData{nullptr};

    enum Rows {
        /* generic columns */
        ObjectIndexColumn = 0,
        PropertyIndexColumn = 3,
        PropertyIdColumn = 4,
        RequestColumn = 10,
        /* Data Property columns */
        DataColumn = 12,
        /* Function Property columns */
        InputDataColumn = 11,
        ReturnCodeColumn = 12,
        OutputDataColumn = 13
    };

    int getDataPropertyRow(const KNX::Object_Index object_index, const KNX::Property_Index property_index);
    int getFunctionPropertyRow(const KNX::Object_Index object_index, const KNX::Property_Index property_index);
    QString getObjectName(const KNX::Object_Type object_type) const; // InterfaceObjectType
    QString getPropertyName(const KNX::Object_Type object_type, const KNX::Property_Id property_id) const; // InterfaceObjectProperty
    QString getTypeName(const KNX::Property_Type type) const; // PropertyDataType
};
