#pragma once

#include <QWidget>

#include <Connection/Connection.h>

/**
 * File Stream View
 *
 * Services:
 * - A_FileStream_InfoReport (Individual)
 */
class FileStreamView : public QWidget
{
    Q_OBJECT

public:
    explicit FileStreamView(QWidget * parent = nullptr);
    virtual ~FileStreamView();

    void setConnection(Connection * connection);
    void setDevice(DeviceData * device);

private:
    Connection * connection{nullptr};
    DeviceData * device{nullptr};
};
