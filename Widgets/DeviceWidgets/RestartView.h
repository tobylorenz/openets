#pragma once

#include <QGroupBox>
#include <QPushButton>
#include <QRadioButton>
#include <QSpinBox>
#include <QTableWidget>
#include <QWidget>

#include <Connection/Connection.h>

/**
 * Restart View
 *
 * Services:
 * - A_Restart (Individual)
 */
class RestartView : public QWidget
{
    Q_OBJECT

public:
    explicit RestartView(QWidget * parent = nullptr);
    virtual ~RestartView();

    void setConnection(Connection * connection);
    void setDevice(DeviceData * device);

private Q_SLOTS:
    void A_Restart_ind(const KNX::Restart_Erase_Code erase_code, const uint8_t channel_number, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Individual & asap);
    void on_basicRestartRadioButton_clicked();
    void on_masterRestartRadioButton_clicked();
    // @todo A_Restart.Acon
    void on_restartRequestPushButton_clicked();

private:
    Connection * connection{nullptr};
    DeviceData * device{nullptr};

    QGroupBox * restartTypeGroupBox{nullptr};
    QRadioButton * basicRestartRadioButton{nullptr};
    QRadioButton * masterRestartRadioButton{nullptr};
    QSpinBox * eraseCodeSpinBox{nullptr};
    QSpinBox * channelNumberSpinBox{nullptr};
    QPushButton * restartRequestPushButton{nullptr};
};
