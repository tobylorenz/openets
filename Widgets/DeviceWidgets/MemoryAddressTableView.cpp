#include "MemoryAddressTableView.h"

#include <QDebug>

#include <Project/v20/knx/ApplicationProgramStatic_AddressTable_t.h>
#include <Project/v20/knx/ApplicationProgramStatic_Code_AbsoluteSegment_t.h>
#include <Project/v20/knx/ApplicationProgramStatic_Code_t.h>
#include <Project/v20/knx/ApplicationProgramStatic_t.h>
#include <Project/v20/knx/MaskVersion_t.h>

MemoryAddressTableView::MemoryAddressTableView(QWidget * parent) :
    QTableWidget(parent)
{
    setColumnCount(2 + 2);
    setHorizontalHeaderLabels({
        "Content", "TSAP",
        "Address", "SizeInByte" // calculated
    });
    connect(this, static_cast<void (QTableWidget::*)()>(&QTableWidget::itemSelectionChanged), this, &MemoryAddressTableView::on_itemSelectionChanged);
}

MemoryAddressTableView::~MemoryAddressTableView()
{
}

void MemoryAddressTableView::setDevice(DeviceData * deviceData)
{
    Q_ASSERT(deviceData);
    this->deviceData = deviceData;

    const Project::v20::knx::ApplicationProgram_t * applicationProgram = deviceData->ets_ApplicationProgram;
    Q_ASSERT(applicationProgram);
    const Project::v20::knx::ApplicationProgramStatic_t * applicationProgramStatic = applicationProgram->Static;
    Q_ASSERT(applicationProgramStatic);
    const Project::v20::knx::ApplicationProgramStatic_Code_t * code = applicationProgramStatic->Code;
    Q_ASSERT(code);
    const Project::v20::knx::ApplicationProgramStatic_AddressTable_t * addressTable = applicationProgramStatic->AddressTable;
    Q_ASSERT(addressTable);

    const Project::v20::knx::MaskVersion_t * maskVersion = applicationProgram->getMaskVersion();
    Q_ASSERT(maskVersion);

    int row = 0;
    setRowCount(0);
    clearContents();

    for (const Project::v20::knx::ApplicationProgramStatic_Code_AbsoluteSegment_t * absoluteSegment : code->AbsoluteSegment) {
        Q_ASSERT(absoluteSegment);
        if (absoluteSegment->Id == addressTable->CodeSegment) {
            uint32_t address = (absoluteSegment->UserMemory == "true" ? 0x80000000 : 0) + absoluteSegment->Address.toUShort() + addressTable->Offset.toUInt();

            switch (maskVersion->MaskVersion.toUShort()) {
            case 0x701: {
                QTableWidgetItem * item;
                unsigned int tsap = 0;

                // Length
                insertRow(row);
                setItem(row, 0, new QTableWidgetItem("Length"));
                item = new QTableWidgetItem();
                item->setData(Qt::DisplayRole, address);
                setItem(row, 2, item);
                item = new QTableWidgetItem();
                item->setData(Qt::DisplayRole, 1);
                setItem(row, 3, item);
                address += 1;
                row++;

                // Individual Address
                insertRow(row);
                setItem(row, 0, new QTableWidgetItem("Individual Address"));
                setItem(row, 1, new QTableWidgetItem(QString::number(tsap)));
                item = new QTableWidgetItem();
                item->setData(Qt::DisplayRole, address);
                setItem(row, 2, item);
                item = new QTableWidgetItem();
                item->setData(Qt::DisplayRole, 2);
                setItem(row, 3, item);
                tsap++;
                address += 2;
                row++;

                // Group Addresses (MaxEntries seem to reflect only Group Addresses)
                while (tsap < addressTable->MaxEntries.toUInt() + 1) {
                    insertRow(row);
                    setItem(row, 0, new QTableWidgetItem("Group Address"));
                    setItem(row, 1, new QTableWidgetItem(QString::number(tsap)));
                    item = new QTableWidgetItem();
                    item->setData(Qt::DisplayRole, address);
                    setItem(row, 2, item);
                    item = new QTableWidgetItem();
                    item->setData(Qt::DisplayRole, 2);
                    setItem(row, 3, item);
                    tsap++;
                    address += 2;
                    row++;
                }
            }
            break;
            case 0x705:
                break;
            case 0x7B0: // IP Interface/Router Email and Time server function
                break;
            case 0x91A: // KNX IP Router
                break;
            }
        }
    }

    sortByColumn(2, Qt::AscendingOrder); // sort by address
    resizeColumnsToContents();
}

void MemoryAddressTableView::on_itemSelectionChanged()
{
    const int row = currentRow();

    const bool userMemory = item(row, 2)->data(Qt::DisplayRole).toUInt() & 0x80000000;
    const KNX::Memory_Address address = item(row, 2)->data(Qt::DisplayRole).toUInt() & ~0x80000000;
    const KNX::Memory_Address size = item(row, 3)->data(Qt::DisplayRole).toUInt();

    Q_ASSERT(deviceData);
    Q_EMIT deviceData->memory_selected(userMemory, address, size);
}
