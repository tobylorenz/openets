#include "MemoryAssociationTableView.h"

#include <QDebug>

#include <Project/v20/knx/ApplicationProgramStatic_AssociationTable_t.h>
#include <Project/v20/knx/ApplicationProgramStatic_Code_AbsoluteSegment_t.h>
#include <Project/v20/knx/ApplicationProgramStatic_Code_t.h>
#include <Project/v20/knx/ApplicationProgramStatic_t.h>
#include <Project/v20/knx/MaskVersion_t.h>

MemoryAssociationTableView::MemoryAssociationTableView(QWidget * parent) :
    QTableWidget(parent)
{
    setColumnCount(1 + 2);
    setHorizontalHeaderLabels({
        "Content",
        "Address", "SizeInByte" // calculated
    });
    connect(this, static_cast<void (QTableWidget::*)()>(&QTableWidget::itemSelectionChanged), this, &MemoryAssociationTableView::on_itemSelectionChanged);
}

MemoryAssociationTableView::~MemoryAssociationTableView()
{
}

void MemoryAssociationTableView::setDevice(DeviceData * deviceData)
{
    Q_ASSERT(deviceData);
    this->deviceData = deviceData;

    const Project::v20::knx::ApplicationProgram_t * applicationProgram = deviceData->ets_ApplicationProgram;
    Q_ASSERT(applicationProgram);
    const Project::v20::knx::ApplicationProgramStatic_t * applicationProgramStatic = applicationProgram->Static;
    Q_ASSERT(applicationProgramStatic);
    const Project::v20::knx::ApplicationProgramStatic_Code_t * code = applicationProgramStatic->Code;
    Q_ASSERT(code);
    const Project::v20::knx::ApplicationProgramStatic_AssociationTable_t * associationTable = applicationProgramStatic->AssociationTable;
    Q_ASSERT(associationTable);

    const Project::v20::knx::MaskVersion_t * maskVersion = applicationProgram->getMaskVersion();
    Q_ASSERT(maskVersion);

    int row = 0;
    setRowCount(0);
    clearContents();

    for (const Project::v20::knx::ApplicationProgramStatic_Code_AbsoluteSegment_t * absoluteSegment : code->AbsoluteSegment) {
        Q_ASSERT(absoluteSegment);
        if (absoluteSegment->Id == associationTable->CodeSegment) {
            uint32_t address = (absoluteSegment->UserMemory == "true" ? 0x80000000 : 0) + absoluteSegment->Address.toUShort() + associationTable->Offset.toUInt();

            switch (maskVersion->MaskVersion.toUShort()) {
            case 0x701: {
                QTableWidgetItem * item;

                // Current Size
                insertRow(row);
                setItem(row, 0, new QTableWidgetItem("Current Size"));
                item = new QTableWidgetItem();
                item->setData(Qt::DisplayRole, address);
                setItem(row, 1, item);
                item = new QTableWidgetItem();
                item->setData(Qt::DisplayRole, 1);
                setItem(row, 2, item);
                address += 1;
                row++;

                // TSAP / ASAP
                for (unsigned int i = 0; i < associationTable->MaxEntries.toUInt(); i++) {
                    insertRow(row);
                    setItem(row, 0, new QTableWidgetItem("TSAP / ASAP"));
                    item = new QTableWidgetItem();
                    item->setData(Qt::DisplayRole, address);
                    setItem(row, 1, item);
                    item = new QTableWidgetItem();
                    item->setData(Qt::DisplayRole, 2);
                    setItem(row, 2, item);
                    address += 2;
                    row++;
                }
            }
            break;
            case 0x705:
                break;
            case 0x7B0: // IP Interface/Router Email and Time server function
                break;
            case 0x91A: // KNX IP Router
                break;
            }
        }
    }

    sortByColumn(1, Qt::AscendingOrder); // sort by address
    resizeColumnsToContents();
}

void MemoryAssociationTableView::on_itemSelectionChanged()
{
    const int row = currentRow();


    const bool userMemory = item(row, 1)->data(Qt::DisplayRole).toUInt() & 0x80000000;
    const KNX::Memory_Address address = item(row, 1)->data(Qt::DisplayRole).toUInt() & ~0x80000000;
    const KNX::Memory_Address size = item(row, 2)->data(Qt::DisplayRole).toUInt();

    Q_ASSERT(deviceData);
    Q_EMIT deviceData->memory_selected(userMemory, address, size);
}
