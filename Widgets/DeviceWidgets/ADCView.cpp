#include "ADCView.h"

ADCView::ADCView(QWidget * parent) :
    QWidget(parent)
{
    setWindowTitle("ADC Read");
}

ADCView::~ADCView()
{
}

void ADCView::setConnection(Connection * connection)
{
    Q_ASSERT(connection);
    this->connection = connection;
}

void ADCView::setDevice(DeviceData * device)
{
    Q_ASSERT(device);
    this->device = device;
}
