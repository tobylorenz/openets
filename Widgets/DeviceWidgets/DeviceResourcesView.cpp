#include "DeviceResourcesView.h"

DeviceResourcesView::DeviceResourcesView(QWidget * parent) :
    QTabWidget(parent)
{
    setWindowTitle("Device Resources");

    // one tab for each available Device Resource
    addTab(nullptr, tr("Device Descriptors"));
    addTab(nullptr, tr("Device Object"));
    //addTab(nullptr, tr("Router Object"));
    //addTab(nullptr, tr("LTE Address Routing Table Object"));
    //addTab(nullptr, tr("cEMI Server Object"));
    //addTab(nullptr, tr("KNXnet/IP Parameter Object"));
    //addTab(nullptr, tr("File Server Object"));
    //addTab(nullptr, tr("RF Medium Object"));
    addTab(nullptr, tr("Group Address Table (GrAT)"));
    addTab(nullptr, tr("Group Object Association Table (GrOAT)"));
    addTab(nullptr, tr("Group Object Table"));
    //addTab(nullptr, tr("Easy Parameter Block Table (PaBT)"));
    addTab(nullptr, tr("Application Program"));
    addTab(nullptr, tr("PEI Program"));
    addTab(nullptr, tr("KNX Serial Number"));
    addTab(nullptr, tr("Load State Machine"));
    addTab(nullptr, tr("Run State Machine"));
    //addTab(nullptr, tr("OptionReg"));
    addTab(nullptr, tr("Programming Mode"));
    //addTab(nullptr, tr("Group Telegram Rate Limitation"));
}

DeviceResourcesView::~DeviceResourcesView()
{
}

void DeviceResourcesView::setConnection(Connection * connection)
{
    Q_ASSERT(connection);
    this->connection = connection;

    Q_ASSERT(connection->getStack());
    // connect(connection->getStack(), &Stack::A_ADC_Read_Lcon, this, &ADCView::A_ADC_Read_Lcon);
    // connect(connection->getStack(), &Stack::A_ADC_Read_ind, this, &ADCView::A_ADC_Read_ind);
    // connect(connection->getStack(), &Stack::A_ADC_Read_Acon, this, &ADCView::A_ADC_Read_Acon);
}

void DeviceResourcesView::setDevice(DeviceData * device)
{
    Q_ASSERT(device);
    this->device = device;
}
