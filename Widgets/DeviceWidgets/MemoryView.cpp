#include "MemoryView.h"

#include <QBoxLayout>
#include <QDebug>
#include <QLabel>
#include <QSplitter>
#include <QToolBar>

#include <KNX/yaml_output.h>
#include <Project/v20/knx/ApplicationProgramRef_t.h>
#include <Project/v20/knx/ApplicationProgramStatic_Code_AbsoluteSegment_t.h>
#include <Project/v20/knx/ApplicationProgramStatic_Code_t.h>
#include <Project/v20/knx/ApplicationProgramStatic_Parameters_Parameter_t.h>
#include <Project/v20/knx/ApplicationProgramStatic_Parameters_t.h>
#include <Project/v20/knx/ApplicationProgramStatic_t.h>
#include <Project/v20/knx/ApplicationProgram_t.h>
#include <Project/v20/knx/DeviceInstance_t.h>
#include <Project/v20/knx/Hardware2Program_t.h>
#include <Project/v20/knx/KNX_t.h>
#include <Project/v20/knx/MemoryParameter_t.h>
#include <Project/v20/knx/ParameterType_TypeColor_t.h>
#include <Project/v20/knx/ParameterType_TypeDate_t.h>
#include <Project/v20/knx/ParameterType_TypeFloat_t.h>
#include <Project/v20/knx/ParameterType_TypeIPAddress_t.h>
#include <Project/v20/knx/ParameterType_TypeNone_t.h>
#include <Project/v20/knx/ParameterType_TypeNumber_t.h>
#include <Project/v20/knx/ParameterType_TypePicture_t.h>
#include <Project/v20/knx/ParameterType_TypeRawData_t.h>
#include <Project/v20/knx/ParameterType_TypeRestriction_Enumeration_t.h>
#include <Project/v20/knx/ParameterType_TypeRestriction_t.h>
#include <Project/v20/knx/ParameterType_TypeText_t.h>
#include <Project/v20/knx/ParameterType_TypeTime_t.h>
#include <Project/v20/knx/ParameterType_t.h>
#include <Project/v20/knx/ParameterType_t.h>
#include <Project/v20/knx/Project_Installations_Installation_t.h>
#include <Project/v20/knx/Project_Installations_t.h>
#include <Project/v20/knx/Project_t.h>
#include <Project/v20/knx/PropertyParameter_t.h>
#include <Project/v20/knx/Topology_Area_Line_t.h>
#include <Project/v20/knx/Topology_Area_t.h>
#include <Project/v20/knx/Topology_t.h>
#include <Project/xs/ID.h>

MemoryView::MemoryView(QWidget * parent) :
    QMainWindow(parent)
{
    setWindowTitle("(User)Memory(Bit) Read/Write");

    /* create Actions */
    connectAction = new QAction("Connect", this);
    disconnectAction = new QAction("Disconnect", this);
    readMemoryAction = new QAction("Read Memory", this);
    connect(readMemoryAction, &QAction::triggered, this, &MemoryView::on_readMemoryAction_triggered);

    /* create Elements */
    etsPresetsComboBox = new QComboBox();
    etsPresetsComboBox->setEditable(true);
    connect(etsPresetsComboBox, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged), this, &MemoryView::on_etsPresetsComboBox_currentIndexChanged);
    userMemoryCheckBox = new QCheckBox();
    connect(userMemoryCheckBox, static_cast<void (QCheckBox::*)(int)>(&QCheckBox::stateChanged), this, &MemoryView::on_userMemoryChecked_stateChanged);
    memoryStartAddressSpinBox = new SpinBox4HexDigits();
    connect(memoryStartAddressSpinBox, static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged), this, &MemoryView::on_memoryStartAddressSpinBox_valueChanged);
    memorySizeSpinBox = new SpinBox4HexDigits();
    connect(memorySizeSpinBox, static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged), this, &MemoryView::on_memorySizeSpinBox_valueChanged);
    memoryEndAddressSpinBox = new SpinBox4HexDigits();
    connect(memoryEndAddressSpinBox, static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged), this, &MemoryView::on_memoryEndAddressSpinBox_valueChanged);

    /* create ToolBars */
    QToolBar * toolBar = new QToolBar();
    toolBar->addWidget(new QLabel(tr("ETS Presets:")));
    toolBar->addWidget(etsPresetsComboBox);
    toolBar->addSeparator();
    toolBar->addWidget(new QLabel(tr("User Memory:")));
    toolBar->addWidget(userMemoryCheckBox);
    toolBar->addWidget(new QLabel(tr("Start Address:")));
    toolBar->addWidget(memoryStartAddressSpinBox);
    toolBar->addWidget(new QLabel(tr("Size:")));
    toolBar->addWidget(memorySizeSpinBox);
    toolBar->addWidget(new QLabel(tr("End Address:")));
    toolBar->addWidget(memoryEndAddressSpinBox);
    toolBar->addSeparator();
    toolBar->addAction(connectAction);
    toolBar->addAction(disconnectAction);
    toolBar->addAction(readMemoryAction);
    addToolBar(toolBar);

    /* create memory TableWidget */
    memoryTableWidget = new QTableWidget();
    memoryTableWidget->setColumnCount(18);
    memoryTableWidget->setHorizontalHeaderLabels({"address", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "ascii"});

    /* create bottom widgets */
    tabWidget = new QTabWidget();
    memoryResourceWidget = new MemoryResourcesView();
    tabWidget->addTab(memoryResourceWidget, tr("Resources"));
    memoryGroupObjectWidget = new MemoryGroupObjectView();
    tabWidget->addTab(memoryGroupObjectWidget, tr("Group Objects"));
    memoryAddressTableWidget = new MemoryAddressTableView();
    tabWidget->addTab(memoryAddressTableWidget, tr("Address Table"));
    memoryAssociationTableWidget = new MemoryAssociationTableView();
    tabWidget->addTab(memoryAssociationTableWidget, tr("Association Table"));
    memoryComObjectTableWidget = new MemoryComObjectTableView();
    tabWidget->addTab(memoryComObjectTableWidget, tr("Com Object Table"));
    parameterReferencesTableWidget = new MemoryParameterView();
    tabWidget->addTab(parameterReferencesTableWidget, tr("Parameters"));

    /* layout */
    QSplitter * splitter = new QSplitter(Qt::Vertical);
    splitter->addWidget(memoryTableWidget);
    splitter->addWidget(tabWidget);
    setCentralWidget(splitter);
}

MemoryView::~MemoryView()
{
}

void MemoryView::setConnection(Connection * connection)
{
    Q_ASSERT(connection);

    this->connection = connection;
}

void MemoryView::setDevice(DeviceData * deviceData)
{
    Q_ASSERT(deviceData);

    this->deviceData = deviceData;
    memoryResourceWidget->setDevice(deviceData);
    memoryAddressTableWidget->setDevice(deviceData);
    memoryAssociationTableWidget->setDevice(deviceData);
    memoryComObjectTableWidget->setDevice(deviceData);
    memoryGroupObjectWidget->setDevice(deviceData);
    parameterReferencesTableWidget->setDevice(deviceData);

    connect(connectAction, &QAction::triggered, deviceData, &DeviceData::transport_layer_connect);
    connect(disconnectAction, &QAction::triggered, deviceData, &DeviceData::transport_layer_disconnect);
    connect(deviceData, &DeviceData::transport_layer_connection_state_changed, this, &MemoryView::transportLayerConnectionStateChanged);
    connect(deviceData, &DeviceData::memory_changed, this, &MemoryView::memoryChanged);
    connect(deviceData, &DeviceData::user_memory_changed, this, &MemoryView::memoryChanged);
    connect(deviceData, &DeviceData::memory_selected, this, &MemoryView::on_memory_selected);

    if (deviceData->ets_ApplicationProgram) {
        const Project::v20::knx::ApplicationProgramStatic_t * etsApplicationProgramStatic = deviceData->ets_ApplicationProgram->Static;
        Q_ASSERT(etsApplicationProgramStatic);
        const Project::v20::knx::ApplicationProgramStatic_Code_t * etsCode = etsApplicationProgramStatic->Code;
        Q_ASSERT(etsCode);
        for (const Project::v20::knx::ApplicationProgramStatic_Code_AbsoluteSegment_t * etsAbsoluteSegment : etsCode->AbsoluteSegment) {
            const uint32_t address = etsAbsoluteSegment->Address.toUShort();
            const uint32_t size = etsAbsoluteSegment->Size.toUShort();
            QString text;
            if (etsAbsoluteSegment->UserMemory == "false") {
                text = QString("0x%1 + 0x%2").arg(address, 4, 16, QChar('0')).arg(size, 4, 16, QChar('0'));
            } else if (etsAbsoluteSegment->UserMemory == "true") {
                text = QString("0x%1 + 0x%2 (User memory)").arg(address, 5, 16, QChar('0')).arg(size, 5, 16, QChar('0'));
            }
            etsPresetsComboBox->addItem(text, etsAbsoluteSegment->Id);
        }
    }

    connectAction->setEnabled(!deviceData->transport_layer_connected);
    disconnectAction->setEnabled(deviceData->transport_layer_connected);
    readMemoryAction->setEnabled(deviceData->transport_layer_connected);
}

void MemoryView::transportLayerConnectionStateChanged()
{
    Q_ASSERT(deviceData);

    connectAction->setEnabled(!deviceData->transport_layer_connected);
    disconnectAction->setEnabled(deviceData->transport_layer_connected);
    readMemoryAction->setEnabled(deviceData->transport_layer_connected);
}

void MemoryView::memoryChanged()
{
    Q_ASSERT(userMemoryCheckBox);
    Q_ASSERT(memoryStartAddressSpinBox);
    Q_ASSERT(memorySizeSpinBox);
    Q_ASSERT(memoryEndAddressSpinBox);

    /* update table */
    memoryTableWidget->clearContents();
    memoryTableWidget->setRowCount(0);
    int currentAddress = memoryStartAddressSpinBox->value() & ~ 0x0f;
    int row = 0;
    while (currentAddress < memoryStartAddressSpinBox->value() + memorySizeSpinBox->value()) {
        memoryTableWidget->insertRow(row);

        /* address */
        const QString addrStr = QString("%1").arg(currentAddress, userMemoryCheckBox->isChecked() ? 5 : 4, 16, QChar('0'));
        memoryTableWidget->setItem(row, 0, new QTableWidgetItem(addrStr));

        /* hex / ascii */
        QString ascii;
        for (int offset = 0; offset < 16; offset++) {
            uint8_t ch = 0;
            if ((currentAddress >= memoryStartAddressSpinBox->value()) && (currentAddress < memoryStartAddressSpinBox->value() + memorySizeSpinBox->value())) {
                ch = userMemoryCheckBox->isChecked() ? deviceData->user_memory[currentAddress] : deviceData->memory[currentAddress];
                const QString hex = QString("%1").arg(ch, 2, 16, QChar('0'));
                auto * tableWidgetItem = new QTableWidgetItem(hex);
                if (userMemoryCheckBox->isChecked() ? (deviceData->user_memory_mask[currentAddress] & 0x02) : (deviceData->memory_mask[currentAddress] & 0x02)) {
                    const QColor color(0xCC, 0xFF, 0xCC);
                    const QBrush brush(color);
                    tableWidgetItem->setBackground(brush);
                }
                memoryTableWidget->setItem(row, 1 + offset, tableWidgetItem);
            }
            if (ch < 0x20) {
                ascii.append(QChar(' '));
            } else {
                ascii.append(QChar(ch));
            }
            currentAddress++;
        }

        /* ascii */
        memoryTableWidget->setItem(row, 17, new QTableWidgetItem(ascii));

        row++;
    }
    memoryTableWidget->resizeColumnsToContents();
}

void MemoryView::on_etsPresetsComboBox_currentIndexChanged(int /*index*/)
{
    Q_ASSERT(etsPresetsComboBox);
    const Project::xs::ID id = etsPresetsComboBox->currentData().toString();

    Q_ASSERT(deviceData->ets_ApplicationProgram);
    const Project::v20::knx::ApplicationProgramStatic_t * etsApplicationProgramStatic = deviceData->ets_ApplicationProgram->Static;
    Q_ASSERT(etsApplicationProgramStatic);
    const Project::v20::knx::ApplicationProgramStatic_Code_t * etsCode = etsApplicationProgramStatic->Code;
    Q_ASSERT(etsCode);
    for (const Project::v20::knx::ApplicationProgramStatic_Code_AbsoluteSegment_t * etsAbsoluteSegment : etsCode->AbsoluteSegment) {
        Q_ASSERT(etsAbsoluteSegment);
        if (etsAbsoluteSegment->Id == id) {
            const uint32_t address = etsAbsoluteSegment->Address.toUShort();
            const uint32_t size = etsAbsoluteSegment->Size.toUShort();

            Q_ASSERT(userMemoryCheckBox);
            Q_ASSERT(memoryStartAddressSpinBox);
            Q_ASSERT(memorySizeSpinBox);
            Q_ASSERT(memoryEndAddressSpinBox);

            QSignalBlocker blocker1{userMemoryCheckBox};
            QSignalBlocker blocker2{memoryStartAddressSpinBox};
            QSignalBlocker blocker3{memorySizeSpinBox};
            QSignalBlocker blocker4{memoryEndAddressSpinBox};

            userMemoryCheckBox->setChecked(etsAbsoluteSegment->UserMemory == "true");
            memoryStartAddressSpinBox->setValue(address);
            memorySizeSpinBox->setValue(size);
            memoryEndAddressSpinBox->setMinimum(address);
            memoryEndAddressSpinBox->setValue(address + size);

            memoryChanged();
        }
    }
}

void MemoryView::on_userMemoryChecked_stateChanged(int /*value*/)
{
    Q_ASSERT(etsPresetsComboBox);
    Q_ASSERT(memoryStartAddressSpinBox);
    Q_ASSERT(memorySizeSpinBox);
    Q_ASSERT(memoryEndAddressSpinBox);

    QSignalBlocker blocker1{etsPresetsComboBox};
    QSignalBlocker blocker2{memoryStartAddressSpinBox};
    QSignalBlocker blocker3{memorySizeSpinBox};
    QSignalBlocker blocker4{memoryEndAddressSpinBox};

    etsPresetsComboBox->setCurrentIndex(-1);
    etsPresetsComboBox->setCurrentText("");
    memoryStartAddressSpinBox->setValue(0);
    memorySizeSpinBox->setValue(0);
    memoryEndAddressSpinBox->setMinimum(0);
    memoryEndAddressSpinBox->setValue(0);

    memoryChanged();
}

void MemoryView::on_memoryStartAddressSpinBox_valueChanged(int value)
{
    Q_ASSERT(etsPresetsComboBox);
    Q_ASSERT(memorySizeSpinBox);
    Q_ASSERT(memoryEndAddressSpinBox);

    QSignalBlocker blocker1{etsPresetsComboBox};
    QSignalBlocker blocker2{userMemoryCheckBox};
    QSignalBlocker blocker3{memorySizeSpinBox};
    QSignalBlocker blocker4{memoryEndAddressSpinBox};

    etsPresetsComboBox->setCurrentIndex(-1);
    etsPresetsComboBox->setCurrentText("");
    memoryEndAddressSpinBox->setMinimum(value);
    memoryEndAddressSpinBox->setValue(value + memorySizeSpinBox->value());

    memoryChanged();
}

void MemoryView::on_memorySizeSpinBox_valueChanged(int value)
{
    Q_ASSERT(etsPresetsComboBox);
    Q_ASSERT(memoryStartAddressSpinBox);
    Q_ASSERT(memoryEndAddressSpinBox);

    QSignalBlocker blocker1{etsPresetsComboBox};
    QSignalBlocker blocker2{userMemoryCheckBox};
    QSignalBlocker blocker3{memoryStartAddressSpinBox};
    QSignalBlocker blocker4{memoryEndAddressSpinBox};

    etsPresetsComboBox->setCurrentIndex(-1);
    etsPresetsComboBox->setCurrentText("");
    memoryEndAddressSpinBox->setValue(memoryStartAddressSpinBox->value() + value);

    memoryChanged();
}

void MemoryView::on_memoryEndAddressSpinBox_valueChanged(int value)
{
    Q_ASSERT(etsPresetsComboBox);
    Q_ASSERT(memoryStartAddressSpinBox);
    Q_ASSERT(memorySizeSpinBox);

    QSignalBlocker blocker1{etsPresetsComboBox};
    QSignalBlocker blocker2{userMemoryCheckBox};
    QSignalBlocker blocker3{memoryStartAddressSpinBox};
    QSignalBlocker blocker4{memorySizeSpinBox};

    etsPresetsComboBox->setCurrentIndex(-1);
    etsPresetsComboBox->setCurrentText("");
    memorySizeSpinBox->setValue(value - memoryStartAddressSpinBox->value());

    memoryChanged();
}

void MemoryView::on_readMemoryAction_triggered()
{
    Q_ASSERT(userMemoryCheckBox);
    Q_ASSERT(memoryStartAddressSpinBox);
    Q_ASSERT(memorySizeSpinBox);

    /* start reading */
    if (userMemoryCheckBox->isChecked()) {
        deviceData->user_memory_read_request.request = true;
        deviceData->user_memory_read_request.address = memoryStartAddressSpinBox->value();
        deviceData->user_memory_read_request.size = memorySizeSpinBox->value();
        KNX::UserMemory_Number number = 8;
        if (deviceData->user_memory_read_request.size < number) {
            number = deviceData->user_memory_read_request.size;
        }
        connection->getStack()->A_UserMemory_Read_req(KNX::Ack_Request::dont_care, KNX::Priority::low, KNX::Network_Layer_Parameter, {deviceData->address, true}, number, deviceData->memory_read_request.address, [](const KNX::Status /*a_status*/){
            // Lcon
        });
    } else {
        deviceData->memory_read_request.request = true;
        deviceData->memory_read_request.address = memoryStartAddressSpinBox->value();
        deviceData->memory_read_request.size = memorySizeSpinBox->value();
        KNX::Memory_Number number = 8;
        if (deviceData->memory_read_request.size < number) {
            number = deviceData->memory_read_request.size;
        }
        connection->getStack()->A_Memory_Read_req(KNX::Ack_Request::dont_care, KNX::Priority::low, KNX::Network_Layer_Parameter, {deviceData->address, true}, number, deviceData->memory_read_request.address, [](const KNX::Status /*a_status*/){
            // Lcon
        });
    }
}

void MemoryView::on_memory_selected(bool userMemory, KNX::Memory_Address address, KNX::Memory_Address size)
{
    Q_ASSERT(memoryTableWidget);
    memoryTableWidget->clearSelection();

    /* check if ETS preset is available */
    bool addressInDeviceData = false;
    if (deviceData) {
        if (deviceData->ets_ApplicationProgram) {
            const Project::v20::knx::ApplicationProgramStatic_t * etsApplicationProgramStatic = deviceData->ets_ApplicationProgram->Static;
            Q_ASSERT(etsApplicationProgramStatic);
            const Project::v20::knx::ApplicationProgramStatic_Code_t * etsCode = etsApplicationProgramStatic->Code;
            Q_ASSERT(etsCode);
            int index = 0;
            for (const Project::v20::knx::ApplicationProgramStatic_Code_AbsoluteSegment_t * etsAbsoluteSegment : etsCode->AbsoluteSegment) {
                if (((etsAbsoluteSegment->UserMemory == "true") == userMemory) &&
                        (etsAbsoluteSegment->Address.toUInt() <= address) &&
                        (address + size <= etsAbsoluteSegment->Address.toUInt() + etsAbsoluteSegment->Size.toUInt())) {
                    addressInDeviceData = true;

                    Q_ASSERT(userMemoryCheckBox);
                    Q_ASSERT(memoryStartAddressSpinBox);
                    Q_ASSERT(memorySizeSpinBox);
                    Q_ASSERT(memoryEndAddressSpinBox);

                    QSignalBlocker blocker1{userMemoryCheckBox};
                    QSignalBlocker blocker2{memoryStartAddressSpinBox};
                    QSignalBlocker blocker3{memorySizeSpinBox};
                    QSignalBlocker blocker4{memoryEndAddressSpinBox};

                    etsPresetsComboBox->setCurrentIndex(index);
                    userMemoryCheckBox->setChecked(etsAbsoluteSegment->UserMemory == "true");
                    memoryStartAddressSpinBox->setValue(etsAbsoluteSegment->Address.toUInt());
                    memorySizeSpinBox->setValue(etsAbsoluteSegment->Size.toUInt());
                    memoryEndAddressSpinBox->setMinimum(etsAbsoluteSegment->Address.toUInt());
                    memoryEndAddressSpinBox->setValue(etsAbsoluteSegment->Address.toUInt() + etsAbsoluteSegment->Size.toUInt());

                    memoryChanged();
                }
                index++;
            }
        }
    }

    /* else show unknown memory section */
    if (!addressInDeviceData) {
        Q_ASSERT(userMemoryCheckBox);
        Q_ASSERT(memoryStartAddressSpinBox);
        Q_ASSERT(memorySizeSpinBox);
        Q_ASSERT(memoryEndAddressSpinBox);

        QSignalBlocker blocker1{userMemoryCheckBox};
        QSignalBlocker blocker2{memoryStartAddressSpinBox};
        QSignalBlocker blocker3{memorySizeSpinBox};
        QSignalBlocker blocker4{memoryEndAddressSpinBox};

        etsPresetsComboBox->setCurrentIndex(-1);
        userMemoryCheckBox->setChecked(userMemory);
        memoryStartAddressSpinBox->setValue(address);
        memorySizeSpinBox->setValue(size);
        memoryEndAddressSpinBox->setMinimum(address);
        memoryEndAddressSpinBox->setValue(address + size);

        memoryChanged();
    }

    /* select items */
    const unsigned int rangeStart = (address > memoryStartAddressSpinBox->value()) ? address : memoryStartAddressSpinBox->value();
    const unsigned int rangeEnd = (address + size < memoryStartAddressSpinBox->value() + memorySizeSpinBox->value()) ? address + size : memoryStartAddressSpinBox->value() + memorySizeSpinBox->value();
    if (rangeStart >= rangeEnd) {
        return;
    }
    const int viewStartAddress = memoryStartAddressSpinBox->value() & ~ 0x0f;
    const unsigned int viewOffset = rangeStart - viewStartAddress;
    for (unsigned int i = 0; i < rangeEnd - rangeStart; i++) {
        memoryTableWidget->item((viewOffset + i) / 16, (viewOffset + i) % 16 + 1)->setSelected(true);
    }

    /* scroll to first selected item */
    memoryTableWidget->scrollToItem(memoryTableWidget->item(viewOffset / 16, viewOffset % 16 + 1));
}
