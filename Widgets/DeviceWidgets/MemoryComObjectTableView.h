#pragma once

#include <QTableWidget>

#include <Connection/Connection.h>

/**
 * Memory Com Object Table View
 */
class MemoryComObjectTableView : public QTableWidget
{
    Q_OBJECT

public:
    explicit MemoryComObjectTableView(QWidget * parent = nullptr);
    virtual ~MemoryComObjectTableView();

    void setDevice(DeviceData * deviceData);

private Q_SLOTS:
    void on_itemSelectionChanged();

private:
    /* device data */
    DeviceData * deviceData{nullptr};
};
