#pragma once

#include <QTableWidget>

#include <Connection/Connection.h>

/**
 * Memory Parameter View
 */
class MemoryParameterView : public QTableWidget
{
    Q_OBJECT

public:
    explicit MemoryParameterView(QWidget * parent = nullptr);
    virtual ~MemoryParameterView();

    void setDevice(DeviceData * deviceData);

private Q_SLOTS:
    void on_itemSelectionChanged();

private:
    /* device data */
    DeviceData * deviceData{nullptr};
};
