#include <QApplication>
#include <QCommandLineParser>
#include <QCommandLineOption>
#include <QDebug>
#include <QIcon>

#include <DebugWindow.h>
#include <MainWindow.h>
#include <Project/ProjectBundle.h>

static inline void initResources()
{
    Q_INIT_RESOURCE(Icons);
}

int main(int argc, char ** argv)
{
    QApplication application(argc, argv);

    QCoreApplication::setApplicationName("OpenETS");
    QCoreApplication::setApplicationVersion("0.1.0");
    QCoreApplication::setOrganizationDomain("tomilo.org");
    QCoreApplication::setOrganizationName("ToMiLo");

    QCommandLineParser commandLineParser;
    commandLineParser.setApplicationDescription("OpenETS");
    commandLineParser.addHelpOption();
    commandLineParser.addVersionOption();
    commandLineParser.addPositionalArgument("file", "file to open");
    commandLineParser.process(application);

    qInstallMessageHandler(DebugWindow::messageHandler);

    initResources();
    QIcon::setThemeName("OpenETS");

    MainWindow * mainWindow = new MainWindow;
    mainWindow->show();

    return application.exec();
}
