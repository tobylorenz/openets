#include "Stack.h"

#include <QDebug>

#include <KNX/03/06/03/cEMI.h>

Stack::Stack(asio::io_context & io_context, QObject * parent) :
    QObject(parent),
    cemi_client(io_context)
{
    cemi_client.cemi_req = std::bind(&Stack::local_cemi_req, this, std::placeholders::_1);

    /* interface objects */
    // (0) Device Object
    // created in CEMI_Client constructor
    // (1) Group Address Table
    // created in CEMI_Client constructor
    // (2) Group Object Association Table
    // created in CEMI_Client constructor

    A_Connect_ind_initiator();
    A_Disconnect_ind_initiator();

    /* Services (Group) */
    A_GroupValue_Read_ind_initiator();
    A_GroupValue_Read_Acon_initiator();
    A_GroupValue_Write_ind_initiator();

    /* Services (Broadcast) */
    A_IndividualAddress_Write_ind_initiator();
    A_IndividualAddress_Read_ind_initiator();
    A_IndividualAddress_Read_Acon_initiator();
    A_IndividualAddressSerialNumber_Read_ind_initiator();
    A_IndividualAddressSerialNumber_Read_Acon_initiator();
    A_IndividualAddressSerialNumber_Write_ind_initiator();
    A_NetworkParameter_Read_ind_initiator();
    A_NetworkParameter_Read_Acon_initiator();
    A_NetworkParameter_Write_ind_initiator();
    A_NetworkParameter_InfoReport_ind_initiator();

    /* Services (System Broadcast) */
    A_DeviceDescriptor_InfoReport_ind_initiator();
    A_DomainAddress_Write_ind_initiator();
    A_DomainAddress_Read_ind_initiator();
    A_DomainAddress_Read_Acon_initiator();
    A_DomainAddressSelective_Read_ind_initiator();
    A_DomainAddressSerialNumber_Read_ind_initiator();
    A_DomainAddressSerialNumber_Read_Acon_initiator();
    A_DomainAddressSerialNumber_Write_ind_initiator();
    A_SystemNetworkParameter_Read_ind_initiator();
    A_SystemNetworkParameter_Read_Acon_initiator();
    A_SystemNetworkParameter_Write_ind_initiator();

    /* Services (Individual) */
    A_DeviceDescriptor_Read_ind_initiator();
    A_DeviceDescriptor_Read_Acon_initiator();
    A_Restart_ind_initiator();
    A_FileStream_InfoReport_ind_initiator();
    A_PropertyValue_Read_ind_initiator();
    A_PropertyValue_Read_Acon_initiator();
    A_PropertyValue_Write_ind_initiator();
    A_PropertyDescription_Read_ind_initiator();
    A_PropertyDescription_Read_Acon_initiator();
    A_FunctionPropertyCommand_ind_initiator();
    A_FunctionPropertyCommand_Acon_initiator();
    A_FunctionPropertyState_Read_ind_initiator();
    A_FunctionPropertyState_Read_Acon_initiator();

    /* Services (Connected) */
    A_ADC_Read_ind_initiator();
    A_ADC_Read_Acon_initiator();
    A_Memory_Read_ind_initiator();
    A_Memory_Read_Acon_initiator();
    A_Memory_Write_ind_initiator();
    A_Memory_Write_Acon_initiator();
    A_MemoryBit_Write_ind_initiator();
    A_UserMemory_Read_ind_initiator();
    A_UserMemory_Read_Acon_initiator();
    A_UserMemory_Write_ind_initiator();
    A_UserMemory_Write_Acon_initiator();
    A_UserMemoryBit_Write_ind_initiator();
    A_UserMemoryBit_Write_Acon_initiator();
    A_UserManufacturerInfo_Read_ind_initiator();
    A_UserManufacturerInfo_Read_Acon_initiator();
    A_Authorize_Request_ind_initiator();
    A_Authorize_Request_Acon_initiator();
    A_Key_Write_ind_initiator();
    A_Key_Write_Acon_initiator();
}

void Stack::local_cemi_req(const std::vector<uint8_t> cemi_frame_data)
{
    if (cemi_req) {
        cemi_req(cemi_frame_data);
    }
};

void Stack::cemi_con_ind(const std::vector<uint8_t> cemi_frame_data)
{
    cemi_client.cemi_con_ind(cemi_frame_data);
}

void Stack::A_Connect_req(const KNX::ASAP_Individual asap, const KNX::Priority priority, std::function<void(const KNX::Status a_status)> Lcon)
{
    qInfo() << "A_Connect_req";

    cemi_client.application_layer.A_Connect_req(asap, priority, Lcon);
}

void Stack::A_Disconnect_req(const KNX::Priority priority, const KNX::ASAP_Individual asap, std::function<void(const KNX::Status a_status)> Lcon)
{
    qInfo() << "A_Disconnect_req";

    cemi_client.application_layer.A_Disconnect_req(priority, asap, Lcon);
}

/* Services (Group) */

void Stack::A_GroupValue_Read_req(const KNX::Ack_Request ack_request, const KNX::ASAP_Group asap, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, std::function<void(const KNX::Status a_status)> Lcon)
{
    qInfo() << "A_GroupValue_Read_req";

    cemi_client.application_layer.A_GroupValue_Read_req(ack_request, asap, priority, hop_count_type, Lcon);
}

void Stack::A_GroupValue_Read_res(const KNX::Ack_Request ack_request, const KNX::ASAP_Group asap, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const std::vector<uint8_t> data, std::function<void(const KNX::Status a_status)> Rcon)
{
    qInfo() << "A_GroupValue_Read_res";

    cemi_client.application_layer.A_GroupValue_Read_res(ack_request, asap, priority, hop_count_type, data, Rcon);
}

void Stack::A_GroupValue_Write_req(const KNX::Ack_Request ack_request, const KNX::ASAP_Group asap, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const std::vector<uint8_t> data, std::function<void(const KNX::Status a_status)> Lcon)
{
    qInfo() << "A_GroupValue_Write_req";

    cemi_client.application_layer.A_GroupValue_Write_req(ack_request, asap, priority, hop_count_type, data, Lcon);
}

/* Services (Broadcast) */

void Stack::A_IndividualAddress_Write_req(const KNX::Ack_Request ack_request, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::Individual_Address newaddress, std::function<void(const KNX::Status a_status)> Lcon)
{
    qInfo() << "A_IndividualAddress_Write_req";

    cemi_client.application_layer.A_IndividualAddress_Write_req(ack_request, priority, hop_count_type, newaddress, Lcon);
}

void Stack::A_IndividualAddress_Read_req(const KNX::Ack_Request ack_request, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, std::function<void(const KNX::Status a_status)> Lcon)
{
    qInfo() << "A_IndividualAddress_Read_req";

    cemi_client.application_layer.A_IndividualAddress_Read_req(ack_request, priority, hop_count_type, Lcon);
}

void Stack::A_IndividualAddress_Read_res(const KNX::Ack_Request ack_request, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::Individual_Address individual_address, std::function<void(const KNX::Status a_status)> Rcon)
{
    qInfo() << "A_IndividualAddress_Read_res";

    cemi_client.application_layer.A_IndividualAddress_Read_res(ack_request, priority, hop_count_type, individual_address, Rcon);
}

void Stack::A_IndividualAddressSerialNumber_Read_req(const KNX::Ack_Request ack_request, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::Serial_Number serial_number, std::function<void(const KNX::Status a_status)> Lcon)
{
    qInfo() << "A_IndividualAddressSerialNumber_Read_req";

    cemi_client.application_layer.A_IndividualAddressSerialNumber_Read_req(ack_request, priority, hop_count_type, serial_number, Lcon);
}

void Stack::A_IndividualAddressSerialNumber_Read_res(const KNX::Ack_Request ack_request, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::Serial_Number serial_number, const KNX::Domain_Address_2 domain_address, std::function<void(const KNX::Status a_status)> Rcon)
{
    qInfo() << "A_IndividualAddressSerialNumber_Read_res";

    cemi_client.application_layer.A_IndividualAddressSerialNumber_Read_res(ack_request, priority, hop_count_type, serial_number, domain_address, Rcon);
}

void Stack::A_IndividualAddressSerialNumber_Write_req(const KNX::Ack_Request ack_request, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::Serial_Number serial_number, const KNX::Individual_Address newaddress, std::function<void(const KNX::Status a_status)> Lcon)
{
    qInfo() << "A_IndividualAddressSerialNumber_Write_req";

    cemi_client.application_layer.A_IndividualAddressSerialNumber_Write_req(ack_request, priority, hop_count_type, serial_number, newaddress, Lcon);
}

void Stack::A_NetworkParameter_Read_req(const KNX::ASAP_Individual asap, const KNX::Comm_Mode comm_mode_req, const KNX::Hop_Count_Type hop_count_type, const KNX::Parameter_Type parameter_type, const KNX::Priority priority, const KNX::Parameter_Test_Info test_info, std::function<void(const KNX::Status a_status)> Lcon)
{
    qInfo() << "A_NetworkParameter_Read_req";

    cemi_client.application_layer.A_NetworkParameter_Read_req(asap, comm_mode_req, hop_count_type, parameter_type, priority, test_info, Lcon);
}

void Stack::A_NetworkParameter_Read_res(const KNX::ASAP_Individual asap, const KNX::Comm_Mode comm_mode, const KNX::Hop_Count_Type hop_count_type, const KNX::Parameter_Type parameter_type, const KNX::Priority priority, const KNX::Parameter_Test_Info test_info, const KNX::Parameter_Test_Result test_result, std::function<void(const KNX::Status a_status)> Rcon)
{
    qInfo() << "A_NetworkParameter_Read_res";

    cemi_client.application_layer.A_NetworkParameter_Read_res(asap, comm_mode, hop_count_type, parameter_type, priority, test_info, test_result, Rcon);
}

void Stack::A_NetworkParameter_Write_req(const KNX::ASAP_Individual asap, const KNX::Comm_Mode comm_mode, const KNX::Hop_Count_Type hop_count_type, const KNX::Parameter_Type parameter_type, const KNX::Priority priority, const std::vector<uint8_t> value, std::function<void(const KNX::Status a_status)> Lcon)
{
    qInfo() << "A_NetworkParameter_Write_req";

    cemi_client.application_layer.A_NetworkParameter_Write_req(asap, comm_mode, hop_count_type, parameter_type, priority, value, Lcon);
}

void Stack::A_NetworkParameter_InfoReport_req(const KNX::ASAP_Individual asap, const KNX::Comm_Mode comm_mode_req, const KNX::Hop_Count_Type hop_count_type, const KNX::Parameter_Type parameter_type, const KNX::Priority priority, const KNX::Parameter_Test_Info test_info, const KNX::Parameter_Test_Result test_result, std::function<void(const KNX::Status a_status)> Lcon)
{
    qInfo() << "A_NetworkParameter_InfoReport_req";

    cemi_client.application_layer.A_NetworkParameter_InfoReport_req(asap, comm_mode_req, hop_count_type, parameter_type, priority, test_info, test_result, Lcon);
}

/* Services (System Broadcast) */

void Stack::A_DeviceDescriptor_InfoReport_req(const KNX::Ack_Request ack_request, const KNX::Descriptor_Type descriptor_type, const std::vector<uint8_t> device_descriptor, const KNX::Hop_Count_Type hop_count_type, const KNX::Priority priority, std::function<void(const KNX::Status a_status)> Lcon)
{
    qInfo() << "A_DeviceDescriptor_InfoReport_req";

    cemi_client.application_layer.A_DeviceDescriptor_InfoReport_req(ack_request, descriptor_type, device_descriptor, hop_count_type, priority, Lcon);
}

void Stack::A_DomainAddress_Write_req(const KNX::Ack_Request ack_request, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::Domain_Address domain_address_new, std::function<void(const KNX::Status a_status)> Lcon)
{
    qInfo() << "A_DomainAddress_Write_req";

    cemi_client.application_layer.A_DomainAddress_Write_req(ack_request, priority, hop_count_type, domain_address_new, Lcon);
}

void Stack::A_DomainAddress_Read_req(const KNX::Ack_Request ack_request, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, std::function<void(const KNX::Status a_status)> Lcon)
{
    qInfo() << "A_DomainAddress_Read_req";

    cemi_client.application_layer.A_DomainAddress_Read_req(ack_request, priority, hop_count_type, Lcon);
}

void Stack::A_DomainAddress_Read_res(const KNX::Ack_Request ack_request, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::Domain_Address domain_address, std::function<void(const KNX::Status a_status)> Rcon)
{
    qInfo() << "A_DomainAddress_Read_res";

    cemi_client.application_layer.A_DomainAddress_Read_res(ack_request, priority, hop_count_type, domain_address, Rcon);
}

void Stack::A_DomainAddressSelective_Read_req(const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASDU asdu, std::function<void(const KNX::Status a_status)> Lcon)
{
    qInfo() << "A_DomainAddressSelective_Read_req";

    cemi_client.application_layer.A_DomainAddressSelective_Read_req(priority, hop_count_type, asdu, Lcon);
}

void Stack::A_DomainAddressSerialNumber_Read_req(const KNX::Ack_Request ack_request, const KNX::Hop_Count_Type hop_count_type, const KNX::Priority priority, const KNX::Serial_Number serial_number, std::function<void(const KNX::Status a_status)> Lcon)
{
    qInfo() << "A_DomainAddressSerialNumber_Read_req";

    cemi_client.application_layer.A_DomainAddressSerialNumber_Read_req(ack_request, hop_count_type, priority, serial_number, Lcon);
}

void Stack::A_DomainAddressSerialNumber_Read_res(const KNX::Ack_Request ack_request, const KNX::Domain_Address domain_address, const KNX::Hop_Count_Type hop_count_type, const KNX::Priority priority, const KNX::Serial_Number serial_number, std::function<void(const KNX::Status a_status)> Rcon)
{
    qInfo() << "A_DomainAddressSerialNumber_Read_res";

    cemi_client.application_layer.A_DomainAddressSerialNumber_Read_res(ack_request, domain_address, hop_count_type, priority, serial_number, Rcon);
}

void Stack::A_DomainAddressSerialNumber_Write_req(const KNX::Ack_Request ack_request, const KNX::Domain_Address domain_address, const KNX::Hop_Count_Type hop_count_type, const KNX::Priority priority, const KNX::Serial_Number serial_number, std::function<void(const KNX::Status a_status)> Lcon)
{
    qInfo() << "A_DomainAddressSerialNumber_Write_req";

    cemi_client.application_layer.A_DomainAddressSerialNumber_Write_req(ack_request, domain_address, hop_count_type, priority, serial_number, Lcon);
}

void Stack::A_SystemNetworkParameter_Read_req(const KNX::Hop_Count_Type hop_count_type, const KNX::System_Parameter_Type parameter_type, const KNX::Priority priority, const KNX::Parameter_Test_Info test_info, std::function<void(const KNX::Status a_status)> Lcon)
{
    qInfo() << "A_SystemNetworkParameter_Read_req";

    cemi_client.application_layer.A_SystemNetworkParameter_Read_req(hop_count_type, parameter_type, priority, test_info, Lcon);
}

void Stack::A_SystemNetworkParameter_Read_res(const KNX::Hop_Count_Type hop_count_type, const KNX::System_Parameter_Type parameter_type, const KNX::Priority priority, const KNX::Parameter_Test_Info test_info, const KNX::Parameter_Test_Result test_result, std::function<void(const KNX::Status a_status)> Rcon)
{
    qInfo() << "A_SystemNetworkParameter_Read_res";

    cemi_client.application_layer.A_SystemNetworkParameter_Read_res(hop_count_type, parameter_type, priority, test_info, test_result, Rcon);
}

void Stack::A_SystemNetworkParameter_Write_req(const KNX::Hop_Count_Type hop_count_type, const KNX::System_Parameter_Type parameter_type, const KNX::Priority priority, const std::vector<uint8_t> value, std::function<void(const KNX::Status a_status)> Lcon)
{
    qInfo() << "A_SystemNetworkParameter_Write_req";

    cemi_client.application_layer.A_SystemNetworkParameter_Write_req(hop_count_type, parameter_type, priority, value, Lcon);
}

/* Services (Individual) */

void Stack::A_DeviceDescriptor_Read_req(const KNX::Ack_Request ack_request, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Individual asap, const KNX::Descriptor_Type descriptor_type, std::function<void(const KNX::Status a_status)> Lcon)
{
    qInfo() << "A_DeviceDescriptor_Read_req";

    cemi_client.application_layer.A_DeviceDescriptor_Read_req(ack_request, priority, hop_count_type, asap, descriptor_type, Lcon);
}

void Stack::A_DeviceDescriptor_Read_res(const KNX::Ack_Request ack_request, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Individual asap, const KNX::Descriptor_Type descriptor_type, const std::vector<uint8_t> device_descriptor, std::function<void(const KNX::Status a_status)> Rcon)
{
    qInfo() << "A_DeviceDescriptor_Read_res";

    cemi_client.application_layer.A_DeviceDescriptor_Read_res(ack_request, priority, hop_count_type, asap, descriptor_type, device_descriptor, Rcon);
}

void Stack::A_Restart_req(const KNX::Ack_Request ack_request, const uint8_t channel_number, const KNX::Restart_Erase_Code erase_code, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::Restart_Type restart_type, const KNX::ASAP_Individual asap, std::function<void(const KNX::Status a_status)> Lcon)
{
    qInfo() << "A_Restart_req";

    cemi_client.application_layer.A_Restart_req(ack_request, channel_number, erase_code, priority, hop_count_type, restart_type, asap, Lcon);
}

void Stack::A_Restart_res(const KNX::Restart_Error_Code error_code, const KNX::Priority priority, const uint16_t process_time, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Individual asap, std::function<void(const KNX::Status a_status)> Rcon)
{
    qInfo() << "A_Restart_res";

    cemi_client.application_layer.A_Restart_res(error_code, priority, process_time, hop_count_type, asap, Rcon);
}

void Stack::A_FileStream_InfoReport_req(const KNX::Ack_Request ack_request, const KNX::ASAP_Individual asap, const KNX::File_Block file_block, const KNX::File_Block_Sequence_Number file_block_sequence_number, const KNX::File_Handle file_handle, const KNX::Hop_Count_Type hop_count_type, std::function<void(const KNX::Status a_status)> Lcon)
{
    qInfo() << "A_FileStream_InfoReport_req";

    cemi_client.application_layer.A_FileStream_InfoReport_req(ack_request, asap, file_block, file_block_sequence_number, file_handle, hop_count_type, Lcon);
}

void Stack::A_PropertyValue_Read_req(const KNX::Ack_Request ack_request, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Individual asap, const KNX::Object_Index object_index, const KNX::Property_Id property_id, const KNX::PropertyValue_Nr_Of_Elem nr_of_elem, const KNX::PropertyValue_Start_Index start_index, std::function<void(const KNX::Status a_status)> Lcon)
{
    qInfo() << "A_PropertyValue_Read_req";

    cemi_client.application_layer.A_PropertyValue_Read_req(ack_request, priority, hop_count_type, asap, object_index, property_id, nr_of_elem, start_index, Lcon);
}

void Stack::A_PropertyValue_Read_res(const KNX::Ack_Request ack_request, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Individual asap, const KNX::Object_Index object_index, const KNX::Property_Id property_id, const KNX::PropertyValue_Nr_Of_Elem nr_of_elem, const KNX::PropertyValue_Start_Index start_index, const std::vector<uint8_t> data, std::function<void(const KNX::Status a_status)> Rcon)
{
    qInfo() << "A_PropertyValue_Read_res";

    cemi_client.application_layer.A_PropertyValue_Read_res(ack_request, priority, hop_count_type, asap, object_index, property_id, nr_of_elem, start_index, data, Rcon);
}

void Stack::A_PropertyValue_Write_req(const KNX::Ack_Request ack_request, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Individual asap, const KNX::Object_Index object_index, const KNX::Property_Id property_id, const KNX::PropertyValue_Nr_Of_Elem nr_of_elem, const KNX::PropertyValue_Start_Index start_index, const std::vector<uint8_t> data, std::function<void(const KNX::Status a_status)> Lcon)
{
    qInfo() << "A_PropertyValue_Write_req";

    cemi_client.application_layer.A_PropertyValue_Write_req(ack_request, priority, hop_count_type, asap, object_index, property_id, nr_of_elem, start_index, data, Lcon);
}

void Stack::A_PropertyDescription_Read_req(const KNX::Ack_Request ack_request, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Individual asap, const KNX::Object_Index object_index, const KNX::Property_Id property_id, const KNX::Property_Index property_index, std::function<void(const KNX::Status a_status)> Lcon)
{
    qInfo() << "A_PropertyDescription_Read_req";

    cemi_client.application_layer.A_PropertyDescription_Read_req(ack_request, priority, hop_count_type, asap, object_index, property_id, property_index, Lcon);
}

void Stack::A_PropertyDescription_Read_res(const KNX::Ack_Request ack_request, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Individual asap, const KNX::Object_Index object_index, const KNX::Property_Id property_id, const KNX::Property_Index property_index, const bool write_enable, const KNX::Property_Type type, const KNX::Max_Nr_Of_Elem max_nr_of_elem, const KNX::Access access, std::function<void(const KNX::Status a_status)> Rcon)
{
    qInfo() << "A_PropertyDescription_Read_res";

    cemi_client.application_layer.A_PropertyDescription_Read_res(ack_request, priority, hop_count_type, asap, object_index, property_id, property_index, write_enable, type, max_nr_of_elem, access, Rcon);
}

void Stack::A_Link_Read_req(const KNX::ASAP_Individual asap, const KNX::Group_Object_Number group_object_number, const KNX::Priority priority, const KNX::Link_Start_Index start_index, std::function<void(const KNX::Status a_status)> Lcon)
{
    qInfo() << "A_Link_Read_req";

    cemi_client.application_layer.A_Link_Read_req(asap, group_object_number, priority, start_index, Lcon);
}

void Stack::A_Link_Read_res(const KNX::ASAP_Individual asap, const std::vector<KNX::Group_Address> group_address_list, const uint8_t group_object_number, const KNX::Priority priority, const KNX::Link_Sending_Address sending_address, const KNX::Link_Start_Index start_index, std::function<void(const KNX::Status a_status)> Rcon)
{
    qInfo() << "A_Link_Read_res";

    cemi_client.application_layer.A_Link_Read_res(asap, group_address_list, group_object_number, priority, sending_address, start_index, Rcon);
}

void Stack::A_Link_Write_req(const KNX::ASAP_Individual asap, const KNX::Link_Write_Flags flags, const KNX::Group_Address group_address, const uint8_t group_object_number, const KNX::Priority priority, std::function<void(const KNX::Status a_status)> Lcon)
{
    qInfo() << "A_Link_Write_req";

    cemi_client.application_layer.A_Link_Write_req(asap, flags, group_address, group_object_number, priority, Lcon);
}

void Stack::A_FunctionPropertyCommand_req(const KNX::Ack_Request ack_request, const KNX::ASAP_Individual asap, const KNX::Comm_Mode comm_mode, const std::vector<uint8_t> data, const KNX::Hop_Count_Type hop_count_type, const KNX::Object_Index object_index, const KNX::Priority priority, const KNX::Property_Id property_id, std::function<void(const KNX::Status a_status)> Lcon)
{
    qInfo() << "A_FunctionPropertyCommand_req";

    cemi_client.application_layer.A_FunctionPropertyCommand_req(ack_request, asap, comm_mode, data, hop_count_type, object_index, priority, property_id, Lcon);
}

void Stack::A_FunctionPropertyCommand_res(const KNX::Ack_Request ack_request, const KNX::ASAP_Individual asap, const KNX::Comm_Mode comm_mode, const std::vector<uint8_t> data, const KNX::Hop_Count_Type hop_count_type, const KNX::Object_Index object_index, const KNX::Priority priority, const KNX::Property_Id property_id, const std::optional<KNX::Property_Return_Code> return_code, std::function<void(const KNX::Status a_status)> Rcon)
{
    qInfo() << "A_FunctionPropertyCommand_res";

    cemi_client.application_layer.A_FunctionPropertyCommand_res(ack_request, asap, comm_mode, data, hop_count_type, object_index, priority, property_id, return_code, Rcon);
}

void Stack::A_FunctionPropertyState_Read_req(const KNX::ASAP_Individual asap, const KNX::Ack_Request ack_request, const KNX::Comm_Mode comm_mode, const std::vector<uint8_t> data, const KNX::Hop_Count_Type hop_count_type, const KNX::Object_Index object_index, const KNX::Priority priority, const KNX::Property_Id property_id, std::function<void(const KNX::Status a_status)> Lcon)
{
    qInfo() << "A_FunctionPropertyState_Read_req";

    cemi_client.application_layer.A_FunctionPropertyState_Read_req(asap, ack_request, comm_mode, data, hop_count_type, object_index, priority, property_id, Lcon);
}

void Stack::A_FunctionPropertyState_Read_res(const KNX::Ack_Request ack_request, const KNX::ASAP_Individual asap, const KNX::Comm_Mode comm_mode, const std::vector<uint8_t> data, const KNX::Hop_Count_Type hop_count_type, const KNX::Object_Index object_index, const KNX::Priority priority, const KNX::Property_Id property_id, const std::optional<KNX::Property_Return_Code> return_code, std::function<void(const KNX::Status a_status)> Rcon)
{
    qInfo() << "A_FunctionPropertyState_Read_res";

    cemi_client.application_layer.A_FunctionPropertyState_Read_res(ack_request, asap, comm_mode, data, hop_count_type, object_index, priority, property_id, return_code, Rcon);
}

/* Services (Connected) */

void Stack::A_ADC_Read_req(const KNX::Ack_Request ack_request, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::ADC_Channel_Nr channel_nr, const uint8_t read_count, std::function<void(const KNX::Status a_status)> Lcon)
{
    qInfo() << "A_ADC_Read_req";

    cemi_client.application_layer.A_ADC_Read_req(ack_request, priority, hop_count_type, asap, channel_nr, read_count, Lcon);
}

void Stack::A_ADC_Read_res(const KNX::Ack_Request ack_request, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::ADC_Channel_Nr channel_nr, const uint8_t read_count, const uint16_t sum, std::function<void(const KNX::Status a_status)> Rcon)
{
    qInfo() << "A_ADC_Read_res";

    cemi_client.application_layer.A_ADC_Read_res(ack_request, priority, hop_count_type, asap, channel_nr, read_count, sum, Rcon);
}

void Stack::A_Memory_Read_req(const KNX::Ack_Request ack_request, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::Memory_Number number, const KNX::Memory_Address memory_address, std::function<void(const KNX::Status a_status)> Lcon)
{
    qInfo() << "A_Memory_Read_req";

    cemi_client.application_layer.A_Memory_Read_req(ack_request, priority, hop_count_type, asap, number, memory_address, Lcon);
}

void Stack::A_Memory_Read_res(const KNX::Ack_Request ack_request, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::Memory_Number number, const KNX::Memory_Address memory_address, const std::vector<uint8_t> data, std::function<void(const KNX::Status a_status)> Rcon)
{
    qInfo() << "A_Memory_Read_res";

    cemi_client.application_layer.A_Memory_Read_res(ack_request, priority, hop_count_type, asap, number, memory_address, data, Rcon);
}

void Stack::A_Memory_Write_req(const KNX::Ack_Request ack_request, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::Memory_Number number, const KNX::Memory_Address memory_address, const std::vector<uint8_t> data, std::function<void(const KNX::Status a_status)> Lcon)
{
    qInfo() << "A_Memory_Write_req";

    cemi_client.application_layer.A_Memory_Write_req(ack_request, priority, hop_count_type, asap, number, memory_address, data, Lcon);
}

void Stack::A_Memory_Write_res(const KNX::Ack_Request ack_request, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::Memory_Number number, const KNX::Memory_Address memory_address, const std::vector<uint8_t> data, std::function<void(const KNX::Status a_status)> Rcon)
{
    qInfo() << "A_Memory_Write_res";

    cemi_client.application_layer.A_Memory_Write_res(ack_request, priority, hop_count_type, asap, number, memory_address, data, Rcon);
}

void Stack::A_MemoryBit_Write_req(const KNX::Ack_Request ack_request, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::MemoryBit_Number number, const KNX::MemoryBit_Address memory_address, const std::vector<uint8_t> and_data, const std::vector<uint8_t> xor_data, std::function<void(const KNX::Status a_status)> Lcon)
{
    qInfo() << "A_MemoryBit_Write_req";

    cemi_client.application_layer.A_MemoryBit_Write_req(ack_request, priority, hop_count_type, asap, number, memory_address, and_data, xor_data, Lcon);
}

void Stack::A_MemoryBit_Write_res(const KNX::ASAP_Connected asap, const KNX::Priority priority, const KNX::MemoryBit_Number number, const KNX::MemoryBit_Address memory_address, const std::vector<uint8_t> data, std::function<void(const KNX::Status a_status)> Rcon)
{
    qInfo() << "A_MemoryBit_Write_res";

    cemi_client.application_layer.A_MemoryBit_Write_res(asap, priority, number, memory_address, data, Rcon);
}

void Stack::A_UserMemory_Read_req(const KNX::Ack_Request ack_request, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::UserMemory_Number number, const KNX::UserMemory_Address memory_address, std::function<void(const KNX::Status a_status)> Lcon)
{
    qInfo() << "A_UserMemory_Read_req";

    cemi_client.application_layer.A_UserMemory_Read_req(ack_request, priority, hop_count_type, asap, number, memory_address, Lcon);
}

void Stack::A_UserMemory_Read_res(const KNX::Ack_Request ack_request, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::UserMemory_Number number, const KNX::UserMemory_Address memory_address, const std::vector<uint8_t> data, std::function<void(const KNX::Status a_status)> Rcon)
{
    qInfo() << "A_UserMemory_Read_res";

    cemi_client.application_layer.A_UserMemory_Read_res(ack_request, priority, hop_count_type, asap, number, memory_address, data, Rcon);
}

void Stack::A_UserMemory_Write_req(const KNX::Ack_Request ack_request, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::UserMemory_Number number, const KNX::UserMemory_Address memory_address, const std::vector<uint8_t> data, std::function<void(const KNX::Status a_status)> Lcon)
{
    qInfo() << "A_UserMemory_Write_req";

    cemi_client.application_layer.A_UserMemory_Write_req(ack_request, priority, hop_count_type, asap, number, memory_address, data, Lcon);
}

void Stack::A_UserMemory_Write_res(const KNX::Ack_Request ack_request, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::UserMemory_Number number, const KNX::UserMemory_Address memory_address, const std::vector<uint8_t> data, std::function<void(const KNX::Status a_status)> Rcon)
{
    qInfo() << "A_UserMemory_Write_res";

    cemi_client.application_layer.A_UserMemory_Write_res(ack_request, priority, hop_count_type, asap, number, memory_address, data, Rcon);
}

void Stack::A_UserMemoryBit_Write_req(const KNX::Ack_Request ack_request, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::UserMemoryBit_Number number, const KNX::UserMemoryBit_Address memory_address, const std::vector<uint8_t> and_data, const std::vector<uint8_t> xor_data, std::function<void(const KNX::Status a_status)> Lcon)
{
    qInfo() << "A_UserMemoryBit_Write_req";

    cemi_client.application_layer.A_UserMemoryBit_Write_req(ack_request, priority, hop_count_type, asap, number, memory_address, and_data, xor_data, Lcon);
}

void Stack::A_UserMemoryBit_Write_res(const KNX::Ack_Request ack_request, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::UserMemoryBit_Number number, const KNX::UserMemoryBit_Address memory_address, const std::vector<uint8_t> data, std::function<void(const KNX::Status a_status)> Rcon)
{
    qInfo() << "A_UserMemoryBit_Write_res";

    cemi_client.application_layer.A_UserMemoryBit_Write_res(ack_request, priority, hop_count_type, asap, number, memory_address, data, Rcon);
}

void Stack::A_UserManufacturerInfo_Read_req(const KNX::Ack_Request ack_request, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, std::function<void(const KNX::Status a_status)> Lcon)
{
    qInfo() << "A_UserManufacturerInfo_Read_req";

    cemi_client.application_layer.A_UserManufacturerInfo_Read_req(ack_request, priority, hop_count_type, asap, Lcon);
}

void Stack::A_UserManufacturerInfo_Read_res(const KNX::Ack_Request ack_request, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::MFact_Info mfact_info, std::function<void(const KNX::Status a_status)> Rcon)
{
    qInfo() << "A_UserManufacturerInfo_Read_res";

    cemi_client.application_layer.A_UserManufacturerInfo_Read_res(ack_request, priority, hop_count_type, asap, mfact_info, Rcon);
}

void Stack::A_Authorize_Request_req(const KNX::Ack_Request ack_request, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::Key key, std::function<void(const KNX::Status a_status)> Lcon)
{
    qInfo() << "A_Authorize_Request_req";

    cemi_client.application_layer.A_Authorize_Request_req(ack_request, priority, hop_count_type, asap, key, Lcon);
}

void Stack::A_Authorize_Request_res(const KNX::Ack_Request ack_request, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::Level level, std::function<void(const KNX::Status a_status)> Rcon)
{
    qInfo() << "A_Authorize_Request_res";

    cemi_client.application_layer.A_Authorize_Request_res(ack_request, priority, hop_count_type, asap, level, Rcon);
}

void Stack::A_Key_Write_req(const KNX::Ack_Request ack_request, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::Level level, const KNX::Key key, std::function<void(const KNX::Status a_status)> Lcon)
{
    qInfo() << "A_Key_Write_req";

    cemi_client.application_layer.A_Key_Write_req(ack_request, priority, hop_count_type, asap, level, key, Lcon);
}

void Stack::A_Key_Write_res(const KNX::Ack_Request ack_request, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::Level level, std::function<void(const KNX::Status a_status)> Rcon)
{
    qInfo() << "A_Key_Write_res";

    cemi_client.application_layer.A_Key_Write_res(ack_request, priority, hop_count_type, asap, level, Rcon);
}

KNX::NM_IndividualAddress_Read * Stack::makeNetworkManagementIndividualAddressRead()
{
    return new KNX::NM_IndividualAddress_Read(cemi_client.io_context, cemi_client.application_layer);
}

KNX::NM_IndividualAddress_Write * Stack::makeNetworkManagementIndividualAddressWrite()
{
    return new KNX::NM_IndividualAddress_Write(cemi_client.io_context, cemi_client.application_layer);
}

KNX::NM_IndividualAddress_SerialNumber_Read * Stack::makeNetworkManagementIndividualAddressSerialNumberRead()
{
    return new KNX::NM_IndividualAddress_SerialNumber_Read(cemi_client.io_context, cemi_client.application_layer);
}

KNX::NM_IndividualAddress_SerialNumber_Write * Stack::makeNetworkManagementIndividualAddressSerialNumberWrite()
{
    return new KNX::NM_IndividualAddress_SerialNumber_Write(cemi_client.io_context, cemi_client.application_layer);
}

KNX::NM_IndividualAddress_SerialNumber_Write2 * Stack::makeNetworkManagementIndividualAddressSerialNumberWrite2()
{
    return new KNX::NM_IndividualAddress_SerialNumber_Write2(cemi_client.io_context, cemi_client.application_layer);
}

void Stack::A_Connect_ind_initiator()
{
    cemi_client.application_layer.A_Connect_ind([this](const KNX::ASAP_Connected asap) -> void {
        qInfo() << "A_Connect_ind";

        Q_EMIT A_Connect_ind(asap);

        A_Connect_ind_initiator();
    });
}

void Stack::A_Disconnect_ind_initiator()
{
    cemi_client.application_layer.A_Disconnect_ind([this](const KNX::ASAP_Connected asap) -> void {
        qInfo() << "A_Disconnect_ind";

        Q_EMIT A_Disconnect_ind(asap);

        A_Disconnect_ind_initiator();
    });
}

/* Services (Group) */

void Stack::A_GroupValue_Read_ind_initiator()
{
    cemi_client.application_layer.A_GroupValue_Read_ind([this](const KNX::ASAP_Group asap, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type) -> void {
        qInfo() << "A_GroupValue_Read_ind";

        Q_EMIT A_GroupValue_Read_ind(asap, priority, hop_count_type);

        A_GroupValue_Read_ind_initiator();
    });
}

void Stack::A_GroupValue_Read_Acon_initiator()
{
    cemi_client.application_layer.A_GroupValue_Read_Acon([this](const KNX::ASAP_Group asap, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const std::vector<uint8_t> data) -> void {
        qInfo() << "A_GroupValue_Read_Acon";

        Q_EMIT A_GroupValue_Read_Acon(asap, priority, hop_count_type, data);

        A_GroupValue_Read_Acon_initiator();
    });
}

void Stack::A_GroupValue_Write_ind_initiator()
{
    cemi_client.application_layer.A_GroupValue_Write_ind([this](const KNX::ASAP_Group asap, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const std::vector<uint8_t> data) -> void {
        qInfo() << "A_GroupValue_Write_ind";

        Q_EMIT A_GroupValue_Write_ind(asap, priority, hop_count_type, data);

        A_GroupValue_Write_ind_initiator();
    });
}

/* Services (Broadcast) */

void Stack::A_IndividualAddress_Write_ind_initiator()
{
    cemi_client.application_layer.A_IndividualAddress_Write_ind([this](const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::Individual_Address newaddress) -> void {
        qInfo() << "A_IndividualAddress_Write_ind";

        Q_EMIT A_IndividualAddress_Write_ind(priority, hop_count_type, newaddress);

        A_IndividualAddress_Write_ind_initiator();
    });
}

void Stack::A_IndividualAddress_Read_ind_initiator()
{
    cemi_client.application_layer.A_IndividualAddress_Read_ind([this](const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type) -> void {
        qInfo() << "A_IndividualAddress_Read_ind";

        Q_EMIT A_IndividualAddress_Read_ind(priority, hop_count_type);

        A_IndividualAddress_Read_ind_initiator();
    });
}

void Stack::A_IndividualAddress_Read_Acon_initiator()
{
    cemi_client.application_layer.A_IndividualAddress_Read_Acon([this](const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::Individual_Address individual_address) -> void {
        qInfo() << "A_IndividualAddress_Read_Acon";

        Q_EMIT A_IndividualAddress_Read_Acon(priority, hop_count_type, individual_address);

        A_IndividualAddress_Read_Acon_initiator();
    });
}

void Stack::A_IndividualAddressSerialNumber_Read_ind_initiator()
{
    cemi_client.application_layer.A_IndividualAddressSerialNumber_Read_ind([this](const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::Serial_Number serial_number) -> void {
        qInfo() << "A_IndividualAddressSerialNumber_Read_ind";

        Q_EMIT A_IndividualAddressSerialNumber_Read_ind(priority, hop_count_type, serial_number);

        A_IndividualAddressSerialNumber_Read_ind_initiator();
    });
}

void Stack::A_IndividualAddressSerialNumber_Read_Acon_initiator()
{
    cemi_client.application_layer.A_IndividualAddressSerialNumber_Read_Acon([this](const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::Serial_Number serial_number, const KNX::Individual_Address individual_address, const KNX::Domain_Address_2 domain_address) -> void {
        qInfo() << "A_IndividualAddressSerialNumber_Read_Acon";

        Q_EMIT A_IndividualAddressSerialNumber_Read_Acon(priority, hop_count_type, serial_number, individual_address, domain_address);

        A_IndividualAddressSerialNumber_Read_Acon_initiator();
    });
}

void Stack::A_IndividualAddressSerialNumber_Write_ind_initiator()
{
    cemi_client.application_layer.A_IndividualAddressSerialNumber_Write_ind([this](const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::Serial_Number serial_number, const KNX::Individual_Address newaddress) -> void {
        qInfo() << "A_IndividualAddressSerialNumber_Write_ind";

        Q_EMIT A_IndividualAddressSerialNumber_Write_ind(priority, hop_count_type, serial_number, newaddress);

        A_IndividualAddressSerialNumber_Write_ind_initiator();
    });
}

void Stack::A_NetworkParameter_Read_ind_initiator()
{
    cemi_client.application_layer.A_NetworkParameter_Read_ind([this](const KNX::ASAP_Individual asap, const KNX::Comm_Mode comm_mode_req, const KNX::Hop_Count_Type hop_count_type, const KNX::Parameter_Type parameter_type, const KNX::Priority priority, const KNX::Parameter_Test_Info test_info) -> void {
        qInfo() << "A_NetworkParameter_Read_ind";

        Q_EMIT A_NetworkParameter_Read_ind(asap, comm_mode_req, hop_count_type, parameter_type, priority, test_info);

        A_NetworkParameter_Read_ind_initiator();
    });
}

void Stack::A_NetworkParameter_Read_Acon_initiator()
{
    cemi_client.application_layer.A_NetworkParameter_Read_Acon([this](const KNX::ASAP_Individual asap, const KNX::Hop_Count_Type hop_count_type, const KNX::Individual_Address individual_address, const KNX::Parameter_Type parameter_type, const KNX::Priority priority, const KNX::Parameter_Test_Info_Result test_info_result) -> void {
        qInfo() << "A_NetworkParameter_Read_Acon";

        Q_EMIT A_NetworkParameter_Read_Acon(asap, hop_count_type, individual_address, parameter_type, priority, test_info_result);

        A_NetworkParameter_Read_Acon_initiator();
    });
}

void Stack::A_NetworkParameter_Write_ind_initiator()
{
    cemi_client.application_layer.A_NetworkParameter_Write_ind([this](const KNX::ASAP_Individual asap, const KNX::Parameter_Type parameter_type, const KNX::Priority priority, const std::vector<uint8_t> value) -> void {
        qInfo() << "A_NetworkParameter_Write_ind";

        Q_EMIT A_NetworkParameter_Write_ind(asap, parameter_type, priority, value);

        A_NetworkParameter_Write_ind_initiator();
    });
}

void Stack::A_NetworkParameter_InfoReport_ind_initiator()
{
    cemi_client.application_layer.A_NetworkParameter_InfoReport_ind([this](const KNX::ASAP_Individual asap, const KNX::Comm_Mode comm_mode_req, const KNX::Hop_Count_Type hop_count_type, const KNX::Parameter_Type parameter_type, const KNX::Priority priority, const KNX::Parameter_Test_Info_Result test_info_result) -> void {
        qInfo() << "A_NetworkParameter_InfoReport_ind";

        Q_EMIT A_NetworkParameter_InfoReport_ind(asap, comm_mode_req, hop_count_type, parameter_type, priority, test_info_result);

        A_NetworkParameter_InfoReport_ind_initiator();
    });
}

/* Services (System Broadcast) */

void Stack::A_DeviceDescriptor_InfoReport_ind_initiator()
{
    cemi_client.application_layer.A_DeviceDescriptor_InfoReport_ind([this](const KNX::Descriptor_Type descriptor_type, const std::vector<uint8_t> device_descriptor, const KNX::Hop_Count_Type hop_count_type, const KNX::Priority priority) -> void {
        qInfo() << "A_DeviceDescriptor_InfoReport_ind";

        Q_EMIT A_DeviceDescriptor_InfoReport_ind(descriptor_type, device_descriptor, hop_count_type, priority);

        A_DeviceDescriptor_InfoReport_ind_initiator();
    });
}

void Stack::A_DomainAddress_Write_ind_initiator()
{
    cemi_client.application_layer.A_DomainAddress_Write_ind([this](const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::Domain_Address domain_address_new) -> void {
        qInfo() << "A_DomainAddress_Write_ind";

        Q_EMIT A_DomainAddress_Write_ind(priority, hop_count_type, domain_address_new);

        A_DomainAddress_Write_ind_initiator();
    });
}

void Stack::A_DomainAddress_Read_ind_initiator()
{
    cemi_client.application_layer.A_DomainAddress_Read_ind([this](const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type) -> void {
        qInfo() << "A_DomainAddress_Read_ind";

        Q_EMIT A_DomainAddress_Read_ind(priority, hop_count_type);

        A_DomainAddress_Read_ind_initiator();
    });
}

void Stack::A_DomainAddress_Read_Acon_initiator()
{
    cemi_client.application_layer.A_DomainAddress_Read_Acon([this](const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Individual asap, const KNX::Domain_Address domain_address) -> void {
        qInfo() << "A_DomainAddress_Read_Acon";

        Q_EMIT A_DomainAddress_Read_Acon(priority, hop_count_type, asap, domain_address);

        A_DomainAddress_Read_Acon_initiator();
    });
}

void Stack::A_DomainAddressSelective_Read_ind_initiator()
{
    cemi_client.application_layer.A_DomainAddressSelective_Read_ind([this](const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASDU asdu) -> void {
        qInfo() << "A_DomainAddressSelective_Read_ind";

        Q_EMIT A_DomainAddressSelective_Read_ind(priority, hop_count_type, asdu);

        A_DomainAddressSelective_Read_ind_initiator();
    });
}

void Stack::A_DomainAddressSerialNumber_Read_ind_initiator()
{
    cemi_client.application_layer.A_DomainAddressSerialNumber_Read_ind([this](const KNX::Hop_Count_Type hop_count_type, const KNX::Priority priority, const KNX::Serial_Number serial_number) -> void {
        qInfo() << "A_DomainAddressSerialNumber_Read_ind";

        Q_EMIT A_DomainAddressSerialNumber_Read_ind(hop_count_type, priority, serial_number);

        A_DomainAddressSerialNumber_Read_ind_initiator();
    });
}

void Stack::A_DomainAddressSerialNumber_Read_Acon_initiator()
{
    cemi_client.application_layer.A_DomainAddressSerialNumber_Read_Acon([this](const KNX::Domain_Address domain_address, const KNX::Hop_Count_Type hop_count_type, const KNX::Priority priority, const KNX::Serial_Number serial_number) -> void {
        qInfo() << "A_DomainAddressSerialNumber_Read_Acon";

        Q_EMIT A_DomainAddressSerialNumber_Read_Acon(domain_address, hop_count_type, priority, serial_number);

        A_DomainAddressSerialNumber_Read_Acon_initiator();
    });
}

void Stack::A_DomainAddressSerialNumber_Write_ind_initiator()
{
    cemi_client.application_layer.A_DomainAddressSerialNumber_Write_ind([this](const KNX::Domain_Address domain_address, const KNX::Hop_Count_Type hop_count_type, const KNX::Priority priority, const KNX::Serial_Number serial_number) -> void {
        qInfo() << "A_DomainAddressSerialNumber_Write_ind";

        Q_EMIT A_DomainAddressSerialNumber_Write_ind(domain_address, hop_count_type, priority, serial_number);

        A_DomainAddressSerialNumber_Write_ind_initiator();
    });
}

void Stack::A_SystemNetworkParameter_Read_ind_initiator()
{
    cemi_client.application_layer.A_SystemNetworkParameter_Read_ind([this](const KNX::Hop_Count_Type hop_count_type, const KNX::System_Parameter_Type parameter_type, const KNX::Priority priority, const KNX::Parameter_Test_Info test_info) -> void {
        qInfo() << "A_SystemNetworkParameter_Read_ind";

        Q_EMIT A_SystemNetworkParameter_Read_ind(hop_count_type, parameter_type, priority, test_info);

        A_SystemNetworkParameter_Read_ind_initiator();
    });
}

void Stack::A_SystemNetworkParameter_Read_Acon_initiator()
{
    cemi_client.application_layer.A_SystemNetworkParameter_Read_Acon([this](const KNX::ASAP_Individual asap, const KNX::Hop_Count_Type hop_count_type, const KNX::System_Parameter_Type parameter_type, const KNX::Priority priority, const KNX::Parameter_Test_Info_Result test_info_result) -> void {
        qInfo() << "A_SystemNetworkParameter_Read_Acon";

        Q_EMIT A_SystemNetworkParameter_Read_Acon(asap, hop_count_type, parameter_type, priority, test_info_result);

        A_SystemNetworkParameter_Read_Acon_initiator();
    });
}

void Stack::A_SystemNetworkParameter_Write_ind_initiator()
{
    cemi_client.application_layer.A_SystemNetworkParameter_Write_ind([this](const KNX::Hop_Count_Type hop_count_type, const KNX::System_Parameter_Type parameter_type, const KNX::Priority priority, const std::vector<uint8_t> value) -> void {
        qInfo() << "A_SystemNetworkParameter_Write_ind";

        Q_EMIT A_SystemNetworkParameter_Write_ind(hop_count_type, parameter_type, priority, value);

        A_SystemNetworkParameter_Write_ind_initiator();
    });
}

/* Services (Individual) */

void Stack::A_DeviceDescriptor_Read_ind_initiator()
{
    cemi_client.application_layer.A_DeviceDescriptor_Read_ind([this](const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Individual asap, const KNX::Descriptor_Type descriptor_type) -> void {
        qInfo() << "A_DeviceDescriptor_Read_ind";

        Q_EMIT A_DeviceDescriptor_Read_ind(priority, hop_count_type, asap, descriptor_type);

        A_DeviceDescriptor_Read_ind_initiator();
    });
}

void Stack::A_DeviceDescriptor_Read_Acon_initiator()
{
    cemi_client.application_layer.A_DeviceDescriptor_Read_Acon([this](const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Individual asap, const KNX::Descriptor_Type descriptor_type, const std::vector<uint8_t> device_descriptor) -> void {
        qInfo() << "A_DeviceDescriptor_Read_Acon";

        Q_EMIT A_DeviceDescriptor_Read_Acon(priority, hop_count_type, asap, descriptor_type, device_descriptor);

        A_DeviceDescriptor_Read_Acon_initiator();
    });
}

void Stack::A_Restart_ind_initiator()
{
    cemi_client.application_layer.A_Restart_ind([this](const KNX::Restart_Erase_Code erase_code, const uint8_t channel_number, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Individual asap) -> void {
        qInfo() << "A_Restart_ind";

        Q_EMIT A_Restart_ind(erase_code, channel_number, priority, hop_count_type, asap);

        A_Restart_ind_initiator();
    });
}

void Stack::A_FileStream_InfoReport_ind_initiator()
{
    cemi_client.application_layer.A_FileStream_InfoReport_ind([this](const KNX::ASAP_Individual asap, const KNX::File_Block file_block, const KNX::File_Block_Sequence_Number file_block_sequence_number, const KNX::File_Handle file_handle, const KNX::Hop_Count_Type hop_count_type) -> void {
        qInfo() << "A_FileStream_InfoReport_ind";

        Q_EMIT A_FileStream_InfoReport_ind(asap, file_block, file_block_sequence_number, file_handle, hop_count_type);

        A_FileStream_InfoReport_ind_initiator();
    });
}

void Stack::A_PropertyValue_Read_ind_initiator()
{
    cemi_client.application_layer.A_PropertyValue_Read_ind([this](const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Individual asap, const KNX::Object_Index object_index, const KNX::Property_Id property_id, const KNX::PropertyValue_Nr_Of_Elem nr_of_elem, const KNX::PropertyValue_Start_Index start_index) -> void {
        qInfo() << "A_PropertyValue_Read_ind";

        Q_EMIT A_PropertyValue_Read_ind(priority, hop_count_type, asap, object_index, property_id, nr_of_elem, start_index);

        A_PropertyValue_Read_ind_initiator();
    });
}

void Stack::A_PropertyValue_Read_Acon_initiator()
{
    cemi_client.application_layer.A_PropertyValue_Read_Acon([this](const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Individual asap, const KNX::Object_Index object_index, const KNX::Property_Id property_id, const KNX::PropertyValue_Nr_Of_Elem nr_of_elem, const KNX::PropertyValue_Start_Index start_index, const std::vector<uint8_t> data) -> void {
        qInfo() << "A_PropertyValue_Read_Acon";

        Q_EMIT A_PropertyValue_Read_Acon(priority, hop_count_type, asap, object_index, property_id, nr_of_elem, start_index, data);

        A_PropertyValue_Read_Acon_initiator();
    });
}

void Stack::A_PropertyValue_Write_ind_initiator()
{
    cemi_client.application_layer.A_PropertyValue_Write_ind([this](const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Individual asap, const KNX::Object_Index object_index, const KNX::Property_Id property_id, const KNX::PropertyValue_Nr_Of_Elem nr_of_elem, const KNX::PropertyValue_Start_Index start_index, const std::vector<uint8_t> data) -> void {
        qInfo() << "A_PropertyValue_Write_ind";

        Q_EMIT A_PropertyValue_Write_ind(priority, hop_count_type, asap, object_index, property_id, nr_of_elem, start_index, data);

        A_PropertyValue_Write_ind_initiator();
    });
}

void Stack::A_PropertyDescription_Read_ind_initiator()
{
    cemi_client.application_layer.A_PropertyDescription_Read_ind([this](const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Individual asap, const KNX::Object_Index object_index, const KNX::Property_Id property_id, const KNX::Property_Index property_index) -> void {
        qInfo() << "A_PropertyDescription_Read_ind";

        Q_EMIT A_PropertyDescription_Read_ind(priority, hop_count_type, asap, object_index, property_id, property_index);

        A_PropertyDescription_Read_ind_initiator();
    });
}

void Stack::A_PropertyDescription_Read_Acon_initiator()
{
    cemi_client.application_layer.A_PropertyDescription_Read_Acon([this](const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Individual asap, const KNX::Object_Index object_index, const KNX::Property_Id property_id, const KNX::Property_Index property_index, const bool write_enable, const KNX::Property_Type type, const KNX::Max_Nr_Of_Elem max_nr_of_elem, const KNX::Access access) -> void {
        qInfo() << "A_PropertyDescription_Read_Acon";

        Q_EMIT A_PropertyDescription_Read_Acon(priority, hop_count_type, asap, object_index, property_id, property_index, write_enable, type, max_nr_of_elem, access);

        A_PropertyDescription_Read_Acon_initiator();
    });
}

void Stack::A_FunctionPropertyCommand_ind_initiator()
{
    cemi_client.application_layer.A_FunctionPropertyCommand_ind([this](const KNX::ASAP_Individual asap, const KNX::Comm_Mode comm_mode, const std::vector<uint8_t> data, const KNX::Hop_Count_Type hop_count_type, const KNX::Object_Index object_index, const KNX::Priority priority, const KNX::Property_Id property_id) -> void {
        qInfo() << "A_FunctionPropertyCommand_ind";

        Q_EMIT A_FunctionPropertyCommand_ind(asap, comm_mode, data, hop_count_type, object_index, priority, property_id);

        A_FunctionPropertyCommand_ind_initiator();
    });
}

void Stack::A_FunctionPropertyCommand_Acon_initiator()
{
    cemi_client.application_layer.A_FunctionPropertyCommand_Acon([this](const KNX::ASAP_Individual asap, const KNX::Comm_Mode comm_mode, const KNX::Property_Value data, const KNX::Hop_Count_Type hop_count_type, const KNX::Object_Index object_index, const KNX::Priority priority, const KNX::Property_Id property_id, const std::optional<KNX::Property_Return_Code> return_code) -> void {
        qInfo() << "A_FunctionPropertyCommand_Acon";

        Q_EMIT A_FunctionPropertyCommand_Acon(asap, comm_mode, data, hop_count_type, object_index, priority, property_id, return_code);

        A_FunctionPropertyCommand_Acon_initiator();
    });
}

void Stack::A_FunctionPropertyState_Read_ind_initiator()
{
    cemi_client.application_layer.A_FunctionPropertyState_Read_ind([this](const KNX::ASAP_Individual asap, const KNX::Comm_Mode comm_mode, const std::vector<uint8_t> data, const KNX::Hop_Count_Type hop_count_type, const KNX::Object_Index object_index, const KNX::Priority priority, const KNX::Property_Id property_id) -> void {
        qInfo() << "A_FunctionPropertyState_Read_ind";

        Q_EMIT A_FunctionPropertyState_Read_ind(asap, comm_mode, data, hop_count_type, object_index, priority, property_id);

        A_FunctionPropertyState_Read_ind_initiator();
    });
}

void Stack::A_FunctionPropertyState_Read_Acon_initiator()
{
    cemi_client.application_layer.A_FunctionPropertyState_Read_Acon([this](const KNX::ASAP_Individual asap, const KNX::Comm_Mode comm_mode, const KNX::Property_Value data, const KNX::Hop_Count_Type hop_count_type, const KNX::Object_Index object_index, const KNX::Priority priority, const KNX::Property_Id property_id, const std::optional<KNX::Property_Return_Code> return_code) -> void {
        qInfo() << "A_FunctionPropertyState_Read_Acon";

        Q_EMIT A_FunctionPropertyState_Read_Acon(asap, comm_mode, data, hop_count_type, object_index, priority, property_id, return_code);

        A_FunctionPropertyState_Read_Acon_initiator();
    });
}

/* Services (Connected) */

void Stack::A_ADC_Read_ind_initiator()
{
    cemi_client.application_layer.A_ADC_Read_ind([this](const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::ADC_Channel_Nr channel_nr, const uint8_t read_count) -> void {
        qInfo() << "A_ADC_Read_ind";

        Q_EMIT A_ADC_Read_ind(priority, hop_count_type, asap, channel_nr, read_count);

        A_ADC_Read_ind_initiator();
    });
}

void Stack::A_ADC_Read_Acon_initiator()
{
    cemi_client.application_layer.A_ADC_Read_Acon([this](const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::ADC_Channel_Nr channel_nr, const uint8_t read_count, const uint16_t sum) -> void {
        qInfo() << "A_ADC_Read_Acon";

        Q_EMIT A_ADC_Read_Acon(priority, hop_count_type, asap, channel_nr, read_count, sum);

        A_ADC_Read_Acon_initiator();
    });
}

void Stack::A_Memory_Read_ind_initiator()
{
    cemi_client.application_layer.A_Memory_Read_ind([this](const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::Memory_Number number, const KNX::Memory_Address memory_address) -> void {
        qInfo() << "A_Memory_Read_ind";

        Q_EMIT A_Memory_Read_ind(priority, hop_count_type, asap, number, memory_address);

        A_Memory_Read_ind_initiator();
    });
}

void Stack::A_Memory_Read_Acon_initiator()
{
    cemi_client.application_layer.A_Memory_Read_Acon([this](const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::Memory_Number number, const KNX::Memory_Address memory_address, const std::vector<uint8_t> data) -> void {
        qInfo() << "A_Memory_Read_Acon";

        Q_EMIT A_Memory_Read_Acon(priority, hop_count_type, asap, number, memory_address, data);

        A_Memory_Read_Acon_initiator();
    });
}

void Stack::A_Memory_Write_ind_initiator()
{
    cemi_client.application_layer.A_Memory_Write_ind([this](const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::Memory_Number number, const KNX::Memory_Address memory_address, const std::vector<uint8_t> data) -> void {
        qInfo() << "A_Memory_Write_ind";

        Q_EMIT A_Memory_Write_ind(priority, hop_count_type, asap, number, memory_address, data);

        A_Memory_Write_ind_initiator();
    });
}

void Stack::A_Memory_Write_Acon_initiator()
{
    cemi_client.application_layer.A_Memory_Write_Acon([this](const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::Memory_Number number, const KNX::Memory_Address memory_address, const std::vector<uint8_t> data) -> void {
        qInfo() << "A_Memory_Write_Acon";

        Q_EMIT A_Memory_Write_Acon(priority, hop_count_type, asap, number, memory_address, data);

        A_Memory_Write_Acon_initiator();
    });
}

void Stack::A_MemoryBit_Write_ind_initiator()
{
    cemi_client.application_layer.A_MemoryBit_Write_ind([this](const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::MemoryBit_Number number, const KNX::MemoryBit_Address memory_address, const std::vector<uint8_t> and_data, const std::vector<uint8_t> xor_data) -> void {
        qInfo() << "A_MemoryBit_Write_ind";

        Q_EMIT A_MemoryBit_Write_ind(priority, hop_count_type, asap, number, memory_address, and_data, xor_data);

        A_MemoryBit_Write_ind_initiator();
    });
}

void Stack::A_UserMemory_Read_ind_initiator()
{
    cemi_client.application_layer.A_UserMemory_Read_ind([this](const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::UserMemory_Number number, const KNX::UserMemory_Address memory_address) -> void {
        qInfo() << "A_UserMemory_Read_ind";

        Q_EMIT A_UserMemory_Read_ind(priority, hop_count_type, asap, number, memory_address);

        A_UserMemory_Read_ind_initiator();
    });
}

void Stack::A_UserMemory_Read_Acon_initiator()
{
    cemi_client.application_layer.A_UserMemory_Read_Acon([this](const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::UserMemory_Number number, const KNX::UserMemory_Address memory_address, const std::vector<uint8_t> data) -> void {
        qInfo() << "A_UserMemory_Read_Acon";

        Q_EMIT A_UserMemory_Read_Acon(priority, hop_count_type, asap, number, memory_address, data);

        A_UserMemory_Read_Acon_initiator();
    });
}

void Stack::A_UserMemory_Write_ind_initiator()
{
    cemi_client.application_layer.A_UserMemory_Write_ind([this](const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::UserMemory_Number number, const KNX::UserMemory_Address memory_address, const std::vector<uint8_t> data) -> void {
        qInfo() << "A_UserMemory_Write_ind";

        Q_EMIT A_UserMemory_Write_ind(priority, hop_count_type, asap, number, memory_address, data);

        A_UserMemory_Write_ind_initiator();
    });
}

void Stack::A_UserMemory_Write_Acon_initiator()
{
    cemi_client.application_layer.A_UserMemory_Write_Acon([this](const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::UserMemory_Number number, const KNX::UserMemory_Address memory_address, const std::vector<uint8_t> data) -> void {
        qInfo() << "A_UserMemory_Write_Acon";

        Q_EMIT A_UserMemory_Write_Acon(priority, hop_count_type, asap, number, memory_address, data);

        A_UserMemory_Write_Acon_initiator();
    });
}

void Stack::A_UserMemoryBit_Write_ind_initiator()
{
    cemi_client.application_layer.A_UserMemoryBit_Write_ind([this](const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::UserMemoryBit_Number number, const KNX::UserMemoryBit_Address memory_address, const std::vector<uint8_t> and_data, const std::vector<uint8_t> xor_data) -> void {
        qInfo() << "A_UserMemoryBit_Write_ind";

        Q_EMIT A_UserMemoryBit_Write_ind(priority, hop_count_type, asap, number, memory_address, and_data, xor_data);

        A_UserMemoryBit_Write_ind_initiator();
    });
}

void Stack::A_UserMemoryBit_Write_Acon_initiator()
{
    cemi_client.application_layer.A_UserMemoryBit_Write_Acon([this](const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::UserMemoryBit_Number number, const KNX::UserMemoryBit_Address memory_address, const std::vector<uint8_t> data) -> void {
        qInfo() << "A_UserMemoryBit_Write_Acon";

        Q_EMIT A_UserMemoryBit_Write_Acon(priority, hop_count_type, asap, number, memory_address, data);

        A_UserMemoryBit_Write_Acon_initiator();
    });
}

void Stack::A_UserManufacturerInfo_Read_ind_initiator()
{
    cemi_client.application_layer.A_UserManufacturerInfo_Read_ind([this](const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap) -> void {
        qInfo() << "A_UserManufacturerInfo_Read_ind";

        Q_EMIT A_UserManufacturerInfo_Read_ind(priority, hop_count_type, asap);

        A_UserManufacturerInfo_Read_ind_initiator();
    });
}

void Stack::A_UserManufacturerInfo_Read_Acon_initiator()
{
    cemi_client.application_layer.A_UserManufacturerInfo_Read_Acon([this](const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::MFact_Info mfact_info) -> void {
        qInfo() << "A_UserManufacturerInfo_Read_Acon";

        Q_EMIT A_UserManufacturerInfo_Read_Acon(priority, hop_count_type, asap, mfact_info);

        A_UserManufacturerInfo_Read_Acon_initiator();
    });
}

void Stack::A_Authorize_Request_ind_initiator()
{
    cemi_client.application_layer.A_Authorize_Request_ind([this](const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::Key key) -> void {
        qInfo() << "A_Authorize_Request_ind";

        Q_EMIT A_Authorize_Request_ind(priority, hop_count_type, asap, key);

        A_Authorize_Request_ind_initiator();
    });
}

void Stack::A_Authorize_Request_Acon_initiator()
{
    cemi_client.application_layer.A_Authorize_Request_Acon([this](const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::Level level) -> void {
        qInfo() << "A_Authorize_Request_Acon";

        Q_EMIT A_Authorize_Request_Acon(priority, hop_count_type, asap, level);

        A_Authorize_Request_Acon_initiator();
    });
}

void Stack::A_Key_Write_ind_initiator()
{
    cemi_client.application_layer.A_Key_Write_ind([this](const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::Level level, const KNX::Key key) -> void {
        qInfo() << "A_Key_Write_ind";

        Q_EMIT A_Key_Write_ind(priority, hop_count_type, asap, level, key);

        A_Key_Write_ind_initiator();
    });
}

void Stack::A_Key_Write_Acon_initiator()
{
    cemi_client.application_layer.A_Key_Write_Acon([this](const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::Level level) -> void {
        qInfo() << "A_Key_Write_Acon";

        Q_EMIT A_Key_Write_Acon(priority, hop_count_type, asap, level);

        A_Key_Write_Acon_initiator();
    });
}
