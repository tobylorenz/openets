#include "KNXnet.h"

#include <QDebug>
#include <QTimer>

KNXnet::KNXnet(QObject * parent) :
    QObject(parent),
    io_context(),
    work_guard(make_work_guard(io_context)),
    discovery_channel(io_context)
{
    QTimer::singleShot(0, this, &KNXnet::idleActions);
}

KNXnet::~KNXnet()
{
    work_guard.reset();
    io_context.stop();

    for (std::pair<const KNX::IP_Host_Protocol_Address_Information, KNX::IP_Communication_Channel *> communication_channel : communication_channels) {
        delete communication_channel.second;
    }
}

KNX::IP_Communication_Channel * KNXnet::communication_channel(const KNX::IP_Host_Protocol_Address_Information remote_control_endpoint)
{
    if (communication_channels.count(remote_control_endpoint) == 0) {
        /* communication channel and stack missing */
        communication_channels[remote_control_endpoint] = new KNX::IP_Communication_Channel(io_context, remote_control_endpoint);
        stacks[remote_control_endpoint] = new Stack(io_context, this);

        /* bind handlers */
        stacks[remote_control_endpoint]->cemi_req = std::bind(&KNX::IP_Communication_Channel::tunnelling_req, communication_channels[remote_control_endpoint], std::placeholders::_1);
        //communication_channels[remote_control_endpoint]->tunnelling_con = std::bind(&Stack::cemi_con, stacks[remote_control_endpoint], std::placeholders::_1);
        communication_channels[remote_control_endpoint]->tunnelling_ind = std::bind(&Stack::cemi_con_ind, stacks[remote_control_endpoint], std::placeholders::_1);

        /* signal */
        Q_EMIT connectionCreated(remote_control_endpoint);
    }

    return communication_channels[remote_control_endpoint];
}

Stack * KNXnet::stack(const KNX::IP_Host_Protocol_Address_Information remote_control_endpoint)
{
    if (stacks.count(remote_control_endpoint) == 0) {
        /* communication channel and stack missing */
        communication_channels[remote_control_endpoint] = new KNX::IP_Communication_Channel(io_context, remote_control_endpoint);
        stacks[remote_control_endpoint] = new Stack(io_context, this);

        /* bind handlers */
        stacks[remote_control_endpoint]->cemi_req = std::bind(&KNX::IP_Communication_Channel::tunnelling_req, communication_channels[remote_control_endpoint], std::placeholders::_1);
        //communication_channels[remote_control_endpoint]->tunnelling_con = std::bind(&Stack::cemi_con, stacks[remote_control_endpoint], std::placeholders::_1);
        communication_channels[remote_control_endpoint]->tunnelling_ind = std::bind(&Stack::cemi_con_ind, stacks[remote_control_endpoint], std::placeholders::_1);

        /* signal */
        Q_EMIT connectionCreated(remote_control_endpoint);
    }

    return stacks[remote_control_endpoint];
}

void KNXnet::idleActions()
{
    asio::io_context::count_type count = io_context.poll(); // @todo is this needed with a work guard?
    if (count > 0) {
        qInfo() << count << " asio events processed.";
    }

    QTimer::singleShot(0, this, &KNXnet::idleActions);
}
