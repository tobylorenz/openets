#pragma once

#include <KNX/03/08/02_Core.h>

#include "config.h"

#ifdef YAML_CPP_FOUND
#include <yaml-cpp/yaml.h>
#endif

std::string to_hex(const uint8_t value);
std::string to_hex(const uint16_t value);
std::string to_string(const std::array<uint8_t, 4> & ip_address);
std::string to_string(const std::array<uint8_t, 6> & mac_address);
std::string to_string(const std::array<char, 30> & name);
std::string to_string(const std::vector<uint16_t> & additional_addresses);
std::string to_string(const std::vector<uint8_t> & data);

#ifdef YAML_CPP_FOUND

/* 5.3 KNXnet/IP services */

std::string to_string(const KNX::Service_Type_Identifier service_type_identifier);

/* 5.4 Connection types */

std::string to_string(const KNX::Connection_Type_Code connection_type_code);

/* 5.5 Error codes */

std::string to_string(const KNX::Error_Code error_code);

/* 5.6 Description Information Block (DIB) */

std::string to_string(const KNX::Description_Type_Code description_type_code);
std::string to_string(const KNX::Medium_Code medium_code);

/* 5.7 Host protocol codes */

std::string to_string(const KNX::Host_Protocol_Code host_protocol_code);

/* 2 KNXnet/IP frames */

/* 2.2 Frame format */

YAML::Emitter & operator<<(YAML::Emitter & out, const KNX::Frame & obj);

/* 7.5 Placeholders */

/* 7.5.1 Host Protocol Address Information (HPAI) */

YAML::Emitter & operator<<(YAML::Emitter & out, const KNX::Host_Protocol_Address_Information & obj);

/* 7.5.2 Connection Request Information (CRI) */

YAML::Emitter & operator<<(YAML::Emitter & out, const KNX::Connection_Request_Information & obj);

/* 7.5.3 Connection Response Data Block (CRD) */

YAML::Emitter & operator<<(YAML::Emitter & out, const KNX::Connection_Response_Data_Block & obj);

/* 7.5.4 Description Information Block (DIB) */

YAML::Emitter & operator<<(YAML::Emitter & out, const KNX::Description_Information_Block & obj);
YAML::Emitter & operator<<(YAML::Emitter & out, const KNX::Device_Information_DIB & obj);
YAML::Emitter & operator<<(YAML::Emitter & out, const KNX::Supported_Service_Families_DIB & obj);
YAML::Emitter & operator<<(YAML::Emitter & out, const KNX::IP_Config_DIB & obj);
YAML::Emitter & operator<<(YAML::Emitter & out, const KNX::IP_Current_Config_DIB & obj);
YAML::Emitter & operator<<(YAML::Emitter & out, const KNX::Addresses_DIB & obj);
YAML::Emitter & operator<<(YAML::Emitter & out, const KNX::Manufacturer_Data_DIB & obj);

/* 7.6 Discovery */

YAML::Emitter & operator<<(YAML::Emitter & out, const KNX::Search_Request_Frame & obj);
YAML::Emitter & operator<<(YAML::Emitter & out, const KNX::Search_Response_Frame & obj);

/* 7.7 Self description */

YAML::Emitter & operator<<(YAML::Emitter & out, const KNX::Description_Request_Frame & obj);
YAML::Emitter & operator<<(YAML::Emitter & out, const KNX::Description_Response_Frame & obj);

/* 7.8 Connection management */

YAML::Emitter & operator<<(YAML::Emitter & out, const KNX::Connect_Request_Frame & obj);
YAML::Emitter & operator<<(YAML::Emitter & out, const KNX::Connect_Response_Frame & obj);
YAML::Emitter & operator<<(YAML::Emitter & out, const KNX::Connectionstate_Request_Frame & obj);
YAML::Emitter & operator<<(YAML::Emitter & out, const KNX::Connectionstate_Response_Frame & obj);
YAML::Emitter & operator<<(YAML::Emitter & out, const KNX::Disconnect_Request_Frame & obj);
YAML::Emitter & operator<<(YAML::Emitter & out, const KNX::Disconnect_Response_Frame & obj);

/* 8.6.2 Host Protocol Address Information */

YAML::Emitter & operator<<(YAML::Emitter & out, const KNX::IP_Host_Protocol_Address_Information & obj);

#endif
