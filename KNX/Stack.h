#pragma once

#include <asio.hpp>

#include <QObject>

#include <KNX/03/05/02/Network_Management/NM_IndividualAddress_Read.h>
#include <KNX/03/05/02/Network_Management/NM_IndividualAddress_SerialNumber_Read.h>
#include <KNX/03/05/02/Network_Management/NM_IndividualAddress_SerialNumber_Write.h>
#include <KNX/03/05/02/Network_Management/NM_IndividualAddress_SerialNumber_Write2.h>
#include <KNX/03/05/02/Network_Management/NM_IndividualAddress_Write.h>
#include <KNX/03/06/03/cEMI/CEMI_Client.h>

/**
 * KNX Stack adapter to Qt
 */
class Stack : public QObject
{
    Q_OBJECT

public:
    explicit Stack(asio::io_context & io_context, QObject * parent = nullptr);

    std::function<void(const std::vector<uint8_t> cemi_frame_data)> cemi_req;
    void cemi_con_ind(const std::vector<uint8_t> cemi_frame_data);

    void A_Connect_req(const KNX::ASAP_Individual asap, const KNX::Priority priority, std::function<void(const KNX::Status a_status)> Lcon);
    void A_Disconnect_req(const KNX::Priority priority, const KNX::ASAP_Individual asap, std::function<void(const KNX::Status a_status)> Lcon);

    /* Services (Group) */
    void A_GroupValue_Read_req(const KNX::Ack_Request ack_request, const KNX::ASAP_Group asap, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, std::function<void(const KNX::Status a_status)> Lcon);
    void A_GroupValue_Read_res(const KNX::Ack_Request ack_request, const KNX::ASAP_Group asap, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const std::vector<uint8_t> data, std::function<void(const KNX::Status a_status)> Rcon);
    void A_GroupValue_Write_req(const KNX::Ack_Request ack_request, const KNX::ASAP_Group asap, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const std::vector<uint8_t> data, std::function<void(const KNX::Status a_status)> Lcon);

    /* Services (Broadcast) */
    void A_IndividualAddress_Write_req(const KNX::Ack_Request ack_request, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::Individual_Address newaddress, std::function<void(const KNX::Status a_status)> Lcon);
    void A_IndividualAddress_Read_req(const KNX::Ack_Request ack_request, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, std::function<void(const KNX::Status a_status)> Lcon);
    void A_IndividualAddress_Read_res(const KNX::Ack_Request ack_request, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::Individual_Address individual_address, std::function<void(const KNX::Status a_status)> Rcon);
    void A_IndividualAddressSerialNumber_Read_req(const KNX::Ack_Request ack_request, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::Serial_Number serial_number, std::function<void(const KNX::Status a_status)> Lcon);
    void A_IndividualAddressSerialNumber_Read_res(const KNX::Ack_Request ack_request, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::Serial_Number serial_number, const KNX::Domain_Address_2 domain_address, std::function<void(const KNX::Status a_status)> Rcon);
    void A_IndividualAddressSerialNumber_Write_req(const KNX::Ack_Request ack_request, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::Serial_Number serial_number, const KNX::Individual_Address newaddress, std::function<void(const KNX::Status a_status)> Lcon);
    void A_NetworkParameter_Read_req(const KNX::ASAP_Individual asap, const KNX::Comm_Mode comm_mode_req, const KNX::Hop_Count_Type hop_count_type, const KNX::Parameter_Type parameter_type, const KNX::Priority priority, const std::vector<uint8_t> test_info, std::function<void(const KNX::Status a_status)> Lcon);
    void A_NetworkParameter_Read_res(const KNX::ASAP_Individual asap, const KNX::Comm_Mode comm_mode, const KNX::Hop_Count_Type hop_count_type, const KNX::Parameter_Type parameter_type, const KNX::Priority priority, const std::vector<uint8_t> test_info, const std::vector<uint8_t> test_result, std::function<void(const KNX::Status a_status)> Rcon);
    void A_NetworkParameter_Write_req(const KNX::ASAP_Individual asap, const KNX::Comm_Mode comm_mode, const KNX::Hop_Count_Type hop_count_type, const KNX::Parameter_Type parameter_type, const KNX::Priority priority, const std::vector<uint8_t> value, std::function<void(const KNX::Status a_status)> Lcon);
    void A_NetworkParameter_InfoReport_req(const KNX::ASAP_Individual asap, const KNX::Comm_Mode comm_mode_req, const KNX::Hop_Count_Type hop_count_type, const KNX::Parameter_Type parameter_type, const KNX::Priority priority, const std::vector<uint8_t> test_info, const std::vector<uint8_t> test_result, std::function<void(const KNX::Status a_status)> Lcon);

    /* Services (System Broadcast) */
    void A_DeviceDescriptor_InfoReport_req(const KNX::Ack_Request ack_request, const KNX::Descriptor_Type descriptor_type, const std::vector<uint8_t> device_descriptor, const KNX::Hop_Count_Type hop_count_type, const KNX::Priority priority, std::function<void(const KNX::Status a_status)> Lcon);
    void A_DomainAddress_Write_req(const KNX::Ack_Request ack_request, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::Domain_Address domain_address_new, std::function<void(const KNX::Status a_status)> Lcon);
    void A_DomainAddress_Read_req(const KNX::Ack_Request ack_request, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, std::function<void(const KNX::Status a_status)> Lcon);
    void A_DomainAddress_Read_res(const KNX::Ack_Request ack_request, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::Domain_Address domain_address, std::function<void(const KNX::Status a_status)> Rcon);
    void A_DomainAddressSelective_Read_req(const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASDU asdu, std::function<void(const KNX::Status a_status)> Lcon);
    void A_DomainAddressSerialNumber_Read_req(const KNX::Ack_Request ack_request, const KNX::Hop_Count_Type hop_count_type, const KNX::Priority priority, const KNX::Serial_Number serial_number, std::function<void(const KNX::Status a_status)> Lcon);
    void A_DomainAddressSerialNumber_Read_res(const KNX::Ack_Request ack_request, const KNX::Domain_Address domain_address, const KNX::Hop_Count_Type hop_count_type, const KNX::Priority priority, const KNX::Serial_Number serial_number, std::function<void(const KNX::Status a_status)> Rcon);
    void A_DomainAddressSerialNumber_Write_req(const KNX::Ack_Request ack_request, const KNX::Domain_Address domain_address, const KNX::Hop_Count_Type hop_count_type, const KNX::Priority priority, const KNX::Serial_Number serial_number, std::function<void(const KNX::Status a_status)> Lcon);
    void A_SystemNetworkParameter_Read_req(const KNX::Hop_Count_Type hop_count_type, const KNX::System_Parameter_Type parameter_type, const KNX::Priority priority, const std::vector<uint8_t> test_info, std::function<void(const KNX::Status a_status)> Lcon);
    void A_SystemNetworkParameter_Read_res(const KNX::Hop_Count_Type hop_count_type, const KNX::System_Parameter_Type parameter_type, const KNX::Priority priority, const std::vector<uint8_t> test_info, const std::vector<uint8_t> test_result, std::function<void(const KNX::Status a_status)> Rcon);
    void A_SystemNetworkParameter_Write_req(const KNX::Hop_Count_Type hop_count_type, const KNX::System_Parameter_Type parameter_type, const KNX::Priority priority, const std::vector<uint8_t> value, std::function<void(const KNX::Status a_status)> Lcon);

    /* Services (Individual) */
    void A_DeviceDescriptor_Read_req(const KNX::Ack_Request ack_request, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Individual asap, const KNX::Descriptor_Type descriptor_type, std::function<void(const KNX::Status a_status)> Lcon);
    void A_DeviceDescriptor_Read_res(const KNX::Ack_Request ack_request, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Individual asap, const KNX::Descriptor_Type descriptor_type, const std::vector<uint8_t> device_descriptor, std::function<void(const KNX::Status a_status)> Rcon);
    void A_Restart_req(const KNX::Ack_Request ack_request, const uint8_t channel_number, const KNX::Restart_Erase_Code erase_code, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::Restart_Type restart_type, const KNX::ASAP_Individual asap, std::function<void(const KNX::Status a_status)> Lcon);
    void A_Restart_res(const KNX::Restart_Error_Code error_code, const KNX::Priority priority, const uint16_t process_time, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Individual asap, std::function<void(const KNX::Status a_status)> Rcon);
    void A_FileStream_InfoReport_req(const KNX::Ack_Request ack_request, const KNX::ASAP_Individual asap, const KNX::File_Block file_block, const KNX::File_Block_Sequence_Number file_block_sequence_number, const KNX::File_Handle file_handle, const KNX::Hop_Count_Type hop_count_type, std::function<void(const KNX::Status a_status)> Lcon);
    void A_PropertyValue_Read_req(const KNX::Ack_Request ack_request, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Individual asap, const KNX::Object_Index object_index, const KNX::Property_Id property_id, const KNX::PropertyValue_Nr_Of_Elem nr_of_elem, const KNX::PropertyValue_Start_Index start_index, std::function<void(const KNX::Status a_status)> Lcon);
    void A_PropertyValue_Read_res(const KNX::Ack_Request ack_request, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Individual asap, const KNX::Object_Index object_index, const KNX::Property_Id property_id, const KNX::PropertyValue_Nr_Of_Elem nr_of_elem, const KNX::PropertyValue_Start_Index start_index, const KNX::Property_Value data, std::function<void(const KNX::Status a_status)> Rcon);
    void A_PropertyValue_Write_req(const KNX::Ack_Request ack_request, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Individual asap, const KNX::Object_Index object_index, const KNX::Property_Id property_id, const KNX::PropertyValue_Nr_Of_Elem nr_of_elem, const KNX::PropertyValue_Start_Index start_index, const KNX::Property_Value data, std::function<void(const KNX::Status a_status)> Lcon);
    void A_PropertyDescription_Read_req(const KNX::Ack_Request ack_request, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Individual asap, const KNX::Object_Index object_index, const KNX::Property_Id property_id, const KNX::Property_Index property_index, std::function<void(const KNX::Status a_status)> Lcon);
    void A_PropertyDescription_Read_res(const KNX::Ack_Request ack_request, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Individual asap, const KNX::Object_Index object_index, const KNX::Property_Id property_id, const KNX::Property_Index property_index, const bool write_enable, const KNX::Property_Type type, const KNX::Max_Nr_Of_Elem max_nr_of_elem, const KNX::Access access, std::function<void(const KNX::Status a_status)> Rcon);
    void A_Link_Read_req(const KNX::ASAP_Individual asap, const KNX::Group_Object_Number group_object_number, const KNX::Priority priority, const KNX::Link_Start_Index start_index, std::function<void(const KNX::Status a_status)> Lcon);
    void A_Link_Read_res(const KNX::ASAP_Individual asap, const std::vector<KNX::Group_Address> group_address_list, const uint8_t group_object_number, const KNX::Priority priority, const KNX::Link_Sending_Address sending_address, const KNX::Link_Start_Index start_index, std::function<void(const KNX::Status a_status)> Rcon);
    void A_Link_Write_req(const KNX::ASAP_Individual asap, const KNX::Link_Write_Flags flags, const KNX::Group_Address group_address, const uint8_t group_object_number, const KNX::Priority priority, std::function<void(const KNX::Status a_status)> Lcon);
    void A_FunctionPropertyCommand_req(const KNX::Ack_Request ack_request, const KNX::ASAP_Individual asap, const KNX::Comm_Mode comm_mode, const std::vector<uint8_t> data, const KNX::Hop_Count_Type hop_count_type, const KNX::Object_Index object_index, const KNX::Priority priority, const KNX::Property_Id property_id, std::function<void(const KNX::Status a_status)> Lcon);
    void A_FunctionPropertyCommand_res(const KNX::Ack_Request ack_request, const KNX::ASAP_Individual asap, const KNX::Comm_Mode comm_mode, const std::vector<uint8_t> data, const KNX::Hop_Count_Type hop_count_type, const KNX::Object_Index object_index, const KNX::Priority priority, const KNX::Property_Id property_id, const std::optional<KNX::Property_Return_Code> return_code, std::function<void(const KNX::Status a_status)> Rcon);
    void A_FunctionPropertyState_Read_req(const KNX::ASAP_Individual asap, const KNX::Ack_Request ack_request, const KNX::Comm_Mode comm_mode, const std::vector<uint8_t> data, const KNX::Hop_Count_Type hop_count_type, const KNX::Object_Index object_index, const KNX::Priority priority, const KNX::Property_Id property_id, std::function<void(const KNX::Status a_status)> Lcon);
    void A_FunctionPropertyState_Read_res(const KNX::Ack_Request ack_request, const KNX::ASAP_Individual asap, const KNX::Comm_Mode comm_mode, const std::vector<uint8_t> data, const KNX::Hop_Count_Type hop_count_type, const KNX::Object_Index object_index, const KNX::Priority priority, const KNX::Property_Id property_id, const std::optional<KNX::Property_Return_Code> return_code, std::function<void(const KNX::Status a_status)> Rcon);

    /* Services (Connected) */
    void A_ADC_Read_req(const KNX::Ack_Request ack_request, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::ADC_Channel_Nr channel_nr, const uint8_t read_count, std::function<void(const KNX::Status a_status)> Lcon);
    void A_ADC_Read_res(const KNX::Ack_Request ack_request, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::ADC_Channel_Nr channel_nr, const uint8_t read_count, const uint16_t sum, std::function<void(const KNX::Status a_status)> Rcon);
    void A_Memory_Read_req(const KNX::Ack_Request ack_request, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::Memory_Number number, const KNX::Memory_Address memory_address, std::function<void(const KNX::Status a_status)> Lcon);
    void A_Memory_Read_res(const KNX::Ack_Request ack_request, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::Memory_Number number, const KNX::Memory_Address memory_address, const std::vector<uint8_t> data, std::function<void(const KNX::Status a_status)> Rcon);
    void A_Memory_Write_req(const KNX::Ack_Request ack_request, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::Memory_Number number, const KNX::Memory_Address memory_address, const std::vector<uint8_t> data, std::function<void(const KNX::Status a_status)> Lcon);
    void A_Memory_Write_res(const KNX::Ack_Request ack_request, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::Memory_Number number, const KNX::Memory_Address memory_address, const std::vector<uint8_t> data, std::function<void(const KNX::Status a_status)> Rcon);
    void A_MemoryBit_Write_req(const KNX::Ack_Request ack_request, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::MemoryBit_Number number, const KNX::MemoryBit_Address memory_address, const std::vector<uint8_t> and_data, const std::vector<uint8_t> xor_data, std::function<void(const KNX::Status a_status)> Lcon);
    void A_MemoryBit_Write_res(const KNX::ASAP_Connected asap, const KNX::Priority priority, const KNX::MemoryBit_Number number, const KNX::MemoryBit_Address memory_address, const std::vector<uint8_t> data, std::function<void(const KNX::Status a_status)> Rcon);
    void A_UserMemory_Read_req(const KNX::Ack_Request ack_request, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::UserMemory_Number number, const KNX::UserMemory_Address memory_address, std::function<void(const KNX::Status a_status)> Lcon);
    void A_UserMemory_Read_res(const KNX::Ack_Request ack_request, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::UserMemory_Number number, const KNX::UserMemory_Address memory_address, const std::vector<uint8_t> data, std::function<void(const KNX::Status a_status)> Rcon);
    void A_UserMemory_Write_req(const KNX::Ack_Request ack_request, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::UserMemory_Number number, const KNX::UserMemory_Address memory_address, const std::vector<uint8_t> data, std::function<void(const KNX::Status a_status)> Lcon);
    void A_UserMemory_Write_res(const KNX::Ack_Request ack_request, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::UserMemory_Number number, const KNX::UserMemory_Address memory_address, const std::vector<uint8_t> data, std::function<void(const KNX::Status a_status)> Rcon);
    void A_UserMemoryBit_Write_req(const KNX::Ack_Request ack_request, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::UserMemoryBit_Number number, const KNX::UserMemoryBit_Address memory_address, const std::vector<uint8_t> and_data, const std::vector<uint8_t> xor_data, std::function<void(const KNX::Status a_status)> Lcon);
    void A_UserMemoryBit_Write_res(const KNX::Ack_Request ack_request, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::UserMemoryBit_Number number, const KNX::UserMemoryBit_Address memory_address, const std::vector<uint8_t> data, std::function<void(const KNX::Status a_status)> Rcon);
    void A_UserManufacturerInfo_Read_req(const KNX::Ack_Request ack_request, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, std::function<void(const KNX::Status a_status)> Lcon);
    void A_UserManufacturerInfo_Read_res(const KNX::Ack_Request ack_request, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::MFact_Info mfact_info, std::function<void(const KNX::Status a_status)> Rcon);
    void A_Authorize_Request_req(const KNX::Ack_Request ack_request, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::Key key, std::function<void(const KNX::Status a_status)> Lcon);
    void A_Authorize_Request_res(const KNX::Ack_Request ack_request, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::Level level, std::function<void(const KNX::Status a_status)> Rcon);
    void A_Key_Write_req(const KNX::Ack_Request ack_request, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::Level level, const KNX::Key key, std::function<void(const KNX::Status a_status)> Lcon);
    void A_Key_Write_res(const KNX::Ack_Request ack_request, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::Level level, std::function<void(const KNX::Status a_status)> Rcon);

    /* Network Management */
    KNX::NM_IndividualAddress_Read * makeNetworkManagementIndividualAddressRead();
    KNX::NM_IndividualAddress_Write * makeNetworkManagementIndividualAddressWrite();
    KNX::NM_IndividualAddress_SerialNumber_Read * makeNetworkManagementIndividualAddressSerialNumberRead();
    KNX::NM_IndividualAddress_SerialNumber_Write * makeNetworkManagementIndividualAddressSerialNumberWrite();
    KNX::NM_IndividualAddress_SerialNumber_Write2 * makeNetworkManagementIndividualAddressSerialNumberWrite2();

Q_SIGNALS:
    void A_Connect_ind(const KNX::ASAP_Connected asap);
    void A_Disconnect_ind(const KNX::ASAP_Connected asap);

    /* Services (Group) */
    void A_GroupValue_Read_ind(const KNX::ASAP_Group asap, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type);
    void A_GroupValue_Read_Acon(const KNX::ASAP_Group asap, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const std::vector<uint8_t> data);
    void A_GroupValue_Write_ind(const KNX::ASAP_Group asap, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const std::vector<uint8_t> data);

    /* Services (Broadcast) */
    void A_IndividualAddress_Write_ind(const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::Individual_Address newaddress);
    void A_IndividualAddress_Read_ind(const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type);
    void A_IndividualAddress_Read_Acon(const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::Individual_Address individual_address);
    void A_IndividualAddressSerialNumber_Read_ind(const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::Serial_Number serial_number);
    void A_IndividualAddressSerialNumber_Read_Acon(const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::Serial_Number serial_number, const KNX::Individual_Address individual_address, const KNX::Domain_Address_2 domain_address);
    void A_IndividualAddressSerialNumber_Write_ind(const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::Serial_Number serial_number, const KNX::Individual_Address newaddress);
    void A_NetworkParameter_Read_ind(const KNX::ASAP_Individual asap, const KNX::Comm_Mode comm_mode_req, const KNX::Hop_Count_Type hop_count_type, const KNX::Parameter_Type parameter_type, const KNX::Priority priority, const std::vector<uint8_t> test_info);
    void A_NetworkParameter_Read_Acon(const KNX::ASAP_Individual asap, const KNX::Hop_Count_Type hop_count_type, const KNX::Individual_Address individual_address, const KNX::Parameter_Type parameter_type, const KNX::Priority priority, const KNX::Parameter_Test_Info_Result test_info_result);
    void A_NetworkParameter_Write_ind(const KNX::ASAP_Individual asap, const KNX::Parameter_Type parameter_type, const KNX::Priority priority, const std::vector<uint8_t> value);
    void A_NetworkParameter_InfoReport_ind(const KNX::ASAP_Individual asap, const KNX::Comm_Mode comm_mode_req, const KNX::Hop_Count_Type hop_count_type, const KNX::Parameter_Type parameter_type, const KNX::Priority priority, const KNX::Parameter_Test_Info_Result test_info_result);

    /* Services (System Broadcast) */
    void A_DeviceDescriptor_InfoReport_ind(const KNX::Descriptor_Type descriptor_type, const std::vector<uint8_t> device_descriptor, const KNX::Hop_Count_Type hop_count_type, const KNX::Priority priority);
    void A_DomainAddress_Write_ind(const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::Domain_Address domain_address_new);
    void A_DomainAddress_Read_ind(const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type);
    void A_DomainAddress_Read_Acon(const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Individual asap, const KNX::Domain_Address domain_address);
    void A_DomainAddressSelective_Read_ind(const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASDU asdu);
    void A_DomainAddressSerialNumber_Read_ind(const KNX::Hop_Count_Type hop_count_type, const KNX::Priority priority, const KNX::Serial_Number serial_number);
    void A_DomainAddressSerialNumber_Read_Acon(const KNX::Domain_Address domain_address, const KNX::Hop_Count_Type hop_count_type, const KNX::Priority priority, const KNX::Serial_Number serial_number);
    void A_DomainAddressSerialNumber_Write_ind(const KNX::Domain_Address domain_address, const KNX::Hop_Count_Type hop_count_type, const KNX::Priority priority, const KNX::Serial_Number serial_number);
    void A_SystemNetworkParameter_Read_ind(const KNX::Hop_Count_Type hop_count_type, const KNX::System_Parameter_Type parameter_type, const KNX::Priority priority, const std::vector<uint8_t> test_info);
    void A_SystemNetworkParameter_Read_Acon(const KNX::ASAP_Individual asap, const KNX::Hop_Count_Type hop_count_type, const KNX::System_Parameter_Type parameter_type, const KNX::Priority priority, const KNX::Parameter_Test_Info_Result test_info);
    void A_SystemNetworkParameter_Write_ind(const KNX::Hop_Count_Type hop_count_type, const KNX::System_Parameter_Type parameter_type, const KNX::Priority priority, const std::vector<uint8_t> value);

    /* Services (Individual) */
    void A_DeviceDescriptor_Read_ind(const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Individual asap, const KNX::Descriptor_Type descriptor_type);
    void A_DeviceDescriptor_Read_Acon(const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Individual asap, const KNX::Descriptor_Type descriptor_type, const std::vector<uint8_t> device_descriptor);
    void A_Restart_ind(const KNX::Restart_Erase_Code erase_code, const uint8_t channel_number, const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Individual asap);
    // @todo A_Restart_Acon
    void A_FileStream_InfoReport_ind(const KNX::ASAP_Individual asap, const std::vector<uint8_t> file_block, const KNX::File_Block_Sequence_Number file_block_sequence_number, const KNX::File_Handle file_handle, const KNX::Hop_Count_Type hop_count_type);
    void A_PropertyValue_Read_ind(const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Individual asap, const KNX::Object_Index object_index, const KNX::Property_Id property_id, const KNX::PropertyValue_Nr_Of_Elem nr_of_elem, const KNX::PropertyValue_Start_Index start_index);
    void A_PropertyValue_Read_Acon(const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Individual asap, const KNX::Object_Index object_index, const KNX::Property_Id property_id, const KNX::PropertyValue_Nr_Of_Elem nr_of_elem, const KNX::PropertyValue_Start_Index start_index, const std::vector<uint8_t> data);
    void A_PropertyValue_Write_ind(const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Individual asap, const KNX::Object_Index object_index, const KNX::Property_Id property_id, const KNX::PropertyValue_Nr_Of_Elem nr_of_elem, const KNX::PropertyValue_Start_Index start_index, const std::vector<uint8_t> data);
    void A_PropertyDescription_Read_ind(const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Individual asap, const KNX::Object_Index object_index, const KNX::Property_Id property_id, const KNX::Property_Index property_index);
    void A_PropertyDescription_Read_Acon(const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Individual asap, const KNX::Object_Index object_index, const KNX::Property_Id property_id, const KNX::Property_Index property_index, const bool write_enable, const KNX::Property_Type type, const KNX::Max_Nr_Of_Elem max_nr_of_elem, const KNX::Access access);
    void A_FunctionPropertyCommand_ind(const KNX::ASAP_Individual asap, const KNX::Comm_Mode comm_mode, const std::vector<uint8_t> data, const KNX::Hop_Count_Type hop_count_type, const KNX::Object_Index object_index, const KNX::Priority priority, const KNX::Property_Id property_id);
    void A_FunctionPropertyCommand_Acon(const KNX::ASAP_Individual asap, const KNX::Comm_Mode comm_mode, const std::vector<uint8_t> data, const KNX::Hop_Count_Type hop_count_type, const KNX::Object_Index object_index, const KNX::Priority priority, const KNX::Property_Id property_id, const std::optional<KNX::Property_Return_Code> return_code);
    void A_FunctionPropertyState_Read_ind(const KNX::ASAP_Individual asap, const KNX::Comm_Mode comm_mode, const std::vector<uint8_t> data, const KNX::Hop_Count_Type hop_count_type, const KNX::Object_Index object_index, const KNX::Priority priority, const KNX::Property_Id property_id);
    void A_FunctionPropertyState_Read_Acon(const KNX::ASAP_Individual asap, const KNX::Comm_Mode comm_mode, const std::vector<uint8_t> data, const KNX::Hop_Count_Type hop_count_type, const KNX::Object_Index object_index, const KNX::Priority priority, const KNX::Property_Id property_id, const std::optional<KNX::Property_Return_Code> return_code);

    /* Services (Connected) */
    void A_ADC_Read_ind(const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::ADC_Channel_Nr channel_nr, const uint8_t read_count);
    void A_ADC_Read_Acon(const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::ADC_Channel_Nr channel_nr, const uint8_t read_count, const uint16_t sum);
    void A_Memory_Read_ind(const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::Memory_Number number, const KNX::Memory_Address memory_address);
    void A_Memory_Read_Acon(const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::Memory_Number number, const KNX::Memory_Address memory_address, const std::vector<uint8_t> data);
    void A_Memory_Write_ind(const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::Memory_Number number, const KNX::Memory_Address memory_address, const std::vector<uint8_t> data);
    void A_Memory_Write_Acon(const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::Memory_Number number, const KNX::Memory_Address memory_address, const std::vector<uint8_t> data);
    void A_MemoryBit_Write_ind(const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::MemoryBit_Number number, const KNX::MemoryBit_Address memory_address, const std::vector<uint8_t> and_data, const std::vector<uint8_t> xor_data);
    void A_UserMemory_Read_ind(const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::UserMemory_Number number, const KNX::UserMemory_Address memory_address);
    void A_UserMemory_Read_Acon(const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::UserMemory_Number number, const KNX::UserMemory_Address memory_address, const std::vector<uint8_t> data);
    void A_UserMemory_Write_ind(const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::UserMemory_Number number, const KNX::UserMemory_Address memory_address, const std::vector<uint8_t> data);
    void A_UserMemory_Write_Acon(const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::UserMemory_Number number, const KNX::UserMemory_Address memory_address, const std::vector<uint8_t> data);
    void A_UserMemoryBit_Write_ind(const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::UserMemoryBit_Number number, const KNX::UserMemoryBit_Address memory_address, const std::vector<uint8_t> and_data, const std::vector<uint8_t> xor_data);
    void A_UserMemoryBit_Write_Acon(const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::UserMemoryBit_Number number, const KNX::UserMemoryBit_Address memory_address, const std::vector<uint8_t> data);
    void A_UserManufacturerInfo_Read_ind(const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap);
    void A_UserManufacturerInfo_Read_Acon(const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::MFact_Info mfact_info);
    void A_Authorize_Request_ind(const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::Key key);
    void A_Authorize_Request_Acon(const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::Level level);
    void A_Key_Write_ind(const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::Level level, const KNX::Key key);
    void A_Key_Write_Acon(const KNX::Priority priority, const KNX::Hop_Count_Type hop_count_type, const KNX::ASAP_Connected asap, const KNX::Level level);

private:
    /** cEMI client */
    KNX::CEMI_Client cemi_client;

    void local_cemi_req(const std::vector<uint8_t> cemi_frame_data);

    void A_Connect_ind_initiator();
    void A_Disconnect_ind_initiator();

    /* Services (Group) */
    void A_GroupValue_Read_ind_initiator();
    void A_GroupValue_Read_Acon_initiator();
    void A_GroupValue_Write_ind_initiator();

    /* Services (Broadcast) */
    void A_IndividualAddress_Write_ind_initiator();
    void A_IndividualAddress_Read_ind_initiator();
    void A_IndividualAddress_Read_Acon_initiator();
    void A_IndividualAddressSerialNumber_Read_ind_initiator();
    void A_IndividualAddressSerialNumber_Read_Acon_initiator();
    void A_IndividualAddressSerialNumber_Write_ind_initiator();
    void A_NetworkParameter_Read_ind_initiator();
    void A_NetworkParameter_Read_Acon_initiator();
    void A_NetworkParameter_Write_ind_initiator();
    void A_NetworkParameter_InfoReport_ind_initiator();

    /* Services (System Broadcast) */
    void A_DeviceDescriptor_InfoReport_ind_initiator();
    void A_DomainAddress_Write_ind_initiator();
    void A_DomainAddress_Read_ind_initiator();
    void A_DomainAddress_Read_Acon_initiator();
    void A_DomainAddressSelective_Read_ind_initiator();
    void A_DomainAddressSerialNumber_Read_ind_initiator();
    void A_DomainAddressSerialNumber_Read_Acon_initiator();
    void A_DomainAddressSerialNumber_Write_ind_initiator();
    void A_SystemNetworkParameter_Read_ind_initiator();
    void A_SystemNetworkParameter_Read_Acon_initiator();
    void A_SystemNetworkParameter_Write_ind_initiator();

    /* Services (Individual) */
    void A_DeviceDescriptor_Read_ind_initiator();
    void A_DeviceDescriptor_Read_Acon_initiator();
    void A_Restart_ind_initiator();
    void A_FileStream_InfoReport_ind_initiator();
    void A_PropertyValue_Read_ind_initiator();
    void A_PropertyValue_Read_Acon_initiator();
    void A_PropertyValue_Write_ind_initiator();
    void A_PropertyDescription_Read_ind_initiator();
    void A_PropertyDescription_Read_Acon_initiator();
    void A_FunctionPropertyCommand_ind_initiator();
    void A_FunctionPropertyCommand_Acon_initiator();
    void A_FunctionPropertyState_Read_ind_initiator();
    void A_FunctionPropertyState_Read_Acon_initiator();

    /* Services (Connected) */
    void A_ADC_Read_ind_initiator();
    void A_ADC_Read_Acon_initiator();
    void A_Memory_Read_ind_initiator();
    void A_Memory_Read_Acon_initiator();
    void A_Memory_Write_ind_initiator();
    void A_Memory_Write_Acon_initiator();
    void A_MemoryBit_Write_ind_initiator();
    void A_UserMemory_Read_ind_initiator();
    void A_UserMemory_Read_Acon_initiator();
    void A_UserMemory_Write_ind_initiator();
    void A_UserMemory_Write_Acon_initiator();
    void A_UserMemoryBit_Write_ind_initiator();
    void A_UserMemoryBit_Write_Acon_initiator();
    void A_UserManufacturerInfo_Read_ind_initiator();
    void A_UserManufacturerInfo_Read_Acon_initiator();
    void A_Authorize_Request_ind_initiator();
    void A_Authorize_Request_Acon_initiator();
    void A_Key_Write_ind_initiator();
    void A_Key_Write_Acon_initiator();
};
