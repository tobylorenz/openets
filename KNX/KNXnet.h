#pragma once

#include <map>

#include <asio.hpp>

#include <QObject>

#include <KNX/03/08/IP_Communication_Channel.h>
#include <KNX/03/08/IP_Discovery_Channel.h>

#include "KNX/Stack.h"

/**
 * This KNX Network class provides access to KNX IP Routers
 *
 * @todo support multiple communication channels and stacks
 */
class KNXnet : public QObject
{
    Q_OBJECT

public:
    KNXnet(QObject * parent = nullptr);
    virtual ~KNXnet();

    /** IO Context */
    asio::io_context io_context;

    /** Work Guard */
    asio::executor_work_guard<asio::io_context::executor_type> work_guard;

    /** IP discovery channel */
    KNX::IP_Discovery_Channel discovery_channel;

    /** get IP communication channel */
    KNX::IP_Communication_Channel * communication_channel(const KNX::IP_Host_Protocol_Address_Information remote_control_endpoint);

    /** get stack */
    Stack * stack(const KNX::IP_Host_Protocol_Address_Information remote_control_endpoint);

    /** IP communication channel */
    std::map<KNX::IP_Host_Protocol_Address_Information, KNX::IP_Communication_Channel *> communication_channels;

Q_SIGNALS:
    void connectionCreated(const KNX::IP_Host_Protocol_Address_Information remote_control_endpoint);

private Q_SLOTS:
    /** idle actions, e.g. io_context poll */
    void idleActions();

private:
    /** KNX stack */
    std::map<KNX::IP_Host_Protocol_Address_Information, Stack *> stacks;
};
