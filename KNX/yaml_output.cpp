#include "yaml_output.h"

#include <array>
#include <iostream>
#include <map>
#include <sstream>
#include <string>
#include <vector>

#include <KNX/types.h>

std::string to_hex(const uint8_t value)
{
    std::ostringstream os;
    os.width(2);
    os.fill('0');
    os << std::hex << static_cast<uint16_t>(value);
    return os.str();
}

std::string to_hex(const uint16_t value)
{
    std::ostringstream os;
    os.width(4);
    os.fill('0');
    os << std::hex << value;
    return os.str();
}

std::string to_string(const std::array<uint8_t, 4> & ip_address)
{
    std::ostringstream os;
    os << std::dec
       << static_cast<uint16_t>(ip_address[0])
       << "."
       << static_cast<uint16_t>(ip_address[1])
       << "."
       << static_cast<uint16_t>(ip_address[2])
       << "."
       << static_cast<uint16_t>(ip_address[3]);
    return os.str();
}

std::string to_string(const std::array<uint8_t, 6> & mac_address)
{
    std::ostringstream os;
    os << to_hex(mac_address[0])
       << " "
       << to_hex(mac_address[1])
       << " "
       << to_hex(mac_address[2])
       << " "
       << to_hex(mac_address[3])
       << " "
       << to_hex(mac_address[4])
       << " "
       << to_hex(mac_address[5]);
    return os.str();
}

std::string to_string(const std::array<char, 30> & name)
{
    std::string namestr(std::cbegin(name), std::cend(name));
    while (namestr.back() == 0) {
        namestr.pop_back();
    }
    return namestr;
}

std::string to_string(const std::vector<uint16_t> & additional_addresses)
{
    std::ostringstream os;
    for (auto const & a : additional_addresses) {
        os << to_hex(a) << ",";
    }
    return os.str();
}

std::string to_string(const std::vector<uint8_t> & data)
{
    std::ostringstream os;
    for (const uint8_t & d : data) {
        os << to_hex(d) << " ";
    }
    return os.str();
}

#ifdef YAML_CPP_FOUND

/* 5.3 KNXnet/IP services */

std::string to_string(const KNX::Service_Type_Identifier service_type_identifier)
{
    switch (service_type_identifier) {
    case KNX::Service_Type_Identifier::UNDEFINED:
        break;

    /* Core */
    case KNX::Service_Type_Identifier::SEARCH_REQUEST:
        return "SEARCH_REQUEST";
    case KNX::Service_Type_Identifier::SEARCH_RESPONSE:
        return "SEARCH_RESPONSE";
    case KNX::Service_Type_Identifier::DESCRIPTION_REQUEST:
        return "DESCRIPTION_REQUEST";
    case KNX::Service_Type_Identifier::DESCRIPTION_RESPONSE:
        return "DESCRIPTION_RESPONSE";
    case KNX::Service_Type_Identifier::CONNECT_REQUEST:
        return "CONNECT_REQUEST";
    case KNX::Service_Type_Identifier::CONNECT_RESPONSE:
        return "CONNECT_RESPONSE";
    case KNX::Service_Type_Identifier::CONNECTIONSTATE_REQUEST:
        return "CONNECTIONSTATE_REQUEST";
    case KNX::Service_Type_Identifier::CONNECTIONSTATE_RESPONSE:
        return "CONNECTIONSTATE_RESPONSE";
    case KNX::Service_Type_Identifier::DISCONNECT_REQUEST:
        return "DISCONNECT_REQUEST";
    case KNX::Service_Type_Identifier::DISCONNECT_RESPONSE:
        return "DISCONNECT_RESPONSE";
    case KNX::Service_Type_Identifier::SEARCH_REQUEST_EXT:
        return "SEARCH_REQUEST_EXT";
    case KNX::Service_Type_Identifier::SEARCH_RESPONSE_EXT:
        return "SEARCH_RESPONSE_EXT";

    /* Device Management */
    case KNX::Service_Type_Identifier::DEVICE_CONFIGURATION_REQUEST:
        return "DEVICE_CONFIGURATION_REQUEST";
    case KNX::Service_Type_Identifier::DEVICE_CONFIGURATION_ACK:
        return "DEVICE_CONFIGURATION_ACK";

    /* Tunnelling */
    case KNX::Service_Type_Identifier::TUNNELING_REQUEST:
        return "TUNNELING_REQUEST";
    case KNX::Service_Type_Identifier::TUNNELING_ACK:
        return "TUNNELING_ACK";
    case KNX::Service_Type_Identifier::TUNNELING_FEATURE_GET:
        return "TUNNELING_FEATURE_GET";
    case KNX::Service_Type_Identifier::TUNNELING_FEATURE_RESPONSE:
        return "TUNNELING_FEATURE_RESPONSE";
    case KNX::Service_Type_Identifier::TUNNELING_FEATURE_SET:
        return "TUNNELING_FEATURE_SET";
    case KNX::Service_Type_Identifier::TUNNELING_FEATURE_INFO:
        return "TUNNELING_FEATURE_INFO";

    /* Routing */
    case KNX::Service_Type_Identifier::ROUTING_INDICATION:
        return "ROUTING_INDICATION";
    case KNX::Service_Type_Identifier::ROUTING_LOST_MESSAGE:
        return "ROUTING_LOST_MESSAGE";
    case KNX::Service_Type_Identifier::ROUTING_BUSY:
        return "ROUTING_BUSY";
    case KNX::Service_Type_Identifier::ROUTING_SYSTEM_BROADCAST:
        return "ROUTING_SYSTEM_BROADCAST";

    /* Remote Logging */

    /* Remote Configuration and Diagnosis (see 3/8/7) */
    case KNX::Service_Type_Identifier::REMOTE_DIAGNOSTIC_REQUEST:
        return "REMOTE_DIAGNOSTIC_REQUEST";
    case KNX::Service_Type_Identifier::REMOTE_DIAGNOSTIC_RESPONSE:
        return "REMOTE_DIAGNOSTIC_RESPONSE";
    case KNX::Service_Type_Identifier::REMOTE_BASIC_CONFIGURATION_REQUEST:
        return "REMOTE_BASIC_CONFIGURATION_REQUEST";
    case KNX::Service_Type_Identifier::REMOTE_RESET_REQUEST:
        return "REMOTE_RESET_REQUEST";

    /* KNXnet/IP Object Server services (see 3/8/8) */

    /* KNXnet/IP Secure (see AN 159/13 v04) */
    case KNX::Service_Type_Identifier::SECURE_WRAPPER:
        return "SECURE_WRAPPER";
    case KNX::Service_Type_Identifier::SECURE_CHANNEL_REQUEST:
        return "SECURE_CHANNEL_REQUEST";
    case KNX::Service_Type_Identifier::SECURE_CHANNEL_RESPONSE:
        return "SECURE_CHANNEL_RESPONSE";
    case KNX::Service_Type_Identifier::SECURE_CHANNEL_AUTHORIZE:
        return "SECURE_CHANNEL_AUTHORIZE";
    case KNX::Service_Type_Identifier::SECURE_CHANNEL_STATUS:
        return "SECURE_CHANNEL_STATUS";
    case KNX::Service_Type_Identifier::SECURE_GROUP_SYNC_REQUEST:
        return "SECURE_GROUP_SYNC_REQUEST";
    case KNX::Service_Type_Identifier::SECURE_GROUP_SYNC_RESPONSE:
        return "SECURE_GROUP_SYNC_RESPONSE";
    }

    return "0x" + to_hex(static_cast<uint16_t>(service_type_identifier));
}

/* 5.4 Connection types */

std::string to_string(const KNX::Connection_Type_Code connection_type_code)
{
    switch (connection_type_code) {
    case KNX::Connection_Type_Code::UNDEFINED:
        break;
    case KNX::Connection_Type_Code::DEVICE_MGMT_CONNECTION:
        return "DEVICE_MGMT_CONNECTION";
    case KNX::Connection_Type_Code::TUNNEL_CONNECTION:
        return "TUNNEL_CONNECTION";
    case KNX::Connection_Type_Code::ROUTING_CONNECTION:
        return "ROUTING_CONNECTION";
    case KNX::Connection_Type_Code::REMLOG_CONNECTION:
        return "REMLOG_CONNECTION";
    case KNX::Connection_Type_Code::REMCONF_CONNECTION:
        return "REMCONF_CONNECTION";
    case KNX::Connection_Type_Code::OBJSVR_CONNECTION:
        return "OBJSVR_CONNECTION";
    }

    return "0x" + to_hex(static_cast<uint8_t>(connection_type_code));
}

/* 5.5 Error codes */

std::string to_string(const KNX::Error_Code error_code)
{
    switch (error_code) {
    /* Common */
    case KNX::Error_Code::E_NO_ERROR:
        return "E_NO_ERROR";
    case KNX::Error_Code::E_HOST_PROTOCOL_TYPE:
        return "E_HOST_PROTOCOL_TYPE";
    case KNX::Error_Code::E_VERSION_NOT_SUPPORTED:
        return "E_VERSION_NOT_SUPPORTED";
    case KNX::Error_Code::E_SEQUENCE_NUMBER:
        return "E_SEQUENCE_NUMBER";
    case KNX::Error_Code::E_ERROR:
        return "E_ERROR";

    /* Service specific */
    case KNX::Error_Code::E_CONNECTION_ID:
        return "E_CONNECTION_ID";
    case KNX::Error_Code::E_CONNECTION_TYPE:
        return "E_CONNECTION_TYPE";
    case KNX::Error_Code::E_CONNECTION_OPTION:
        return "E_CONNECTION_OPTION";
    case KNX::Error_Code::E_NO_MORE_CONNECTIONS:
        return "E_NO_MORE_CONNECTIONS";
    case KNX::Error_Code::E_NO_MORE_UNIQUE_CONNECTIONS:
        return "E_NO_MORE_UNIQUE_CONNECTIONS";
    case KNX::Error_Code::E_DATA_CONNECTION:
        return "E_DATA_CONNECTION";
    case KNX::Error_Code::E_KNX_CONNECTION:
        return "E_KNX_CONNECTION";
    case KNX::Error_Code::E_AUTHORISATION_ERROR:
        return "E_AUTHORISATION_ERROR";
    case KNX::Error_Code::E_TUNNELING_LAYER:
        return "E_TUNNELING_LAYER";
    case KNX::Error_Code::E_NO_TUNNELLING_ADDRESS:
        return "E_NO_TUNNELLING_ADDRESS";
    case KNX::Error_Code::E_CONNECTION_IN_USE:
        return "E_CONNECTION_IN_USE";
    }

    return "0x" + to_hex(static_cast<uint8_t>(error_code));
}

/* 5.6 Description Information Block (DIB) */

std::string to_string(const KNX::Description_Type_Code description_type_code)
{
    switch (description_type_code) {
    case KNX::Description_Type_Code::UNDEFINED:
        break;
    case KNX::Description_Type_Code::DEVICE_INFO:
        return "DEVICE_INFO";
    case KNX::Description_Type_Code::SUPP_SVC_FAMILIES:
        return "SUPP_SVC_FAMILIES";
    case KNX::Description_Type_Code::IP_CONFIG:
        return "IP_CONFIG";
    case KNX::Description_Type_Code::IP_CUR_CONFIG:
        return "IP_CUR_CONFIG";
    case KNX::Description_Type_Code::KNX_ADDRESSES:
        return "KNX_ADDRESSES";
    case KNX::Description_Type_Code::SECURED_SERVICES:
        return "SECURED_SERVICES";
    case KNX::Description_Type_Code::TUNNELLING_INFO:
        return "TUNNELLING_INFO";
    case KNX::Description_Type_Code::EXTENDED_DEVICE_INFO:
        return "EXTENDED_DEVICE_INFO";
    case KNX::Description_Type_Code::MFR_DATA:
        return "MFR_DATA";
    }

    return "0x" + to_hex(static_cast<uint8_t>(description_type_code));
}

std::string to_string(const KNX::Medium_Code medium_code)
{
    switch (medium_code) {
    case KNX::Medium_Code::UNDEFINED:
        break;
    case KNX::Medium_Code::TP0:
        return "TP0";
    case KNX::Medium_Code::TP1:
        return "TP1";
    case KNX::Medium_Code::PL110:
        return "PL110";
    case KNX::Medium_Code::PL132:
        return "PL132";
    case KNX::Medium_Code::RF:
        return "RF";
    case KNX::Medium_Code::IP:
        return "IP";
    }

    return "0x" + to_hex(static_cast<uint8_t>(medium_code));
}

/* 5.7 Host protocol codes */

std::string to_string(const KNX::Host_Protocol_Code host_protocol_code)
{
    switch (host_protocol_code) {
    case KNX::Host_Protocol_Code::UNDEFINED:
        break;
    case KNX::Host_Protocol_Code::IPV4_UDP:
        return "IPV4_UDP";
    case KNX::Host_Protocol_Code::IPV4_TCP:
        return "IPV4_TCP";
    }

    return "0x" + to_hex(static_cast<uint8_t>(host_protocol_code));
}

/* 2 KNXnet/IP frames */

/* 2.2 Frame format */

YAML::Emitter & operator<<(YAML::Emitter & out, const KNX::Frame & obj)
{
    out << YAML::Key << "Frame";
    out << YAML::BeginMap;
    //out << YAML::Key << "header_length" << YAML::Value << static_cast<uint16_t>(obj.header_length());
    out << YAML::Key << "protocol_version" << YAML::Value << "0x" + to_hex(obj.protocol_version);
    out << YAML::Key << "service_type_identifier" << YAML::Value << to_string(obj.service_type_identifier);
    //out << YAML::Key << "total_length" << YAML::Value << obj.total_length();
    out << YAML::EndMap;
    return out;
}

/* 7.5 Placeholders */

/* 7.5.1 Host Protocol Address Information (HPAI) */

YAML::Emitter & operator<<(YAML::Emitter & out, const KNX::Host_Protocol_Address_Information & obj)
{
    out << YAML::Key << "Host_Protocol_Address_Information";
    out << YAML::BeginMap;
    //out << YAML::Key << "structure_length" << YAML::Value << static_cast<uint16_t>(obj.structure_length());
    out << YAML::Key << "host_protocol_code" << YAML::Value << to_string(obj.host_protocol_code);
    out << YAML::EndMap;
    return out;
}

/* 7.5.2 Connection Request Information (CRI) */

YAML::Emitter & operator<<(YAML::Emitter & out, const KNX::Connection_Request_Information & obj)
{
    out << YAML::Key << "Connection_Request_Information";
    out << YAML::BeginMap;
    //out << YAML::Key << "structure_length" << YAML::Value << static_cast<const uint16_t>(obj.structure_length());
    out << YAML::Key << "connection_type_code" << YAML::Value << to_string(obj.connection_type_code);
    out << YAML::EndMap;
    return out;
}

/* 7.5.3 Connection Response Data Block (CRD) */

YAML::Emitter & operator<<(YAML::Emitter & out, const KNX::Connection_Response_Data_Block & obj)
{
    out << YAML::Key << "Connection_Response_Data_Block";
    out << YAML::BeginMap;
    //out << YAML::Key << "structure_length" << YAML::Value << static_cast<const uint16_t>(obj.structure_length());
    out << YAML::Key << "connection_type_code" << YAML::Value << to_string(obj.connection_type_code);
    out << YAML::EndMap;
    return out;
}

/* 7.5.4 Description Information Block (DIB) */

YAML::Emitter & operator<<(YAML::Emitter & out, const KNX::Description_Information_Block & obj)
{
    out << YAML::Key << "Description_Information_Block";
    out << YAML::BeginMap;
    //out << YAML::Key << "structure_length" << YAML::Value << static_cast<uint16_t>(obj.structure_length());
    out << YAML::Key << "description_type_code" << YAML::Value << to_string(obj.description_type_code);
    out << YAML::EndMap;
    return out;
}

YAML::Emitter & operator<<(YAML::Emitter & out, const KNX::Device_Information_DIB & obj)
{
    out << YAML::Key << "Device_Information_DIB";
    out << YAML::BeginMap;
    out << static_cast<const KNX::Description_Information_Block>(obj);
    out << YAML::Key << "medium" << YAML::Value << to_string(obj.medium);
    out << YAML::Key << "device_status" << YAML::Value << "0x" + to_hex(obj.device_status);
    out << YAML::Key << "individual_address" << YAML::Value << "0x" + to_hex(obj.individual_address);
    out << YAML::Key << "project_installation_identifier" << YAML::Value << "0x" + to_hex(obj.project_installation_identifier);
    out << YAML::Key << "device_serial_number" << YAML::Value << to_string(obj.device_serial_number);
    out << YAML::Key << "device_routing_multicast_address" << YAML::Value << to_string(obj.device_routing_multicast_address);
    out << YAML::Key << "device_mac_address" << YAML::Value << to_string(obj.device_mac_address);
    out << YAML::Key << "device_friendly_name" << YAML::Value << to_string(obj.device_friendly_name);
    out << YAML::EndMap;
    return out;
}

YAML::Emitter & operator<<(YAML::Emitter & out, const KNX::Supported_Service_Families_DIB & obj)
{
    out << YAML::Key << "Supported_Service_Families_DIB";
    out << YAML::BeginMap;
    out << static_cast<const KNX::Description_Information_Block>(obj);
    out << YAML::Key << "service_families";
    out << YAML::BeginMap;
    for (const auto & service_family : obj.service_families) {
        out << YAML::Key;
        switch (service_family.first) {
        case 0x02:
            out << "Core";
            break;
        case 0x03:
            out << "Device Management";
            break;
        case 0x04:
            out << "Tunnelling";
            break;
        case 0x05:
            out << "Routing";
            break;
        case 0x06:
            out << "Remote Logging";
            break;
        case 0x07:
            out << "Remote Configuration and Diagnosis";
            break;
        case 0x08:
            out << "Object Server";
            break;
        default:
            out << "0x" + to_hex(service_family.first);
        }
        out << YAML::Value << static_cast<uint16_t>(service_family.second);
    }
    out << YAML::EndMap;
    out << YAML::EndMap;
    return out;
}

YAML::Emitter & operator<<(YAML::Emitter & out, const KNX::IP_Config_DIB & obj)
{
    out << YAML::Key << "IP_Config_DIB";
    out << YAML::BeginMap;
    out << static_cast<const KNX::Description_Information_Block>(obj);
    out << YAML::Key << "ip_address" << YAML::Value << to_string(obj.ip_address);
    out << YAML::Key << "subnet_mask" << YAML::Value << to_string(obj.subnet_mask);
    out << YAML::Key << "default_gateway" << YAML::Value << to_string(obj.default_gateway);
    out << YAML::Key << "ip_capabilities" << YAML::Value << static_cast<uint16_t>(obj.ip_capabilities);
    out << YAML::Key << "ip_assignment_method" << YAML::Value << static_cast<uint16_t>(obj.ip_assignment_method);
    out << YAML::EndMap;
    return out;
}

YAML::Emitter & operator<<(YAML::Emitter & out, const KNX::IP_Current_Config_DIB & obj)
{
    out << YAML::Key << "IP_Current_Config_DIB";
    out << YAML::BeginMap;
    out << static_cast<const KNX::Description_Information_Block>(obj);
    out << YAML::Key << "current_ip_address" << YAML::Value << to_string(obj.current_ip_address);
    out << YAML::Key << "current_subnet_mask" << YAML::Value << to_string(obj.current_subnet_mask);
    out << YAML::Key << "current_default_gateway" << YAML::Value << to_string(obj.current_default_gateway);
    out << YAML::Key << "dhcp_server" << YAML::Value << to_string(obj.dhcp_server);
    out << YAML::Key << "current_ip_assignment_method" << YAML::Value << static_cast<uint16_t>(obj.current_ip_assignment_method);
    out << YAML::EndMap;
    return out;
}

YAML::Emitter & operator<<(YAML::Emitter & out, const KNX::Addresses_DIB & obj)
{
    out << YAML::Key << "KNX_Addresses_DIB";
    out << YAML::BeginMap;
    out << static_cast<const KNX::Description_Information_Block>(obj);
    out << YAML::Key << "knx_individual_address" << YAML::Value << static_cast<uint16_t>(obj.individual_address);
    out << YAML::Key << "additional_individual_addresses" << YAML::Value << obj.additional_individual_addresses;
    out << YAML::EndMap;
    return out;
}

YAML::Emitter & operator<<(YAML::Emitter & out, const KNX::Manufacturer_Data_DIB & obj)
{
    out << YAML::Key << "Manufacturer_Data_DIB";
    out << YAML::BeginMap;
    out << static_cast<const KNX::Description_Information_Block>(obj);
    out << YAML::Key << "knx_manufacturer_id" << YAML::Value << static_cast<uint16_t>(obj.manufacturer_id);
    out << YAML::Key << "manufacturer_specific_data" << YAML::Value << obj.manufacturer_specific_data;
    out << YAML::EndMap;
    return out;
}

/* 7.6 Discovery */

YAML::Emitter & operator<<(YAML::Emitter & out, const KNX::Search_Request_Frame & obj)
{
    out << YAML::Key << "Search_Request";
    out << YAML::BeginMap;
    out << dynamic_cast<const KNX::Frame &>(obj);
    out << *obj.discovery_endpoint;
    out << YAML::EndMap;
    return out;
}

YAML::Emitter & operator<<(YAML::Emitter & out, const KNX::Search_Response_Frame & obj)
{
    out << YAML::Key << "Search_Response";
    out << YAML::BeginMap;
    out << dynamic_cast<const KNX::Frame &>(obj);
    out << *obj.control_endpoint;
    out << *obj.device_hardware;
    out << *obj.supported_service_families;
    out << YAML::EndMap;
    return out;
}

/* 7.7 Self description */

YAML::Emitter & operator<<(YAML::Emitter & out, const KNX::Description_Request_Frame & obj)
{
    out << YAML::Key << "Description_Request";
    out << YAML::BeginMap;
    out << dynamic_cast<const KNX::Frame &>(obj);
    out << *obj.control_endpoint;
    out << YAML::EndMap;
    return out;
}

YAML::Emitter & operator<<(YAML::Emitter & out, const KNX::Description_Response_Frame & obj)
{
    out << YAML::Key << "Description_Response";
    out << YAML::BeginMap;
    out << dynamic_cast<const KNX::Frame &>(obj);
    out << *obj.device_hardware;
    out << *obj.supported_service_families;
    for (const auto & dib : obj.other_device_information) {
        out << *dib;
    }
    out << YAML::EndMap;
    return out;
}

/* 7.8 Connection management */

YAML::Emitter & operator<<(YAML::Emitter & out, const KNX::Connect_Request_Frame & obj)
{
    out << YAML::Key << "Description_Request";
    out << YAML::BeginMap;
    out << dynamic_cast<const KNX::Frame &>(obj);
    out << *obj.control_endpoint;
    out << *obj.data_endpoint;
    out << *obj.connection_request_information;
    out << YAML::EndMap;
    return out;
}

YAML::Emitter & operator<<(YAML::Emitter & out, const KNX::Connect_Response_Frame & obj)
{
    out << YAML::Key << "Connect_Response";
    out << YAML::BeginMap;
    out << dynamic_cast<const KNX::Frame &>(obj);
    out << YAML::Key << "communication_channel_id" << YAML::Value << static_cast<uint16_t>(obj.communication_channel_id);
    out << YAML::Key << "status" << YAML::Value << to_string(obj.status);
    out << *obj.data_endpoint;
    out << *obj.connection_response_data_block;
    out << YAML::EndMap;
    return out;
}

YAML::Emitter & operator<<(YAML::Emitter & out, const KNX::Connectionstate_Request_Frame & obj)
{
    out << YAML::Key << "Connectionstate_Request";
    out << YAML::BeginMap;
    out << dynamic_cast<const KNX::Frame &>(obj);
    out << YAML::Key << "communication_channel_id" << YAML::Value << static_cast<uint16_t>(obj.communication_channel_id);
    out << *obj.control_endpoint;
    out << YAML::EndMap;
    return out;
}

YAML::Emitter & operator<<(YAML::Emitter & out, const KNX::Connectionstate_Response_Frame & obj)
{
    out << YAML::Key << "Connectionstate_Response";
    out << YAML::BeginMap;
    out << dynamic_cast<const KNX::Frame &>(obj);
    out << YAML::Key << "communication_channel_id" << YAML::Value << static_cast<uint16_t>(obj.communication_channel_id);
    out << YAML::Key << "status" << YAML::Value << to_string(obj.status);
    out << YAML::EndMap;
    return out;
}

YAML::Emitter & operator<<(YAML::Emitter & out, const KNX::Disconnect_Request_Frame & obj)
{
    out << YAML::Key << "Disconnect_Request";
    out << YAML::BeginMap;
    out << dynamic_cast<const KNX::Frame &>(obj);
    out << YAML::Key << "communication_channel_id" << YAML::Value << static_cast<uint16_t>(obj.communication_channel_id);
    out << *obj.control_endpoint;
    out << YAML::EndMap;
    return out;
}

YAML::Emitter & operator<<(YAML::Emitter & out, const KNX::Disconnect_Response_Frame & obj)
{
    out << YAML::Key << "Disconnect_Response";
    out << YAML::BeginMap;
    out << dynamic_cast<const KNX::Frame &>(obj);
    out << YAML::Key << "communication_channel_id" << YAML::Value << static_cast<uint16_t>(obj.communication_channel_id);
    out << YAML::Key << "status" << YAML::Value << to_string(obj.status);
    out << YAML::EndMap;
    return out;
}

/* 8.6.2 Host Protocol Address Information */

YAML::Emitter & operator<<(YAML::Emitter & out, const KNX::IP_Host_Protocol_Address_Information & obj)
{
    out << YAML::Key << "IP_Host_Protocol_Address_Information";
    out << YAML::BeginMap;
    out << static_cast<const KNX::Host_Protocol_Address_Information>(obj);
    //out << YAML::Key << "structure_length" << YAML::Value << static_cast<uint16_t>(obj.structure_length());
    out << YAML::Key << "host_protocol_code" << YAML::Value << to_string(obj.host_protocol_code);
    switch (obj.host_protocol_code) {
    case KNX::Host_Protocol_Code::UNDEFINED:
        break;
    case KNX::Host_Protocol_Code::IPV4_UDP:
    case KNX::Host_Protocol_Code::IPV4_TCP:
        out << YAML::Key << "ip_address" << YAML::Value << to_string(obj.ip_address);
        out << YAML::Key << "ip_port_number" << YAML::Value << obj.ip_port_number;
        break;
    }
    out << YAML::EndMap;
    return out;
}

#endif
