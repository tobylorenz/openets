#include "DebugWindow.h"

#include <QAction>
#include <QApplication>
#include <QClipboard>
#include <QToolBar>
#include <QVBoxLayout>

/** link to current debug window */
static DebugWindow * currentDebugWindow{nullptr};

DebugWindow::DebugWindow(QWidget * parent) :
    QWidget(parent)
{
    setWindowTitle("Debug");

    /* Layout */
    QVBoxLayout * layout = new QVBoxLayout(this);
    layout->setContentsMargins(0, 0, 0, 0);
    layout->setSpacing(0);
    setLayout(layout);

    /* actions */
    QAction * copyAct = new QAction(tr("Copy to clipboard"), this);
    copyAct->setStatusTip(tr("Copy content as text to clipboard."));
    connect(copyAct, &QAction::triggered, this, &DebugWindow::copyToClipboard);

    /* toolbars */
    QToolBar * toolBar = new QToolBar(tr("Clipboard"));
    toolBar->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
    toolBar->addAction(copyAct);
    layout->addWidget(toolBar);

    /* Text Edit */
    textEdit = new QTextEdit();
    textEdit->setReadOnly(true);
    layout->addWidget(textEdit);

    /* Color Palette */
    QPalette p = textEdit->palette();
    p.setColor(QPalette::Base, Qt::black);
    textEdit->setPalette(p);

    currentDebugWindow = this;
}

DebugWindow::~DebugWindow()
{
    currentDebugWindow = nullptr;
}

void DebugWindow::messageHandler(QtMsgType type, const QMessageLogContext & /*context*/, const QString & msg)
{
    if (!currentDebugWindow) {
        return;
    }

    QString messageText;

    /* header */
    switch (type) {
    case QtDebugMsg:
        messageText = "<p style='color:green'>";
        break;
    case QtWarningMsg:
        messageText = "<p style='color:yellow'>";
        break;
    case QtCriticalMsg:
        messageText = "<p style='color:red'>";
        break;
    case QtFatalMsg:
        messageText = "<p style='color:red; font-weight:bold'>";
        break;
    case QtInfoMsg:
    default:
        messageText = "<p style='color:gray'>";
    }

    /* time */
    //messageText += QTime::currentTime();
    //messageText += ": ";

    /* text */
    messageText += msg;

    /* trailer */
    messageText += "<br/></p>\n";

    /* insert this text */
    currentDebugWindow->textEdit->insertHtml(messageText);

    /* scroll to bottom */
    QTextCursor textCursor = currentDebugWindow->textEdit->textCursor();
    textCursor.movePosition(QTextCursor::End);
    currentDebugWindow->textEdit->setTextCursor(textCursor);
}

void DebugWindow::copyToClipboard()
{
    QClipboard * clipboard = QApplication::clipboard();
    clipboard->setText(textEdit->toPlainText());
}
