#pragma once

#include <QTextEdit>
#include <QWidget>

/**
 * @brief Debug Window
 *
 * This window displays the collection of Qt messages from the DebugManager.
 *
 * @todo Option to select minimum level of interest: Debug, Warning, Critical, Fatal
 */
class DebugWindow : public QWidget
{
    Q_OBJECT

public:
    /**
     * Constructor
     *
     * @param[in] parent Parent widget
     */
    explicit DebugWindow(QWidget * parent = 0);

    virtual ~DebugWindow();

public Q_SLOTS:
    static void messageHandler(QtMsgType type, const QMessageLogContext & context, const QString & msg);

private Q_SLOTS:
    void copyToClipboard();

private:
    QTextEdit * textEdit;
};

// qDebug() is used for writing custom debug output.
// qInfo() is used for informational messages.
// qWarning() is used to report warnings and recoverable errors in your application.
// qCritical() is used for writing critical error messages and reporting system errors.
// qFatal() is used for writing fatal error messages shortly before exiting.
