find_path(KNX_INCLUDE_DIR
    NAME
        KNX.h
    PATHS
        ${CMAKE_SOURCE_DIR}/../KNX/src)

find_library(KNX_LIBRARY
    NAMES
        KNX
    PATHS
        ${CMAKE_SOURCE_DIR}/../build-KNX-Desktop-Debug/src/KNX
        ${CMAKE_SOURCE_DIR}/../build-KNX-Desktop_clang-Debug/src/KNX)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(KNX DEFAULT_MSG KNX_INCLUDE_DIR KNX_LIBRARY)

mark_as_advanced(KNX_INCLUDE_DIR KNX_LIBRARY)

set(KNX_LIBRARIES ${KNX_LIBRARY})
