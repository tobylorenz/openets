find_path(Asio_INCLUDE_DIR
    NAMES asio.hpp
    HINTS
        /usr/include
    DOC "Asio C++ Library")

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(Asio Asio_INCLUDE_DIR)

mark_as_advanced(Asio_INCLUDE_DIR)
