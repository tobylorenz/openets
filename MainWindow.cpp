#include "MainWindow.h"

#include <QApplication>
#include <QDebug>
#include <QFileDialog>
#include <QMdiSubWindow>
#include <QMenuBar>
#include <QMessageBox>
#include <QProgressDialog>
#include <QSettings>
#include <QStatusBar>
#include <QToolBar>

#include <DebugWindow.h>
#include <Project/v20/knx/GroupAddresses_GroupRanges_t.h>
#include <Project/v20/knx/GroupAddresses_t.h>
#include <Project/v20/knx/Locations_t.h>
#include <Project/v20/knx/ManufacturerData_t.h>
#include <Project/v20/knx/MasterData_t.h>
#include <Project/v20/knx/Project_Installations_Installation_t.h>
#include <Project/v20/knx/Project_Installations_t.h>
#include <Project/v20/knx/Project_t.h>
#include <Project/v20/knx/Topology_t.h>
#include <Project/v20/knx/Trades_t.h>
#include <Widgets/BusInterfacesWindow.h>
#include <Widgets/DeviceSelectorWidget.h>
#include <Widgets/DeviceWidgets/ADCView.h>
#include <Widgets/DeviceWidgets/AuthorizeView.h>
#include <Widgets/DeviceWidgets/DeviceDescriptorView.h>
#include <Widgets/DeviceWidgets/FileStreamView.h>
#include <Widgets/DeviceWidgets/KeyView.h>
#include <Widgets/DeviceWidgets/LinkView.h>
#include <Widgets/DeviceWidgets/MemoryView.h>
#include <Widgets/DeviceWidgets/PropertyView.h>
#include <Widgets/DeviceWidgets/RestartView.h>
#include <Widgets/DeviceWidgets/UserManufacturerInfoView.h>
#include <Widgets/NetworkManagement/IndividualAddressView.h>
#include <Widgets/ProjectWidgets/ProjectView.h>
#include <Widgets/ServiceWidgets/DomainAddressView.h>
#include <Widgets/ServiceWidgets/GroupValueView.h>
#include <Widgets/ServiceWidgets/NetworkParameterView.h>
#include <Widgets/ServiceWidgets/SystemNetworkParameterView.h>

#ifdef OPTION_EXPORT_OpenHAB
#include <Exports/OpenHAB.h>
#endif
#ifdef OPTION_EXPORT_SmartHomeNG
#include <Exports/SmartHomeNG.h>
#endif

MainWindow::MainWindow() :
    QMainWindow(),
    knxnet(new KNXnet(this)),
    connection(new Connection(this))
{
    setAttribute(Qt::WA_DeleteOnClose);
    setWindowFilePath(QString());
    setWindowTitle("OpenETS");

    /* MDI Area */
    mdiArea = new QMdiArea(this);
    mdiArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    mdiArea->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    setCentralWidget(mdiArea);

    createActions();
    createMenus();
    createToolBars();
    createStatusBar();

    updateRecentPathActions();

    /* bind handlers */
    connect(knxnet, &KNXnet::connectionCreated, this, &MainWindow::on_knxnet_connectionCreated);
}

MainWindow::~MainWindow()
{
    documentClose();
}

void MainWindow::applicationExit()
{
    qApp->closeAllWindows();
}

void MainWindow::documentNew()
{
    connection->getProjectBundle()->currentPath.clear();
    // @todo MainWindow::documentNew
}

void MainWindow::documentOpenFile()
{
    QFileDialog dialog(this, tr("Open ETS Project File"));
    dialog.setFileMode(QFileDialog::ExistingFile);
    dialog.setNameFilter(tr("All files(*.knxproj *.knxprod);;KNX Projects(*.knxproj);;KNX Products(*.knxprod)"));
    if (dialog.exec()) {
        QStringList fileNames = dialog.selectedFiles();
        if (fileNames.count() == 1) {
            QString fileName = fileNames.first();
            loadPath(fileName);
        }
    }
}

void MainWindow::documentOpenDir()
{
    QFileDialog dialog(this, tr("Open ETS Project Directory"));
    dialog.setFileMode(QFileDialog::Directory);
    dialog.setOption(QFileDialog::ShowDirsOnly, true);
    if (dialog.exec()) {
        QStringList dirNames = dialog.selectedFiles();
        if (dirNames.count() == 1) {
            QString dirName = dirNames.first();
            loadPath(dirName);
        }
    }
}

void MainWindow::documentOpenRecent()
{
    QAction * action = qobject_cast<QAction *>(sender());
    if (action) {
        QString fileName = action->data().toString();
        loadPath(fileName);
    }
}

void MainWindow::documentSave()
{
    if (connection->getProjectBundle()->currentPath.isEmpty()) {
        documentSaveAs();
    } else {
        savePath(connection->getProjectBundle()->currentPath);
    }
}

void MainWindow::documentSaveAs()
{
    QString fileName = QFileDialog::getSaveFileName(this, tr("Save Document"), "", tr("KNX Project (*.knxproj)"));
    if (!fileName.isEmpty()) {
        savePath(fileName);
    }
}

void MainWindow::documentClose()
{
    // @todo close MainWindow if there are other MainWindows available, otherwise just close document
    // @todo check for modifications before closing file

    mdiArea->closeAllSubWindows();

    connection->clearProjectBundle();

    setWindowFilePath(QString());
}

void MainWindow::documentImportKnxProd()
{

}

#ifdef OPTION_EXPORT_OpenHAB
void MainWindow::documentExportAsOpenHAB()
{
    Project::v20::knx::Project_Installations_Installation_t * installation = connection->getEtsInstallation();
    Q_ASSERT(installation);
    Project::v20::knx::Topology_t * topology = installation->Topology;
    Q_ASSERT(topology);
    const Project::v20::knx::Topology_Area_Line_t * tpLine{nullptr}; // @todo select line
    for (const Project::v20::knx::Topology_Area_t * area : topology->Area) {
        Q_ASSERT(area);
        for (const Project::v20::knx::Topology_Area_Line_t * line : area->Line) {
            Q_ASSERT(line);
            if (line->MediumTypeRefId == "MT-0") { // TP
                tpLine = line;
                break;
            }
        }
    }
    if (!tpLine) {
        QMessageBox::warning(this, tr("OpenHAB export"), tr("No TP line found."));
        return;
    }
    Q_ASSERT(tpLine);
    Project::v20::knx::DeviceInstance_t * router{nullptr}; // @todo select router
    for (Project::v20::knx::DeviceInstance_t * deviceInstance : tpLine->DeviceInstance) {
        Q_ASSERT(deviceInstance);
        if (!deviceInstance->ReadMaxRoutingAPDULength.isEmpty()) {
            router = deviceInstance;
            break;
        }
    }
    if (!router) {
        QMessageBox::warning(this, tr("OpenHAB export"), tr("No router found."));
        return;
    }
    Q_ASSERT(router);

    OpenHAB openHAB;
    QString thingsFileName = QFileDialog::getSaveFileName(this, tr("Export OpenHAB Things"), "", tr("OpenHAB Things (*.things)"));
    openHAB.saveThings(thingsFileName, tpLine, router);
    QString itemsFileName = QFileDialog::getSaveFileName(this, tr("Export OpenHAB Items"), "", tr("OpenHAB Items (*.items)"));
    openHAB.saveItems(itemsFileName, tpLine);
}
#endif

#ifdef OPTION_EXPORT_SmartHomeNG
void MainWindow::documentExportAsSmartHomeNG()
{
    Project::v20::knx::Project_Installations_Installation_t * installation = connection->getEtsInstallation();
    Q_ASSERT(installation);
    Project::v20::knx::Locations_t * locations = installation->Locations;
    Q_ASSERT(locations);

    SmartHomeNG smartHomeNG;
    QString fileName = QFileDialog::getSaveFileName(this, tr("Export SmartHomeNG Items"), "", tr("SmartHomeNG Items (*.yaml)"));
    smartHomeNG.save(fileName, locations);
}
#endif

//void MainWindow::editCopy() {
//}

//void MainWindow::editCut() {
//}

//void MainWindow::editDelete() {
//}

//void MainWindow::editPaste() {
//}

void MainWindow::helpDebug()
{
    DebugWindow * debugWindow = new DebugWindow(this);
    Q_ASSERT(debugWindow);

    auto * subWindow = mdiArea->addSubWindow(debugWindow);
    Q_ASSERT(subWindow);
    subWindow->setAttribute(Qt::WA_DeleteOnClose);
    subWindow->setWindowTitle(tr("Debug Window"));

    debugWindow->show();
}

void MainWindow::helpAbout()
{
    QMessageBox::about(this, tr("About OpenETS"), tr("OpenETS allows configuration of KNX networks and devices.\n\nOpenETS aims to provide an advanced view on the network, e.g. direct table and parameter modification, or memory access.\n\nIt supports ETS Project files, however modified files cannot be digitally signed yet, so cannot be imported in original tool afterwards."));
}

void MainWindow::helpAboutQt()
{
    qApp->aboutQt();
}

void MainWindow::knxBusInterfaces()
{
    BusInterfacesWindow * busInterfacesWindow = new BusInterfacesWindow(this);
    Q_ASSERT(busInterfacesWindow);
    busInterfacesWindow->setKNXnet(knxnet);

    auto * subWindow = mdiArea->addSubWindow(busInterfacesWindow);
    Q_ASSERT(subWindow);
    subWindow->setAttribute(Qt::WA_DeleteOnClose);
    subWindow->setWindowIcon(QIcon::fromTheme("Bus"));
    subWindow->setWindowTitle(tr("Bus Interfaces"));

    busInterfacesWindow->show();
}

void MainWindow::knxADCView()
{
    ADCView * adcView = new ADCView(this);
    Q_ASSERT(adcView);

    /* open MDI window */
    auto * subWindow = mdiArea->addSubWindow(adcView);
    Q_ASSERT(subWindow);
    subWindow->setAttribute(Qt::WA_DeleteOnClose);
    subWindow->setWindowTitle(tr("ADC Read"));
    adcView->show();
}

void MainWindow::knxAuthorizeView()
{
    AuthorizeView * authorizeView = new AuthorizeView(this);
    Q_ASSERT(authorizeView);

    /* open MDI window */
    auto * subWindow = mdiArea->addSubWindow(authorizeView);
    Q_ASSERT(subWindow);
    subWindow->setAttribute(Qt::WA_DeleteOnClose);
    subWindow->setWindowTitle(tr("Authorize Request"));
    authorizeView->show();
}

void MainWindow::knxDeviceDescriptorView()
{
    /* select device */
    DeviceSelectorWidget * deviceSelectorWidget = new DeviceSelectorWidget(this);
    Q_ASSERT(deviceSelectorWidget);
    deviceSelectorWidget->setConnection(connection);
    if (deviceSelectorWidget->exec() != QDialog::Accepted) {
        return;
    }
    DeviceData * device = connection->device(deviceSelectorWidget->selectedAddress());

    /* device descriptor view */
    DeviceDescriptorView * deviceDescriptorView = new DeviceDescriptorView(this);
    Q_ASSERT(deviceDescriptorView);
    deviceDescriptorView->setConnection(connection);
    deviceDescriptorView->setDevice(device);

    /* open MDI window */
    auto * subWindow = mdiArea->addSubWindow(deviceDescriptorView);
    Q_ASSERT(subWindow);
    subWindow->setAttribute(Qt::WA_DeleteOnClose);
    subWindow->setWindowTitle(tr("DeviceDescriptor - %1").arg(deviceSelectorWidget->selectedAddressStr()));
    deviceDescriptorView->show();
}

void MainWindow::knxDomainAddressView()
{
    DomainAddressView * domainAddressView = new DomainAddressView(this);
    Q_ASSERT(domainAddressView);

    /* open MDI window */
    auto * subWindow = mdiArea->addSubWindow(domainAddressView);
    Q_ASSERT(subWindow);
    subWindow->setAttribute(Qt::WA_DeleteOnClose);
    subWindow->setWindowTitle(tr("DomainAddress (Selective/SerialNumber) Read/Write"));
    domainAddressView->show();
}

void MainWindow::knxFileStreamView()
{
    FileStreamView * fileStreamView = new FileStreamView(this);
    Q_ASSERT(fileStreamView);

    /* open MDI window */
    auto * subWindow = mdiArea->addSubWindow(fileStreamView);
    Q_ASSERT(subWindow);
    subWindow->setAttribute(Qt::WA_DeleteOnClose);
    subWindow->setWindowTitle(tr("FileStream InfoReport"));
    fileStreamView->show();
}

void MainWindow::knxGroupValueView()
{
    /* group value view */
    GroupValueView * groupValueView = new GroupValueView(this);
    Q_ASSERT(groupValueView);
    groupValueView->setConnection(connection);

    /* open MDI window */
    auto * subWindow = mdiArea->addSubWindow(groupValueView);
    Q_ASSERT(subWindow);
    subWindow->setAttribute(Qt::WA_DeleteOnClose);
    subWindow->setWindowIcon(QIcon::fromTheme("GroupAddress"));
    subWindow->setWindowTitle(tr("GroupValue Read/Write"));
    groupValueView->show();
}

void MainWindow::knxKeyView()
{
    KeyView * keyView = new KeyView(this);
    Q_ASSERT(keyView);

    /* open MDI window */
    auto * subWindow = mdiArea->addSubWindow(keyView);
    Q_ASSERT(subWindow);
    subWindow->setAttribute(Qt::WA_DeleteOnClose);
    subWindow->setWindowTitle(tr("Key Write"));
    keyView->show();
}

void MainWindow::knxLinkView()
{
    LinkView * linkView = new LinkView(this);
    Q_ASSERT(linkView);

    /* open MDI window */
    auto * subWindow = mdiArea->addSubWindow(linkView);
    Q_ASSERT(subWindow);
    subWindow->setAttribute(Qt::WA_DeleteOnClose);
    subWindow->setWindowTitle(tr("Link Read/Write"));
    linkView->show();
}

void MainWindow::knxMemoryView()
{
    /* select device */
    DeviceSelectorWidget * deviceSelectorWidget = new DeviceSelectorWidget(this);
    Q_ASSERT(deviceSelectorWidget);
    deviceSelectorWidget->setConnection(connection);
    if (deviceSelectorWidget->exec() != QDialog::Accepted) {
        return;
    }
    DeviceData * device = connection->device(deviceSelectorWidget->selectedAddress());

    /* memory view */
    MemoryView * memoryView = new MemoryView(this);
    Q_ASSERT(memoryView);
    memoryView->setConnection(connection);
    memoryView->setDevice(device);

    /* open MDI window */
    auto * subWindow = mdiArea->addSubWindow(memoryView);
    Q_ASSERT(subWindow);
    subWindow->setAttribute(Qt::WA_DeleteOnClose);
    subWindow->setWindowTitle(tr("Memory(Bit) Read/Write - %1").arg(deviceSelectorWidget->selectedAddressStr()));
    memoryView->show();
}

void MainWindow::knxNetworkParameterView()
{
    NetworkParameterView * networkParameterView = new NetworkParameterView(this);
    Q_ASSERT(networkParameterView);

    /* open MDI window */
    auto * subWindow = mdiArea->addSubWindow(networkParameterView);
    Q_ASSERT(subWindow);
    subWindow->setAttribute(Qt::WA_DeleteOnClose);
    subWindow->setWindowTitle(tr("NetworkParameter InfoReport/Read/Write"));
    networkParameterView->show();
}

void MainWindow::knxPropertyView()
{
    /* select device */
    DeviceSelectorWidget * deviceSelectorWidget = new DeviceSelectorWidget(this);
    Q_ASSERT(deviceSelectorWidget);
    deviceSelectorWidget->setConnection(connection);
    if (deviceSelectorWidget->exec() != QDialog::Accepted) {
        return;
    }
    DeviceData * device = connection->device(deviceSelectorWidget->selectedAddress());

    /* property view */
    PropertyView * propertyView = new PropertyView(this);
    Q_ASSERT(propertyView);
    propertyView->setConnection(connection);
    propertyView->setDevice(device);

    /* open MDI window */
    auto * subWindow = mdiArea->addSubWindow(propertyView);
    Q_ASSERT(subWindow);
    subWindow->setAttribute(Qt::WA_DeleteOnClose);
    subWindow->setWindowTitle(tr("(Function) Property View - %1").arg(deviceSelectorWidget->selectedAddressStr()));
    propertyView->show();
}

void MainWindow::knxRestartView()
{
    /* select device */
    DeviceSelectorWidget * deviceSelectorWidget = new DeviceSelectorWidget(this);
    Q_ASSERT(deviceSelectorWidget);
    deviceSelectorWidget->setConnection(connection);
    if (deviceSelectorWidget->exec() != QDialog::Accepted) {
        return;
    }
    DeviceData * device = connection->device(deviceSelectorWidget->selectedAddress());

    /* restart view */
    RestartView * restartView = new RestartView(this);
    Q_ASSERT(restartView);
    restartView->setConnection(connection);
    restartView->setDevice(device);

    /* open MDI window */
    auto * subWindow = mdiArea->addSubWindow(restartView);
    Q_ASSERT(subWindow);
    subWindow->setAttribute(Qt::WA_DeleteOnClose);
    subWindow->setWindowTitle(tr("Restart - %1").arg(deviceSelectorWidget->selectedAddressStr()));
    restartView->show();
}

void MainWindow::knxSystemNetworkParameterView()
{
    SystemNetworkParameterView * systemNetworkParameterView = new SystemNetworkParameterView(this);
    Q_ASSERT(systemNetworkParameterView);

    /* open MDI window */
    auto * subWindow = mdiArea->addSubWindow(systemNetworkParameterView);
    Q_ASSERT(subWindow);
    subWindow->setAttribute(Qt::WA_DeleteOnClose);
    subWindow->setWindowTitle(tr("SystemNetworkParameter Read/Write"));
    systemNetworkParameterView->show();
}

void MainWindow::knxUserManufacturerInfoView()
{
    UserManufacturerInfoView * userManufacturerInfoView = new UserManufacturerInfoView(this);
    Q_ASSERT(userManufacturerInfoView);

    /* open MDI window */
    auto * subWindow = mdiArea->addSubWindow(userManufacturerInfoView);
    Q_ASSERT(subWindow);
    subWindow->setAttribute(Qt::WA_DeleteOnClose);
    subWindow->setWindowTitle(tr("UserManufacturerInfo Read"));
    userManufacturerInfoView->show();
}

void MainWindow::knxNetworkManagementIndividualAddressView()
{
    IndividualAddressView * individualAddressView = new IndividualAddressView(this);
    individualAddressView->setConnection(connection);
    Q_ASSERT(individualAddressView);

    /* open MDI window */
    auto * subWindow = mdiArea->addSubWindow(individualAddressView);
    Q_ASSERT(subWindow);
    subWindow->setAttribute(Qt::WA_DeleteOnClose);
    subWindow->setWindowTitle(tr("Network Management - IndividualAddress (SerialNumber) Read/Write"));
    individualAddressView->show();
}

void MainWindow::projectKNX()
{
    auto * knxRoot = connection->getEtsKnx();
    Q_ASSERT(knxRoot);

    auto * knxView = new ProjectView(knxRoot, mdiArea);
    Q_ASSERT(knxView);

    auto * subWindow = mdiArea->addSubWindow(knxView);
    Q_ASSERT(subWindow);
    subWindow->setAttribute(Qt::WA_DeleteOnClose);
    subWindow->setWindowIcon(knxRoot->treeData(Qt::DecorationRole).value<QIcon>());
    subWindow->setWindowTitle(tr("ETS Project KNX (root)"));

    knxView->show();
}

void MainWindow::projectInstallation()
{
    auto * projectInstallation = connection->getEtsInstallation();
    Q_ASSERT(projectInstallation);

    auto * projectRootView = new ProjectView(projectInstallation, mdiArea);
    Q_ASSERT(projectRootView);

    auto * subWindow = mdiArea->addSubWindow(projectRootView);
    Q_ASSERT(subWindow);
    subWindow->setAttribute(Qt::WA_DeleteOnClose);
    subWindow->setWindowIcon(projectInstallation->treeData(Qt::DecorationRole).value<QIcon>());
    subWindow->setWindowTitle(tr("ETS Project Installation"));

    projectRootView->show();
}

void MainWindow::projectTopology()
{
    auto * topology = connection->getEtsInstallation()->Topology;
    Q_ASSERT(topology);

    auto * topologyView = new ProjectView(topology, mdiArea);
    Q_ASSERT(topologyView);

    auto * subWindow = mdiArea->addSubWindow(topologyView);
    Q_ASSERT(subWindow);
    subWindow->setAttribute(Qt::WA_DeleteOnClose);
    subWindow->setWindowIcon(topology->treeData(Qt::DecorationRole).value<QIcon>());
    subWindow->setWindowTitle(tr("ETS Project Topology"));

    topologyView->show();
}

void MainWindow::projectLocations()
{
    auto * locations = connection->getEtsInstallation()->Locations;
    Q_ASSERT(locations);

    auto * locationsView = new ProjectView(locations, mdiArea);
    Q_ASSERT(locationsView);

    auto * subWindow = mdiArea->addSubWindow(locationsView);
    Q_ASSERT(subWindow);
    subWindow->setAttribute(Qt::WA_DeleteOnClose);
    subWindow->setWindowIcon(locations->treeData(Qt::DecorationRole).value<QIcon>());
    subWindow->setWindowTitle(tr("ETS Project Locations"));

    locationsView->show();
}

void MainWindow::projectGroupAddresses()
{
    auto * groupRanges = connection->getEtsInstallation()->GroupAddresses->GroupRanges;
    Q_ASSERT(groupRanges);

    auto * groupAddressesView = new ProjectView(groupRanges, mdiArea);
    Q_ASSERT(groupAddressesView);

    auto * subWindow = mdiArea->addSubWindow(groupAddressesView);
    Q_ASSERT(subWindow);
    subWindow->setAttribute(Qt::WA_DeleteOnClose);
    subWindow->setWindowIcon(groupRanges->treeData(Qt::DecorationRole).value<QIcon>());
    subWindow->setWindowTitle(tr("ETS Project Group Addresses"));

    groupAddressesView->show();
}

void MainWindow::projectTrades()
{
    auto * trades = connection->getEtsInstallation()->Trades;
    Q_ASSERT(trades);

    auto * tradesView = new ProjectView(trades, mdiArea);
    Q_ASSERT(tradesView);

    auto * subWindow = mdiArea->addSubWindow(tradesView);
    Q_ASSERT(subWindow);
    subWindow->setAttribute(Qt::WA_DeleteOnClose);
    subWindow->setWindowIcon(trades->treeData(Qt::DecorationRole).value<QIcon>());
    subWindow->setWindowTitle(tr("ETS Project Trades"));

    tradesView->show();
}

void MainWindow::projectManufacturerData()
{
    auto * manufacturerData = connection->getEtsKnx()->ManufacturerData;
    Q_ASSERT(manufacturerData);

    auto * ManufacturerDataView = new ProjectView(manufacturerData, mdiArea);
    Q_ASSERT(ManufacturerDataView);

    auto * subWindow = mdiArea->addSubWindow(ManufacturerDataView);
    Q_ASSERT(subWindow);
    subWindow->setAttribute(Qt::WA_DeleteOnClose);
    subWindow->setWindowIcon(manufacturerData->treeData(Qt::DecorationRole).value<QIcon>());
    subWindow->setWindowTitle(tr("ETS Project ManufacturerData"));

    ManufacturerDataView->show();
}

void MainWindow::projectMasterData()
{
    auto * masterData = connection->getEtsKnx()->MasterData;
    Q_ASSERT(masterData);

    auto * MasterDataView = new ProjectView(masterData, mdiArea);
    Q_ASSERT(MasterDataView);

    auto * subWindow = mdiArea->addSubWindow(MasterDataView);
    Q_ASSERT(subWindow);
    subWindow->setAttribute(Qt::WA_DeleteOnClose);
    subWindow->setWindowIcon(masterData->treeData(Qt::DecorationRole).value<QIcon>());
    subWindow->setWindowTitle(tr("ETS Project MasterData"));

    MasterDataView->show();
}

void MainWindow::windowClose()
{
    mdiArea->closeActiveSubWindow();
}

void MainWindow::on_knxnet_connectionCreated(const KNX::IP_Host_Protocol_Address_Information remote_control_endpoint)
{
    Q_ASSERT(knxnet);
    //IP_Communication_Channel * communication_channel = knxnet->communication_channels[remote_control_endpoint];
    Stack * stack = knxnet->stack(remote_control_endpoint);
    Q_ASSERT(stack);
    connection->setStack(stack);
}

void MainWindow::loadPath(const QString & path)
{
    QFileInfo fileInfo(path);
    if (!fileInfo.exists()) {
        QMessageBox::warning(this, tr("Recent Files"), tr("Cannot read path %1.").arg(path));
        return;
    }

    /* open project bundle */
    auto * projectBundle = new ProjectBundle(this);
    projectBundle->setPath(path);
    QProgressDialog progressDialog(this);
    progressDialog.setWindowModality(Qt::WindowModal);
    connect(projectBundle, &ProjectBundle::loadProgress, [&progressDialog](int count, int total, QString filename){
        progressDialog.setValue(count);
        progressDialog.setMaximum(total);
        progressDialog.setLabelText(filename);
    });
    projectBundle->load();
    if (projectBundle->etsProjectVersion == 0) {
        QMessageBox::warning(this, tr("Open Document"), tr("Unable to open project"));
        delete projectBundle;
        return;
    }
    connection->setProjectBundle(projectBundle);

    /* set project */
    auto * knxRoot = connection->getEtsKnx();
    Q_ASSERT(knxRoot);
    auto * project = knxRoot->Project.first(); // @todo support multiple projects
    if (knxRoot->Project.count() > 1) {
        qWarning() << "File has multiple projects. Using first.";
    }
    Q_ASSERT(project);
    connection->setEtsProject(project);

    /* enable project actions */
    projectKNXAction->setEnabled(true);
    projectInstallationAction->setEnabled(true);
    projectTopologyAction->setEnabled(true);
    projectLocationsAction->setEnabled(true);
    projectGroupAddressesAction->setEnabled(true);
    projectTradesAction->setEnabled(true);
    projectManufacturerDataAction->setEnabled(true);
    projectMasterDataAction->setEnabled(true);

    /* show project installation widget */
    projectInstallation();

    /* set current file */
    setCurrentPath(path);
    statusBar()->showMessage(tr("ETS Project loaded"), 2000);
}

void MainWindow::savePath(const QString & path)
{
    QFile file(path);
    if (!file.open(QFile::WriteOnly)) {
        QMessageBox::warning(this, tr("Recent Files"), tr("Cannot write file %1:\n%2.").arg(path).arg(file.errorString()));
        return;
    }

    /* save project bundle */
    auto * projectBundle = connection->getProjectBundle();
    projectBundle->save();

    /* set current file */
    setCurrentPath(path);
    statusBar()->showMessage(tr("File saved"), 2000);
}

void MainWindow::createActions()
{
    /* Actions - Application */
    applicationExitAction = new QAction(QIcon::fromTheme("application-exit"), tr("Exit"), this);
    applicationExitAction->setShortcuts(QKeySequence::Quit);
    connect(applicationExitAction, &QAction::triggered, this, &MainWindow::applicationExit);

    /* Actions - Document */
    documentNewAction = new QAction(QIcon::fromTheme("document-new"), tr("New ETS Project"), this);
    documentNewAction->setShortcuts(QKeySequence::New);
    //connect(documentNewAction, &QAction::triggered, this, &MainWindow::documentNew);
    documentOpenFileAction = new QAction(QIcon::fromTheme("document-open"), tr("Open ETS Project File"), this);
    documentOpenFileAction->setShortcuts(QKeySequence::Open);
    connect(documentOpenFileAction, &QAction::triggered, this, &MainWindow::documentOpenFile);
    documentOpenDirAction = new QAction(QIcon::fromTheme("folder-open"), tr("Open ETS Project Directory"), this);
    documentOpenDirAction->setShortcuts(QKeySequence::Open);
    connect(documentOpenDirAction, &QAction::triggered, this, &MainWindow::documentOpenDir);
    documentSaveAction = new QAction(QIcon::fromTheme("document-save"), tr("Save ETS Project"), this);
    documentSaveAction->setShortcuts(QKeySequence::Save);
    //connect(documentSaveAction, &QAction::triggered, this, &MainWindow::documentSave);
    documentSaveAsAction = new QAction(QIcon::fromTheme("document-save-as"), tr("Save ETS Project As"), this);
    documentSaveAsAction->setShortcuts(QKeySequence::SaveAs);
    //connect(documentSaveAsAction, &QAction::triggered, this, &MainWindow::documentSaveAs);
    documentCloseAction = new QAction(QIcon::fromTheme("document-close"), tr("Close ETS Project"), this);
    connect(documentCloseAction, &QAction::triggered, this, &MainWindow::documentClose);
    documentImportKnxProdAction = new QAction(tr("knxprod"), this);
    connect(documentImportKnxProdAction, &QAction::triggered, this, &MainWindow::documentImportKnxProd);
#ifdef OPTION_EXPORT_OpenHAB
    documentExportAsOpenHABAction = new QAction(tr("OpenHAB"), this);
    connect(documentExportAsOpenHABAction, &QAction::triggered, this, &MainWindow::documentExportAsOpenHAB);
#endif
#ifdef OPTION_EXPORT_SmartHomeNG
    documentExportAsSmartHomeNGAction = new QAction(tr("SmartHomeNG"), this);
    connect(documentExportAsSmartHomeNGAction, &QAction::triggered, this, &MainWindow::documentExportAsSmartHomeNG);
#endif

    /* Actions - Edit */
    editCopyAction = new QAction(QIcon::fromTheme("edit-copy"), tr("Copy"), this);
    editCopyAction->setShortcuts(QKeySequence::Copy);
    //connect(editCopyAction, &QAction::triggered, this, &MainWindow::editCopy);
    editCutAction = new QAction(QIcon::fromTheme("edit-cut"), tr("Cut"), this);
    editCutAction->setShortcuts(QKeySequence::Cut);
    //connect(editCutAction, &QAction::triggered, this, &MainWindow::editCut);
    editDeleteAction = new QAction(QIcon::fromTheme("edit-delete"), tr("Delete"), this);
    editDeleteAction->setShortcuts(QKeySequence::Delete);
    //connect(editDeleteAction, &QAction::triggered, this, &MainWindow::editDelete);
    editPasteAction = new QAction(QIcon::fromTheme("edit-paste"), tr("Paste"), this);
    editPasteAction->setShortcuts(QKeySequence::Paste);
    //connect(editPasteAction, &QAction::triggered, this, &MainWindow::editPaste);

    /* Actions - Project */
    projectKNXAction = new QAction(QIcon::fromTheme("KNX"), tr("KNX"), this);
    projectKNXAction->setEnabled(false);
    connect(projectKNXAction, &QAction::triggered, this, &MainWindow::projectKNX);
    projectInstallationAction = new QAction(QIcon::fromTheme("Project_Installations_Installation"), tr("KNX > Project > Installations > Installation"), this);
    projectInstallationAction->setEnabled(false);
    connect(projectInstallationAction, &QAction::triggered, this, &MainWindow::projectInstallation);
    projectTopologyAction = new QAction(QIcon::fromTheme("Topology"), tr("KNX > Project > Installations > Installation > Topology"), this);
    projectTopologyAction->setEnabled(false);
    connect(projectTopologyAction, &QAction::triggered, this, &MainWindow::projectTopology);
    projectLocationsAction = new QAction(QIcon::fromTheme("Locations"), tr("KNX > Project > Installations > Installation > Locations"), this);
    projectLocationsAction->setEnabled(false);
    connect(projectLocationsAction, &QAction::triggered, this, &MainWindow::projectLocations);
    projectGroupAddressesAction = new QAction(QIcon::fromTheme("GroupAddresses"), tr("KNX > Project > Installations > Installation > GroupAddresses"), this);
    projectGroupAddressesAction->setEnabled(false);
    connect(projectGroupAddressesAction, &QAction::triggered, this, &MainWindow::projectGroupAddresses);
    projectTradesAction = new QAction(QIcon::fromTheme("Trades"), tr("KNX > Project > Installations > Installation > Trades"), this);
    projectTradesAction->setEnabled(false);
    connect(projectTradesAction, &QAction::triggered, this, &MainWindow::projectTrades);
    projectManufacturerDataAction = new QAction(QIcon::fromTheme("ManufacturerData"), tr("KNX > ManufacturerData"), this);
    projectManufacturerDataAction->setEnabled(false);
    connect(projectManufacturerDataAction, &QAction::triggered, this, &MainWindow::projectManufacturerData);
    projectMasterDataAction = new QAction(QIcon::fromTheme("MasterData"), tr("KNX > MasterData"), this);
    projectMasterDataAction->setEnabled(false);
    connect(projectMasterDataAction, &QAction::triggered, this, &MainWindow::projectMasterData);

    /* Actions - KNX */
    knxBusInterfacesAction = new QAction(QIcon::fromTheme("Bus"), tr("Bus Interfaces"), this);
    connect(knxBusInterfacesAction, &QAction::triggered, this, &MainWindow::knxBusInterfaces);
    /* Actions - KNX - Application Layer */
    knxApplicationLayerADCViewAction = new QAction(tr("ADC Read"), this);
    knxApplicationLayerADCViewAction->setEnabled(false);
    connect(knxApplicationLayerADCViewAction, &QAction::triggered, this, &MainWindow::knxADCView);
    knxApplicationLayerAuthorizeViewAction = new QAction(tr("Authorize Request"), this);
    knxApplicationLayerAuthorizeViewAction->setEnabled(false);
    connect(knxApplicationLayerAuthorizeViewAction, &QAction::triggered, this, &MainWindow::knxAuthorizeView);
    knxApplicationLayerDeviceDescriptorViewAction = new QAction(tr("DeviceDescriptor InfoReport/Read"), this);
    connect(knxApplicationLayerDeviceDescriptorViewAction, &QAction::triggered, this, &MainWindow::knxDeviceDescriptorView);
    knxApplicationLayerDomainAddressViewAction = new QAction(tr("DomainAddress (Selective/SerialNumber) Read/Write"), this);
    knxApplicationLayerDomainAddressViewAction->setEnabled(false);
    connect(knxApplicationLayerDomainAddressViewAction, &QAction::triggered, this, &MainWindow::knxDomainAddressView);
    knxApplicationLayerFileStreamViewAction = new QAction(tr("FileStream InfoReport"), this);
    knxApplicationLayerFileStreamViewAction->setEnabled(false);
    connect(knxApplicationLayerFileStreamViewAction, &QAction::triggered, this, &MainWindow::knxFileStreamView);
    knxApplicationLayerGroupValueViewAction = new QAction(QIcon::fromTheme("GroupAddress"), tr("GroupValue Read/Write"), this);
    connect(knxApplicationLayerGroupValueViewAction, &QAction::triggered, this, &MainWindow::knxGroupValueView);
    knxApplicationLayerKeyViewAction = new QAction(tr("Key Write"), this);
    knxApplicationLayerKeyViewAction->setEnabled(false);
    connect(knxApplicationLayerKeyViewAction, &QAction::triggered, this, &MainWindow::knxKeyView);
    knxApplicationLayerLinkViewAction = new QAction(tr("Link Read/Write"), this);
    knxApplicationLayerLinkViewAction->setEnabled(false);
    connect(knxApplicationLayerLinkViewAction, &QAction::triggered, this, &MainWindow::knxLinkView);
    knxApplicationLayerMemoryViewAction = new QAction(tr("(User)Memory(Bit) Read/Write"), this);
    connect(knxApplicationLayerMemoryViewAction, &QAction::triggered, this, &MainWindow::knxMemoryView);
    knxApplicationLayerNetworkParameterViewAction = new QAction(tr("NetworkParameter InfoReport/Read/Write"), this);
    knxApplicationLayerNetworkParameterViewAction->setEnabled(false);
    connect(knxApplicationLayerNetworkParameterViewAction, &QAction::triggered, this, &MainWindow::knxNetworkParameterView);
    knxApplicationLayerPropertyViewAction = new QAction(tr("(Function) Property Value/Description Read/Write"), this);
    connect(knxApplicationLayerPropertyViewAction, &QAction::triggered, this, &MainWindow::knxPropertyView);
    knxApplicationLayerRestartViewAction = new QAction(tr("Restart"), this);
    connect(knxApplicationLayerRestartViewAction, &QAction::triggered, this, &MainWindow::knxRestartView);
    knxApplicationLayerSystemNetworkParameterViewAction = new QAction(tr("SystemNetworkParameter Read/Write"), this);
    knxApplicationLayerSystemNetworkParameterViewAction->setEnabled(false);
    connect(knxApplicationLayerSystemNetworkParameterViewAction, &QAction::triggered, this, &MainWindow::knxSystemNetworkParameterView);
    knxApplicationLayerUserManufacturerInfoViewAction = new QAction(tr("UserManufacturerInfo Read"), this);
    knxApplicationLayerUserManufacturerInfoViewAction->setEnabled(false);
    connect(knxApplicationLayerUserManufacturerInfoViewAction, &QAction::triggered, this, &MainWindow::knxUserManufacturerInfoView);
    /* Actions - KNX - Network Management */
    knxNetworkManagementIndividualAddressViewAction = new QAction(tr("Individual Address Read/Write"), this);
    connect(knxNetworkManagementIndividualAddressViewAction, &QAction::triggered, this, &MainWindow::knxNetworkManagementIndividualAddressView);
    /* Actions - KNX - Device Management */

    /* Actions - Window */
    windowCloseAction = new QAction(QIcon::fromTheme("window-close"), tr("Close"), this);
    windowCloseAction->setShortcuts(QKeySequence::Close);
    connect(windowCloseAction, &QAction::triggered, this, &MainWindow::windowClose);

    /* Actions - Help */
    helpDebugAction = new QAction("Debug", this);
    connect(helpDebugAction, &QAction::triggered, this, &MainWindow::helpDebug);
    helpAboutAction = new QAction(QIcon::fromTheme("help-about"), tr("About"), this);
    connect(helpAboutAction, &QAction::triggered, this, &MainWindow::helpAbout);
    helpAboutQtAction = new QAction(tr("About Qt"), this);
    connect(helpAboutQtAction, &QAction::triggered, this, &MainWindow::helpAboutQt);

    /* recent file list */
    for (int i = 0; i < maxRecentPaths; ++i) {
        QAction * recentPathAction = new QAction(this);
        recentPathAction->setVisible(false);
        recentPathActions.append(recentPathAction);
        connect(recentPathAction, &QAction::triggered, this, &MainWindow::documentOpenRecent);
    }
}

void MainWindow::createMenus()
{
    /* Menu Bar */
    // File
    fileMenu = menuBar()->addMenu(tr("File"));
    //    fileMenu->addAction(documentNewAction);
    fileMenu->addAction(documentOpenFileAction);
    fileMenu->addAction(documentOpenDirAction);
    recentPathsMenu = fileMenu->addMenu(QIcon::fromTheme("document-open-recent"), tr("Open Recent"));
    fileMenu->addMenu(recentPathsMenu);
    for (QAction * recentPathAction : recentPathActions) {
        recentPathsMenu->addAction(recentPathAction);
    }
    importMenu = fileMenu->addMenu(QIcon::fromTheme("document-import"), tr("Import"));
    importMenu->addAction(documentImportKnxProdAction);
#if defined(OPTION_EXPORT_OpenHAB) || defined(OPTION_EXPORT_SmartHomeNG)
    exportMenu = fileMenu->addMenu(QIcon::fromTheme("document-export"), tr("Export"));
#endif
#ifdef OPTION_EXPORT_OpenHAB
    exportMenu->addAction(documentExportAsOpenHABAction);
#endif
#ifdef OPTION_EXPORT_SmartHomeNG
    exportMenu->addAction(documentExportAsSmartHomeNGAction);
#endif
    fileMenu->addAction(documentCloseAction);
    //    fileMenu->addAction(documentSaveAction);
    //    fileMenu->addAction(documentSaveAsAction);
    fileMenu->addSeparator();
    fileMenu->addAction(applicationExitAction);
    // Edit
    //    editMenu = menuBar()->addMenu(tr("Edit"));
    //    editMenu->addAction(editCutAction);
    //    editMenu->addAction(editCopyAction);
    //    editMenu->addAction(editPasteAction);
    //    editMenu->addAction(editDeleteAction);
    // KNX
    knxMenu = menuBar()->addMenu(tr("KNX"));
    knxMenu->addAction(knxBusInterfacesAction);
    knxApplicationLayerMenu = knxMenu->addMenu(tr("Application Layer"));
    knxApplicationLayerMenu->addSection("Services (Group)");
    knxApplicationLayerMenu->addAction(knxApplicationLayerGroupValueViewAction);
    knxApplicationLayerMenu->addSection("Services (Broadcast)");
    knxApplicationLayerMenu->addAction(knxApplicationLayerNetworkParameterViewAction);
    knxApplicationLayerMenu->addSection("Services (System Broadcast)");
    knxApplicationLayerMenu->addAction(knxApplicationLayerDomainAddressViewAction);
    knxApplicationLayerMenu->addAction(knxApplicationLayerSystemNetworkParameterViewAction);
    knxApplicationLayerMenu->addSection("Services (Individual)");
    knxApplicationLayerMenu->addAction(knxApplicationLayerDeviceDescriptorViewAction);
    knxApplicationLayerMenu->addAction(knxApplicationLayerRestartViewAction);
    knxApplicationLayerMenu->addAction(knxApplicationLayerFileStreamViewAction);
    knxApplicationLayerMenu->addAction(knxApplicationLayerPropertyViewAction);
    knxApplicationLayerMenu->addAction(knxApplicationLayerLinkViewAction);
    knxApplicationLayerMenu->addSection("Services (Connected)");
    knxApplicationLayerMenu->addAction(knxApplicationLayerADCViewAction);
    knxApplicationLayerMenu->addAction(knxApplicationLayerMemoryViewAction);
    knxApplicationLayerMenu->addAction(knxApplicationLayerUserManufacturerInfoViewAction);
    knxApplicationLayerMenu->addAction(knxApplicationLayerAuthorizeViewAction);
    knxApplicationLayerMenu->addAction(knxApplicationLayerKeyViewAction);
    knxNetworkManagementMenu = knxMenu->addMenu(tr("Network Management"));
    // @todo Network Management
    /* NM_IndividualAddress_Read */
    /* NM_IndividualAddress_Write */
    knxNetworkManagementMenu->addAction(knxNetworkManagementIndividualAddressViewAction);
    /* NM_IndividualAddress_SerialNumber_Read */
    /* NM_IndividualAddress_SerialNumber_Write */
    /* NM_IndividualAddress_SerialNumber_Write2 */
    /* NM_IndividualAddress_SerialNumber_Write3 */
    /* NM_DomainAddress_Read */
    /* NM_DomainAndIndividualAddress_Read */
    /* NM_DomainAndIndividualAddress_Write */
    /* NM_DomainAndIndividualAddress_Write2 */
    /* NM_DomainAndIndividualAddress_Write3 */
    /* NM_DomainAddress_Scan2 */
    /* NM_Router_Scan */
    /* NM_SubnetworkDevices_Scan */
    /* NM_IndividualAddress_Reset */
    /* NM_IndividualAddress_Check */
    /* NM_Read_SerialNumber_by_ProgrammingMode */
    /* NM_Read_SerialNumber_By_ExFactoryState */
    /* NM_Read_SerialNumber_By_PowerReset */
    /* NM_Read_SerialNumber_Manufacturer */
    /* NM_NetworkParameter_Write_R */
    /* NM_IndividualAddress_Check_LocalSubNetwork */
    /* NM_IndividualAddress_SerialNumber_Report */
    /* NM_NetworkParameter_Read_R */
    /* NM_GroupAddress_Scan */
    /* NM_ObjectIndex_Read */
    /* NM_SerialNumberDefaultIA_Scan */
    knxDeviceManagementMenu = knxMenu->addMenu(tr("Device Management"));
    /* DM_Connect */
    /* DM_DeviceDescriptor_InfoReport */
    /* DM_Disconnect */
    /* DM_Identify */
    /* DM_Authorize */
    /* DM_Authorize2_RCo */
    /* DM_SetKey */
    /* DM_SetKey_RCo */
    /* DM_Restart */
    /* DM_Restart_RCl */
    /* DM_Restart_RCo */
    /* DM_Delay */
    /* DM_IndividualAddressRead */
    /* DM_IndividualAddressWrite */
    /* DM_DomainAddress_Read */
    /* DM_DomainAddressWrite */
    /* DM_ProgMode_Switch */
    /* DM_PeiTypeVerify */
    /* DM_PeiTypeRead */
    /* DM_MemWrite */
    /* DM_MemVerify */
    /* DM_MemRead */
    /* DM_UserMemWrite */
    /* DM_UserMemVerify */
    /* DM_UserMemRead */
    /* DM_InterfaceObjectWrite */
    /* DM_InterfaceObjectVerify */
    /* DM_InterfaceObjectRead */
    /* DM_InterfaceObjectScan */
    /* DM_InterfaceObjectInfoReport */
    /* DM_FunctionProperty_Write_R */
    /* DM_LoadStateMachineWrite */
    /* DM_LoadStateMachineVerify */
    /* DM_LoadStateMachineVerify_RCo_Mem */
    /* DM_LoadStateMachineVerify_R_IO */
    /* DM_LoadStateMachineRead */
    /* DM_RunStateMachineWrite */
    /* DM_RunStateMachineVerify */
    /* DM_RunStateMachineRead */
    /* DM_GroupObjectLink_Read */
    /* DM_GroupObjectLink_Write */
    /* DM_GroupObjectLink_Read_RCl */
    /* DM_GroupObjectLink_Write_RCl */
    /* DM_LCSlaceMemWrite */
    /* DM_LCSlaveMemVerify */
    /* DM_LCSlaveMemRead */
    /* DM_LCSlaveMemWrite */
    /* DM_LCExtMemVerify */
    /* DM_LCExtMemRead */
    /* DM_LCExtMemOpen */
    /* DM_LCRouteTableStateWrite */
    /* DM_LCRouteTableStateVerify */
    /* DM_LCRouteTableStateRead */
    // ETS Project
    etsProjectMenu = menuBar()->addMenu(tr("ETS Project"));
    etsProjectMenu->addAction(projectKNXAction);
    etsProjectMenu->addSection("Project");
    etsProjectMenu->addAction(projectInstallationAction);
    etsProjectMenu->addAction(projectTopologyAction);
    etsProjectMenu->addAction(projectLocationsAction);
    etsProjectMenu->addAction(projectGroupAddressesAction);
    etsProjectMenu->addAction(projectTradesAction);
    etsProjectMenu->addSection("ManufacturerData");
    etsProjectMenu->addAction(projectManufacturerDataAction);
    etsProjectMenu->addSection("MasterData");
    etsProjectMenu->addAction(projectMasterDataAction);
    // Window
    windowMenu = menuBar()->addMenu(tr("Window"));
    windowMenu->addAction(windowCloseAction);
    // Help
    helpMenu = menuBar()->addMenu(tr("Help"));
    helpMenu->addAction(helpDebugAction);
    helpMenu->addSeparator();
    helpMenu->addAction(helpAboutAction);
    helpMenu->addAction(helpAboutQtAction);
}

void MainWindow::createToolBars()
{
    // File
    fileToolBar = addToolBar(tr("File"));
    //    fileToolBar->addAction(documentNewAction);
    fileToolBar->addAction(documentOpenFileAction);
    fileToolBar->addAction(documentOpenDirAction);
    //    fileToolBar->addAction(documentSaveAction);
    // KNX
    knxToolBar = addToolBar(tr("KNX"));
    knxToolBar->addAction(knxBusInterfacesAction);
    knxToolBar->addSeparator();
    knxToolBar->addAction(knxApplicationLayerGroupValueViewAction);
    // Project
    etsProjectToolBar = addToolBar(tr("Project"));
    etsProjectToolBar->addAction(projectLocationsAction);
    etsProjectToolBar->addAction(projectGroupAddressesAction);
    etsProjectToolBar->addAction(projectTopologyAction);
    etsProjectToolBar->addAction(projectTradesAction);
    etsProjectToolBar->addAction(projectInstallationAction);
}

void MainWindow::createStatusBar()
{
    statusBar()->showMessage(tr("Ready"));
}

void MainWindow::setCurrentPath(const QString & path)
{
    if (!connection) {
        return;
    }
    auto * projectBundle = connection->getProjectBundle();
    if (!projectBundle) {
        return;
    }
    projectBundle->currentPath = path;
    setWindowFilePath(projectBundle->currentPath);

    QSettings settings;
    QStringList recentPaths = settings.value("RecentPaths").toStringList();
    recentPaths.removeAll(path);
    recentPaths.prepend(path);
    while (recentPaths.size() > maxRecentPaths) {
        recentPaths.removeLast();
    }
    settings.setValue("RecentPaths", recentPaths);

    for (QWidget * widget : QApplication::topLevelWidgets()) {
        MainWindow * mainWindow = qobject_cast<MainWindow *>(widget);
        if (mainWindow) {
            mainWindow->updateRecentPathActions();
        }
    }
}

void MainWindow::updateRecentPathActions()
{
    QSettings settings;
    QStringList recentPaths = settings.value("RecentPaths").toStringList();

    int recentPathsCount = qMin(recentPaths.size(), maxRecentPaths);

    for (int i = 0; i < recentPathsCount; ++i) {
        recentPathActions[i]->setText(QString("%1 - %2").arg(i + 1).arg(recentPaths[i]));
        recentPathActions[i]->setData(recentPaths[i]);
        recentPathActions[i]->setVisible(true);
    }
    for (int i = recentPathsCount; i < maxRecentPaths; ++i) {
        recentPathActions[i]->setText(QString());
        recentPathActions[i]->setData(QVariant());
        recentPathActions[i]->setVisible(false);
    }
}
