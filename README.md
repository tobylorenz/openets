# OpenETS

OpenETS allows configuration of KNX networks and devices.

OpenETS aims to provide an Open Source alternative for the well-known KNX configuration tool. While the original tool is good for most installers, it lacks expert functionality, e.g. direct table and parameter modifications, or memory access.

OpenETS supports ETS project files. However the digital signatures are not updated on save, so the files cannot be imported back into the original tool yet.

For me personally the original software lacks performance, usability, and expert functionality. It is also not multi-platform. It only runs under Windows operating system, but not Linux, Android, or iOS.

## Compilation instructions

### Docker environment

There is a `Dockerfile` containing all source code dependencies (except KNX lib) and used development tools used.
The environment can be started with the `docker-run.sh` script.

For those who prefer to manually install dependencies, the chapter "Standard dependencies" contains the instructions.

### KNX dependency

OpenETS is based on KNX library available here https://bitbucket.org/tobylorenz/knx/

Check the build instructions in this repository. The aforementioned Docker contained can also be used to compile KNX lib.

In the OpenETS project there is a cmake find module responsible to search for KNX dependency here: `cmake/modules/FindKNX.cmake`.

The `KNX_INCLUDE_DIR` is configured in a way that KNX and OpenETS need to be checked out beside it each other.

The `KNX_LIBRARY` expects the binary directory, where QtCreator would place it by default, assuming gcc or clang are used and it's a Debug build.

Adjust both settings to your needs.

### Standard dependencies

You need the following dependencies:

- Qt6 (Core, Gui, Widgets)
- ZLIB
- Threads
- Asio
- QuaZip for Qt6
- yaml-cpp (optional)

You can install these dependencies under Debian/Ubuntu using
```
apt install qt6-base-dev qt6-base-dev-tools zlib1g-dev libc6-dev libasio-dev libquazip1-qt6-dev libyaml-cpp-dev
```

### Build

Compilation is done as usual with cmake:
```
mkdir build
cd build
cmake ..
cmake --build .
```

## Developer information

### Directory Structure
The folders contains the following parts:

- **Connection**: Links KNX Stack and ETS Project
  - Device Data (Individual Addresses / Devices)
    - Individual Services
      - Device Descriptor
      - Restart
      - File Stream
      - (Function) Property Value
      - Link
    - Connected Services
      - ADC Read
      - (User)Memory
      - User Manufacturer Info
      - Authorize Request
      - Key Write
  - Group Data (Group Addresses / Group Values)

- **Exports**: Exports to other formats and tools

- **KNX**: KNX Stack integration into Qt
  - Stack
  - KNXnet / IP
    - Discovery Channel
    - Communication Channels
      - Device Management
      - Tunnelling
      - Routing
      - Remote Logging
      - Remote Configuration and Diagnosis
      - Object Server

- **Project**: ETS Project Import
  - Project
  - ManufacturerData
  - MasterData

- **Widgets**: OpenETS User Interface

- **cmake**: CMake find modules

- **os_conf**: Android make files

## ToDos

- all needed features for a smooth configuration
- global highlighting, if a configuration option is selected, the corresponding parameter references and memory locations in other windows get highlighted too
- global drag & drop, to allow group object linkage, etc.
- all @todo's in the source code
  - Full support for ETS project versions other than v20.
- KNX Secure
- remaining icons
- ETS project save including signing
- Configuration options should be presented in three levels:
  - Basic: Only Device and Network Management, ETS Project Parameter Blocks and Parameter References
  - Advanced: Basic plus Interface Objects (Group Address Table, Group Object Associaion Table, ...)
  - Expert: Advanced plus Application Layer (Memory access, ...)
- Clearly split KNX and ETS functionality, also with compile time configuration, e.g. have an OpenETS config only, without KNX, or have an KNX only without ETS project is a possibility.
